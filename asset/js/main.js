$(document).ready(function () {

    $('tfoot').each(function () {
        $(this).insertAfter($(this).siblings('thead'));
    });

    bsCustomFileInput.init();
    $('#reservationdate').datetimepicker({
        format: 'L'
    });

    $('#reservation').daterangepicker()
    $('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });

    $( ".rupiah" ).keyup(function() {
        var nominal = $(this).val();
        nominal = formatRupiah(nominal);
        $(this).val(nominal);
    });

    $("select").closest("form").on("reset",function(ev){
        var targetJQForm = $(ev.target);
        setTimeout((function(){
            this.find("select").trigger("change");
        }).bind(targetJQForm),0);
    });
});

function formatRupiah(angka){
    var number_string = angka.replace(/[^,-\d]/g, '').toString(),
    split           = number_string.split(','),
    sisa            = split[0].length % 3,
    rupiah          = split[0].substr(0, sisa),
    ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
    if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return rupiah;
}

$(function () {
	$('.select2').select2()
	$('.select2bs4').select2({
		theme: 'bootstrap4'
	});
});

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#preview').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	}
}

$(".upload-photo").change(function() {
  if(this.files.length > 0){
    readURL(this);
  }
});

function preview(el)
{
    const url = $(el).attr('data-href');
    const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
    const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

    const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    const systemZoom = width / window.screen.availWidth;
    const left = (width - 800) / 2 / systemZoom + dualScreenLeft
    const top = (height - 600) / 2 / systemZoom + dualScreenTop
    const newWindow = window.open(url, 'preview', 
      `
      scrollbars=yes,
      width=${800 / systemZoom}, 
      height=${600 / systemZoom}, 
      top=${top}, 
      left=${left}
      `
    )
    // if (window.focus) new Window.focus();
}

function confirm_resign(el)
{
    if (confirm('Set Karyawan resign ?')) 
    {
        window.location.href = $(el).attr('data-href');
    }
    return false;
}

function confirm_del(el)
{
    if (confirm('Hapus data ini ?')) 
    {
        window.location.href = $(el).attr('data-href');
    }
    return false;
}

function confirm_cancel(el)
{
    if (confirm('Batalkan data ini ?')) 
    {
        window.location.href = $(el).attr('data-href');
    }
    return false;
}

$('.numeric').on('input', function (event) { 
    this.value = this.value.replace(/[^0-9^\-]/g, '');
});

function printContent(){
    var restorepage = $('body').html();
    var printcontent = $('.print').clone();
    $('body').empty().html(printcontent);
    window.print();
    $('body').html(restorepage);
}
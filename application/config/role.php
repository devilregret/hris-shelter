<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['employee'] 				= array(1);
$config['administrator'] 			= array(2,3);
$config['relation_officer'] 		= array(4,6,8,9,10);
$config['relation_officer_security']= array(5,7,8,9,10);
$config['payroll'] 					= array(11,12);
$config['benefit'] 					= array(13,14);
$config['personalia'] 				= array(15,16,17,18,19,20,21);
$config['recruitment'] 				= array(22,23,24,25,26,27,28);
$config['sales'] 					= array(29,30,31,32,33,34,35);
$config['accounting'] 				= array(36,37,38,39,40,41,42);
$config['legal'] 					= array(43,44,45,46,47);
$config['directur'] 				= array(98,99);

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$config['cuti_request_name']		= 'cuti_request';
$config['cuti_request_namespace']	= '2b6acec9_ccf5_43ce_922f_7a166cd72584';

$config['cuti_atasan_name']			= 'cuti_action';
$config['cuti_atasan_namespace']	= '2b6acec9_ccf5_43ce_922f_7a166cd72584';

$config['reset_payroll_name']		= 'blant_payroll2';
$config['reset_payroll_namespace']	= '2b6acec9_ccf5_43ce_922f_7a166cd72584';

$config['reset_password_name']		= 'reset_password';
$config['reset_password_namespace']	= '2b6acec9_ccf5_43ce_922f_7a166cd72584';

$config['template_registration_name']		= 'template_registration';
$config['template_registration_namespace']	= '2b6acec9_ccf5_43ce_922f_7a166cd72584';

$config['global_template_name']		= 'global_template';
$config['global_template_namespace']= '2b6acec9_ccf5_43ce_922f_7a166cd72584'; 
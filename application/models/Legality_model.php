<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Legality_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_legality	= 'm_legality';
		$this->table_company 	= 'm_company';
		$this->table_branch 	= 'm_branch';
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		$this->db->from($this->table_legality.' A');
		$this->db->select('A.*');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_legality.' A');
		$this->db->join($this->table_company.' B', 'A.company_id = B.id', 'left');
		$this->db->join($this->table_branch.' C', 'A.branch_id = C.id', 'left');
	
		$this->db->where('A.is_active', 1);
		if(isset($params['company_code'])){
			if($params['company_code'] != ''){
				$this->db->where('B.code', $params['company_code']);
			}
		}		
		if(isset($params['company_name'])){
			if($params['company_name'] != ''){
				$this->db->like('B.name', $params['company_name']);
			}
		}
		if(isset($params['branch_name'])){
			if($params['branch_name'] != ''){
				$this->db->like('C.name', $params['branch_name']);
			}
		}
		if(isset($params['date_start'])){
			if($params['date_start'] != ''){
				$this->db->like("DATE_FORMAT(A.date_start, '%d/%m/%Y')", $params['date_start']);
			}
		}
		if(isset($params['date_end'])){
			if($params['date_end'] != ''){
				$this->db->like("DATE_FORMAT(A.date_end, '%d/%m/%Y')", $params['date_end']);
			}
		}	
		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}
		if(isset($params['regulation'])){
			$this->db->like('A.regulation', $params['regulation']);
		}
		if(isset($params['note'])){
			$this->db->like('A.note', $params['note']);
		}
		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
			if($params['orderby'] == 'date_start'){
				$params['orderby'] = 'A.date_start';
			}
			if($params['orderby'] == 'date_end'){
				$params['orderby'] = 'A.date_end';
			}
		}else{
			$this->db->order_by('A.created_at', 'DESC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function gets_expired($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_legality.' A');
		$this->db->join($this->table_company.' B', 'A.company_id = B.id', 'left');
		$this->db->join($this->table_branch.' C', 'A.branch_id = C.id', 'left');
		$this->db->where('A.is_active', 1);

		if(isset($params['expired_day'])){
			$this->db->where("A.date_end != '0000-00-00' AND A.date_end <= DATE_FORMAT(NOW() - INTERVAL ".$params['expired_day']." DAY, '%Y-%m-%d')") ;	
		}

		if(isset($params['expired_month'])){
			$this->db->where("A.date_end != '0000-00-00' AND A.date_end <= DATE_FORMAT(NOW() - INTERVAL ".$params['expired_month']." MONTH, '%Y-%m-%d')") ;	
		}

		if ($return_count){
			$result = $this->db->count_all_results();
			return $result;
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.date_end', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_legality, $params);
			$id = $params['id'];
		}else{
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_legality, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_model extends CI_Model{

	private $table_user;
	private $table_email;

	public function __construct()
	{
		parent::__construct();

		$this->table_user	= 'm_user';
		$this->table_email	= 'm_email_template';
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		$this->db->from($this->table_email.' A');
		$this->db->select('A.*');

		$result = $this->db->get()->row();
		return $result;
	}

	public function preview($params = array())
	{
		
		$this->db->where('A.id', $params['id']);
		$this->db->where('A.is_active', 1);
		
		$this->db->from($this->table_email.' A');
		$this->db->join($this->table_user.' B', 'A.created_by = B.id', 'left');
		$this->db->join($this->table_user.' C', 'A.updated_by = C.id', 'left');

		$this->db->select('A.*, B.full_name AS creator, C.full_name AS updater');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_email.' A');
	
		$this->db->where('A.is_active', 1);
		
		if(isset($params['type'])){
			$this->db->where('A.type', $params['type']);
		}
		
		if(isset($params['email_sender'])){
			$this->db->like('A.email_sender', $params['email_sender']);
		}
		
		if(isset($params['name_sender'])){
			$this->db->like('A.name_sender', $params['name_sender']);
		}

		if(isset($params['subject'])){
			$this->db->like('A.subject', $params['subject']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.updated_at', 'DESC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_email, $params);
			$id = $params['id'];
		}
		return $id;
	}
}
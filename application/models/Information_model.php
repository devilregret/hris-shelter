<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Information_model extends CI_Model{
	private $table_user;
	private $table_information;
	private $table_branch;

	public function __construct()
	{
		parent::__construct();
		$this->table_user       = 'm_user';
		$this->table_information= 'm_information';
		$this->table_branch   	= 'm_branch';
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_information.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function preview($params = array())
	{
		$this->db->where('A.id', $params['id']);
		$this->db->where('A.is_active', 1);
		$this->db->from($this->table_information.' A');
		$this->db->join($this->table_branch.' B', 'A.branch_id = B.id', 'left');
		$this->db->join($this->table_user.' C', 'A.created_by = C.id');
		$this->db->join($this->table_user.' D', 'A.updated_by = D.id');

		$this->db->select('A.*, B.name AS branch_name, C.full_name AS creator, D.full_name AS updater');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_information.' A');
		$this->db->join($this->table_branch.' B', 'A.branch_id = B.id', 'left');
		
		$this->db->where('A.is_active', 1);

		if(isset($params['session_branch'])){
			$this->db->where('A.branch_id', $params['session_branch']);
		}

		if(isset($params['branch_id'])){
			$this->db->where('A.branch_id', $params['branch_id']);
		}
		
		if(isset($params['active'])){
			$this->db->where(" NOW() BETWEEN A.start_date AND A.end_date ");
		}

		if(isset($params['start_date'])){
			$this->db->like('DATE_FORMAT(A.start_date, "%Y-%m-%d")', $params['start_date']);
		}

		if(isset($params['end_date'])){
			$this->db->like('DATE_FORMAT(A.end_date, "%Y-%m-%d")', $params['end_date']);
		}

		if(isset($params['title'])){
			$this->db->like('A.title', $params['title']);
		}
		
		if(isset($params['branch_name'])){
			$this->db->where("(B.name LIKE '%".$params['branch_name']."%') OR B.name IS NULL");
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*, B.name AS branch_name');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_information, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_information, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
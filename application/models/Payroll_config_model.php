<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll_config_model extends CI_Model{
	private $table_site;
	private $table_payroll_config;

	public function __construct()
	{
		parent::__construct();
		$this->table_site			= 'm_site';
		$this->table_payroll_config	= 't_payroll_config';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_payroll_config.' A');
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('B.id', $params['id']);
		}

		if(isset($params['site_id'])){
			$this->db->where('A.id', $params['site_id']);
		}

		if(isset($params['is_name'])){
			$this->db->where('A.name', $params['is_name']);
		}

		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->from($this->table_site.' A');
		$this->db->join($this->table_payroll_config.' B', 'A.id = B.site_id', 'left');
		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{

		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_payroll_config, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_payroll_config, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Candidate_temporary_model extends CI_Model{
	public function __construct()
	{
		parent::__construct();
		$this->table_candidate_temporary	= 'm_candidate_temporary';
		$this->table_city				= 'm_city';
		$this->table_province			= 'm_province';
	}

	public function get($params = array())
	{
		
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', 1);
		}
		
		if(isset($params['id_card'])){
			$this->db->where('A.id_card', $params['id_card']);
		}

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_candidate_temporary.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function preview($params = array())
	{
		$this->db->from($this->table_candidate_temporary.' A');
		$this->db->join($this->table_city.' B', 'A.city_id = B.id', 'left');
		$this->db->join($this->table_province.' C', 'B.province_id = C.id', 'left');

		$this->db->where('A.is_active', 1);
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_candidate_temporary.' A');
		$this->db->join($this->table_city.' B', 'A.city_id = B.id', 'left');
		$this->db->join($this->table_province.' C', 'B.province_id = C.id', 'left');

		$this->db->where('A.is_active', 1);
		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('C.branch_id', $params['branch_id']);
			}
		}
		if(isset($params['type'])){
			if($params['type'] != ''){
				$this->db->where('A.type', $params['type']);
			}
		}
		if(isset($params['followup_status'])){
			if($params['followup_status'] != ''){
				$this->db->like('A.followup_status', $params['followup_status']);
			}
		}
		if(isset($params['created_at'])){
			if($params['created_at'] != ''){
				$this->db->like('DATE_FORMAT(A.created_at, "%d/%m/%Y")', $params['created_at']);
			}
		}
		if(isset($params['id_card'])){
			if($params['id_card'] != ''){
				$this->db->like('A.id_card', $params['id_card']);
			}
		}
		if(isset($params['full_name'])){
			if($params['full_name'] != ''){
				$this->db->like('A.full_name', $params['full_name']);
			}
		}
		if(isset($params['phone_number'])){
			if($params['phone_number'] != ''){
				$this->db->like('A.phone_number', $params['phone_number']);
			}
		}
		if(isset($params['address'])){
			if($params['address'] != ''){
				$this->db->like('A.address', $params['address']);
			}
		}
		if(isset($params['driver_license'])){
			if($params['driver_license'] != ''){
				$this->db->like('A.driver_license', $params['driver_license']);
			}
		}
		if(isset($params['vaccine_covid'])){
			if($params['vaccine_covid'] != ''){
				$this->db->like('A.vaccine_covid', $params['vaccine_covid']);
			}
		}
		if(isset($params['city_name'])){
			if($params['city_name'] != ''){
				$this->db->like('B.name', $params['city_name']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'created_at'){
				$params['orderby'] = 'A.created_at';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'DESC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_candidate_temporary, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$this->db->insert($this->table_candidate_temporary, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
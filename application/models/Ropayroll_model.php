<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ropayroll_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->table_user			= 'm_user';
		$this->table_employee		= 'm_employee';
		$this->table_site			= 'm_site';
		$this->table_company		= 'm_company';
		$this->table_payroll		= 't_payroll';
		$this->table_payroll_detail	= 't_payroll_detail';
	}
	public function report($params = array(), $return_count = FALSE){
		$this->db->from($this->table_payroll.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		$this->db->join($this->table_site.' C', 'B.site_id = C.id', 'left');
		$this->db->join($this->table_company.' D', 'C.company_id = D.id', 'left');
		
		$this->db->where('A.is_active', 1);
		if(isset($params['site_name'])){
			if($params['site_name'] != ''){
				$this->db->like('C.name', $params['site_name']);
			}
		}
		if(isset($params['company_code'])){
			if($params['company_code'] != ''){
				$this->db->like('D.code', $params['company_code']);
			}
		}
		if(isset($params['payment'])){
			$this->db->where_in('A.payment', $params['payment']);
		}
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		$this->db->group_by('C.id, A.periode_end');

		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'period'){
				$params['orderby'] = 'A.periode_end';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.contract_end', 'ASC');
		}
		if ($return_count){
			return $this->db->count_all_results();
		}
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		$result = $this->db->get()->result();
		return $result;
	}
	public function resume($params = array()){
		if(isset($params['periode_end'])){
			$query 	= "SELECT COUNT(p.employee_id) AS employee_payroll, SUM(p.salary) AS summary_payroll, p.periode_start, p.periode_end FROM t_payroll p LEFT JOIN m_employee e ON p.employee_id = e.id WHERE p.periode_end = '".$params['periode_end']."' AND p.is_active = 1 AND e.site_id = '".$params['site_id']."' AND p.payment = 'Selesai' GROUP BY p.periode_end";
			$result = $this->db->query($query)->row();
		}else if(isset($params['site_id'])){
			$query 	= "SELECT COUNT(p.employee_id) AS employee_payroll, SUM(p.salary) AS summary_payroll, p.periode_start, p.periode_end FROM t_payroll p LEFT JOIN m_employee e ON p.employee_id = e.id WHERE p.periode_end = (SELECT MAX(tp.periode_end) FROM t_payroll tp LEFT JOIN m_employee me ON tp.employee_id = me.id WHERE tp.is_active = 1 AND me.site_id = '".$params['site_id']."' AND tp.payment = 'Selesai') AND p.is_active = 1 AND e.site_id = '".$params['site_id']."' AND p.payment = 'Selesai' GROUP BY p.periode_end";
			$result = $this->db->query($query)->row();
		}else{
			$result = $this->db->query("SELECT COUNT(employee_id) AS employee_payroll, SUM(salary) AS summary_payroll, MAX(periode_start) AS periode_start, MAX(periode_end) AS periode_end FROM t_payroll WHERE periode_end = (SELECT MAX(periode_end) FROM t_payroll WHERE is_active = 1) AND is_active = 1 GROUP BY periode_end")->row();
		}
		return $result;
	}

	public function resume_submission(){
		$result = $this->db->query("SELECT s.name AS site_name, s.address AS site_address, SUM(p.salary) AS summary_payroll, count(p.id) AS payroll_submission, count(p.employee_id) AS employee_submission FROM t_payroll p INNER JOIN m_employee e ON p.employee_id = e.id INNER JOIN m_site s ON e.site_id = s.id WHERE p.payment = 'Menunggu' AND p.is_active = 1 GROUP BY s.id")->result();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_payroll.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		
		$this->db->where('A.is_active', '1');
		
		if(isset($params['site_id'])){
			$this->db->where('B.site_id', $params['site_id']);
		}

		if(isset($params['employee_name'])){
			$this->db->like('B.full_name', $params['employee_name']);
		}

		if(isset($params['id_card'])){
			$this->db->like('B.id_card', $params['id_card']);
		}

		if(isset($params['benefit_labor_note'])){
			$this->db->like('B.benefit_labor_note', $params['benefit_labor_note']);
		}

		if(isset($params['benefit_labor'])){
			$this->db->like('B.benefit_labor', $params['benefit_labor']);
		}

		if(isset($params['benefit_health_note'])){
			$this->db->like('B.benefit_health_note', $params['benefit_health_note']);
		}

		if(isset($params['benefit_health'])){
			$this->db->like('B.benefit_health', $params['benefit_health']);
		}

		if(isset($params['employee_email'])){
			$this->db->like('B.email', $params['employee_email']);
		}

		if(isset($params['periode_start'])){
			$this->db->like('DATE_FORMAT(A.periode_start, "%d/%m/%Y")', $params['periode_start']);
		}

		if(isset($params['periode_end'])){
			$this->db->like('DATE_FORMAT(A.periode_end, "%d/%m/%Y")', $params['periode_end']);
		}

		if(isset($params['salary'])){
			$this->db->like('A.salary', $params['salary']);
		}

		if(isset($params['bpjs_ks'])){
			$this->db->like('A.bpjs_ks', $params['bpjs_ks']);
		}

		if(isset($params['bpjs_jht'])){
			$this->db->like('A.bpjs_jht', $params['bpjs_jht']);
		}

		if(isset($params['bpjs_jp'])){
			$this->db->like('A.bpjs_jp', $params['bpjs_jp']);
		}

		if(isset($params['payment'])){
			$this->db->like('A.payment', $params['payment']);
		}

		if(isset($params['email'])){
			$this->db->like('A.email', $params['email']);
		}

		if(isset($params['note'])){
			$this->db->like('A.note', $params['note']);
		}
		
		if(isset($params['not_payment'])){
			$this->db->where_not_in('A.payment', $params['not_payment']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'periode_start'){
				$params['orderby'] = 'A.periode_start';
			}
			if($params['orderby'] == 'periode_end'){
				$params['orderby'] = 'A.periode_end';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('B.full_name', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->group_by('A.id');
		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', $params['is_active']);
		
		}else{
			$this->db->where('A.is_active', 1);
		}

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['periode_start'])){
			$this->db->where('A.periode_start', $params['periode_start']);
		}

		if(isset($params['periode_end'])){
			$this->db->where('A.periode_end', $params['periode_end']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_payroll.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_payroll, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_payroll, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}

	public function gets_detail($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_payroll_detail.' A');
		
		$this->db->where('A.is_active', '1');
		
		if(isset($params['payroll_id'])){
			$this->db->where('A.payroll_id', $params['payroll_id']);
		}

		if(isset($params['category'])){
			$this->db->like('A.category', $params['category']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.payroll_id', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function delete_detail($params = array())
	{
		$this->db->where('payroll_id',$params['payroll_id']);
		$this->db->delete($this->table_payroll_detail);
	}

	public function save_detail($params = array())
	{
		$params['is_active']  = 1;
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_payroll_detail, $params);
			$id = $this->db->insert_id();
		return $id;
	}
}
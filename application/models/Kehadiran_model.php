<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kehadiran_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->table_employee		= 'm_employee';
		$this->table_kehadiran		= 't_rekap_kehadiran';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_kehadiran.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id ', 'left');

		$this->db->where('A.is_active', '1');

		if(isset($params['site_id'])){
			if($params['site_id'] != ''){
				$this->db->where('B.site_id', $params['site_id']);
			}
		}

		if(isset($params['full_name'])){
			if($params['full_name'] != ''){
				$this->db->like('B.full_name', $params['full_name']);
			}
		}

		if(isset($params['id_card'])){
			if($params['id_card'] != ''){
				$this->db->like('B.id_card', $params['id_card']);
			}
		}

		if(isset($params['periode'])){
			if($params['periode'] != ''){
				$this->db->like('A.periode', $params['periode']);
			}
		}

		if(isset($params['periode_start'])){
			if($params['periode_start'] != ''){
				$this->db->where('A.periode >=', $params['periode_start']);
			}
		}

		if(isset($params['periode_end'])){
			if($params['periode_end'] != ''){
				$this->db->where('A.periode <=', $params['periode_end']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('B.full_name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['periode'])){
			$this->db->where('A.periode', $params['periode']);
		}

		if(isset($params['periode_start'])){
			if($params['periode_start'] != ''){
				$this->db->where('A.periode >=', $params['periode_start']);
			}
		}

		if(isset($params['periode_end'])){
			if($params['periode_end'] != ''){
				$this->db->where('A.periode <=', $params['periode_end']);
			}
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_kehadiran.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_kehadiran, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_kehadiran, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Workday_model extends CI_Model{
	private $table_role;
	private $table_user;
	
	public function __construct()
	{
		parent::__construct();
		$this->table_user		= 'm_user';
		$this->table_workday	= 'm_work_day';
		$this->table_site		= 'm_site';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_workday.' A');
		
		$this->db->where('A.is_active', 1);

		if(isset($params['day'])){
			$this->db->like('A.day', $params['day']);
		}

		if(isset($params['hours_start'])){
			$this->db->like('A.hours_start', $params['hours_start']);
		}

		if(isset($params['hours_end'])){
			$this->db->like('A.hours_end', $params['hours_end']);
		}

		if(isset($params['shift'])){
			$this->db->like('A.shift', $params['shift']);
		}


		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'desc');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->group_by('A.id');
		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', $params['is_active']);
		
		}else{
			$this->db->where('A.is_active', 1);
		}

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if(isset($params['shift'])){
			$this->db->where('A.shift', $params['shift']);
		}

		if(isset($params['day'])){
			$this->db->where('A.day', $params['day']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_workday.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function preview($params = array())
	{
		
		$this->db->where('A.id', $params['id']);
		$this->db->where('A.is_active', 1);

		$this->db->from($this->table_workday.' A');
		$this->db->join($this->table_site.' B', 'A.site_id = B.id');
		$this->db->join($this->table_user.' D', 'A.created_by = D.id', 'left');
		$this->db->join($this->table_user.' E', 'A.updated_by = E.id', 'left');

		$this->db->select("A.*, B.name AS site_name, B.address, A.shift, D.full_name AS creator, E.full_name AS updater");

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_workday, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_workday, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
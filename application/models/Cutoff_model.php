<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cutoff_model extends CI_Model{
	private $table_cutoff;
	private $table_user;
	
	public function __construct()
	{
		parent::__construct();
		$this->table_user		= 'm_user';
		$this->table_cutoff		= 'm_cut_off';
		$this->table_formula	= 'm_formula';
		$this->table_site		= 'm_site';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_cutoff.' A');
		
		$this->db->where('A.is_active', 1);
		
		if(isset($params['start_date'])){
			$this->db->like('A.start_date', $params['start_date']);
		}

		if(isset($params['end_date'])){
			$this->db->like('A.end_date', $params['end_date']);
		}

		if(isset($params['description'])){
			$this->db->like('A.description', $params['description']);
		}

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if(isset($params['periode'])){
			$this->db->where('A.end_date < ', $params['periode']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.description', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->group_by('A.id');
		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);
	
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_cutoff.' A');

		$result = $this->db->get()->row();
		return $result;
	}
	
	public function preview($params = array())
	{	
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		$this->db->where('A.is_active', 1);

		$this->db->from($this->table_cutoff.' A');
		$this->db->join($this->table_site.' B', 'A.site_id = B.id');
		$this->db->join($this->table_user.' C', 'A.created_by = C.id', 'left');
		$this->db->join($this->table_user.' D', 'A.updated_by = D.id', 'left');

		$this->db->select("A.*, B.name AS site_name, B.address, C.full_name AS creator, D.full_name AS updater");

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_cutoff, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_cutoff, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
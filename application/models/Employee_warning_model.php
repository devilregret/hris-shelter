<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_warning_model extends CI_Model{

	private $table_employee;
	private $table_warning;
	private $table_user;

	public function __construct()
	{
		parent::__construct();
		$this->table_warning   	= 't_employee_warning';
		$this->table_employee   = 'm_employee';
		$this->table_user 	    = 'm_user';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_warning.' A');
		$this->db->where('A.is_active', 1);

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}else{
			$this->db->where('A.employee_id IS NULL');
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.warning_date', 'DESC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		$this->db->from($this->table_warning.' A');

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_warning, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_warning, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Position_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_user		= 'm_user';
		$this->table_position	= 'm_position';
		$this->table_company	= 'm_company';
		$this->table_employee	= 'm_employee';
	}

	public function get($params = array())
	{
		
		// $this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['is_name'])){
			$this->db->where('A.name', $params['is_name']);
		}

		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}

		if(isset($params['company'])){
			$this->db->where('A.company_id', $params['company']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->from($this->table_position.' A');
		$this->db->join($this->table_company.' B', 'A.company_id = B.id', 'left');

		$result = $this->db->get()->row();
		return $result;
	}

	public function preview($params = array())
	{
		
		$this->db->where('A.id', $params['id']);
		$this->db->where('A.is_active', 1);
		
		$this->db->from($this->table_position.' A');
		$this->db->join($this->table_user.' C', 'A.created_by = C.id', 'left');
		$this->db->join($this->table_user.' D', 'A.updated_by = D.id', 'left');

		$this->db->select('A.*, C.full_name AS creator, D.full_name AS updater');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_position.' A');
		$this->db->join($this->table_company.' B', 'A.company_id = B.id', 'left');

		$this->db->where('A.is_active', 1);

		if(isset($params['list_id'])){
			$this->db->where_in('A.id', $params['list_id']);
		}

		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}

		if(isset($params['description'])){
			$this->db->like('A.description', $params['description']);
		}

		if(isset($params['company_name'])){
			if($params['company_name'] != ''){
				$this->db->like('B.name', $params['company_name']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*, B.code AS company_code, B.name AS company_name');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function gets_indirect($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_position.' A');
		$this->db->join($this->table_employee.' B', 'A.id = B.position_id', 'left');

		$this->db->where('A.is_active', 1);

		if(isset($params['site_id'])){
			$this->db->where('B.site_id', $params['site_id']);
		}
		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}
		if(isset($params['description'])){
			$this->db->like('A.description', $params['description']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.name', 'ASC');
		}

		if(isset($params['group_by'])){
			$this->db->group_by($params['group_by']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_position, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_position, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
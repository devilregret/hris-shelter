<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Applicant_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->table_applicant	= 't_applicant';
		$this->table_employee	= 'm_employee';
	}

	public function submission_gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_applicant.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id AND B.status_approval = 2', 'inner');
		
		$this->db->where('A.is_active', 1);
		
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['vacancy_id'])){
			$this->db->where('A.vacancy_id', $params['vacancy_id']);
		}

		if(isset($params['approved_at'])){
			if($params['approved_at'] != ''){
				$this->db->like('B.approved_at', $params['approved_at']);
			}
		}

		if(isset($params['submitted_at'])){
			if($params['submitted_at'] != ''){
				$this->db->like('B.submitted_at', $params['submitted_at']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.created_at', 'DESC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function approved_gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_applicant.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id AND B.status_approval > 2', 'inner');
		
		$this->db->where('A.is_active', 1);
		
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['vacancy_id'])){
			$this->db->where('A.vacancy_id', $params['vacancy_id']);
		}

		if(isset($params['approved_at'])){
			if($params['approved_at'] != ''){
				$this->db->like('B.approved_at', $params['approved_at']);
			}
		}

		if(isset($params['submitted_at'])){
			if($params['submitted_at'] != ''){
				$this->db->like('B.submitted_at', $params['submitted_at']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.created_at', 'DESC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_applicant.' A');
		
		$this->db->where('A.is_active', 1);
		
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['vacancy_id'])){
			$this->db->where('A.vacancy_id', $params['vacancy_id']);
		}

		if(isset($params['created_at'])){
			if($params['created_at'] != ''){
				$this->db->like('A.created_at', $params['created_at']);
			}
		}

		if ($return_count){
			$count = $this->db->count_all_results();
			return $count;
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.created_at', 'DESC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);
	
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['vacancy_id'])){
			$this->db->where('A.vacancy_id', $params['vacancy_id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_applicant.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_applicant, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_applicant, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}

	public function getDataBelumTes($employeeId){
		$q = "SELECT * from (
			SELECT v.title,v.id as vacancy_id, DATE_FORMAT(ap.created_at, '%Y-%m-%d') as tanggal_apply,(SELECT name from m_province WHERE id = v.province_id ) as provinsi,
			IF(v.is_tes_psikotes=1,(select count(*)+1 from t_psikotes WHERE deleted_at is null and is_aktif = 1 and m_employee_id = em.id and m_vacancy_id = v.id),0) as count_psikotes,
			IF(v.is_tes_deret_angka=1,(select count(*)+1 from t_deret_angka WHERE deleted_at is null and is_aktif = 1 and m_employee_id = em.id and m_vacancy_id = v.id),0) as count_deret_angka,
			IF(v.is_tes_hitung_cepat=1,(select count(*)+1 from t_hitung_cepat WHERE deleted_at is null and is_aktif = 1 and m_employee_id = em.id and m_vacancy_id = v.id),0) as count_hitung_cepat,
			IF(v.is_ketelitian=1,(select count(*)+1 from t_ketelitian WHERE deleted_at is null and is_aktif = 1 and m_employee_id = em.id and m_vacancy_id = v.id),0) as count_ketelitian,
			IF(v.is_kraepelin=1,(select count(*)+1 from t_kraepelin WHERE deleted_at is null and is_aktif = 1 and m_employee_id = em.id and m_vacancy_id = v.id),0) as count_kraepelin,
			v.durasi_psikotes,v.durasi_deret_angka,v.durasi_hitung_cepat,v.durasi_ketelitian,v.durasi_kolom_kraepelin,
			(select instruksi from m_instruksi_tes where deleted_at is null and tes='psikotes') as instruksi_psikotes,
			(select instruksi from m_instruksi_tes where deleted_at is null and tes='deret_angka') as instruksi_deret_angka,
			(select instruksi from m_instruksi_tes where deleted_at is null and tes='hitung_cepat') as instruksi_hitung_cepat,
			(select instruksi from m_instruksi_tes where deleted_at is null and tes='ketelitian') as instruksi_ketelitian,
			(select instruksi from m_instruksi_tes where deleted_at is null and tes='kraepelin') as instruksi_kraepelin
			from t_applicant ap 
			INNER JOIN m_vacancy v ON ap.vacancy_id = v.id
			INNER JOIN m_employee em ON ap.employee_id = em.id
			WHERE 
			ap.is_active = 1
			and em.is_active = 1
			and v.end_date >=date(now())
			and v.created_at >='2023-07-10 00:00:00'
			and v.is_active = 1
			and ( v.is_tes_psikotes = 1 OR v.is_tes_deret_angka=1 OR v.is_tes_hitung_cepat = 1 OR v.is_tes_hitung_cepat = 1 OR v.is_ketelitian = 1  OR v.is_kraepelin = 1)
			and ap.employee_id = ".$employeeId."
			) a
			WHERE 
			count_psikotes = 1
			OR count_deret_angka = 1
			OR count_hitung_cepat = 1
			OR count_ketelitian= 1
			OR count_kraepelin= 1;";

		return $this->db->query($q)->result();
	}

	public function getInstruksiTes($tes){
		$q = "select instruksi from m_instruksi_tes where deleted_at is null and tes='".$tes."'";
		return $this->db->query($q)->result()[0]->instruksi;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_model extends CI_Model{

	private $table_user;
	public function __construct()
	{
		parent::__construct();
		$this->table_user = 'm_user';
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);
		
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_user.' A');
		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = $params['id'];
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->where('id', $id);
		$this->db->update($this->table_user, $params);
		return $id;
	}
}
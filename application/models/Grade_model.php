<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grade_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_employee			= 'm_employee';
		$this->table_position			= 'm_position';
		$this->table_user				= 'm_user';
		$this->table_grade_competency	= 'm_grade_competency';
		$this->table_grade_position		= 'm_grade_position';
		$this->table_grade				= 't_grade';
		$this->table_grade_detail		= 't_grade_detail';
	}

	// function getMasterParent()
    // {
	// 	$this->db->select('A.id, B.name as position_name');
	// 	$this->db->from($this->table_struktur_organisasi.' A');
	// 	$this->db->join($this->table_position.' B', 'A.position_id = B.id', 'left');
	// 	$this->db->where('A.is_active', 1);
	// 	$this->db->order_by('B.name', 'ASC');
		
	// 	return $this->db->get()->result();
    // }

	// public function getStrukturOrganisasi($params = array(), $return_count = FALSE)
	// {
	// 	$this->db->from($this->table_struktur_organisasi.' A');
	// 	$this->db->join($this->table_position.' B', 'A.position_id = B.id', 'left');
	// 	$this->db->join($this->table_employee.' C', 'B.id = C.position_id', 'left');
	// 	$this->db->join($this->table_struktur_organisasi.' D', 'A.parent_id = D.id', 'left');
	// 	$this->db->join($this->table_position.' E', 'D.position_id = E.id', 'left');
		
	// 	$this->db->where('A.is_active = ', 1);
	// 	if ($return_count){
	// 		return $this->db->count_all_results();
	// 	}

	// 	if(isset($params['columns'])){
	// 		$this->db->select($params['columns']);
	// 	}else{
	// 		$this->db->select('A.*, B.level_name AS child_name, D.level_name AS parent_name');
	// 	}

	// 	$result = $this->db->get()->result();
	// 	return $result;
	// }

	// public function getStrukturByParent($params = array(), $return_count = FALSE)
	// {
	// 	$this->db->from($this->table_struktur_organisasi.' A');
	// 	$this->db->join($this->table_position.' B', 'A.position_id = B.id', 'left');
	// 	$this->db->join($this->table_employee.' C', 'B.id = C.position_id', 'left');

	// 	$this->db->select('A.id, B.name as position_name, C.full_name, A.parent_id, (select count(id) from m_struktur_organisasi where parent_id =  1) as children');
	// 	$this->db->where('A.parent_id = ', $params['parent_id']);
	// 	$this->db->where('A.is_active = ', 1);

	// 	$result = $this->db->get()->result();
	// 	return $result;
	// }

	public function get($params = array())
	{
		
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['is_name'])){
			$this->db->where('A.name', $params['is_name']);
		}

		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.id, B.full_name, C.name as position_name, A.periode, A.created_at, A.created_by, A.updated_at, A.updated_by ');
		}
		
		$this->db->from($this->table_grade.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		$this->db->join($this->table_position.' C', 'A.position_id = C.id', 'left');

		$result = $this->db->get()->row();
		return $result;
	}

	public function getDetails($params = array())
	{
	
		if(isset($params['grade_id'])){
			$this->db->where('A.grade_id', $params['grade_id']);
		}

		$this->db->select('A.id, A.value,B.name');
		
		$this->db->from($this->table_grade_detail.' A');
		$this->db->join($this->table_grade_competency.' B', 'A.m_grade_competency = B.id', 'left');

		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array(), $listCompetency)
	{
		$id = false;
		if ($params['grade_id'] != '') {
			foreach($listCompetency as $kompetensi){
				$data['value'] 	= $params[$kompetensi->id];
				$data['updated_at'] = date('Y-m-d H:i:s');
				$data['updated_by'] = $this->session->userdata('user_id');	
				$this->db->where('id', $kompetensi->id);
				$this->db->update($this->table_grade_detail, $data);
				// print_r($data);
				$id = $params['grade_id'];
				// die();
			}


			$paramsGrade['updated_at'] = date('Y-m-d H:i:s');
			$paramsGrade['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['grade_id']);
			$this->db->update($this->table_grade, $paramsGrade);
			// $id = $params['id'];
		} else {
			$paramsGrade['is_active']  = 1;
			$paramsGrade['position_id'] = $params['position_id'];
			$paramsGrade['employee_id'] = $params['employee_id'];
			$paramsGrade['periode'] = $params['periode'];
			$paramsGrade['created_at'] = date('Y-m-d H:i:s');
			$paramsGrade['created_by'] = $this->session->userdata('user_id');
			$paramsGrade['updated_at'] = date('Y-m-d H:i:s');
			$paramsGrade['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_grade, $paramsGrade);
			$id = $this->db->insert_id();

			foreach($listCompetency as $kompetensi){
				$data['m_grade_competency'] 	= $kompetensi->id;
				$data['value'] 	= 0;
				$data['grade_id'] 	= $id;
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['created_by'] = $this->session->userdata('user_id');	
				$data['updated_at'] = date('Y-m-d H:i:s');
				$data['updated_by'] = $this->session->userdata('user_id');	
				$this->db->insert($this->table_grade_detail, $data);
			}
		}
		return $id;
	}

	public function delete($params = array())
	{
		$id = $params['id'];
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->where('id', $params['id']);
		$this->db->update($this->table_grade, $params);
			
		return $id;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_grade.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		$this->db->join($this->table_position.' C', 'A.position_id = C.id', 'left');
		$this->db->join($this->table_user.' D', 'A.created_by = D.id', 'left');
		$this->db->join($this->table_user.' E', 'A.updated_by = E.id', 'left');

		$this->db->where('A.is_active', 1);

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.position_id', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function getCompetencyByPositionId($params = array())
	{
		$this->db->from($this->table_grade_position.' A');
		
		$this->db->where('A.is_active', 1);
		$this->db->where('A.position_id', $params['position_id']);
		$this->db->select('A.grade_competency_id as id');

		$result = $this->db->get()->result();
		return $result;
	}

}
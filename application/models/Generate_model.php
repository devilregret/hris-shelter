<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generate_model extends CI_Model{

	function __construct()
	{
		parent::__construct();

		$this->table_user 		= 'm_user';
		$this->table_employee	= 'm_employee';
	}

	public function generate_user($params = array())
	{
		$this->db->where('A.is_active', 1);
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_user.' B', 'A.id = B.employee_id', 'INNER');
		$this->db->where('A.full_name != B.full_name');
		$this->db->select('A.full_name AS "employee_name", A.id_card, B.employee_id, B.id AS user_id, B.full_name');
		$result = $this->db->get()->result();
		return $result;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Allowance_config_model extends CI_Model{
	private $table_site;
	private $table_allowance_config;

	public function __construct()
	{
		parent::__construct();
		$this->table_site				= 'm_site';
		$this->table_allowance_config	= 't_allowance_config';
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['is_name'])){
			$this->db->where('A.name', $params['is_name']);
		}

		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->from($this->table_site.' A');
		$this->db->join($this->table_allowance_config.' B', 'A.id = B.site_id', 'left');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		$params['is_active']  = 1;
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->replace($this->table_allowance_config, $params);
		$id = $this->db->insert_id();
		return $id;
	}
}
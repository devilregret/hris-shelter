<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department_casual_model extends CI_Model{
	public function __construct()
	{
		parent::__construct();
		$this->table_department_casual	= 'm_department_casual';
	}

	public function get($params = array())
	{
		
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', 1);
		}
		
		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}
        if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_department_casual.' A');

		$result = $this->db->get()->row();
// 		print_prev($this->db->last_query());
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_department_casual.' A');
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', $params['is_active']);
		}else{
			$this->db->where('A.is_active', 1);
		}

		if(isset($params['site_id'])){
			if($params['site_id'] != ''){
				$this->db->where('A.site_id', $params['site_id']);
			}
		}

		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_department_casual, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			
			$this->db->insert($this->table_department_casual, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
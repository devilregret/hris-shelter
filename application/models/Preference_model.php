<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Preference_model extends CI_Model{
	private $table_user;
	private $table_preference;

	public function __construct()
	{
		parent::__construct();
		$this->table_user		= 'm_user';
		$this->table_preference	= 'm_preference_job';
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_preference.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function preview($params = array())
	{
		$this->db->where('A.id', $params['id']);
		$this->db->where('A.is_active', 1);
		$this->db->from($this->table_preference.' A');
		$this->db->join($this->table_user.' B', 'A.created_by = B.id');
		$this->db->join($this->table_user.' C', 'A.updated_by = C.id');

		$this->db->select('A.*, B.full_name AS creator, C.full_name AS updater');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_preference.' A');
		
		$this->db->where('A.is_active', 1);

		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}
		
		if(isset($params['description'])){
			$this->db->like('A.description', $params['description']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.name', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_preference, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_preference, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
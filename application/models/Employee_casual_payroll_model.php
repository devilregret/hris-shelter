<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_casual_payroll_model extends CI_Model{
	public function __construct()
	{
		parent::__construct();
		$this->table_casual_payroll		= 't_employee_casual_payroll';
		$this->table_casual_employee	= 'm_employee_casual';
		$this->table_site				= 'm_site';
		$this->table_company			= 'm_company';
		$this->table_position			= 'm_position';
	}

	public function get($params = array())
	{
		
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', 1);
		}
		
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}
		
		if(isset($params['range_start'])){
			$this->db->where('A.periode_start >=', $params['range_start']);
		}
		
		if(isset($params['range_end'])){
			$this->db->where('A.periode_end <=', $params['range_end']);
		}

		if(isset($params['periode_start'])){
			$this->db->where('A.periode_start', $params['periode_start']);
		}
		
		if(isset($params['periode_end'])){
			$this->db->where('A.periode_end', $params['periode_end']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.id');
		}

		$this->db->from($this->table_casual_payroll.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_casual_payroll.' A');
		$this->db->join($this->table_casual_employee.' B', 'A.employee_id = B.id', 'left');
		$this->db->join($this->table_site.' C', 'B.site_id = C.id', 'left');
		$this->db->join($this->table_position.' D', 'B.position_id = D.id', 'left');

		if(isset($params['is_active'])){
			$this->db->where('A.is_active', $params['is_active']);
		}else{
			$this->db->where('A.is_active', 1);
		}

		if(isset($params['is_status'])){
			if($params['is_status'] != ''){
				$this->db->where('B.status', $params['is_status']);
			}
		}

		if(isset($params['date_start'])){
			if($params['date_start'] != ''){
				$this->db->where('A.periode_end >=', $params['date_start']);
			}
		}

		if(isset($params['date_finish'])){
			if($params['date_finish'] != ''){
				$this->db->where('A.periode_end <=', $params['date_finish']);
			}
		}

		if(isset($params['status_approval'])){
			if($params['status_approval'] != ''){
				$this->db->like('A.status', $params['status_approval']);
			}
		}

		if(isset($params['site_id'])){
			if($params['site_id'] != ''){
				$this->db->where('B.site_id', $params['site_id']);
			}
		}

		if(isset($params['status_employee'])){
			if($params['status_employee'] != ''){
				$this->db->like('B.status', $params['status_employee']);
			}
		}
		
		if(isset($params['employee_number'])){
			if($params['employee_number'] != ''){
				$this->db->like('B.employee_number', $params['employee_number']);
			}
		}

		if(isset($params['pin_finger'])){
			if($params['pin_finger'] != ''){
				$this->db->like('B.pin_finger', $params['pin_finger']);
			}
		}

		if(isset($params['full_name'])){
			if($params['full_name'] != ''){
				$this->db->like('B.full_name', $params['full_name']);
			}
		}

		if(isset($params['id_card'])){
			if($params['id_card'] != ''){
				$this->db->like('B.id_card', $params['id_card']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function report($params = array(), $return_count = FALSE){
		$this->db->from($this->table_casual_payroll.' A');
		$this->db->join($this->table_casual_employee.' B', 'A.employee_id = B.id', 'left');
		$this->db->join($this->table_site.' C', 'B.site_id = C.id', 'left');
		$this->db->join($this->table_company.' D', 'C.company_id = D.id', 'left');
		
		$this->db->where('A.is_active', 1);
		if(isset($params['site_id'])){
			if($params['site_id'] != ''){
				$this->db->where('C.id', $params['site_id']);
			}
		}
		if(isset($params['end_period'])){
			if($params['end_period'] != ''){
				$this->db->where('A.periode_end', $params['end_period']);
			}
		}
		if(isset($params['site_name'])){
			if($params['site_name'] != ''){
				$this->db->like('C.name', $params['site_name']);
			}
		}
		if(isset($params['company_code'])){
			if($params['company_code'] != ''){
				$this->db->like('D.code', $params['company_code']);
			}
		}
		if(isset($params['status'])){
			$this->db->where_in('A.status', $params['status']);
		}
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		$this->db->group_by('C.id, A.periode_end');

		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'period'){
				$params['orderby'] = 'A.periode_end';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.periode_end', 'ASC');
		}
		if ($return_count){
			return $this->db->count_all_results();
		}
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		$result = $this->db->get()->result();
		return $result;
	}

	public function resume($params = array()){
		if(isset($params['periode_end'])){
			$query 	= "SELECT COUNT(p.employee_id) AS employee_payroll, SUM(p.salary) AS summary_payroll, MAX(p.periode_start) AS periode_start, MAX(p.periode_end) AS periode_end FROM t_employee_casual_payroll p LEFT JOIN m_employee_casual e ON p.employee_id = e.id WHERE p.periode_end = '".$params['periode_end']."' AND p.is_active = 1 AND e.site_id = '".$params['site_id']."' AND p.status = 'Selesai' GROUP BY p.periode_end";
			$result = $this->db->query($query)->row();
		}else if(isset($params['site_id'])){
			$query 	= "SELECT COUNT(p.employee_id) AS employee_payroll, SUM(p.salary) AS summary_payroll, MAX(p.periode_start) AS periode_start, MAX(p.periode_end) AS periode_end FROM t_employee_casual_payroll p LEFT JOIN m_employee_casual e ON p.employee_id = e.id WHERE p.periode_end = (SELECT MAX(tp.periode_end) FROM t_employee_casual_payroll tp LEFT JOIN m_employee_casual me ON tp.employee_id = me.id WHERE tp.is_active = 1 AND me.site_id = '".$params['site_id']."' AND tp.payment = 'Selesai') AND p.is_active = 1 AND e.site_id = '".$params['site_id']."' AND p.payment = 'Selesai' GROUP BY p.periode_end";
			$result = $this->db->query($query)->row();
		}else{
			$result = $this->db->query("SELECT COUNT(employee_id) AS employee_payroll, SUM(salary) AS summary_payroll, MAX(periode_start) AS periode_start, MAX(periode_end) AS periode_end FROM t_employee_casual_payroll WHERE periode_end = (SELECT MAX(periode_end) FROM t_employee_casual_payroll WHERE is_active = 1) AND is_active = 1 GROUP BY periode_end")->row();
		}
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_casual_payroll, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_casual_payroll, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vacation_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->table_user		= 'm_user';
		$this->table_vacation	= 't_vacation';
		$this->table_employee	= 'm_employee';
		$this->table_site		= 'm_site';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_site.' B', 'A.site_id = B.id', 'left');
		$this->db->join($this->table_vacation.' C', 'A.id = C.employee_id', 'left');
		
		$this->db->where('A.is_active', 1);
		
		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('B.branch_id', $params['branch_id']);
			}
		}

		if(isset($params['vacation'])){
			if($params['vacation'] != ''){
				$this->db->like('C.vacation', $params['vacation']);
			}
		}
		if(isset($params['full_name'])){
			if($params['full_name'] != ''){
				$this->db->like('A.full_name', $params['full_name']);
			}
		}
	
		if(isset($params['employee_number'])){
			if($params['employee_number'] != ''){
				$this->db->like('A.employee_number', $params['employee_number']);
			}
		}

		if(isset($params['id_card'])){
			if($params['id_card'] != ''){
				$this->db->like('A.id_card', $params['id_card']);
			}
		}

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.full_name', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->group_by('A.id');
		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_vacation.' B', 'A.id = B.employee_id', 'left');
		$this->db->where('A.is_active', 1);
		
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		if(isset($params['employee_id'])){
			$this->db->where('B.employee_id', $params['employee_id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array()){
		
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->replace($this->table_vacation, $params);
		return $this->db->insert_id();
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_salary_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_site			= 'm_site';
		$this->table_salary 		= 't_site_salary';
		$this->table_salary_detail 	= 't_site_salary_detail';
	}
	
	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_site.' A');
		$this->db->join($this->table_salary.' B', 'A.id = B.site_id', 'left');
		$this->db->where('A.is_active', 1);
		$this->db->where('A.status_approval', 3);

		if(isset($params['site_id'])){
			$this->db->where('A.id', $params['site_id']);
		}

		if(isset($params['site_name'])){
			$this->db->like('A.name', $params['site_name']);
		}

		if(isset($params['basic_salary'])){
			if($params['basic_salary'] != ''){
				$this->db->like('B.basic_salary', $params['basic_salary']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['site_id'])){
			$this->db->where('A.id', $params['site_id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_site.' A');
		$this->db->join($this->table_salary.' B', 'A.id = B.site_id', 'left');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array()){
		
		$params['is_active']  = 1;
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->replace($this->table_salary, $params);
		return $this->db->insert_id();
	}

	public function gets_detail($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_salary_detail.' A');
		$this->db->join($this->table_site.' B', 'A.site_id = B.id', 'left');
		
		if(isset($params['site_id'])){
			$this->db->where('B.id', $params['site_id']);
		}


		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('B.name', 'ASC');
		}

		if(isset($params['groupby'])){
			$this->db->group_by($params['groupby']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function get_detail($params = array())
	{

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if(isset($params['name'])){
			$this->db->where('A.name', $params['name']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_salary_detail.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function delete_detail($params = array())
	{
		$id = false;
		if(isset($params['site_id'])){
			$id = $this->db->delete($this->table_salary_detail, array('site_id' => $params['site_id']));
		}
		return $id;
	}

	public function save_detail($params = array())
	{
		$this->db->insert($this->table_salary_detail, $params);
		$id = $this->db->insert_id();
		return $id;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roattendance_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->table_user			= 'm_user';
		$this->table_employee		= 'm_employee';
		$this->table_site			= 'm_site';
		$this->table_shift			= 't_employee_shift';
		$this->table_attendance		= 't_employee_attendance';
		$this->table_workday		= 'm_work_day';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->where('A.is_active', '1');

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if(isset($params['pin_finger'])){
			$this->db->like('A.pin_finger', $params['pin_finger']);
		}

		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}

		if(isset($params['employee_number'])){
			$this->db->like('A.employee_number', $params['employee_number']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.full_name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function employee_gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_attendance.' A');
		$this->db->where('A.is_active', '1');

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['date'])){
			$this->db->like('A.date', $params['date']);
		}

		if(isset($params['hours_start'])){
			$this->db->like('A.hours_start', $params['hours_start']);
		}

		if(isset($params['hours_end'])){
			$this->db->like('A.hours_end', $params['hours_end']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.date', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function get_schedule($params = array())
	{
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', $params['is_active']);
		
		}else{
			$this->db->where('A.is_active', 1);
		}

		if(isset($params['pin_finger'])){
			$this->db->where('A.pin_finger', $params['pin_finger']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_shift.' B', 'A.id = B.employee_id ', 'left');
		$this->db->join($this->table_workday.' C', 'B.shift = C.shift  AND C.site_id ='.$params['site_id'].' AND C.day = "'.$params['day'].'"', 'left');

		$result = $this->db->get()->row();
		return $result;
	}

	public function get($params = array())
	{
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', $params['is_active']);
		
		}else{
			$this->db->where('A.is_active', 1);
		}

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['date'])){
			$this->db->where('A.date', $params['date']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_attendance.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function update($params = array()){

		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->where('employee_id', $params['employee_id']);
		$this->db->where('date', $params['date']);
		$this->db->update($this->table_attendance, $params);
		
		return true;
	}

	public function insert($params = array())
	{
		$params['is_active']  = 1;
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_attendance, $params);
		
		return true;
	}
}
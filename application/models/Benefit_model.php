<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Benefit_model extends CI_Model{
	private $table_employee;
	public function __construct()
	{
		parent::__construct();
		$this->table_employee	= 'm_employee';
	}

	public function count($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->where('A.is_active', 1);
		if(isset($params['status'])){
			$this->db->where('A.status', $params['status']);
		}
		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if(isset($params['not_site_id'])){
			$this->db->where('A.site_id != ', $params['not_site_id']);
		}

		if(isset($params['status_approval'])){
			$this->db->where('A.status_approval', $params['status_approval']);
		}

		if(isset($params['benefit_labor'])){
			$this->db->where('A.benefit_labor', $params['benefit_labor']);
		}

		if(isset($params['benefit_health'])){
			$this->db->where('A.benefit_health', $params['benefit_health']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		$result = $this->db->get()->result();
		return $result;
	}
}
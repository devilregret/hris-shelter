<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City_model extends CI_Model{
	private $table_user;
	private $table_city;
	private $table_province;

	public function __construct()
	{
		parent::__construct();
		$this->table_user       = 'm_user';
		$this->table_city       = 'm_city';
		$this->table_province   = 'm_province';
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		if(isset($params['name'])){
			$this->db->where('A.name', $params['name']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_city.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function get_detail($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		if(isset($params['name'])){
			$this->db->where('A.name', $params['name']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_city.' A');
		$this->db->join($this->table_province.' B', 'A.province_id = B.id');

		$result = $this->db->get()->row();
		return $result;
	}

	public function preview($params = array())
	{
		$this->db->where('A.id', $params['id']);
		$this->db->where('A.is_active', 1);
		$this->db->from($this->table_city.' A');
		$this->db->join($this->table_province.' B', 'A.province_id = B.id');
		$this->db->join($this->table_user.' C', 'A.created_by = C.id');
		$this->db->join($this->table_user.' D', 'A.updated_by = D.id');

		$this->db->select('A.*, B.name AS province_name, C.full_name AS creator, D.full_name AS updater');

		$result = $this->db->get()->row();
		return $result;
	}

	public function list($params = array())
	{
		$this->db->where('A.is_active', 1);
		$this->db->from($this->table_city.' A');
		$this->db->join($this->table_province.' B', 'A.province_id = B.id');

		$this->db->select('A.id, A.name, B.name AS province_name');
		$result = $this->db->get()->result();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_city.' A');
		$this->db->join($this->table_province.' B', 'A.province_id = B.id');
		
		$this->db->where('A.is_active', 1);

		if(isset($params['branch_id'])){
			$this->db->like('B.branch_id', $params['branch_id']);
		}

		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}

		if(isset($params['minimum_salary'])){
			$this->db->like('A.minimum_salary', $params['minimum_salary']);
		}
		
		if(isset($params['province_name'])){
			$this->db->like('B.name', $params['province_name']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.name', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*, B.name AS province_name');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_city, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_city, $params);
			$id = $this->db->insert_id();
		}
		
		return $id;
	}
}
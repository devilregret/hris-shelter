<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cooperation_history_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_cooperation 			= 'h_cooperation';
		$this->table_cooperation_detail		= 'h_cooperation_detail';
		$this->table_cooperation_component	= 'h_cooperation_detail_component';
		$this->table_cooperation_approval	= 'h_cooperation_approval';
		$this->table_selling_price			= 'm_selling_price';
		$this->table_site					= 'm_site';
		$this->table_position				= 'm_position';
		$this->table_user       			= 'm_user';
		$this->table_company   				= 'm_company';
		$this->table_branch   				= 'm_branch';
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_cooperation.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_cooperation.' A');
		$this->db->join($this->table_company.' B', 'A.company_id = B.id', 'left');
		$this->db->join($this->table_branch.' C', 'A.branch_id = C.id', 'left');
		$this->db->join($this->table_site.' D', 'A.site_id = D.id', 'left');
		$this->db->join($this->table_user.' E', 'A.created_by = E.id', 'left');
		$this->db->where('A.is_active', 1);

		
		if(isset($params['client_name'])){
			$this->db->like('A.client_name', $params['client_name']);
		}
		
		if(isset($params['client_address'])){
			$this->db->like('A.client_address', $params['client_address']);
		}
		
		if(isset($params['status'])){
			$this->db->like('A.status', $params['status']);
		}
		
		if(isset($params['sales_id'])){
			$this->db->where('A.created_by', $params['sales_id']);
		}

		if(isset($params['company_name'])){
			if($params['company_name'] != ''){
				$this->db->like('B.name', $params['company_name']);
			}
		}

		if(isset($params['branch_name'])){
			if($params['branch_name'] != ''){
				$this->db->like('C.name', $params['branch_name']);
			}
		}

		if(isset($params['site_name'])){
			if($params['site_name'] != ''){
				$this->db->like('D.name', $params['site_name']);
			}
		}

		if(isset($params['sales_name'])){
			if($params['sales_name'] != ''){
				$this->db->like('E.full_name', $params['sales_name']);
			}
		}
				
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		// if ($params['id'] != '') {
		// 	$params['updated_at'] = date('Y-m-d H:i:s');
		// 	$params['updated_by'] = $this->session->userdata('user_id');
		// 	$this->db->where('id', $params['id']);
		// 	$this->db->update($this->table_cooperation, $params);
		// 	$id = $params['id'];
		// } else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_cooperation, $params);
			$id = $this->db->insert_id();
		// }
		
		return $id;
	}

	public function gets_detail($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_position.' A');
		$this->db->join($this->table_cooperation_detail.' B', 'A.id = B.position_id AND B.cooperation_id = '.$params['cooperation_id'], 'left');
		$this->db->where('A.is_active', 1);
		$this->db->where_in('A.id', $params['list_position']);
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function get_detail($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['cooperation_id'])){
			$this->db->where('A.cooperation_id', $params['cooperation_id']);
		}

		if(isset($params['position_id'])){
			$this->db->where('A.position_id', $params['position_id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_cooperation_detail.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save_detail($params = array())
	{
		$id = false;
		// if ($params['id'] != '') {
		// 	$params['updated_at'] = date('Y-m-d H:i:s');
		// 	$params['updated_by'] = $this->session->userdata('user_id');
		// 	$this->db->where('id', $params['id']);
		// 	$this->db->update($this->table_cooperation_detail, $params);
		// 	$id = $params['id'];
		// } else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_cooperation_detail, $params);
			$id = $this->db->insert_id();
		// }
		
		return $id;
	}

	public function gets_component($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_cooperation_component.' A');
		$this->db->where('A.is_active', 1);
		$this->db->where('A.cooperation_id', $params['cooperation_id']);

		if(isset($params['position_id'])){
			$this->db->where('A.position_id', $params['position_id']);
		}
		if(isset($params['selling_price_id'])){
			$this->db->where('A.selling_price_id', $params['selling_price_id']);
		}
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		if(isset($params['groupby'])){
			$this->db->group_by($params['groupby']);
		}
		
		$result = $this->db->get()->result();
		return $result;
	}



	public function get_component($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_cooperation_component.' A');
		$this->db->where('A.is_active', 1);
		$this->db->where('A.cooperation_id', $params['cooperation_id']);

		if(isset($params['position_id'])){
			$this->db->where('A.position_id', $params['position_id']);
		}
		if(isset($params['selling_price_id'])){
			$this->db->where('A.selling_price_id', $params['selling_price_id']);
		}
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		if(isset($params['groupby'])){
			$this->db->group_by($params['groupby']);
		}
		
		$result = $this->db->get()->row();
		return $result;
	}

	public function save_component($params = array())
	{
		$id = false;
		// if ($params['id'] != '') {
		// 	$params['updated_at'] = date('Y-m-d H:i:s');
		// 	$params['updated_by'] = $this->session->userdata('user_id');
		// 	$this->db->where('id', $params['id']);
		// 	$this->db->update($this->table_cooperation_component, $params);
		// 	$id = $params['id'];
		// } else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_cooperation_component, $params);
			$id = $this->db->insert_id();
		// }
		return $id;
	}

	public function gets_selling_price($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_cooperation_component.' A');
		$this->db->join($this->table_selling_price.' B', 'A.selling_price_id = B.id', 'left');
		$this->db->where('A.is_active', 1);
		$this->db->where('A.cooperation_id', $params['cooperation_id']);
		if(isset($params['list_position'])){
			$this->db->where_in('A.position_id', $params['list_position']);
		}
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		if(isset($params['groupby'])){
			$this->db->group_by($params['groupby']);
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function save_approval($params = array())
	{
		$id = false;
		$params['is_active']  = 1;
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_cooperation_approval, $params);
		$id = $this->db->insert_id();
		return $id;
	}
}
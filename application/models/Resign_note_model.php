<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resign_note_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_resign_note   	= 'm_resign_note';
		$this->table_user			= 'm_user';
	}

	public function get($params = array()){
		$this->db->from($this->table_resign_note.' A');

		$this->db->where('A.is_active', 1);
		
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		$this->db->limit(1);
		$this->db->order_by('A.id', 'DESC');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_resign_note.' A');

		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			if($params['id'] != ''){
				$this->db->where('A.id', $params['id']);
			}
		}
		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}
		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.updated_at', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('*');
		}
		$result = $this->db->get()->result();
		return $result;
	}
	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_resign_note, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_resign_note, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
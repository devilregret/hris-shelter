<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History_tes_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_history_tes	= 't_history_tes';
		$this->table_vacancy 		= 'm_vacancy';
		$this->table_province   	= 'm_province';
		$this->table_branch   		= 'm_branch';
		$this->table_company   		= 'm_company';
		$this->table_site   		= 'm_site';
		$this->table_position   	= 'm_position';
		$this->table_applicant		= 't_applicant';
		$this->table_employee		= 'm_employee';
		$this->table_tes			= 'm_tes';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_history_tes.' A');
		$this->db->join($this->table_vacancy.' B', 'A.m_vacancy_id = B.id', 'left');
		$this->db->join($this->table_province.' C', 'B.province_id = C.id', 'left');
		$this->db->join($this->table_branch.' D', 'C.branch_id = D.id', 'left');
		$this->db->join($this->table_company.' E', 'B.company_id = E.id', 'left');
		$this->db->join($this->table_site.' F', 'B.site_id = F.id', 'left');
		$this->db->join($this->table_position.' G', 'B.position_id = G.id', 'left');
		$this->db->join($this->table_tes.' H', 'H.tes = A.tes', 'left');

		$this->db->where('A.deleted_at IS NULL');

		if(isset($params['session_branch'])){
			$this->db->where('D.branch_id', $params['session_branch']);
		}

		if(isset($params['province_id'])){
			$this->db->where('C.province_id', $params['province_id']);
		}

		if(isset($params['province_name'])){
			if($params['province_name'] != ''){
				$this->db->like('C.name', $params['province_name']);
			}
		}
		
		if(isset($params['branch_name'])){
			if($params['branch_name'] != ''){
				$this->db->like('D.name', $params['branch_name']);
			}
		}
		
		if(isset($params['company_name'])){
			if($params['company_name'] != ''){
				$this->db->like('E.name', $params['company_name']);
			}
		}

		if(isset($params['site_name'])){
			if($params['site_name'] != ''){
				$this->db->like('F.name', $params['site_name']);
			}
		}

		if(isset($params['position_name'])){
			if($params['position_name'] != ''){
				$this->db->like('G.name', $params['position_name']);
			}
		}
		if(isset($params['kategori'])){
			if($params['kategori'] != ''){
				$this->db->like('A.kategori', $params['kategori']);
			}
		}
		if(isset($params['title'])){
			if($params['title'] != ''){
				$this->db->like('B.title', $params['title']);
			}
		}
		if(isset($params['tgl_tes'])){
			$this->db->like("DATE_FORMAT(A.tgl_tes, '%d/%m/%Y')", $params['tgl_tes']);
		}

		if(isset($params['nama'])){
			if($params['nama'] != ''){
				$this->db->like('A.nama', $params['nama']);
			}
		}

		if(isset($params['stes'])){
			if($params['stes'] != ''){
				$this->db->like('H.keterangan', $params['stes']);
			}
		}
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'id'){
				$params['orderby'] = 'A.id';
			}
			// if($params['orderby'] == 'end_date'){
			// 	$params['orderby'] = 'A.end_date';
			// }
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id ', 'DESC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}
	public function get_list_data_combo_province (){
		$q = "SELECT distinct province_id as id,(SELECT `name` from m_province WHERE id = province_id) as `name` from m_vacancy WHERE is_active = 1 and id IN ( SELECT m_vacancy_id from t_history_tes WHERE deleted_at is null );";
		return $this->db->query($q)->result();
	}
	public function get_list_data_combo_company (){
		$q = "SELECT distinct company_id as id,(SELECT `name` from m_company WHERE id = company_id) as `name` from m_vacancy WHERE is_active = 1 and id IN ( SELECT m_vacancy_id from t_history_tes WHERE deleted_at is null );";
		return $this->db->query($q)->result();
	}
	public function get_list_data_combo_site (){
		$q = "SELECT distinct site_id as id,(SELECT `name` from m_site WHERE id = site_id) as `name` from m_vacancy WHERE is_active = 1 and id IN ( SELECT m_vacancy_id from t_history_tes WHERE deleted_at is null );";
		return $this->db->query($q)->result();
	}
	public function get_list_data_combo_position (){
		$q = "SELECT distinct position_id as id,(SELECT `name` from m_position WHERE id = position_id) as `name` from m_vacancy WHERE is_active = 1 and id IN ( SELECT m_vacancy_id from t_history_tes WHERE deleted_at is null );";
		return $this->db->query($q)->result();
	}
	public function get_list_data_combo_lowongan (){
		$q = "SELECT distinct id,title as `name` from m_vacancy WHERE is_active = 1 and id IN ( SELECT m_vacancy_id from t_history_tes WHERE deleted_at is null )";
		return $this->db->query($q)->result();
	}
	public function get_list_data_combo_employee (){
		$q = "SELECT DISTINCT m_employee_id as id,nama as `name` from t_history_tes WHERE deleted_at is null;";
		return $this->db->query($q)->result();
	}
	public function getListHistoryTes($tes,$company_id,$site_id,$position_id,$vacancy_id,$employee_id,$order,$id,$doc_id) {
		$qtes = "";
		$qorder = "";
		$qwhere = "";

		if($tes!=null){
			$arrTes = explode(",",$tes);
			foreach ($arrTes as $key => $value) {
				if($key==0){
					$qtes = "'".$value."'";
				}else{
					$qtes =$qtes.",'".$value."'";
				}
			}
	
			$qwhere = " and tes IN (".$qtes.")";
		}

		if($company_id!='all' && $company_id!=null){
			$qwhere .= " and company_id = ".$company_id;
		}
		if($site_id!='all' && $site_id!=null){
			$qwhere .= " and site_id = ".$site_id;
		}
		if($position_id!='all' && $position_id!=null){
			$qwhere .= " and m_vacancy_id IN (SELECT id from m_vacancy WHERE position_id = ".$position_id." and is_active = 1) ";
		}
		if($vacancy_id!='all' && $vacancy_id!=null){
			$qwhere .= " and m_vacancy_id = ".$vacancy_id;
		}
		if($employee_id!='all' && $employee_id!=null){
			$qwhere .= " and m_employee_id = ".$employee_id;
		}
		if($id !=null && $id !="") {
			$qwhere .= " and id = ".$id;
		}

		if($doc_id !=null && $doc_id !="") {
			$qwhere .= " and doc_id = ".$doc_id;
		}

		if($order !=null){
			if($order=='peserta'){
				$qorder = " ORDER BY m_employee_id asc,tes asc,m_vacancy_id asc";
			}else if($order=='tes'){
				$qorder = " ORDER BY tes asc,m_employee_id asc,m_vacancy_id asc";
			}
		}

		$q = "select * from t_history_tes where deleted_at is null".$qwhere.$qorder;

		return $this->db->query($q)->result();
	}

	public function get_export_tes ( $params ){
		$tes = $params['tes'];
		$company_id = $params['company_id'];
		$site_id = $params['site_id'];
		$position_id = $params['position_id'];
		$province_id = $params['province_id'];
		$vacancy_id = $params['vacancy_id'];
		$employee_id = $params['employee_id'];
		$order = $params['order'];

		//where query
		$qtes = "";
		$arrTes = explode(",",$tes);
		foreach ($arrTes as $key => $value) {
			if($key==0){
				$qtes = "'".$value."'";
			}else{
				$qtes =$qtes.",'".$value."'";
			}
		}

		$qwhere = " and m.tes IN (".$qtes.")";
		$qorder = "";

		if($company_id!='all'){
			$qwhere .= " and m.company_id = ".$company_id;
		}
		if($site_id!='all'){
			$qwhere .= " and m.site_id = ".$site_id;
		}
		if($position_id!='all'){
			$qwhere .= " and m.m_vacancy_id IN (SELECT id from m_vacancy WHERE position_id = ".$position_id." and is_active = 1) ";
		}
		if($vacancy_id!='all'){
			$qwhere .= " and m.m_vacancy_id = ".$vacancy_id;
		}
		if($employee_id!='all'){
			$qwhere .= " and m.m_employee_id = ".$employee_id;
		}

		if($order=='peserta'){
			$qorder = " ORDER BY m.m_employee_id asc,m.tes asc,m.m_vacancy_id asc";
		}else if($order=='tes'){
			$qorder = " ORDER BY m.tes asc,m.m_employee_id asc,m.m_vacancy_id asc";
		}



		$q = "SELECT m.id,(SELECT keterangan from m_tes where tes = m.tes ) as tes,(SELECT `name` from m_company WHERE id = m.company_id) as company_name,(SELECT `name` from m_branch WHERE id = m.branch_id) as branch_name,(SELECT `name` from m_site WHERE id = m.site_id) as site_name,(SELECT `name` from m_province WHERE id = m.province_id) as province_name,(SELECT `name` from m_position WHERE id = m.position_id) as position_name,m.nama,m.usia,m.jk,m.tgl_tes,m.tes_dimulai,m.tes_selesai,m.tidak_diisi,m.jawaban_benar,m.jawaban_salah,m.persentase,m.persentase_minimal,m.petugas,m.kategori,v.title
		from t_history_tes m
		LEFT JOIN m_vacancy v ON v.id = m.m_vacancy_id
		WHERE m.deleted_at is null ".$qwhere.$qorder;
		return $this->db->query($q)->result();

	}

	public function save($params = array()){
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_history_tes, $params);
		$id = $this->db->insert_id();
		return $id;
	}

}
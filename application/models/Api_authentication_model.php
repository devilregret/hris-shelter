<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_authentication_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_api_authentication = 'm_api_authentication';
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['expired'])){
			$this->db->where('NOW() - INTERVAL 1 DAY < A.updated_at');
		}

		if(isset($params['client_key'])){
			$this->db->where('A.client_key', $params['client_key']);
		}
		if(isset($params['secret_key'])){
			$this->db->where('A.secret_key', $params['secret_key']);
		}
		if(isset($params['token'])){
			$this->db->where('A.token', $params['token']);
		}
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_api_authentication.' A');
		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if (isset($params['id'])) {
			$params['is_active']  = 1;
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_api_authentication, $params);
			$id = $params['id'];
		}
		return $id;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Benefit_labor_model extends CI_Model{	
	public function __construct()
	{
		parent::__construct();
		$this->table_employee		= 'm_employee';
		$this->table_site			= 'm_site';
		$this->table_company 		= 'm_company';
		$this->table_benefit_labor	= 't_benefit_labor';
	}

	public function last_benefit($params = array())
	{
		$this->db->where('A.is_active', '1');
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		if(isset($params['periode'])){
			$this->db->where('A.periode', $params['periode']);
		}

		$this->db->from($this->table_benefit_labor.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id');
		$result = $this->db->get()->row();
		return $result;
	}
	public function get_summary($params){
		$this->db->from($this->table_benefit_labor.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id');
		$this->db->join($this->table_site.' C', 'B.site_id = C.id');
		$this->db->where('A.is_active', '1');

		if(isset($params['periode'])){
			$this->db->where('A.periode', $params['periode']);
		}
		if(isset($params['site_id'])){
			if($params['site_id'] != ''){
				$this->db->where('B.site_id', $params['site_id']);
			}
		}
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		if(isset($params['groupby'])){
			$this->db->group_by($params['groupby']);
		}
		$result = $this->db->get()->row();
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_benefit_labor.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		$this->db->join($this->table_site.' C', 'B.site_id = C.id', 'left');
		
		$this->db->where('A.is_active', '1');
		
		if(isset($params['preview'])){
			$this->db->where('A.periode', $params['preview']);
		}

		if(isset($params['site_id'])){
			if($params['site_id'] != ''){
				$this->db->where('B.site_id', $params['site_id']);
			}
		}

		if(isset($params['is_labor_company'])){
			if($params['is_labor_company'] != ''){
				$this->db->where('A.benefit_labor_company', $params['is_labor_company']);
			}
		}

		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('C.branch_id', $params['branch_id']);
			}
		}

		if(isset($params['id_card'])){
			$this->db->like('B.id_card', $params['id_card']);
		}

		if(isset($params['full_name'])){
			$this->db->like('B.full_name', $params['full_name']);
		}

		if(isset($params['card_number'])){
			$this->db->like('A.card_number', $params['card_number']);
		}

		if(isset($params['program'])){
			$this->db->like('A.program', $params['program']);
		}

		if(isset($params['periode'])){
			$this->db->like('DATE_FORMAT(A.periode, "%Y-%m")', $params['periode']);
		}

		if(isset($params['employee_payment'])){
			$this->db->like('A.employee_payment', $params['employee_payment']);
		}

		if(isset($params['company_payment'])){
			$this->db->like('A.company_payment', $params['company_payment']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'periode'){
				$params['orderby'] = 'A.periode';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('B.full_name', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		if(isset($params['groupby'])){
			$this->db->group_by($params['groupby']);
		}
		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);
		
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['periode'])){
			$this->db->where('A.periode', $params['periode']);
		}

		if(isset($params['program'])){
			$this->db->where('A.program', $params['program']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_benefit_labor.' A');
		
		
		if(isset($params['groupby'])){
			$this->db->group_by($params['groupby']);
		}
		$result = $this->db->get()->row();
		return $result;
	}

	public function delete_detail($params = array()){
		$params['is_active']  = 0;
		if ($params['periode'] != '') {
			$this->db->where('periode', $params['periode']);
		}
		$this->db->update($this->table_benefit_labor, $params);
		$id = $params['id'];
	}
	
	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_benefit_labor, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_benefit_labor, $params);
			$id = $this->db->insert_id();
		}
		
		return $id;
	}

	public function update($params = array())
	{
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->where('employee_id', $params['employee_id']);
		$this->db->where('periode', $params['periode']);
		$this->db->update($this->table_benefit_labor, $params);
		return true;
	}
}
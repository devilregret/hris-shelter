<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_model extends CI_Model{
	public function __construct()
	{
		parent::__construct();
		$this->table_user		= 'm_user';
		$this->table_city		= 'm_city';
		$this->table_employee	= 'm_employee';
		$this->table_site		= 'm_site';
		$this->table_branch		= 'm_branch';
		$this->table_position	= 'm_position';
		$this->table_preference	= 'm_preference_job';
		$this->table_skill		= 'm_employee_skill';
		$this->table_company 	= 'm_company';
		$this->table_contract 	= 't_employee_contract';
		$this->table_tax 		= 'm_setting_tax';
	}

	public function getByStruktur($params = array())
	{
		if(isset($params['struktur_id'])){
			$this->db->where('A.struktur_id', $params['struktur_id']);    
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_employee.' A');

		$result = $this->db->get()->result();
		return $result;
	}
	
	public function get($params = array())
	{
		
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', 1);
		}
		
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['registration_number'])){
			$this->db->where('A.registration_number', $params['registration_number']);
		}

		if(isset($params['benefit_labor'])){
			$this->db->where('A.benefit_labor', $params['benefit_labor']);
		}
		
		if(isset($params['benefit_health'])){
			$this->db->where('A.benefit_health', $params['benefit_health']);
		}

		if(isset($params['employee_number'])){
			$this->db->where('A.employee_number', $params['employee_number']);
		}

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if(isset($params['position_id'])){
			$this->db->where('A.position_id', $params['position_id']);
		}
		
		if(isset($params['id_card'])){
			$this->db->where('A.id_card', $params['id_card']);    
		}
		
		if(isset($params['pin_finger'])){
			$this->db->where('A.pin_finger', $params['pin_finger']);    
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_site.' B', 'A.site_id = B.id', 'left');
		$this->db->join($this->table_company.' C', 'A.company_id = C.id', 'left');

		$result = $this->db->get()->row();
		return $result;
	}

	public function get_detail($params = array())
	{
		
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', 1);
		}
		
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_site.' B', 'A.site_id = B.id', 'left');
		$this->db->join($this->table_company.' C', 'A.company_id = C.id', 'left');
		$this->db->join($this->table_position.' D', 'A.position_id = D.id', 'left');
		$this->db->join($this->table_city.' E', 'A.city_birth_id = E.id', 'left');
		// $this->db->join($this->table_city.' H', 'A.city_card_id = H.id', 'left');
		// $this->db->join($this->table_city.' I', 'A.city_domisili_id = I.id', 'left');

		$result = $this->db->get()->row();
		return $result;
	}

	public function count($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_site.' B', 'A.site_id = B.id', 'left');
		$this->db->join($this->table_position.' C', 'A.position_id = C.id', 'left');
		$this->db->where('A.is_active', 1);
		
		if(isset($params['status'])){
			$this->db->where('A.status', $params['status']);
		}
		if(isset($params['status_nonjob'])){
			$this->db->where('A.status_nonjob', $params['status_nonjob']);
		}
		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}
		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('B.branch_id', $params['branch_id']);
			}
		}
		if(isset($params['employee_branch'])){
			if($params['employee_branch'] != ''){
				$this->db->where('A.branch_id', $params['employee_branch']);
			}
		}
		if(isset($params['company_id'])){
			$this->db->where('A.company_id', $params['company_id']);
		}

		if(isset($params['bussiness_company_id'])){
			$this->db->where('B.company_id', $params['bussiness_company_id']);
		}

		if(isset($params['not_site_id'])){
			$this->db->where('A.site_id != ', $params['not_site_id']);
		}

		if(isset($params['status_approval'])){
			$this->db->where('A.status_approval', $params['status_approval']);
		}

		if(isset($params['position_type'])){
			if($params['position_type'] != ''){
				$this->db->where('C.description', $params['position_type']);
			}
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}

		if(isset($params['groupby'])){
			$this->db->group_by($params['groupby']);
		}

		if ($return_count){
			$result = $this->db->count_all_results();
			return $result;
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function gets_benefit_company($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_site.' B', 'A.site_id = B.id', 'left');
		$this->db->where('A.is_active', 1);
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('B.branch_id', $params['branch_id']);
			}
		}
		if(isset($params['groupby'])){
			$this->db->group_by($params['groupby']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.full_name', 'ASC');
		}

		$result = $this->db->get()->result();
		return $result;

	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_site.' B', 'A.site_id = B.id', 'left');
		$this->db->join($this->table_company.' C', 'A.company_id = C.id', 'left');
		$this->db->join($this->table_contract.' D', 'A.id = D.employee_id AND D.id = (SELECT MAX(id) FROM t_employee_contract WHERE employee_id = A.id AND is_active = 1 ORDER BY contract_start DESC LIMIT 1)', 'left');
		$this->db->join($this->table_position.' E', 'A.position_id = E.id', 'left');
		$this->db->join($this->table_branch.' F', 'B.branch_id = F.id', 'left');
		$this->db->join($this->table_city.' G', 'A.city_birth_id = G.id', 'left');
		$this->db->join($this->table_city.' H', 'A.city_card_id = H.id', 'left');
		$this->db->join($this->table_city.' I', 'A.city_domisili_id = I.id', 'left');
		$this->db->join($this->table_company.' J', 'B.company_id = J.id', 'left');
		$this->db->join($this->table_tax.' K', 'A.ptkp = K.id', 'left');

		$this->db->where('A.is_active', 1);
		if(isset($params['assurance_active'])){
			$this->db->where("(A.status_approval > 3 OR A.status_approval = 0) AND (A.benefit_health <> '' OR A.benefit_labor <> '')");
		}
		
		if(isset($params['assurance_non_active'])){
			$this->db->where("(A.status_approval = 3 AND (A.benefit_health = '' OR A.benefit_labor = ''))");
		}

		if(isset($params['employee_id'])){
			$this->db->where('A.id', $params['employee_id']);
		}

		if(isset($params['position_id'])){
			$this->db->where('A.position_id', $params['position_id']);
		}

		if(isset($params['status'])){
			$this->db->where('A.status', $params['status']);
		}
		
		if(isset($params['expired'])){
			if($params['expired'] <= 0){
				$this->db->where('D.contract_end < NOW()');	
			}else if($params['expired'] > 0){
				$this->db->where("D.contract_end = DATE_FORMAT(NOW() + INTERVAL ".$params['expired']." DAY, '%Y-%m-%d')") ;	
			}
		}

		if(isset($params['status_nonjob'])){
			$this->db->where('A.status_nonjob', $params['status_nonjob']);
		}

		if(isset($params['site_id'])){
			if($params['site_id'] != ''){
				$this->db->where('A.site_id', $params['site_id']);
			}
		}

		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('B.branch_id', $params['branch_id']);
			}
		}

		if(isset($params['is_labor_company'])){
			if($params['is_labor_company'] != ''){
				$this->db->where('A.benefit_labor_company', $params['is_labor_company']);
			}
		}

		if(isset($params['is_health_company'])){
			if($params['is_health_company'] != ''){
				$this->db->where('A.benefit_health_company', $params['is_health_company']);
			}
		}

		if(isset($params['not_site_id'])){
			$this->db->where('A.site_id != ', $params['not_site_id']);
		}

		if(isset($params['status_approval'])){
			$this->db->where('A.status_approval', $params['status_approval']);
		}

		if(isset($params['status_benefit'])){
			$this->db->where('A.status_benefit', $params['status_benefit']);
		}

		if(isset($params['benefit_complete'])){
			$this->db->where("A.benefit_labor != ''");
			$this->db->where("A.benefit_health != ''");
		}

		if(isset($params['benefit_not_complete'])){
			$this->db->where("A.benefit_labor_company = ''");
			$this->db->where("A.benefit_labor = ''");
			$this->db->where("A.benefit_health_company = ''");
			$this->db->where("A.benefit_health = ''");
		}

		if(isset($params['benefit_labor_company'])){
			$this->db->like('A.benefit_labor_company', $params['benefit_labor_company']);
		}

		if(isset($params['benefit_health_company'])){
			$this->db->like('A.benefit_health_company', $params['benefit_health_company']);
		}

		if(isset($params['id_card'])){
			$this->db->like('A.id_card', $params['id_card']);
		}

		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}

		if(isset($params['email'])){
			$this->db->like('A.email', $params['email']);
		}

		if(isset($params['employee_number'])){
			$this->db->like('A.employee_number', $params['employee_number']);
		}

		if(isset($params['phone_number'])){
			$this->db->like('A.phone_number', $params['phone_number']);
		}

		if(isset($params['health_insurance_note'])){
			$this->db->like('A.health_insurance_note', $params['health_insurance_note']);
		}

		if(isset($params['health_insurance'])){
			$this->db->like('A.health_insurance', $params['health_insurance']);
		}

		if(isset($params['benefit_labor_note'])){
			$this->db->like('A.benefit_labor_note', $params['benefit_labor_note']);
		}

		if(isset($params['benefit_labor'])){
			$this->db->like('A.benefit_labor', $params['benefit_labor']);
		}

		if(isset($params['benefit_health_note'])){
			$this->db->like('A.benefit_health_note', $params['benefit_health_note']);
		}

		if(isset($params['benefit_health'])){
			$this->db->like('A.benefit_health', $params['benefit_health']);
		}


		if(isset($params['pin_finger'])){
			if($params['pin_finger'] != ''){
				$this->db->like('A.pin_finger', $params['pin_finger']);
			}
		}

		if(isset($params['ptkp'])){
		    if($params['ptkp'] != ''){
				$this->db->like('K.code', $params['ptkp']);
			}
		}

		if(isset($params['bank_name'])){
			$this->db->like('A.bank_name', $params['bank_name']);
		}

		if(isset($params['bank_account'])){
			$this->db->like('A.bank_account', $params['bank_account']);
		}

		if(isset($params['bank_account_name'])){
			$this->db->like('A.bank_account_name', $params['bank_account_name']);
		}

		if(isset($params['company_name'])){
		    if($params['company_name'] != ''){
			    $this->db->like('C.name', $params['company_name']);
		    }
		}

		if(isset($params['company_code'])){
		    if($params['company_code'] != ''){
			    $this->db->where('C.code', $params['company_code']);
		    }
		}

		if(isset($params['site_name'])){
		    if($params['site_name'] != ''){
			    $this->db->like('B.name', $params['site_name']);
		    }
		}

		if(isset($params['branch_name'])){
		    if($params['branch_name'] != ''){
			    $this->db->like('F.name', $params['branch_name']);
		    }
		}

		if(isset($params['business_code'])){
		    if($params['business_code'] != ''){
			    $this->db->where('J.code', $params['business_code']);
		    }
		}

		if(isset($params['business_code'])){
		    if($params['business_code'] != ''){
			    $this->db->where('J.code', $params['business_code']);
		    }
		}

		if(isset($params['contract_type'])){
			if($params['contract_type'] != ''){
				$this->db->like("DATE_FORMAT(D.contract_end, '%d/%m/%Y')", $params['contract_type']);
			}
		}

		if(isset($params['position'])){
			if($params['position'] != ''){
				$this->db->like('E.name', $params['position']);
			}
		}

		if(isset($params['position_type'])){
			if($params['position_type'] != ''){
				$this->db->where('E.description', $params['position_type']);
			}
		}

		if(isset($params['contract_end'])){
			if($params['contract_end'] != ''){
				$this->db->like('D.contract_end', $params['contract_end']);
			}
		}

		if(isset($params['approved_at'])){
			if($params['approved_at'] != ''){
				$this->db->like("DATE_FORMAT(A.approved_at, '%d/%m/%Y')", $params['approved_at']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'contract_end'){
				$params['orderby'] = 'D.contract_end';
			}
			if($params['orderby'] == 'contract_start'){
				$params['orderby'] = 'D.contract_end';
			}
			if($params['orderby'] == 'approved_at'){
				$params['orderby'] = 'A.approved_at';
			}

			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.full_name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function employee_gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_company.' B', 'A.company_id = B.id', 'left');
		$this->db->join($this->table_branch.' C', 'A.branch_id = C.id', 'left');
		$this->db->join($this->table_position.' D', 'A.position_id = D.id', 'left');
		$this->db->join($this->table_site.' E', 'A.site_id = E.id', 'left');
		$this->db->join($this->table_company.' F', 'E.company_id = F.id', 'left');
		$this->db->join($this->table_branch.' G', 'E.branch_id = G.id', 'left');
		$this->db->join($this->table_city.' H', 'A.city_birth_id = H.id', 'left');
		$this->db->join($this->table_city.' I', 'A.city_domisili_id = I.id', 'left');

		$this->db->where('A.is_active', 1);

		if(isset($params['status'])){
			$this->db->where('A.status', $params['status']);
		}
		
		if(isset($params['status_approval'])){
			$this->db->where('A.status_approval', $params['status_approval']);
		}

		if(isset($params['status_nonjob'])){
			$this->db->where('A.status_nonjob', $params['status_nonjob']);
		}
		
		if(isset($params['site_id'])){
			if($params['site_id'] != ''){
				$this->db->where('A.site_id', $params['site_id']);
			}
		}

		if(isset($params['employee_number'])){
			$this->db->like('A.employee_number', $params['employee_number']);
		}

		if(isset($params['id_card'])){
			$this->db->like('A.id_card', $params['id_card']);
		}

		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}

		if(isset($params['employee_company'])){
			if($params['employee_company'] != ''){
				$this->db->where('B.code', $params['employee_company']);
			}
		}

		if(isset($params['employee_branch'])){
			$this->db->like('C.name', $params['employee_branch']);
		}

		if(isset($params['position_name'])){
			$this->db->like('D.name', $params['position_name']);
		}

		if(isset($params['site_name'])){
			$this->db->like('E.name', $params['site_name']);
		}

		if(isset($params['site_company'])){
			if($params['site_company'] != ''){
				$this->db->where('F.code', $params['site_company']);
			}
		}

		if(isset($params['site_branch'])){
			$this->db->like('G.name', $params['site_branch']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.full_name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function ro_gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_company.' B', 'A.company_id = B.id', 'left');
		$this->db->join($this->table_position.' C', 'A.position_id = C.id', 'left');

		$this->db->where('A.is_active', 1);

		if(isset($params['status'])){
			$this->db->where('A.status', $params['status']);
		}
		
		if(isset($params['status_approval'])){
			$this->db->where('A.status_approval', $params['status_approval']);
		}
		
		if(isset($params['site_id'])){
			if($params['site_id'] != ''){
				$this->db->where('A.site_id', $params['site_id']);
			}
		}

		if(isset($params['employee_number'])){
			$this->db->like('A.employee_number', $params['employee_number']);
		}

		if(isset($params['id_card'])){
			$this->db->like('A.id_card', $params['id_card']);
		}

		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}

		if(isset($params['ptkp'])){
			$this->db->like('A.ptkp', $params['ptkp']);
		}

		if(isset($params['phone_number'])){
			$this->db->like('A.phone_number', $params['phone_number']);
		}

		if(isset($params['bank_name'])){
			$this->db->like('A.bank_name', $params['bank_name']);
		}

		if(isset($params['bank_account'])){
			$this->db->like('A.bank_account', $params['bank_account']);
		}

		if(isset($params['bank_account_name'])){
			$this->db->like('A.bank_account_name', $params['bank_account_name']);
		}

		if(isset($params['position'])){
			$this->db->like('C.name', $params['position']);
		}

		if(isset($params['company_code'])){
			if($params['company_code'] != ''){
				$this->db->where('B.code', $params['company_code']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.full_name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function gets_ptkp($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_tax.' B', 'A.ptkp = B.id', 'left');

		$this->db->where('A.is_active', 1);

		if(isset($params['status'])){
			$this->db->where('A.status', $params['status']);
		}

		if(isset($params['status_approval'])){
			$this->db->where('A.status_approval', $params['status_approval']);
		}

		if(isset($params['site_id'])){
			if($params['site_id'] != ''){
				$this->db->where('A.site_id', $params['site_id']);
			}
		}

		if(isset($params['resign_submit'])){
			if($params['resign_submit'] != ''){
				$this->db->like('A.resign_submit', $params['resign_submit']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.full_name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function preview($params = array())
	{
		if(isset($params['id_card'])){
			$this->db->where('A.id_card', $params['id_card']);
		}
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		$this->db->where('A.is_active', 1);
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_user.' B', 'A.created_by = B.id', 'left');
		$this->db->join($this->table_user.' C', 'A.updated_by = C.id', 'left');
		$this->db->join($this->table_city.' D', 'A.city_birth_id = D.id', 'left');
		$this->db->join($this->table_city.' E', 'A.city_card_id = E.id', 'left');
		$this->db->join($this->table_city.' F', 'A.city_domisili_id = F.id', 'left');
		$this->db->join($this->table_preference.' G', 'A.preference_job1 = G.id', 'left');
		$this->db->join($this->table_preference.' H', 'A.preference_job2 = H.id', 'left');
		$this->db->join($this->table_preference.' I', 'A.preference_job3 = I.id', 'left');
		$this->db->join($this->table_position.' J', 'A.position_id = J.id', 'left');
		$this->db->join($this->table_site.' K', 'A.site_id = K.id', 'left');
		$this->db->join($this->table_company.' L', 'A.company_id = L.id', 'left');
		$this->db->join($this->table_branch.' N', 'A.branch_id = N.id', 'left');

		$this->db->select('A.*, B.full_name AS creator, C.full_name AS updater, D.name AS city_birth, E.name AS city_card, E.id AS city_card_id, F.name AS city_domisili, G.name AS preference1, H.name AS preference2, I.name AS preference3, J.name AS position, K.name AS site_name, K.address AS site_address, K.code AS site_code, L.name AS company_name, L.code AS company_code, L.address AS company_address, N.name AS branch_name');

		$result = $this->db->get()->row();
		return $result;
	}

	public function last_employee_number($params = array())
	{
		$this->db->from($this->table_employee.' A');
		$this->db->select('A.employee_number');
		$this->db->order_by('A.employee_number', 'DESC');
		$this->db->like('A.employee_number', $params['prefix'], 'after');
		$this->db->limit(1);
		$result = $this->db->get()->row();
		return $result;
	}

	public function set_nonjob($params = array()){
		if ($params['site_id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('site_id', $params['site_id']);
			$this->db->update($this->table_employee, array('status_nonjob' => 1));
			$id = $params['id'];
		}
		return $id;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_employee, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['approved_at']= date('Y-m-d H:i:s');
			$params['approved_by']= $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			
			if(isset($params['employee_number'])){
				$params['created_at'] = '2020-01-01'.date('H:i:s');
				$params['approved_at']= '2020-01-01'.date('H:i:s');
				$params['updated_at'] = '2020-01-01'.date('H:i:s');
			}
			
			$this->db->insert($this->table_employee, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
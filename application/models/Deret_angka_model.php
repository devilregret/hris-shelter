<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deret_angka_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_user       			= 'm_user';
		$this->table_deret_angka			= 't_deret_angka';
		$this->table_deret_angka_d			= 't_deret_angka_d';
	}

	public function getSoal(){
		$result = $this->db->query("SELECT * from m_deret_angka WHERE deleted_at is null and jawaban_benar_id is not null ORDER BY no_soal")->result();
		return $result;
	}
	public function getDeret(){
		$result = $this->db->query("SELECT * from m_deret_angka_d WHERE deleted_at is null ORDER BY id")->result();
		return $result;
	}
	public function getJawaban(){
		$result = $this->db->query("SELECT * from m_deret_angka_jawaban WHERE deleted_at is null ORDER BY id")->result();
		return $result;
	}

	public function save($params = array()){
		$params['is_aktif']  = 1;
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_deret_angka, $params);
		$id = $this->db->insert_id();
		return $id;
	}

	public function delete($params = array())
	{
		$id = false;

		$params['is_aktif']  = 0;
		$params['deleted_at'] = date('Y-m-d H:i:s');
		$params['deleted_by'] = $this->session->userdata('user_id');
		$this->db->where('id', $params['id']);
		$this->db->update($this->table_deret_angka, $params);
		$id = $params['id'];

		return $id;
	}

	public function saveD($params = array()){
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_deret_angka_d, $params);
		$id = $this->db->insert_id();
		return $id;
	}

	public function updateHasil($params = array())
	{
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->where('id', $params['id']);
		$this->db->update($this->table_deret_angka, $params);
		$id = $params['id'];
		return $id;
	}

	public function count_deret_angka_vacancy ($employeeId,$vacancyId){
		$query = "SELECT id FROM t_deret_angka WHERE deleted_at is null and is_aktif = 1 and m_employee_id = ".$employeeId." and m_vacancy_id = ".$vacancyId;
		return $this->db->query($query)->result();
	}

	public function get_data_deret_angka($deretAngkaId){
		$q = "SELECT * from t_deret_angka WHERE deleted_at is null and id = ".$deretAngkaId.";";
		return $this->db->query($q)->result();
	}

	public function get_data_deret_angka_d($deretAngkaId){
		$q = "SELECT *,(select no_soal from m_deret_angka where id = m_deret_angka_id) as no_soal from t_deret_angka_d WHERE deleted_at is null and t_deret_angka_id = ".$deretAngkaId.";";
		return $this->db->query($q)->result();
	}

}
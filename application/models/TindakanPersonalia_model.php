<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TindakanPersonalia_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_tindakan   	= 't_form_tindakan';
		$this->table_employee  		= 'm_employee';
		$this->table_struktur 		= 'm_struktur_jabatan';
		$this->table_approval 		= 'm_struktur_approval';
		$this->table_vacation		= 't_vacation';
		$this->table_m_cuti_type	= 'm_cuti_type';	
		$this->table_position 		= 'm_position';
	}

	public function gets_cuti($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_tindakan.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		$this->db->join($this->table_employee.' C', 'A.verifikasi_by = C.id', 'left');
		$this->db->join($this->table_employee.' D', 'A.disetujui_by = D.id', 'left');
		$this->db->join($this->table_employee.' E', 'A.diketahui_by = E.id', 'left');
		
		$this->db->where('A.is_active', 1);
		$this->db->where('A.employee_id', $params['employee_id']);

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'date'){
				$params['orderby'] = 'A.created_at';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.created_at', 'DESC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function personalia_gets_cuti($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_tindakan.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		$this->db->join($this->table_employee.' C', 'A.verifikasi_by = C.id', 'left');
		$this->db->join($this->table_employee.' D', 'A.disetujui_by = D.id', 'left');
		$this->db->join($this->table_employee.' E', 'A.diketahui_by = E.id', 'left');

		$this->db->where('A.is_active', 1);
		
		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'date'){
				$params['orderby'] = 'A.created_at';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.created_at', 'DESC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function preview($params = array())
	{
		$this->db->from($this->table_tindakan.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		$this->db->join($this->table_m_cuti_type.' C', 'A.type_id = C.id', 'left');
		$this->db->join($this->table_employee.' D', 'A.verifikasi_by = D.id', 'left');
		$this->db->join($this->table_employee.' E', 'A.disetujui_by = E.id', 'left');
		$this->db->join($this->table_employee.' F', 'A.diketahui_by = F.id', 'left');
		
		$this->db->where('A.is_active', 1);
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		
		$this->db->order_by('A.created_at', 'DESC');
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->row();
		// print_r($result);

		return $result;
	}

	public function get_struktur($params = array())
	{
		$this->db->from($this->table_struktur.' SJ');
		$this->db->join($this->table_struktur.' apr1', 'SJ.parent_id = apr1.id', 'left');
		$this->db->join($this->table_struktur.' apr2', 'apr1.parent_id = apr2.id', 'left');
		$this->db->join($this->table_struktur.' apr3', 'apr2.parent_id = apr3.id', 'left');
		
		$this->db->where('SJ.id', $this->session->userdata('struktur_id'));
		$this->db->select($params['columns']);

		$result = $this->db->get()->row();
		// print_r($result);
		// die();
		return $result;
	}

	public function get_strukturv2($params = array())
	{
		$this->db->from($this->table_approval.' A');
		
		$this->db->where('A.position_id', $this->session->userdata('position_id'));
		$this->db->select($params['columns']);
		$this->db->where('A.is_active', 1);
		$result = $this->db->get()->row();
		// print_r($result);
		// die();
		return $result;
	}

	public function getPersonaliaStruktur($params = array())
	{
		$this->db->from($this->table_employee.' empl');
		$this->db->join($this->table_struktur.' SJ', 'empl.struktur_id = SJ.id', 'left');
		$this->db->join($this->table_struktur.' apr1', 'SJ.parent_id = apr1.id', 'left');
		$this->db->join($this->table_struktur.' apr2', 'apr1.parent_id = apr2.id', 'left');
		$this->db->join($this->table_struktur.' apr3', 'apr2.parent_id = apr3.id', 'left');
		
		$this->db->where('empl.id', $params['employee_id']);
		$this->db->select($params['columns']);

		$result = $this->db->get()->row();
		return $result;
	}

	public function getCountCuti($kategoriId = 0){
		$this->db->select_sum('A.jumlah_hari');
		$this->db->from($this->table_tindakan.' A');
		
		$this->db->where('A.employee_id', $this->session->userdata('employee_id'));
		$this->db->where('A.is_active', 1);
		$this->db->where('A.kategori_id', $kategoriId);
		$this->db->where('A.diketahui_status', 1);

		$result = $this->db->get()->row();
		return $result;
	}

	private function getChild($parentId){
		$this->db->select('B.id, B.full_name');
		$this->db->from($this->table_struktur.' A');
		$this->db->join($this->table_employee.' B', 'A.id = B.struktur_id', 'left');
		$this->db->where('A.parent_id', $parentId);

		$result = $this->db->get()->result();
		return $result;
	}

	public function gets_approval($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_tindakan.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		$this->db->join($this->table_employee.' C', 'A.verifikasi_by = C.id', 'left');
		$this->db->join($this->table_employee.' D', 'A.disetujui_by = D.id', 'left');
		$this->db->join($this->table_employee.' E', 'A.diketahui_by = E.id', 'left');
		
		$this->db->where("A.is_active = 1 and (A.struktur_1 = ".$this->session->userdata('position_id')." or A.struktur_2 = ".$this->session->userdata('position_id')." or A.struktur_3 = ".$this->session->userdata('position_id').") ", NULL, FALSE);
		// $this->db->where('C.is_active', 1);

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		$this->db->order_by('A.created_at', 'DESC');

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		
		return $result;
	}

	public function get($params = array())
	{
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		$this->db->from($this->table_tindakan.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		$result = $this->db->get()->row();
		return $result;
	}

	public function getMasterTypeCutiById($params = array())
	{
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		$this->db->from($this->table_m_cuti_type.' A');

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		$result = $this->db->get()->row();
		return $result;
	}

	function getMasterTypeCuti()
    {
		$this->db->select('A.id, concat(A.nama,  " - ", A.jumlah_hari, " hari")as nama, A.jumlah_hari');
		$this->db->from($this->table_m_cuti_type.' A');
		$this->db->where('A.is_active', 1);
		
		// print_r($this->db->get()->result());
		// die();
		return $this->db->get()->result();
    }

	function getMasterEmployee()
    {
		$this->db->select('A.id, A.id_card, A.full_name ');
		$this->db->from($this->table_employee.' A');
		$this->db->where('A.is_active', 1);
		$this->db->where('A.id_card !=', "");
		$this->db->where('A.id_card is not null');
		$this->db->where('A.site_id IN ("2")');
		$this->db->where('A.status_approval =', 3);
		$this->db->order_by('A.full_name', 'ASC');
		// print_r($this->db->get()->result());
		// die();
		return $this->db->get()->result();
    }

	public function save($params = array(), $sisa_cuti = array())
	{
		$id = false;
		if (isset($params['id'])) {
			$params['last_updated'] = date('Y-m-d H:i:s');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_tindakan, $params);
			$id = $params['id'];
		}else{
			$id = $params['employee_id'];
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['type_kompensasi'] = "Cuti";
			$params['is_active'] = 1;
			$params['jumlah_hari'] = date("d", strtotime($params['end_date'])) - date("d", strtotime($params['start_date'])) + 1;
			// $this->personal($params['struktur_1']);
			$this->db->insert($this->table_tindakan, $params);
			$id = $this->db->insert_id();

		}
		return $id;
	}

	public function saveCutiByPersonalia($params = array())
	{
		// print_r($params);
		// die();
		$id = false;
		if (isset($params['id'])) {
			$params['last_updated'] = date('Y-m-d H:i:s');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_tindakan, $params);
			$id = $params['id'];
		}else{
			$id = $params['employee_id'];
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['type_kompensasi'] = "Cuti";
			$params['is_active'] = 1;
			$params['jumlah_hari'] = date("d", strtotime($params['end_date'])) - date("d", strtotime($params['start_date'])) + 1;

			$this->db->insert($this->table_tindakan, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}

	public function approve($params = array())
	{
		$this->load->model(array('employee_model'));
		$id = false;
		if (isset($params['id'])) {
			$params['last_updated'] = date('Y-m-d H:i:s');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_tindakan, $params);
			
			$this->db->from($this->table_tindakan.' A');
			$this->db->where('A.id', $params['id']);
			$result = get_object_vars($this->db->get()->row());
			$kategori_id = $result['kategori_id'];
			$type_id = $result['kategori_id'];
			$id = $this->db->affected_rows();
			
			// disetujui_status, verifikasi_status, diketahui_status

			if($kategori_id == 1 && $result['struktur_1'] == $this->session->userdata('position_id') && $result['struktur_2'] == 0 && $result['struktur_3'] == 0){
				$jumlahHari = $result['jumlah_hari'];
		
				// $vacation['vacation'] = $sisa_cuti['sisa_cuti']['vacation'] + $jumlahHari;
				$vacation['updated_at'] = date('Y-m-d H:i:s');
				$vacation['updated_by'] = $this->session->userdata('user_id');
				print_r($jumlahHari);
			
				$this->db->set('vacation', '`vacation` - '.$jumlahHari, FALSE);
				$this->db->where('employee_id', $result['employee_id']);
				$this->db->where('is_active', 1);
				$this->db->update('t_vacation');
			}else if($kategori_id == 1 &&  $result['struktur_2'] == $this->session->userdata('position_id') && $result['struktur_3'] == 0){
				$jumlahHari = $result['jumlah_hari'];
		
				// $vacation['vacation'] = $sisa_cuti['sisa_cuti']['vacation'] + $jumlahHari;
				$vacation['updated_at'] = date('Y-m-d H:i:s');
				$vacation['updated_by'] = $this->session->userdata('user_id');
				print_r($jumlahHari);
			
				$this->db->set('vacation', '`vacation` - '.$jumlahHari, FALSE);
				$this->db->where('employee_id', $result['employee_id']);
				$this->db->where('is_active', 1);
				$this->db->update('t_vacation');
			}else if($kategori_id == 1 && $result['struktur_3'] == $this->session->userdata('position_id')){
				$jumlahHari = $result['jumlah_hari'];
		
				// $vacation['vacation'] = $sisa_cuti['sisa_cuti']['vacation'] + $jumlahHari;
				$vacation['updated_at'] = date('Y-m-d H:i:s');
				$vacation['updated_by'] = $this->session->userdata('user_id');
				print_r($jumlahHari);
			
				$this->db->set('vacation', '`vacation` - '.$jumlahHari, FALSE);
				$this->db->where('employee_id', $result['employee_id']);
				$this->db->where('is_active', 1);
				$this->db->update('t_vacation');
			}

		}
		return $id;
	}

	public function updateDocument($params = array())
	{
		$id = false;
		if (isset($params['id'])) {
			$params['last_updated'] = date('Y-m-d H:i:s');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_tindakan, $params);
			$id = $this->db->affected_rows();
	
		}
		return $id;
	}

	// public function list_atasan($params = array())
	// {
	// 	$this->db->from($this->table_employee.' A');
	// 	$this->db->join($this->table_position.' B', 'A.position_id = B.id', 'left');
	// 	$this->db->where("A.is_active = 1 AND A.struktur_id = ".$params['struktur_id'], NULL, FALSE);
	
		
	// 	$this->db->select("A.id, A.full_name, A.phone_number, B.name as position_name");
		
		
	// 	$result = $this->db->get()->result();
		
	// 	return $result;
	// }

	public function list_atasanV2($params = array())
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_position.' B', 'A.position_id = B.id', 'left');
		$this->db->where("A.is_active = 1 AND A.position_id = ".$params['position_id'], NULL, FALSE);
	
		
		$this->db->select("A.id, A.full_name, A.phone_number, B.name as position_name");
		
		
		$result = $this->db->get()->result();
		
		return $result;
	}
}
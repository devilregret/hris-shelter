<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_activity   = 't_daily_activity';
		$this->table_employee  	= 'm_employee';
		$this->table_branch 	= 'm_branch';
		$this->table_company 	= 'm_company';
		$this->table_position 	= 'm_position';
	}

	public function gets_employee($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_activity.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		$this->db->join($this->table_branch.' C', 'B.branch_id = C.id', 'left');
		$this->db->join($this->table_company.' D', 'B.company_id = D.id', 'left');
		$this->db->join($this->table_position.' E', 'B.position_id = E.id', 'left');
		
		$this->db->where('A.is_active', 1);
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}
		if(isset($params['date'])){
			if($params['date'] != ''){
				$this->db->like("DATE_FORMAT(A.date, '%d/%m/%Y')", $params['date']);
			}
		}
		if(isset($params['time_start'])){
			$this->db->like('A.time_start', $params['time_start']);
		}
		if(isset($params['time_finish'])){
			$this->db->like('A.time_finish', $params['time_finish']);
		}
		if(isset($params['activity'])){
			$this->db->like('A.activity', $params['activity']);
		}
		if(isset($params['result'])){
			$this->db->like('A.result', $params['result']);
		}
		if(isset($params['employee_name'])){
			if($params['employee_name'] != ''){
				$this->db->like('B.full_name', $params['employee_name']);
			}
		}
		if(isset($params['branch_name'])){
			if($params['branch_name'] != ''){
				$this->db->like('C.name', $params['branch_name']);
			}
		}
		if(isset($params['company_code'])){
			if($params['company_code'] != ''){
				$this->db->where('D.code', $params['company_code']);
			}
		}
		if(isset($params['position_name'])){
			if($params['position_name'] != ''){
				$this->db->like('E.name', $params['position_name']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'date'){
				$params['orderby'] = 'A.date';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.date', 'DESC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_activity.' A');
		$this->db->where('A.is_active', 1);

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}
		if(isset($params['date'])){
			if($params['date'] != ''){
				$this->db->like("DATE_FORMAT(A.date, '%d/%m/%Y')", $params['date']);
			}
		}
		if(isset($params['time_start'])){
			$this->db->like('A.time_start', $params['time_start']);
		}
		if(isset($params['time_finish'])){
			$this->db->like('A.time_finish', $params['time_finish']);
		}
		if(isset($params['activity'])){
			$this->db->like('A.activity', $params['activity']);
		}
		if(isset($params['result'])){
			$this->db->like('A.result', $params['result']);
		}
		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'date'){
				$params['orderby'] = 'A.date';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.date', 'DESC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}
		if(isset($params['date'])){
			$this->db->where('A.date', $params['date']);
		}
		if(isset($params['time_start'])){
			$this->db->where('A.time_start', $params['time_start']);
		}
		if(isset($params['time_finish'])){
			$this->db->where('A.time_finish', $params['time_finish']);
		}

		$this->db->from($this->table_activity.' A');

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.employee_id');
		}
		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = $params['employee_id'];
		$params['is_active']  = 1;
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_activity, $params);
		return $id;
	}

	public function update($params = array())
	{
		$id = $params['employee_id'];
		$params['is_active']  = 1;
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->where('employee_id', $params['employee_id']);
		$this->db->where('date', $params['date']);
		$this->db->where('time_start', $params['time_start']);
		$this->db->where('time_finish', $params['time_finish']);
		$this->db->update($this->table_activity, $params);
		return $id;
	}
}
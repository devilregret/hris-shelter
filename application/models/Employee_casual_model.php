<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_casual_model extends CI_Model{
	public function __construct()
	{
		parent::__construct();
		$this->table_employee_casual	= 'm_employee_casual';
		$this->table_department_casual	= 'm_department_casual';
		$this->table_position			= 'm_position';
	}

	public function get($params = array())
	{
		
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', 1);
		}
		
		if(isset($params['pin_finger'])){
			$this->db->where('A.pin_finger', $params['pin_finger']);
		}

		if(isset($params['employee_number'])){
			$this->db->where('A.employee_number', $params['employee_number']);
		}

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}
		
		if(isset($params['id_card'])){
			$this->db->where('A.id_card', $params['id_card']);    
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_employee_casual.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee_casual.' A');
		$this->db->join($this->table_department_casual.' B', 'A.department_casual_id = B.id', 'left');
		$this->db->join($this->table_position.' C', 'A.position_id = C.id', 'left');
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', $params['is_active']);
		}else{
			$this->db->where('A.is_active', 1);
		}

		if(isset($params['is_status'])){
			if($params['is_status'] != ''){
				$this->db->where('A.status', $params['is_status']);
			}
		}
		if(isset($params['site_id'])){
			if($params['site_id'] != ''){
				$this->db->where('A.site_id', $params['site_id']);
			}
		}

		if(isset($params['status'])){
			$this->db->like('A.status', $params['status']);
		}
		
		if(isset($params['employee_number'])){
			$this->db->like('A.employee_number', $params['employee_number']);
		}

		if(isset($params['pin_finger'])){
			$this->db->like('A.pin_finger', $params['pin_finger']);
		}

		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}

		if(isset($params['id_card'])){
			$this->db->like('A.id_card', $params['id_card']);
		}

		if(isset($params['address_card'])){
			$this->db->like('A.address_card', $params['address_card']);
		}

		if(isset($params['phone_number'])){
			$this->db->like('A.phone_number', $params['phone_number']);
		}

		if(isset($params['bank_name'])){
			$this->db->like('A.bank_name', $params['bank_name']);
		}

		if(isset($params['bank_account'])){
			$this->db->like('A.bank_account', $params['bank_account']);
		}

		if(isset($params['department_name'])){
			if($params['department_name'] != ''){
				$this->db->like('B.name', $params['department_name']);
			}
		}

		if(isset($params['position_name'])){
			if($params['position_name'] != ''){
				$this->db->like('C.name', $params['position_name']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.full_name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_employee_casual, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			
			$this->db->insert($this->table_employee_casual, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
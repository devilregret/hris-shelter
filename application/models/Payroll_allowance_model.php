<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll_allowance_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->table_user						= 'm_user';
		$this->table_employee					= 'm_employee';
		$this->table_payroll_allowance			= 't_allowance';
		$this->table_payroll_allowance_detail	= 't_allowance_detail';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_payroll_allowance.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		
		$this->db->where('A.is_active', '1');
		
		if(isset($params['site_id'])){
			$this->db->where('B.site_id', $params['site_id']);
		}

		if(isset($params['employee_name'])){
			$this->db->like('B.full_name', $params['employee_name']);
		}

		if(isset($params['employee_email'])){
			$this->db->like('B.email', $params['employee_email']);
		}

		if(isset($params['periode_start'])){
			$this->db->like('DATE_FORMAT(A.periode_start, "%d/%m/%Y")', $params['periode_start']);
		}

		if(isset($params['periode_end'])){
			$this->db->like('DATE_FORMAT(A.periode_end, "%d/%m/%Y")', $params['periode_end']);
		}

		if(isset($params['salary'])){
			$this->db->like('A.salary', $params['salary']);
		}

		if(isset($params['payment'])){
			$this->db->like('A.payment', $params['payment']);
		}

		if(isset($params['email'])){
			$this->db->like('A.email', $params['email']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'periode_start'){
				$params['orderby'] = 'A.periode_start';
			}
			if($params['orderby'] == 'periode_end'){
				$params['orderby'] = 'A.periode_end';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.name', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->group_by('A.id');
		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', $params['is_active']);
		
		}else{
			$this->db->where('A.is_active', 1);
		}

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['periode_start'])){
			$this->db->where('A.periode_start', $params['periode_start']);
		}

		if(isset($params['periode_end'])){
			$this->db->where('A.periode_end', $params['periode_end']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_payroll_allowance.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_payroll_allowance, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_payroll_allowance, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}

	public function gets_detail($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_payroll_allowance_detail.' A');
		
		$this->db->where('A.is_active', '1');
		
		if(isset($params['payroll_id'])){
			$this->db->where('A.payroll_id', $params['payroll_id']);
		}

		if(isset($params['category'])){
			$this->db->like('A.category', $params['category']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.name', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function delete_detail($params = array())
	{
		$this->db->where('payroll_id',$params['payroll_id']);
		$this->db->delete($this->table_payroll_allowance_detail);
	}

	public function save_detail($params = array())
	{
		$params['is_active']  = 1;
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_payroll_allowance_detail, $params);
			$id = $this->db->insert_id();
		return $id;
	}
}
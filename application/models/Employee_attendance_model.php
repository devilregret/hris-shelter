<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_attendance_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->table_user			= 'm_user';
		$this->table_attendance		= 't_employee_attendance';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_attendance.' A');
		$this->db->where('A.is_active', '1');

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['date'])){
			$this->db->like("DATE_FORMAT(A.date, '%d/%m/%Y')", $params['date']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'date'){
				$params['orderby'] = 'A.date';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.date', 'DESC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', $params['is_active']);
		}else{
			$this->db->where('A.is_active', 1);
		}

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['date'])){
			$this->db->where('A.date', $params['date']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_attendance.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function update($params = array()){

		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->where('employee_id', $params['employee_id']);
		$this->db->where('date', $params['date']);
		$this->db->update($this->table_attendance, $params);
		
		return true;
	}

	public function insert($params = array())
	{
		$params['is_active']  = 1;
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_attendance, $params);
		
		return true;
	}
}
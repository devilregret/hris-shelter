<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job_description_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->table_employee		= 'm_employee';
		$this->table_position		= 'm_position';
		$this->table_jobdesc 		= 'm_job_description';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_position.' B', 'B.id = A.position_id', 'left');
		$this->db->join($this->table_jobdesc.' C', 'C.position_id = B.id', 'left');
		$this->db->where('A.is_active', '1');
		$this->db->where('A.status', 1);
		$this->db->where('A.status_approval', 3);

		if(isset($params['site_id'])){
			if($params['site_id'] != ''){
				$this->db->where('A.site_id', $params['site_id']);
			}
		}

		if(isset($params['employee_id'])){
			$this->db->where('A.id', $params['employee_id']);
		}

		if(isset($params['position_name'])){
			if($params['position_name'] != ''){
				$this->db->like('B.name', $params['position_name']);
			}
		}

		if(isset($params['description'])){
			if($params['description'] != ''){
				$this->db->like('C.description', $params['description']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'date'){
				$params['orderby'] = 'A.date';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		if(isset($params['groupby'])){
  			$this->db->group_by($params['groupby']);
  		}
		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', $params['is_active']);
		}

		if(isset($params['position_id'])){
			$this->db->where('A.position_id', $params['position_id']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_jobdesc.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array()){
		
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->replace($this->table_jobdesc, $params);
		return 1;
	}
}
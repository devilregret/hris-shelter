<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model{
	private $table_user;
	private $table_city;
	private $table_employee;
	private $table_site;
	private $table_branch;
	private $table_position;
	private $table_skill;
	private $table_education;
	private $table_emergency;
	private $table_history;
	private $table_language;
	private $table_reference;

	public function __construct()
	{
		parent::__construct();
		$this->table_user		= 'm_user';
		$this->table_city		= 'm_city';
		$this->table_employee	= 'm_employee';
		$this->table_site		= 'm_site';
		$this->table_branch		= 'm_branch';
		$this->table_position	= 'm_position';
		$this->table_preference	= 'm_preference_job';
		$this->table_skill		= 'm_employee_skill';
		$this->table_education	= 'm_employee_education';
		$this->table_emergency	= 'm_employee_emergency';
		$this->table_history	= 'm_employee_history';
		$this->table_language	= 'm_employee_language';
		$this->table_reference	= 'm_employee_reference';
	}

	public function chart_cuti_req($params = array()){	
		$query = "SELECT 
						count(id) as total
					FROM 
						t_form_tindakan 
					where
						is_active = 1
						and (DATE_FORMAT(start_date, '%Y-%m') = '".$params['period']."' or DATE_FORMAT(end_date, '%Y-%m') = '".$params['period']."')
						and (verifikasi_status is null or disetujui_status is null or diketahui_status is null) ";

		$result = $this->db->query($query)->row();
		return $result->total;
	}

	public function chart_cuti_aprove($params = array()){
		$query = "SELECT 
					count(id) as total
				FROM 
					t_form_tindakan 
				where
					is_active = 1
					and (DATE_FORMAT(start_date, '%Y-%m') = '".$params['period']."' or DATE_FORMAT(end_date, '%Y-%m') = '".$params['period']."')
					and diketahui_status = 1";

		$result = $this->db->query($query)->row();
		// print_r($result->total);
		return $result->total;
	}

	public function chart_cuti_reject($params = array()){	
		$query = "SELECT 
						count(id) as total
					FROM 
						t_form_tindakan 
					where
						is_active = 1
						and (DATE_FORMAT(start_date, '%Y-%m') = '".$params['period']."' or DATE_FORMAT(end_date, '%Y-%m') = '".$params['period']."')
						and (verifikasi_status = 0 or disetujui_status = 0 or diketahui_status = 0) ";

		$result = $this->db->query($query)->row();
		return $result->total;
	}
	
	public function count_category($params = array()){
		$branch = '';
		if(isset($params['session_branch'])){
			$branch = "AND e.branch_id = '".$params['session_branch']."'";
		}
		if(isset($params['period'])){
			$branch = "AND e.created_at LIKE '".$params['period']."%'";
		}

		$total = 0;
		$result1 = $this->db->query("SELECT COUNT(e.id) AS total FROM m_employee e 
			JOIN m_preference_job pj ON pj.id = e.preference_job1 
			WHERE status = 0 AND e.is_active = 1 AND e.status_approval = 0 ".$branch)->row();
		$total = $total + $result1->total;

		$result2 = $this->db->query("SELECT COUNT(e.id) AS total FROM m_employee e 
			JOIN m_preference_job pj ON pj.id = e.preference_job2 
			WHERE status = 0 AND e.is_active = 1 AND e.status_approval = 0 ".$branch)->row();
		$total = $total + $result2->total;

		$result3 = $this->db->query("SELECT COUNT(e.id) AS total FROM m_employee e 
			JOIN m_preference_job pj ON pj.id = e.preference_job3 
			WHERE status = 0 AND e.is_active = 1 AND e.status_approval = 0 ".$branch)->row();
		$total = $total + $result3->total;

		return $total;
	}

	public function count_candidate($params = array()){
		$branch = '';
		$period = '';
		if(isset($params['session_branch'])){
			$branch = "AND branch_id = '".$params['session_branch']."'";
		}
		if(isset($params['period'])){
			$branch = "AND created_at LIKE '".$params['period']."%'";
		}else{
			$branch = "AND created_at LIKE '".date('Y-m')."%'";	
		}

		$result = $this->db->query("SELECT COUNT(id) AS total FROM m_employee WHERE is_active = 1 ".$branch." ".$period)->row();		
		// $result = $this->db->query("SELECT COUNT(id) AS total FROM m_employee WHERE status = 0 AND is_active = 1 AND status_approval = 0 AND updated_at BETWEEN NOW() - INTERVAL 3 MONTH AND NOW() ".$branch)->row();
		return $result->total;
	}

	public function count_client($params = array()){
		$branch = '';
		if(isset($params['session_branch'])){
			$branch = "AND B.branch_id = '".$params['session_branch']."'";
		}
		if(isset($params['period'])){
			$branch = "AND A.created_at LIKE '".$params['period']."%'";
		}
		$result = $this->db->query("SELECT COUNT(A.id) AS total FROM m_employee A LEFT JOIN m_site B ON A.site_id = B.id WHERE A.status = 0 AND A.is_active = 1 AND A.status_approval = 1 ".$branch)->row();
		return $result->total;
	}

	public function count_personalia($params = array()){
		$branch = '';
		if(isset($params['session_branch'])){
			$branch = "AND B.branch_id = '".$params['session_branch']."'";
		}
		if(isset($params['period'])){
			$branch = "AND A.created_at LIKE '".$params['period']."%'";
		}
		$result = $this->db->query("SELECT COUNT(A.id) AS total FROM m_employee A LEFT JOIN m_site B ON A.site_id = B.id WHERE A.status = 0 AND A.is_active = 1 AND A.status_approval = 2 ".$branch)->row();
		return $result->total;
	}
	
	public function count_approved($params = array()){
		$branch = '';
		if(isset($params['session_branch'])){
			if($params['session_branch'] > 1){
				$branch = "AND branch_id = '".$params['session_branch']."'";
			}
		}
		if(isset($params['period'])){
			$branch = "AND created_at LIKE '".$params['period']."%'";
		}
		$result = $this->db->query("SELECT COUNT(id) AS total FROM m_employee WHERE status = 1 AND is_active = 1 AND approved_at BETWEEN NOW() - INTERVAL 3 MONTH AND NOW() ".$branch)->row();
		return $result->total;
	}

	public function total_resume_job($params = array()){
		$branch = '';
		if(isset($params['session_branch'])){
			$branch = "AND e.branch_id = '".$params['session_branch']."'";
		}

		if(isset($params['period'])){
			$query = "SELECT  
				SUM((SELECT COUNT(e.id) FROM m_employee e WHERE e.preference_job1 = p.id AND e.status_approval = 0 AND e.is_active = 1 AND DATE_FORMAT(e.created_at, '%Y-%m')  = '".$params['period']."' ".$branch.") + 
				(SELECT COUNT(e.id) FROM m_employee e WHERE e.preference_job2 = p.id AND e.status_approval = 0 AND e.is_active = 1 AND DATE_FORMAT(e.created_at, '%Y-%m')  = '".$params['period']."' ".$branch.") +
				(SELECT COUNT(e.id) FROM m_employee e WHERE e.preference_job3 = p.id AND e.status_approval = 0 AND e.is_active = 1 AND DATE_FORMAT(e.created_at, '%Y-%m')  = '".$params['period']."' ".$branch.")) AS total
				FROM m_preference_job p ORDER BY p.name ASC";
		}else{
			$query = "SELECT 
				SUM((SELECT COUNT(id) FROM m_employee e WHERE e.preference_job1 = p.id AND e.status_approval = 0 AND e.is_active = 1 ".$branch." ) +
				(SELECT COUNT(id) FROM m_employee e WHERE e.preference_job2 = p.id AND e.status_approval = 0 AND e.is_active = 1 ".$branch." ) +
				(SELECT COUNT(id) FROM m_employee e WHERE e.preference_job3 = p.id AND e.status_approval = 0 AND e.is_active = 1 ".$branch.")) AS total
				FROM m_preference_job p  ORDER BY p.name ASC";
		}
		$result = $this->db->query($query)->row();
		return $result->total;
	}

	public function chart_job($params = array()){
		$branch = '';
		if(isset($params['session_branch'])){
			$branch = "AND e.branch_id = '".$params['session_branch']."'";
		}
		
		if(isset($params['period'])){
			$query = "SELECT p.name, 
				((SELECT COUNT(e.id) FROM m_employee e WHERE e.preference_job1 = p.id AND e.status_approval = 0 AND e.is_active = 1 AND DATE_FORMAT(e.created_at, '%Y-%m')  = '".$params['period']."' ".$branch.") +
				(SELECT COUNT(e.id) FROM m_employee e WHERE e.preference_job2 = p.id AND e.status_approval = 0 AND e.is_active = 1 AND DATE_FORMAT(e.created_at, '%Y-%m')  = '".$params['period']."' ".$branch.") +
				(SELECT COUNT(e.id) FROM m_employee e WHERE e.preference_job3 = p.id AND e.status_approval = 0 AND e.is_active = 1 AND DATE_FORMAT(e.created_at, '%Y-%m')  = '".$params['period']."' ".$branch.")) AS total
				FROM m_preference_job p ORDER BY p.name ASC";
		}else{
			$query = "SELECT p.name, 
				((SELECT COUNT(id) FROM m_employee e WHERE e.preference_job1 = p.id AND e.status_approval = 0 AND e.is_active = 1 ".$branch.") +
				(SELECT COUNT(id) FROM m_employee e WHERE e.preference_job2 = p.id AND e.status_approval = 0 AND e.is_active = 1 ".$branch.") +
				(SELECT COUNT(id) FROM m_employee e WHERE e.preference_job3 = p.id AND e.status_approval = 0 AND e.is_active = 1 ".$branch.")) AS total
				FROM m_preference_job p ORDER BY p.name ASC";
		}
		$result = $this->db->query($query)->result();
		return $result;
	}
	
	public function approved_candidate($date){
		$this->db->select("COUNT(A.id) AS total");
		$this->db->from($this->table_employee.' A');
		$this->db->where('A.is_active', 1);

		$this->db->where("DATE_FORMAT(A.approved_at, '%Y-%m') = '".$date."'");
		$result = $this->db->get()->row();
		return $result;
	}

	public function chart_education($params){
		$branch = '';
		if(isset($params['session_branch'])){
			$branch = "AND e.branch_id = '".$params['session_branch']."'";
		}

		if(isset($params['period'])){
			$query = "SELECT COUNT(e.id) AS total, e.education_level AS name FROM m_employee e
				WHERE e.status = 0 AND e.is_active = 1 AND DATE_FORMAT(e.created_at, '%Y-%m')  = '".$params['period']."' ".$branch." GROUP BY e.education_level ORDER BY total DESC";
		}else{
			$query = "SELECT COUNT(e.id) AS total, e.education_level AS name FROM m_employee e
				WHERE e.status = 0 AND e.is_active = 1 ".$branch." GROUP BY e.education_level ORDER BY total DESC";
		}
		$result = $this->db->query($query)->result();
		return $result;
	}

	public function chart_recommendation($params){
		$branch = '';
		if(isset($params['session_branch'])){
			$branch = "AND e.branch_id = '".$params['session_branch']."'";
		}

		if(isset($params['period'])){
			$query = "SELECT COUNT(e.id) AS total, e.recommendation AS name FROM m_employee e
				WHERE e.status = 0 AND e.is_active = 1 AND DATE_FORMAT(e.created_at, '%Y-%m')  = '".$params['period']."' ".$branch." GROUP BY e.recommendation ORDER BY total DESC LIMIT 10";
		}else{
			$query = "SELECT COUNT(e.id) AS total, e.recommendation AS name FROM m_employee e
				WHERE e.status = 0 AND e.is_active = 1 ".$branch." GROUP BY e.recommendation ORDER BY total DESC LIMIT 10";
		}
		$result = $this->db->query($query)->result();
		return $result;
	}

	public function count_range_age($params = array()){
		$branch = '';
		if(isset($params['session_branch'])){
			$branch = "AND e.branch_id = '".$params['session_branch']."'";
		}

		if(isset($params['start']) && isset($params['end'])){
			if(isset($params['period'])){
				$query = "SELECT COUNT(id) AS total FROM m_employee e 
					WHERE FLOOR(DATEDIFF(CURRENT_DATE, e.date_birth)/365.25) BETWEEN '".$params['start']."' AND '".$params['end']."' 
					AND e.status = 0 AND e.is_active = 1 AND DATE_FORMAT(e.created_at, '%Y-%m')  = '".$params['period']."' ".$branch;
			}else{
				$query = "SELECT COUNT(id) AS total FROM m_employee e 
					WHERE FLOOR(DATEDIFF(CURRENT_DATE, e.date_birth)/365.25) BETWEEN '".$params['start']."' AND '".$params['end']."' 
					AND e.status = 0 AND e.is_active = 1 ".$branch;
			}
			
			$result = $this->db->query($query)->row();
			return $result->total;
		}else{
			return FALSE;
		}
	}

	public function chart_applicant($params = array()){
		$branch = '';
		if(isset($params['session_branch'])){
			$branch = "AND branch_id = '".$params['session_branch']."'";
		}

		$query 	= "SELECT COUNT(id) AS total, DATE_FORMAT(created_at, '%M %Y') AS month FROM `m_employee` WHERE is_active = 1 ".$branch." GROUP BY DATE_FORMAT(created_at, '%Y%m') ORDER BY created_at DESC LIMIT 12";
		$result = $this->db->query($query)->result();
		return $result;
	}

	public function chart_employee($params = array()){
		$where = '';
		if(isset($params['branch_id'])){
			$where .= "AND e.branch_id = '".$params['branch_id']."' ";
		}
		if(isset($params['company_id'])){
			$where .= "AND e.company_id = '".$params['company_id']."' ";
		}

		$query 	= "SELECT COUNT(ec.employee_id) AS total, DATE_FORMAT(ec.contract_end, '%d %M %Y') AS 'date'  FROM t_employee_contract ec 
				INNER JOIN m_employee e ON ec.employee_id = e.id
				WHERE ec.is_active = 1 AND ec.contract_end >= NOW() AND ec.contract_end <= NOW() + INTERVAL 1 MONTH ".$where." 
				GROUP BY DATE_FORMAT(ec.contract_end, '%Y%m%d') ORDER BY ec.contract_end DESC";
		$result = $this->db->query($query)->result();
		return $result;
	}

	public function history_employee($params = array()){
		$where = '';
		if(isset($params['branch_id'])){
			$where .= "AND e.branch_id = '".$params['branch_id']."' ";
		}
		if(isset($params['company_id'])){
			$where .= "AND e.company_id = '".$params['company_id']."' ";
		}
		//$query = "SELECT COUNT(e.id) AS total
		//		FROM m_employee e 
		//		WHERE e.is_active = 1 AND ((e.approved_at <= '".$params['period']."' AND e.status_approval = 3) OR (e.resign_submit IS NOT NULL AND e.resign_submit >= '".$params['period']."')) ".$where;
				
		$query = "SELECT COUNT(e.id) AS total
				FROM m_employee e 
				WHERE e.is_active = 1 AND ((e.approved_at <= '".$params['period']."' AND e.status_approval = 3)) ".$where;
		$result = $this->db->query($query)->row();
		return $result->total;
	}

	public function chart_position($params = array()){
		$where = '';
		if(isset($params['branch_id'])){
			$where .= "AND e.branch_id = '".$params['branch_id']."' ";
		}
		if(isset($params['company_id'])){
			$where .= "AND e.company_id = '".$params['company_id']."' ";
		}

		$query = "SELECT COUNT(e.id) AS total, p.name AS position FROM m_employee e
				JOIN m_position p ON e.position_id = p.id 
				WHERE e.is_active = 1 AND e.status_approval = 3 AND e.status_nonjob = 0 ".$where." GROUP BY e.position_id  ORDER BY total DESC LIMIT 50 ";
		$result = $this->db->query($query)->result();
		return $result;

	}

	public function chart_site($params = array()){
		$where = '';
		if(isset($params['branch_id'])){
			$where .= "AND e.branch_id = '".$params['branch_id']."' ";
		}
		if(isset($params['company_id'])){
			$where .= "AND e.company_id = '".$params['company_id']."' ";
		}
		$query = "SELECT COUNT(e.id) AS total, s.name, DATE_FORMAT(s.contract_end, '%Y/%m/%d') AS end_contract FROM m_employee e 
			INNER JOIN m_site s ON e.site_id = s.id AND s.contract_status = 'Aktif' 
			WHERE e.is_active = 1 AND e.status_approval = 3 AND e.status_nonjob = 0 ".$where." GROUP BY e.site_id";

		$result = $this->db->query($query)->result();
		return $result;
	}

	public function count_range_salary($params = array()){
		$where = '';
		if(isset($params['branch_id'])){
			$where .= "AND e.branch_id = '".$params['branch_id']."' ";
		}
		if(isset($params['company_id'])){
			$where .= "AND e.company_id = '".$params['company_id']."' ";
		}
		if(isset($params['low'])){
			$where .= "AND (p.basic_salary >= '".$params['low']."' AND p.basic_salary < '".$params['high']."')";
		}

		$query = "SELECT COUNT(e.id) AS total FROM m_employee e 
			LEFT JOIN t_payroll p ON e.id = p.employee_id AND p.payment = 'Selesai' 
			WHERE e.is_active = 1 AND e.status_approval = 3 ".$where." GROUP BY e.site_id";
		$result = $this->db->query($query)->row();
		if($result){
			return $result->total;	
		}
		return 0;
	}

	public function gets_payroll($params = array()){
		$where = '';
		if(isset($params['site_id'])){
			$where .= "AND e.site_id = '".$params['site_id']."' ";
		}
		if(isset($params['not_site_id'])){
			$where .= "AND e.site_id != '".$params['not_site_id']."' ";
		}
		if(isset($params['employee_branch'])){
			if($params['employee_branch'] != ''){
				$where .= "AND e.branch_id = '".$params['employee_branch']."' ";
			}
		}
		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$where .= "AND s.branch_id = '".$params['branch_id']."' ";
			}
		}
		if(isset($params['company_id'])){
			$where .= "AND e.company_id = '".$params['company_id']."' ";
		}
		if(isset($params['period'])){
			$where .= "AND DATE_FORMAT(p.periode_end, '%Y-%m') = '".$params['period']."' ";
		}
		

		$query = "SELECT IFNULL(SUM(p.salary), 0) AS payroll,  IFNULL(COUNT(p.employee_id), 0) AS employee FROM t_payroll p 
					JOIN m_employee e ON e.id = p.employee_id
					LEFT JOIN m_site s ON s.id = e.site_id
					WHERE p.payment = 'Selesai' AND p.is_active = 1 ".$where;
		$result = $this->db->query($query)->row();
		return $result;
	}

	public function count_payroll($params = array()){
		$where = '';
		if(isset($params['site_id'])){
			$where .= "AND e.site_id = '".$params['site_id']."' ";
		}
		if(isset($params['not_site_id'])){
			$where .= "AND e.site_id != '".$params['not_site_id']."' ";
		}
		if(isset($params['employee_branch'])){
			if($params['employee_branch'] != ''){
				$where .= "AND e.branch_id = '".$params['employee_branch']."' ";
			}
		}
		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$where .= "AND s.branch_id = '".$params['branch_id']."' ";
			}
		}
		if(isset($params['company_id'])){
			$where .= "AND e.company_id = '".$params['company_id']."' ";
		}
		if(isset($params['period'])){
			$where .= "AND DATE_FORMAT(p.periode_end, '%Y-%m') = '".$params['period']."' ";
		}
		

		$query = "SELECT IFNULL(SUM(p.salary), 0) AS total FROM t_payroll p 
					JOIN m_employee e ON e.id = p.employee_id
					LEFT JOIN m_site s ON s.id = e.site_id
					WHERE p.payment = 'Selesai' AND p.is_active = 1 ".$where;
		$result = $this->db->query($query)->row();
		if($result){
			return $result->total;	
		}
		return 0;
	}

	public function employee_benefit($params = array()){
		$where = '';
		if(isset($params['not_site_id'])){
			$where .= "AND e.site_id != '".$params['not_site_id']."' ";
		}
		if(isset($params['site_id'])){
			$where .= "AND e.site_id = '".$params['site_id']."' ";
		}
		if(isset($params['employee_branch'])){
			if($params['employee_branch'] != ''){
				$where .= "AND e.branch_id = '".$params['employee_branch']."' ";
			}
		}
		if(isset($params['company_branch'])){
			if($params['company_branch'] != ''){
				$where .= "AND s.branch_id = '".$params['company_branch']."' ";
			}
		}
		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$where .= "AND s.branch_id = '".$params['branch_id']."' ";
			}
		}
		if(isset($params['company_id'])){
			$where .= "AND e.company_id = '".$params['company_id']."' ";
		}
		if(isset($params['period'])){
			$where .= "AND DATE_FORMAT(b.periode, '%Y-%m') = '".$params['period']."' ";
		}
		

		$total = 0;
		$query_employee = "SELECT COUNT(*) AS total FROM (SELECT COUNT(employee_id) FROM (SELECT b.employee_id FROM t_benefit_labor b 
			JOIN m_employee e ON e.id = b.employee_id
			LEFT JOIN m_site s ON s.id = e.site_id
			WHERE b.is_active = 1 ".$where." 
			UNION ALL
			SELECT b.employee_id FROM t_benefit_health b 
			JOIN m_employee e ON e.id = b.employee_id
			LEFT JOIN m_site s ON s.id = e.site_id
			WHERE b.is_active = 1 ".$where." ) benefit  GROUP BY employee_id) count ";
		$result_employee = $this->db->query($query_employee)->row();
		if($result_employee){
			$total = $total + $result_employee->total;	
		}
		
		return $total;
	}

	public function count_benefit($params = array()){
		$where = '';
		if(isset($params['not_site_id'])){
			$where .= "AND e.site_id != '".$params['not_site_id']."' ";
		}
		if(isset($params['site_id'])){
			$where .= "AND e.site_id = '".$params['site_id']."' ";
		}
		if(isset($params['employee_branch'])){
			if($params['employee_branch'] != ''){
				$where .= "AND e.branch_id = '".$params['employee_branch']."' ";
			}
		}
		if(isset($params['company_branch'])){
			if($params['company_branch'] != ''){
				$where .= "AND s.branch_id = '".$params['company_branch']."' ";
			}
		}
		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$where .= "AND s.branch_id = '".$params['branch_id']."' ";
			}
		}
		if(isset($params['company_id'])){
			$where .= "AND e.company_id = '".$params['company_id']."' ";
		}
		if(isset($params['period'])){
			$where .= "AND DATE_FORMAT(b.periode, '%Y-%m') = '".$params['period']."' ";
		}
		

		$total = 0;
		$query_labor = "SELECT SUM(b.employee_payment + b.company_payment) AS total FROM t_benefit_labor b 
					JOIN m_employee e ON e.id = b.employee_id
					LEFT JOIN m_site s ON s.id = e.site_id
					WHERE b.is_active = 1 ".$where;
		$result_labor = $this->db->query($query_labor)->row();
		if($result_labor){
			$total = $total + $result_labor->total;	
		}
		$query_health = "SELECT SUM(b.employee_payment + b.company_payment) AS total FROM t_benefit_health b 
					JOIN m_employee e ON e.id = b.employee_id
					LEFT JOIN m_site s ON s.id = e.site_id
					WHERE b.is_active = 1 ".$where;
		$result_health = $this->db->query($query_health)->row();

		if($result_health){
			$total = $total + $result_health->total;	
		}
		
		return $total;
	}

	public function count_health($params = array()){
		$where = '';
		if(isset($params['not_site_id'])){
			$where .= "AND e.site_id != '".$params['not_site_id']."' ";
		}
		if(isset($params['site_id'])){
			$where .= "AND e.site_id = '".$params['site_id']."' ";
		}
		if(isset($params['branch_id'])){
			$where .= "AND e.branch_id = '".$params['branch_id']."' ";
		}
		if(isset($params['company_id'])){
			$where .= "AND e.company_id = '".$params['company_id']."' ";
		}
		if(isset($params['period'])){
			$where .= "AND DATE_FORMAT(b.periode, '%Y-%m') = '".$params['period']."' ";
		}
		
		$result_health = "SELECT SUM(b.employee_payment) AS employee_payment, SUM(b.company_payment) AS company_payment FROM t_benefit_health b 
					JOIN m_employee e ON e.id = b.employee_id
					WHERE b.is_active = 1 ".$where;
		$result_health = $this->db->query($result_health)->row();
		return $result_health;
	}

	public function count_labor($params = array()){
		$where = '';
		if(isset($params['not_site_id'])){
			$where .= "AND e.site_id != '".$params['not_site_id']."' ";
		}
		if(isset($params['site_id'])){
			$where .= "AND e.site_id = '".$params['site_id']."' ";
		}
		if(isset($params['branch_id'])){
			$where .= "AND e.branch_id = '".$params['branch_id']."' ";
		}
		if(isset($params['company_id'])){
			$where .= "AND e.company_id = '".$params['company_id']."' ";
		}
		if(isset($params['period'])){
			$where .= "AND DATE_FORMAT(b.periode, '%Y-%m') = '".$params['period']."' ";
		}

		$query_labor = "SELECT SUM(b.employee_payment) AS employee_payment, SUM(b.company_payment) AS company_payment FROM t_benefit_labor b 
					JOIN m_employee e ON e.id = b.employee_id
					WHERE b.is_active = 1 ".$where;
		$result_labor = $this->db->query($query_labor)->row();
		return $result_labor;
	}

	public function gets_all_employee($params = array()){
		$query = "SELECT E.employee_number, E.full_name, B.name AS branch_name, S.name AS site_name, P.name AS position, C1.code AS company_code, C2.code AS business_code FROM m_employee E 
INNER JOIN m_branch B ON E.branch_id = B.id
INNER JOIN m_site S ON E.site_id = S.id
INNER JOIN m_position P ON E.position_id = P.id
INNER JOIN m_company C1 ON E.company_id = C1.id
INNER JOIN m_company C2 ON S.company_id = C2.id
WHERE E.status_approval = 3 AND E.status_nonjob = 0 AND E.is_active = 1 AND S.id > 2 ORDER BY site_name, full_name;";
		$result = $this->db->query($query)->result();
		return $result;
	}
}
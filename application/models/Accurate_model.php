<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accurate_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_accurate		= 'm_accurate_configuration';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_accurate.' A');
				
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
	
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_accurate.' A');

		$result = $this->db->get()->row();
		return $result;
	}
	
	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_accurate, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$this->db->insert($this->table_accurate, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
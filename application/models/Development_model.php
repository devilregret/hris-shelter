<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Development_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_user       		= 'm_user';
		$this->table_city       		= 'm_city';
		$this->table_province   		= 'm_province';
		$this->table_position   		= 'm_position';
		$this->table_development		= 't_development';
		$this->table_development_detail	= 't_development_detail';
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		if(isset($params['name'])){
			$this->db->where('A.name', $params['name']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_development.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function preview($params = array())
	{
		$this->db->from($this->table_development.' A');
		$this->db->join($this->table_position.' B', 'A.position_id = B.id', 'left');
		$this->db->join($this->table_city.' C', 'A.city_id = C.id', 'left');
		
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_development.' A');
		$this->db->join($this->table_position.' B', 'A.position_id = B.id', 'left');
		$this->db->join($this->table_city.' C', 'A.city_id = C.id', 'left');
		
		$this->db->where('A.is_active', 1);

		if(isset($params['position_id']) && isset($params['employee_id'])){
			$this->db->where("(A.position_id ='".$params['position_id']."' OR A.created_by = '".$params['employee_id']."')");
		}
		if(isset($params['developer_id'])){
			$this->db->where('A.developer_id', $params['developer_id']);
		}
		if(isset($params['title'])){
			$this->db->like('A.title', $params['title']);
		}
		if(isset($params['description'])){
			$this->db->like('A.description', $params['description']);
		}
		if(isset($params['status'])){
			$this->db->like('A.status', $params['status']);
		}
		if(isset($params['target'])){
			$this->db->like('DATE_FORMAT(A.target, "%Y-%m-%d")', $params['target']);
		}
		if(isset($params['cost'])){
			$this->db->like('A.cost', $params['cost']);
		}
		if(isset($params['priority'])){
			$this->db->like('A.priority', $params['priority']);
		}
		if(isset($params['created_at'])){
			$this->db->like('DATE_FORMAT(A.created_at, "%Y-%m-%d")', $params['created_at']);
		}
		if(isset($params['updated_at'])){
			$this->db->like('DATE_FORMAT(A.updated_at, "%Y-%m-%d")', $params['updated_at']);
		}
		if(isset($params['position'])){
			$this->db->like('B.name', $params['position']);
		}
		if(isset($params['city'])){
			$this->db->like('C.name', $params['city']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'target'){
				$params['orderby'] = 'A.target';
			}
			if($params['orderby'] == 'created_at'){
				$params['orderby'] = 'A.created_at';
			}
			if($params['orderby'] == 'updated_at'){
				$params['orderby'] = 'A.updated_at';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_development, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_development, $params);
			$id = $this->db->insert_id();
		}
		
		return $id;
	}

	public function gets_detail($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_development_detail.' A');
		$this->db->join($this->table_position.' B', 'A.position_id = B.id', 'left');
		$this->db->join($this->table_city.' C', 'A.city_id = C.id', 'left');
		
		$this->db->where('A.is_active', 1);
		if(isset($params['development_id'])){
			$this->db->where('A.development_id', $params['development_id']);
		}
		if(isset($params['description'])){
			$this->db->like('A.description', $params['description']);
		}
		if(isset($params['status'])){
			$this->db->like('A.status', $params['status']);
		}
		if(isset($params['target'])){
			$this->db->like('DATE_FORMAT(A.target, "%Y-%m-%d")', $params['target']);
		}
		if(isset($params['cost'])){
			$this->db->like('A.cost', $params['cost']);
		}
		if(isset($params['priority'])){
			$this->db->like('A.priority', $params['priority']);
		}
		if(isset($params['created_at'])){
			$this->db->like('DATE_FORMAT(A.created_at, "%Y-%m-%d")', $params['created_at']);
		}
		if(isset($params['updated_at'])){
			$this->db->like('DATE_FORMAT(A.updated_at, "%Y-%m-%d")', $params['updated_at']);
		}
		if(isset($params['position'])){
			$this->db->like('B.name', $params['position']);
		}
		if(isset($params['city'])){
			$this->db->like('C.name', $params['city']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'target'){
				$params['orderby'] = 'A.target';
			}
			if($params['orderby'] == 'created_at'){
				$params['orderby'] = 'A.created_at';
			}
			if($params['orderby'] == 'updated_at'){
				$params['orderby'] = 'A.updated_at';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function save_detail($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_development_detail, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_development_detail, $params);
			$id = $this->db->insert_id();
		}
		
		return $id;
	}

	// public function get_detail($params = array())
	// {
	// 	$this->db->where('A.is_active', 1);

	// 	if(isset($params['id'])){
	// 		$this->db->where('A.id', $params['id']);
	// 	}
		
	// 	if(isset($params['name'])){
	// 		$this->db->where('A.name', $params['name']);
	// 	}
		
	// 	if(isset($params['columns'])){
	// 		$this->db->select($params['columns']);
	// 	}else{
	// 		$this->db->select('A.*');
	// 	}

	// 	$this->db->from($this->table_city.' A');
	// 	$this->db->join($this->table_province.' B', 'A.province_id = B.id');

	// 	$result = $this->db->get()->row();
	// 	return $result;
	// }

	// public function preview($params = array())
	// {
	// 	$this->db->where('A.id', $params['id']);
	// 	$this->db->where('A.is_active', 1);
	// 	$this->db->from($this->table_city.' A');
	// 	$this->db->join($this->table_province.' B', 'A.province_id = B.id');
	// 	$this->db->join($this->table_user.' C', 'A.created_by = C.id');
	// 	$this->db->join($this->table_user.' D', 'A.updated_by = D.id');

	// 	$this->db->select('A.*, B.name AS province_name, C.full_name AS creator, D.full_name AS updater');

	// 	$result = $this->db->get()->row();
	// 	return $result;
	// }

	// public function list($params = array())
	// {
	// 	$this->db->where('A.is_active', 1);
	// 	$this->db->from($this->table_city.' A');
	// 	$this->db->join($this->table_province.' B', 'A.province_id = B.id');

	// 	$this->db->select('A.id, A.name, B.name AS province_name');
	// 	$result = $this->db->get()->result();
	// 	return $result;
	// }
	
}
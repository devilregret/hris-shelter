<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formula_site_model extends CI_Model{

	public function __construct()
	{
		$this->table_formula	= 't_formula_site';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_formula.' A');
		
		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.no', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$this->db->insert($this->table_formula, $params);
		$id = $this->db->insert_id();
		return $id;
	}

	public function delete($params = array())
	{
		$id = $this->db->delete($this->table_formula, array('site_id' => $params['site_id']));
		return $id;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_report_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_upload_report		= 'm_upload_report';
	}

	public function get($params = array())
	{
		
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->from($this->table_upload_report.' A');
		$result = $this->db->get()->row();
		return $result;
	}
	
	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_upload_report, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_upload_report, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_model extends CI_Model{
	private $table_sales;
	public function __construct()
	{
		parent::__construct();
		$this->table_sales      = 't_site';

	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_sales.' A');

		if(isset($params['reporting_date'])){
			$this->db->like('A.reporting_date', $params['reporting_date']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.site', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}
}
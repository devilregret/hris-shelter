<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Struktur_jabatan_model extends CI_Model{
	// private $table_user;
	// private $table_position;
	// private $table_company;

	public function __construct()
	{
		parent::__construct();
		$this->table_user		= 'm_user';
		$this->table_employee_level	= 'm_employee_level';
		$this->table_employee	= 'm_employee';
		$this->table_company	= 'm_company';
		$this->table_position	= 'm_position';
		$this->table_struktur_jabatan	= 'm_struktur_jabatan';
		$this->table_branch	= 'm_branch';
	}

	function getMasterLevel()
    {
		$this->db->select('A.id, A.level_name as name');
		$this->db->from($this->table_employee_level.' A');
		$this->db->where('A.is_active', 1);
		
		return $this->db->get()->result();
    }

	function getMasterParent()
    {
		$this->db->select('A.id, A.struktur_name as name');
		$this->db->from($this->table_struktur_jabatan.' A');
		$this->db->where('A.is_active', 1);
		$this->db->order_by('A.struktur_name', 'ASC');
		
		return $this->db->get()->result();
    }

	public function getStruktur($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_struktur_jabatan.' A');
		$this->db->join($this->table_employee_level.' B', 'A.level_id = B.id', 'left');
		$this->db->join($this->table_struktur_jabatan.' C', 'C.id = A.parent_id', 'left');
		$this->db->join($this->table_employee_level.' D', 'C.level_id = D.id', 'left');
		$this->db->join($this->table_branch.' E', 'E.id = A.branch_id', 'left');
		$this->db->join($this->table_branch.' F', 'C.branch_id = F.id', 'left');
		$this->db->where('A.is_active = ', 1);
		if ($return_count){
			return $this->db->count_all_results();
		}

		if(isset($params['columns'])){
		// 	print_r($params['columns']);
		// die();
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*, B.level_name AS child_name, D.level_name AS parent_name');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function getStrukturByParent($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_struktur_jabatan.' A');
		$this->db->join($this->table_employee.' B', 'A.id = B.struktur_id', 'left');
		$this->db->join($this->table_position.' C', 'B.position_id = C.id', 'left');
		$this->db->join($this->table_employee_level.' D', 'A.level_id = D.id', 'left');

		$this->db->select('A.id, A.struktur_name as name, C.name as posisi_name, B.full_name, (select count(id) from m_struktur_jabatan where parent_id = A.id) as children, D.level_name');
		$this->db->where('A.parent_id = ', $params['parent_id']);
		$this->db->where('A.is_active = ', 1);

		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['is_name'])){
			$this->db->where('A.name', $params['is_name']);
		}

		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->from($this->table_struktur_jabatan.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_struktur_jabatan, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_struktur_jabatan, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_position.' A');
		$this->db->join($this->table_company.' B', 'A.company_id = B.id', 'left');

		$this->db->where('A.is_active', 1);

		if(isset($params['list_id'])){
			$this->db->where_in('A.id', $params['list_id']);
		}

		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}

		if(isset($params['description'])){
			$this->db->like('A.description', $params['description']);
		}

		if(isset($params['company_name'])){
			if($params['company_name'] != ''){
				$this->db->like('B.name', $params['company_name']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*, B.code AS company_code, B.name AS company_name');
		}

		$result = $this->db->get()->result();
		return $result;
	}

}
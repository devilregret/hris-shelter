<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kraepelin_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_kraepelin				= 't_kraepelin';
		$this->table_kraepelin_d			= 't_kraepelin_d';
		$this->table_persiapan1				= 't_kraepelin_persiapan_1';
		$this->table_persiapan2				= 't_kraepelin_persiapan_2';
	}

	public function save($params = array()){
		$params['is_aktif']  = 1;
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_kraepelin, $params);
		$id = $this->db->insert_id();
		return $id;
	}

	public function delete($params = array())
	{
		$id = false;

		$params['is_aktif']  = 0;
		$params['deleted_at'] = date('Y-m-d H:i:s');
		$params['deleted_by'] = $this->session->userdata('user_id');
		$this->db->where('id', $params['id']);
		$this->db->update($this->table_kraepelin, $params);
		$id = $params['id'];

		return $id;
	}


	public function savePersiapan1($params = array()){
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_persiapan1, $params);
		$id = $this->db->insert_id();
		return $id;
	}

	public function savePersiapan2($params = array()){
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_persiapan2, $params);
		$id = $this->db->insert_id();
		return $id;
	}

	public function saveD($params = array()){
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_kraepelin_d, $params);
		$id = $this->db->insert_id();
		return $id;
	}


	public function updateHasil($params = array())
	{
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->where('id', $params['id']);
		$this->db->update($this->table_kraepelin, $params);
		$id = $params['id'];
		return $id;
	}

	public function count_kraepelin_vacancy ($employeeId,$vacancyId){
		$query = "SELECT id FROM t_kraepelin WHERE deleted_at is null and is_aktif = 1 and m_employee_id = ".$employeeId." and m_vacancy_id = ".$vacancyId;
		return $this->db->query($query)->result();
	}

	public function get_data_kraepelin($id){
		$q = "SELECT * from t_kraepelin WHERE deleted_at is null and id = ".$id.";";
		return $this->db->query($q)->result();
	}

	public function get_data_kraepelin_d($id){
		$q = "SELECT * from t_kraepelin_d WHERE deleted_at is null and t_kraepelin_id = ".$id.";";
		return $this->db->query($q)->result();
	}

	public function convert_nilai($grade,$tipe,$nilai){
		$q = "SELECT kriteria from m_kraepelin_konversi_nilai WHERE deleted_at is null and grade='".$grade."' and tipe='".$tipe."' and '".$nilai."' BETWEEN nilai_min and nilai_max";
		return $this->db->query($q)->result()[0]->kriteria;
	}

}
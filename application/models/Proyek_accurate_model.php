<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyek_accurate_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_proyek_accurate 	= 'm_proyek_accurate';
		$this->table_site 				= 'm_site';
		$this->table_client 			= 'm_client';
	}

	public function gets_site($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_site.' A');
		$this->db->where('A.is_active', 1);
		$this->db->order_by('A.id', 'DESC');
		$this->db->where('A.proyek_id != "" OR A.proyek_id != "0"');

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		$result = $this->db->get()->result();
		return $result;
	}

	public function gets_report($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_proyek_accurate.' A');
		$this->db->join($this->table_client.' B', 'A.customer_id = B.accurate_customer_id', 'inner');
		$this->db->join($this->table_site.' C', 'B.id = C.client_id AND C.is_active = 1', 'inner');
		$this->db->where('A.customer_id > ', 0);
		// $this->db->where('A.is_active', 1);
		
		if(isset($params['list_proyek'])){
			$this->db->where_in('A.id ', $params['list_proyek']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'DESC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		if(isset($params['group_by'])){
			$this->db->group_by($params['group_by']);
		}
		$result = $this->db->get()->result();

		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_proyek_accurate.' A');
		$this->db->where('A.is_active', 1);
		
		if(isset($params['customer_id'])){
			$this->db->where('A.customer_id', $params['customer_id']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'DESC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function get_customer($params = array())
	{
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		if(isset($params['customer_id'])){
			$this->db->where('A.customer_id', $params['customer_id']);
		}
		if(isset($params['nomor_proyek'])){
			$this->db->where('A.nomor_proyek', $params['nomor_proyek']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_proyek_accurate.' A');
		$this->db->join($this->table_client.' B', 'A.customer_id = B.accurate_customer_id', 'inner');

		$result = $this->db->get()->row();
		return $result;
	}

	public function get($params = array())
	{
		// $this->db->where('A.is_active', 1);
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		if(isset($params['in_id'])){
			$this->db->where_in('A.id', $params['in_id']);
		}
		if(isset($params['customer_id'])){
			$this->db->where('A.customer_id', $params['customer_id']);
		}
		if(isset($params['db_id'])){
			$this->db->where('A.db_id', $params['db_id']);
		}
		if(isset($params['nomor_proyek'])){
			$this->db->where('A.nomor_proyek', $params['nomor_proyek']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_proyek_accurate.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['update_terakhir'] = date('Y-m-d H:i:s');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_proyek_accurate, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  		= 1;
			$params['update_terakhir'] 	= date('Y-m-d H:i:s');
			$this->db->insert($this->table_proyek_accurate, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}

	public function replace($params = array())
	{
		$params['update_terakhir'] = date('Y-m-d H:i:s');
		$this->db->replace($this->table_proyek_accurate, $params);
		return $this->db->insert_id();
	}

	public function nonactive_all($params = array()){
		$params['is_active']  = 0;
		$this->db->update($this->table_proyek_accurate, $params);
	}
}
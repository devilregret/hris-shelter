<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_user		= 'm_user';
		$this->table_employee	= 'm_employee';
		$this->table_site		= 'm_site';
		$this->table_company	= 'm_company';
		$this->table_position	= 'm_position';
		$this->table_approval	= 't_approval_history';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_approval.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		$this->db->join($this->table_company.' C', 'A.company_id = C.id', 'left');
		$this->db->join($this->table_site.' D', 'A.site_id = D.id', 'left');
		$this->db->join($this->table_user.' E', 'A.created_by = E.id', 'left');
		$this->db->join($this->table_position.' F', 'A.position_id = F.id', 'left');
		
		$this->db->where('A.is_active', 1);

		if ($return_count){
			$this->db->select('A.id');
			return $this->db->count_all_results();
		}
		
		if(isset($params['status_approval'])){
			$this->db->where('A.status_approval', $params['status_approval']);    
		}
		
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);    
		}
		if(isset($params['created_at'])){
			$this->db->like("DATE_FORMAT(A.created_at, '%d/%m/%Y')", $params['created_at']);
		}
		if(isset($params['note'])){
			if($params['note'] != ''){
				$this->db->like('A.note', $params['note']);
			}
		}
		if(isset($params['full_name'])){
			if($params['full_name'] != ''){
				$this->db->like('B.full_name', $params['full_name']);
			}
		}
		if(isset($params['id_card'])){
			if($params['id_card'] != ''){
				$this->db->like('B.id_card', $params['id_card']);
			}
		}
		if(isset($params['education_majors'])){
			if($params['education_majors'] != ''){
				$this->db->like('B.education_majors', $params['education_majors']);
			}
		}
		if(isset($params['site_name'])){
			if($params['site_name'] != ''){
				$this->db->like('D.name', $params['site_name']);
			}
		}
		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('D.branch_id', $params['branch_id']);
			}
		}
		if(isset($params['position'])){
			if($params['position'] != ''){
				$this->db->like('F.name', $params['position']);
			}
		}
		if(isset($params['age'])){
			if($params['age'] != ''){
				$this->db->like('FLOOR(DATEDIFF(CURRENT_DATE, B.date_birth)/365.25)', $params['age']);
			}
		}
		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'created_at'){
				$params['orderby'] = 'A.created_at';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.created_at', 'DESC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);
	
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['status_approval'])){
			$this->db->where('A.status_approval', $params['status_approval']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_approval.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_approval, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_approval, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}

	public function force_save($params = array()){
			$params['is_active']  = 1;
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_approval, $params);
			$id = $this->db->insert_id();
	}
}
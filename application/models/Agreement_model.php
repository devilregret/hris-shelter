<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agreement_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_user       = 'm_user';
		$this->table_agreement  = 'm_cooperation_agreement';
		$this->table_city   	= 'm_city';
		$this->table_province   = 'm_province';
		$this->table_branch    	= 'm_branch';

	}

	public function preview($params = array())
	{
		$this->db->where('A.id', $params['id']);
		$this->db->where('A.is_active', 1);
		$this->db->from($this->table_agreement.' A');
		$this->db->join($this->table_city.' B', 'A.city_id = B.id', 'left');
		$this->db->join($this->table_province.' C', 'B.province_id = C.id', 'left');
		$this->db->join($this->table_branch.' D', 'C.branch_id = D.id', 'left');
		$this->db->join($this->table_user.' E', 'A.created_by = E.id');
		$this->db->join($this->table_user.' F', 'A.updated_by = F.id');

		$this->db->select('A.*, B.name AS city_name, C.name AS province_name, D.name AS branch_name, E.full_name AS creator, F.full_name AS updater');

		$result = $this->db->get()->row();
		return $result;
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_agreement.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_agreement.' A');
		$this->db->join($this->table_city.' B', 'A.city_id = B.id', 'left');
		$this->db->join($this->table_province.' C', 'B.province_id = C.id', 'left');
		$this->db->join($this->table_branch.' D', 'C.branch_id = D.id', 'left');
		
		$this->db->where('A.is_active', 1);

		if(isset($params['branch_name'])){
			$this->db->like('D.name', $params['branch_name']);
		}
		if(isset($params['branch_id'])){
			$this->db->like('C.branch_id', $params['branch_id']);
		}
		if(isset($params['province_name'])){
			$this->db->like('C.name', $params['province_name']);
		}
		if(isset($params['city_name'])){
			$this->db->like('B.name', $params['city_name']);
		}
		if(isset($params['start_contract'])){
			$this->db->like('DATE_FORMAT(A.start_contract, "%d/%m/%Y")', $params['start_contract']);
		}
		if(isset($params['end_contract'])){
			$this->db->like('DATE_FORMAT(A.end_contract, "%d/%m/%Y")', $params['end_contract']);
		}
		if(isset($params['agency_name'])){
			$this->db->like('A.agency_name', $params['agency_name']);
		}
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'start_contract'){
				$params['orderby'] = 'A.start_contract';
			}
			if($params['orderby'] == 'end_contract'){
				$params['orderby'] = 'A.end_contract';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'DESC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_agreement, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_agreement, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
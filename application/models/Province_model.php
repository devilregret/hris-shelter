<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Province_model extends CI_Model{
	private $table_user;
	private $table_province;
	private $table_branch;

	public function __construct()
	{
		parent::__construct();
		$this->table_user       = 'm_user';
		$this->table_province   = 'm_province';
		$this->table_branch   	= 'm_branch';
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_province.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function preview($params = array())
	{
		$this->db->where('A.id', $params['id']);
		$this->db->where('A.is_active', 1);
		$this->db->from($this->table_province.' A');
		$this->db->from($this->table_branch.' B', 'A.branch_id = B.id');
		$this->db->join($this->table_user.' C', 'A.created_by = C.id');
		$this->db->join($this->table_user.' D', 'A.updated_by = D.id');

		$this->db->select('A.*, B.name AS branch_name, C.full_name AS creator, D.full_name AS updater');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_province.' A');
		$this->db->join($this->table_branch.' B', 'A.branch_id = B.id');
		
		$this->db->where('A.is_active', 1);

		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}
		
		if(isset($params['branch_name'])){
			$this->db->like('B.name', $params['branch_name']);
		}
		
		if(isset($params['branch_id'])){
			$this->db->where('B.id', $params['branch_id']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.name', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*, B.name AS branch_name');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_province, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_province, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
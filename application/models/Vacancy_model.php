<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vacancy_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_user       = 'm_user';
		$this->table_vacancy   	= 'm_vacancy';
		$this->table_question   = 'm_vacancy_question';
		$this->table_province   = 'm_province';
		$this->table_branch   	= 'm_branch';
		$this->table_company   	= 'm_company';
		$this->table_site   	= 'm_site';
		$this->table_position   = 'm_position';
		$this->table_applicant	= 't_applicant';
		$this->table_employee	= 'm_employee';
		$this->table_city		= 'm_city';
		$this->table_skill		= 'm_employee_skill';
		$this->table_approval	= 't_approval_history';

	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		if(isset($params['title'])){
			$this->db->like('A.title', $params['title']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_vacancy.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function preview($params = array())
	{
		$this->db->where('A.id', $params['id']);
		$this->db->where('A.is_active', 1);
		$this->db->from($this->table_vacancy.' A');
		$this->db->join($this->table_province.' B', 'A.province_id = B.id', 'left');
		$this->db->join($this->table_user.' C', 'A.created_by = C.id');
		$this->db->join($this->table_user.' D', 'A.updated_by = D.id');
		$this->db->join($this->table_branch.' E', 'B.branch_id = E.id', 'left');
		$this->db->join($this->table_company.' F', 'A.company_id = F.id', 'left');
		$this->db->join($this->table_site.' G', 'A.site_id = G.id', 'left');
		$this->db->join($this->table_position.' H', 'A.position_id = H.id', 'left');

		$this->db->select('A.*, B.name AS province_name, E.name AS branch_name, C.full_name AS creator, D.full_name AS updater, F.name AS company_name, G.name AS site_name, H.name AS position_name');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_vacancy.' A');
		$this->db->join($this->table_province.' B', 'A.province_id = B.id', 'left');
		$this->db->join($this->table_branch.' C', 'B.branch_id = C.id', 'left');
		$this->db->join($this->table_company.' D', 'A.company_id = D.id', 'left');
		$this->db->join($this->table_site.' E', 'A.site_id = E.id', 'left');
		$this->db->join($this->table_position.' F', 'A.position_id = F.id', 'left');
		
		$this->db->where('A.is_active', 1);

		if(isset($params['session_branch'])){
			$this->db->where('B.branch_id', $params['session_branch']);
		}

		if(isset($params['province_id'])){
			if($params['province_id'] != ''){
				$this->db->where('A.province_id', $params['province_id']);
			}
		}

		if(isset($params['search'])){
			if($params['search'] != ''){
				$this->db->where('(A.title LIKE "%'.$params['search'].'%" OR A.content LIKE "%'.$params['search'].'%" OR F.name LIKE "%'.$params['search'].'%")');
			}
		}
		
		if(isset($params['active'])){
			$this->db->where(" NOW() BETWEEN A.start_date AND A.end_date ");
		}

		if(isset($params['start_date'])){
			$this->db->like('DATE_FORMAT(A.start_date, "%d/%m/%Y")', $params['start_date']);
		}

		if(isset($params['end_date'])){
			$this->db->like('DATE_FORMAT(A.end_date, "%d/%m/%Y")', $params['end_date']);
		}

		if(isset($params['title'])){
			$this->db->like('A.title', $params['title']);
		}
		
		if(isset($params['province_name'])){
			if($params['province_name'] != ''){
				$this->db->like('B.name', $params['province_name']);
			}
		}
		
		if(isset($params['branch_name'])){
			if($params['branch_name'] != ''){
				$this->db->like('C.name', $params['branch_name']);
			}
		}
		
		if(isset($params['company_name'])){
			if($params['company_name'] != ''){
				$this->db->like('D.name', $params['company_name']);
			}
		}

		if(isset($params['site_name'])){
			if($params['site_name'] != ''){
				$this->db->like('E.name', $params['site_name']);
			}
		}

		if(isset($params['position_name'])){
			if($params['position_name'] != ''){
				$this->db->like('F.name', $params['position_name']);
			}
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'start_date'){
				$params['orderby'] = 'A.start_date';
			}
			if($params['orderby'] == 'end_date'){
				$params['orderby'] = 'A.end_date';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.end_date ', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*, B.name AS province_name, C.name AS branch_name');
		}
		
		$result = $this->db->get()->result();
		// print_prev($this->db->last_query());
		// die()
		return $result;
	}

	public function apply_gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_applicant.' A');
		$this->db->join($this->table_vacancy.' B', 'A.vacancy_id = B.id', 'left');
		$this->db->join($this->table_province.' C', 'B.province_id = C.id', 'left');
		$this->db->join($this->table_branch.' D', 'C.branch_id = D.id', 'left');
				
		$this->db->where('A.is_active', 1);
		if(isset($params['active'])){
			$this->db->where(" NOW() BETWEEN B.start_date AND B.end_date ");
		}

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function applicant_gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_applicant.' C');
		$this->db->join($this->table_employee.' A', 'A.id = C.employee_id', 'left');
		$this->db->join($this->table_city.' B', 'A.city_domisili_id = B.id', 'left');
		$this->db->join($this->table_vacancy.' D', 'C.vacancy_id = D.id', 'left');

		$this->db->group_by('A.id'); 
		$this->db->where('A.is_active', 1);
		
		if(isset($params['vacancy_id'])){
			$this->db->where('C.vacancy_id', $params['vacancy_id']);
		}

		if(isset($params['created_at'])){
			if($params['created_at'] != ''){
				$this->db->like('DATE_FORMAT(A.created_at, "%d/%m/%Y")', $params['created_at']);
			}
		}
		if(isset($params['full_name'])){
			if($params['full_name'] != ''){
				$this->db->like('A.full_name', $params['full_name']);
			}
		}
		if(isset($params['id_card'])){
			if($params['id_card'] != ''){
				$this->db->like('A.id_card', $params['id_card']);
			}
		}
		if(isset($params['age'])){
			if($params['age'] != ''){
				$this->db->like('FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25)', $params['age']);
			}
		}
		if(isset($params['gender'])){
			if($params['gender'] != ''){
				$this->db->like('A.gender', $params['gender']);
			}
		}
		if(isset($params['education_level'])){
			if($params['education_level'] != ''){
				$this->db->like('A.education_level', $params['education_level']);
			}
		}
		if(isset($params['education_majors'])){
			if($params['education_majors'] != ''){
				$this->db->like('A.education_majors', $params['education_majors']);
			}
		}
		if(isset($params['phone_number'])){
			if($params['phone_number'] != ''){
				$this->db->like('A.phone_number', $params['phone_number']);
			}
		}
		if(isset($params['address_domisili'])){
			if($params['address_domisili'] != ''){
				$this->db->like('A.address_domisili', $params['address_domisili']);
			}
		}
		if(isset($params['city_domisili'])){
			if($params['city_domisili'] != ''){
				$this->db->like('B.name', $params['city_domisili']);
			}
		}

		if ($return_count){
			$this->db->select('A.id');
			$total = $this->db->count_all_results();
			return $total;
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
		    if($params['orderby'] == 'date_registration'){
				$params['orderby'] = 'A.created_at';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.full_name', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function employee_gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_applicant.' G');
		$this->db->join($this->table_employee.' A', 'A.id = G.employee_id', 'left');
		$this->db->join($this->table_city.' B', 'A.city_domisili_id = B.id', 'left');
		$this->db->join($this->table_site.' C', 'A.site_id = C.id', 'left');
		$this->db->join($this->table_company.' D', 'A.company_id = D.id', 'left');
		$this->db->join($this->table_position.' E', 'A.position_id = E.id', 'left');

		$this->db->group_by('A.id'); 
		$this->db->where('A.is_active', 1);

		if(isset($params['vacancy_id'])){
			$this->db->where('G.vacancy_id', $params['vacancy_id']);
		}

		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('C.branch_id', $params['branch_id']);
			}
		}

		if(isset($params['period'])){
			if($params['period']!= ""){
				$this->db->like('A.approved_at', $params['period']);
			}
		}
		
		if(isset($params['status'])){
			$this->db->where('A.status', $params['status']);
		}

		if(isset($params['status_approval_in'])){
			$this->db->where_in('A.status_approval', $params['status_approval_in']);
		}

		if(isset($params['status_approval'])){
			$this->db->where('A.status_approval', $params['status_approval']);
		}
		
		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}

		if(isset($params['id_card'])){
			$this->db->like('A.id_card', $params['id_card']);
		}
		
		if(isset($params['age'])){
			if($params['age'] != ''){
				$this->db->like('FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25)', $params['age']);
			}
		}

		if(isset($params['education_level'])){
			$this->db->like('A.education_level', $params['education_level']);
		}

		if(isset($params['education_majors'])){
			$this->db->like('A.education_majors', $params['education_majors']);
		}
		
		if(isset($params['approved_at'])){
			$this->db->like("DATE_FORMAT(A.approved_at, '%d/%m/%Y')", $params['approved_at']);
		}

		if(isset($params['city_domisili'])){
			if($params['city_domisili'] != ''){
				$this->db->like('B.name', $params['city_domisili']);
			}
		}

		if(isset($params['site_name'])){
			if($params['site_name'] != ''){
				$this->db->like('C.name', $params['site_name']);
			}
		}

		if(isset($params['company_name'])){
			if($params['company_name'] != ''){
			$this->db->like('D.name', $params['company_name']);
			}
		}

		if(isset($params['position'])){
			if($params['position'] != ''){
				$this->db->like('E.name', $params['position']);
			}
		}
		
		if ($return_count){
			$this->db->select('A.id');
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
		    if($params['orderby'] == 'date_registration'){
				$params['orderby'] = 'A.created_at';
			}
			if($params['orderby'] == 'approved_at'){
				$params['orderby'] = 'A.approved_at';
			}

			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.full_name', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_vacancy, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_vacancy, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}

	public function question_gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_question.' A');
		$this->db->where('A.is_active', 1);

		if(isset($params['vacancy_id'])){
			$this->db->where('A.vacancy_id', $params['vacancy_id']);
		}

		if(isset($params['question'])){
			$this->db->like('A.question', $params['question']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function last_status($params = array()){
		$this->db->where('A.is_active', 1);	

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		$this->db->order_by('A.id', 'DESC');
		$this->db->limit(1);
		$this->db->from($this->table_approval.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function question_get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['vacancy_id'])){
			$this->db->where('A.vacancy_id', $params['vacancy_id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_question.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function question_save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_question, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_question, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}

	public function getDataForTes($vacancyId){
		$result = $this->db->query("SELECT *,DATE_FORMAT(now(), '%d/%m/%Y') AS stgl_tes,now() as tgl_tes,date_add(now(),interval m_vacancy.durasi_psikotes minute) as tgl_selesai_psikotes,date_add(now(),interval m_vacancy.durasi_deret_angka minute) as tgl_selesai_deret_angka,date_add(now(),interval m_vacancy.durasi_hitung_cepat minute) as tgl_selesai_hitung_cepat,date_add(now(),interval m_vacancy.durasi_ketelitian minute) as tgl_selesai_ketelitian,durasi_kolom_kraepelin as detik_pindah_soal_kraepelin,durasi_deret_angka,durasi_hitung_cepat,durasi_ketelitian FROM m_vacancy WHERE id =".$vacancyId)->result();
		return $result;
	}
}
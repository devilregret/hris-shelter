<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Struktur_approval_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		// $this->table_user		= 'm_user';
		$this->table_employee	= 'm_employee';
		// $this->table_company	= 'm_company';
		$this->table_position	= 'm_position';
		$this->table_struktur_approval	= 'm_struktur_approval';
		// $this->table_branch	= 'm_branch';
	}

	// function getMasterParent()
    // {
	// 	$this->db->select('A.id, B.name as position_name');
	// 	$this->db->from($this->table_struktur_organisasi.' A');
	// 	$this->db->join($this->table_position.' B', 'A.position_id = B.id', 'left');
	// 	$this->db->where('A.is_active', 1);
	// 	$this->db->order_by('B.name', 'ASC');
		
	// 	return $this->db->get()->result();
    // }

	// public function getStrukturOrganisasi($params = array(), $return_count = FALSE)
	// {
	// 	$this->db->from($this->table_struktur_organisasi.' A');
	// 	$this->db->join($this->table_position.' B', 'A.position_id = B.id', 'left');
	// 	$this->db->join($this->table_employee.' C', 'B.id = C.position_id', 'left');
	// 	$this->db->join($this->table_struktur_organisasi.' D', 'A.parent_id = D.id', 'left');
	// 	$this->db->join($this->table_position.' E', 'D.position_id = E.id', 'left');
		
	// 	$this->db->where('A.is_active = ', 1);
	// 	if ($return_count){
	// 		return $this->db->count_all_results();
	// 	}

	// 	if(isset($params['columns'])){
	// 		$this->db->select($params['columns']);
	// 	}else{
	// 		$this->db->select('A.*, B.level_name AS child_name, D.level_name AS parent_name');
	// 	}

	// 	$result = $this->db->get()->result();
	// 	return $result;
	// }

	// public function getStrukturByParent($params = array(), $return_count = FALSE)
	// {
	// 	$this->db->from($this->table_struktur_organisasi.' A');
	// 	$this->db->join($this->table_position.' B', 'A.position_id = B.id', 'left');
	// 	$this->db->join($this->table_employee.' C', 'B.id = C.position_id', 'left');

	// 	$this->db->select('A.id, B.name as position_name, C.full_name, A.parent_id, (select count(id) from m_struktur_organisasi where parent_id =  1) as children');
	// 	$this->db->where('A.parent_id = ', $params['parent_id']);
	// 	$this->db->where('A.is_active = ', 1);

	// 	$result = $this->db->get()->result();
	// 	return $result;
	// }

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		$this->db->from($this->table_struktur_approval.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_struktur_approval, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_struktur_approval, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_struktur_approval.' A');
		$this->db->join($this->table_position.' B', 'A.position_id = B.id', 'left');
		$this->db->join($this->table_position.' C', 'A.approval_1 = C.id', 'left');
		$this->db->join($this->table_position.' D', 'A.approval_2 = D.id', 'left');
		$this->db->join($this->table_position.' E', 'A.approval_3 = E.id', 'left');

		$this->db->where('A.is_active', 1);

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.position_id', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*, B.code AS company_code, B.name AS company_name');
		}

		$result = $this->db->get()->result();
		return $result;
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->table_user			= 'm_user';
		$this->table_employee		= 'm_employee';
		$this->table_site			= 'm_site';
		$this->table_position		= 'm_position';
		$this->table_attendance		= 't_employee_attendance';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_attendance.' A');
		$this->db->join($this->table_employee.' B', 'B.id = A.employee_id');
		$this->db->join($this->table_position.' C', 'C.id = B.position_id', 'left');
		$this->db->where('A.is_active', '1');
		$this->db->where('B.status', 1);
		$this->db->where('B.status_approval', 3);

		if(isset($params['site_id'])){
			$this->db->where('B.site_id', $params['site_id']);
		}

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['full_name'])){
			$this->db->like('B.full_name', $params['full_name']);
		}

		if(isset($params['id_card'])){
			$this->db->like('B.id_card', $params['id_card']);
		}

		if(isset($params['employee_number'])){
			$this->db->like('B.employee_number', $params['employee_number']);
		}

		if(isset($params['position'])){
			$this->db->like('C.name', $params['position']);
		}

		if(isset($params['date'])){
			$this->db->like("DATE_FORMAT(A.date, '%d/%m/%Y')", $params['date']);
		}

		if(isset($params['date_start'])){
			$this->db->where("A.date >=", $params['date_start']);
		}
		
		if(isset($params['date_end'])){
			$this->db->where("A.date <=", $params['date_end']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'date'){
				$params['orderby'] = 'A.date';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.employee_id', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', $params['is_active']);
		
		}else{
			$this->db->where('A.is_active', 1);
		}

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['date'])){
			$this->db->where('A.date', $params['date']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_attendance.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array()){
		
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->replace($this->table_attendance, $params);
		return $this->db->insert_id();
	}
}
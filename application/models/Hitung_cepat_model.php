<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hitung_cepat_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_user       			= 'm_user';
		$this->table_hitung_cepat			= 't_hitung_cepat';
		$this->table_hitung_cepat_d			= 't_hitung_cepat_d';
	}

	public function getSoal(){
		$result = $this->db->query("SELECT * from m_hitung_cepat WHERE deleted_at is null and jawaban_benar_id is not null ORDER BY no_soal")->result();
		return $result;
	}
	public function getJawaban(){
		$result = $this->db->query("SELECT * from m_hitung_cepat_jawaban WHERE deleted_at is null ORDER BY id")->result();
		return $result;
	}

	public function save($params = array()){
		$params['is_aktif']  = 1;
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_hitung_cepat, $params);
		$id = $this->db->insert_id();
		return $id;
	}

	public function saveD($params = array()){
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_hitung_cepat_d, $params);
		$id = $this->db->insert_id();
		return $id;
	}

	public function delete($params = array())
	{
		$id = false;

		$params['is_aktif']  = 0;
		$params['deleted_at'] = date('Y-m-d H:i:s');
		$params['deleted_by'] = $this->session->userdata('user_id');
		$this->db->where('id', $params['id']);
		$this->db->update($this->table_hitung_cepat, $params);
		$id = $params['id'];

		return $id;
	}

	public function updateHasil($params = array())
	{
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->where('id', $params['id']);
		$this->db->update($this->table_hitung_cepat, $params);
		$id = $params['id'];
		return $id;
	}

	public function count_hitung_cepat_vacancy ($employeeId,$vacancyId){
		$query = "SELECT id FROM t_hitung_cepat WHERE deleted_at is null and is_aktif = 1 and m_employee_id = ".$employeeId." and m_vacancy_id = ".$vacancyId;
		return $this->db->query($query)->result();
	}

	public function get_data_hitung_cepat($deretAngkaId){
		$q = "SELECT * from t_hitung_cepat WHERE deleted_at is null and id = ".$deretAngkaId.";";
		return $this->db->query($q)->result();
	}

	public function get_data_hitung_cepat_d($deretAngkaId){
		$q = "SELECT *,(select no_soal from m_hitung_cepat where id = m_hitung_cepat_id) as no_soal,(select soal from m_hitung_cepat where id = m_hitung_cepat_id) as soal from t_hitung_cepat_d WHERE deleted_at is null and t_hitung_cepat_id = ".$deretAngkaId.";";
		return $this->db->query($q)->result();
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Psikotes_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_user       			= 'm_user';
		$this->table_psikotes				= 't_psikotes';
		$this->table_psikotes_d				= 't_psikotes_d';
		$this->table_psikotes_result_line	= 't_psikotes_result_line';
		$this->t_psikotes_result_disc		= 't_psikotes_result_disc';
		$this->t_psikotes_result			= 't_psikotes_result';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_psikotes.' A');		
		$this->db->where('A.is_aktif', 1);

		if(isset($params['usia'])){
			$this->db->like('A.usia', $params['usia']);
		}
		if(isset($params['jk'])){
			$this->db->like('A.jk', $params['jk']);
		}
		if(isset($params['deskripsi_kepribadian'])){
			$this->db->like('A.deskripsi_kepribadian', $params['deskripsi_kepribadian']);
		}
		if(isset($params['job_match'])){
			$this->db->like('A.job_match', $params['job_match']);
		}
		if(isset($params['tgl_tes'])){
			$this->db->like('DATE_FORMAT(A.tgl_tes, "%d/%m/%Y")', $params['tgl_tes']);
		}
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'DESC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function get_list_nomor_psikotes(){
		$result = $this->db->query("SELECT distinct(nomor) as nomor FROM m_psikotes WHERE deleted_at is null ORDER BY nomor asc")->result();
		return $result;
	}

	public function get_list_master_psikotes(){
		$result = $this->db->query("SELECT * FROM m_psikotes WHERE deleted_at is null ORDER BY nomor asc , poin asc")->result();
		return $result;
	}

	public function getEmployeeInfo($employeeId){
		return $this->db->query("SELECT full_name,TIMESTAMPDIFF(year,date_birth,now()) as umur,if(gender='laki-laki','L',if(gender='perempuan','P','-')) as jk,DATE_FORMAT(now(), '%d/%m/%Y') AS stgl_tes,now() as tgl_tes,education_level,education_majors from m_employee WHERE id = ".$employeeId)->result();
	}

	public function save($params = array())
	{
		$params['is_aktif']  = 1;
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_psikotes, $params);
		$id = $this->db->insert_id();
		return $id;
	}

	public function getMPsikotes($id){
		$query = "select * from m_psikotes where id = '".$id."'";
		return $this->db->query($query)->result()[0];
	}

	public function saveD($params = array()){
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_psikotes_d, $params);
		$id = $this->db->insert_id();
		return $id;
	}

	public function countDisc ($psikotesId,$disc,$pk){
		$query = "SELECT COUNT(*) as jumlah_data FROM t_psikotes_d WHERE deleted_at is null and pk='".$pk."' and disc like '".$disc."' and t_psikotes_id ='".$psikotesId."'";
		return $this->db->query($query)->result()[0]->jumlah_data;
	}

	public function countDiscPk ($psikotesId,$disc){
		$jumlahP = $this->db->query("SELECT COUNT(*) as jumlah_data FROM t_psikotes_d WHERE deleted_at is null and pk='P' and disc like '".$disc."' and t_psikotes_id ='".$psikotesId."'")->result()[0]->jumlah_data;
		$jumlahK = $this->db->query("SELECT COUNT(*) as jumlah_data FROM t_psikotes_d WHERE deleted_at is null and pk='K' and disc like '".$disc."' and t_psikotes_id ='".$psikotesId."'")->result()[0]->jumlah_data;
		return $jumlahP - $jumlahK;
	}

	public function convertDisc($disc,$pk,$poin){
		$q = "SELECT id,".$disc." as conv FROM m_psikotes_convert_disc WHERE deleted_at is null and poin=".$poin." and pk ='".$pk."'";
		return $this->db->query($q)->result()[0]->conv;
	}

	public function saveResultLine($params = array()){
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_psikotes_result_line, $params);
		$id = $this->db->insert_id();
		return $id;
	}

	public function saveResultDisc($params = array()){
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->t_psikotes_result_disc, $params);
		$id = $this->db->insert_id();
		return $id;
	}

	public function getResultLine($psikotesId,$pk){
		$q = "SELECT * FROM t_psikotes_result_line WHERE deleted_at is null and t_psikotes_id = '".$psikotesId."' and pk='".$pk."'";
		return $this->db->query($q)->result()[0];
	}

	public function getResultPoinDisc($psikotesId,$pk){
		$q = "SELECT urutan from t_psikotes_result_disc WHERE deleted_at is null and pk ='".$pk."' and t_psikotes_id = ".$psikotesId." and poin = 1 ORDER BY urutan asc limit 1";
		$result = $this->db->query($q)->result();
		if (!empty($result)) {
			return $result[0]->urutan;
		};
		return 0;
	}

	public function getResultFromPoin($poin){
		$q = "SELECT * from m_psikotes_result_disc WHERE deleted_at is null and id = ".$poin.";";
		$result = $this->db->query($q)->result();
		if (!empty($result)) {
			return $result[0];
		};
		return null;
	}

	public function saveResult($params = array()){
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->t_psikotes_result, $params);
		$id = $this->db->insert_id();
		return $id;
	}

	public function updateDeskripsi($params = array())
	{
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->where('id', $params['id']);
		$this->db->update($this->table_psikotes, $params);
		$id = $params['id'];
		return $id;
	}

	public function get_data_psikotes($psikotesId){
		$q = "SELECT nama,usia,jk,DATE_FORMAT(tgl_tes, '%d-%m-%Y') as tgl_tes,m_employee_id,deskripsi_kepribadian,job_match,nama_file from t_psikotes WHERE deleted_at is null and id = ".$psikotesId.";";
		return $this->db->query($q)->result();
	}

	public function get_data_psikotes_d($psikotesId){
		$q = "SELECT * from t_psikotes_d WHERE deleted_at is null and t_psikotes_id = ".$psikotesId.";";
		return $this->db->query($q)->result();
	}

	public function get_data_psikotes_result($psikotesId){
		$q = "SELECT kepribadian,REPLACE(kepribadian_detail,',','<br>') as kepribadian_detail from t_psikotes_result WHERE deleted_at is null and t_psikotes_id = ".$psikotesId.";";
		return $this->db->query($q)->result();
	}

	public function get_data_psikotes_result_disc($psikotesId){
		$q = "SELECT * from t_psikotes_result_disc WHERE deleted_at is null and t_psikotes_id = ".$psikotesId.";";
		return $this->db->query($q)->result();
	}

	public function get_data_psikotes_result_line($psikotesId){
		$q = "SELECT * from t_psikotes_result_line WHERE deleted_at is null and t_psikotes_id = ".$psikotesId.";";
		return $this->db->query($q)->result();
	}

	public function delete($params = array())
	{
		$id = false;

		$params['is_aktif']  = 0;
		$params['deleted_at'] = date('Y-m-d H:i:s');
		$params['deleted_by'] = $this->session->userdata('user_id');
		$this->db->where('id', $params['id']);
		$this->db->update($this->table_psikotes, $params);
		$id = $params['id'];

		return $id;
	}

	public function count_psikotes_vacancy ($employeeId,$vacancyId){
		$query = "SELECT id FROM t_psikotes WHERE deleted_at is null and is_aktif = 1 and m_employee_id = ".$employeeId." and m_vacancy_id = ".$vacancyId;
		return $this->db->query($query)->result();
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->table_user		= 'm_user';
		$this->table_site		= 'm_site';
		$this->table_city		= 'm_city';
		$this->table_company	= 'm_company';
		$this->table_province	= 'm_province';
		$this->table_branch		= 'm_branch';
		$this->table_employee	= 'm_employee';
		$this->table_client		= 'm_client';
		$this->table_position	= 'm_position';
	}

	public function gets_security($params = array()){
		
		$this->db->from($this->table_site.' A');
		$this->db->join($this->table_employee.' B', 'B.site_id = A.id');
		$this->db->join($this->table_position.' C', 'C.id = B.position_id');

		$this->db->where('A.id >', 1);
		$this->db->where('A.is_active ', 1);
		$this->db->where('B.is_active ', 1);
		
		if(isset($params['job_type'])){
			$this->db->where('C.description', $params['job_type']);
		}
		if(isset($params['status_approval'])){
			$this->db->where('B.status_approval', $params['status_approval']);
		}
		if(isset($params['group_by'])){
			$this->db->group_by($params['group_by']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		
		$this->db->where('A.is_active', 1);

		if(!isset($params['bank'])){
			$this->db->where('A.id !=', 1);
		}

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['site_name'])){
			$this->db->where('A.name', $params['site_name']);
		}

		if(isset($params['code'])){
			$this->db->where('A.code', $params['code']);
		}

		if(isset($params['count_code'])){
			$this->db->like('A.code', $params['count_code']);
		}

		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.code', 'desc');
		}
		$this->db->from($this->table_site.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function get_site($params = array())
	{
		
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->from($this->table_site.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function preview($params = array())
	{
		
		$this->db->where('A.id', $params['id']);
		$this->db->where('A.is_active', 1);
		
		if(!isset($params['bank']))
		{
			$this->db->where('A.id !=', 1);
		}

		$this->db->from($this->table_site.' A');
		$this->db->join($this->table_city.' B', 'A.city_id = B.id', 'left');
		$this->db->join($this->table_user.' C', 'A.created_by = C.id', 'left');
		$this->db->join($this->table_user.' D', 'A.updated_by = D.id', 'left');
		$this->db->join($this->table_company.' E', 'A.company_id = E.id', 'left');
		// $this->db->join($this->table_user.' F', 'A.pic_id = F.id', 'left');
		$this->db->join($this->table_province.' G', 'B.province_id = G.id', 'left');
		$this->db->join($this->table_branch.' H', 'A.branch_id = H.id', 'left');
		$this->db->join($this->table_user.' I', 'A.pic_id_1 = I.id', 'left');
		$this->db->join($this->table_user.' J', 'A.pic_id_2 = J.id', 'left');
		$this->db->join($this->table_user.' K', 'A.pic_id_3 = K.id', 'left');
		$this->db->join($this->table_user.' L', 'A.supervisor_id = L.id', 'left');
		$this->db->join($this->table_client.' M', 'A.client_id = M.id', 'left');

		$this->db->select("A.*, B.name AS city_name, C.full_name AS creator, D.full_name AS updater, E.name AS company_name, H.name AS branch, CONCAT_WS(', ', I.full_name, J.full_name, K.full_name) AS ro, L.full_name AS supervisor, M.name AS client_name ");

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets_ro_site($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_site.' A');
		$this->db->join($this->table_company.' B', 'A.company_id = B.id', 'left');
		$this->db->join($this->table_branch.' C', 'A.branch_id = C.id', 'left');
		$this->db->where('A.id !=', 1);
		$this->db->where('A.is_active', 1);
		if(!isset($params['all'])){
			$this->db->where('A.contract_status =', 'Aktif');
		}
		if(isset($params['company_code'])){
			if($params['company_code'] != ''){
				$this->db->where('B.code', $params['company_code']);
			}
		}
		if(isset($params['site_code'])){
			if($params['site_code'] != ''){
				$this->db->like('A.code', $params['site_code']);
			}
		}
		if(isset($params['vacation'])){
			if($params['vacation'] != ''){
				$this->db->like('A.vacation', $params['vacation']);
			}
		}
		if(isset($params['branch_name'])){
			if($params['branch_name'] != ''){
				$this->db->like('C.name', $params['branch_name']);
			}
		}
		if(isset($params['site_name'])){
			$this->db->like('A.name', $params['site_name']);
		}

		if(isset($params['site_address'])){
			$this->db->like('A.address', $params['site_address']);
		}

		if($this->session->userdata('role') == 4 || $this->session->userdata('role') == 6){
			$this->db->where('(A.pic_id_1 = '.$this->session->userdata('user_id').' OR A.pic_id_2 = '.$this->session->userdata('user_id').' OR A.pic_id_3 = '.$this->session->userdata('user_id').' OR A.supervisor_id = '.$this->session->userdata('user_id').') AND A.is_active = 1 ');
		}
		if(!in_array($this->session->userdata('role'), $this->config->item('personalia'))){
			$this->db->where('A.id > ', 2);
		}

		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('A.branch_id', $params['branch_id']);
			}
		}

		if ($return_count){
			$total = $this->db->count_all_results();
			return $total;
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('B.code, A.name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}
	public function count_employee($params = array())
	{
		$this->db->from($this->table_site.' A');
		$this->db->join($this->table_employee.' B', 'A.id = B.site_id AND B.is_active = 1 AND B.status_approval = 3', 'left');
		$this->db->where('A.is_active', 1);
		if(!isset($params['all'])){
			$this->db->where('A.contract_status =', 'Aktif');
		}
		if($this->session->userdata('role') == 4 || $this->session->userdata('role') == 6){
			$this->db->where('(A.pic_id_1 = '.$this->session->userdata('user_id').' OR A.pic_id_2 = '.$this->session->userdata('user_id').' OR A.pic_id_3 = '.$this->session->userdata('user_id').' OR A.supervisor_id = '.$this->session->userdata('user_id').') ');
		}else{
			$this->db->where('A.id', 0);
		}
		$this->db->group_by('A.id');
		$this->db->select('A.id, A.name, A.address, COUNT(B.id) AS employee');
		$this->db->order_by('A.name');
		$result = $this->db->get()->result();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_site.' A');
		$this->db->join($this->table_city.' B', 'A.city_id = B.id', 'left');
		$this->db->join($this->table_company.' C', 'A.company_id = C.id', 'left');
		$this->db->join($this->table_province.' E', 'B.province_id = E.id', 'left');
		$this->db->join($this->table_branch.' F', 'A.branch_id = F.id', 'left');
		$this->db->join($this->table_user.' G', 'A.pic_id_1 = G.id', 'left');
		$this->db->join($this->table_user.' H', 'A.pic_id_2 = H.id', 'left');
		$this->db->join($this->table_user.' I', 'A.pic_id_3 = I.id', 'left');
		$this->db->join($this->table_user.' J', 'A.supervisor_id = J.id', 'left');
		$this->db->join($this->table_client.' K', 'A.client_id = K.id', 'left');

		if(!isset($params['all'])){
			$this->db->where('A.contract_status =', 'Aktif');
		}

		$this->db->where('A.is_active', 1);
		if($this->session->userdata('role') == 4 || $this->session->userdata('role') == 6){
			if(isset($params['session_branch'])){
				$this->db->where('A.branch_id', $params['session_branch']);
			}
		}
		
		if(!isset($params['bank'])){
			$this->db->where('A.id !=', 1);
		}

		if(!isset($params['indirect'])){
			$this->db->where('A.id !=', 2);
		}

		if(isset($params['id'])){
			$this->db->where('A.id =', $params['id']);
		}

		if(isset($params['code'])){
			if($params['code'] != ''){
				$this->db->like('A.code', $params['code']);
			}
		}

		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}

		if(isset($params['contract_status'])){
			$this->db->like('A.contract_status', $params['contract_status']);
		}

		if(isset($params['vacation'])){
			$this->db->like('A.vacation', $params['vacation']);
		}

		if(isset($params['ro'])){
			if($params['ro'] != ''){
				$this->db->like("CONCAT_WS(', ', G.full_name, H.full_name, I.full_name)", $params['ro']);
			}
		}

		if(isset($params['supervisor'])){
			if($params['supervisor'] != ''){
				$this->db->like('J.full_name', $params['supervisor']);
			}
		}

		if(isset($params['address'])){
			$this->db->like('A.address', $params['address']);
		}

		if(isset($params['city_name'])){
			if($params['city_name'] != ''){
				$this->db->like('B.name', $params['city_name']);
			}
		}

		if(isset($params['company_id'])){
			if($params['company_id'] != ''){
				$this->db->where('A.company_id', $params['company_id']);
			}
		}

		if(isset($params['company_code'])){
			if($params['company_code'] != ''){
				$this->db->where('C.code', $params['company_code']);
			}
		}

		if(isset($params['company_name'])){
			if($params['company_name'] != ''){
				$this->db->like('C.name', $params['company_name']);
			}
		}

		if(isset($params['branch_id'])){
			if($params['branch_id'] != '' && $params['branch_id'] > 1){
				$this->db->where('A.branch_id', $params['branch_id']);
			}
		}

		if(isset($params['branch'])){
			if($params['branch'] != ''){
				$this->db->like('F.name', $params['branch']);
			}
		}

		if(isset($params['client_name'])){
			if($params['client_name'] != ''){
				$this->db->like('K.name', $params['client_name']);
			}
		}

		if ($return_count){
			$total = $this->db->count_all_results();
			return $total;
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('C.code, A.name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*, B.name AS city_name, C.code AS company_code, C.name AS company_name');
		}

		$result = $this->db->get()->result();

		return $result;
	}

	public function gets_api($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_site.' A');
		$this->db->join($this->table_company.' B', 'A.company_id = B.id', 'left');
		$this->db->join($this->table_branch.' C', 'A.branch_id = C.id', 'left');
		$this->db->join($this->table_user.' D', 'A.pic_id_1 = D.id', 'left');

		if(!isset($params['all'])){
			$this->db->where('A.contract_status =', 'Aktif');
		}

		$this->db->where('A.is_active', 1);
		
		if(!isset($params['bank'])){
			$this->db->where('A.id !=', 1);
		}

		if(!isset($params['indirect'])){
			$this->db->where('A.id !=', 2);
		}

		if(isset($params['id'])){
			$this->db->where('A.id =', $params['id']);
		}

		if(isset($params['nama'])){
			$this->db->like('A.name', $params['nama']);
		}

		if(isset($params['bisnis_unit'])){
			if($params['bisnis_unit'] != ''){
				$this->db->where('B.code', $params['bisnis_unit']);
			}
		}

		if(isset($params['cabang'])){
			if($params['cabang'] != ''){
				$this->db->like('C.name', $params['cabang']);
			}
		}

		if(isset($params['nama_pic'])){
			if($params['nama_pic'] != ''){
				$this->db->like('D.full_name', $params['nama_pic']);
			}
		}

		if(isset($params['terakhir_update'])){
			if($params['terakhir_update'] != ''){
				$this->db->like('A.updated_at', $params['terakhir_update']);
			}
		}

		if(isset($params['tanggal_awal'])){
			if($params['tanggal_awal'] != ''){
				$this->db->where('A.updated_at >=', $params['tanggal_awal']);
			}
		}

		if(isset($params['tanggal_akhir'])){
			if($params['tanggal_akhir'] != ''){
				$this->db->where('A.updated_at <=', $params['tanggal_akhir']);
			}
		}

		if ($return_count){
			$total = $this->db->count_all_results();
			return $total;
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['offset'])){
			$this->db->offset($params['offset']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.updated_at', 'DESC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();

		return $result;
	}

	public function gets_site($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_site.' A');
		$this->db->join($this->table_city.' B', 'A.city_id = B.id', 'left');
		$this->db->join($this->table_company.' C', 'A.company_id = C.id', 'left');
		$this->db->join($this->table_province.' E', 'B.province_id = E.id');
		$this->db->join($this->table_branch.' F', 'E.branch_id = E.id', 'left');
		$this->db->join($this->table_client.' G', 'A.client_id = G.id', 'left');

		$this->db->where('A.is_active', 1);

		if(isset($params['assurance'])){
			$this->db->where("(A.health_insurance_status = 'Terdaftar' OR A.labor_insurance_status = 'Terdaftar')");
		}
		
		if(isset($params['non_active'])){
			$this->db->where('A.contract_status !=', 'Aktif');
		}

		if(!isset($params['bank'])){
			$this->db->where('A.id !=', 1);
		}

		if(isset($params['id'])){
			$this->db->where('A.id =', $params['id']);
		}

		if(isset($params['code'])){
			$this->db->like('A.code', $params['code']);
		}

		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}

		if(isset($params['contract_status'])){
			$this->db->like('A.contract_status', $params['contract_status']);
		}

		if(isset($params['address'])){
			$this->db->like('A.address', $params['address']);
		}

		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('A.branch_id', $params['branch_id']);
			}
		}

		if(isset($params['city_name'])){
			if($params['city_name'] != ''){
				$this->db->like('B.name', $params['city_name']);
			}
		}

		if(isset($params['company_id'])){
			if($params['company_id'] != ''){
				$this->db->where('A.company_id', $params['company_id']);
			}
		}

		if(isset($params['company_name'])){
			if($params['company_name'] != ''){
				$this->db->like('C.name', $params['company_name']);
			}
		}

		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('E.branch_id', $params['branch_id']);
			}
		}

		if(isset($params['branch'])){
			if($params['branch'] != ''){
				$this->db->like('F.name', $params['branch']);
			}
		}

		if(isset($params['client_name'])){
			if($params['client_name'] != ''){
				$this->db->like('G.name', $params['client_name']);
			}
		}

		if ($return_count){
			$total = $this->db->count_all_results();
			return $total;
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if($params['id'] == 1){
			return $id;
		}
		if ($params['id'] != '') {
			if(isset($params['contract_status'])){
				if($params['contract_status'] == 'Aktif'){
					$params['health_insurance_status'] = 'Terdaftar';
					$params['labor_insurance_status'] = 'Terdaftar';
				}
			}
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_site, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_site, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}

	public function update_employee($params){
		
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->where('site_id', $params['site_id']);
		$this->db->update($this->table_employee, $params);
		$id = $params['id'];
		return $id;
	}

	public function check_duplicate(){
		$result = $this->db->query("SELECT s.code, s.name, s.id FROM m_site s WHERE is_active = 1 AND s.code = 'PKN02' ORDER BY s.id")->result();
		$result = $this->db->query("SELECT s.code, s.name, s.id FROM m_site s WHERE is_active = 1 ORDER BY s.id")->result();
		return $result;
	}
}
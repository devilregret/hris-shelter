<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formula_employee_model extends CI_Model{

	public function __construct()
	{
		$this->table_formula	= 't_formula_employee';
		$this->table_employee	= 'm_employee';
		$this->table_position	= 'm_position';
	}

	public function gets_employee($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_position.' B', 'A.position_id = B.id', 'left');
		
		$this->db->where('A.is_active', 1);
		$this->db->where('A.status_approval', 3);
		$this->db->where('A.status', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if(isset($params['id_card'])){
			$this->db->like('A.id_card', $params['id_card']);
		}

		if(isset($params['employee_number'])){
			$this->db->like('A.employee_number', $params['employee_number']);
		}

		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}

		if(isset($params['position'])){
			$this->db->like('B.name', $params['position']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_formula.' A');
		
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['column_not_empty'])){
			$this->db->where('A.kolom != ""');
		}

		if(isset($params['min_no'])){
			$this->db->where('A.no >=', $params['min_no']);
		}

		if(isset($params['max_no'])){
			$this->db->where('A.no <=', $params['max_no']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.no', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}
		
		if(isset($params['no'])){
			$this->db->where('A.no', $params['no']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_formula.' A');
		
		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$this->db->insert($this->table_formula, $params);
		$id = $this->db->insert_id();
		// print_prev($this->db->last_query());
		return $id;
	}

	public function delete($params = array())
	{
		$id = $this->db->delete($this->table_formula, array('employee_id' => $params['employee_id']));
		return $id;
	}


	public function gets_formula_null($site_id){
		$sql = "SELECT e.id, e.position_id FROM m_employee e 
		LEFT JOIN t_formula_employee f ON e.id= f.employee_id 
		WHERE f.employee_id IS NULL AND e.site_id = ".$site_id."
		AND e.is_active = 1 AND e.status_approval = 3";

		$result = $this->db->query($sql)->result();
		return $result;
	}

	public function gets_employee_with_fromula($site_id, $position_id){
		$sql = "SELECT e.id, e.position_id FROM m_employee e LEFT JOIN t_formula_employee f ON e.id= f.employee_id WHERE f.employee_id IS NOT NULL AND e.site_id = ".$site_id." AND e.is_active = 1 AND e.status_approval = 3 AND e.position_id = ".$position_id." LIMIT 1";

		$result = $this->db->query($sql)->row();
		return $result;
	}

	public function save_formula_null($employee_id, $employee_exist){
		$sql = " INSERT INTO t_formula_employee(employee_id, no, nama, tipe, kategori, kolom, nilai)
		SELECT ".$employee_id." AS employee_id, no, nama, tipe, kategori, kolom, nilai FROM t_formula_employee WHERE employee_id = ".$employee_exist." ;";
		$this->db->query($sql);
	}
}
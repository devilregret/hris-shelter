<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Candidate_model extends CI_Model{
	private $table_user;
	private $table_city;
	private $table_province;
	private $table_employee;
	private $table_site;
	private $table_branch;
	private $table_position;
	private $table_skill;
	private $table_education;
	private $table_emergency;
	private $table_history;
	private $table_language;
	private $table_reference;

	public function __construct()
	{
		parent::__construct();
		$this->table_user		= 'm_user';
		$this->table_city		= 'm_city';
		$this->table_province	= 'm_province';
		$this->table_employee	= 'm_employee';
		$this->table_site		= 'm_site';
		$this->table_company	= 'm_company';
		$this->table_branch		= 'm_branch';
		$this->table_position	= 'm_position';
		$this->table_preference	= 'm_preference_job';
		$this->table_skill		= 'm_employee_skill';
		$this->table_education	= 'm_employee_education';
		$this->table_emergency	= 'm_employee_emergency';
		$this->table_history	= 'm_employee_history';
		$this->table_language	= 'm_employee_language';
		$this->table_reference	= 'm_employee_reference';
	}

	public function gets_new($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_preference.' B', 'A.preference_job1 = B.id', 'left');

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}
		if(isset($params['date'])){
			$this->db->where("DATE_FORMAT(A.created_at, '%Y-%m-%d') = DATE_FORMAT(NOW() - INTERVAL ".$params['date']." DAY, '%Y-%m-%d')");
		}
		if ($return_count){
			$result = $this->db->count_all_results();
			return $result;
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.created_at', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->group_by('A.preference_job1');
		$result = $this->db->get()->result();
// 		print_r($this->db->last_query());
// 		die();
		return $result;
	}

	public function check_card($params = array())
	{	
		if(isset($params['id_card'])){
			$this->db->where('A.id_card', $params['id_card']);    
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_employee.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function get($params = array())
	{
		// $this->db->where('A.is_active', 1);

		if(isset($params['status'])){
			$this->db->where('A.status', $params['status']);
		}

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		// else{
		// 	$this->db->where('A.id IS NULL');
		// }
		
		if(isset($params['id_card'])){
			$this->db->where('A.id_card', $params['id_card']);    
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_employee.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function approved_gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A ');
		$this->db->join($this->table_city.' B', 'A.city_domisili_id = B.id', 'left');
		$this->db->join($this->table_site.' C', 'A.site_id = C.id', 'left');
		$this->db->join($this->table_company.' D', 'A.company_id = D.id', 'left');
		$this->db->join($this->table_position.' E', 'A.position_id = E.id', 'left');

		// $this->db->group_by('A.id'); 

		$this->db->where('A.is_active', 1);
		if(isset($params['session_branch'])){
			$this->db->where('A.branch_id', $params['session_branch']);
		}

		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('C.branch_id', $params['branch_id']);
			}
		}

		if(isset($params['period'])){
			if($params['period']!= ""){
				$this->db->like('A.approved_at', $params['period']);
			}
		}
		
		if(isset($params['status'])){
			$this->db->where('A.status', $params['status']);
		}

		if(isset($params['type'])){
			if($params['type'] == 'indirect'){
				$this->db->where('A.site_id', 2);
			}else{
				$this->db->where('A.site_id >', 2);
			}
		}
		
		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}

		if(isset($params['id_card'])){
			$this->db->like('A.id_card', $params['id_card']);
		}
		
		if(isset($params['age'])){
			if($params['age'] != ''){
				$this->db->like('FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25)', $params['age']);
			}
		}

		if(isset($params['education_level'])){
			$this->db->like('A.education_level', $params['education_level']);
		}

		if(isset($params['education_majors'])){
			$this->db->like('A.education_majors', $params['education_majors']);
		}
		
		if(isset($params['approved_at'])){
			$this->db->like("DATE_FORMAT(A.approved_at, '%d/%m/%Y')", $params['approved_at']);
		}

		if(isset($params['city_domisili'])){
			if($params['city_domisili'] != ''){
				$this->db->like('B.name', $params['city_domisili']);
			}
		}

		if(isset($params['site_name'])){
			$this->db->like('C.name', $params['site_name']);
		}

		if(isset($params['company_name'])){
			if($params['company_name'] != ''){
			$this->db->like('D.name', $params['company_name']);
			}
		}

		if(isset($params['position'])){
			if($params['position'] != ''){
				$this->db->like('E.name', $params['position']);
			}
		}
		
		if ($return_count){
			$this->db->select('A.id');
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
		    if($params['orderby'] == 'date_registration'){
				$params['orderby'] = 'A.created_at';
			}
			if($params['orderby'] == 'approved_at'){
				$params['orderby'] = 'A.approved_at';
			}

			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.full_name', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A ');
		$this->db->join($this->table_city.' B', 'A.city_domisili_id = B.id', 'left');
		$this->db->join($this->table_skill.' C', 'A.id = C.employee_id', 'left');
		$this->db->join($this->table_preference.' D', 'A.preference_job1 = D.id', 'left');
		$this->db->join($this->table_preference.' E', 'A.preference_job2 = E.id', 'left');
		$this->db->join($this->table_preference.' F', 'A.preference_job3 = F.id', 'left');
		$this->db->join($this->table_city.' G', 'A.city_birth_id = G.id', 'left');
		$this->db->join($this->table_city.' H', 'A.city_card_id = H.id', 'left');
		$this->db->join($this->table_site.' I', 'A.site_id = I.id', 'left');
		$this->db->join($this->table_company.' J', 'A.company_id = J.id', 'left');
		$this->db->join($this->table_position.' K', 'A.position_id = K.id', 'left');
		$this->db->join($this->table_province.' L', 'B.province_id = L.id', 'left');
		$this->db->join($this->table_branch.' M', 'A.branch_id = M.id', 'left');
		$this->db->join($this->table_user.' N', 'A.submitted_by = N.id', 'left');
		
		$this->db->group_by('A.id'); 
		$this->db->where('A.is_active', 1);
		if(isset($params['session_branch'])){
			if($params['session_branch'] != ''){
				$this->db->where('A.branch_id', $params['session_branch']);
			}
		}
		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('I.branch_id', $params['branch_id']);
			}
		}
		if(isset($params['not_followup'])){
			$this->db->where(' (A.followup_status = "" OR A.followup_status IS NULL) ');
		}
		if(isset($params['followup_status'])){
			if($params['followup_status'] != ''){
				$this->db->where('A.followup_status', $params['followup_status']);
			}
		}
		if(isset($params['status_completeness'])){
			$this->db->where('A.status_completeness', $params['status_completeness']);
		}

		if(isset($params['nonactive'])){
			$this->db->where("A.updated_at < NOW() - INTERVAL 3 MONTH");
		}

		if(isset($params['status_nonjob'])){
			$this->db->where('A.status_nonjob', $params['status_nonjob']);
		}

		if(isset($params['active'])){
			$this->db->where("A.updated_at >= NOW() - INTERVAL 3 MONTH");
		}
		
		if(isset($params['status'])){
			$this->db->where('A.status', $params['status']);
		}

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if(isset($params['not_site_id'])){
			$this->db->where('A.site_id != ', $params['not_site_id']);
		}

		if(isset($params['status_approval'])){
			$this->db->where('A.status_approval', $params['status_approval']);
		}

		if(isset($params['registration_number'])){
			$this->db->like('A.registration_number', $params['registration_number']);
		}

		if(isset($params['created_at'])){
			if($params['created_at'] != ''){
				$this->db->like('DATE_FORMAT(A.created_at, "%d/%m/%Y")', $params['created_at']);
			}
		}

		if(isset($params['updated_at'])){
			if($params['updated_at'] != ''){
				$this->db->like('DATE_FORMAT(A.updated_at, "%d/%m/%Y")', $params['updated_at']);
			}
		}

		if(isset($params['full_name'])){
			if($params['full_name'] != ''){
				$this->db->like('A.full_name', $params['full_name']);
			}
		}

		if(isset($params['id_card'])){
			if($params['id_card'] != ''){
				$this->db->like('A.id_card', $params['id_card']);
			}
		}
		
		if(isset($params['age'])){
			if($params['age'] != ''){
				$this->db->like('FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25)', $params['age']);
			}
		}
		if(isset($params['gender'])){
			if($params['gender'] != ''){
				$this->db->like('A.gender', $params['gender']);
			}
		}

		if(isset($params['education_level'])){
			if($params['education_level'] != ''){
				$this->db->like('A.education_level', $params['education_level']);
			}
		}

		if(isset($params['education_majors'])){
			if($params['education_majors'] != ''){
				$this->db->like('A.education_majors', $params['education_majors']);
			}
		}

		if(isset($params['phone_number'])){
			if($params['phone_number'] != ''){
				$this->db->like('A.phone_number', $params['phone_number']);
			}
		}

		if(isset($params['site_preference'])){
			if($params['site_preference'] != ''){
				$this->db->like('A.site_preference', $params['site_preference']);
			}
		}

		if(isset($params['date_birth'])){
			if($params['date_birth'] != ''){
				$this->db->like("DATE_FORMAT(A.date_birth, '%d/%m/%Y')", $params['date_birth']);
			}
		}

		if(isset($params['city_domisili'])){
			if($params['city_domisili'] != ''){
				$this->db->like('B.name', $params['city_domisili']);
			}
		}

		if(isset($params['city_birth'])){
			if($params['city_birth'] != ''){
				$this->db->like('G.name', $params['city_birth']);
			}
		}

		if(isset($params['province_domisili'])){
			if($params['province_domisili'] != ''){
				$this->db->like('L.name', $params['province_domisili']);
			}
		}

		if(isset($params['branch_name'])){
			if($params['branch_name'] != ''){
				$this->db->like("M.name", $params['branch_name']);
			}
		}

		if(isset($params['preference_job'])){
			if($params['preference_job'] != ''){
				$this->db->like("CONCAT_WS(', ', D.name, E.name, F.name)", $params['preference_job']);
			}
		}

		if(isset($params['certificate'])){
			if($params['certificate'] != ''){
				$this->db->like('C.certificate_name', $params['certificate']);
			}
		}

		if(isset($params['site_name'])){
			if($params['site_name'] != ''){
				$this->db->like('I.name', $params['site_name']);
			}
		}

		if(isset($params['company_name'])){
			if($params['company_name'] != ''){
			$this->db->like('J.name', $params['company_name']);
			}
		}

		if(isset($params['company_code'])){
			if($params['company_code'] != ''){
				$this->db->where('J.code', $params['company_code']);
			}
		}

		if(isset($params['position'])){
			if($params['position'] != ''){
				$this->db->like('K.name', $params['position']);
			}
		}

		if(isset($params['submitted_by'])){
			if($params['submitted_by'] != ''){
				$this->db->like('N.full_name', $params['submitted_by']);
			}
		}

		if(isset($params['submitted_at'])){
			$this->db->like("DATE_FORMAT(A.submitted_at, '%d/%m/%Y')", $params['submitted_at']);
		}

		if(isset($params['placement_date'])){
			if($params['placement_date'] != ''){
				$this->db->like("DATE_FORMAT(A.placement_date, '%d/%m/%Y')", $params['placement_date']);
			}
		}

		if(isset($params['last_date'])){
			$this->db->where("A.created_at >= ", $params['last_date']);
		}

		if ($return_count){
			$this->db->select('A.id');
			$total = $this->db->count_all_results();
			return $total;
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
		    if($params['orderby'] == 'date_registration'){
				$params['orderby'] = 'A.created_at';
			}
			if($params['orderby'] == 'last_update'){
				$params['orderby'] = 'A.updated_at';
			}
			if($params['orderby'] == 'submitted_at'){
				$params['orderby'] = 'A.submitted_at';
			}
			if($params['orderby'] == 'placement_date'){
				$params['orderby'] = 'A.placement_date';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.full_name', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*, B.name AS province_name');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function gets_register($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A ');
		$this->db->join($this->table_city.' B', 'A.city_domisili_id = B.id', 'left');
		$this->db->join($this->table_preference.' C', 'A.preference_job1 = C.id', 'left');
		$this->db->join($this->table_branch.' D', 'A.branch_id = D.id', 'left');

		if(isset($params['last_date'])){
			$this->db->where("A.created_at >= ", $params['last_date']);
		}

		if ($return_count){
			$this->db->select('A.id');
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
		    if($params['orderby'] == 'created_at'){
				$params['orderby'] = 'A.created_at';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.full_name', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function export($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_city.' B', 'A.city_domisili_id = B.id', 'left');
		$this->db->join($this->table_skill.' C', 'A.id = C.employee_id', 'left');
		$this->db->join($this->table_preference.' D', 'A.preference_job1 = D.id', 'left');
		$this->db->join($this->table_preference.' E', 'A.preference_job2 = E.id', 'left');
		$this->db->join($this->table_preference.' F', 'A.preference_job3 = F.id', 'left');
		$this->db->join($this->table_education.' G', 'A.id = G.employee_id', 'left');
		$this->db->join($this->table_emergency.' H', 'A.id = H.employee_id', 'left');
		$this->db->join($this->table_history.' I', 'A.id = I.employee_id', 'left');
		$this->db->join($this->table_language.' J', 'A.id = J.employee_id', 'left');
		$this->db->join($this->table_reference.' K', 'A.id = K.employee_id', 'left');
		
		$this->db->group_by('A.id'); 
		$this->db->where('A.is_active', 1);

		if(isset($params['status'])){
			$this->db->where('A.status', $params['status']);
		}

		if(isset($params['status_approval'])){
			$this->db->where('A.status_approval', $params['status_approval']);
		}

		if(isset($params['registration_number'])){
			$this->db->like('A.registration_number', $params['registration_number']);
		}

		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}

		if(isset($params['id_card'])){
			$this->db->like('A.id_card', $params['id_card']);
		}
		
		if(isset($params['age'])){
			if($params['age'] != ''){
				$this->db->like('FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25)', $params['age']);
			}
		}

		if(isset($params['education_level'])){
			$this->db->like('A.education_level', $params['education_level']);
		}

		if(isset($params['education_majors'])){
			$this->db->like('A.education_majors', $params['education_majors']);
		}

		if(isset($params['city_domisili'])){
			$this->db->like('B.name', $params['city_domisili']);
		}

		if(isset($params['preference_job'])){
			if($params['preference_job'] != ''){
				$this->db->like("CONCAT_WS(', ', D.name, E.name, F.name)", $params['preference_job']);
			}
		}

		if(isset($params['certificate'])){
			if($params['certificate'] != ''){
				$this->db->like('C.certificate_name', $params['certificate']);
			}
		}
		
		if ($return_count){
			$this->db->select('A.id');
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.name', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*, B.name AS province_name');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function preview($params = array())
	{
		$this->db->where('A.id', $params['id']);
		$this->db->where('A.is_active', 1);
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_user.' B', 'A.created_by = B.id', 'left');
		$this->db->join($this->table_user.' C', 'A.updated_by = C.id', 'left');
		$this->db->join($this->table_city.' D', 'A.city_birth_id = D.id', 'left');
		$this->db->join($this->table_city.' E', 'A.city_card_id = E.id', 'left');
		$this->db->join($this->table_city.' F', 'A.city_domisili_id = F.id', 'left');
		$this->db->join($this->table_preference.' G', 'A.preference_job1 = G.id', 'left');
		$this->db->join($this->table_preference.' H', 'A.preference_job2 = H.id', 'left');
		$this->db->join($this->table_preference.' I', 'A.preference_job3 = I.id', 'left');
		$this->db->join($this->table_province.' J', 'E.province_id = J.id', 'left');
		$this->db->join($this->table_province.' K', 'F.province_id = K.id', 'left');

		$this->db->select('A.*, B.full_name AS creator, C.full_name AS updater, D.name AS city_birth, E.name AS city_card, F.name AS city_domisili, G.name AS preference1, H.name AS preference2, I.name AS preference3, J.name AS province_card, K.name AS province_domisili');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_employee, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$user_id = $this->session->userdata('user_id');
			if($user_id != ""){
				$params['created_by'] = $this->session->userdata('user_id');
				$params['updated_by'] = $this->session->userdata('user_id');
			}else{
				$params['created_by'] = 1;
				$params['updated_by'] = 1;
			}
			$params['updated_at'] = date('Y-m-d H:i:s');
			$this->db->insert($this->table_employee, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}

	public function update($params = array())
	{
		$this->db->where('id', $params['id']);
		$this->db->update($this->table_employee, $params);	
	}

	public function reset_resign_submit(){
		$this->db->where('status_approval', 3);
		$this->db->update($this->table_employee, array('resign_submit' => null));

		$this->db->where('status_approval', 3);
		$this->db->where('company_id', 0);
		$this->db->update($this->table_employee, array('company_id' => 3));

		$this->db->where('status_approval', 3);
		$this->db->where('branch_id', 0);
		$this->db->update($this->table_employee, array('branch_id' => 2));

		$this->db->where('status_approval', 3);
		$this->db->where('site_id', 0);
		$this->db->update($this->table_employee, array('status_approval' => 0, 'status' => 0));
	}

	public function generate_approval(){
		$query = "SELECT e.id, MAX(ec.contract_start) AS contract_start, e.site_id, e.company_id, e.position_id, e.status_approval FROM m_employee e 
				JOIN t_employee_contract ec ON e.id = ec.employee_id 
				LEFT JOIN t_approval_history ah ON e.id = ah.employee_id AND ah.status_approval = 'Diterima' 
				WHERE e.status_approval = 3 AND ec.employee_id IS NOT NULL AND ec.contract_start > '2013-01-01' AND ah.employee_id IS NULL 
				GROUP BY e.id";
		$result = $this->db->query($query)->result();
		return $result;
	}
}
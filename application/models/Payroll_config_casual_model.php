<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll_config_casual_model extends CI_Model{
	
	private $table_payroll_config_casual;
	public function __construct()
	{
		parent::__construct();
		$this->table_payroll_config_casual	= 't_payroll_config_casual';
	}

	public function get($params = array())
	{
		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->from($this->table_payroll_config_casual.' A');
		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$this->db->replace($this->table_payroll_config_casual, $params);
		return 1;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wappin_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_wappin		= 'm_wappin_configuration';
	}

	public function get($params = array())
	{
	
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_wappin.' A');

		$result = $this->db->get()->row();
		return $result;
	}
	
	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_wappin, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$this->db->insert($this->table_wappin, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}

	public function save_log($params = array())
	{

		$this->db->insert('m_wappin_log', $params);
		$id = $this->db->insert_id();
		return $id;
	}

}
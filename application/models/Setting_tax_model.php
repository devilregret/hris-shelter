<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_tax_model extends CI_Model{
	private $table_user;
	private $table_tax;

	public function __construct()
	{
		parent::__construct();
		$this->table_user      	 = 'm_user';
		$this->table_setting_tax = 'm_setting_tax';
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);
		
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		if(isset($params['code'])){
			$this->db->where('A.code', $params['code']);
		}
				
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_setting_tax.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function preview($params = array())
	{
		if(isset($params['code'])){
			$this->db->where('A.code', $params['code']);
		}
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		$this->db->where('A.is_active', 1);
		$this->db->from($this->table_setting_tax.' A');
		$this->db->join($this->table_user.' B', 'A.created_by = B.id');
		$this->db->join($this->table_user.' C', 'A.updated_by = C.id');

		$this->db->select('A.*, B.full_name AS creator, C.full_name AS updater');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_setting_tax.' A');
		
		$this->db->where('A.is_active', 1);

		if(isset($params['code'])){
			$this->db->like('A.code', $params['code']);
		}

		if(isset($params['description'])){
			$this->db->like('A.description', $params['description']);
		}

		if(isset($params['minimal'])){
			$this->db->like('A.minimal', $params['minimal']);
		}

		if(isset($params['tax'])){
			$this->db->like('A.tax', $params['tax']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.code', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}



	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_setting_tax, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_setting_tax, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
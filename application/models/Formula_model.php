<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formula_model extends CI_Model{
	private $table_role;

	public function __construct()
	{
		parent::__construct();
		$this->table_formula       = 'm_formula';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_formula.' A');
		
		$this->db->where('A.is_active', 1);

		
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['name'])){
			$this->db->like('A.name', $params['name']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.name', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}
}
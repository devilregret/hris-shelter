<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_contract_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_contract   = 't_employee_contract';
		$this->table_employee   = 'm_employee';
		$this->table_site   	= 'm_site';
		$this->table_branch   	= 'm_branch';
		$this->table_company   	= 'm_company';
		$this->table_user 	    = 'm_user';
	}

	public function gets_expired($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' B');
		$this->db->join('(SELECT MAX(contract_end) AS contract_end, MAX(contract_start) AS contract_start, employee_id FROM t_employee_contract WHERE is_active = 1 GROUP BY employee_id) A', 'A.employee_id = B.id', 'left');
		
		$this->db->join($this->table_site.' C', 'B.site_id = C.id', 'left');
		$this->db->join($this->table_company.' D', 'C.company_id = D.id', 'left');
		$this->db->join($this->table_branch.' E', 'C.branch_id = E.id', 'left');
		$this->db->where('C.id > ', 1);
		$this->db->where('B.status_approval ', 3);
		$this->db->where('B.status_nonjob != ', 1);

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}
		if(isset($params['company_id'])){
			$this->db->where('D.id', $params['company_id']);
		}
		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('C.branch_id', $params['branch_id']);
			}
		}
		if(isset($params['expired'])){
			if($params['expired'] <= 0){
				$this->db->where('A.contract_end < NOW()');	
			}else if($params['expired'] > 0){
				$this->db->where("A.contract_end = DATE_FORMAT(NOW() + INTERVAL ".$params['expired']." DAY, '%Y-%m-%d')") ;	
			}
		}
		if ($return_count){
			$result = $this->db->count_all_results();
			return $result;
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.contract_end', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->group_by('B.site_id');
		$result = $this->db->get()->result();
		return $result;
	}


	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_contract.' A');
		$this->db->where('A.is_active', 1);

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}else{
			$this->db->where('A.employee_id IS NULL');
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.contract_end', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		$this->db->from($this->table_contract.' A');

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if (isset($params['id'])) {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_contract, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_contract, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_casual_schedule_model extends CI_Model{
	public function __construct()
	{
		parent::__construct();
		$this->table_casual_schedule	= 't_employee_casual_schedule';
		$this->table_casual_employee	= 'm_employee_casual';
		$this->table_casual_department	= 'm_department_casual';
	}

	public function get($params = array())
	{
		
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', 1);
		}
		
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['date'])){
			$this->db->where('A.date', $params['date']);
		}

		if(isset($params['department_id'])){
			$this->db->where('A.department_id', $params['department_id']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_casual_schedule.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_casual_schedule.' A');
		$this->db->join($this->table_casual_employee.' B', 'A.employee_id = B.id', 'left');
		$this->db->join($this->table_casual_department.' C', 'A.department_id = C.id', 'left');

		if(isset($params['is_active'])){
			$this->db->where('A.is_active', $params['is_active']);
		}else{
			$this->db->where('A.is_active', 1);
		}


		if(isset($params['is_status'])){
			if($params['is_status'] != ''){
				$this->db->where('B.status', $params['is_status']);
			}
		}

		if(isset($params['date_start'])){
			if($params['date_start'] != ''){
				$this->db->where('A.date >=', $params['date_start']);
			}
		}

		if(isset($params['date_finish'])){
			if($params['date_finish'] != ''){
				$this->db->where('A.date <=', $params['date_finish']);
			}
		}

		if(isset($params['site_id'])){
			if($params['site_id'] != ''){
				$this->db->where('B.site_id', $params['site_id']);
			}
		}

		if(isset($params['date'])){
			if($params['date'] != ''){
				$this->db->like("DATE_FORMAT(A.date, '%d/%m/%Y')", $params['date']);
			}
		}

		if(isset($params['department'])){
			if($params['department'] != ''){
				$this->db->like('C.name', $params['department']);
			}
		}

		if(isset($params['status'])){
			if($params['status'] != ''){
				$this->db->like('B.status', $params['status']);
			}
		}
		
		if(isset($params['employee_number'])){
			if($params['employee_number'] != ''){
				$this->db->like('B.employee_number', $params['employee_number']);
			}
		}

		if(isset($params['pin_finger'])){
			if($params['pin_finger'] != ''){
				$this->db->like('B.pin_finger', $params['pin_finger']);
			}
		}

		if(isset($params['full_name'])){
			if($params['full_name'] != ''){
				$this->db->like('B.full_name', $params['full_name']);
			}
		}

		if(isset($params['id_card'])){
			if($params['id_card'] != ''){
				$this->db->like('B.id_card', $params['id_card']);
			}
		}

		if(isset($params['address_card'])){
			if($params['address_card'] != ''){
				$this->db->like('B.address_card', $params['address_card']);
			}
		}

		if(isset($params['phone_number'])){
			if($params['phone_number'] != ''){
				$this->db->like('B.phone_number', $params['phone_number']);
			}
		}

		if(isset($params['bank_name'])){
			if($params[' bank_name'] != ''){
				$this->db->like('B.bank_name', $params['bank_name']);
			}
		}

		if(isset($params['bank_account'])){
			if($params['bank_account'] != ''){
				$this->db->like('B.bank_account', $params['bank_account']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'date'){
				$params['orderby'] = 'A.date';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('B.full_name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_casual_schedule, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_casual_schedule, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
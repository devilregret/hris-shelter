<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rosecurityapproval_model extends CI_Model{
	public function __construct()
	{
		parent::__construct();
		$this->table_employee   = 'm_employee';
		$this->table_company	= 'm_company';
		$this->table_site		= 'm_site';
		$this->table_position	= 'm_position';
		$this->table_user		= 'm_user';
	}

	// public function get($params = array()){
	// 	$this->db->from($this->table_employee.' A');

	// 	$this->db->where('A.is_active', 1);
		
	// 	if(isset($params['employee_id'])){
	// 		$this->db->where('A.employee_id', $params['employee_id']);
	// 	}

	// 	if(isset($params['columns'])){
	// 		$this->db->select($params['columns']);
	// 	}else{
	// 		$this->db->select('A.*');
	// 	}
	// 	$this->db->limit(1);
	// 	$this->db->order_by('A.id', 'DESC');

	// 	$result = $this->db->get()->row();
	// 	return $result;
	// }

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_site.' B', 'A.site_id = B.id', 'left');
		$this->db->join($this->table_position.' C', 'A.position_id = C.id', 'left');

		$this->db->where('A.is_active', 1);
		
		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('B.branch_id', $params['branch_id']);
			}
		}

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if(isset($params['status_approval'])){
			$this->db->where('A.status_approval', $params['status_approval']);
		}

		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}

		if(isset($params['employee_number'])){
			$this->db->like('A.employee_number', $params['employee_number']);
		}

		if(isset($params['id_card'])){
			$this->db->like('A.id_card', $params['id_card']);
		}

		if(isset($params['resign_submit'])){
			$this->db->like('DATE_FORMAT(A.resign_submit, "%d/%m/%Y")', $params['resign_submit']);
		}

		if(isset($params['resign_type'])){
			$this->db->like('A.resign_type', $params['resign_type']);
		}

		if(isset($params['resign_note'])){
			$this->db->like('A.resign_note', $params['resign_note']);
		}

		if(isset($params['resign_burden'])){
			$this->db->like('A.resign_burden', $params['resign_burden']);
		}

		if(isset($params['site_name'])){
			$this->db->like('B.name', $params['site_name']);
		}

		if(isset($params['position_type'])){
			$this->db->like('C.description', $params['position_type']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
			if($params['orderby'] == 'resign_submit'){
				$params['orderby'] = 'A.resign_submit';
			}
		}else{
			$this->db->order_by('A.updated_at', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('*');
		}

		$result = $this->db->get()->result();
		return $result;
	}
}
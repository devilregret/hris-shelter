<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ketelitian_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->table_user       			= 'm_user';
		$this->table_ketelitian				= 't_ketelitian';
		$this->table_ketelitian_d			= 't_ketelitian_d';
	}

	public function getSoal(){
		$result = $this->db->query("SELECT * from m_ketelitian WHERE deleted_at is null and jawaban is not null ORDER BY no_soal")->result();
		return $result;
	}

	public function save($params = array()){
		$params['is_aktif']  = 1;
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_ketelitian, $params);
		$id = $this->db->insert_id();
		return $id;
	}

	public function delete($params = array())
	{
		$id = false;

		$params['is_aktif']  = 0;
		$params['deleted_at'] = date('Y-m-d H:i:s');
		$params['deleted_by'] = $this->session->userdata('user_id');
		$this->db->where('id', $params['id']);
		$this->db->update($this->table_ketelitian, $params);
		$id = $params['id'];

		return $id;
	}

	public function saveD($params = array()){
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$this->db->insert($this->table_ketelitian_d, $params);
		$id = $this->db->insert_id();
		return $id;
	}

	public function updateHasil($params = array())
	{
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->where('id', $params['id']);
		$this->db->update($this->table_ketelitian, $params);
		$id = $params['id'];
		return $id;
	}

	public function count_ketelitian_vacancy ($employeeId,$vacancyId){
		$query = "SELECT id FROM t_ketelitian WHERE deleted_at is null and is_aktif = 1 and m_employee_id = ".$employeeId." and m_vacancy_id = ".$vacancyId;
		return $this->db->query($query)->result();
	}

	public function get_data_ketelitian($id){
		$q = "SELECT * from t_ketelitian WHERE deleted_at is null and id = ".$id.";";
		return $this->db->query($q)->result();
	}

	public function get_data_ketelitian_d($id){
		$q = "SELECT *,(select no_soal from m_ketelitian where id = m_ketelitian_id) as no_soal,(select soal from m_ketelitian where id = m_ketelitian_id) as soal from t_ketelitian_d WHERE deleted_at is null and t_ketelitian_id = ".$id.";";
		return $this->db->query($q)->result();
	}

}
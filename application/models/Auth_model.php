<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model{

	private $table_user;
	public function __construct()
	{
		parent::__construct();
		$this->table_user 		= 'm_user';
		$this->table_employee 	= 'm_employee';
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		// if(isset($params['username']) && isset($params['email'])){
		// 	$this->db->where("(A.username = '".$params['username']."' OR B.id_card = '".$params['username']."' OR A.email = '".$params['email']."')");
		// }else if(isset($params['username'])){
		// 	$this->db->where("(A.username = '".$params['username']."' OR B.id_card = '".$params['username']."')");
		// }else if(isset($params['email'])){
		// 	$this->db->where('A.email', $params['email']);
		// }

		if(isset($params['username'])){
			if(is_numeric($params['username'])){
				$this->db->where('B.id_card', $params['username']);
			}else{
				$this->db->where('A.username', $params['username']);
			}

		}
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['password'])){
			$this->db->where('A.password', $params['password']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*, B.full_name, B.phone_number');
		}

		$this->db->from($this->table_user.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		$result = $this->db->get()->row();
		return $result;
	}

	public function validate($params = array())
	{

		if(isset($params['password'])){
			$this->db->or_where('A.password', $params['password']);
		}
		if(isset($params['username'])){
			$this->db->or_where('A.username', $params['username']);
		}
		if(isset($params['email'])){
			$this->db->or_where('A.email', $params['email']);
		}
		if(isset($params['id_card'])){
			$this->db->or_where('B.id_card', $params['id_card']);
		}

		$this->db->where('A.is_active', 1);
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_user.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		$result = $this->db->get()->row();
		return $result;
	}

	public function validate_employee($params = array())
	{

		if(isset($params['id_card'])){
			$this->db->where('B.id_card', $params['id_card']);
		}

		// $this->db->where('A.is_active', 1);
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_user.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		$result = $this->db->get()->row();
		return $result;
	}

	public function check($params = array())
	{

		if(isset($params['phone_number'])){
			$this->db->where('B.phone_number', $params['phone_number']);
		}
		if(isset($params['id_card'])){
			$this->db->where('B.id_card', $params['id_card']);
		}

		$this->db->where('A.is_active', 1);
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_user.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if (isset($params['id'])) {
			$params['is_active']  = 1;
			$params['updated_at'] = date('Y-m-d H:i:s');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_user, $params);
			$id = $params['id'];
		}else{
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = 1;
			$params['updated_by'] = 1;
			
			$this->db->insert($this->table_user, $params);
			$id = $this->db->insert_id();
			
			$this->db->where('id', $id);
			$this->db->update($this->table_user, array('created_by' => $id, 'updated_by' => $id));
		}
		return $id;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{

	function __construct()
	{
		parent::__construct();

		$this->table_user 		= 'm_user';
		$this->table_role 		= 'm_role';
		$this->table_branch 	= 'm_branch';
		$this->table_company	= 'm_company';
		$this->table_city		= 'm_city';
		$this->table_employee	= 'm_employee';
		$this->table_province	= 'm_province';
		$this->table_site		= 'm_site';
		$this->table_struktur_approval		= 'm_struktur_jabatan';
		$this->table_struktur_level			= 'm_employee_level';
	}

	public function get($params = array())
	{
		
		// $this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['username'])){
			$this->db->like('A.username', $params['username']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->from($this->table_user.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		// $this->db->join($this->table_struktur_approval.' C', 'B.struktur_id = C.id', 'left');
		// $this->db->join($this->table_struktur_level.' D', 'C.level_id = D.id', 'left');

		$result = $this->db->get()->row();
		return $result;
	}

	public function preview($params = array())
	{
		$this->db->where('A.id', $params['id']);
		$this->db->where('A.is_active', 1);
		$this->db->from($this->table_user.' A');
		$this->db->join($this->table_employee.' G', 'A.employee_id = G.id', 'left');
		$this->db->join($this->table_role.' B', 'A.role_id = B.id');
		$this->db->join($this->table_branch.' C', 'G.branch_id = C.id', 'left');
		$this->db->join($this->table_user.' D', 'A.created_by = D.id');
		$this->db->join($this->table_user.' E', 'A.updated_by = E.id');
		$this->db->join($this->table_company.' F', 'A.company_id = F.id', 'left');
		// $this->db->join($this->table_struktur_approval.' J', 'G.struktur_id = J.id', 'left');
		// $this->db->join($this->table_struktur_level.' K', 'J.level_id = K.id', 'left');

		$this->db->select('A.id, G.full_name, G.id_card, G.email, A.created_at, A.updated_at, B.name AS role_name, C.name AS branch_name, D.full_name AS creator, E.full_name AS updater, F.name AS company_name');

		$result = $this->db->get()->row();
		return $result;
	}

	public function employee_gets($params = array())
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_user.' B', 'A.id = B.employee_id', 'left');
		$this->db->where('A.is_active', 1);
		if(isset($params['not_user'])){
			$this->db->where('B.id IS NULL');
		}

		if(isset($params['site_id'])){
			if($params['site_id'] != ''){
				$this->db->where('A.site_id', $params['site_id']);
			}
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		$result = $this->db->get()->result();
		return $result;
	}

	public function employee_get($params = array())
	{
		$this->db->from($this->table_employee.' A');
		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_user.' A');
		$this->db->join($this->table_employee.' H', 'A.employee_id = H.id', 'left');
		$this->db->join($this->table_role.' B', 'A.role_id = B.id', 'left');
		$this->db->join($this->table_branch.' C', 'H.branch_id = C.id', 'left');
		$this->db->join($this->table_company.' D', 'H.company_id = D.id', 'left');
		$this->db->join($this->table_city.' E', 'H.city_domisili_id = E.id', 'left');
		$this->db->join($this->table_province.' G', 'E.province_id = G.id', 'left');
		$this->db->join($this->table_site.' I', 'H.site_id = I.id', 'left');
		// $this->db->join($this->table_struktur_approval.' J', 'H.struktur_id = J.id', 'left');
		// $this->db->join($this->table_struktur_level.' K', 'J.level_id = K.id', 'left');

		if(isset($params['role_in'])){
			$this->db->where_in('A.role_id', $params['role_in']);
		}

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['notcomplete'])){
			$this->db->join($this->table_employee.' F', 'A.employee_id = F.id', 'left');
			$this->db->where('F.id IS NULL');
		}
		// if(!isset($params['all'])){
		// 	$this->db->where('A.is_active', 1);
		// }
		
		if(isset($params['site_id'])){
			$this->db->where('H.site_id', $params['site_id']);
			$this->db->where('H.is_active', 1);
		}
		
		if(isset($params['session_branch'])){
			$this->db->where('G.branch_id', $params['session_branch']);
		}
		
		if(isset($params['role'])){
			$this->db->where('A.role_id', $params['role']);
		}
		
		if(isset($params['role_id'])){
			$this->db->where('A.role_id', 4);
// 			$this->db->where('H.site_id', 2);			
		}

		if(isset($params['supervisor_id'])){
			$this->db->where('A.role_id', 6);
			$this->db->where('H.site_id', 2);
		}

		if(isset($params['recruitment'])){
			$this->db->where_in('A.role_id', array(22,23,24,25,26,27,28));
		}
		
		if(isset($params['role_sales'])){
			$this->db->where_in('A.role_id', array(29));
		}

		if(isset($params['applicant'])){
			$this->db->where('A.role_id', 1);
		}

		if(isset($params['not_applicant'])){
			$this->db->where('A.role_id !=', 1);
		}

		if(isset($params['not_employee'])){
			$this->db->where('A.employee_id IS NULL');
		}
		
		if(isset($params['username'])){
			$this->db->like('A.username', $params['username']);
		}

		if(isset($params['email'])){
			$this->db->like('A.email', $params['email']);
		}

		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}

		if(isset($params['created_at'])){
			$this->db->like('DATE_FORMAT(A.created_at, "%d/%m/%Y")', $params['created_at']);
		}

		if(isset($params['updated_at'])){
			$this->db->like('DATE_FORMAT(A.updated_at, "%d/%m/%Y")', $params['updated_at']);
		}

		if(isset($params['role_name'])){
			$this->db->like('B.name', $params['role_name']);
		}

		if(isset($params['branch_name'])){
			if($params['branch_name'] != ''){
				$this->db->like('C.name', $params['branch_name']);
			}
		}

		if(isset($params['company_name'])){
			if($params['company_name'] != ''){
				$this->db->like('D.name', $params['company_name']);
			}
		}

		if(isset($params['company_code'])){
			if($params['company_code'] != ''){
				$this->db->where('D.code', $params['company_code']);
			}
		}

		if(isset($params['site_name'])){
			if($params['site_name'] != ''){
				$this->db->like('I.name', $params['site_name']);
			}
		}

		if(isset($params['phone_number'])){
			if($params['phone_number'] != ''){
				$this->db->like('H.phone_number', $params['phone_number']);
			}
		}

		if(isset($params['city_domisili'])){
			if($params['city_domisili'] != ''){
				$this->db->like('E.name', $params['city_domisili']);
			}
		}

		if(isset($params['province_name'])){
			if($params['province_name'] != ''){
				$this->db->like('G.name', $params['province_name']);
			}
		}

		if(isset($params['id_card'])){
			if($params['id_card'] != ''){
				$this->db->like('H.id_card', $params['id_card']);
			}
		}

		if(isset($params['employee_name'])){
			if($params['employee_name'] != ''){
				$this->db->like('H.full_name', $params['employee_name']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
		    if($params['orderby'] == 'date_regitration'){
				$params['orderby'] = 'A.created_at';
			}
			if($params['orderby'] == 'last_login'){
				$params['orderby'] = 'A.updated_at';
			}
			if($params['orderby'] == 'struktur_approval'){
				$params['orderby'] = 'K.id';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.updated_at', 'DESC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*, B.name AS role_name, C.name AS branch_name');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function gets_client($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_user.' A');
		$this->db->join($this->table_site.' B', 'A.site_id = B.id', 'left');
		$this->db->join($this->table_company.' C', 'B.company_id = C.id', 'left');
		$this->db->join($this->table_branch.' D', 'B.branch_id = D.id', 'left');

		$this->db->where('A.is_active', 1);
		
		if(isset($params['site_id'])){
			$this->db->where('B.site_id', $params['site_id']);
			$this->db->where('B.is_active', 1);
		}

		if(isset($params['role_id'])){
			$this->db->where('A.role_id', $params['role_id']);
		}
		
		if(isset($params['username'])){
			$this->db->like('A.username', $params['username']);
		}

		if(isset($params['email'])){
			$this->db->like('A.email', $params['email']);
		}

		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}

		if(isset($params['site_name'])){
			if($params['site_name'] != ''){
				$this->db->like('B.name', $params['site_name']);
			}
		}
		
		if(isset($params['company_code'])){
			if($params['company_code'] != ''){
				$this->db->where('C.code', $params['company_code']);
			}
		}

		if(isset($params['branch_name'])){
			if($params['branch_name'] != ''){
				$this->db->like('D.name', $params['branch_name']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
		    if($params['orderby'] == 'date_regitration'){
				$params['orderby'] = 'A.created_at';
			}
			if($params['orderby'] == 'last_login'){
				$params['orderby'] = 'A.updated_at';
			}
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.updated_at', 'DESC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*, B.name AS role_name, C.name AS branch_name');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function gets_developer($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_user.' A');
		$this->db->join($this->table_company.' B', 'A.company_id = B.id', 'left');

		$this->db->where('A.is_active', 1);

		if(isset($params['role_id'])){
			$this->db->where('A.role_id', $params['role_id']);
		}
		
		if(isset($params['username'])){
			$this->db->like('A.username', $params['username']);
		}

		if(isset($params['email'])){
			$this->db->like('A.email', $params['email']);
		}

		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}
		
		if(isset($params['company_code'])){
			if($params['company_code'] != ''){
				$this->db->where('C.code', $params['company_code']);
			}
		}

		if(isset($params['company_name'])){
			$this->db->like('A.name', $params['company_name']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.updated_at', 'DESC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array(),  $employeeId = '')
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_user, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_user, $params);
			$id = $this->db->insert_id();
		}

		return $id;
	}

	public function get_applicant($params = array())
	{
		
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$this->db->from($this->table_user.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function get_user($params = array())
	{
		
		// $this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select("CASE WHEN B.full_name != '' THEN B.full_name ELSE A.full_name END AS full_name, 
				CASE WHEN B.id_card != '' THEN B.id_card ELSE A.username END AS username");
		}
		
		$this->db->from($this->table_user.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');

		$result = $this->db->get()->row();
		return $result;
	}
}
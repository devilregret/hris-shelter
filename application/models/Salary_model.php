<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salary_model extends CI_Model{
	private $table_user;
	private $table_salary;
	private $table_site;
	private $table_position;
	private $table_salary_detail;
	private $table_employee;

	public function __construct()
	{
		parent::__construct();
		$this->table_user    		= 'm_user';
		$this->table_salary   		= 'm_salary';
		$this->table_site   		= 'm_site';
		$this->table_position  		= 'm_position';
		$this->table_salary_detail	= 'm_salary_detail';
		$this->table_employee		= 'm_employee';
	}

	public function position($params = array()){
		$this->db->where('A.is_active', 1);
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_position.' B', ' A.position_id = B.id');

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('B.id, B.name');
		}

		$this->db->group_by('B.id');
		
		$result = $this->db->get()->result_array();
		return $result;
	}


	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if(isset($params['position_id'])){
			$this->db->where('A.position_id', $params['position_id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_salary.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function preview($params = array())
	{
		$this->db->where('A.id', $params['id']);
		$this->db->where('A.is_active', 1);
		$this->db->from($this->table_salary.' A');
		$this->db->join($this->table_user.' B', 'A.created_by = B.id');
		$this->db->join($this->table_user.' C', 'A.updated_by = C.id');
		$this->db->join($this->table_site.' D', 'A.site_id = D.id');
		$this->db->join($this->table_position.' E', 'A.position_id = E.id');

		$this->db->select('A.*, B.full_name AS creator, C.full_name AS updater, D.name AS site_name, E.name AS position');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_salary.' A');
		$this->db->join($this->table_site.' B', 'A.site_id = B.id');
		$this->db->join($this->table_position.' C', 'A.position_id = C.id');
		
		$this->db->where('A.is_active', 1);

		if(isset($params['site_name'])){
			$this->db->like('B.name', $params['site_name']);
		}
		
		if(isset($params['position'])){
			$this->db->like('C.name', $params['position']);
		}
		
		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.id', 'ASC');
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_salary, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_salary, $params);
			$id = $this->db->insert_id();
		}
		
		return $id;
	}

	public function detail_gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_salary_detail.' A');
		
		$this->db->where('A.is_active', 1);

		if (isset($params['salary_id'])){
			$this->db->where('A.salary_id', $params['salary_id']);
		}

		if (isset($params['code'])){
			$this->db->where('A.code', $params['code']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function detail_get($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_salary_detail.' A');
		
		$this->db->where('A.is_active', 1);

		if (isset($params['salary_id'])){
			$this->db->where('A.salary_id', $params['salary_id']);
		}

		if (isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if (isset($params['code'])){
			$this->db->where('A.code', $params['code']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		
		$result = $this->db->get()->row();
		return $result;
	}

	public function detail_save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_salary_detail, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_salary_detail, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}

	public function detail_delete($params = array())
	{
		$id = false;
		if(isset($params['id'])){
			$id = $this->db->delete($this->table_salary_detail, array('id' => $params['id']));
		}
		return $id;
	}
}
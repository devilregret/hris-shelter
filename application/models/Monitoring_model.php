<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring_model extends CI_Model{
	private $table_employee;
	private $table_contract;
	private $table_company;
	private $table_site;
	private $table_position;
	private $table_user;

	public function __construct()
	{
		parent::__construct();
		$this->table_contract   = 't_employee_contract';
		$this->table_employee   = 'm_employee';
		$this->table_company	= 'm_company';
		$this->table_site		= 'm_site';
		$this->table_position	= 'm_position';
		$this->table_user		= 'm_user';
	}

	public function get_work_period($params = array()){
		$this->db->from('t_employee_contract A');
		$this->db->select('MIN(contract_start) AS contract_start, MAX(contract_end) AS contract_end');
		$this->db->where('A.employee_id', $params['id']);

		$result = $this->db->get()->row();
		return $result;
	}
	public function get($params = array()){
		$this->db->where('A.id', $params['id']);
		$this->db->where('A.is_active', 1);
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_position.' B', 'A.position_id = B.id', 'left');
		$this->db->join($this->table_site.' C', 'A.site_id = C.id', 'left');
		$this->db->join($this->table_company.' D', 'A.company_id = D.id', 'left');

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_site.' B', 'A.site_id = B.id', 'left');
		$this->db->join($this->table_company.' C', 'A.company_id = C.id', 'left');
		$this->db->join($this->table_contract.' D', 'A.id = D.employee_id AND D.id = (SELECT id FROM t_employee_contract WHERE employee_id = A.id AND is_active = 1 ORDER BY contract_start DESC LIMIT 1)');
		$this->db->join($this->table_position.' E', 'A.position_id = E.id', 'left');
		$this->db->join($this->table_user.' F', 'B.pic_id_1 = F.id', 'left');
		$this->db->join($this->table_user.' G', 'B.pic_id_2 = G.id', 'left');
		$this->db->join($this->table_user.' H', 'B.pic_id_3 = H.id', 'left');
		$this->db->join($this->table_company.' I', 'B.company_id = I.id', 'left');

		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('B.branch_id', $params['branch_id']);
			}
		}
		if(isset($params['status'])){
			$this->db->where('A.status', $params['status']);
		}		
		if(isset($params['status_nonjob'])){
			$this->db->where('A.status_nonjob', $params['status_nonjob']);
		}

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if(isset($params['not_site_id'])){
			$this->db->where('A.site_id != ', $params['not_site_id']);
		}

		if(isset($params['status_approval'])){
			$this->db->where('A.status_approval', $params['status_approval']);
		}

		if(isset($params['id_card'])){
			$this->db->like('A.id_card', $params['id_card']);
		}

		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}

		if(isset($params['email'])){
			$this->db->like('A.email', $params['email']);
		}

		if(isset($params['employee_number'])){
			$this->db->like('A.employee_number', $params['employee_number']);
		}

		if(isset($params['phone_number'])){
			$this->db->like('A.phone_number', $params['phone_number']);
		}

		if(isset($params['health_insurance_note'])){
			$this->db->like('A.health_insurance_note', $params['health_insurance_note']);
		}

		if(isset($params['ptkp'])){
			$this->db->like('A.ptkp', $params['ptkp']);
		}

		if(isset($params['bank_name'])){
			$this->db->like('A.bank_name', $params['bank_name']);
		}

		if(isset($params['bank_account'])){
			$this->db->like('A.bank_account', $params['bank_account']);
		}

		if(isset($params['bank_account_name'])){
			$this->db->like('A.bank_account_name', $params['bank_account_name']);
		}

		if(isset($params['warning_letter'])){
			$this->db->like('A.warning_letter', $params['warning_letter']);
		}

		if(isset($params['company_name'])){
		    if($params['company_name'] != ''){
			    $this->db->like('C.name', $params['company_name']);
		    }
		}

		if(isset($params['company_code'])){
		    if($params['company_code'] != ''){
			    $this->db->like('C.code', $params['company_code']);
		    }
		}

		if(isset($params['business_name'])){
		    if($params['business_name'] != ''){
			    $this->db->like('I.name', $params['business_name']);
		    }
		}

		if(isset($params['company_name'])){
		    if($params['company_name'] != ''){
			    $this->db->like('C.name', $params['company_name']);
		    }
		}
		if(isset($params['contract_type'])){
			if($params['contract_type'] != ''){
				$this->db->like("DATE_FORMAT(D.contract_end, '%d/%m/%Y')", $params['contract_type']);
			}
		}

		if(isset($params['position'])){
			if($params['position'] != ''){
				$this->db->like('E.name', $params['position']);
			}
		}

		if(isset($params['contract_end'])){
			if($params['contract_end'] != ''){
				$this->db->like('D.contract_end', $params['contract_end']);
			}
		}

		if(isset($params['ro'])){
			if($params['ro'] != ''){
				$this->db->like("CONCAT_WS(', ', F.full_name, G.full_name, H.full_name)", $params['ro']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'contract_end'){
				$params['orderby'] = 'D.contract_end';
			}

			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('D.contract_end', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*, B.name AS site_name, C.code AS company_code, D.contract_start, D.contract_end, D.contract_type, E.name AS position_name');
		}

		$result = $this->db->get()->result();
		return $result;
	}
}
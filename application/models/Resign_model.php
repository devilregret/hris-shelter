<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resign_model extends CI_Model{
	private $table_employee;
	private $table_approval;
	private $table_company;
	private $table_site;
	private $table_position;
	private $table_user;

	public function __construct()
	{
		parent::__construct();
		$this->table_approval   = 't_approval_history';
		$this->table_employee   = 'm_employee';
		$this->table_company	= 'm_company';
		$this->table_site		= 'm_site';
		$this->table_position	= 'm_position';
		$this->table_user		= 'm_user';
	}

	public function get($params = array()){
		$this->db->from($this->table_approval.' A');

		$this->db->where('A.is_active', 1);
		
		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}
		$this->db->limit(1);
		$this->db->order_by('A.id', 'DESC');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_approval.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id');
		$this->db->join($this->table_company.' C', 'B.company_id = C.id', 'left');
		$this->db->join($this->table_site.' D', 'A.site_id = D.id', 'left');
		$this->db->join($this->table_position.' E', 'B.position_id = E.id', 'left');
		$this->db->join($this->table_user.' F', 'D.pic_id_1 = F.id', 'left');
		$this->db->join($this->table_user.' G', 'D.pic_id_2 = G.id', 'left');
		$this->db->join($this->table_user.' H', 'D.pic_id_3 = H.id', 'left');

		$this->db->where('A.is_active', 1);

		if(isset($params['branch_id'])){
			if($params['branch_id'] != ''){
				$this->db->where('D.branch_id', $params['branch_id']);
			}
		}

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if(isset($params['status_approval'])){
			$this->db->where('A.status_approval', $params['status_approval']);
		}

		if(isset($params['full_name'])){
			$this->db->like('B.full_name', $params['full_name']);
		}

		if(isset($params['id_card'])){
			$this->db->like('B.id_card', $params['id_card']);
		}

		if(isset($params['employee_number'])){
			$this->db->like('B.employee_number', $params['employee_number']);
		}

		if(isset($params['company_name'])){
			if($params['company_name'] != ""){
				$this->db->like('C.name', $params['company_name']);
			}
		}

		if(isset($params['site_name'])){
			if($params['site_name'] != ""){
				$this->db->like('D.name', $params['site_name']);
			}
		}

		if(isset($params['position'])){
			if($params['position'] != ""){
				$this->db->like('E.name', $params['position']);
			}
		}

		if(isset($params['resign_submit'])){
			if($params['resign_submit'] != ""){
				$this->db->like("DATE_FORMAT(B.resign_submit, '%d/%m/%Y')", $params['resign_submit']);
			}
		}

		if(isset($params['contract_end'])){
			if($params['contract_end'] != ""){
				$this->db->like("DATE_FORMAT(A.updated_at, '%d/%m/%Y')", $params['contract_end']);
			}
		}

		if(isset($params['ro'])){
			if($params['ro'] != ""){
				$this->db->like("CONCAT_WS(', ', F.full_name, G.full_name, H.full_name)", $params['ro']);
			}
		}

		if(isset($params['note'])){
			$this->db->like('A.note', $params['note']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			if($params['orderby'] == 'updated_at'){
				$params['orderby'] = 'A.updated_at';
			}
			if($params['orderby'] == 'resign_submit'){
				$params['orderby'] = 'B.resign_submit';
			}
			
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.updated_at', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('*');
		}
		$this->db->group_by('A.employee_id');
		$result = $this->db->get()->result();
		return $result;
	}
}
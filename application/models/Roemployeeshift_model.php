<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roemployeeshift_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->table_user			= 'm_user';
		$this->table_employee		= 'm_employee';
		$this->table_position		= 'm_position';
		$this->table_site			= 'm_site';
		$this->table_employee_shift	= 't_employee_shift';
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_employee_shift.' B', 'A.id = B.employee_id AND B.is_active = 1', 'left');
		$this->db->join($this->table_position.' C', 'A.position_id = C.id ', 'left');
		$this->db->where('A.is_active', 1);
		$this->db->where('A.status_approval', 3);

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}

		if(isset($params['employee_number'])){
			$this->db->like('A.employee_number', $params['employee_number']);
		}

		if(isset($params['shift'])){
			if($params['shift'] != ''){
				$this->db->like('B.shift', $params['shift']);
			}
		}

		if(isset($params['position_name'])){
			if($params['position_name'] != ''){
				$this->db->like('C.name', $params['position_name']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.full_name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		if(isset($params['is_active'])){
			$this->db->where('A.is_active', $params['is_active']);
		
		}else{
			$this->db->where('A.is_active', 1);
		}

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_employee_shift.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_employee_shift, $params);
			$id = $params['id'];
		} else {
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_employee_shift, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
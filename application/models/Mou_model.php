<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mou_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();

		$this->table_mou		= 'm_mou';
		$this->table_company 	= 'm_company';
		$this->table_province 	= 'm_province';
		$this->table_city 		= 'm_city';
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['id'])){
			$this->db->where('A.id', $params['id']);
		}
		
		$this->db->from($this->table_mou.' A');
		$this->db->select('A.*');

		$result = $this->db->get()->row();
		return $result;
	}

	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_mou.' A');
		$this->db->join($this->table_company.' B', 'A.begining_company_id = B.id', 'left');
		$this->db->join($this->table_company.' C', 'A.now_company_id = C.id', 'left');
		$this->db->join($this->table_province.' D', 'A.province_id = D.id', 'left');
		$this->db->join($this->table_city.' E', 'A.city_id = E.id', 'left');
	
		$this->db->where('A.is_active', 1);
		if(isset($params['onprocess'])){
			$this->db->not_like('A.status_registration', $params['onprocess']);
		}
		if(isset($params['status_mou'])){
			$this->db->like('A.status_mou', $params['status_mou']);
		}
		if(isset($params['company_name'])){
			$this->db->like('A.company_name', $params['company_name']);
		}
		if(isset($params['company_code'])){
			$this->db->like('C.code', $params['company_code']);
		}
		if(isset($params['province_name'])){
			$this->db->like('D.name', $params['province_name']);
		}
		if(isset($params['city_name'])){
			$this->db->like('E.name', $params['city_name']);
		}
		if(isset($params['crm'])){
			$this->db->like('A.crm', $params['crm']);
		}
		if(isset($params['status_registration'])){
			$this->db->like('A.status_registration', $params['status_registration']);
		}
		if(isset($params['contract_start'])){
			if($params['contract_start'] != ''){
				$this->db->like("DATE_FORMAT(A.contract_start, '%d/%m/%Y')", $params['contract_start']);
			}
		}
		if(isset($params['contract_end'])){
			if($params['contract_end'] != ''){
				$this->db->like("DATE_FORMAT(A.contract_end, '%d/%m/%Y')", $params['contract_end']);
			}
		}
		if(isset($params['headcount'])){
			if($params['headcount'] != ''){
				$this->db->like("A.headcount", $params['headcount']);
			}
		}
		
		// if(isset($params['name_sender'])){
		// 	$this->db->like('A.name_sender', $params['name_sender']);
		// }

		// if(isset($params['subject'])){
		// 	$this->db->like('A.subject', $params['subject']);
		// }

		if ($return_count){
			return $this->db->count_all_results();
		}

		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}

		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}

		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.created_at', 'DESC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function save($params = array())
	{
		$id = false;
		if ($params['id'] != '') {
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->where('id', $params['id']);
			$this->db->update($this->table_mou, $params);
			$id = $params['id'];
		}else{
			$params['is_active']  = 1;
			$params['created_at'] = date('Y-m-d H:i:s');
			$params['created_by'] = $this->session->userdata('user_id');
			$params['updated_at'] = date('Y-m-d H:i:s');
			$params['updated_by'] = $this->session->userdata('user_id');
			$this->db->insert($this->table_mou, $params);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}
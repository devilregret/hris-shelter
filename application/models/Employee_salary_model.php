<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_salary_model extends CI_Model{

	private $table_employee_salary;

	public function __construct()
	{
		parent::__construct();
		$this->table_user			= 'm_user';
		$this->table_employee		= 'm_employee';
		$this->table_position		= 'm_position';
		$this->table_site			= 'm_site';
		$this->table_employee_salary= 't_employee_salary';
		$this->table_salary_detail 	= 't_employee_salary_detail';
	}
	
	public function gets($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_employee_salary.' B', 'A.id = B.employee_id', 'left');
		$this->db->join($this->table_position.' D', 'A.position_id = D.id ', 'left');
		$this->db->where('A.is_active', 1);
		$this->db->where('A.status_approval', 3);

		if(isset($params['site_id'])){
			$this->db->where('A.site_id', $params['site_id']);
		}

		if(isset($params['full_name'])){
			$this->db->like('A.full_name', $params['full_name']);
		}

		if(isset($params['id_card'])){
			$this->db->like('A.id_card', $params['id_card']);
		}

		if(isset($params['employee_number'])){
			$this->db->like('A.employee_number', $params['employee_number']);
		}

		if(isset($params['basic_salary'])){
			if($params['basic_salary'] != ''){
				$this->db->like('B.basic_salary', $params['basic_salary']);
			}
		}

		if(isset($params['position_name'])){
			if($params['position_name'] != ''){
				$this->db->like('D.name', $params['position_name']);
			}
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('A.full_name', 'ASC');
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function get($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['employee_id'])){
			$this->db->where('A.id', $params['employee_id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}


		$this->db->from($this->table_employee.' A');
		$this->db->join($this->table_employee_salary.' B', 'A.id = B.employee_id', 'left');

		$result = $this->db->get()->row();
		return $result;
	}

	public function get_salary($params = array())
	{
		$this->db->where('A.is_active', 1);

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}


		$this->db->from($this->table_employee_salary.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function save($params = array()){
		
		$params['is_active']  = 1;
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('user_id');
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['updated_by'] = $this->session->userdata('user_id');
		$this->db->replace($this->table_employee_salary, $params);
		return $this->db->insert_id();
	}

	public function gets_detail($params = array(), $return_count = FALSE)
	{
		$this->db->from($this->table_salary_detail.' A');
		$this->db->join($this->table_employee.' B', 'A.employee_id = B.id', 'left');
		
		if(isset($params['site_id'])){
			$this->db->where('B.site_id', $params['site_id']);
		}

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if ($return_count){
			return $this->db->count_all_results();
		}
		
		if (isset($params['limit'])){
			$this->db->limit($params['limit']);
		}
		
		if (isset($params['page'])){
			$this->db->offset($params['page']);
		}
		
		if (isset($params['orderby']) && isset($params['order'])){
			$this->db->order_by($params['orderby'], $params['order']);
		}else{
			$this->db->order_by('B.full_name', 'ASC');
		}

		if(isset($params['groupby'])){
			$this->db->group_by($params['groupby']);
		}

		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function get_detail($params = array())
	{

		if(isset($params['employee_id'])){
			$this->db->where('A.employee_id', $params['employee_id']);
		}

		if(isset($params['name'])){
			$this->db->where('A.name', $params['name']);
		}
		
		if(isset($params['columns'])){
			$this->db->select($params['columns']);
		}else{
			$this->db->select('A.*');
		}

		$this->db->from($this->table_salary_detail.' A');

		$result = $this->db->get()->row();
		return $result;
	}

	public function delete_detail($params = array())
	{
		$id = false;
		if(isset($params['employee_id'])){
			$id = $this->db->delete($this->table_salary_detail, array('employee_id' => $params['employee_id']));
		}
		return $id;
	}

	public function save_detail($params = array())
	{
		$this->db->insert($this->table_salary_detail, $params);
		$id = $this->db->insert_id();
		return $id;
	}
}
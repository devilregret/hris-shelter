<?php

if(!defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

/**
 * ========================================================================
 * LAYOUT HELPER
 * ========================================================================
 * This Helper contain functions related to layout/View
 * 
 * example:
 *      - function to construct css
 *      - function to construct js
 * 
 */

/**
 * Asset Helper
 * Used to Load JS, CSS, and Image 
 * ============================================================================
 */

function asset_url($uri = '')
{
    return base_url('/asset/') . $uri;
}

function asset_img($uri = '', $tag = FALSE)
{
    if ($tag)
    {
        return '<img src="' . asset_url('img/' . $uri) . '" alt="' . $tag . '">';
    }
    else
    {
        return asset_url('img/' . $uri);
    }
}

function asset_js($uri, $tag = FALSE)
{
    return '<script type="text/javascript" src="' . asset_url($uri) . '"></script>';
}

function asset_css($uri)
{
    return '<link href="' . asset_url($uri) . '" type="text/css" rel="stylesheet" />';
}

function message_box($message, $type)
{
    return '<div class="alert alert-'.$type.' col-sm-12">
              <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>'.$message.'
            </div>';
}

function set_config_pagination(){
    $config['next_link'] = '&raquo;';
	$config['prev_link'] = '&laquo;';

	$config['attributes'] = array('class' => 'page-link');
	$config['full_tag_open'] = '<ul class="pagination">';
	$config['full_tag_close'] = '</ul>';
	$config['num_tag_open'] = '<li class="paginate_button page-item  ">';
	$config['num_tag_close'] = '</li>';
	$config['cur_tag_open'] = '<li class="paginate_button page-item active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0" class="page-link">';
	$config['cur_tag_close'] = '</a></li>';
	$config['prev_tag_open'] = '<li class="paginate_button page-item ">';
	$config['prev_tag_close'] = '</li>';
	$config['next_tag_open'] = '<li class="paginate_button page-item " >';
	$config['next_tag_close'] = '</li>';
	$config['use_page_numbers'] = TRUE;
    $config['page_query_string'] = TRUE;
    $config['query_string_segment'] = 'page';

    return $config;
}

function generate_password($password){
    return md5('SHELTER-'.$password.'-SHELTER');
}

function replace_null($value)
{
    $return = '';
    if(!is_null($value)){
        $return = $value;
    }
    return $return;
}

function ina_to_date($date){
    $date = str_replace(" januari ", "-01-", strtolower($date));
    $date = str_replace(" februari ", "-02-", strtolower($date));
    $date = str_replace(" maret ", "-03-", strtolower($date));
    $date = str_replace(" april ", "-04-", strtolower($date));
    $date = str_replace(" mei ", "-05-", strtolower($date));
    $date = str_replace(" juni ", "-06-", strtolower($date));
    $date = str_replace(" juli ", "-07-", strtolower($date));
    $date = str_replace(" agustus ", "-08-", strtolower($date));
    $date = str_replace(" september ", "-09-", strtolower($date));
    $date = str_replace(" oktober ", "-10-", strtolower($date));
    $date = str_replace(" november ", "-11-", strtolower($date));
    $date = str_replace(" desember ", "-12-", strtolower($date));
    $return = substr($date, 6, 4)."-".substr($date, 3, 2)."-".substr($date, 0, 2);
    return $return;
}

function format_date_ina($date){
    $return = '';
    if($date){
        $return = substr($date, 8, 2)." ".get_month(substr($date, 5, 2))." ".substr($date, 0, 4);
    }
    return $return;
}

function get_month($month){
    switch ($month){
        case '01': 
            return "Januari";
            break;
        case '02':
            return "Februari";
            break;
        case '03':
            return "Maret";
            break;
        case '04':
            return "April";
            break;
        case '05':
            return "Mei";
            break;
        case '06':
            return "Juni";
            break;
        case '07':
            return "Juli";
            break;
        case '08':
            return "Agustus";
            break;
        case '09':
            return "September";
            break;
        case '10':
            return "Oktober";
            break;
        case '11':
            return "November";
            break;
        case '12':
            return "Desember";
            break;
    }
} 
function get_roman_month($month){
    switch ($month){
        case '01': 
            return "I";
            break;
        case '02':
            return "II";
            break;
        case '03':
            return "III";
            break;
        case '04':
            return "IV";
            break;
        case '05':
            return "V";
            break;
        case '06':
            return "VI";
            break;
        case '07':
            return "VII";
            break;
        case '08':
            return "VIII";
            break;
        case '09':
            return "IX";
            break;
        case '10':
            return "X";
            break;
        case '11':
            return "XI";
            break;
        case '12':
            return "XII";
            break;
    }
}

function get_day($day){
    switch ($day){
        case 'Sunday': 
            return "Minggu";
            break;
        case 'Monday':
            return "Senin";
            break;
        case 'Tuesday':
            return "Selasa";
            break;
        case 'Wednesday':
            return "Rabu";
            break;
        case 'Thursday':
            return "Kamis";
            break;
        case 'Friday':
            return "Jumat";
            break;
        case 'Saturday':
            return "Sabtu";
            break;
    }
}

function rupiah_round($value = 0){
    if($value ==  ""){
        $value = 0.00;
    }
    $rupiah = number_format($value,0,',','.');
    return $rupiah;
}

function rupiah($value){
    if($value == ""){
        $value = 0;
    }
    $rupiah = number_format($value,2,',','.');
    return $rupiah;
}

function format_rupiah($value){
    if($value == ""){
        $value = 0;
    }
    $rupiah = "Rp " . number_format($value,2,',','.');
    return $rupiah;
}

function nominal_rupiah($value){
    if($value == ""){
        $value = 0;
    }
    $rupiah = "Rp " . number_format($value,0,',','.');
    return $rupiah;
}

function str_contains(string $haystack, string $needle): bool{
    return '' === $needle || false !== strpos($haystack, $needle);
}

function print_prev($print){
    echo ("<pre>");
    print_r($print);
    echo ("</pre>");
}

function evalmath($equation)
{
    $result = 0;
    // sanitize imput
    $equation = preg_replace("/[^a-z0-9+\-.*\/()%]/","",$equation);
    // convert alphabet to $variabel 
    $equation = preg_replace("/([a-z])+/i", "\$$0", $equation); 
    // convert percentages to decimal
    $equation = preg_replace("/([+-])([0-9]{1})(%)/","*(1\$1.0\$2)",$equation);
    $equation = preg_replace("/([+-])([0-9]+)(%)/","*(1\$1.\$2)",$equation);
    $equation = preg_replace("/([0-9]{1})(%)/","\$1/100",$equation);
    $equation = preg_replace("/([0-9]+)(%)/",".\$1",$equation);

    if ( $equation != "" ){
        $result = @eval("return " . $equation . ";" );
    }
    if ($result == null) {
        $result = 0;
    }
    return $result;
}

function check_numeric($value){
    $result = 0;
    if($value == ''){
        $result = 0;
    }else{
        $result = $value;
    }
    return $result;
}
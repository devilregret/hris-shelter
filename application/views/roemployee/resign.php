<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('roemployee/list'); ?>">Daftar Karyawan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-list"></i>&nbsp;<?php echo $employee->full_name; ?></h3>
				</div>
				<div class="card-body">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4" action="<?php echo base_url("roemployee/resign"); ?>" method="POST" enctype="multipart/form-data">
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">No Induk Karyawan </label>
								<div class="col-sm-10">
									:  <strong><?php echo $employee->employee_number; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Nama Lengkap </label>
								<div class="col-sm-10">
									: <strong><?php echo $employee->full_name; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Nomor KTP </label>
								<div class="col-sm-10">
									: <strong><?php echo $employee->id_card; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Tanggal Lahir </label>
								<div class="col-sm-10">
									: <strong><?php echo $employee->date_birth; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Alamat Asal </label>
								<div class="col-sm-10">
									: <strong><?php echo $employee->address_card; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Foto Pengajuan Resign </label>
								<div class="col-sm-10">
									<input type="hidden" class="form-control" name="id_card" value="<?php echo $employee->id_card; ?>">
									<input type="hidden" class="form-control" name="employee_id" value="<?php echo $employee->id; ?>">
									<input type="hidden" class="form-control" name="site_id" value="<?php echo $employee->site_id; ?>">
									<input type="hidden" class="form-control" name="company_id" value="<?php echo $employee->company_id; ?>">
									<input type="hidden" class="form-control" name="position_id" value="<?php echo $employee->position_id; ?>">
									<input type="file" class="custom-file-input upload-document" name="document_resign" accept="image/png, image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile" >Pilih file</label>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Tanggal Resign</label>
								<div class="col-sm-2">
									<input type="date" class="form-control datepicker" name="resign_submit" required>
								</div>
								<div class="col-sm-8">&nbsp;</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Alasan Resign </label>
								<div class="col-sm-4">
									<select class="form-control select2" name="resign_type" required>
										<?php foreach ($list_resign_note as $item) :  ?>
										<option value="<?php echo $item->name;?>"><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-sm-6">&nbsp;</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Catatan </label>
								<div class="col-sm-10">
									<textarea class="form-control" name="resign_note" rows="3" placeholder="Catatan"></textarea>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Catatan Tanggungan </label>
								<div class="col-sm-10">
									<textarea class="form-control" name="resign_burden" rows="3" placeholder="Alasan Tanggungan"></textarea>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
	$(document).ready(function () {
		$('.upload-document').imageUploadResizer({
			max_width: 1200, // Defaults 1000
			max_height: 800, // Defaults 1000
			quality: 1, // Defaults 1
			do_not_resize: ['gif', 'svg'], // Defaults []
		});
	});
	$('.custom-file-input').on('change',function(){
		var fileName = $(this).val();
		$(this).next('.custom-file-label').html(fileName);
	})
</script>
<style type="text/css">
	input[type="text" i], input[type="email" i], input[type="select" i], input[type="date" i], .select2-container, textarea {
    	margin-left: -7px;
	}
</style>
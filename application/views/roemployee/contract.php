<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('roemployee/list'); ?>">Daftar Karyawan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-list"></i>&nbsp;Kontrak <?php echo $employee->full_name; ?></h3>
				</div>
				<div class="card-body">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row form-group contract mt-3">
						<div class="row col-12">
							<label class="col-sm-3">Nama Karyawan</label>
							<label class="col-sm-9">:&nbsp;<?php echo $employee->full_name; ?> </label>
						</div>
						<div class="row col-12">
							<label class="col-sm-3">ID Karyawan </label>
							<label class="col-sm-9">:&nbsp;<?php echo $employee->id_card; ?> </label>
						</div>
						<div class="row col-12">
							<label class="col-sm-3">Alamat Karyawan </label>
							<label class="col-sm-9">:<?php echo $employee->address_card; ?> </label>
						</div>
					</div>
					<div class="row">
						<div class="scroll-panel">
							<table class="table table-bordered table-striped dataTable dtr-inline" role="grid" width="100%">
								<thead>
									<tr role="row" class="text-uppercase">
										<th style="width: 10px;">No</th>
										<th>Tipe Kontrak</th>
										<th>Kontrak Mulai</th>
										<th>Kontrak Selesai</th>
										<th>#</th>
									</tr>
								</thead>
								<tbody>
									<form method="POST" id="contract" action="<?php echo base_url('employee_contract/save_contract'); ?>">
									<?php 
									$i = 1;
									foreach($list_contract as $item): 
									?>
									<tr role="row" <?php if ($i % 2 == 0): echo 'class="odd"'; endif; ?>>
										<td width="50px"><?php echo $i; $i++ ?></td>
										<td><?php echo $item->contract_type; ?></td>
										<td><?php echo $item->contract_start; ?></td>
										<td><?php echo $item->contract_end; ?></td>
										<td width="120px">
											<a onclick="preview_contract(this)" style="cursor:pointer;" class="btn-sm btn-info btn-block" data-href="<?php echo base_url('contract/preview_contract/'.$item->id); ?>">Lihat</a>
											<a onclick="create_pdf(this)" class="btn-sm btn-success btn-block" data-href="<?php echo base_url('contract/pdf/'.$item->id); ?>">PDF</a>
										</td>
									</tr>
									<?php endforeach; ?>
									</form>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</section>
</div>

<script type="text/javascript">
	function preview_contract(el)
	{
		const url = $(el).attr('data-href');
	    const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
	    const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

	    const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	    const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	    const systemZoom = width / window.screen.availWidth;
	    const left = (width - 1000) / 2 / systemZoom + dualScreenLeft
	    const top = (height - 800) / 2 / systemZoom + dualScreenTop
	    const newWindow = window.open(url, 'preview', 
	      `
	      scrollbars=yes,
	      width=${1000 / systemZoom}, 
	      height=${800 / systemZoom}, 
	      top=${top}, 
	      left=${left}
	      `
	    )
	  return false;
	};

	function create_pdf(el)
	{
		const url = $(el).attr('data-href');
	    window.open(url);
	}
</script>
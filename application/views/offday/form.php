<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('offday/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Ubah Data Kota</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("offday/form"); ?>">
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tanggal <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="date" name="date" class="form-control datepicker" value="<?php echo $date; ?>" required>
								</div>
								<div class="col-sm-7"></div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Tipe<span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<select class="form-control select2" name="type" required>
										<option value="Libur Nasional" <?php if($type === 'Libur Nasional'): echo "selected"; endif;?>> Libur Nasional</option>
										<option value="Cuti Bersama" <?php if($type === 'Cuti Bersama'): echo "selected"; endif;?>> Cuti Bersama</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Deskripsi <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<textarea class="form-control" name="description" rows="3" placeholder="Deskripsi" required><?php echo $description; ?></textarea>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
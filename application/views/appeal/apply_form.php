<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('appeal/vacancy'); ?>">Daftar Lowongan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Pertanyaan : <strong><?php echo $vacancy->title; ?> </strong></h3>
				</div>
				<div class="card-header">
					&nbsp;<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4 salary" method="post" action="<?php echo base_url("appeal/apply_form/".$vacancy->id); ?>">
							<input type="hidden" class="form-control" name="vacancy_id" value="<?php echo $vacancy->id; ?>">
							<?php foreach ($list_question as $key => $item): ?>
								<div class="form-group row">
									<span class="col-sm-12"><strong>Pertanyaan <?php echo $key+1; ?> :</strong> <?php echo $item->question; ?></span>
									<div class="col-md-12">
										<input type="hidden" class="form-control" name="question[]" value="<?php echo $item->question; ?>">
										<input type="text" class="form-control" placeholder="Jawaban" name="answer[]" required>
									</div>
								</div>
								<hr/>
							<?php endforeach; ?>
							<hr>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$(".add-detail").click(function() {
			$(".salary").submit();
		});
	});

	function remove(el){
		$(el).parent().parent().remove();
	}
</script>
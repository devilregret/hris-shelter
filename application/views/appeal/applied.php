<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('appeal/applied'); ?>">Lamaran Aktif</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<style>
		#data-table tr th {
			text-align:center;
		}
	</style>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
				</div>
				<div class="card">
					<div class="card-header">
						<h3 class="card-title"><i class="fa fa-book"></i>&nbsp;Lowongan yang dilamar</h3>
					</div>
					<div class="card-body">
						<div class="scroll-panel">
							<table id="data-table" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th width="50px">No</th>
										<th>Judul Lowongan</th>
										<th>Provinsi</th>
										<th>Tanggal Apply</th>
										<th>Status</th>
										<th>Tes Online</th>
										<th>Catatan</th>
									</tr>
									<?php foreach ($vacancy as $item): ?>
									<tr>
										<td><?php echo $item['no']; ?></td>
										<td><?php echo $item['title']; ?></td>
										<td><?php echo $item['province_name']; ?></td>
										<td><?php echo $item['apply_date']; ?></td>
										<td><?php echo $item['followup_status']; ?></td>
										<td style="text-align:center">
											<?php if($item['is_tes_psikotes']=="1") { ?>
												<?php if($item['is_done_psikotes']=="1") { ?>
													<a class="btn btn-success" href="#" style="margin-bottom:5px;width:100%"><i class="fa fa-check"></i>&nbsp;DISC Selesai</a>
												<?php } else { ?>
													<a class="btn btn-primary"  onclick="konfirmasi(`<?php echo $item['id']; ?>`,'psikotes',`<?php echo $item['durasi_psikotes']; ?>`)" style="margin-bottom:5px;width:100%;color:white"><i class="fa fa-pencil-alt"></i>&nbsp;Kerjakan DISC</a>
												<?php } ?>
												<br>
											<?php } ?>
											<?php if($item['is_tes_deret_angka']=="1") { ?>
												<?php if($item['is_done_deret_angka']=="1") { ?>
													<a class="btn btn-success" href="#" style="margin-bottom:5px;width:100%"><i class="fa fa-check"></i>&nbsp;Deret Angka Selesai</a>
												<?php } else { ?>
													<a class="btn btn-primary" onclick="konfirmasi(`<?php echo $item['id']; ?>`,'deret_angka',`<?php echo $item['durasi_deret_angka']; ?>`)" style="margin-bottom:5px;width:100%;color:white"><i class="fa fa-pencil-alt"></i>&nbsp;Kerjakan Deret Angka</a>
												<?php } ?>
												<br>
											<?php } ?>
											<?php if($item['is_tes_hitung_cepat']=="1") { ?>
												<?php if($item['is_done_hitung_cepat']=="1") { ?>
													<a class="btn btn-success" style="margin-bottom:5px;width:100%" href="#"><i class="fa fa-check"></i>&nbsp;Hitung Cepat Selesai</a>
												<?php } else { ?>
													<a class="btn btn-primary" onclick="konfirmasi(`<?php echo $item['id']; ?>`,'hitung_cepat',`<?php echo $item['durasi_hitung_cepat']; ?>`)" style="margin-bottom:5px;width:100%;color:white"><i class="fa fa-pencil-alt"></i>&nbsp;Kerjakan Hitung Cepat</a>
												<?php } ?>
												<br>
											<?php } ?>
											<?php if($item['is_ketelitian']=="1") { ?>
												<?php if($item['is_done_ketelitian']=="1") { ?>
													<a class="btn btn-success" style="margin-bottom:5px;width:100%" href="#"><i class="fa fa-check"></i>&nbsp;Ketelitian Selesai</a>
												<?php } else { ?>
													<a class="btn btn-primary" onclick="konfirmasi(`<?php echo $item['id']; ?>`,'ketelitian',`<?php echo $item['durasi_ketelitian']; ?>`)" style="margin-bottom:5px;width:100%;color:white"><i class="fa fa-pencil-alt"></i>&nbsp;Kerjakan Ketelitian</a>
												<?php } ?>
												<br>
											<?php } ?>
											<?php if($item['is_kraepelin']=="1") { ?>
												<?php if($item['is_done_kraepelin']=="1") { ?>
													<a class="btn btn-success" style="margin-bottom:5px;width:100%" href="#"><i class="fa fa-check"></i>&nbsp;Kraepelin Selesai</a>
												<?php } else { ?>
													<a class="btn btn-primary" onclick="konfirmasi(`<?php echo $item['id']; ?>`,'kraepelin',`<?php echo $item['durasi_kolom_kraepelin']; ?>`)" style="margin-bottom:5px;width:100%;color:white" ><i class="fa fa-pencil-alt"></i>&nbsp;Kerjakan Kraepelin</a>
												<?php } ?>
												<br>
											<?php } ?>
										</td>
										<td><?php echo $item['followup_description']; ?></td>
									</tr>
									<?php endforeach; ?>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
$(document).ready(function(){
		<?php 
			if(!empty($list_belum_tes)){
			foreach ($list_belum_tes as $key => $element) {
				$url 		= "";
				$instruksi 	= "";
				$title		= "";

				if ($element->count_psikotes==1) { 
					$url 		= base_url('psikotes/form?vacancy='.$element->vacancy_id);
					$instruksi 	= $element->instruksi_psikotes;
					$instruksi 	= str_replace("[durasi]",$element->durasi_psikotes." Menit",$instruksi);
					$title		= "INSTRUKSI TES DISC";
				};
				if ($element->count_deret_angka==1) { 
					$url 		= base_url('deret_angka/form?vacancy='.$element->vacancy_id);
					$instruksi 	= $element->instruksi_deret_angka;
					$instruksi 	= str_replace("[durasi]",$element->durasi_deret_angka." Menit",$instruksi);
					$title		= "INSTRUKSI TES DERET ANGKA";
				};
				if ($element->count_hitung_cepat==1) {
					$url 		= base_url('hitung_cepat/form?vacancy='.$element->vacancy_id);
					$instruksi 	= $element->instruksi_hitung_cepat;
					$instruksi 	= str_replace("[durasi]",$element->durasi_hitung_cepat." Menit",$instruksi);
					$title		= "INSTRUKSI TES HITUNG CEPAT";
				};
				if ($element->count_ketelitian==1) {
					$url 		= base_url('ketelitian/form?vacancy='.$element->vacancy_id);
					$instruksi 	= $element->instruksi_ketelitian;
					$instruksi 	= str_replace("[durasi]",$element->durasi_ketelitian." Menit",$instruksi);
					$title		= "INSTRUKSI TES KETELITIAN";
				};
				if ($element->count_kraepelin==1) { 
					$url 		= base_url('kraepelin/form?vacancy='.$element->vacancy_id);
					$instruksi 	= $element->instruksi_kraepelin;
					$instruksi 	= str_replace("[durasi]",$element->durasi_kolom_kraepelin." Detik",$instruksi);
					$title		= "INSTRUKSI TES KRAEPELIN";
				};
		?>
				Swal.fire({
						icon:'info',
						title:"<?php echo $title ?>",
						html: `<?php echo $instruksi ?>`,
						width:1000,
						showDenyButton: true,
						confirmButtonText: 'Kerjakan',
						denyButtonText: `Batal`,
					}).then((result) => {
					if (result.isConfirmed) {
						window.location.href = "<?php echo $url ?>"
					} 
					})
		<?php break;} } ?>
	})

	function konfirmasi(vacancyId,tes,durasi){
		let url = "";
		let title = "";
		let instruksi = "";

		if (tes=="psikotes") {
			instruksi =  `<?php echo $item['intruksi_psikotes']; ?>`;
			instruksi = instruksi.replace("[durasi]",durasi+" Menit");
			url = `<?php echo base_url('psikotes/form?vacancy='); ?>`+vacancyId;
			title = "INSTRUKSI TES DISC";
		}else if (tes=="deret_angka") {
			instruksi =  `<?php echo $item['intruksi_deret_angka']; ?>`;
			instruksi = instruksi.replace("[durasi]",durasi+" Menit");
			url = `<?php echo base_url('deret_angka/form?vacancy='); ?>`+vacancyId;
			title = "INSTRUKSI TES DERET ANGKA";
		}else if (tes=="hitung_cepat") {
			instruksi =  `<?php echo $item['intruksi_hitung_cepat']; ?>`;
			instruksi = instruksi.replace("[durasi]",durasi+" Menit");
			url = `<?php echo base_url('hitung_cepat/form?vacancy='); ?>`+vacancyId;
			title = "INSTRUKSI TES HITUNG CEPAT";
		}else if (tes=="ketelitian") {
			instruksi =  `<?php echo $item['intruksi_ketelitian']; ?>`;
			instruksi = instruksi.replace("[durasi]",durasi+" Menit");
			url = `<?php echo base_url('ketelitian/form?vacancy='); ?>`+vacancyId;
			title = "INSTRUKSI TES KETELITIAN";
		}else if (tes=="kraepelin") {
			instruksi =  `<?php echo $item['intruksi_kraepelin']; ?>`;
			instruksi = instruksi.replace("[durasi]",durasi+" Detik");
			url = `<?php echo base_url('kraepelin/form?vacancy='); ?>`+vacancyId;
			title = "INSTRUKSI TES KRAEPELIN";
		}

		Swal.fire({
				icon:'info',
				title: title,
				html:instruksi,
				width:1000,
				showDenyButton: true,
				confirmButtonText: 'Mulai Kerjakan',
				denyButtonText: `Batal`,
			}).then((result) => {
			if (result.isConfirmed) {
				window.location.href = url
			} 
		})
	}

</script>
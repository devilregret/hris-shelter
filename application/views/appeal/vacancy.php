<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('appeal/vacancy'); ?>">Daftar Lowongan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="form-group row">
						<form action="<?php echo base_url('appeal/vacancy')?>" method="get" class="form-horizontal col-md-12">
							<div class="input-group row">
								<div class="input-group-append col-md-5">
									<select class="form-control select2" name="province">
										<option value="">&nbsp;SEMUA PROVINSI</option>
										<?php foreach ($list_province as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $province_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="input-group-append col-md-5">
									<input type="text" class="form-control" placeholder="Pencarian" name="search" value="<?php echo $search; ?>" >
								</div>
								<div class="input-group-append col-md-2">
									<button type="submit" class="btn btn-block btn-danger">Cari</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="card">
					<div class="card-header ui-sortable-handle" style="cursor: move;">
						<h3 class="card-title"><i class="ion ion-clipboard mr-1"></i>Daftar Lowongan</h3>
						<div class="card-tools"><?php echo $this->pagination->create_links(); ?></div>
					</div>
					<div class="card-body row">
						<?php foreach ($vacancy AS $item): 
							$date = get_day($item->day).", ".substr($item->date, 0, 2)." ".get_month(substr($item->date, 3,2))." ".substr($item->date, 6,4);
							?>
							<div class="card col-md-6 col-sm-12">
								<div class="card-header bg-light color-palette">
									<h3 class="card-title"><strong><?php echo $item->province_name; ?></strong>, <?php echo $item->title; ?> (Paling Lambat <strong> <?php echo $date; ?></strong>) <?php echo $item->txt_tes_online; ?></h3>
								</div>
								<div>
									<div class="card-body"><?php echo $item->content; ?></div>
									<?php if($item->flyer != ''): ?>
										<img src="<?php echo $item->flyer; ?>" width="100%" alt="Lowongan" />
									<?php endif; ?>
									<?php if($item->apply): ?>
										<span class="bg-success" style="width:100%;text-align:center;display: block;height: 36px;padding-top: 4px;">Sudah dilamar</span>
									<?php else: ?>
										<?php if($item->question): ?>
											<a class="btn btn-primary pull-right btn-block" href="<?php echo base_url('appeal/apply_form/'.$item->id); ?>">Lamar Sekarang</a>
										<?php else: ?>
											<a class="btn btn-primary pull-right btn-block" href="<?php echo base_url('appeal/apply/'.$item->id); ?>">Lamar Sekarang</a>
										<?php endif; ?>
									<?php endif; ?>
								</div>
							</div>
							<hr/>
						<?php endforeach; ?>
					</div>
					<div class="card-header ui-sortable-handle" style="cursor: move;">
						<div class="card-tools"><?php echo $this->pagination->create_links(); ?></div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
	$(document).ready(function(){
		<?php 
			if(!empty($list_belum_tes)){
			foreach ($list_belum_tes as $key => $element) {
				$url 		= "";
				$instruksi 	= "";
				$title		= "";

				if ($element->count_psikotes==1) { 
					$url 		= base_url('psikotes/form?vacancy='.$element->vacancy_id);
					$instruksi 	= $element->instruksi_psikotes;
					$instruksi 	= str_replace("[durasi]",$element->durasi_psikotes." Menit",$instruksi);
					$title		= "INSTRUKSI TES DISC";
				};
				if ($element->count_deret_angka==1) { 
					$url 		= base_url('deret_angka/form?vacancy='.$element->vacancy_id);
					$instruksi 	= $element->instruksi_deret_angka;
					$instruksi 	= str_replace("[durasi]",$element->durasi_deret_angka." Menit",$instruksi);
					$title		= "INSTRUKSI TES DERET ANGKA";
				};
				if ($element->count_hitung_cepat==1) {
					$url 		= base_url('hitung_cepat/form?vacancy='.$element->vacancy_id);
					$instruksi 	= $element->instruksi_hitung_cepat;
					$instruksi 	= str_replace("[durasi]",$element->durasi_hitung_cepat." Menit",$instruksi);
					$title		= "INSTRUKSI TES HITUNG CEPAT";
				};
				if ($element->count_ketelitian==1) {
					$url 		= base_url('ketelitian/form?vacancy='.$element->vacancy_id);
					$instruksi 	= $element->instruksi_ketelitian;
					$instruksi 	= str_replace("[durasi]",$element->durasi_ketelitian." Menit",$instruksi);
					$title		= "INSTRUKSI TES KETELITIAN";
				};
				if ($element->count_kraepelin==1) { 
					$url 		= base_url('kraepelin/form?vacancy='.$element->vacancy_id);
					$instruksi 	= $element->instruksi_kraepelin;
					$instruksi 	= str_replace("[durasi]",$element->durasi_kolom_kraepelin." Detik",$instruksi);
					$title		= "INSTRUKSI TES KRAEPELIN";
				};
		?>
				Swal.fire({
						icon:'info',
						title:"<?php echo $title ?>",
						html: `<?php echo $instruksi ?>`,
						width:1000,
						showDenyButton: true,
						confirmButtonText: 'Kerjakan',
						denyButtonText: `Batal`,
					}).then((result) => {
					if (result.isConfirmed) {
						window.location.href = "<?php echo $url ?>"
					} 
					})
		<?php break;} } ?>
	})

</script>
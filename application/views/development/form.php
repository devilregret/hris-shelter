<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('development/list'); ?>">Request</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Request</h3>
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("development/form"); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Menu<span class="text-danger"> *</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="hidden" name="status" value="NEW">
									<input type="hidden" name="target" value="<?php echo date('Y-m-d'); ?>">
									<input type="hidden" name="cost" value="0">
									<input type="hidden" name="priority" value="NORMAL">
									<input type="hidden" name="position_id" value="619">
									<input type="hidden" name="city_id" value="273">
									<?php if($id == ''): ?>
									<select class="form-control select2" name="title" >
										<option value="Dashboard Progress" <?php if($title == 'Dashboard Progress'): echo "selected"; endif; ?>> Dashboard Progress</option>
										<option value="Dashboard Rekrutmen" <?php if($title == 'Dashboard Rekrutmen'): echo "selected"; endif; ?>> Dashboard Rekrutmen</option>
										<option value="Dashboard Personalia" <?php if($title == 'Dashboard Personalia'): echo "selected"; endif; ?>> Dashboard Personalia</option>
										<option value="Dashboard Payroll" <?php if($title == 'Dashboard Payroll'): echo "selected"; endif; ?>> Dashboard Payroll</option>
										<option value="Dashboard Benefit" <?php if($title == 'Dashboard Benefit'): echo "selected"; endif; ?>> Dashboard Benefit</option>
										<option value="Monitoring Kontrak" <?php if($title == 'Monitoring Kontrak'): echo "selected"; endif; ?>> Monitoring Kontrak</option>
										<option value="Monitoring Kandidat" <?php if($title == 'Monitoring Kandidat'): echo "selected"; endif; ?>> Monitoring Kandidat</option>
										<option value="Monitoring Request" <?php if($title == 'Monitoring Request'): echo "selected"; endif; ?>> Monitoring Request</option>
										<option value="Struktur Organisasi" <?php if($title == 'Struktur Organisasi'): echo "selected"; endif; ?>> Struktur Organisasi</option>
										<option value="Deskripsi Pekerjaan" <?php if($title == 'Deskripsi Pekerjaan'): echo "selected"; endif; ?>> Deskripsi Pekerjaan</option>
										<option value="Sasaran Mutu" <?php if($title == 'Sasaran Mutu'): echo "selected"; endif; ?>> Sasaran Mutu</option>
										<option value="Data Diri" <?php if($title == 'Data Diri'): echo "selected"; endif; ?>> Data Diri</option>
										<option value="Catatan Aktifitas" <?php if($title == 'Catatan Aktifitas'): echo "selected"; endif; ?>> Catatan Aktifitas</option>
										<option value="Request" <?php if($title == 'Request'): echo "selected"; endif; ?>> Request</option>
										<option value="Karyawan Indirect"  <?php if($title == 'Karyawan Indirect'): echo "selected"; endif; ?>> Karyawan Indirect</option>
										<option value="Karyawan Direct"  <?php if($title == 'Karyawan Direct'): echo "selected"; endif; ?>> Karyawan Direct</option>
										<option value="Daftar Pelamar"  <?php if($title == 'Daftar Pelamar'): echo "selected"; endif; ?>> Daftar Pelamar</option>
										<option value="Pengajuan Client"  <?php if($title == 'Pengajuan Client'): echo "selected"; endif; ?>> Pengajuan Client</option>
										<option value="Pengajuan Personalia"  <?php if($title == 'Pengajuan Personalia'): echo "selected"; endif; ?>> Pengajuan Personalia</option>
										<option value="Pengajuan Indirect"  <?php if($title == 'Pengajuan Indirect'): echo "selected"; endif; ?>> Pengajuan Indirect</option>
										<option value="Kandidat Diterima"  <?php if($title == 'Kandidat Diterima'): echo "selected"; endif; ?>> Kandidat Diterima</option>
										<option value="Kandidat Ditolak"  <?php if($title == 'Kandidat Ditolak'): echo "selected"; endif; ?>> Kandidat Ditolak</option>
										<option value="Kandidat Diajukan"  <?php if($title == 'Kandidat Diajukan'): echo "selected"; endif; ?>> Kandidat Diajukan</option>
										<option value="Kontrak Karyawan"  <?php if($title == 'Kontrak Karyawan'): echo "selected"; endif; ?>> Kontrak Karyawan</option>
										<option value="Monitoring Kontrak"  <?php if($title == 'Monitoring Kontrak'): echo "selected"; endif; ?>> Monitoring Kontrak</option>
										<option value="Pengajuan Benefit"  <?php if($title == 'Pengajuan Benefit'): echo "selected"; endif; ?>> Pengajuan Benefit</option>
										<option value="Data Lengkap"  <?php if($title == 'Data Lengkap'): echo "selected"; endif; ?>> Data Lengkap</option>
										<option value="Belum Lengkap"  <?php if($title == 'Belum Lengkap'): echo "selected"; endif; ?>> Belum Lengkap</option>
										<option value="Ketenagakerjaan"  <?php if($title == 'Ketenagakerjaan'): echo "selected"; endif; ?>> Ketenagakerjaan</option>
										<option value="Kesehatan"  <?php if($title == 'Kesehatan'): echo "selected"; endif; ?>> Kesehatan</option>
										<option value="Jadwal Kerja"  <?php if($title == 'Jadwal Kerja'): echo "selected"; endif; ?>> Jadwal Kerja</option>
										<option value="Shift Karyawan"  <?php if($title == 'Shift Karyawan'): echo "selected"; endif; ?>> Shift Karyawan</option>
										<option value="Payroll"  <?php if($title == 'Payroll'): echo "selected"; endif; ?>> Payroll</option>
										<option value="Pengajuan Resign"  <?php if($title == 'Pengajuan Resign'): echo "selected"; endif; ?>> Pengajuan Resign</option>
										<option value="Karyawan Resign"  <?php if($title == 'Karyawan Resign'): echo "selected"; endif; ?>> Karyawan Resign</option>
										<option value="Karyawan Non Job"  <?php if($title == 'Karyawan Non Job'): echo "selected"; endif; ?>> Karyawan Non Job</option>
										<option value="Pengajuan Payroll"  <?php if($title == 'Pengajuan Payroll'): echo "selected"; endif; ?>> Pengajuan Payroll</option>
										<option value="Payroll Non Security"  <?php if($title == 'Payroll Non Security'): echo "selected"; endif; ?>> Payroll Non Security</option>
										<option value="Payroll Security"  <?php if($title == 'Payroll Security'): echo "selected"; endif; ?>> Payroll Security</option>
										<option value="Approval Payroll Security"  <?php if($title == 'Approval Payroll Security'): echo "selected"; endif; ?>> Approval Payroll Security</option>
										<option value="Aktifitas Karyawan"  <?php if($title == 'Aktifitas Karyawan'): echo "selected"; endif; ?>> Aktifitas Karyawan</option>
										<option value="Daftar Perizinan"  <?php if($title == 'Daftar Perizinan'): echo "selected"; endif; ?>> Daftar Perizinan</option>
										<option value="Dokumen Legalitas"  <?php if($title == 'Dokumen Legalitas'): echo "selected"; endif; ?>> Dokumen Legalitas</option>
										<option value="Dokumen Kerjasama"  <?php if($title == 'Dokumen Kerjasama'): echo "selected"; endif; ?>> Dokumen Kerjasama</option>
										<option value="Akun"  <?php if($title == 'Akun'): echo "selected"; endif; ?>> Akun</option>
									</select>
									<?php else: ?>
										<input type="text" class="form-control" placeholder="Menu" name="title" value="<?php echo $title; ?>">
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Deskripsi Request <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<textarea class="form-control" name="description" rows="3" placeholder="Deskripsi Development" required><?php echo $description; ?></textarea>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Developer <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="developer_id" >
										<option value=""> &nbsp;-- PILIH DEVELOPER --</option>
										<?php foreach ($list_developer as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $developer_id): echo "selected"; endif;?>><?php echo $item->full_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
					<?php if($detail): ?>
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th id='description'>Deskripsi</th>
									<th id='status'>Status</th>
									<th id='target'>Target Selesai</th>
									<th id='cost'>Estimasi Biaya</th>
									<th id='priority'>Priority</th>
									<th id='position'>Divisi</th>
									<th id='city'>Kota</th>
									<th id='developer'>Developer</th>
									<th id='created_at'>Tanggal Dibuat</th>
									<th id='created_by'>Dibuat Oleh</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($detail as $item): ?>
								<tr>
									<td><?php echo $item['description']; ?></td>
									<td><?php echo $item['status']; ?></td>
									<td><?php echo $item['target']; ?></td>
									<td><?php echo $item['cost']; ?></td>
									<td><?php echo $item['priority']; ?></td>
									<td><?php echo $item['position']; ?></td>
									<td><?php echo $item['city']; ?></td>
									<td><?php echo $item['developer']; ?></td>
									<td><?php echo $item['created_at']; ?></td>
									<td><?php echo $item['created_by']; ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
					<?php endif; ?>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
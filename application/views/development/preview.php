<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('development/list'); ?>">Development</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-body">
					<div class="card-header">
						<h3 class="card-title"><i class="fas fa-eye"></i>&nbsp;Preview Development</h3>
					</div>
					<div class="row">
						<div class="col-12 table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Id</td>
										<td>: <?php echo $preview->id; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Judul</td>
										<td>: <?php echo $preview->title; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Deskiripsi</td>
										<td>: <?php echo $preview->description; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Status</td>
										<td>: <?php echo $preview->status; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Target Selesai</td>
										<td>: <?php echo $preview->target; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Estimasi Biaya</td>
										<td>: <?php echo $preview->cost; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Skala Prioritas</td>
										<td>: <?php echo $preview->priority; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Divisi </td>
										<td>: <?php echo $preview->position; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Kota </td>
										<td>: <?php echo $preview->city; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Developer </td>
										<td>: <?php echo $preview->developer; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Dibuat oleh</td>
										<td>: <?php echo $preview->target; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Tanggal dibuat</td>
										<td>: <?php echo $preview->created_at; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Diubah oleh</td>
										<td>: <?php echo $preview->updated_by; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Tanggal diubah</td>
										<td>: <?php echo $preview->updated_at; ?></td>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<?php if($detail): ?>
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th id='description'>Deskripsi</th>
									<th id='status'>Status</th>
									<th id='target'>Target Selesai</th>
									<th id='cost'>Estimasi Biaya</th>
									<th id='priority'>Priority</th>
									<th id='position'>Divisi</th>
									<th id='city'>Kota</th>
									<th id='developer'>Developer</th>
									<th id='created_at'>Tanggal Dibuat</th>
									<th id='created_by'>Dibuat Oleh</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($detail as $item): ?>
								<tr>
									<td><?php echo $item['description']; ?></td>
									<td><?php echo $item['status']; ?></td>
									<td><?php echo $item['target']; ?></td>
									<td><?php echo $item['cost']; ?></td>
									<td><?php echo $item['priority']; ?></td>
									<td><?php echo $item['position']; ?></td>
									<td><?php echo $item['city']; ?></td>
									<td><?php echo $item['developer']; ?></td>
									<td><?php echo $item['created_at']; ?></td>
									<td><?php echo $item['created_by']; ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
					<?php endif; ?>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
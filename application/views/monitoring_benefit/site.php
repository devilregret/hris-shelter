<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('monitoring_benefit/site'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Site Bisnis</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-md-4">&nbsp;</div>
						<div class="col-md-8 process" hidden="true">
							<a href="#" class="btn btn-danger labor float-right"  style="margin-right:10px;"><i class="fas fa-ban"></i> Putus Asuransi Ketenagakerjaan</a>							
							<a href="#" class="btn btn-warning health float-right"  style="margin-right:10px;"><i class="fas fa-ban"></i> Putus Asuransi Kesehatan</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="POST" id="process" action="<?php echo base_url('monitoring_benefit/site_process'); ?>">
					<input type="hidden" class="form-control action" name="action" required="">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>
										<input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll">
									</th>
									<th id='name'>Nama</th>
									<th id='client_name'>Nama Perusahaan</th>
									<th id='company_name'>Bisnis Unit</th>
									<th id='address'>Alamat</th>
									<th id='branch'>Branch</th>
									<th id='contract_status'>Status Kontrak</th>
									<th id='count_employee'>Jumlah Karyawan</th>
									<th id='health_insurance'>Asuransi Kesehatan</th>
									<th id='labor_insurance'>Asuransi Ketenagakerjaan</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("monitoring_benefit/site_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'name' },
				{ data: 'client_name' },
				{ data: 'company_name' },
				{ data: 'address' },
				{ data: 'branch' },
				{ data: 'contract_status' },
				{ data: 'count_employee' },
				{ data: 'health_insurance_status' },
				{ data: 'labor_insurance_status' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[2, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<7){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".process").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".process").prop('hidden', false);
    		});
		});
		$("input:checkbox").change(function() {
			$(".process").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".process").prop('hidden', false);
    		});
		});
		$( ".btn.labor" ).click(function() {
			$( ".action").val("labor");
			$( "#process" ).submit();
		});
		$( ".btn.health" ).click(function() {
			$( ".action").val("health");
			$( "#process" ).submit();
		});

	});
</script>
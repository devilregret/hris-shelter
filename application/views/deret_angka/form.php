<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<style>
		.table-identitas tr td {
			padding:1px !important;
			font-size:12px;
			border: none !important;
		}
		.td-isi {
			vertical-align:top;
			padding-left:20px;
			padding-right:20px;
		}
		.td-lompat {
			vertical-align:top;
			padding-left:10px;
			padding-right:10px;
			margin-top:30px;
			border: 1px solid black;
		}
		.td-jawaban {
			padding-left:10px;
			padding-right:10px;
		}
		.td-benar {
			border: 1px solid black;
			border-radius:30px;
		}

		/* style soal */
	.questionsBox {
		display: block;
		border: solid 1px #e3e3e3;
		padding: 10px 20px 0px;
		box-shadow: inset 0 0 30px rgba(000,000,000,0.1), inset 0 0 4px rgba(255,255,255,1);
		border-radius: 3px;
		margin: 0 10px;
	}.questions {
		background: #007fbe;
		color: #FFF;
		font-size: 22px;
		padding: 8px 30px;
		font-weight: 300;
		margin: 0 -30px 10px;
		position: relative;
	}
	.questions:after {
		background: url(../img/icon.png) no-repeat left 0;
		display: block;
		position: absolute;
		top: 100%;
		width: 9px;
		height: 7px;
		content: '.';
		left: 0;
		text-align: left;
		font-size: 0;
	}
	.questions:after {
		left: auto;
		right: 0;
		background-position: -10px 0;
	}
	.questions:before, .questions:after {
		background: black;
		display: block;
		position: absolute;
		top: 100%;
		width: 9px;
		height: 7px;
		content: '.';
		left: 0;
		text-align: left;
		font-size: 0;
	}
	.answerList {
		margin-bottom: 15px;
	}


	ol, ul {
		list-style: none;
	}
	.answerList li:first-child {
		border-top-width: 0;
	}

	.answerList li {
		padding: 3px 0;
	}
	.answerList label {
		display: block;
		padding: 6px;
		border-radius: 6px;
		border: solid 1px #dde7e8;
		font-weight: 400;
		font-size: 13px;
		cursor: pointer;
		font-family: Arial, sans-serif;
	}
	input[type=checkbox], input[type=radio] {
		margin: 4px 0 0;
		margin-top: 1px;
		line-height: normal;
	}
	.questionsRow {
		background: #dee3e6;
		margin: 0 -20px;
		padding: 10px 20px;
		border-radius: 0 0 3px 3px;
	}
	.button, .greyButton {
		background-color: #f2f2f2;
		color: #888888;
		display: inline-block;
		border: solid 3px #cccccc;
		vertical-align: middle;
		text-shadow: 0 1px 0 #ffffff;
		line-height: 27px;
		min-width: 160px;
		text-align: center;
		padding: 5px 20px;
		text-decoration: none;
		border-radius: 0px;
		text-transform: capitalize;
	}
	.questionsRow span {
		float: right;
		display: inline-block;
		line-height: 30px;
		border: solid 1px #aeb9c0;
		padding: 0 10px;
		background: #FFF;
		color: #007fbe;
	}
	.border {
		border:1px solid black;
	}

	.jawaban-terpilih {
		border:2px solid black;
		padding:5px;
		border-radius:30px
	}
	#countdown {
			text-align: center;
			font-size: 30px;
			font-weight:bold;
		}
	</style>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<div class="row">
						<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Tes Deret Angka</h3>
					</div>
					<br>
					<div>
						<p id="countdown"></p>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-2" method="post" action="<?php echo base_url("deret_angka/saveform"); ?>" enctype="multipart/form-data" onsubmit="return validateForm()">
							<input type="hidden" name="full_name" value="<?php echo $data_employee[0]->full_name ?>">
							<input type="hidden" name="umur" value="<?php echo $data_employee[0]->umur ?>">
							<input type="hidden" name="jk" value ="<?php echo $data_employee[0]->jk ?>">
							<input type="hidden" name="vacancy_id" value ="<?php echo $vacancy_id ?>">
							<input type="hidden" name="tgl_tes" value="<?php echo $data_employee[0]->tgl_tes ?>">
							<input type="hidden" name="tgl_selesai_deret_angka" value="<?php echo $data_vacancy[0]->tgl_selesai_deret_angka ?>">
							<input type="hidden" name="tes_mulai" value="<?php echo date('Y-m-d H:i:s'); ?>">
							<input type="hidden" id="jumlah_soal" value="<?php echo count($list_nomor); ?>">
							<input type="hidden" id="is_validate" value="1">

							<div class="form-group row">
								<div class="col-sm-4" style="background-color:#74b9ff;font-size:12px;text-align:justify;padding-top:10px">
									<table class="table table-identitas">
										<tbody>
											<tr>
												<td style="width:25%"><b>Nama</b></td>
												<td><b>&nbsp;:&nbsp;</b></td>
												<td><?php echo $data_employee[0]->full_name ?></td>
											</tr>
											<tr>
												<td style="width:25%"><b>Usia</b></td>
												<td><b>&nbsp;:&nbsp;</b></td>
												<td>
													<?php 
														if ($data_employee[0]->umur==null) {
															echo '-';
														} else {
															echo $data_employee[0]->umur;
														} ?> Tahun
												</td>
											</tr>
											<tr>
												<td style="width:25%"><b>Jenis Kelamin</b></td>
												<td><b>&nbsp;:&nbsp;</b></td>
												<td><?php echo $data_employee[0]->jk ?></td>
											</tr>
											<tr>
												<td style="width:25%"><b>Tanggal Tes</b></td>
												<td><b>&nbsp;:&nbsp;</b></td>
												<td><?php echo $data_employee[0]->stgl_tes ?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-sm-8" style="background-color:#74b9ff;font-size:12px;text-align:justify;padding-top:10px">
									<span>
									<b>INSTRUKSI :</b>
									</span><br>
									<span>
									1. Temukan pola pada tiap deret angka, lalu isi angka yang kosong sesuai pola tersebut.
									</span><br>
									<span>
									2. Lingkari pilihan jawaban yang tepat.
									</span>
									<br>
								</div>
							</div>
							<div class="form-group row">
								<span>
									<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;CONTOH</h3>
								</span>
							</div>
							<div class="form-group row">
								<div class="col-sm-6 privew">
									<div class="questionsBox">
											<div class="questions">
												<table style="width:100%;text-align :center;">
													<tr>
														<td><b>[1]</b></td>
														<td>2</td>
														<td></td>
														<td>4</td>
														<td></td>
														<td>.....?</td>
														<td></td>
														<td>8</td>
														<td></td>
														<td>10</td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td class="border">+2</td>
														<td></td>
														<td class="border">+2</td>
														<td></td>
														<td class="border">+2</td>
														<td></td>
														<td class="border">+2</td>
														<td></td>
													</tr>
												</table>
											</div>
										<ul class="answerList">
											<li>
												<label><span class="jawaban-terpilih">A.</span>  6</label>
											</li>
											<li>
												<label>B. 5</label>
											</li>
											<li>
												<label>C. 7</label>
											</li>
											<li>
												<label>D. 3</label>
											</li>
										</ul>
										<h6 style='margin-bottom:20px;'>Penjelasan : Pola adalah +2 , jadi jawaban yang tepat adalah <b>A</b>. </h6>
									</div>
								</div>
								<div class="col-sm-6 privew">
									<div class="questionsBox">
										<div class="questions">
											<table style="width:100%;text-align :center;">
												<tr>
													<td><b>[2]</b></td>
													<td>10</td>
													<td></td>
													<td>12</td>
													<td></td>
													<td>9</td>
													<td></td>
													<td>11</td>
													<td></td>
													<td>.....?</td>
												</tr>
												<tr>
													<td></td>
													<td></td>
													<td class="border">+2</td>
													<td></td>
													<td class="border">-3</td>
													<td></td>
													<td class="border">+2</td>
													<td></td>
													<td class="border">-3</td>
													<td></td>
												</tr>
											</table>
										</div>
										<ul class="answerList">
											<li>
												<label>A. 6</label>
											</li>
											<li>
												<label>B. 7</label>
											</li>
											<li>
												<label><span class="jawaban-terpilih">C.</span> 8</label>
											</li>
											<li>
												<label>D. 3</label>
											</li>
										</ul>
										<h6 style='margin-bottom:20px;'>Penjelasan : Pola adalah +2 lalu -3, jadi jawaban yang tepat adalah <b>C</b>. </h6>
									</div>
								</div>
							</div>
							<div class="row">
								<span>
									<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;SOAL</h3>
								</span>
							</div>
							<div class="row">
								<?php foreach ($list_nomor as $key => $element) { ?>
									<div class="col-sm-6 privew" style="margin-top:30px">
										<div class="questionsBox">
											<div class="questions"> <b>[<?php echo $element->no_soal ?>]</b> &nbsp;
												<?php $txt = ""; ?>
												<?php foreach ($list_deret as $keyd => $elementd) { ?>
													<?php if($elementd->m_deret_angka_id == $element->id ) $txt .=$elementd->deret." , "; ?>
												<?php } ?>
												<?php $txt = substr($txt, 0, -2); 
														echo $txt ?>
											</div>
											<ul class="answerList">
												<?php foreach ($list_jawaban as $keyj => $j) { ?>
													<?php if($j->m_deret_angka_id == $element->id ) { ?>
														<li>
															<label>
																	<input type="radio" name="soal_<?php echo $element->id ?>" value="<?php echo $j->id ?>"> <?php echo $j->alpabet.". ".$j->jawaban ?>
															</label>
														</li>
													<?php }; ?>
												<?php } ?>
											</ul>
										</div>
									</div>
								<?php } ?>
							</div>
							<br>
							<br>
							<div class="form-group row  float-right">
								<button type="button" onclick="window.history.back();" class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>	
		</div>
	</section>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
	function validateForm() {
		let isValidate = $("#is_validate").val();
		if(isValidate =="0"){
			return true;
		}

		let msg ="";
		let noBelumIsi = "";
		let jumlahSoal = $('#jumlah_soal').val();
		for (let i = 1; i <= jumlahSoal; i++) {
			let radioName = 'soal_'+i;
			let isCek = false;
			if($('input[name='+radioName+']').is(':checked')) {
				isCek = true;
			}

			if (!isCek) {
				noBelumIsi += i+",";
			}
		}

		noBelumIsi = noBelumIsi.slice(0, -1);
		if (noBelumIsi != "") {
			msg ="Terdapat nomor yang belum anda isi yaitu : nomor "+noBelumIsi+". Apakah anda ingin tetap menyimpan ?";
		}else{
			msg = "Apakah anda ingin menyimpan tes ini ?";
		}

		if (confirm(msg)) {
			return true;	
		}else{
			return false;
		}
		return true;
	}

	$(document).ready(function () {
		var countDownDate = new Date();
		countDownDate.setMinutes(countDownDate.getMinutes() + <?php echo $data_vacancy[0]->durasi_deret_angka ?>);

		// Update the count down every 1 second
		var x = setInterval(function() {

			// Get today's date and time
			var now = new Date().getTime();
			// Find the distance between now and the count down date
			var distance = countDownDate - now;
			
			// Time calculations for days, hours, minutes and seconds
			var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);
			
			// Output the result in an element with id="demo"
			document.getElementById("countdown").innerHTML = "Sisa Waktu : "+hours + " Jam "
			+ minutes + " Menit " + seconds + " Detik ";
			
			// If the count down is over, write some text 
			if (distance < 0) {
				$("#is_validate").val("0");
				clearInterval(x);
				document.getElementById("countdown").innerHTML = "WAKTU HABIS";
				alert("Waktu Sudah Habis , Dokumen akan otomatis Tersimpan");
				$('form').submit();
			}
		}, 1000);
	})

</script>
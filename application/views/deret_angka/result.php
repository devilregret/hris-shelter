<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/css/adminlte.min.css"); ?>
		<?php echo asset_js("plugins/jquery/jquery.min.js"); ?>	
		<style>
				.maintable { 
					border-spacing: 10px;
					border-collapse: separate;
				}
				.tabel-soal {
					border-spacing: 10px;
					border-collapse: separate;
				}
				.color-title {
					background-color:#341f97;
					-webkit-print-color-adjust:exact;
				}
				.color-content {
					background-color:#48dbfb;
					-webkit-print-color-adjust:exact;
				}
				.color-sub-title {
					background-color:#2e86de;
					-webkit-print-color-adjust:exact;
				}
				.jawaban-terpilih {
					border:2px solid black;
					padding:5px;
					border-radius:30px
				}
				.color-red {
					background-color:red;
					-webkit-print-color-adjust:exact;
				}
				.tabel-simpulan {
					border-spacing: 30px;
				}
				.tabel-simpulan tr td {
					border:1px solid black;
					padding-top:10px;
					padding-bottom:10px;
				}
				.jawaban-tidak-diisi {
					background-color:#ff6b6b;
					-webkit-print-color-adjust:exact;
				}
				.jawaban-salah {
					background-color:#feca57;
					-webkit-print-color-adjust:exact;
				}
				.jawaban-benar {
					background-color:#1dd1a1;
					-webkit-print-color-adjust:exact;
				}

			</style>
	</head>
	<body>
		<div class="wrapper">
			<section class="deret-angka-print">
			<?php echo asset_js("plugins/chart.js/Chart.min.js"); ?>
			<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
			<style>
				
			</style>
				<br>
				<div class="row">
					<table style="width:100%" class="maintable">
						<tr class="color-title">
							<td colspan="3" style="padding:10px">
								<h2 style="color:white;text-align:center">TES DERET ANGKA</h2>
							</td>
						</tr>
						<tr>
							<td colspan="3" class="color-content">
								<table style="width:100%">
									<tr>
										<td style="width:10%">Name</td>
										<td style="width:3%">:</td>
										<td style="width:87%"><?php echo $data_deret_angka[0]->nama ?></td>
									</tr>
									<tr>
										<td>Age</td>
										<td>:</td>
										<td><?php echo $data_deret_angka[0]->usia ?> Tahun</td>
									</tr>
									<tr>
										<td>Gender</td>
										<td>:</td>
										<td><?php echo $data_deret_angka[0]->jk ?></td>
									</tr>
									<tr>
										<td>Date</td>
										<td>:</td>
										<td><?php echo $data_deret_angka[0]->tgl_tes ?></td>
									</tr>
								</table>
							</td>
						</tr>
						<?php if($is_hanya_hasil==0) { ?>
						<tr class="color-sub-title">
							<td colspan="3" style="padding:5px">
								<h2 style="color:white;text-align:center">Soal</h2>
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<table class="tabel-soal" style="width:100%">
									<?php foreach ($data_deret_angka_d as $key => $value) { ?>
										<?php
										$label = 'jawaban-tidak-diisi';
										
										if($value->m_deret_angka_jawaban_id!=null){
											if($value->is_benar ==1){
												$label = 'jawaban-benar';
											}else{
												$label = 'jawaban-salah';
											};
										};
										?>
										<tr>
											<td style="width:2%" class="<?php echo $label; ?>">&nbsp;</td>
											<td>[<?php echo $value->no_soal ?>] </td>
											<?php 
												$jmlKol = 0; 
												foreach ($list_deret as $keyd => $valued) {
													if($valued->m_deret_angka_id == $value->m_deret_angka_id){
														$jmlKol++;
											?>
												<td><?php echo $valued->deret ?></td>
											<?php
													}
												}
											?>
											<?php for ($i=0; $i < 9-$jmlKol ; $i++) { ?> 
												<td></td>
											<?php } ?>
											
											<?php 
												foreach ($list_jawaban as $keyj => $valuej) { 
													if($valuej->m_deret_angka_id == $value->m_deret_angka_id){
											?>
												<td>
													<?php
														if ($value->m_deret_angka_jawaban_id==$valuej->id) {
															echo '<span class="jawaban-terpilih">'.$valuej->alpabet.'.</span> '.$valuej->jawaban;	
														}else{
															echo $valuej->alpabet.". ".$valuej->jawaban;
														};
													?>
												</td>
											<?php ;}; } ?>
										</tr>
									<?php } ?>
								</table>
							</td>
						</tr>
						<?php } ?>
						<tr>
							<td>
								<table class="tabel-simpulan" style="width:100%;text-align:center">
									<tr class="color-sub-title" style="color:white;font-size:16px;font-weight:bold">
										<td>Betul</td>
										<td>Salah</td>
										<td>Tidak Diisi</td>
										<td>%</td>
										<td>Petugas</td>
										<td>Kategori</td>
									</tr>
									<tr>
										<td><?php echo $data_deret_angka[0]->jawaban_benar ?></td>
										<td><?php echo $data_deret_angka[0]->jawaban_salah ?></td>
										<td><?php echo $data_deret_angka[0]->tidak_diisi ?></td>
										<td><?php echo $data_deret_angka[0]->persentase ?> %</td>
										<td><?php echo $petugas ?></td>
										<td><?php echo $data_deret_angka[0]->kategori ?></td>
									</tr>
								</table>
							</td>
						</tr>
						<?php if($is_hanya_hasil==0) { ?>
						<tr>
							<td>
								<table style="width:100%">
									<tr>
										<td colspan="2">Note :</td>
									</tr>
									<tr>
										<td class="jawaban-tidak-diisi" style="width:20%;">&nbsp;</td>
										<td>: Jawaban tidak diisi</td>
									</tr>
									<tr>
									<td class ="jawaban-salah" style="width:20%;">&nbsp;</td>
										<td>: Jawaban salah</td>
									</tr>
									<tr>
									<td class="jawaban-benar" style="width:20%;">&nbsp;</td>
										<td>: Jawaban benar</td>
									</tr>
								</table>
							</td>
						</tr>
						<?php } ?>
					</table>
					<br>
				</div>
			</section>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="printDeretAngka();">Print</button>
			<button type="button" class="btn btn-default" onclick="window.close();">Tutup</button>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.main-sidebar').remove();
				$('.main-header').remove();
			})
			function printDeretAngka() {
				$('.modal-footer').remove();
				window.print();
				window.close();
			}
		</script>
	</body>
</html>

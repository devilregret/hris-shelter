<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6"><h1><?php echo $_TITLE_ ?></h1></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $_TITLE_ ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card card-primary card-outline col-12">
						<div class="card-body">
							<div class="row">
								<div class="col-12 col-sm-6 col-md-3">
									<div class="info-box">
										<span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan Direct</span>
											<a href="<?php echo base_url('payroll_employee/list'); ?>">
												<span class="info-box-number"><?php echo $total_direct; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-3">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan Indirect</span>
											<a href="<?php echo base_url('payroll_employee/list'); ?>">
												<span class="info-box-number"><?php echo $total_indirect; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-3">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-secondary elevation-1"><i class="fas fa-clipboard-check"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Pengajuan Payroll SNI</span>
											<a href="<?php echo base_url('payroll/list'); ?>">
												<span class="info-box-number"><?php echo $waiting_non_security; ?>&nbsp;<small>Pengajuan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-3">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-secondary elevation-1"><i class="fas fa-clipboard-check"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Pengajuan Payroll SN</span>
											<a href="<?php echo base_url('payroll/security'); ?>">
												<span class="info-box-number"><?php echo $waiting_security; ?>&nbsp;<small>Pengajuan</small></span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"><strong>Daftar Pengajuan Payroll</strong></h3>
						</div>
						<div class="card-body">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Nama Site</th>
										<th>Alamat Site</th>
										<th>Jumlah Pengajuan</th>
										<th>Total Nilai Pengajuan</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$no = 1;
									foreach ($list_submission as $item): 
									?>
									<tr>
										<td><?php echo $no; ?></td>
										<td><?php echo $item->site_name.' - '.$item->company_code; ?></td>
										<td><?php echo $item->site_address; ?></td>
										<td><?php echo $item->payroll_submission; ?></td>
										<td><?php echo rupiah($item->summary_payroll); ?></td>
										
									</tr>
									<?php 
									$no++;
									endforeach; 
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

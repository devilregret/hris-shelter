<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6"><h1><?php echo $_TITLE_ ?></h1></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $_TITLE_ ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">	
	<?php
		switch ($_ROLE_){
			case 1: 
				$this->load->view("dashboard/candidate");
				break;
			case 2: 
				$this->load->view("dashboard/administrator");
				break;
			case 4: 
				$this->load->view("dashboard/recruitment");
				break;
		}
	?>
	</section>
</div>
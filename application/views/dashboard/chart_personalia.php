<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6"><h1><?php echo $_TITLE_ ?></h1></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url('dashboard/personalia'); ?>"><?php echo $_TITLE_ ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card card-primary card-outline col-12">
						<div class="card-body">
							<div class="row">
								<div class="col-md-12">
									<a href="<?php echo base_url('dashboard/employee_site_export'); ?>" class="btn btn-success"> DOWNLOAD DATA KARYAWAN</a>
								</div>
								<p>&nbsp;</p>
								<div class="col-md-12">
									<div class="card card-primary">
										<div class="card-header">
											<h3 class="card-title">RESUME KARYAWAN</h3>
										</div>
										<div class="card-body">
											<div class="scroll-panel" style="height:420px !important;">
											<table class="table table-bordered">
												<thead>
													<tr style="text-align:center;">
														<th rowspan="2" style="vertical-align: middle;"><?php echo strtoupper(get_month(date('m'))." ".date('Y')); ?></th>
														<th colspan="2">SN</th>
														<th colspan="2">SNI</th>
														<th colspan="2">ION</th>
														<th colspan="2">SAPTA</th>
														<th colspan="2">TOTAL</th>
														<th rowspan="2">GRAND TOTAL</th>
													</tr>
													<tr>
														<th>INDIRECT</th>
														<th>DIRECT</th>
														<th>INDIRECT</th>
														<th>DIRECT</th>
														<th>INDIRECT</th>
														<th>DIRECT</th>
														<th>INDIRECT</th>
														<th>DIRECT</th>
														<th>INDIRECT</th>
														<th>DIRECT</th>
													</tr>
												</thead>
												<tbody>
													<?php 
													$total_ind_sn 	= 0;
													$total_sn 		= 0;
													$total_ind_sni 	= 0;
													$total_sni 		= 0;
													$total_ind_ion 	= 0;
													$total_ion 		= 0;
													$total_ind_sap	= 0;
													$total_sap 		= 0;
													foreach($resume_employee AS $item):
														$total_ind_sn 	+= $item['indirect_sn'];
														$total_sn 		+= $item['sn'];
														$total_ind_sni 	+= $item['indirect_sni'];
														$total_sni 		+= $item['sni'];
														$total_ind_ion 	+= $item['indirect_ion'];
														$total_ion 		+= $item['ion'];
														$total_ind_sap 	+= $item['indirect_sapta'];
														$total_sap 		+= $item['sapta'];
													?>
													<tr>
														<td><?php echo $item['branch']; ?></td>
														<td><a href="<?php echo base_url('employee/list/indirect/sn/'.$item['branch_id']); ?>"><?php echo $item['indirect_sn']; ?></a></td>
														<td><a href="<?php echo base_url('employee/list/direct/sn/'.$item['branch_id']); ?>"><?php echo $item['sn']; ?></a></td>
														<td><a href="<?php echo base_url('employee/list/indirect/sni/'.$item['branch_id']); ?>"><?php echo $item['indirect_sni']; ?></a></td>
														<td><a href="<?php echo base_url('employee/list/direct/sni/'.$item['branch_id']); ?>"><?php echo $item['sni']; ?></a></td>
														<td><a href="<?php echo base_url('employee/list/indirect/ion/'.$item['branch_id']); ?>"><?php echo $item['indirect_ion']; ?></a></td>
														<td><a href="<?php echo base_url('employee/list/direct/ion/'.$item['branch_id']); ?>"><?php echo $item['ion']; ?></a></td>
														<td><a href="<?php echo base_url('employee/list/indirect/sapta/'.$item['branch_id']); ?>"><?php echo $item['indirect_sapta']; ?></a></td>
														<td><a href="<?php echo base_url('employee/list/direct/sapta/'.$item['branch_id']); ?>"><?php echo $item['sapta']; ?></a></td>
														<td><?php echo $item['indirect_sn']+$item['indirect_sni']+$item['indirect_ion']+$item['indirect_sapta']; ?></td>
														<td><?php echo $item['sn']+$item['sni']+$item['ion']+$item['sapta']; ?></td>
														<th><?php echo $item['indirect_sn']+$item['indirect_sni']+$item['indirect_ion']+$item['indirect_sapta']+$item['sn']+$item['sni']+$item['ion']+$item['sapta']; ?></th>
													</tr>
													<?php
													endforeach;
													?>
													<tr>
														<th>TOTAL</th>
														<th><?php echo $total_ind_sn; ?></th>
														<th><?php echo $total_sn; ?></th>
														<th><?php echo $total_ind_sni; ?></th>
														<th><?php echo $total_sni; ?></th>
														<th><?php echo $total_ind_ion; ?></th>
														<th><?php echo $total_ion; ?></th>
														<th><?php echo $total_ind_ion; ?></th>
														<th><?php echo $total_sap; ?></th>
														<th><?php echo $total_ind_sn+$total_ind_sni+$total_ind_ion+$total_ind_sap; ?></th>
														<th><?php echo $total_sn+$total_sni+$total_ion+$total_sap; ?></th>
														<th><?php echo $total_ind_sn+$total_ind_sni+$total_ind_ion+$total_ind_sap+$total_sn+$total_sni+$total_ion+$total_sap; ?></th>
													</tr>
												</tbody>
											</table>
										</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card-header">
							<form action="<?php echo base_url('dashboard/personalia');?>" method="get">
							<div class="row">
								<div class="col-md-6">
									&nbsp;
								</div>
								<div class="col-md-2">
									<select class="form-control select2" name="business_id">
										<option value="">SEMUA BISNIS UNIT</option>
										<?php foreach ($list_company as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id == $business_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-md-2">
									<select class="form-control select2" name="branch_id">
										<?php 
										foreach ($list_branch as $item) :  
											if($item->id == 1){
												$item->name = "SEMUA BRANCH";
											}
										?>
										<option value="<?php echo $item->id;?>" <?php if($item->id == $branch_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-md-2">
									<button class="btn btn-primary btn-sm" style="width: 100px;"> CARI</button> &nbsp;
									<a href="<?php echo base_url('dashboard'); ?>" class="btn btn-primary btn-sm" style="width: 100px;">RESET</a>
								</div>
							</div>
							</form>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-12 col-sm-6 col-md-3">
									<div class="info-box">
										<span class="info-box-icon bg-warning elevation-1"><i class="fas fa-handshake"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Site Bisnis</span>
											<span class="info-box-number"><?php echo $total_site; ?>&nbsp;<small>Site</small></span>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-3">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan Aktif</span>
											<span class="info-box-number"><?php echo $total_employee; ?>&nbsp;<small>Karyawan</small></span>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-3">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-danger elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan Nonjob</span>
											<span class="info-box-number"><?php echo $total_nonjob; ?>&nbsp;<small>Karyawan</small></span>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-3">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-secondary elevation-1"><i class="fas fa-briefcase"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Jenis Pekerjaan</span>
											<span class="info-box-number"><?php echo $total_position; ?>&nbsp;<small>Posisi</small></span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card card-primary">
										<div class="card-header">
											<h3 class="card-title">KONTRAK AKAN HABIS</h3>
										</div>
										<div class="card-body">
											<div class="chart">
												<canvas id="last-contract-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="card card-primary">
										<div class="card-header">
											<h3 class="card-title">SUMMARY KARYAWAN</h3>
										</div>
										<div class="card-body">
											<div class="chart">
												<canvas id="employee-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="card card-primary">
										<div class="card-header">
											<h3 class="card-title">RESUME 50 PEKERJAAN DENGAN KARYAWAN TERBANYAK</h3>
										</div>
										<div class="card-body">
											<div class="chart">
												<canvas id="position-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="card card-primary">
										<div class="card-header">
											<h3 class="card-title">RESUME SITE BISNIS</h3>
										</div>
										<div class="card-body">
											<table id="table-site" class="table table-bordered table-hover">
												<thead>
													<tr>
														<th>NAMA SITE</th>
														<th>AKHIR KONTRAK</th>
														<th>JUMLAH KARYAWAN</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th colspan="2">TOTAL</th>
														<th><?php echo $total_employee; ?></th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="card card-primary">
										<div class="card-header">
											<h3 class="card-title">RESUME GAJI KARYAWAN</h3>
										</div>
										<div class="card-body">
											<div class="chart">
												<canvas id="salary-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="card card-primary">
										<div class="card-header">
											<h3 class="card-title">SUMMARY CUTI</h3>
										</div>
										<div class="card-body">
											<div class="chart">
												<canvas id="cuti-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php echo asset_js("plugins/chart.js/Chart.min.js"); ?>
<script>
$(document).ready(function() {
	$.ajax({
		url: "<?php echo base_url('dashboard/chart_last_contract/'.$branch_id.'/'.$business_id); ?>",
		dataType: "json",
		success: function(data) {

			var areaChartData = {
			labels  : data.labels,
				datasets: [
				{
					label 				: "Karyawan ",
					backgroundColor		: 'rgba(60,141,188,0.9)',
					borderColor			: 'rgba(60,141,188,0.8)',
					pointRadius			: false,
					pointColor 			: '#3b8bba',
					pointStrokeColor	: 'rgba(60,141,188,1)',
					pointHighlightFill	: '#fff',
					pointHighlightStroke: 'rgba(60,141,188,1)',
					data 				: data.total
				}
				]
			}

			canvas = document.getElementById('canvas');
			var barChartCanvas = $('#last-contract-chart').get(0).getContext('2d')
			var barChartData = jQuery.extend(true, {}, areaChartData)
			var temp0 = areaChartData.datasets[0]

			barChartData.datasets[0] = temp0
			var barChartOptions = {
				responsive              : true,
				maintainAspectRatio     : false,
				datasetFill             : false,
				hover: {
					animationDuration: 0
				},
				animation: {
					duration: 1,
					onComplete: function () {
						var chartInstance = this.chart,
						ctx = chartInstance.ctx;
						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
						ctx.textAlign = 'center';
						ctx.textBaseline = 'bottom';
						this.data.datasets.forEach(function (dataset, i) {
							var meta = chartInstance.controller.getDatasetMeta(i);
							meta.data.forEach(function (bar, index) {
								var data = dataset.data[index];
								ctx.fillText(data, bar._model.x, bar._model.y - 5);
							});
						});
					}
				}
			}

			var barChart = new Chart(barChartCanvas, {
				type: 'bar', 
				data: barChartData,
				options: barChartOptions
			});
		}
	});

	$.ajax({
		url: "<?php echo base_url('dashboard/chart_employee/'.$branch_id.'/'.$business_id); ?>",
		dataType: "json",
		success: function(data) {

			var areaChartData = {
			labels  : data.labels,
				datasets: [
				{
					label 				: "Karyawan ",
					backgroundColor		: 'rgba(60,141,188,0.9)',
					borderColor			: 'rgba(60,141,188,0.8)',
					pointRadius			: false,
					pointColor 			: '#3b8bba',
					pointStrokeColor	: 'rgba(60,141,188,1)',
					pointHighlightFill	: '#fff',
					pointHighlightStroke: 'rgba(60,141,188,1)',
					data 				: data.total
				}
				]
			}

			canvas = document.getElementById('canvas');
			var barChartCanvas = $('#employee-chart').get(0).getContext('2d')
			var barChartData = jQuery.extend(true, {}, areaChartData)
			var temp0 = areaChartData.datasets[0]

			barChartData.datasets[0] = temp0


			var barChartOptions = {
				responsive              : true,
				maintainAspectRatio     : false,
				datasetFill             : false,
				hover: {
					animationDuration: 0
				},
				animation: {
					duration: 1,
					onComplete: function () {
						var chartInstance = this.chart,
						ctx = chartInstance.ctx;
						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
						ctx.textAlign = 'center';
						ctx.textBaseline = 'bottom';
						this.data.datasets.forEach(function (dataset, i) {
							var meta = chartInstance.controller.getDatasetMeta(i);
							meta.data.forEach(function (bar, index) {
								var data = dataset.data[index];
								ctx.fillText(data, bar._model.x, bar._model.y - 5);
							});
						});
					}
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}

			var barChart = new Chart(barChartCanvas, {
				type: 'bar', 
				data: barChartData,
				options: barChartOptions
			});
		}
	});

	$.ajax({
		url: "<?php echo base_url('dashboard/chart_cuti'); ?>",
		dataType: "json",
		success: function(data) {

			var areaChartData = {
			labels  : data.labels,
				datasets: [
				{
					label 				: "Request ",
					backgroundColor		: 'yellow',
					borderColor			: 'rgba(60,141,188,0.8)',
					pointRadius			: false,
					pointColor 			: '#3b8bba',
					pointStrokeColor	: 'rgba(60,141,188,1)',
					pointHighlightFill	: '#fff',
					pointHighlightStroke: 'rgba(60,141,188,1)',
					data 				: data.value1
				},
				{
					label 				: "Approve ",
					backgroundColor		: 'green',
					borderColor			: 'rgba(60,141,188,0.8)',
					pointRadius			: false,
					pointColor 			: '#3b8bba',
					pointStrokeColor	: 'rgba(60,141,188,1)',
					pointHighlightFill	: '#fff',
					pointHighlightStroke: 'rgba(60,141,188,1)',
					data 				: data.value2
				},
				{
					label 				: "Reject ",
					backgroundColor		: 'red',
					borderColor			: 'rgba(60,141,188,0.8)',
					pointRadius			: false,
					pointColor 			: '#3b8bba',
					pointStrokeColor	: 'rgba(60,141,188,1)',
					pointHighlightFill	: '#fff',
					pointHighlightStroke: 'rgba(60,141,188,1)',
					data 				: data.value3
				}
				]
			}

			canvas = document.getElementById('cuti-chart');
			var barChartCanvas = $('#cuti-chart').get(0).getContext('2d')
			var barChartData = jQuery.extend(true, {}, areaChartData)

			var barChartOptions = {
				responsive              : true,
				maintainAspectRatio     : false,
				datasetFill             : false,
				hover: {
					animationDuration: 0
				},
				animation: {
					duration: 1,
					onComplete: function () {
						var chartInstance = this.chart,
						ctx = chartInstance.ctx;
						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
						ctx.textAlign = 'center';
						ctx.textBaseline = 'bottom';
						this.data.datasets.forEach(function (dataset, i) {
							var meta = chartInstance.controller.getDatasetMeta(i);
							meta.data.forEach(function (bar, index) {
								var data = dataset.data[index];
								ctx.fillText(data, bar._model.x, bar._model.y - 5);
							});
						});
					}
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}

			var barChart = new Chart(barChartCanvas, {
				type: 'bar', 
				data: barChartData,
				options: barChartOptions
			});

			canvas.onclick = function(evt) {
				var activePoint = barChart.getElementAtEvent(evt)[0];
				var data = activePoint._chart.data;
				var datasetIndex = activePoint._datasetIndex;
				
				var label = data.datasets[datasetIndex].label;
				var value = data.datasets[datasetIndex].data[activePoint._index];
				
				console.log(activePoint._index, label, value);
			};
		}
	});
	
	$.ajax({
		url: "<?php echo base_url('dashboard/chart_position/'.$branch_id.'/'.$business_id); ?>",
		dataType: "json",
		success: function(data) {

			var areaChartData = {
			labels  : data.labels,
				datasets: [
				{
					label 				: "Karyawan ",
					backgroundColor		: 'rgba(60,141,188,0.9)',
					borderColor			: 'rgba(60,141,188,0.8)',
					pointRadius			: false,
					pointColor 			: '#3b8bba',
					pointStrokeColor	: 'rgba(60,141,188,1)',
					pointHighlightFill	: '#fff',
					pointHighlightStroke: 'rgba(60,141,188,1)',
					data 				: data.total
				}
				]
			}

			canvas = document.getElementById('canvas');
			var barChartCanvas = $('#position-chart').get(0).getContext('2d')
			var barChartData = jQuery.extend(true, {}, areaChartData)
			var temp0 = areaChartData.datasets[0]

			barChartData.datasets[0] = temp0


			var barChartOptions = {
				responsive              : true,
				maintainAspectRatio     : false,
				datasetFill             : false,
				hover: {
					animationDuration: 0
				},
				animation: {
					duration: 1,
					onComplete: function () {
						var chartInstance = this.chart,
						ctx = chartInstance.ctx;
						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
						ctx.textAlign = 'center';
						ctx.textBaseline = 'bottom';
						this.data.datasets.forEach(function (dataset, i) {
							var meta = chartInstance.controller.getDatasetMeta(i);
							meta.data.forEach(function (bar, index) {
								var data = dataset.data[index];
								ctx.fillText(data, bar._model.x, bar._model.y - 5);
							});
						});
					}
				}
			}

			var barChart = new Chart(barChartCanvas, {
				type: 'bar', 
				data: barChartData,
				options: barChartOptions
			});
		}
	});

	$('#table-site').DataTable({
			'autoWidth'	: false,
			'scrollY'	: 300,
			'scrollX'	: true,
			'paging'	:false,
			'info'		:false,
			'ajax': {
				'url':'<?php echo base_url('dashboard/list_site/'.$branch_id.'/'.$business_id); ?>'
			},
			'columns': [
				{ data: 'name' },
				{ data: 'end_contract' },
				{ data: 'total' },
			],
			'order': [[2, 'desc']]
	});

	$.ajax({
		url: "<?php echo base_url('dashboard/chart_salary/'.$branch_id.'/'.$business_id); ?>",
		dataType: "json",
		success: function(data) {

			var areaChartData = {
			labels  : data.labels,
				datasets: [
				{
					label 				: "Pelamar ",
					backgroundColor		: 'rgba(60,141,188,0.9)',
					borderColor			: 'rgba(60,141,188,0.8)',
					pointRadius			: false,
					pointColor 			: '#3b8bba',
					pointStrokeColor	: 'rgba(60,141,188,1)',
					pointHighlightFill	: '#fff',
					pointHighlightStroke: 'rgba(60,141,188,1)',
					data 				: data.total
				}
				]
			}

			canvas = document.getElementById('canvas');
			var barChartCanvas = $('#salary-chart').get(0).getContext('2d')
			var barChartData = jQuery.extend(true, {}, areaChartData)
			var temp0 = areaChartData.datasets[0]

			barChartData.datasets[0] = temp0


			var barChartOptions = {
				responsive              : true,
				maintainAspectRatio     : false,
				datasetFill             : false,
				hover: {
					animationDuration: 0
				},
				animation: {
					duration: 1,
					onComplete: function () {
						var chartInstance = this.chart,
						ctx = chartInstance.ctx;
						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
						ctx.textAlign = 'left';
						ctx.textBaseline = 'top';
						this.data.datasets.forEach(function (dataset, i) {
							var meta = chartInstance.controller.getDatasetMeta(i);
							meta.data.forEach(function (bar, index) {
								var data = dataset.data[index];
								ctx.fillText(data, bar._model.x + 5, bar._model.y - 5);
							});
						});
					}
				}
			}

			var barChart = new Chart(barChartCanvas, {
				type: 'horizontalBar', 
				data: barChartData,
				options: barChartOptions
			});
		}
	});
});
</script>
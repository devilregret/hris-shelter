<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6"><h1><?php echo $_TITLE_ ?></h1></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $_TITLE_ ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card card-primary card-outline col-12">
						<div class="card-body">
							<div class="row">
								<div class="col-md-12">
									<div class="card">
										<div class="card-body">
											<div class="scroll-panel">
												<table id="data-table" class="table table-bordered table-hover">
													<thead>
														<tr>
															<th style="width: 10px">#</th>
															<th>Site</th>
															<th>Alamat</th>
															<th style="width: 40px">Jumlah</th>
														</tr>
													</thead>
													<tbody>
														<?php 
														$total = 0;
														$no = 1;
														foreach ($count_employee as $item): 
														?>
														<tr>
															<td><?php echo $no; ?></td>
															<td><?php echo $item->name; ?></td>
															<td><?php echo $item->address; ?></td>
															<td><?php echo $item->employee; ?></td>
														</tr>
														<?php 
														$total = $total + $item->employee;
														$no++;
														endforeach; 
														?>
													</tbody>
												</table>
											</div>
										</div>
										<div class="card-header">
											<h3 class="card-title"><strong><?php echo $total; ?> Karyawan Security</strong></h3>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
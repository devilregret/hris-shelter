<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6"><h1><?php echo $_TITLE_ ?></h1></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url('dashboard/recruitment'); ?>"><?php echo $_TITLE_ ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="card card-primary">
							<div class="card-header">
								<h3 class="card-title">PELAMAR HARI INI : <?php echo (int) $register_today; ?> PELAMAR</h3>
							</div>
							<div class="card-body">
								<div class="scroll-panel" style="height:420px !important;">
								<table id="table-today" class="table table-bordered table-hover">
									<thead>
										<tr>
											<th>NAMA</th>
											<th>NIK</th>
											<th>NOMOR HP</th>
											<th>JENIS KELAMIN</th>
											<th>TANGGAL LAHIR</th>
											<th>PENDIDIKAN</th>
											<th>JURUSAN</th>
											<th>PENGALAMAN</th>
											<th>PREFERENCE</th>
											<th>PERUSAHAAN YANG DILAMAR</th>
											<th>DOMISILI</th>
										</tr>
									</thead>
								</table>
								</div>
							</div>
						</div>
					</div>
					<div class="card card-primary card-outline col-12">
						<div class="card-header">
							<form action="<?php echo base_url('dashboard/recruitment');?>" method="get">
							<div class="row">
								<div class="col-md-6">
									&nbsp;
								</div>
								<div class="col-md-2">
									<select class="form-control select2" name="branch_id">
										<?php 
										foreach ($list_branch as $item) :  
											if($item->id == 1){
												$item->name = "SEMUA BRANCH";
											}
										?>
										<option value="<?php echo $item->id;?>" <?php if($item->id == $branch_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-md-2">
									<div class="input-group date" id="reservationdate" data-target-input="nearest">
										<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name='period' value="<?php echo $period; ?>"/>
										<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="fa fa-calendar"></i></div>
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<button class="btn btn-primary btn-sm" style="width: 100px;"> CARI</button> &nbsp;
									<a href="<?php echo base_url('dashboard'); ?>" class="btn btn-primary btn-sm" style="width: 100px;">RESET</a>
								</div>
							</div>
							</form>
						</div>
						<div class="card bg-gradient-info">
							<div class="card-header border-0">
								<h3 class="card-title">
									<i class="fas fa-th mr-1"></i>
									Resume Pelamar
								</h3>
								<div class="card-tools">
									<button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
										<i class="fas fa-minus"></i>
									</button>
								</div>
							</div>
							<div class="card-body">
								<canvas class="chart" id="applicant-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
							</div>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box">
										<span class="info-box-icon bg-secondary elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Pelamar Total</span>
											<a href="<?php echo base_url('candidate/list/all/'.$branch_id); ?>">
												<span class="info-box-number"><?php echo $candidate_total; ?>&nbsp;<small>Pelamar</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Pelamar Aktif</span>
											<a href="<?php echo base_url('candidate/list/active/'.$branch_id); ?>">
												<span class="info-box-number"><?php echo $candidate_active; ?>&nbsp;<small>Pelamar</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan NonJob</span>
											<a href="<?php echo base_url('candidate/list/nonjob/'.$branch_id); ?>">
												<span class="info-box-number"><?php echo $candidate_nonjob; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-danger elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Belum Lengkap</span>
											<a href="<?php echo base_url('candidate/list/notcomplete/'.$branch_id); ?>">
												<span class="info-box-number"><?php echo $candidate_notcomplete; ?>&nbsp;<small>Kandidat</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-info elevation-1"><i class="fas fa-clipboard-check"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Pengajuan Client </span>
											<a href="<?php echo base_url('submission/client'); ?>">
												<span class="info-box-number"><?php echo $total_client; ?>&nbsp;<small>Kandidat</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-primary elevation-1"><i class="fas fa-clipboard-check"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Pengajuan Personalia</span>
											<a href="<?php echo base_url('submission/personalia'); ?>">
												<span class="info-box-number"><?php echo $total_personalia; ?>&nbsp;<small>Kandidat</small></span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="card card-primary">
									<div class="card-header">
										<h3 class="card-title">RESUME KATEGORI PEKERJAAN</h3>
									</div>
									<div class="card-body">
										<div class="chart">
											<canvas id="job-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card card-primary">
									<div class="card-header">
										<h3 class="card-title">RESUME KATEGORI PEKERJAAN</h3>
									</div>
									<div class="card-body">
										<table id="table-job" class="table table-bordered table-hover">
											<thead>
												<tr>
													<th>KATEGORI</th>
													<th>PELAMAR</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th>TOTAL</th>
													<th><?php echo $total_resume_job; ?></th>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card card-primary">
									<div class="card-header">
										<h3 class="card-title">RESUME PENDIDIKAN</h3>
									</div>
									<div class="card-body">
										<div class="chart">
											<canvas id="education-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card card-primary">
									<div class="card-header">
										<h3 class="card-title">RESUME INFO LOKER</h3>
									</div>
									<div class="card-body">
										<div class="chart">
											<canvas id="recommendation-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card card-primary">
									<div class="card-header">
										<h3 class="card-title">RESUME USIA PELAMAR</h3>
									</div>
									<div class="card-body">
										<div class="chart">
											<canvas id="age-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Perjanjian Kerjasama</h3>
						</div>
						<div class="card-body">
							<table id="data-table" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th id='agency_name'>Nama Instansi</th>
										<th id='start_contract'>Awal Kontrak</th>
										<th id='end_contract '>Akhir Kontrak</th>
										<th id='city_name'>Kota</th>
										<th id='province_name'>Provinsi</th>
										<th id='branch_name'>Branch</th>
										<th id='document'>Dokumen</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php echo asset_js("plugins/chart.js/Chart.min.js"); ?>
<script>
$(document).ready(function() {
	$.ajax({
		url: "<?php echo base_url('dashboard/chart_job/'.(isset($branch_id) ? $branch_id : '').'/'.(isset($_GET['period']) ? $_GET['period'] : '')) ?>",
		dataType: "json",
		success: function(data) {

			var areaChartData = {
			labels  : data.labels,
				datasets: [
				{
					label 				: "Pelamar ",
					backgroundColor		: 'rgba(60,141,188,0.9)',
					borderColor			: 'rgba(60,141,188,0.8)',
					pointRadius			: false,
					pointColor 			: '#3b8bba',
					pointStrokeColor	: 'rgba(60,141,188,1)',
					pointHighlightFill	: '#fff',
					pointHighlightStroke: 'rgba(60,141,188,1)',
					data 				: data.total
				}
				]
			}

			canvas = document.getElementById('canvas');
			var barChartCanvas = $('#job-chart').get(0).getContext('2d')
			var barChartData = jQuery.extend(true, {}, areaChartData)
			var temp0 = areaChartData.datasets[0]

			barChartData.datasets[0] = temp0


			var barChartOptions = {
				responsive              : true,
				maintainAspectRatio     : false,
				datasetFill             : false,
				hover: {
					animationDuration: 0
				},
				animation: {
					duration: 1,
					onComplete: function () {
						var chartInstance = this.chart,
						ctx = chartInstance.ctx;
						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
						ctx.textAlign = 'center';
						ctx.textBaseline = 'bottom';
						this.data.datasets.forEach(function (dataset, i) {
							var meta = chartInstance.controller.getDatasetMeta(i);
							meta.data.forEach(function (bar, index) {
								var data = dataset.data[index];
								ctx.fillText(data, bar._model.x, bar._model.y - 5);
							});
						});
					}
				}
			}

			var barChart = new Chart(barChartCanvas, {
				type: 'bar', 
				data: barChartData,
				options: barChartOptions
			});
		}
	});

	$.ajax({
		url: "<?php echo base_url('dashboard/chart_education/'.(isset($branch_id) ? $branch_id : '').'/'.(isset($_GET['period']) ? $_GET['period'] : '')) ?>",
		dataType: "json",
		success: function(data) {

			var areaChartData = {
			labels  : data.labels,
				datasets: [
				{
					label 				: "Pelamar ",
					backgroundColor		: 'rgba(60,141,188,0.9)',
					borderColor			: 'rgba(60,141,188,0.8)',
					pointRadius			: false,
					pointColor 			: '#3b8bba',
					pointStrokeColor	: 'rgba(60,141,188,1)',
					pointHighlightFill	: '#fff',
					pointHighlightStroke: 'rgba(60,141,188,1)',
					data 				: data.total
				}
				]
			}

			canvas = document.getElementById('canvas');
			var barChartCanvas = $('#education-chart').get(0).getContext('2d')
			var barChartData = jQuery.extend(true, {}, areaChartData)
			var temp0 = areaChartData.datasets[0]

			barChartData.datasets[0] = temp0


			var barChartOptions = {
				responsive              : true,
				maintainAspectRatio     : false,
				datasetFill             : false,
				hover: {
					animationDuration: 0
				},
				animation: {
					duration: 1,
					onComplete: function () {
						var chartInstance = this.chart,
						ctx = chartInstance.ctx;
						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
						ctx.textAlign = 'left';
						ctx.textBaseline = 'top';
						this.data.datasets.forEach(function (dataset, i) {
							var meta = chartInstance.controller.getDatasetMeta(i);
							meta.data.forEach(function (bar, index) {
								var data = dataset.data[index];
								ctx.fillText(data, bar._model.x + 5, bar._model.y - 5);
							});
						});
					}
				}
			}

			var barChart = new Chart(barChartCanvas, {
				type: 'horizontalBar', 
				data: barChartData,
				options: barChartOptions
			});
		}
	});

	$.ajax({
		url: "<?php echo base_url('dashboard/chart_recommendation/'.(isset($branch_id) ? $branch_id : '').'/'.(isset($_GET['period']) ? $_GET['period'] : '')) ?>",
		dataType: "json",
		success: function(data) {

			var areaChartData = {
			labels  : data.labels,
				datasets: [
				{
					label 				: "Pelamar ",
					backgroundColor		: 'rgba(60,141,188,0.9)',
					borderColor			: 'rgba(60,141,188,0.8)',
					pointRadius			: false,
					pointColor 			: '#3b8bba',
					pointStrokeColor	: 'rgba(60,141,188,1)',
					pointHighlightFill	: '#fff',
					pointHighlightStroke: 'rgba(60,141,188,1)',
					data 				: data.total
				}
				]
			}

			canvas = document.getElementById('canvas');
			var barChartCanvas = $('#recommendation-chart').get(0).getContext('2d')
			var barChartData = jQuery.extend(true, {}, areaChartData)
			var temp0 = areaChartData.datasets[0]

			barChartData.datasets[0] = temp0


			var barChartOptions = {
				responsive              : true,
				maintainAspectRatio     : false,
				datasetFill             : false,
				hover: {
					animationDuration: 0
				},
				animation: {
					duration: 1,
					onComplete: function () {
						var chartInstance = this.chart,
						ctx = chartInstance.ctx;
						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
						ctx.textAlign = 'left';
						ctx.textBaseline = 'top';
						this.data.datasets.forEach(function (dataset, i) {
							var meta = chartInstance.controller.getDatasetMeta(i);
							meta.data.forEach(function (bar, index) {
								var data = dataset.data[index];
								ctx.fillText(data, bar._model.x + 5, bar._model.y - 5);
							});
						});
					}
				}
			}

			var barChart = new Chart(barChartCanvas, {
				type: 'horizontalBar', 
				data: barChartData,
				options: barChartOptions
			});
		}
	});

	$.ajax({
		url: "<?php echo base_url('dashboard/chart_age/'.(isset($branch_id) ? $branch_id : '').'/'.(isset($_GET['period']) ? $_GET['period'] : '')) ?>",
		dataType: "json",
		success: function(data) {

			var areaChartData = {
			labels  : data.labels,
				datasets: [
				{
					label 				: "Pelamar ",
					backgroundColor		: 'rgba(60,141,188,0.9)',
					borderColor			: 'rgba(60,141,188,0.8)',
					pointRadius			: false,
					pointColor 			: '#3b8bba',
					pointStrokeColor	: 'rgba(60,141,188,1)',
					pointHighlightFill	: '#fff',
					pointHighlightStroke: 'rgba(60,141,188,1)',
					data 				: data.total
				}
				]
			}

			canvas = document.getElementById('canvas');
			var barChartCanvas = $('#age-chart').get(0).getContext('2d')
			var barChartData = jQuery.extend(true, {}, areaChartData)
			var temp0 = areaChartData.datasets[0]

			barChartData.datasets[0] = temp0


			var barChartOptions = {
				responsive              : true,
				maintainAspectRatio     : false,
				datasetFill             : false,
				hover: {
					animationDuration: 0
				},
				animation: {
					duration: 1,
					onComplete: function () {
						var chartInstance = this.chart,
						ctx = chartInstance.ctx;
						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
						ctx.textAlign = 'left';
						ctx.textBaseline = 'top';
						this.data.datasets.forEach(function (dataset, i) {
							var meta = chartInstance.controller.getDatasetMeta(i);
							meta.data.forEach(function (bar, index) {
								var data = dataset.data[index];
								ctx.fillText(data, bar._model.x + 5, bar._model.y - 5);
							});
						});
					}
				}
			}

			var barChart = new Chart(barChartCanvas, {
				type: 'horizontalBar', 
				data: barChartData,
				options: barChartOptions
			});
		}
	});

	$.ajax({
		url: "<?php echo base_url('dashboard/chart_applicant/'.(isset($_GET['branch_id']) ? $_GET['branch_id'] : '')); ?>",
		dataType: "json",
		success: function(data) {

			var areaChartData = {
			labels  : data.labels,
				datasets: [{
					label: 'Pelamar',
					fill: false,
					borderWidth: 2,
					lineTension: 0,
					spanGaps: true,
					borderColor: '#efefef',
					pointRadius: 3,
					pointHoverRadius: 7,
					pointColor: '#efefef',
					pointBackgroundColor: '#efefef',
					data : data.total
				}]
			}

			canvas = document.getElementById('canvas');
			var barChartCanvas = $('#applicant-chart').get(0).getContext('2d')
			var barChartData = jQuery.extend(true, {}, areaChartData)
			var temp0 = areaChartData.datasets[0]

			barChartData.datasets[0] = temp0


			var barChartOptions = {
				maintainAspectRatio: false,
				responsive: true,
				legend: {
					display: false
				},
				scales: {
					xAxes: [{
						ticks: {
							fontColor: '#efefef'
						},
						gridLines: {
							display: false,
							color: '#efefef',
							drawBorder: false
						}
					}],
					yAxes: [{
						ticks: {
							stepSize: 500,
							fontColor: '#efefef'
						},
						gridLines: {
							display: true,
							color: '#efefef',
							drawBorder: false
						}
					}]
				},
				hover: {
					animationDuration: 0
				},
				animation: {
					duration: 1,
					onComplete: function () {
						var chartInstance = this.chart,
						ctx = chartInstance.ctx;
						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
						ctx.textAlign = 'center';
						ctx.textBaseline = 'bottom';
						this.data.datasets.forEach(function (dataset, i) {
							var meta = chartInstance.controller.getDatasetMeta(i);
							meta.data.forEach(function (bar, index) {
								var data = dataset.data[index];
								ctx.fillText(data, bar._model.x, bar._model.y - 5);
							});
						});
					}
				}
			}

			var barChart = new Chart(barChartCanvas, {
				type: 'line', 
				data: barChartData,
				options: barChartOptions
			});
		}
	});

	$('#table-job').DataTable({
			'autoWidth'	: false,
			'scrollY'	: 300,
			'scrollX'	: true,
			'paging'	:false,
			'info'		:false,
			'ajax': {
				'url':'<?php echo base_url("dashboard/list_job/".(isset($branch_id) ? $branch_id : '').'/'.(isset($_GET['period']) ? $_GET['period'] : ''));?>'
			},
			'columns': [
				{ data: 'name' },
				{ data: 'total' },
			],
			'order': [[0, 'asc']]
	});
	
	$('#reservationdate').datetimepicker({
		format: 'YYYY-MM'
	});

	$('#table-today').DataTable({
			'autoWidth'	: false,
			'scrollY'	: 300,
			'scrollX'	: true,
			'paging'	:false,
			'info'		:false,
			'ajax': {
				'url':'<?php echo base_url("dashboard/list_employee_today");?>'
			},
			'columns': [
				{ data: 'full_name' },
				{ data: 'id_card' },
				{ data: 'phone_number' },
				{ data: 'gender' },
				{ data: 'date_birth' },
				{ data: 'education_level' },
				{ data: 'education_majors' },
				{ data: 'experience' },
				{ data: 'preference_job1' },
				{ data: 'site_preference' },
				{ data: 'address' }
			],
			'order': [[0, 'asc']]
	});

	$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("agreement/list_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'agency_name' },
				{ data: 'start_contract' },
				{ data: 'end_contract' },
				{ data: 'city_name' },
				{ data: 'province_name' },
				{ data: 'branch_name' },
				{ data: 'document' }
			],
			'order': [[2, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>=0 && index<=5){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
});
</script>
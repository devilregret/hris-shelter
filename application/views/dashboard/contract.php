<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6"><h1><?php echo $_TITLE_ ?></h1></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $_TITLE_ ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card card-danger card-outline col-12">
						<div class="card-header-">
							<h3 class="card-title"><strong>Daftar Kontrak Habis</strong></h3>
						</div>
						<div class="card-body">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Keterangan</th>
										<th>Detail</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$no = 1;
									foreach ($list_expired as $key => $item): 
										$total_summary = 0;
									?>
									<tr>
										<td><?php echo $no; ?></td>
										<td><?php echo $key; ?></td>
										<td>
											<table class="table table-bordered">
												<tbody>
													<tr>
														<th style="width:10%">Bisnis Unit</th>
														<th style="width:60%">Nama Site</th>
														<th style="width:10%">Branch</th>
														<th style="width:20%">Jumlah</th>
													</tr>
													<?php foreach ($item['summary'] as $detail): 
														$total_summary += $detail->summary; ?>
													<tr>
														<td><?php echo $detail->company_code; ?></td>
														<td><?php echo $detail->site_name; ?></td>
														<td><?php echo $detail->branch_name; ?></td>
														<td><?php echo $detail->summary." Karyawan"; ?></td>
													</tr>
													<?php endforeach; ?>
												</tbody>
											</table>
										</td>
										<td>
											<?php echo $total_summary." Karyawan"; ?>
										</td>
									</tr>
									<?php 
									$no++;
									endforeach; 
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6"><h1><?php echo $_TITLE_ ?></h1></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $_TITLE_ ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card card-primary card-outline col-12">
						<div class="card-body">
							<div class="row">
								<div class="col-md-12">
									<div class="card-header">
										<?php if($message != ''): echo $message; endif; ?>
										<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
									</div>	
									<div class="card card-primary">
										<div class="card-header">
											<h3 class="card-title"><?php if($site_name == ''): echo 'Silahkan Pilih Site';else: echo $site_name; endif; ?></h3>
										</div>
										<div class="card-body">
											<form method="POST" action="<?php echo base_url('dashboard'); ?>">
												<div class="row">
													<div class="col-sm-4" >
														<label>Site Bisnis</label>
														<select class="form-control select2" name="site_id" required>
															<option value=""> &nbsp; </option>
															<?php foreach ($list_site as $item) :  ?>
															<option value="<?php echo $item->id;?>" <?php if($item->id == $site_id): echo 'selected'; endif;?>><?php echo
																$item->name.' - '.$item->company_code.' - '.$item->branch_name; ?></option>
															<?php endforeach; ?>
														</select>
													</div>
													<div class="col-sm-3" >
														<label for="exampleInputEmail1">Username</label>
														<input type="text" class="form-control" name="username" placeholder="username/email" value="<?php echo $this->session->userdata('username'); ?>" required readonly>
													</div>
													<div class="col-sm-3" >
														<label for="exampleInputEmail1">Password</label>
														<input type="password" name="password" class="form-control" placeholder="Password" required>
													</div>
													<div class="col-sm-1" >
														<label>&nbsp;</label>
														<button type="submit" class="btn btn-primary btn-block">Simpan</button>
													</div>
												</div>
											</form>
										</div>
									</div>
									<div class="card">
										<div class="card-header">
											<h3 class="card-title"><strong>Jumlah Karyawan</strong></h3>
										</div>
										<div class="card-body">
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="width: 10px">#</th>
														<th>Site</th>
														<th>Alamat</th>
														<th style="width: 40px">Jumlah</th>
													</tr>
												</thead>
												<tbody>
													<?php 
													$no = 1;
													foreach ($count_employee as $item): 
													?>
													<tr>
														<td><?php echo $no; ?></td>
														<td><?php echo $item->name; ?></td>
														<td><?php echo $item->address; ?></td>
														<td><?php echo $item->employee; ?></td>
													</tr>
													<?php 
													$no++;
													endforeach; 
													?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
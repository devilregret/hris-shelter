<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6"><h1><?php echo $_TITLE_ ?></h1></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $_TITLE_ ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card card-primary card-outline col-12">
						<div class="card-body">
							<div class="row">
								<div class="col-12 col-sm-6 col-md-3">
									<div class="info-box">
										<span class="info-box-icon bg-info elevation-1"><i class="fa fa-wrench"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Baru</span>
												<span class="info-box-number"><?php echo $development_new; ?>&nbsp;<small>Development</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-3">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-warning elevation-1"><i class="fa fa-wrench"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Processed</span>
												<span class="info-box-number"><?php echo $development_processed; ?>&nbsp;<small>Development</small></span>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-3">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-success elevation-1"><i class="fa fa-wrench"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Done</span>
												<span class="info-box-number"><?php echo $development_done; ?>&nbsp;<small>Done</small></span>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-3">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-secondary elevation-1"><i class="fa fa-wrench"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Closed</span>
												<span class="info-box-number"><?php echo $development_closed; ?>&nbsp;<small>Closed</small></span>
										</div>
									</div>
								</div>
							</div>
							<p>&nbsp;</p>
							<div class="row">
								<div class="col-sm-4">
									<strong>Pilih Status ditampilkan :</strong>
								</div>
								<div class="col-sm-4">
									<select class="form-control preview float-right">
										<option value="<?php echo base_url("dashboard/development/all");?>" <?php if($view == "all" ): echo "selected"; endif; ?>>ALL</option>
										<option value="<?php echo base_url("dashboard/development/new");?>" <?php if($view == "new" ): echo "selected"; endif; ?>>NEW</option>
										<option value="<?php echo base_url("dashboard/development/process");?>" <?php if($view == "processed" ): echo "selected"; endif; ?>>PROCESSED</option>
										<option value="<?php echo base_url("dashboard/development/done");?>" <?php if($view == "done" ): echo "selected"; endif; ?>>DONE</option>
										<option value="<?php echo base_url("dashboard/development/closed");?>" <?php if($view == "closed" ): echo "selected"; endif; ?>>CLOSED</option>
									</select>
								</div>
								<div class="col-sm-4">
								</div>
							</div>
							<br>
							<div class="scroll-panel">
								<table id="data-table" class="table table-bordered table-hover">
									<thead>
										<tr>
											<th id='title'>Judul</th>
											<th id='description'>Deskripsi</th>
											<th id='status'>Status</th>
											<th id='target'>Target Selesai</th>
											<th id='cost'>Estimasi Biaya</th>
											<th id='priority'>Priority</th>
											<th id='position'>Divisi</th>
											<th id='city'>Kota</th>
											<th id='developer'>Developer</th>
											<th id='created_at'>Tanggal Dibuat</th>
											<th id='created_by'>Dibuat Oleh</th>
											<th id='updated_at'>Update Terakhir</th>
											<th id='updated_by'>Diupdate Oleh</th>
											<th width="105px">#</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.preview').on('change', function() {
			window.location.href = this.value;
		});
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("dashboard/development_ajax/".$view);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'title' },
				{ data: 'description' },
				{ data: 'status' },
				{ data: 'target' },
				{ data: 'cost' },
				{ data: 'priority' },
				{ data: 'position' },
				{ data: 'city' },
				{ data: 'developer' },
				{ data: 'created_at' },
				{ data: 'created_by' },
				{ data: 'updated_at' },
				{ data: 'updated_by' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[1, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>=0 && index<13){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
	});
</script>
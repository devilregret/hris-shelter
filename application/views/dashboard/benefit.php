<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6"><h1><?php echo $_TITLE_ ?></h1></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $_TITLE_ ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card card-primary card-outline col-12">
						<div class="card-body">
							<div class="row">
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box">
										<span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan Direct</span>
											<a href="<?php echo base_url('benefit_employee/list'); ?>">
												<span class="info-box-number"><?php echo $total_direct; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan Indirect</span>
											<a href="<?php echo base_url('benefit_employee/list'); ?>">
												<span class="info-box-number"><?php echo $total_indirect; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-secondary elevation-1"><i class="fas fa-shield-virus"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">BPJS TK Belum dibuat</span>
											<a href="<?php echo base_url('benefit_employee/list'); ?>">
												<span class="info-box-number"><?php echo $benefit_labor; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-info elevation-1"><i class="fas fa-medkit"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">BPJS KS Belum dibuat</span>
											<a href="<?php echo base_url('benefit_employee/list'); ?>">
												<span class="info-box-number"><?php echo $benefit_health; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-warning elevation-1"><i class="fas fa-clinic-medical"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan Resign</span>
											<a href="<?php echo base_url('monitoring_benefit/employee'); ?>">
												<span class="info-box-number"><?php echo $employee_resign; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-danger elevation-1"><i class="fas fa-handshake"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Site Putus</span>
											<a href="<?php echo base_url('monitoring_benefit/site'); ?>">
												<span class="info-box-number"><?php echo $break_site; ?>&nbsp;<small>Site Bisnis</small></span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"><strong>Daftar Pengajuan Resign</strong></h3>
						</div>
						<div class="card-body">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Nomor KTP</th>
										<th>Nama Lengkap</th>
										<th>Tanggal Pengajuan</th>
										<th>Catatan Resign</th>
										<th width="80px">#</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$no = 1;
									foreach ($list_submission as $item): 
									?>
									<tr>
										<td><?php echo $no; ?></td>
										<td><?php echo $item->id_card; ?></td>
										<td><?php echo $item->full_name; ?></td>
										<td><?php echo $item->resign_submit; ?></td>
										<td><?php echo $item->resign_note; ?></td>
										<td>
											<a onclick='preview(this)' class='btn-sm btn-info btn-action'  style='cursor:pointer;' data-href='<?php echo base_url("resign/work/".$item->id); ?>'><i class="fas fa-file-alt text-white"></i></a>
										</td>
									</tr>
									<?php 
									$no++;
									endforeach; 
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

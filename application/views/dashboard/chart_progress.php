<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6"><h1><?php echo $_TITLE_ ?></h1></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url('dashboard/personalia'); ?>"><?php echo $_TITLE_ ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card card-primary card-outline col-12">
						<div class="card-body">
							<div class="row">
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box">
										<span class="info-box-icon bg-warning elevation-1"><i class="fas fa-handshake"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Site East</span>
											<a href="<?php echo base_url('site/list');?>">
												<span class="info-box-number"><?php echo $site_east; ?>&nbsp;<small>Site</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box">
										<span class="info-box-icon bg-warning elevation-1"><i class="fas fa-handshake"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Site Central</span>
											<a href="<?php echo base_url('site/list');?>">
												<span class="info-box-number"><?php echo $site_central; ?>&nbsp;<small>Site</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box">
										<span class="info-box-icon bg-warning elevation-1"><i class="fas fa-handshake"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Site West</span>
											<a href="<?php echo base_url('site/list');?>">
												<span class="info-box-number"><?php echo $site_west; ?>&nbsp;<small>Site</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box">
										<span class="info-box-icon bg-warning elevation-1"><i class="fas fa-handshake"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Site Makasar</span>
											<a href="<?php echo base_url('site/list');?>">
												<span class="info-box-number"><?php echo $site_makasar; ?>&nbsp;<small>Site</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box">
										<span class="info-box-icon bg-warning elevation-1"><i class="fas fa-handshake"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Site Bali</span>
											<a href="<?php echo base_url('site/list');?>">
												<span class="info-box-number"><?php echo $site_bali; ?>&nbsp;<small>Site</small></span>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box">
										<span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan East</span>
											<a href="<?php echo base_url('employee/list/direct');?>">
												<span class="info-box-number"><?php echo $total_east; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box">
										<span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan Central</span>
											<a href="<?php echo base_url('employee/list/direct');?>">
												<span class="info-box-number"><?php echo $total_central; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box">
										<span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan West</span>
											<a href="<?php echo base_url('employee/list/direct');?>">
												<span class="info-box-number"><?php echo $total_west; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box">
										<span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan Makasar</span>
											<a href="<?php echo base_url('employee/list/direct');?>">
												<span class="info-box-number"><?php echo $total_makasar; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box">
										<span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan Bali</span>
											<a href="<?php echo base_url('employee/list/direct');?>">
												<span class="info-box-number"><?php echo $total_bali; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="card card-primary">
						<div class="card-body">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th></th>
										<th>EAST</th>
										<th>CENTRAL</th>
										<th>WEST</th>
										<th>MAKASAR</th>
										<th>BALI</th>
										<th>TOTAL</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>SECURITY</td>
										<td><?php echo $security_east; ?></td>
										<td><?php echo $security_central; ?></td>
										<td><?php echo $security_west; ?></td>
										<td><?php echo $security_makasar; ?></td>
										<td><?php echo $security_bali; ?></td>
										<th><?php echo $security_east + $security_central + $security_west + $security_makasar + $security_bali; ?></th>
									</tr>
									<tr>
										<td>NON SECURITY</td>
										<td><?php echo $nonsecurity_east; ?></td>
										<td><?php echo $nonsecurity_central; ?></td>
										<td><?php echo $nonsecurity_west; ?></td>
										<td><?php echo $nonsecurity_makasar; ?></td>
										<td><?php echo $nonsecurity_bali; ?></td>
										<th><?php echo $nonsecurity_east + $nonsecurity_central + $nonsecurity_west + $nonsecurity_makasar + $nonsecurity_bali; ?></th>
									</tr>
									<tr>
										<td>INDIRECT</td>
										<td><?php echo $indirect_east; ?></td>
										<td><?php echo $indirect_central; ?></td>
										<td><?php echo $indirect_west; ?></td>
										<td><?php echo $indirect_makasar; ?></td>
										<td><?php echo $indirect_bali; ?></td>
										<th><?php echo $indirect_east + $indirect_central + $indirect_west + $indirect_makasar + $indirect_bali; ?></th>
									</tr>
									<tr>
										<th>TOTAL</th>
										<th><?php echo $total_east; ?></th>
										<th><?php echo $total_central; ?></th>
										<th><?php echo $total_west; ?></th>
										<th><?php echo $total_makasar; ?></th>
										<th><?php echo $total_bali; ?></th>
										<th><?php echo $total_east + $total_central + $total_west + $total_makasar + $total_bali; ?></th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
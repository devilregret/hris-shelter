<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6"><h1><?php echo $_TITLE_ ?></h1></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $_TITLE_ ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">	
		<div class="container-fluid">
			<div class="card card-default">
				<div class="card-header">
					<h3 class="card-title">
						Selamat Datang <strong><?php echo $this->session->userdata('name');?></strong>
					</h3>
				</div>
				<br>
				<?php if($candidate): ?>
					<?php if($candidate->status_approval ==  0): ?>
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Silahkan lengkapi data anda agar segera diproses</h3>
						</div>
						<div class="card-body">
							<ul class="todo-list" data-widget="todo-list">
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<div  class="icheck-primary d-inline ml-2">
										<input type="checkbox" value="" name="todo1" id="todoCheck1"><label for="todoCheck1"></label>
									</div>
									<span class="text"></span>
								</li>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<div  class="icheck-primary d-inline ml-2">
										<input type="checkbox" value="" name="todo2" id="todoCheck2" checked>
										<label for="todoCheck2"></label>
									</div>
									<span class="text">Make the theme responsive</span>
								</li>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<div  class="icheck-primary d-inline ml-2">
										<input type="checkbox" value="" name="todo3" id="todoCheck3">
										<label for="todoCheck3"></label>
									</div>
									<span class="text">Let theme shine like a star</span>
								</li>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<div  class="icheck-primary d-inline ml-2">
										<input type="checkbox" value="" name="todo4" id="todoCheck4">
										<label for="todoCheck4"></label>
									</div>
									<span class="text">Let theme shine like a star</span>
								</li>
							</ul>
						</div>
					</div>
				<?php endif; ?>
				<?php else: ?>
					<div class="card-body">
						<div class="alert alert-info alert-dismissible">
							<h5><i class="icon fas fa-ban"></i> Info</h5>
							Silahkan Isi dan Lengkapi <a href="<?php echo base_url('account/register'); ?>">Form Pendaftaran.</a>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
</div>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6"><h1><?php echo $_TITLE_ ?></h1></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url('dashboard/payroll'); ?>"><?php echo $_TITLE_ ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card card-primary card-outline col-12">
						<div class="card-body">
							<div class="row">
								<div class="col-md-12">
									<div class="card card-primary">
										<div class="card-header">
											<h3 class="card-title">RESUME BENEFIT</h3>
										</div>
										<div class="card-body">
											<div class="scroll-panel" style="height:420px !important;">
											<table class="table table-bordered">
												<thead>
													<tr style="text-align:center;">
														<?php $month = strtotime($period); ?>
														<th rowspan="4" style="vertical-align: middle;"><?php echo strtoupper(get_month(date('m', $month))." ".date('Y', $month)); ?></th>
														<th colspan="4">SN</th>
														<th colspan="4">SNI</th>
														<th colspan="4">ION</th>
														<th colspan="4">SAPTA</th>
														<th colspan="4">TOTAL</th>
													</tr>

													<tr>
														<th colspan="2">INDIRECT</th>
														<th colspan="2">DIRECT</th>
														<th colspan="2">INDIRECT</th>
														<th colspan="2">DIRECT</th>
														<th colspan="2">INDIRECT</th>
														<th colspan="2">DIRECT</th>
														<th colspan="2">INDIRECT</th>
														<th colspan="2">DIRECT</th>
														<th colspan="2">INDIRECT</th>
														<th colspan="2">DIRECT</th>
													</tr>
													<tr>
														<th>HC</th>
														<th>BENEFIT</th>
														<th>HC</th>
														<th>BENEFIT</th>
														<th>HC</th>
														<th>BENEFIT</th>
														<th>HC</th>
														<th>BENEFIT</th>
														<th>HC</th>
														<th>BENEFIT</th>
														<th>HC</th>
														<th>BENEFIT</th>
														<th>HC</th>
														<th>BENEFIT</th>
														<th>HC</th>
														<th>BENEFIT</th>
														<th>HC</th>
														<th>BENEFIT</th>
														<th>HC</th>
														<th>BENEFIT</th>
													</tr>
												</thead>
												<tbody>
													<?php 
													$total_ind_sn 	= 0;
													$total_sn 		= 0;
													$total_ind_sni 	= 0;
													$total_sni 		= 0;
													$total_ind_ion 	= 0;
													$total_ion 		= 0;
													$total_ind_sap	= 0;
													$total_sap 		= 0;

													$hc_ind_sn 	= 0;
													$hc_sn 		= 0;
													$hc_ind_sni = 0;
													$hc_sni 	= 0;
													$hc_ind_ion = 0;
													$hc_ion 	= 0;
													$hc_ind_sap	= 0;
													$hc_sap 	= 0;
													foreach($resume_benefit AS $item):
														$total_ind_sn 	+= $item['indirect_sn'];
														$total_sn 		+= $item['sn'];
														$total_ind_sni 	+= $item['indirect_sni'];
														$total_sni 		+= $item['sni'];
														$total_ind_ion 	+= $item['indirect_ion'];
														$total_ion 		+= $item['ion'];
														$total_ind_sap 	+= $item['indirect_sapta'];
														$total_sap 		+= $item['sapta'];

														$hc_ind_sn 		+= $item['indirect_sn_hc'];
														$hc_sn 			+= $item['sn_hc'];
														$hc_ind_sni 	+= $item['indirect_sni_hc'];
														$hc_sni 		+= $item['sni_hc'];
														$hc_ind_ion 	+= $item['indirect_ion_hc'];
														$hc_ion 		+= $item['ion_hc'];
														$hc_ind_sap 	+= $item['indirect_sapta_hc'];
														$hc_sap 		+= $item['sapta_hc'];
													?>
													<tr>
														<td><?php echo $item['branch']; ?></td>
														<td><?php echo $item['indirect_sn_hc']; ?></td>
														<td><?php echo rupiah($item['indirect_sn']); ?></td>
														<td><?php echo $item['sn_hc']; ?></td>
														<td><?php echo rupiah($item['sn']); ?></td>
														<td><?php echo $item['indirect_sni_hc']; ?></td>
														<td><?php echo rupiah($item['indirect_sni']); ?></td>
														<td><?php echo $item['sni_hc']; ?></td>
														<td><?php echo rupiah($item['sni']); ?></td>
														<td><?php echo $item['indirect_ion_hc']; ?></td>
														<td><?php echo rupiah($item['indirect_ion']); ?></td>
														<td><?php echo $item['ion_hc']; ?></td>
														<td><?php echo rupiah($item['ion']); ?></td>
														<td><?php echo $item['indirect_sapta_hc']; ?></td>
														<td><?php echo rupiah($item['indirect_sapta']); ?></td>
														<td><?php echo $item['sapta_hc']; ?></td>
														<td><?php echo rupiah($item['sapta']); ?></td>
														<td><?php echo $item['indirect_sn_hc']+$item['indirect_sni_hc']+$item['indirect_ion_hc']+$item['indirect_sapta_hc']; ?></td>
														<td><?php echo rupiah($item['indirect_sn']+$item['indirect_sni']+$item['indirect_ion']+$item['indirect_sapta']); ?></td>
														<td><?php echo $item['sn_hc']+$item['sni_hc']+$item['ion_hc']+$item['sapta_hc']; ?></td>
														<td><?php echo rupiah($item['sn']+$item['sni']+$item['ion']+$item['sapta']); ?></td>
													</tr>
													<?php
													endforeach;
													?>
													<tr>
														<th>TOTAL</th>
														<th><?php echo $hc_ind_sn; ?></th>
														<th><?php echo rupiah($total_ind_sn); ?></th>
														<th><?php echo $hc_sn; ?></th>
														<th><?php echo rupiah($total_sn); ?></th>
														<th><?php echo $hc_ind_sni; ?></th>
														<th><?php echo rupiah($total_ind_sni); ?></th>
														<th><?php echo $hc_sni; ?></th>
														<th><?php echo rupiah($total_sni); ?></th>
														<th><?php echo $hc_ind_ion; ?></th>
														<th><?php echo rupiah($total_ind_ion); ?></th>
														<th><?php echo $hc_ion; ?></th>
														<th><?php echo rupiah($total_ion); ?></th>
														<th><?php echo $hc_ind_sap; ?></th>
														<th><?php echo rupiah($total_ind_sap); ?></th>
														<th><?php echo $hc_sap; ?></th>
														<th><?php echo rupiah($total_sap); ?></th>
														<th><?php echo $hc_ind_sn+$hc_ind_sni+$hc_ind_ion+$hc_ind_sap; ?></th>
														<th><?php echo rupiah($total_ind_sn+$total_ind_sni+$total_ind_ion+$total_ind_sap); ?></th>
														<th><?php echo $hc_sn+$hc_sni+$hc_ion+$hc_sap; ?></th>
														<th><?php echo rupiah($total_sn+$total_sni+$total_ion+$total_sap); ?></th>
													</tr>
												</tbody>
											</table>
										</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card-header">
							<form action="<?php echo base_url('dashboard/benefit');?>" method="get">
							<div class="row">
								<div class="col-md-4">
									&nbsp;
								</div>
								<div class="col-md-2">
									<select class="form-control select2" name="business_id">
										<option value="">SEMUA BISNIS UNIT</option>
										<?php foreach ($list_company as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id == $business_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-md-2">
									<select class="form-control select2" name="branch_id">
										<?php 
										foreach ($list_branch as $item) :  
											if($item->id == 1){
												$item->name = "SEMUA BRANCH";
											}
										?>
										<option value="<?php echo $item->id;?>" <?php if($item->id == $branch_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-md-2">
									<div class="input-group date" id="reservationdate" data-target-input="nearest">
										<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name='period' value="<?php echo $period; ?>"/>
										<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="fa fa-calendar"></i></div>
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<button class="btn btn-primary btn-sm" style="width: 100px;"> CARI</button> &nbsp;
									<a href="<?php echo base_url('dashboard'); ?>" class="btn btn-primary btn-sm" style="width: 100px;">RESET</a>
								</div>
							</div>
							</form>
						</div>
						<div class="card bg-gradient-info">
							<div class="card-header border-0">
								<h3 class="card-title">
									<i class="fas fa-th mr-1"></i>
									History Benefit
								</h3>
								<div class="card-tools">
									<button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
										<i class="fas fa-minus"></i>
									</button>
								</div>
							</div>
							<div class="card-body">
								<canvas class="chart" id="benefit-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
							</div>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="card card-primary">
										<div class="card-header">
											<h3 class="card-title">PEMBAGIAN PEMBAYARAN</h3>
										</div>
										<div class="card-body">
											<div class="chart">
												<canvas id="payment-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="card card-primary">
										<div class="card-header">
											<h3 class="card-title">BENEFIT BERDASARKAN BISNIS UNIT</h3>
										</div>
										<div class="card-body">
											<div class="chart">
												<canvas id="pieChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 764px;" width="764" height="250" class="chartjs-render-monitor"></canvas>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php echo asset_js("plugins/chart.js/Chart.min.js"); ?>
<script>
$(document).ready(function() {
	$.ajax({
		url: "<?php echo base_url('dashboard/chart_benefit_payment/'.$period.'/'.$branch_id.'/'.$business_id); ?>",
		dataType: "json",
		success: function(data) {

			var areaChartData = {
			labels  : data.labels,
				datasets: [
				{
					label 				: "",
					backgroundColor		: 'rgba(60,141,188,0.9)',
					borderColor			: 'rgba(60,141,188,0.8)',
					pointRadius			: false,
					pointColor 			: '#3b8bba',
					pointStrokeColor	: 'rgba(60,141,188,1)',
					pointHighlightFill	: '#fff',
					pointHighlightStroke: 'rgba(60,141,188,1)',
					data 				: data.total
				}
				]
			}

			canvas = document.getElementById('canvas');
			var barChartCanvas = $('#payment-chart').get(0).getContext('2d')
			var barChartData = jQuery.extend(true, {}, areaChartData)
			var temp0 = areaChartData.datasets[0]

			barChartData.datasets[0] = temp0


			var barChartOptions = {
				responsive              : true,
				maintainAspectRatio     : false,
				datasetFill             : false,
				hover: {
					animationDuration: 0
				},
				animation: {
					duration: 1,
					onComplete: function () {
						var chartInstance = this.chart,
						ctx = chartInstance.ctx;
						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
						ctx.textAlign = 'left';
						ctx.textBaseline = 'top';
						this.data.datasets.forEach(function (dataset, i) {
							var meta = chartInstance.controller.getDatasetMeta(i);
							meta.data.forEach(function (bar, index) {
								var data = dataset.data[index];
								ctx.fillText(data, bar._model.x + 5, bar._model.y - 5);
							});
						});
					}
				}
			}

			var barChart = new Chart(barChartCanvas, {
				type: 'horizontalBar', 
				data: barChartData,
				options: barChartOptions
			});
		}
	});

	$.ajax({
		url: "<?php echo base_url('dashboard/benefit_chart/'.$period.'/'.$branch_id.'/'.$business_id); ?>",
		dataType: "json",
		success: function(data) {

			var areaChartData = {
			labels  : data.labels,
				datasets: [{
					label: 'Juta',
					position: 'right',
					fill: false,
					borderWidth: 2,
					lineTension: 0,
					spanGaps: true,
					borderColor: '#efefef',
					pointRadius: 3,
					pointHoverRadius: 7,
					pointColor: '#efefef',
					pointBackgroundColor: '#efefef',
					data : data.total
				}]
			}

			canvas = document.getElementById('canvas');
			var barChartCanvas = $('#benefit-chart').get(0).getContext('2d')
			var barChartData = jQuery.extend(true, {}, areaChartData)
			var temp0 = areaChartData.datasets[0]

			barChartData.datasets[0] = temp0


			var barChartOptions = {
				maintainAspectRatio: false,
				responsive: true,
				tooltips: {
					callbacks: {
						label: function(tooltipItem, data) {
							return tooltipItem.yLabel+' Juta';
						}
					}
				},
				legend: {
					display: false
				},
				scales: {
					xAxes: [{
						ticks: {
							fontColor: '#efefef'
						},
						gridLines: {
							display: false,
							color: '#efefef',
							drawBorder: false
						}
					}],
					yAxes: [{
						ticks: {
							stepSize: 500,
							fontColor: '#efefef'
						},
						gridLines: {
							display: true,
							color: '#efefef',
							drawBorder: false
						}
					}]
				},
				hover: {
					animationDuration: 0
				},
				animation: {
					duration: 1,
					onComplete: function () {
						var chartInstance = this.chart,
						ctx = chartInstance.ctx;
						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
						ctx.textAlign = 'left';
						ctx.textBaseline = 'top';
						this.data.datasets.forEach(function (dataset, i) {
							var meta = chartInstance.controller.getDatasetMeta(i);
							meta.data.forEach(function (bar, index) {
								var data = dataset.data[index];
								ctx.fillText(data, bar._model.x + 5, bar._model.y - 5);
							});
						});
					}
				}
			}

			var barChart = new Chart(barChartCanvas, {
				type: 'line', 
				data: barChartData,
				options: barChartOptions
			});
		}
	});


	$.ajax({
		url: "<?php echo base_url('dashboard/chart_resume_benefit/'.$period.'/'.$branch_id.'/'.$business_id); ?>",
		dataType: "json",
		success: function(data) {
			var donutData        = {
				labels: data.labels,
				datasets: [{ 
					data: data.total,
					backgroundColor : data.color,
				}]
			}

			var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
			var pieData        = donutData;
			var pieOptions     = {
				maintainAspectRatio : false,
				responsive : true, 
				legend: {
					position: 'left'
				}
			}

			new Chart(pieChartCanvas, {
				type: 'pie',
				data: pieData,
				options: pieOptions
			});
		}
	});

	$('#reservationdate').datetimepicker({
		format: 'YYYY-MM'
	});
});
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6"><h1><?php echo $_TITLE_ ?></h1></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $_TITLE_ ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card card-primary card-outline col-12">
						<div class="card-body">
							<div class="row">
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box">
										<span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan Direct</span>
											<a href="<?php echo base_url('employee_contract/list/direct'); ?>">
												<span class="info-box-number"><?php echo $total_direct; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan Indirect</span>
											<a href="<?php echo base_url('employee_contract/list/indirect'); ?>">
												<span class="info-box-number"><?php echo $total_indirect; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box">
										<span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Pengajuan Direct</span>
											<a href="<?php echo base_url('employee/submission/direct'); ?>">
												<span class="info-box-number"><?php echo $approval_direct; ?>&nbsp;<small>Kandidat</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Pengajuan Indirect</span>
											<a href="<?php echo base_url('employee/submission/indirect'); ?>">
												<span class="info-box-number"><?php echo $approval_indirect; ?>&nbsp;<small>Kandidat</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-danger elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Pengajuan Resign</span>
											<a href="<?php echo base_url('resign/approval/direct'); ?>">
												<span class="info-box-number"><?php echo $total_resign; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">
									<div class="info-box mb-3">
										<span class="info-box-icon bg-danger elevation-1"><i class="fas fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Karyawan Nonjob</span>
											<a href="<?php echo base_url('nonjob/list'); ?>">
												<span class="info-box-number"><?php echo $total_nonjob; ?>&nbsp;<small>Karyawan</small></span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card card-danger card-outline col-12">
						<div class="card-header-">
							<h3 class="card-title"><strong>Daftar Kontrak Habis</strong></h3>
						</div>
						<div class="card-body">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Keterangan</th>
										<th>Detail</th>
										<th>Total</th>
										<th width="80px">#</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$no = 1;
									foreach ($list_expired as $key => $item): 
										$total_summary = 0;
									?>
									<tr>
										<td><?php echo $no; ?></td>
										<td><?php echo $key; ?></td>
										<td>
											<table class="table table-bordered">
												<tbody>
													<tr>
														<th style="width:10%">Bisnis Unit</th>
														<th style="width:60%">Nama Site</th>
														<th style="width:10%">Branch</th>
														<th style="width:20%">Jumlah</th>
													</tr>
													<?php foreach ($item['summary'] as $detail): 
														$total_summary += $detail->summary; ?>
													<tr>
														<td><?php echo $detail->company_code; ?></td>
														<td><?php echo $detail->site_name; ?></td>
														<td><?php echo $detail->branch_name; ?></td>
														<td><?php echo $detail->summary." Karyawan"; ?></td>
													</tr>
													<?php endforeach; ?>
												</tbody>
											</table>
										</td>
										<td>
											<?php echo $total_summary." Karyawan"; ?>
										</td>
										<td>
											<?php
											$key = $item['day'];
											if($key === 0):
												$key = 'expired';
											endif;
											?>
											<a class='btn-sm btn-info btn-action'  style='cursor:pointer;' href='<?php echo base_url("employee_contract/list/direct/".$key); ?>'><i class="fas fa-file-alt text-white"></i></a>
										</td>
									</tr>
									<?php 
									$no++;
									endforeach; 
									?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"><strong>Daftar Payroll Periode Terakhir</strong></h3>
						</div>
						<div class="card-body">
							<table id="data-table" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Unit Bisnis</th>
										<th>Nama Site</th>
										<th>Periode</th>
										<th>Jumlah Karyawan</th>
										<th>THP</th>
										<th>BPJS TK</th>
										<th>Asuransi KS</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("dashboard/resume_salary/");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'company_code', 'orderable':false, },
				{ data: 'site_name', 'orderable':false },
				{ data: 'periode', 'orderable':false, 'searchable':false },
				{ data: 'employee', 'orderable':false, 'searchable':false },
				{ data: 'payroll', 'orderable':false, 'searchable':false },
				{ data: 'benefit_labor', 'orderable':false, 'searchable':false },
				{ data: 'benefit_health', 'orderable':false, 'searchable':false },
			],
			'order': [[1, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<3){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
	});
</script>
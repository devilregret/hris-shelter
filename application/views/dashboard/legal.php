<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6"><h1><?php echo $_TITLE_ ?></h1></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $_TITLE_ ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"><strong>MOU sedang dalam proses</strong></h3>
						</div>
						<div class="card-body">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Nama Perusahaan</th>
										<th>Bendera</th>
										<th>Wilayah</th>
										<th>Lokasi Site</th>
										<th>Awal Kontrak</th>
										<th>Akhir Kontrak</th>
										<th>CRM</th>
										<th>Status Pendaftaran</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$no = 1;
									foreach ($list_mou as $item): 
									?>
									<tr>
										<td><?php echo $no; ?></td>
										<td><?php echo $item->company_name; ?></td>
										<td><?php echo $item->company_now; ?></td>
										<td><?php echo $item->province_name; ?></td>
										<td><?php echo $item->city_name; ?></td>
										<td><?php echo $item->contract_start; ?></td>
										<td><?php echo $item->contract_end; ?></td>
										<td><?php echo $item->crm; ?></td>
										<td><?php echo $item->status_registration; ?></td>			
									</tr>
									<?php 
									$no++;
									endforeach; 
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
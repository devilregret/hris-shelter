<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('client_report/payroll_regular'); ?>">Payroll Regular</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Laporan Payroll Karyawan Regular</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-2">
							<a href="#" class="btn btn-success export btn-block"><i class="fas fa-file-export"></i> Export PDF</a>
						</div>
						<div class="col-sm-10">
							&nbsp;
						</div>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
							<?php
								if($resume){ 
								$start_date = substr($resume->periode_start, 8, 2).' '.get_month(substr($resume->periode_start, 5, 2));
								$end_date 	= substr($resume->periode_end, 8, 2).' '.get_month(substr($resume->periode_end, 5, 2)).' '.substr($resume->periode_end, 0, 4);
							?>
							<tr>
								<td width="250px"><strong>Periode Terakhir</strong></td>
								<td><strong>: <?php echo $start_date; ?> - <?php echo $end_date; ?></strong></td>
							</tr>
							<tr>
								<td width="250px"><strong>Total Karyawan Aktif</strong></td>
								<td><strong>: <?php echo $employee; ?> Karyawan</strong></td>
							</tr>
							<tr>
								<td width="250px"><strong>Total Karyawan digaji</strong></td>
								<td><strong>: <?php echo $resume->employee_payroll; ?> Karyawan</strong></td>
							</tr>
							<tr>
								<td width="250px"><strong>Total Payroll </strong></td>
								<td><strong>: <?php echo format_rupiah($resume->summary_payroll); ?></strong></td>
							</tr>
							<?php } ?>
						</thead>
					</table>
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="50px">No</th>
									<th>Nama</th>
									<th>Posisi</th>
									<th>Kehadiran</th>
									<th>Lembur</th>
									<th>Gaji Pokok</th>
									<th>THP</th>
									<!-- <th width="105px">#</th> -->
								</tr>
							</thead>
							<tfoot>
								<?php 
								$i = 1;
								foreach ($payroll as $item):
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $item->employee_name; ?></td>
									<td><?php echo $item->position; ?></td>
									<td><?php echo $item->attendance; ?></td>
									<td><?php echo $item->overtime_hour; ?></td>
									<td><?php echo rupiah_round((int) $item->basic_salary); ?></td>
									<td><?php echo rupiah_round((int) $item->salary); ?></td>
									<!-- <td><a onclick="preview(this)" class="btn-sm btn-info btn-action"  style="cursor:pointer;" data-href="<?php echo base_url("ropayroll/preview/".$item->id); ?>"><i class="fas fa-eye text-white"></i></a></td> -->
								</tr>
								<?php
								$i++;
								endforeach;
								?>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$( ".export" ).click(function() {
			var url = "<?php echo base_url("report/payroll_regular_export/".$resume->periode_end."/".$resume->periode_start); ?>";
			window.location = url;
		});
	});
</script>
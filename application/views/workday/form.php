<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('workday/site/'.$site_id); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Site Bisnis</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("workday/form"); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label"> Hari Kerja<span class="text-danger"> *</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="hidden" class="form-control" name="site_id" value="<?php echo $site_id; ?>">
									<select class="form-control select2" name="day" data-placeholder="Pilih Hari Kerja" required>
										<option value='Minggu' <?php if("Minggu"==$day): echo "selected"; endif;?>>Minggu</option>
										<option value='Senin' <?php if("Senin"==$day): echo "selected"; endif;?>>Senin</option>
										<option value='Selasa' <?php if("Selasa"==$day): echo "selected"; endif;?>>Selasa</option>
										<option value='Rabu' <?php if("Rabu"==$day): echo "selected"; endif;?>>Rabu</option>
										<option value='Kamis' <?php if("Kamis"==$day): echo "selected"; endif;?>>Kamis</option>
										<option value='Jumat' <?php if("Jumat"==$day): echo "selected"; endif;?>>Jumat</option>
										<option value='Sabtu' <?php if("Sabtu"==$day): echo "selected"; endif;?>>Sabtu</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Shift <span class="text-danger"> *</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="shift" required>
										<option value='Shift 1' <?php if("Shift 1"==$shift): echo "selected"; endif;?>>Shift 1</option>
										<option value='Shift 2' <?php if("Shift 2"==$shift): echo "selected"; endif;?>>Shift 2</option>
										<option value='Shift 3' <?php if("Shift 3"==$shift): echo "selected"; endif;?>>Shift 3</option>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Jam Masuk<span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<div class="input-group date" id="hours_start" data-target-input="nearest">
										<input type="text" class="form-control datetimepicker-input" data-target="#hours_start" data-toggle="datetimepicker" name='hours_start' value="<?php echo $hours_start; ?>" required/>
										<div class="input-group-append" data-target="#hours_start" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="fa fa-calendar"></i></div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									&nbsp;
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Jam Pulang <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<div class="input-group date" id="hours_end" data-target-input="nearest">
										<input type="text" class="form-control datetimepicker-input" data-target="#hours_end" name='hours_end' value="<?php echo $hours_end; ?>" required/>
										<div class="input-group-append" data-target="#hours_end" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="fa fa-calendar"></i></div>
										</div>
									</div>
								</div>
								<div class="col-sm-7">
									&nbsp;
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#hours_start').datetimepicker({
			format: 'HH:mm',
			pickDate: false,
			pickSeconds: false,
			pick12HourFormat: false  
		});
		$('#hours_end').datetimepicker({
			format: 'HH:mm',
			pickDate: false,
			pickSeconds: false,
			pick12HourFormat: false  
		});
	});
</script>
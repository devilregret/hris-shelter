<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('payroll_employee/list'); ?>">Daftar Karyawan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-list"></i>&nbsp;<?php echo $employee->full_name; ?></h3>
				</div>
				<div class="card-body">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4" action="<?php echo base_url("payroll_employee/resign"); ?>" method="POST" enctype="multipart/form-data">
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">No Induk Karyawan </label>
								<div class="col-sm-10">
									:  <strong><?php echo $employee->employee_number; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Nama Lengkap </label>
								<div class="col-sm-10">
									: <strong><?php echo $employee->full_name; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Nomor KTP </label>
								<div class="col-sm-10">
									: <strong><?php echo $employee->id_card; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Tanggal Lahir </label>
								<div class="col-sm-10">
									: <strong><?php echo $employee->date_birth; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Alamat Asal </label>
								<div class="col-sm-10">
									: <strong><?php echo $employee->address_card; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Pengajuan Resign </label>
								<div class="col-sm-10">
									<input type="hidden" class="form-control" name="id_card" value="<?php echo $employee->id_card; ?>">
									<input type="hidden" class="form-control" name="employee_id" value="<?php echo $employee->id; ?>">
									<input type="file" class="custom-file-input upload-photo" name="document_resign" accept="image/png, image/jpeg, image/jpg, .pdf, .doc, .docx">
									<label class="custom-file-label" for="customFile" >Pilih file</label>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Catatan Resign </label>
								<div class="col-sm-10">
									<textarea class="form-control" name="note" rows="3" placeholder="Alasan Resign"></textarea>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
	$('.custom-file-input').on('change',function(){
		var fileName = $(this).val();
		$(this).next('.custom-file-label').html(fileName);
	})
</script>
<style type="text/css">
	input[type="text" i], input[type="email" i], input[type="select" i], input[type="date" i], .select2-container, textarea {
    	margin-left: -7px;
	}
</style>
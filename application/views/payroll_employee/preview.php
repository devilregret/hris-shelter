<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>

	</head>
	<body>
		<section class="content">
			<div class="container-fluid">
				<div class="card card-primary card-outline col-12 mt-2">
					<div class="card-header">
						<h3 class="card-title"><i class="fas fa-eye"></i>&nbsp;Preview Karyawan</h3>
					</div>
					<div class="row">
						<div class="col-12 table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<td colspan="3"><strong>A. DATA DIRI</strong></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Id</td>
										<td>: <?php echo $preview->id; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">No Pendaftaran</td>
										<td>: <?php echo $preview->registration_number; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">No Induk Karyawan</td>
										<td>: <?php echo $preview->employee_number; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Nama Lengkap</td>
										<td>: <?php echo $preview->full_name; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Nomor KTP</td>
										<td>: <?php echo $preview->id_card; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Masa Berlaku KTP</td>
										<td>: <?php echo $preview->id_card_period; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Tempat Lahir</td>
										<td>: <?php echo $preview->city_birth; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Tanggal Lahir</td>
										<td>: <?php echo $preview->date_birth; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Kota Asal</td>
										<td>: <?php echo $preview->city_card; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Alamat Asal</td>
										<td>: <?php echo $preview->address_card; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Kota Domisili</td>
										<td>: <?php echo $preview->city_domisili; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Alamat Domisili</td>
										<td>: <?php echo $preview->address_domisili; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Nomor Telepon/HP</td>
										<td>: <?php echo $preview->phone_number; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Email</td>
										<td>: <?php echo $preview->email; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td colspan="2">Sosial Media</td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td colspan="2">
										<table class="table table-bordered">
											<tr>
												<td>Facebook</td>
												<td>Instagram</td>
												<td>Twitter</td>
											</tr>
											<tr>
												<td><?php echo $preview->socmed_fb; ?></td>
												<td><?php echo $preview->socmed_ig; ?></td>
												<td><?php echo $preview->socmed_tw; ?></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Agama</td>
										<td>: <?php echo $preview->religion; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Status Rumah</td>
										<td>: <?php echo $preview->status_residance; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Status Pernikahan</td>
										<td>: <?php echo $preview->marital_status; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Jenis Kelamin</td>
										<td>: <?php echo $preview->gender; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Tinggi & Berat Badan</td>
										<td>: <?php echo $preview->heigh.' CM / '.$preview->weigh." KG"; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Pendidikan Terakhir</td>
										<td>: <?php echo $preview->education_level; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Jurusan</td>
										<td>: <?php echo $preview->education_majors; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td colspan="2">Kontak Darurat</td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td colspan="2">
										<table class="table table-bordered">
											<tr>
												<th>Nama Kontak</th>
												<th>Hubungan</th>
												<th>Nomor Telepon</th>
												<th>Alamat</th>
											</tr>
											<?php 
											foreach ($list_emergency as $item): 
											?>
											<tr>
												<td><?php echo $item->name; ?></td>
												<td><?php echo $item->relation; ?></td>
												<td><?php echo $item->phone; ?></td>
												<td><?php echo $item->address; ?></td>
											</tr>
											<?php 
											endforeach;
											?>
										</table>
										</td>
									</tr>
									<tr>
										<td colspan="3"><strong>B. PENEMPATAN</strong></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Site Bisnis</td>
										<td>: <?php echo $preview->site_name; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Branch</td>
										<td>: <?php echo $preview->branch_name; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Hasil Interview</td>
										<td>:&nbsp; 
											<?php if($preview->document_interview !== ''): ?>
												<a href="<?php echo $preview->document_interview; ?>" target="_blank">Download</a>
											<?php else: ?> 
												Belum upload
											<?php endif; ?> 
										</td>
									</tr>
									<tr>
										<td colspan="3"><strong>C. ANGGOTA KELUARGA</strong></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Nama Ibu</td>
										<td>: <?php echo $preview->family_mother; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Nama Pasangan</td>
										<td>: <?php echo $preview->family_mate; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Nama Anak</td>
										<td>: <?php echo $preview->family_child; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Kode PTKP</td>
										<td>: <?php echo $preview->ptkp; ?></td>
									</tr>
									<tr>
										<td colspan="3"><strong>D. BENEFIT</strong></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">BPJS TK</td>
										<td>: <?php echo $preview->benefit_labor; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Asuransi Kesehatan</td>
										<td>: <?php echo $preview->benefit_health; ?></td>
									</tr>
									<tr>
										<td colspan="3"><strong>E. RIWAYAT PENDIDIKAN</strong></td>
									</tr>
									<tr>
										<td colspan="3">
										<table class="table table-bordered">
											<tr>
												<th>Nama Sekolah/Universitas</th>
												<th>Mulai</th>
												<th>Selesai</th>
											</tr>
											<?php 
											foreach ($list_education as $item): 
											?>
											<tr>
												<td><?php echo $item->institute; ?></td>
												<td><?php echo $item->start; ?></td>
												<td><?php echo $item->end; ?></td>
											</tr>
											<?php 
											endforeach;
											?>
										</table>
										</td>
									</tr>
									<tr>
										<td colspan="3"><strong>F. KEMAMPUAN BAHASA</strong></td>
									</tr>
									<tr>
										<td colspan="3">
										<table class="table table-bordered">
											<tr>
												<th>Bahasa</th>
												<th>Lisan</th>
												<th>Tulisan</th>
											</tr>
											<?php 
											foreach ($list_language as $item): 
											?>
											<tr>
												<td><?php echo $item->language; ?></td>
												<td><?php echo $item->verbal; ?></td>
												<td><?php echo $item->write; ?></td>
											</tr>
											<?php 
											endforeach;
											?>
										</table>
										</td>
									</tr>
									<tr>
										<td colspan="3"><strong>G. KEAHLIAN</strong></td>
									</tr>
									<tr>
										<td colspan="3">
										<table class="table table-bordered">
											<tr>
												<th>Pelatihan</th>
												<th>Nomor Sertifikat</th>
											</tr>
											<?php 
											foreach ($list_skill as $item): 
											?>
											<tr>
												<td><?php echo $item->certificate_name; ?></td>
												<td><?php echo $item->certificate_number; ?></td>
											</tr>
											<?php 
											endforeach;
											?>
										</table>
										</td>
									</tr>
									<tr>
										<td colspan="3"><strong>H. RIWAYAT KESEHATAN</strong></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Sakit Ringan yang sering dialami</td>
										<td>: <?php echo $preview->illness_minor; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Sebutkan Sakit Serius yang pernah dialami</td>
										<td>: <?php echo $preview->illness_serious; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Perawatan yang pernah dilakukan</td>
										<td>: <?php echo $preview->illness_treated; ?></td>
									</tr>
									<tr>
										<td colspan="3"><strong>I. RIWAYAT PEKERJAAN</strong></td>
									</tr>
									<tr>
										<td colspan="3">
										<table class="table table-bordered">
											<tr>
												<th>Nama Perusahaan</th>
												<th>Posisi Terakhir</th>
												<th>Tahun</th>
												<th>Gaji</th>
												<th>Alamat Perusahaan</th>
												<th>Alasan Resign</th>
											</tr>
											<?php 
											foreach ($list_history as $item): 
											?>
											<tr>
												<td><?php echo $item->company_name; ?></td>
												<td><?php echo $item->position; ?></td>
												<td><?php echo $item->period; ?></td>
												<td><?php echo $item->salary; ?></td>
												<td><?php echo $item->company_address; ?></td>
												<td><?php echo $item->resign_note; ?></td>
											</tr>
											<?php 
											endforeach;
											?>
										</table>
										</td>
									</tr>
									<tr>
										<td colspan="3"><strong>J. REFERENSI</strong></td>
									</tr>
									<tr>
										<td colspan="3">
										<table class="table table-bordered">
											<tr>
												<th>Nama</th>
												<th>Nama Perusahaan</th>
												<th>Posisi</th>
												<th>Nomor Telepon</th>
											</tr><?php 
											foreach ($list_reference as $item): 
											?>
											<tr>
												<td><?php echo $item->name; ?></td>
												<td><?php echo $item->company_name; ?></td>
												<td><?php echo $item->position; ?></td>
												<td><?php echo $item->phone; ?></td>
											</tr>
											<?php 
											endforeach;
											?>
										</table>
										</td>
									</tr>
									<tr>
										<td colspan="3"><strong>K. INFORMASI TAMBAHAN</strong></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Pernah berhubungan dengan pihak berwajib?</td>
										<td>: <?php echo $preview->crime_notes; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">No Rekening Mandiri</td>
										<td>: <?php echo $preview->bank_account; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">No Rekening Atas Nama</td>
										<td>: <?php echo $preview->bank_account_name; ?></td>
									</tr>
									<?php if($preview->bank_account == ''):?>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Bersedia Membuat Rekening Yangditunujuk perusahaan ?</td>
										<td>: <?php echo $preview->bank_account_create; ?></td>
									</tr>
									<?php endif; ?>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">NPWP</td>
										<td>: <?php echo $preview->tax_number; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Memiliki BPJS Kesehatan?</td>
										<td>: <?php echo $preview->health_insurance; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Keterangan BPJS Kesehatan?</td>
										<td>: <?php echo $preview->health_insurance_note; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Bersedia Mengikuti BPJS Kesehatan dari perusahaan?</td>
										<td>: <?php echo $preview->health_insurance_company; ?></td>
									</tr>
									<tr>
										<td colspan="3"><strong>L. KELENGKAPAN DOKUMEN</strong></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Foto</td>
										<td> 
											<?php if($preview->document_photo !== ''): ?>
											<img id="preview" src="<?php echo $preview->document_photo; ?>" width="200px" alt="Foto" />
											<?php endif; ?>
										</td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Foto KTP</td>
										<td>:&nbsp; 
											<?php if($preview->document_id_card !== ''): ?>
												<a href="<?php echo $preview->document_id_card; ?>" target="_blank">Download</a>
											<?php else: ?> 
												Belum upload
											<?php endif; ?> 
										</td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Foto Kartu Keluarga</td>
										<td>:&nbsp; 
											<?php if($preview->document_family_card !== ''): ?>
												<a href="<?php echo $preview->document_family_card; ?>" target="_blank">Download</a>
											<?php else: ?> 
												Belum upload
											<?php endif; ?> 
										</td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Surat Keterangan Sehat</td>
										<td>:&nbsp; 
											<?php if($preview->document_doctor_note !== ''): ?>
												<a href="<?php echo $preview->document_doctor_note; ?>" target="_blank">Download</a>
											<?php else: ?> 
												Belum upload
											<?php endif; ?> 
										</td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Foto Buku Nikah</td>
										<td>:&nbsp; 
											<?php if($preview->document_marriage_certificate !== ''): ?>
												<a href="<?php echo $preview->document_marriage_certificate; ?>" target="_blank">Download</a>
											<?php else: ?> 
												Belum upload
											<?php endif; ?> 
										</td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">SKCK</td>
										<td>:&nbsp; 
											<?php if($preview->document_police_certificate !== ''): ?>
												<a href="<?php echo $preview->document_police_certificate; ?>" target="_blank">Download</a>
											<?php else: ?> 
												Belum upload
											<?php endif; ?> 
										</td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">NPWP</td>
										<td>:&nbsp; 
											<?php if($preview->document_tax_number !== ''): ?>
												<a href="<?php echo $preview->document_tax_number; ?>" target="_blank">Download</a>
											<?php else: ?> 
												Belum upload
											<?php endif; ?> 
										</td>
									</tr>
									<tr>
										<td colspan="3"><strong>M. LAIN LAIN</strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Dibuat oleh</td>
										<td>: <?php echo $preview->creator; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Tanggal dibuat</td>
										<td>: <?php echo $preview->created_at; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Diubah oleh</td>
										<td>: <?php echo $preview->updater; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Tanggal diubah</td>
										<td>: <?php echo $preview->updated_at; ?></td>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="window.close();">Tutup</button>
       	</div>
	</body>
</html>
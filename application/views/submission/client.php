<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('candidate/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Pengajuan Ke Client</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-5">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="note" hidden="true" placeholder="Catatan">
						</div>
						<div class="col-sm-2 text-right">
							<a href="#" class="btn btn-info approve" hidden="true" ><i class="fas fa-check"></i> Lanjutkan</a>
							<a href="#" class="btn btn-danger cancel" hidden="true"><i class="fas fa-window-close"></i> Batalkan</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="POST" id="approval" action="<?php echo base_url('submission/approval/client'); ?>">
					<input type="hidden" class="form-control action" name="action" required="">
					<input type="hidden" class="form-control note" name="note" required="">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th width="50px">No</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='id_card'>KTP</th>
									<th id='age'>Usia</th>
									<th id='education_level'>Pendidikan</th>
									<th id='education_majors'>Jurusan</th>
									<th id='city_domisili'>Domisili</th>
									<th id='certificate'>Sertifikat</th>
									<th id='site_name'>Penempatan</th>
									<th id='company_code'>Bisnis Unit</th>
									<th id='position'>Jabatan</th>
									<th id='placement_date'>Tanggal Penempatan</th>
									<th id='document'>Kelengkapan Dokumen</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("submission/client_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'full_name' },
				{ data: 'id_card' },
				{ data: 'age' },
				{ data: 'education_level' },
				{ data: 'education_majors' },
				{ data: 'city_domisili' },
				{ data: 'certificate' },
				{ data: 'site_name' },
				{ data: 'company_code' },
				{ data: 'position' },
				{ data: 'placement_date' },
				{ data: 'document', 'orderable':false, 'searchable':false },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[2, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>1 && index<13){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$( ".export" ).click(function() {
			var url = "<?php echo base_url('submission/export'); ?>" + "?full_name="+$("#s_full_name").val()+"&id_card="+$("#s_id_card").val()+"&age="+$("#s_age").val()+"&education_level="+$("#s_education_level").val()+"&education_majors="+$("#s_education_majors").val()+"&city_domisili="+$("#s_city_domisili").val()+"&site_name="+$("#s_site_name").val()+"&certificate="+$("#s_certificate").val()+"&company_code="+$("#s_company_code").val()+"&position="+$("#s_position").val()+"&status=pengajuan_ke_client";
			window.location = url;
		
		});

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".cancel").prop('hidden', true);
			$(".approve").prop('hidden', true);
			$("#note").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".cancel").prop('hidden', false);
				$(".approve").prop('hidden', false);
				$("#note").prop('hidden', false);
			});
		});
		
		$("input:checkbox").change(function() {
			$(".cancel").prop('hidden', true);
			$(".approve").prop('hidden', true);
			$("#note").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".cancel").prop('hidden', false);
				$(".approve").prop('hidden', false);
				$("#note").prop('hidden', false);
			});
		});

		$( ".cancel" ).click(function() {
			$( ".action").val("reject");
			$( ".note").val($("#note").val());
			$( "#approval" ).submit();
		});

		$( ".approve" ).click(function() {
			$( ".action").val("approve");
			$( ".note").val($("#note").val());
			$( "#approval" ).submit();
		});

	});
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('benefit_employee/list'); ?>">Karyawan Lengkap</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-id-card"></i>&nbsp;Daftar Karyawan Lengkap</h3>
				</div>
				<div class="card-body">
					<form method="GET" action="<?php echo base_url('benefit_employee/list'); ?>">
						<div class="row">
							<label for="name" class="col-sm-2 col-form-label">Lihat berdasarkan :</label>
							<div class="col-sm-2" >
								<select class="form-control select2 filter" name="filter">
									<option value="" >-- Pilih Pencarian --</option>
									<option value="site" <?php if($filter == 'site'): echo 'selected'; endif;?>>Site Bisnis</option>
									<option value="nppks" <?php if($filter == 'nppks'): echo 'selected'; endif;?>>NPP Kesehatan</option>
									<option value="npptk" <?php if($filter == 'npptk'): echo 'selected'; endif;?>>NPP Ketenagakerjaan</option>
								</select>
							</div>
							<div class="col-sm-6" >
								<div class="div-nppks" <?php if($filter != 'nppks'): echo 'hidden'; endif; ?> >
									<select class="form-control select2 nppks" name="nppks" <?php if($filter != 'nppks'): echo 'disable'; endif; ?>>
										<option value=""> &nbsp; </option>
										<?php foreach ($list_nppks as $item) :  ?>
											<option value="<?php echo $item->benefit_health_company;?>" <?php if($item->benefit_health_company == $nppks): echo 'selected'; endif;?>><?php echo
											$item->benefit_health_company; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="div-npptk" <?php if($filter != 'npptk'): echo 'hidden'; endif; ?>>
									<select class="form-control select2 npptk" name="npptk" <?php if($filter != 'npptk'): echo 'disable'; endif; ?> >
										<option value=""> &nbsp; </option>
										<?php foreach ($list_npptk as $item) :  ?>
											<option value="<?php echo $item->benefit_labor_company;?>" <?php if($item->benefit_labor_company == $npptk): echo 'selected'; endif;?>><?php echo
											$item->benefit_labor_company; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="div-site" <?php if($filter != 'site'): echo 'hidden'; endif; ?>>
									<select class="form-control select2 site" name="site" <?php if($filter != 'site'): echo 'disable'; endif; ?>>
										<option value=""> &nbsp; </option>
										<?php foreach ($list_site as $item) :  ?>
											<option value="<?php echo $item->id;?>" <?php if($item->id == $site_id): echo 'selected'; endif;?>><?php echo
											$item->name.' - '.$item->company_code; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="col-sm-2" >
								<button type="submit" class="btn btn-primary btn-block">Cari</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-4">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
						<div class="col-sm-8">
							<form method="post" action="<?php echo base_url("benefit_employee/import"); ?>" enctype="multipart/form-data">
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input btn-sm" name="file" accept=".xlsx" required> 
										<label class="custom-file-label" for="exampleInputFile">Update Data</label>
									</div>
									<div class="input-group-append">
										<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>   
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
				<div class="card-body">
					<div class="alert alert-info alert-dismissible">
						<h5><i class="icon fas fa-info"></i> Informasi</h5>
						<ul>
							<li>gunakan data export untuk update data</li>
							<li>update data hanya bisa digunakan untuk update Nama Asuransi Kesehatan,nomor Asuransi Kesehatan, Program BPJS Ketenagakerjaan dan nomor BPJS Ketenagakerjaan karyawan</li>
							<li>program BPJS ketenaga kerjaan dipisah dengan tanda baca koma (,) contoh : JKK, JKM, JHT, JP </li>
						</ul> 
					</div>
					<?php if($site_id): ?>
					<p>&nbsp;</p>
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
							<tr>
								<td width="250px">Jumlah Karyawan</td>									
								<td>: <?php echo $employee.' Karyawan'; ?></td>
							</tr>
						</thead>
					</table>
					<?php endif; ?>
					<p>&nbsp;</p>
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="50px">No</th>
									<th id='company_name'>Unit Kontrak</th>
									<th id='site_name'>Site Bisnis</th>
									<th id='employee_number'>ID Karyawan</th>
									<th id='id_card'>NIK KTP</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='benefit_labor_note'>Program BPJS Ketenagakerjaan</th>
									<th id='benefit_labor_company'>NPP Ketenagakerjaan</th>
									<th id='benefit_labor'>Nomor KJP</th>
									<th id='benefit_health_note'>Asuransi Kesehatan</th>
									<th id='benefit_health_company'>NPP Kesehatan</th>
									<th id='benefit_health'>Nomor KIS</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			 'searching'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("benefit_employee/list_ajax?site=".$site_id."&nppks=".$nppks."&npptk=".$npptk);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'company_name' },
				{ data: 'site_name' },
				{ data: 'employee_number' },
				{ data: 'id_card' },
				{ data: 'full_name' },
				{ data: 'benefit_labor_note' },
				{ data: 'benefit_labor_company' },
				{ data: 'benefit_labor' },
				{ data: 'benefit_health_note' },
				{ data: 'benefit_health_company' },
				{ data: 'benefit_health' },
				{ data: 'action', 'orderable':false, 'searchable':false  }
			],
			'order': [[5, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<12){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
		$( ".export" ).click(function() {
			var url = "<?php echo base_url('benefit_employee/export/complete?&site_id='.$site_id.'&is_health_company='.$nppks.'&is_labor_company='.$npptk); ?>" + "&full_name="+$("#s_full_name").val()+"&employee_number="+$("#s_employee_number").val()+"&id_card="+$("#s_id_card").val()+"&benefit_labor_note="+$("#s_benefit_labor_note").val()+"&benefit_labor_company="+$("#s_benefit_labor_company").val()+"&benefit_labor="+$("#s_benefit_labor").val()+"&benefit_health_note="+$("#s_benefit_health_note").val()+"&benefit_health_company="+$("#s_benefit_health_company").val()+"&benefit_health="+$("#s_benefit_health").val()+"&company_name="+$("#s_company_name").val()+"&site_name="+$("#s_site_name").val();
			window.location = url;
		
		});

		$('.filter').on('change', function() {
			if(this.value == 'site'){
				$('.nppks').attr('disabled', 'disabled');
				$('.npptk').attr('disabled', 'disabled');
				$('.site').removeAttr('disabled');

				$('.div-nppks').attr('hidden', true);
				$('.div-npptk').attr('hidden', true);
				$('.div-site').removeAttr('hidden');
			}else if(this.value == 'npptk'){
				$('.nppks').attr('disabled', 'disabled');
				$('.site').attr('disabled', 'disabled');
				$('.npptk').removeAttr('disabled');

				$('.div-site').attr('hidden', true);
				$('.div-nppks').attr('hidden', true);
				$('.div-npptk').removeAttr('hidden');
			}else if(this.value == 'nppks'){
				$('.npptk').attr('disabled', 'disabled');
				$('.site').attr('disabled', 'disabled');
				$('.nppks').removeAttr('disabled');

				$('.div-site').attr('hidden', true);
				$('.div-npptk').attr('hidden', true);
				$('.div-nppks').removeAttr('hidden');
			}
		});
	});
</script>
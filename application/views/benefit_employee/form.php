<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('benefit_employee/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Karyawan</h3>
				</div>
				<div class="card-body">
					<?php if(isset($message)): echo $message; endif; ?>
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4" method="post" action="<?php echo base_url("benefit_employee/form"); ?>" enctype="multipart/form-data">
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nomor KTP</label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="hidden" class="form-control" name="site_id" value="<?php echo $site_id; ?>">
									<strong>: <?php echo $id_card; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">No Induk Karyawan </label>
								<div class="col-sm-9">
									<strong>: <?php echo $employee_number; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">preview </label>
								<div class="col-sm-9">
									<?php if($document_photo != ''): ?>
										<img id="preview" src="<?php echo $document_photo; ?>" width="200px" alt="Foto" />
									<?php else: ?>
										<img id="preview" src="<?php echo asset_img("candidate.png"); ?>" width="200px" alt="Foto" />
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Lengkap <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<strong>: <?php echo $full_name; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tanggal Lahir <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<strong>: <?php echo $date_birth; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Alamat Asal <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<strong>: <?php echo $address_card; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Alamat Domisili <i class="text-xs">(Diisi jika berbeda dengan KTP)</i><span class="text-danger"></span></label>
								<div class="col-sm-8">
									<strong>: <?php echo $address_domisili; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nomor Telepon/HP <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<strong>: <?php echo $phone_number; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Email <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<strong>: <?php echo $email; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Jenis Kelamin <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<strong>: <?php echo $gender; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Catatan Asuransi<span class="text-danger"></span></label>
								<div class="col-sm-9">
									<strong>: <?php echo $health_insurance; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Catatan Asuransi<span class="text-danger"></span></label>
								<div class="col-sm-9">
									<strong>: <?php echo $health_insurance_note; ?></strong>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Program BPJS <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<?php $labor = explode(",", $benefit_labor_note); ?>
									<select multiple class="form-control select2" name="benefit_labor_choice[]" placeholder="Program BPJS  Ketenagakerjaan">
										<option value="">&nbsp;</option>
										<option value="JKK" <?php if(in_array('JKK', $labor)): echo "selected"; endif;?>>JKK</option>
										<option value="JKM" <?php if(in_array('JKM', $labor)): echo "selected"; endif;?>>JKM</option>
										<option value="JHT" <?php if(in_array('JHT', $labor)): echo "selected"; endif;?>>JHT</option>
										<option value="JP" <?php if(in_array('JP', $labor)): echo "selected"; endif;?>>JP</option>
										
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">BPJS Ketenagakerjaan</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nomor BPJS  Ketenagakerjaan" name="benefit_labor" value="<?php echo $benefit_labor; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">NPP Ketenagakerjaan</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="NPP  Ketenagakerjaan" name="benefit_labor_company" value="<?php echo $benefit_labor_company; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Layanan Asuransi Kesehatan </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="BPJS Kesehatan" name="benefit_health_note" value="<?php echo $benefit_health_note; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nomor Asuransi Kesehatan </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nomor Asuransi Kesehatan" name="benefit_health" value="<?php echo $benefit_health; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">NPP Asuransi Kesehatan</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="NPP Ketenagakerjaan" name="benefit_health_company" value="<?php echo $benefit_health_company; ?>">
								</div>
							</div>
							<hr>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" name="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
	$(document).ready(function () {
		$('.upload-document').imageUploadResizer({
			max_width: 1200, // Defaults 1000
			max_height: 800, // Defaults 1000
			quality: 1, // Defaults 1
			do_not_resize: ['gif', 'svg'], // Defaults []
		});
	});
</script>
<style type="text/css">
	input[type="text" i], input[type="email" i], input[type="select" i], input[type="date" i], .select2-container, textarea {
    	margin-left: -7px;
	}
</style>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('benefit_employee/notcomplete'); ?>">Karyawan Belum Lengkap</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-id-card"></i>&nbsp;Daftar Karyawan Belum Lengkap</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-4">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
						<div class="col-sm-8">
							<form method="post" action="<?php echo base_url("benefit_employee/import"); ?>" enctype="multipart/form-data">
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input btn-sm" name="file" accept=".xlsx" required> 
										<label class="custom-file-label" for="exampleInputFile">Update Data</label>
									</div>
									<div class="input-group-append">
										<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>   
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
				<div class="card-body">
					<div class="alert alert-info alert-dismissible">
						<h5><i class="icon fas fa-info"></i> Informasi</h5>
						<ul>
							<li>gunakan data export untuk update data</li>
							<li>update data hanya bisa digunakan untuk update Nama Asuransi Kesehatan,nomor Asuransi Kesehatan, Program BPJS Ketenagakerjaan dan nomor BPJS Ketenagakerjaan karyawan</li>
							<li>program BPJS ketenaga kerjaan dipisah dengan tanda baca koma (,) contoh : JKK, JKM, JHT, JP </li>
						</ul> 
					</div>
					<?php if($site_id): ?>
					<p>&nbsp;</p>
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
							<tr>
								<td width="250px">Jumlah Karyawan</td>									
								<td>: <?php echo $employee.' Karyawan'; ?></td>
							</tr>
						</thead>
					</table>
					<?php endif; ?>
					<div class="row submission" hidden="true">
						<div class="col-sm-1 ">
							<select multiple class="form-control select2" name="benefit_labor_choice[]" id="benefit_labor_note_id" placeholder="Program BPJS  Ketenagakerjaan">
								<option value="JKK">JKK</option>
								<option value="JKM">JKM</option>
								<option value="JHT">JHT</option>
								<option value="JP">JP</option>
							</select>
						</div>
						<div class="col-sm-2">
							<input type="text" class="form-control" placeholder="Nomor BPJS  Ketenagakerjaan" id="benefit_labor_id">
						</div>
						<div class="col-sm-2">
							<input type="text" class="form-control" placeholder="NPP  Ketenagakerjaan" id="benefit_labor_company_id">
						</div>
						<div class="col-sm-2">
							<input type="text" class="form-control" placeholder="BPJS Kesehatan" id="benefit_health_note_id">
						</div>
						<div class="col-sm-2">
							<input type="text" class="form-control" placeholder="Nomor Asuransi Kesehatan" id="benefit_health_id">
						</div>
						<div class="col-sm-2">
							<input type="text" class="form-control" placeholder="NPP Ketenagakerjaan" id="benefit_health_company_id">
						</div>
						<div class="col-sm-1">
							<a class="btn btn-primary save"><i class="fas fa-save"></i> Simpan</a>
						</div>
					</div>
					<p>&nbsp;</p>
					<div class="scroll-panel">
					<form method="POST" id="register" action="<?php echo base_url('benefit_employee/register'); ?>">
						<input type="text" name="benefit_labor_note" id="benefit_labor_note" value="" hidden="">
						<input type="text" name="benefit_labor" id="benefit_labor" value="" hidden="">
						<input type="text" name="benefit_labor_company" id="benefit_labor_company" value="" hidden="">
						<input type="text" name="benefit_health_note" id="benefit_health_note" value="" hidden="">
						<input type="text" name="benefit_health" id="benefit_health" value="" hidden="">
						<input type="text" name="benefit_health_company" id="benefit_health_company" value="" hidden="">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th id='company_name'>Unit Kontrak</th>
									<th id='site_name'>Site Bisnis</th>
									<th id='employee_number'>ID Karyawan</th>
									<th id='id_card'>NIK KTP</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='benefit_labor_note'>Program BPJS Ketenagakerjaan</th>
									<th id='benefit_labor_company'>NPP Ketenagakerjaan</th>
									<th id='benefit_labor'>Nomor KJP</th>
									<th id='benefit_health_note'>Asuransi Kesehatan</th>
									<th id='benefit_health_company'>NPP Kesehatan</th>
									<th id='benefit_health'>Nomor KIS</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			 'searching'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("benefit_employee/approval_ajax?site=".$site_id."&nppks=".$nppks."&npptk=".$npptk);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'company_name' },
				{ data: 'site_name' },
				{ data: 'employee_number' },
				{ data: 'id_card' },
				{ data: 'full_name' },
				{ data: 'benefit_labor_note' },
				{ data: 'benefit_labor_company' },
				{ data: 'benefit_labor' },
				{ data: 'benefit_health_note' },
				{ data: 'benefit_health_company' },
				{ data: 'benefit_health' },
				{ data: 'action', 'orderable':false, 'searchable':false  }
			],
			'order': [[5, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<12){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
		$( ".export" ).click(function() {
			var url = "<?php echo base_url('benefit_employee/export/approval?&site_id='.$site_id.'&is_health_company='.$nppks.'&is_labor_company='.$npptk); ?>" + "&full_name="+$("#s_full_name").val()+"&employee_number="+$("#s_employee_number").val()+"&id_card="+$("#s_id_card").val()+"&benefit_labor_note="+$("#s_benefit_labor_note").val()+"&benefit_labor_company="+$("#s_benefit_labor_company").val()+"&benefit_labor="+$("#s_benefit_labor").val()+"&benefit_health_note="+$("#s_benefit_health_note").val()+"&benefit_health_company="+$("#s_benefit_health_company").val()+"&benefit_health="+$("#s_benefit_health").val()+"&company_name="+$("#s_company_name").val()+"&site_name="+$("#s_site_name").val();
			window.location = url;
		
		});
		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});
		$(document).on("change", ".check-id", function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});
		$("input:checkbox").change(function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});
		$(".save" ).click(function() {
			$("#benefit_labor_note").val($("#benefit_labor_note_id").val());
			$("#benefit_labor").val($("#benefit_labor_id").val());
			$("#benefit_labor_company").val($("#benefit_labor_company_id").val());
			$("#benefit_health_note").val($("#benefit_health_note_id").val());
			$("#benefit_health").val($("#benefit_health_id").val());
			$("#benefit_health_company").val($("#benefit_health_company_id").val());
			$("#register").submit();
		});
	});
</script>
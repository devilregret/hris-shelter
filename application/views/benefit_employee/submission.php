<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('benefit_employee/submission'); ?>">Pengajuan Pendaftaran</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Pengajuan Pendaftaran</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>&nbsp;
				</div>
				<div class="card-body">
					<div class="scroll-panel" style="height:400px;">
						<table id="data-submission" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th id='company_name'>Unit Kontrak</th>
									<th id='site_name'>Site Bisnis</th>
									<th id='employee_number'>ID Karyawan</th>
									<th id='id_card'>NIK KTP</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='benefit_labor_note'>Program BPJS Ketenagakerjaan</th>
									<th id='benefit_labor_company'>NPP Ketenagakerjaan</th>
									<th id='benefit_labor'>Nomor KJP</th>
									<th id='benefit_health_note'>Asuransi Kesehatan</th>
									<th id='benefit_health_company'>NPP Kesehatan</th>
									<th id='benefit_health'>Nomor KIS</th>
									<th width="80px">#</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Karyawan</h3>
				</div>
				<div class="card-body">
					<div class="row submission" hidden="true">
						<div class="col-sm-10 ">
						</div>
						<div class="col-sm-2 text-right">
							<a class="btn btn-primary save btn-block"><i class="fas fa-save"></i> Ajukan</a>
						</div>
					</div>
					<p>&nbsp; </p>
					<div class="scroll-panel">
						<form method="POST" id="submission" action="<?php echo base_url('benefit_employee/submit'); ?>">
						<table id="data-employee" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th id='approved_at'>Tanggal Diterima</th>
									<th id='company_name'>Unit Kontrak</th>
									<th id='site_name'>Site Bisnis</th>
									<th id='employee_number'>ID Karyawan</th>
									<th id='id_card'>NIK KTP</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='benefit_labor_note'>Program BPJS Ketenagakerjaan</th>
									<th id='benefit_labor_company'>NPP Ketenagakerjaan</th>
									<th id='benefit_labor'>Nomor KJP</th>
									<th id='benefit_health_note'>Asuransi Kesehatan</th>
									<th id='benefit_health_company'>NPP Kesehatan</th>
									<th id='benefit_health'>Nomor KIS</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<style type="text/css">
	thead {
		background: white;
		position: sticky;
		top: 0;
		box-shadow: 0 2px 2px -1px rgb(0 0 0 / 40%);
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-employee').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			 'searching'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("benefit_employee/employee_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'approved_at' },
				{ data: 'company_name' },
				{ data: 'site_name' },
				{ data: 'employee_number' },
				{ data: 'id_card' },
				{ data: 'full_name' },
				{ data: 'benefit_labor_note' },
				{ data: 'benefit_labor_company' },
				{ data: 'benefit_labor' },
				{ data: 'benefit_health_note' },
				{ data: 'benefit_health_company' },
				{ data: 'benefit_health' },
				{ data: 'action', 'orderable':false, 'searchable':false  }
			],
			'order': [[1, 'desc']]
		});

		$('#data-employee tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<13){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-employee').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$('#data-submission').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			 'searching'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("benefit_employee/submission_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'company_name' },
				{ data: 'site_name' },
				{ data: 'employee_number' },
				{ data: 'id_card' },
				{ data: 'full_name' },
				{ data: 'benefit_labor_note' },
				{ data: 'benefit_labor_company' },
				{ data: 'benefit_labor' },
				{ data: 'benefit_health_note' },
				{ data: 'benefit_health_company' },
				{ data: 'benefit_health' },
				{ data: 'action', 'orderable':false, 'searchable':false  }
			],
			'order': [[4, 'asc']]
		});


		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});
		$(document).on("change", ".check-id", function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});
		$("input:checkbox").change(function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});
		$(".save" ).click(function() {
			$("#submission").submit();
		});
	});
</script>
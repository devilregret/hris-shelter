<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('resign/approval'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Pengajuan Resign</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-5">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>
						</div>
						<div class="col-sm-5 approval"  hidden="true" >
							<select class="form-control select2 " id="status_id" required>
								<option value="Proses">Proses</option>
								<option value="Closed">Closed</option>
							</select>
						</div>
						<div class="col-sm-2 text-right approval"  hidden="true" >
							<a href="#" class="btn btn-info approve" ><i class="fas fa-check"></i> Setuju</a>
							<a href="#" class="btn btn-danger cancel"><i class="fas fa-window-close"></i> Tolak</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="POST" id="approval" action="<?php echo base_url('resign/submit'); ?>">
					<input type="hidden" class="form-control action" name="action">
					<input type="hidden" class="form-control resign_status" name="resign_status" >
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th id='employee_number'>ID Karyawan</th>
									<th id='id_card'>KTP</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='site_name'>Nama Site</th>
									<th id='resign_note'>Alasan Resign</th>
									<th id='resign_burden'>Tanggungan</th>
									<th id='resign_submit'>Tanggal Resign</th>
									<th id='resign_status'>Status Pengajuan</th>
									<th id='document_resign'>Dokumen Resign</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("resign/list_approval");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'employee_number' },
				{ data: 'id_card' },
				{ data: 'full_name' },
				{ data: 'site_name' },
				{ data: 'resign_note' },
				{ data: 'resign_burden' },
				{ data: 'resign_submit' },
				{ data: 'resign_status' },
				{ data: 'document_resign' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[1, 'desc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<9){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".approval").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".approval").prop('hidden', false);
			});
		});
		
		$("input:checkbox").change(function() {
			$(".approval").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".approval").prop('hidden', false);
			});
		});

		$( ".cancel" ).click(function() {
			$( ".action").val("reject");
			$( "#approval" ).submit();
		});

		$( ".approve" ).click(function() {
			$( ".resign_status").val($("#status_id").val());
			$( ".action").val("approve");
			$( "#approval" ).submit();
		});

		$( ".export" ).click(function() {
			var url = "<?php echo base_url('resign/approval_export'); ?>" + "?employee_number="+$("#s_employee_number").val()+"&id_card="+$("#s_id_card").val()+"&full_name="+$("#s_full_name").val()+"&site_name="+$("#s_site_name").val()+"&resign_note="+$("#s_resign_note").val()+"&resign_burden="+$("#s_resign_burden").val()+"&resign_submit="+$("#s_resign_submit").val()+"&resign_status="+$("#s_resign_status").val();
			window.location = url;
		});
	});
</script>
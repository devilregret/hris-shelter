<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('resign/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Karyawan Resign</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-12">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th id="id_card">Nomor KTP</th>
									<th id='employee_number'>ID Karyawan</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='company_name'>Unit Kontrak</th>
									<th id='site_name'>Site Bisnis</th>
									<th id='position'>Jabatan</th>
									<th id='resign_submit'>Tanggal Resign</th>
									<th id='ro'>RO</th>
									<th id='note'>Alasan Resign</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("resign/list_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id_card' },
				{ data: 'employee_number' },
				{ data: 'full_name' },
				{ data: 'company_name' },
				{ data: 'site_name' },
				{ data: 'position' },
				{ data: 'resign_submit' },
				{ data: 'ro' },
				{ data: 'note' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[6, 'desc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>=0 && index<9){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$( ".export" ).click(function() {
			var url = "<?php echo base_url('resign/export'); ?>" + "?full_name="+$("#s_full_name").val()+"&employee_number="+$("#s_employee_number").val()+"&company_name="+$("#s_company_name").val()+"&company_name="+$("#s_company_name").val()+"&site_name="+$("#s_site_name").val()+"&position="+$("#s_position").val()+"&contract_end="+$("#s_resign_submit").val()+"&ro="+$("#s_ro").val()+"&note="+$("#s_note").val()+"&id_card="+$("#s_id_card").val();
			window.location = url;
		});
	});

</script>
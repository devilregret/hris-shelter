<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('agreement/list'); ?>">PKS</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Perjanjian Kerjasama</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("agreement/form"); ?>" enctype="multipart/form-data">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nama Instansi <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="text" class="form-control" placeholder="Nama Instansi" name="agency_name" value="<?php echo $agency_name; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Awal KontraK <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="date" name="start_contract" class="form-control datepicker" value="<?php echo $start_contract; ?>" required>
								</div>
								<div class="col-sm-7"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Akhir Kontrak <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="date" name="end_contract" class="form-control datepicker" value="<?php echo $end_contract; ?>" required>
								</div>
								<div class="col-sm-7"></div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nama Kota <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="city_id">
										<option value=""> -- PILIH KOTA -- </option>
										<?php foreach ($list_city as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $city_id): echo "selected"; endif;?>><?php echo $item->name.' - '.$item->province_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Dokumen Kontrak<span class="text-danger"></span></label>
								<div class="custom-file col-sm-7 file-upload">	
									<input type="file" class="custom-file-input" name="document" accept="image/jpeg, image/jpg, image/png, .pdf" ><label class="custom-file-label" style="margin-left: 7px;">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document != ''): ?>
										<a href="<?php echo $document; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									 <?php endif; ?>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>	
		</div>
	</section>
</div>
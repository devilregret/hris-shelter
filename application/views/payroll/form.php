<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('payroll/list'); ?>">Payroll Karyawan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Payroll Karyawan</h3>
				</div>
				<div class="card-header">
					&nbsp;<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4 payroll" method="post" action="<?php echo base_url("payroll/form"); ?>">
							<input type="hidden" class="form-control" name="payroll_id" value="<?php echo $payroll_id; ?>">
							<?php if($employee): ?>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nama Lengkap <span class="text-danger"></span></label>
								<div class="col-sm-7">
									<input type="text" class="form-control" readonly value="<?php echo $employee->full_name; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Alamat <span class="text-danger"></span></label> 
								<div class="col-sm-7">
									<input type="text" class="form-control" readonly value="<?php echo $employee->address_card; ?>">
								</div>
							</div>
							<?php endif; ?>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Gaji Bersih (THP) <span class="text-danger"></span></label> 
								<div class="col-sm-7">
									<input type="text" class="form-control numeric rupiah" name="salary" value="<?php echo rupiah_round($salary); ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Gaji Pokok <span class="text-danger"></span></label> 
								<div class="col-sm-7">
									<input type="text" class="form-control numeric rupiah" name="basic_salary" value="<?php echo rupiah_round($basic_salary); ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">BPJS Kesehatan Perusahaan <span class="text-danger"></span></label> 
								<div class="col-sm-7">
									<input type="text" class="form-control numeric rupiah" name="bpjs_ks_company" value="<?php echo rupiah_round($bpjs_ks_company); ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">BPJS Ketenagakerjaan Perusahaan <span class="text-danger"></span></label> 
								<div class="col-sm-7">
									<input type="text" class="form-control numeric rupiah" name="bpjs_jht_company" value="<?php echo rupiah_round($bpjs_jht_company); ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Potongan BPJS Kesehatan <span class="text-danger"></span></label> 
								<div class="col-sm-7">
									<input type="text" class="form-control numeric rupiah" name="bpjs_ks" value="<?php echo rupiah_round($bpjs_ks); ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Potongan BPJS Ketenagakerjaan <span class="text-danger"></span></label> 
								<div class="col-sm-7">
									<input type="text" class="form-control numeric rupiah" name="bpjs_jht" value="<?php echo rupiah_round($bpjs_jht); ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">PPH 21 <span class="text-danger"></span></label> 
								<div class="col-sm-7">
									<input type="text" class="form-control numeric rupiah" name="tax_calculation" value="<?php echo rupiah_round($tax_calculation); ?>">
								</div>
							</div>
							<div id="detail">
								<?php 
									foreach ($payroll_detail as $item): 
								?>
								<div class="form-group row detail_salary">
									<div class="col-sm-1">
										<a class="btn btn-danger btn-block" onclick="remove(this);"><i class="fas fa-minus"></i></a>
									</div>
									<div class="col-md-3">
										<select class="form-control select2 " name="payroll_detail_category[]">
											<option value="pendapatan" <?php if($item->category  == 'pendapatan'): echo 'selected'; endif;?>>Pendapatan</option>
											<option value="potongan" <?php if($item->category  == 'potongan'): echo 'selected'; endif;?>>Potongan</option>
										</select>
									</div>
									<div class="col-sm-3">
										<input type="text" class="form-control" placeholder="Keterangan" value="<?php echo $item->name; ?>" name="payroll_detail_name[]" required>
									</div>
									<div class="col-sm-4">
										<input type="text" class="form-control numeric rupiah" placeholder="Nominal" name="payroll_detail_value[]" value="<?php echo rupiah_round($item->value); ?>">
									</div>
								</div>
								<?php 
								endforeach;
								?>
								<div class="form-group row">
									<div class="col-md-1">
										&nbsp;
									</div>
									<div class="col-md-3">
										<select class="form-control select2 " name="payroll_detail_category[]">
											<option value="pendapatan">Pendapatan</option>
											<option value="potongan">Potongan</option>
										</select>
									</div>
									<div class="col-sm-3">
										<input type="text" class="form-control" placeholder="Keterangan" name="payroll_detail_name[]" >
									</div>
									<div class="col-sm-4">
										<input type="text" class="form-control numeric rupiah" placeholder="Nominal" name="payroll_detail_value[]">
									</div>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-3">
									<a class="btn btn-success btn-block add-detail"><i class="fas fa-plus"></i>&nbsp;Tambah</a>
								</div>
							</div>
							<hr>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$(".add-detail").click(function() {
			$(".payroll").submit();
		});
	});

	function remove(el){
		$(el).parent().parent().remove();
	}
</script>
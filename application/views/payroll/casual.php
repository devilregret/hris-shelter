<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('payroll/list'); ?>">Payroll Karyawan Casual</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-handshake"></i>&nbsp;Site Payroll</h3>
				</div>
				<div class="card-body">
					<form method="POST" action="<?php echo base_url('payroll/casual'); ?>">
						<div class="row">
							<label for="name" class="col-sm-2 col-form-label">Pilih Site :</label>
							<div class="col-sm-8" >
								<select class="form-control select2" name="site_id">
									<option value=""> &nbsp; </option>
									<?php foreach ($list_site as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id == $site_id): echo 'selected'; endif;?>><?php echo
										$item->name.' - '.$item->company_code; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-sm-2" >
								<button type="submit" class="btn btn-primary btn-block">Simpan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		<?php if($site_id): ?>
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Payroll Karyawan</h3>
				</div>
				<div class="card-body">

					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
						</thead>
					</table>
					<p>&nbsp;</p>
					<div class="card-header">
						<div class="row submission" hidden="true">
							<div class="col-sm-12 submission">
								<a href="#" class="btn btn-success process float-right" style="margin-right:10px;width:190px;"><i class="fas fa-check"></i> Proses Pembayaran</a>
								<a href="#" class="btn btn-danger cancel float-right" style="margin-right:10px;width:190px;"><i class="fas fa-window-close"></i> Tolak Pengajuan</a>
							</div>
						</div>
					</div>
					<p>&nbsp;</p>
					<div class="row">
						<div class="col-sm-3">
							<strong>Pilih data payroll yang ditampilkan :</strong>
						</div>
						<div class="col-sm-3">
							<select class="form-control preview float-right">
								<option value="<?php echo base_url("payroll/casual/menunggu");?>" <?php if($view == "menunggu" ): echo "selected"; endif; ?>>Pengajuan</option>
								<option value="<?php echo base_url("payroll/casual/selesai");?>" <?php if($view == "selesai" ): echo "selected"; endif; ?>>Selesai</option>
							</select>
						</div>
						<div class="col-sm-6">
						</div>
					<p>&nbsp;</p>
					</div>
					<div class="scroll-panel">
						<form method="POST" id="approve" action="<?php echo base_url('payroll/casual_approve/'.$view); ?>">
						<input type="text" name="status" id="status" value="" hidden="">
							<table id="data-table" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
										<th id='employee_number'>ID Karyawan</th>
										<th id='full_name'>Nama Lengkap</th>
										<th id='status_'>Status Kontrak</th>
										<th id='status_'>Status Pengajuan</th>
										<th id='periode'>Periode</th>
										<th id='total_shift'>Total Shift</th>
										<th id='basic_salary'>Gaji Pokok</th>
										<th id='total_salary'>Total Gaji</th>
										<th id='management_fee'>Management Fee</th>
										<th id='ppn'>PPN</th>
										<th id='pph'>PPH</th>
										<th id='salary'>Grand Total</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</form>
					</div>
				</div>
			</div>
		<?php endif; ?>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.preview').on('change', function() {
			window.location.href = this.value;
		});
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			 'searching'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("payroll/casual_ajax/".$view);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'employee_number' },
				{ data: 'full_name' },
				{ data: 'status_employee' },
				{ data: 'status_approval' },
				{ data: 'periode', 'orderable':false, 'searchable':false   },
				{ data: 'total_shift', 'orderable':false, 'searchable':false  , 'orderable':false, 'searchable':false   },
				{ data: 'basic_salary', 'orderable':false, 'searchable':false   },
				{ data: 'total_salary', 'orderable':false, 'searchable':false   },
				{ data: 'management_fee', 'orderable':false, 'searchable':false   },
				{ data: 'ppn', 'orderable':false, 'searchable':false   },
				{ data: 'pph', 'orderable':false, 'searchable':false   },
				{ data: 'salary', 'orderable':false, 'searchable':false   }
			],
			'order': [[2, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<4){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		});

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$("input:checkbox").change(function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});

		$(document).on("change", ".check-id", function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});

		$(".cancel" ).click(function() {
			$( "#status").val('Ditolak');
			$( "#approve" ).submit();
		});

		$(".process" ).click(function() {
			$( "#status").val('Selesai');
			$( "#approve" ).submit();
		});

		$(".save" ).click(function() {
			$( "#status").val($( "#status_id").val());
			$( "#approve" ).submit();
		});
	});
</script>
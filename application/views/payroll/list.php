<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('payroll/list'); ?>">Payroll Karyawan Regular</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-handshake"></i>&nbsp;Site Payroll</h3>
				</div>
				<div class="card-body">
					<div class="alert alert-info alert-dismissible">
						<h5><i class="icon fas fa-info"></i> Informasi</h5>
						<ul>
							<li>Pastikan masing-masing kolom ada nama kolomnya</li>
						</ul> 
					</div>
					<form method="POST" action="<?php echo base_url('payroll/site'); ?>">
						<div class="row">
							<label for="name" class="col-sm-2 col-form-label">Pilih Site :</label>
							<div class="col-sm-8" >
								<select class="form-control select2" name="site_id">
									<option value=""> &nbsp; </option>
									<?php foreach ($list_site as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id == $site_id): echo 'selected'; endif;?>><?php echo
										$item->name.' - '.$item->company_code; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-sm-2" >
								<button type="submit" class="btn btn-primary btn-block">Simpan</button>
							</div>
						</div>
					</form>
				</div>
				<?php if($site_id): ?>
				<?php
					$prev 	= date('m', strtotime("-1 months"));
					$current= date('m');
					$next 	= date('m', strtotime("1 months"));
					$year 	= date('Y');
				?>
				<div class="card-header">
					<form method="POST" action="<?php echo base_url('payroll/generate'); ?>">		
					<div class="row">
						<div class="col-sm-2">
							<strong>Hitung Payroll bulan :</strong>
						</div>
						<div class="col-sm-8">
							<select class="form-control select2" name="periode">
								<option value="<?php echo $prev; ?>" <?php if($view == $prev ): echo "selected"; endif; ?>>
									<?php echo get_month($prev).' '.$year; ?>
								</option>
								<option value="<?php echo $current; ?>" <?php if($view == $current ): echo "selected"; endif; ?>>
									<?php echo get_month($current).' '.$year; ?>
								</option>
								<option value="<?php echo $next; ?>" <?php if($view == $next ): echo "selected"; endif; ?>>
									<?php echo get_month($next).' '.$year; ?>
								</option>
							</select>
						</div>
						<div class="col-sm-2">
							<button type="submit" class="btn btn-success btn-block">Proses</button>
						</div>
					</div>
					</form>
				</div>
			<?php endif; ?>
			</div>
		<?php if($site_id): ?>
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-cogs"></i>&nbsp;Pengaturan Import Payroll</h3>
				</div>
				<div class="card-body">
					<form method="post" action="<?php echo base_url("payroll/import"); ?>" enctype="multipart/form-data">
						<div class="card card-danger card-outline">					
							<div class="input-group col-md-12 mt-2">
								<label for="name" class="col-md-3 col-form-label">Silahkan Pilih Template yang akan di gunakan :<br> &nbsp;</label>
								<div class="col-md-2">
									<select class="form-control select2 config-template">
										<option value="new">TEMPLATE BARU</option>
										<?php foreach ($list_config as $item) :  ?>
											<option value="<?php echo $item->id;?>" <?php if($item->id == $site->config_id): echo "selected"; endif;?>>
												<?php echo $item->config_name; ?>
											</option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-md-4">
									<input type="hidden" class="form-control" name="config_id" value="<?php echo $site->config_id; ?>">
									<input type="text" class="form-control" name="config_name" value="<?php echo $site->config_name; ?>" required>
								</div>
								<div class="col-sm-3">
									<a onclick="confirm_del(this)" class="btn btn-danger btn" style="width: 100px; cursor:pointer;" data-href="<?php echo base_url("payroll/delete_config/".$site->config_id); ?>"> HAPUS &nbsp;</a>
								</div>
							</div>
						</div>
						<div class="card card-warning card-outline">
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-2 col-form-label">Nomor Baris Awal Payroll <span class="text-danger">*</span></label>
								<div class="col-sm-1">
									<input type="hidden" class="form-control" name="site_id" value="<?php echo $site->site_id?>">
									<input type="text" class="form-control line" name="row_start" value="<?php echo $site->row_start; ?>" required>
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Nomor Baris Akhir Payroll <span class="text-danger">*</span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control line" name="row_end" value="<?php echo $site->row_end; ?>" required>
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Nomor Baris Nama Rincian <span class="text-danger">*</span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control line" name="row_title" value="<?php echo $site->row_title; ?>" required>
								</div>
							</div>
							<br>
						</div>
						<div class="card card-warning card-outline">
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Jumlah Hari Kerja<span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_attendance" value="<?php echo $site->column_attendance; ?>" style="text-transform:uppercase">
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Jam Lembur <span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_overtime_hour" value="<?php echo $site->column_overtime_hour; ?>" style="text-transform:uppercase">
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Perhitungan Lembur <span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_overtime_calculation" value="<?php echo $site->column_overtime_calculation; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Gaji Pokok <span class="text-danger">*</span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_basic_salary" value="<?php echo $site->column_basic_salary; ?>" style="text-transform:uppercase" required>
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Pendapatan Lain-Lain <span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control comma" name="column_income" value="<?php echo $site->column_income; ?>" style="text-transform:uppercase">
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
							</div>
							<br>
						</div>
						<div class="card card-warning card-outline">
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Potongan BPJS TK <span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_bpjs_jht" value="<?php echo $site->column_bpjs_jht; ?>" style="text-transform:uppercase">
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Potongan BPJS KS <span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_bpjs_ks" value="<?php echo $site->column_bpjs_ks; ?>" style="text-transform:uppercase">
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div> 
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Potongan PPH21<span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_tax_calculation" value="<?php echo $site->column_tax_calculation; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom BPJS TK Perusahaan<span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_bpjs_jht_company" value="<?php echo $site->column_bpjs_jht_company; ?>" style="text-transform:uppercase">
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom BPJS KS Perusahaan <span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_bpjs_ks_company" value="<?php echo $site->column_bpjs_ks_company; ?>" style="text-transform:uppercase">
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Potongan Lain <span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control" name="column_outcome" value="<?php echo $site->column_outcome; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<br>
						</div>
						<div class="card card-warning card-outline">
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom NIK <span class="text-danger">*</span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_employee" value="<?php echo $site->column_employee; ?>" style="text-transform:uppercase" required>
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="text" class="col-sm-2 col-form-label column">Nama Kolom THP <span class="text-danger">*</span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_net" value="<?php echo $site->column_net; ?>" style="text-transform:uppercase" required>
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="text" class="col-sm-2 col-form-label column">Nama Kolom Catatan </label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_note" value="<?php echo $site->column_note; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-2 col-form-label">Periode Awal <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="date" class="form-control datepicker" name="periode_start" required>
								</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Periode Selesai <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="date" class="form-control datepicker" name="periode_end" required>
								</div>
								<div class="vl"></div>
							</div>
							<br>
						</div>
						<div class="card card-warning card-outline">
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-3 col-form-label">Pilih File Payroll <span class="text-danger">*</span></label>
								<div class="custom-file col-sm-9">
									<input type="file" class="custom-file-input" name="file" accept=".xlsx" required>
									<label class="custom-file-label">Pilih file</label>
								</div>
								<div class="input-group-append">
									<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>    
								</div>
							</div>
							<br>
						</div>
					</form>
				</div>
			</div> 
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Payroll Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-2">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
						<div class="col-sm-3 process" hidden="true">
							<input type="text" class="form-control note float-right" placeholder="Catatan Pemrosesan" name="note" value="">
						</div>
						<div class="col-sm-7 process" hidden="true">
							<a href="#" class="btn btn-warning pdf float-right" style="width:190px;" data-href="<?php echo base_url('employee_contract/create/pdf'); ?>"><i class="fas fa-file-pdf"></i> Cetak PDF</a>
							<a href="#" onclick="send_mail(this)" class="btn btn-info email float-right" style="margin-right:10px;width:190px;" data-href="<?php echo base_url('payroll/whatsapp/list'); ?>"><i class="fas fa-paper-plane"></i> Kirim Whatsapp</a>
							<a href="#" onclick="preview_payroll(this)" class="btn btn-info preview float-right" style="margin-right:10px;width:190px;" data-href="<?php echo base_url('payroll/preview_payroll'); ?>"><i class="fas fa-eye"></i> Preview Payroll</a>
							<a href="#" class="btn btn-success submission float-right" style="margin-right:10px;width:190px;"><i class="fas fa-check"></i> Proses Pembayaran</a>
							<a href="#" class="btn btn-danger cancel float-right" style="margin-right:10px;width:190px;"><i class="fas fa-window-close"></i> Tolak Pengajuan</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
						</thead>
					</table>
					<div class="row">
						<div class="col-md-6">
							<?php
								if(isset($resume_payroll)):
								$start_date = substr($resume_payroll->periode_start, 8, 2).' '.get_month(substr($resume_payroll->periode_start, 5, 2));
								$end_date 	= substr($resume_payroll->periode_end, 8, 2).' '.get_month(substr($resume_payroll->periode_end, 5, 2)).' '.substr($resume_payroll->periode_end, 0, 4);
							?>
							<table class="table table-striped">
								<thead>
									<tr>
										<td width="250px"><strong>Periode Pengajian Terakhir</strong></td>
										<td><strong>: <?php echo $start_date; ?> - <?php echo $end_date; ?></strong></td>
									</tr>
									<tr>
										<td width="250px"><strong>Total Karyawan Aktif</strong></td>
										<td><strong>: <?php echo $employee; ?> Karyawan</strong></td>
									</tr>
									<tr>
										<td width="250px"><strong>Total Karyawan terakhir digaji</strong></td>
										<td><strong>: <?php echo $resume_payroll->employee_payroll; ?> Karyawan</strong></td>
									</tr>
									<tr>
										<td width="250px"><strong>Total Penggajian Terakhir </strong></td>
										<td><strong>: <?php echo format_rupiah($resume_payroll->summary_payroll); ?></strong></td>
									</tr>
								</thead>
							</table>
							<?php endif; ?>
						</div>
						<div class="col-md-6"><?php
								if(isset($resume_approval)):
								$start_date = substr($resume_approval->periode_start, 8, 2).' '.get_month(substr($resume_approval->periode_start, 5, 2));
								$end_date 	= substr($resume_approval->periode_end, 8, 2).' '.get_month(substr($resume_approval->periode_end, 5, 2)).' '.substr($resume_approval->periode_end, 0, 4);
							?>
							<table class="table table-striped">
								<thead>
									<tr>
										<td width="300px"><strong>Periode Pengajuan Terakhir</strong></td>
										<td><strong>: <?php echo $start_date; ?> - <?php echo $end_date; ?></strong></td>
									</tr>
									<tr>
										<td width="300px"><strong>Total Karyawan Aktif</strong></td>
										<td><strong>: <?php echo $employee; ?> Karyawan</strong></td>
									</tr>
									<tr>
										<td width="300px"><strong>Total Karyawan terakhir Pengajuan</strong></td>
										<td><strong>: <?php echo $resume_approval->employee_payroll; ?> Karyawan</strong></td>
									</tr>
									<tr>
										<td width="300px"><strong>Total Pengajuan Terakhir </strong></td>
										<td><strong>: <?php echo format_rupiah($resume_approval->summary_payroll); ?></strong></td>
									</tr>
								</thead>
							</table>
							<?php endif; ?>
						</div>
					</div>
					<p>&nbsp;</p>
					<div class="row">
						<div class="col-sm-2">
							<strong>Pilih data payroll yang ditampilkan :</strong>
						</div>
						<div class="col-sm-3">
							<select class="form-control preview float-right">
								<option value="<?php echo base_url("payroll/list/last");?>" <?php if($view == "last" ): echo "selected"; endif; ?>>Terakhir</option>
								<option value="<?php echo base_url("payroll/list/all");?>" <?php if($view == "all" ): echo "selected"; endif; ?>>Semua</option>
							</select>
						</div>
						<div class="col-sm-7">
						</div>
					<p>&nbsp;</p>
					</div>
					<form method="POST" id="process" action="<?php echo base_url('payroll/process/list'); ?>">
					<input type="hidden" id="process_type" name="process_type" value="">
					<input type="hidden" id="note" name="note" value="">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th width="50px">No</th>
									<th id='employee_name'>Nama Karyawan</th>
									<th id='phone_number'>Nomor Whatsapp</th>
									<th id='periode_start'>Periode Mulai</th>
									<th id='periode_end'>Periode Selesai</th>
									<th id='bpjs_ks_company'>BPJS KS Perusahaan</th>
									<th id='bpjs_jht_company'>BPJS TK Perusahaan</th>
									<th id='tax_calculation'>PPH 21</th>
									<th id='salary'>Jumlah Pendapatan</th>
									<th id='payment'>Status Pembayaran</th>
									<th id='email'>Pengiriman Whatsapp</th>
									<th id='note'>Catatan</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		<?php endif; ?>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.preview').on('change', function() {
			window.location.href = this.value;
		});
		$('.config-template').on('change', function() {
			window.location.href = '<?php echo base_url("ropayroll/list/".$view."/");?>'+this.value;
		});
		$( ".comma" ).keyup(function() {
			var value = $(this).val();
			console.log(value);
			if(value.includes(",,")){
				$(this).val(value.substring(0, value.length-1));
				alert('Terdapat koma dobel, silahkan cek lagi.');				
			}
		});
		$( ".line" ).keyup(function() {
			var value = $(this).val();
			if(!Number.isInteger(Number(value))){
				alert('Masukkan satu nomor baris saja.');
				$(this).val(value.substring(0, value.length-1));
			}
		});
		$( ".column" ).keyup(function() {
			var value = $(this).val();
			if((/[^a-zA-Z]/.test(value))){
				alert('Masukkan satu nama kolom saja.');
				$(this).val(value.substring(0, value.length-1));
			}
		});
		<?php if($site_id): ?>
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("payroll/list_ajax/".$view);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'employee_name' },
				{ data: 'phone_number' },
				{ data: 'periode_start' },
				{ data: 'periode_end' },
				{ data: 'bpjs_ks_company' },
				{ data: 'bpjs_jht_company' },
				{ data: 'tax_calculation' },
				{ data: 'salary' },
				{ data: 'payment' },
				{ data: 'email' },
				{ data: 'note' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[2, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>1 && index<13){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
	<?php endif; ?>
		$( ".export" ).click(function() {
			var url = "<?php echo base_url('payroll/export/'.$view); ?>" + "?employee_name="+$("#s_employee_name").val()+"&periode_start="+$("#s_periode_start").val()+"&periode_end="+$("#s_periode_end").val()+"&salary="+$("#s_salary").val()+"&payment="+$("#s_payment").val()+"&phone_number="+$("#s_phone_number").val()+"&note="+$("#s_note").val()+"&bpjs_ks_company="+$("#s_bpjs_ks_company").val()+"&bpjs_jht_company="+$("#s_bpjs_jht_company").val()+"&tax_calculation="+$("#s_tax_calculation").val();
			window.location = url;
		
		});

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".process").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".process").prop('hidden', false);
    		});
		});
		$("input:checkbox").change(function() {
			$(".process").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".process").prop('hidden', false);
    		});
		});
		$( ".btn.cancel" ).click(function() {
			$("#process_type").val('cancel');
			$("#note").val($('.note').val());
			$("#process").submit();
		});
		$( ".btn.submission" ).click(function() {
			$("#process_type").val('submission');
			$("#note").val($('.note').val());
			$("#process").submit();
		});

		$( ".btn.pdf" ).click(function() {
			if($('input[type="checkbox"]').length > 51){
				alert("Maksimal 50 checklist ")
				return false;
			}
			$("#process_type").val('pdf');
			$("#process").submit();
		});
	});

	function preview_payroll(el)
	{
		var id = '';
		var i = 0;
		$("input:checkbox.check-id:checked").each(function(){
			if(i > 0){
				id = id + ','; 
			}
			id = id +''+$(this).val();
			i++;
		});
		const url = $(el).attr('data-href')+'?id='+id;
		const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
		const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

		const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

		const systemZoom = width / window.screen.availWidth;
		const left = (width - 1000) / 2 / systemZoom + dualScreenLeft
		const top = (height - 800) / 2 / systemZoom + dualScreenTop
		const newWindow = window.open(url, 'preview',
			`
			scrollbars=yes,
			width=${1000 / systemZoom}, 
			height=${800 / systemZoom}, 
			top=${top}, 
			left=${left}
			`
		)
		return false;
	};

	function send_mail(el)
	{
		var id = '';
		var i = 0;
		$("input:checkbox.check-id:checked").each(function(){
			if(i > 0){
				id = id + ','; 
			}
			id = id +''+$(this).val();
			i++;
		});
		const url = $(el).attr('data-href')+'?id='+id;
		window.location.href = url;
	};
</script>
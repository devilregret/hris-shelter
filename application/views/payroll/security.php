<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('payroll/security'); ?>">Payroll Karyawan Security</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Payroll Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-2">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
						<div class="col-sm-3 process" hidden="true">
							<input type="text" class="form-control note float-right" placeholder="Catatan Pemrosesan" name="note" value="">
						</div>
						<div class="col-sm-7 process" hidden="true">
							<a href="#" class="btn btn-warning pdf float-right" style="width:190px;" data-href="<?php echo base_url('employee_contract/create/pdf'); ?>"><i class="fas fa-file-pdf"></i> Cetak PDF</a>
							<a href="#" onclick="send_mail(this)" class="btn btn-info email float-right" style="margin-right:10px;width:190px;" data-href="<?php echo base_url('payroll/whatsapp/security'); ?>"><i class="fas fa-paper-plane"></i> Kirim Whatsapp</a>
							<a href="#" onclick="preview_payroll(this)" class="btn btn-info preview float-right" style="margin-right:10px;width:190px;" data-href="<?php echo base_url('payroll/preview_payroll'); ?>"><i class="fas fa-eye"></i> Preview Payroll</a>
							<a href="#" class="btn btn-success submission float-right" style="margin-right:10px;width:190px;"><i class="fas fa-check"></i> Proses Pembayaran</a>
							<a href="#" class="btn btn-danger cancel float-right" style="margin-right:10px;width:190px;"><i class="fas fa-window-close"></i> Tolak Pengajuan</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-sm-2">
							<strong>Pilih data payroll yang ditampilkan :</strong>
						</div>
						<div class="col-sm-3">
							<select class="form-control preview float-right">
								<option value="<?php echo base_url("payroll/security/menunggu");?>" <?php if($view == "menunggu" ): echo "selected"; endif; ?>>Menunggu</option>
								<option value="<?php echo base_url("payroll/security/selesai");?>" <?php if($view == "selesai" ): echo "selected"; endif; ?>>Selesai</option>
							</select>
						</div>
						<div class="col-sm-7">
						</div>
					<p>&nbsp;</p>
					</div>
					<form method="POST" id="process" action="<?php echo base_url('payroll/process/security'); ?>">
					<input type="hidden" id="process_type" name="process_type" value="">
					<input type="hidden" id="note" name="note" value="">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th width="50px">No</th>
									<th id='employee_name'>Nama Karyawan</th>
									<th id='phone_number'>Nomor Whatsapp</th>
									<th id='site_name'>Site Bisnis</th>
									<th id='position_name'>Posisi</th>
									<th id='periode_start'>Periode Mulai</th>
									<th id='periode_end'>Periode Selesai</th>
									<th id='tax_calculation'>PPH 21</th>
									<th id='salary'>Jumlah Pendapatan</th>
									<th id='payment'>Status Pembayaran</th>
									<th id='email'>Pengiriman Whatsapp</th>
									<th id='note'>Catatan</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.preview').on('change', function() {
			window.location.href = this.value;
		});
		$( ".comma" ).keyup(function() {
			var value = $(this).val();
			console.log(value);
			if(value.includes(",,")){
				$(this).val(value.substring(0, value.length-1));
				alert('Terdapat koma dobel, silahkan cek lagi.');				
			}
		});
		$( ".line" ).keyup(function() {
			var value = $(this).val();
			if(!Number.isInteger(Number(value))){
				alert('Masukkan satu nomor baris saja.');
				$(this).val(value.substring(0, value.length-1));
			}
		});
		$( ".column" ).keyup(function() {
			var value = $(this).val();
			if((/[^a-zA-Z]/.test(value))){
				alert('Masukkan satu nama kolom saja.');
				$(this).val(value.substring(0, value.length-1));
			}
		});
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("payroll/security_ajax/".$view);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'employee_name' },
				{ data: 'phone_number' },
				{ data: 'site_name' },
				{ data: 'position_name' },
				{ data: 'periode_start' },
				{ data: 'periode_end' },
				{ data: 'tax_calculation' },
				{ data: 'salary' },
				{ data: 'payment' },
				{ data: 'email' },
				{ data: 'note' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[2, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>1 && index<13){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
		$( ".export" ).click(function() {
			var url = "<?php echo base_url('payroll/security_export/'.$view); ?>" + "?employee_name="+$("#s_employee_name").val()+"&periode_start="+$("#s_periode_start").val()+"&periode_end="+$("#s_periode_end").val()+"&salary="+$("#s_salary").val()+"&payment="+$("#s_payment").val()+"&phone_number="+$("#s_phone_number").val()+"&note="+$("#s_note").val()+"&position_name="+$("#s_position_name").val()+"&site_name="+$("#s_site_name").val()+"&tax_calculation="+$("#s_tax_calculation").val();
			window.location = url;
		
		});

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".process").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".process").prop('hidden', false);
    		});
		});
		$("input:checkbox").change(function() {
			$(".process").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".process").prop('hidden', false);
    		});
		});
		$( ".btn.cancel" ).click(function() {
			$("#process_type").val('cancel');
			$("#note").val($('.note').val());
			$("#process").submit();
		});
		$( ".btn.submission" ).click(function() {
			$("#process_type").val('submission');
			$("#note").val($('.note').val());
			$("#process").submit();
		});

		$( ".btn.pdf" ).click(function() {
			if($('input[type="checkbox"]').length > 51){
				alert("Maksimal 50 checklist ")
				return false;
			}
			$("#process_type").val('pdf');
			$("#process").submit();
		});
	});

	function preview_payroll(el)
	{
		var id = '';
		var i = 0;
		$("input:checkbox.check-id:checked").each(function(){
			if(i > 0){
				id = id + ','; 
			}
			id = id +''+$(this).val();
			i++;
		});
		const url = $(el).attr('data-href')+'?id='+id;
		const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
		const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

		const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

		const systemZoom = width / window.screen.availWidth;
		const left = (width - 1000) / 2 / systemZoom + dualScreenLeft
		const top = (height - 800) / 2 / systemZoom + dualScreenTop
		const newWindow = window.open(url, 'preview',
			`
			scrollbars=yes,
			width=${1000 / systemZoom}, 
			height=${800 / systemZoom}, 
			top=${top}, 
			left=${left}
			`
		)
		return false;
	};

	function send_mail(el)
	{
		var id = '';
		var i = 0;
		$("input:checkbox.check-id:checked").each(function(){
			if(i > 0){
				id = id + ','; 
			}
			id = id +''+$(this).val();
			i++;
		});
		const url = $(el).attr('data-href')+'?id='+id;
		window.location.href = url;
	};
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('rocasual/regular'); ?>">Jadwal Regular</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Jadwal Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<form method="GET" action="<?php echo base_url('rocasual/schedule/'.$status); ?>">
					<div class="row">
						<label class="col-sm-2 col-form-label">Tanggal Awal :</label>
						<div class="col-sm-3">
							<input type="date" name="date_start" class="form-control datepicker" value="<?php echo $date_start; ?>">
						</div>
						<label class="col-sm-2 col-form-label">Tanggal Akhir :</label>
						<div class="col-sm-3">
							<input type="date" name="date_finish" class="form-control datepicker" value="<?php echo $date_finish; ?>">
						</div>
						<div class="col-sm-2">
							<input class="btn btn-success" type="submit" value="Cari" style="min-width: 100px;">
							<a class="btn btn-danger delete" style="color: white;"><i class="fas fa-trash"></i> Hapus</a>
						</div>
					</div>
					</form>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
							<tr>
								<td width="250px">Total Karyawan</td>
								<td>: <?php echo $total; ?></td>
							</tr>
						</thead>
					</table>
					<form method="POST" id="delete-schedule" action="<?php echo base_url('rocasual/schedule_delete_all/'.$status.'/'.$date_start.'/'.$date_finish); ?>">
					<div class="scroll-panel">
						<table id="data-schedule" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th id='employee_number'>ID Karyawan</th>
									<th id='pin_finger'>PIN Finger</th>
									<th id='id_card'>NIK KTP</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='date'>Tanggal</th>
									<th id='department'>Departemen</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Karyawan</h3>
				</div>
				<div class="card-header">
					<div class="row">
						<label class="col-sm-2 col-form-label">Tanggal Awal :</label>
						<div class="col-sm-2">
							<input type="date" name="date_start" class="form-control datepicker" id="date_start" value="<?php echo $date_start; ?>">
						</div>
						<label class="col-sm-2 col-form-label">Tanggal Akhir :</label>
						<div class="col-sm-2">
							<input type="date" name="date_finish" class="form-control datepicker" id="date_finish" value="<?php echo $date_finish; ?>">
						</div>
						<div class="col-sm-2 submission">
							<select class="form-control select2" id="template" required>
								<?php foreach ($list_department as $item) :  ?>
								<option value="<?php echo $item->id;?>"><?php echo
									$item->name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="col-sm-2">
							<a class="btn btn-primary save"><i class="fas fa-save"></i> Simpan</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<p>&nbsp; </p>
					<form method="POST" id="schedule" action="<?php echo base_url('rocasual/schedule_save/'.$status); ?>">
					<input type="text" name="date_start" id="date_start_id" value="" hidden="">
					<input type="text" name="date_finish" id="date_finish_id" value="" hidden="">
					<input type="text" name="template_id" id="template_id" value="" hidden="">
					<div class="scroll-panel">
						<table id="data-employee" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th id='employee_number'>ID Karyawan</th>
									<th id='pin_finger'>PIN Finger</th>
									<th id='id_card'>NIK KTP</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='phone_number'>Nomor Whatsapp</th>
									<th id='bank_name'>Nama Bank</th>
									<th id='bank_account'>No Rekening</th>
									<th id='bank_account_name'>Atas Nama</th>
									<th id='status'>Status Karyawan</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-employee').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			 'searching'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("rocasual/employee_ajax/".$status);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'employee_number' },
				{ data: 'pin_finger' },
				{ data: 'id_card' },
				{ data: 'full_name' },
				{ data: 'phone_number' },
				{ data: 'bank_name' },
				{ data: 'bank_account' },
				{ data: 'bank_account_name' },
				{ data: 'status' },
				{ data: 'action', 'orderable':false, 'searchable':false  }
			],
			'order': [[4, 'asc']]
		});

		$('#data-employee tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<10){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-employee').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$('#data-schedule').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			 'searching'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("rocasual/schedule_ajax/".$status."/".$date_start."/".$date_finish);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'employee_number' },
				{ data: 'pin_finger' },
				{ data: 'id_card' },
				{ data: 'full_name' },
				{ data: 'date' },
				{ data: 'department' },
				{ data: 'action', 'orderable':false, 'searchable':false  }
			],
			'order': [[4, 'asc']]
		});

		$('#data-schedule tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<7){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-schedule').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$(".save" ).click(function() {
			$( "#date_start_id").val($( "#date_start").val());
			$( "#date_finish_id").val($("#date_finish").val());
			$( "#template_id").val($( "#template").val());
			$( "#schedule" ).submit();
		});

		$(".delete" ).click(function() {
			$( "#delete-schedule" ).submit();
		});

		$('#data-employee #checkAll').click(function () {
		    $('#data-employee input:checkbox').prop('checked', this.checked);  
		});

		$('#data-schedule #checkAll').click(function () {
		    $('#data-schedule input:checkbox').prop('checked', this.checked);  
		});
	});
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('rocasual/list'); ?>">Daftar Karyawan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-4">
							<a href="<?php echo base_url("rocasual/employee_form"); ?>" class="btn btn-primary btn" style="width:200px"; ><i class="fas fa-plus"></i> Tambah</a>
							<a href="#" class="btn btn-success export" style="width:200px";><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
						<div class="col-sm-8">
							<form method="post" action="<?php echo base_url("rocasual/import"); ?>" enctype="multipart/form-data">
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input btn-sm" name="file" accept=".xlsx" required> 
										<label class="custom-file-label" for="exampleInputFile">Update Data</label>
									</div>
									<div class="input-group-append">
										<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>   
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="card-header submission" hidden="true">
					<div class="row">
						<div class="col-sm-2">
							<label>Status : </label>
						</div>
						<div class="col-sm-3">
							<select class="form-control select2" id="position" required>
								<option value="">Pilih Jabatan</option>
								<?php foreach ($list_position as $item): ?>
								<option value="<?php echo $item->id; ?>"><?php echo $item->name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="col-sm-3">
							<select class="form-control select2" id="department" required>
								<option value="">Pilih Departemen</option>
								<?php foreach ($list_department as $item): ?>
								<option value="<?php echo $item->id; ?>"><?php echo $item->name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="col-sm-3">
							<select class="form-control select2" id="job_status" required>
								<option value="On Call">On Call</option>
								<option value="Regular">Regular</option>
								<option value="Non Aktif">Non Aktif</option>
							</select>
						</div>
						<div class="col-sm-1">
							<a class="btn btn-primary save btn-block"><i class="fas fa-save"></i> Simpan</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
							<tr>
								<td width="250px">Total Karyawan Casual</td>
								<td>: <?php echo $total; ?></td>
							</tr>
						</thead>
					</table>
					<p>&nbsp;</p>
					<p>&nbsp; </p>
					<form method="POST" id="contract" action="<?php echo base_url('rocasual/save_status'); ?>">
					<input type="text" name="status" id="status" value="" hidden="">
					<input type="text" name="position_id" id="position_id" value="" hidden="">
					<input type="text" name="department_casual_id" id="department_casual_id" value="" hidden="">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th id='employee_number'>ID Karyawan</th>
									<th id='pin_finger'>PIN Finger</th>
									<th id='id_card'>NIK KTP</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='phone_number'>Nomor Whatsapp</th>
									<th id='bank_name'>Nama Bank</th>
									<th id='bank_account'>No Rekening</th>
									<th id='bank_account_name'>Atas Nama</th>
									<th id='position_name'>Jabatan</th>
									<th id='department_name'>Departemen</th>
									<th id='status'>Status Karyawan</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			 'searching'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("rocasual/list_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'employee_number' },
				{ data: 'pin_finger' },
				{ data: 'id_card' },
				{ data: 'full_name' },
				{ data: 'phone_number' },
				{ data: 'bank_name' },
				{ data: 'bank_account' },
				{ data: 'bank_account_name' },
				{ data: 'position_name' },
				{ data: 'department_name' },
				{ data: 'status' },
				{ data: 'action', 'orderable':false, 'searchable':false  }
			],
			'order': [[1, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<12){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$( ".export" ).click(function() {
			var url = "<?php echo base_url('rocasual/export'); ?>" + "?employee_number="+$("#s_employee_number").val()+"&pin_finger="+$("#s_pin_finger").val()+"&id_card="+$("#s_id_card").val()+"&full_name="+$("#s_full_name").val()+"&phone_number="+$("#s_phone_number").val()+"&bank_name="+$("#s_bank_name").val()+"&bank_account="+$("#s_bank_account").val()+"&bank_account_name="+$("#s_bank_account_name").val()+"&position_name="+$("#s_position_name").val()+"&department_name="+$("#s_department_name").val()+"&status="+$("#s_status").val();
			window.location = url;
		
		});

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});
		$(document).on("change", ".check-id", function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});
		$("input:checkbox").change(function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});
		$(".save" ).click(function() {
			$( "#status").val($("#job_status").val());
			$( "#position_id").val($("#position").val());
			$( "#department_casual_id").val($("#department").val());
			$( "#contract" ).submit();
		});
	});
</script>
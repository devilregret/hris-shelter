<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('rocasual/payroll_preview'); ?>">Payroll Preview</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Absensi Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<form id="attendance" method="POST" action="<?php echo base_url('rocasual/payroll_preview'); ?>">
					<div class="row">
						<label class="col-sm-1 col-form-label">Tanggal Awal :</label>
						<div class="col-sm-2">
							<div class="form-group">
								<div class="input-group date" id="reservationdate" data-target-input="nearest">
									<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name='date_start' value="<?php echo $date_start; ?>" />
									<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>
							</div>
						</div>
						<label class="col-sm-1 col-form-label">Tanggal Akhir :</label>
						<div class="col-sm-2">
							<div class="input-group date" id="reservationdate2" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate2" name='date_finish' value="<?php echo $date_finish; ?>" />
								<div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<input class="btn btn-success btn-block" type="submit" value="Cari" style="width: 130px;">
						</div>

						<div class="col-sm-2">							
							<a href="#" class="btn btn-warning export_normal btn-block"><i class="fas fa-file-pdf"></i> Export Absensi Normal</a>
						</div>
						<div class="col-sm-2">
							<a href="#" class="btn btn-danger export_overtime btn-block"><i class="fas fa-file-pdf"></i> Export Overtime</a>
						</div>
					</div>
					</form>
				</div>
				<div class="card-body">
					<div class="card-header">
						<table class="table table-striped" style="float: left; width: 40%;">
							<thead>
								<tr>
									<td width="250px">Site Id</td>									
									<td>: <?php echo $site->id; ?></td>
								</tr>
								<tr>
									<td width="250px">Nama Site Bisnis</td>
									<td>: <?php echo $site->name; ?></td>
								</tr>
								<tr>
									<td width="250px">Alamat Site Bisnis</td>
									<td>: <?php echo $site->address; ?></td>
								</tr>
								<tr>
									<td width="250px">Total Karyawan Casual</td>
									<td>: <?php echo $total; ?></td>
								</tr>
							</thead>
						</table>
						<table class="table table-striped" style="float: left; width:40%;">
							<thead>
								<tr>
									<td width="250px">Gaji Pokok</td>									
									<td>: <?php echo rupiah_round($basic_salary); ?></td>
								</tr>
								<tr>
									<td width="250px">MF(%) </td>
									<td>: <?php echo $management_fee; ?></td>
								</tr>
								<tr>
									<td width="250px">PPN(%) </td>
									<td>: <?php echo $ppn; ?></td>
								</tr>
								<tr>
									<td width="250px">PPH(%) </td>
									<td>: <?php echo $pph; ?></td>
								</tr>
								<tr>
									<td width="250px">ID Mesin </td>
									<td>: <?php echo $site->attendance_machine; ?></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="card-header">
						<div class="row submission" hidden="true">
							<label class="col-sm-1 col-form-label text-right">Departemen :</label>
							<div class="col-sm-2">
								<select class="form-control select2" id="department_id">
									<?php foreach ($list_department as $item) :  ?>
									<option value="<?php echo $item->id;?>"><?php echo
										$item->name; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<label class="col-sm-1 col-form-label text-right">Overtime :</label>
							<div class="col-sm-2">
								<select class="form-control select2" id="overtime_id">
									<option value="Tidak">Tidak</option>
									<option value="Ya">Ya</option>
								</select>
							</div>
							<label class="col-sm-2 col-form-label text-right">Status Pengajuan :</label>
							<div class="col-sm-2">
								<select class="form-control select2" id="status_id">
									<option value="Draft">Draft</option>
									<option value="Pengajuan">Pengajuan</option>
									<option value="Disetujui">Disetujui</option>
								</select>
							</div>
							<div class="col-sm-2 submission">
								<a class="btn btn-primary save btn-block"><i class="fas fa-save"></i> Simpan</a>
							</div>
						</div>
					</div>
					<p>&nbsp;</p>
					<div class="scroll-panel">
					<form method="POST" id="contract" action="<?php echo base_url('rocasual/attendance_status/'.$pdate_start."/".$pdate_finish); ?>">
					<input type="text" name="department" id="department" value="" hidden="">
					<input type="text" name="overtime" id="overtime" value="" hidden="">
					<input type="text" name="status" id="status" value="" hidden="">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th id='pin_finger'>PIN Finger</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='status_employee'>Status Karyawan</th>
									<th id='date'>Tanggal</th>
									<th id='check_in'>Check In</th>
									<th id='check_out'>Check Out</th>
									<th id='department'>Departemen</th>
									<th id='overtime'>Overtime</th>
									<th id='status_attendance'>Status Pengajuan</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#reservationdate').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false
		});
		$('#reservationdate2').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false
		});
		
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			 'searching'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("rocasual/payroll_preview_ajax/".$pdate_start."/".$pdate_finish);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'pin_finger' },
				{ data: 'full_name' },
				{ data: 'status_employee' },
				{ data: 'date' },
				{ data: 'check_in' },
				{ data: 'check_out' },
				{ data: 'department' },
				{ data: 'overtime' },
				{ data: 'status_attendance' },
				{ data: 'action', 'orderable':false, 'searchable':false  }
			],
			'order': [[4, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<10){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );


		$( ".export_normal" ).click(function() {
			var url = "<?php echo base_url('rocasual/payroll_export/normal?date_start='.$pdate_start.'&date_finish='.$pdate_finish); ?>" + "&pin_finger="+$("#s_pin_finger").val()+"&full_name="+$("#s_full_name").val()+"&status_employee="+$("#s_status_employee").val()+"&department="+$("#s_department").val()+"&overtime="+$("#s_overtime").val()+"&status_attendance="+$("#s_status_attendance").val()+"&date="+$("#s_date").val();
			window.location = url;
		
		});

		$( ".export_overtime" ).click(function() {
			var url = "<?php echo base_url('rocasual/payroll_export/overtime?date_start='.$pdate_start.'&date_finish='.$pdate_finish); ?>" + "&pin_finger="+$("#s_pin_finger").val()+"&full_name="+$("#s_full_name").val()+"&status_employee="+$("#s_status_employee").val()+"&department="+$("#s_department").val()+"&overtime="+$("#s_overtime").val()+"&status_attendance="+$("#s_status_attendance").val()+"&date="+$("#s_date").val();
			window.location = url;
		
		});

		// $(".export_normal" ).click(function() {
		// 	$('#attendance').attr('action', "<?php echo base_url('rocasual/attendance_export/normal'); ?>").submit();
		// });

		// $(".export_overtime" ).click(function() {
		// 	$('#attendance').attr('action', "<?php echo base_url('rocasual/attendance_export/overtime'); ?>").submit();
		// });

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$("input:checkbox").change(function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});

		$(document).on("change", ".check-id", function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});

		$(".save" ).click(function() {
			$( "#department").val($("#department_id").val());
			$( "#overtime").val($( "#overtime_id").val());
			$( "#status").val($( "#status_id").val());
			$( "#contract" ).submit();
		});
	});
</script>
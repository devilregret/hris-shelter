<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('rocasual/department'); ?>">Daftar Departemen</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Departemen</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("rocasual/employee_form"); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nama Karyawan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="text" class="form-control" placeholder="Nama Karyawan" name="full_name" value="<?php echo $full_name; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nomor Karyawan/PIN Finger <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control numeric" placeholder="Nomor Karyawan/PIN Finger" name="employee_number" value="<?php echo $employee_number; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">NIK KTP <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control numeric" placeholder="NIK KTP" name="id_card" value="<?php echo $id_card; ?>" >
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nomor WA/HP <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control numeric" placeholder="Nomor WA/HP" name="phone_number" value="<?php echo $phone_number; ?>" >
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Bank <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nama Bank" name="bank_name" value="<?php echo $bank_name; ?>" >
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nomor Rekening <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control numeric" placeholder="Nomor Rekening" name="bank_account" value="<?php echo $bank_account; ?>" >
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Atas Nama Rekening <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Atas Nama Rekening" name="bank_account_name" value="<?php echo $bank_account_name; ?>" >
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Alamat <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<textarea class="form-control" name="address" rows="3" placeholder="Contoh : Jl. Raya Nginden No.102, RT.001/RW.03, Baratajaya, Kec. Gubeng 60284"><?php echo $address; ?></textarea>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
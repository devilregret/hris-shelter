<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('rocasual/attendance'); ?>">Absensi Karyawan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Absensi</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("rocasual/attendance_form/".$date_start."/".$date_finish); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Karyawan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<select class="form-control select2" name="employee_id" required>
										<option value=""> &nbsp;-- Daftar Karyawan --</option>
										<?php foreach ($list_employee as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $employee_id): echo "selected"; endif;?>><?php echo $item->employee_number.' - '.$item->full_name; ?></option>
										<?php endforeach; ?>
									</select>

								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Tanggal <span class="text-danger">*</span></label>
								<div class="col-sm-4">
									<div class="input-group date" id="attendancedate" data-target-input="nearest">
										<input type="text" class="form-control datetimepicker-input" data-target="#attendancedate"  data-toggle="datetimepicker" name='date' value="<?php echo $date; ?>" required/>
										<div class="input-group-append" data-target="#attendancedate" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="fa fa-calendar"></i></div>
										</div>
									</div>
								</div>
								<div class="col-sm-7">
									&nbsp;
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Check In <span class="text-danger">*</span></label>
								<div class="col-sm-4">
									<div class="input-group date" id="date_in" data-target-input="nearest">
										<input type="text" class="form-control datetimepicker-input" data-target="#date_in" data-toggle="datetimepicker" name='date_in' value="<?php echo $date_in; ?>" required/>
										<div class="input-group-append" data-target="#date_in" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="fa fa-calendar"></i></div>
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="input-group date" id="check_in" data-target-input="nearest">
										<input type="text" class="form-control datetimepicker-input" data-target="#check_in" data-toggle="datetimepicker" name='check_in' value="<?php echo $check_in; ?>" required/>
										<div class="input-group-append" data-target="#check_in" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="fa fa-calendar"></i></div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									&nbsp;
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Check Out <span class="text-danger">*</span></label>
								<div class="col-sm-4">
									<div class="input-group date" id="date_out" data-target-input="nearest">
										<input type="text" class="form-control datetimepicker-input" data-target="#date_out" data-toggle="datetimepicker" name='date_out' value="<?php echo $date_out; ?>" required/>
										<div class="input-group-append" data-target="#date_out" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="fa fa-calendar"></i></div>
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="input-group date" id="check_out" data-target-input="nearest">
										<input type="text" class="form-control datetimepicker-input" data-target="#check_out" name='check_out' value="<?php echo $check_out; ?>" required/>
										<div class="input-group-append" data-target="#check_out" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="fa fa-calendar"></i></div>
										</div>
									</div>
								</div>
								<div class="col-sm-7">
									&nbsp;
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#attendancedate').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false
		});
		$('#date_in').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false
		});
		$('#date_out').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false
		});
		$('#check_in').datetimepicker({
			format: 'HH:mm:ss',
			pickDate: false,
			pickSeconds: false,
			pick12HourFormat: false  
		});
		$('#check_out').datetimepicker({
			format: 'HH:mm:ss',
			pickDate: false,
			pickSeconds: false,
			pick12HourFormat: false  
		});
	});
</script>
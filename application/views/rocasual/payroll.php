<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('rocasual/payroll'); ?>">Payroll Karyawan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Payroll Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<form id="payroll" method="POST" action="<?php echo base_url('rocasual/payroll'); ?>">
					<div class="row">
						<label class="col-sm-1 col-form-label">Gaji Pokok :</label>
						<div class="col-sm-2">
							<input type="text" name="basic_salary" class="form-control numeric rupiah" value="<?php echo rupiah_round($basic_salary); ?>">
						</div>
						<label class="col-sm-1 col-form-label">MF(%) :</label>
						<div class="col-sm-1">
							<input type="number" name="management_fee" class="form-control" value="<?php echo $management_fee; ?>">
						</div>
						<label class="col-sm-1 col-form-label">PPN(%) :</label>
						<div class="col-sm-1">
							<input type="number" name="ppn" class="form-control" value="<?php echo $ppn; ?>">
						</div>
						<label class="col-sm-1 col-form-label">PPH(%) :</label>
						<div class="col-sm-1">
							<input type="number" name="pph" class="form-control" value="<?php echo $pph; ?>">
						</div>
						<div class="col-sm-1">
							&nbsp;
						</div>
						<div class="col-sm-2">
							<a class="btn btn-warning count" style="color: white; width: 200px;"><i class="fa fa-cogs"></i> Hitung</a>
						</div>
					</div>
					<br>
					<div class="row">
						<label class="col-sm-1 col-form-label">Tanggal Awal :</label>
						<div class="col-sm-4">
							<div class="form-group">
								<div class="input-group date" id="reservationdate" data-target-input="nearest">
									<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name='date_start' value="<?php echo $date_start; ?>" />
									<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>
							</div>
						</div>
						<label class="col-sm-1 col-form-label">Tanggal Akhir :</label>
						<div class="col-sm-4">
							<div class="input-group date" id="reservationdate2" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate2" name='date_finish' value="<?php echo $date_finish; ?>" />
								<div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<input class="btn btn-success search" type="submit" value="Cari" style="min-width: 200px;">
						</div>
					</div>
					</form>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
							<tr>
								<td width="250px">Total Karyawan Casual</td>
								<td>: <?php echo $total; ?></td>
							</tr>
						</thead>
					</table>
					<p>&nbsp;</p>
					<div class="card-header">
						<div class="row submission" hidden="true">
							<label class="col-sm-2 col-form-label">Status Pengajuan :</label>
							<div class="col-sm-6">
								<select class="form-control select2" id="status_id">
									<option value="Draft">Draft</option>
									<option value="Menunggu">Menunggu</option>
									<!-- <option value="Selesai">Selesai</option> -->
								</select>
							</div>
							<div class="col-sm-4 submission">
								<a class="btn btn-primary save" style="width:200px;color: white;"><i class="fas fa-save"></i> Simpan</a>
								<a class="btn btn-danger delete" style="width:200px;color: white;"><i class="fas fa-trash"></i> Hapus</a>
							</div>
						</div>
					</div>
					<p>&nbsp;</p>
					<br>
					<div class="scroll-panel">
					<form method="POST" id="approve" action="<?php echo base_url('rocasual/payroll_approve/'.$pdate_start."/".$pdate_finish); ?>">
					<input type="text" name="status" id="status" value="" hidden="">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th id='employee_number'>ID Karyawan</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='status_'>Status Kontrak</th>
									<th id='status_'>Status Pengajuan</th>
									<th id='periode'>Periode</th>
									<th id='total_shift'>Total Shift</th>
									<th id='basic_salary'>Gaji Pokok</th>
									<th id='total_salary'>Total Gaji</th>
									<th id='management_fee'>Management Fee</th>
									<th id='ppn'>PPN</th>
									<th id='pph'>PPH</th>
									<th id='salary'>Grand Total</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#reservationdate').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false
		});
		$('#reservationdate2').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false
		});
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			 'searching'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("rocasual/payroll_ajax/".$pdate_start."/".$pdate_finish);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'employee_number' },
				{ data: 'full_name' },
				{ data: 'status_employee' },
				{ data: 'status_approval' },
				{ data: 'periode', 'orderable':false, 'searchable':false   },
				{ data: 'total_shift', 'orderable':false, 'searchable':false  , 'orderable':false, 'searchable':false   },
				{ data: 'basic_salary', 'orderable':false, 'searchable':false   },
				{ data: 'total_salary', 'orderable':false, 'searchable':false   },
				{ data: 'management_fee', 'orderable':false, 'searchable':false   },
				{ data: 'ppn', 'orderable':false, 'searchable':false   },
				{ data: 'pph', 'orderable':false, 'searchable':false   },
				{ data: 'salary', 'orderable':false, 'searchable':false   }
			],
			'order': [[2, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<5){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
		
		$(".count" ).click(function() {
			$('#payroll').attr('action', "<?php echo base_url('rocasual/payroll_count'); ?>").submit();
		});

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$("input:checkbox").change(function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});

		$(document).on("change", ".check-id", function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});

		$(".save" ).click(function() {
			$( "#status").val($( "#status_id").val());
			$( "#approve" ).submit();
		});

		$(".delete" ).click(function() {
			$('#approve').attr('action', "<?php echo base_url('rocasual/payroll_delete/'.$pdate_start."/".$pdate_finish); ?>").submit();
		});
	});
</script>
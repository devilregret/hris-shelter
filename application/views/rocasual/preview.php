<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>

	</head>
	<body>
		<section class="content">
			<div class="container-fluid">
				<div class="card card-primary card-outline col-12 mt-2">
					<div class="card-header">
						<h3 class="card-title"><i class="fas fa-eye"></i>&nbsp;Preview Karyawan</h3>
					</div>
					<div class="row">
						<div class="col-12 table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Id</td>
										<td>: <?php echo $preview->id; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">No Karyawan</td>
										<td>: <?php echo $preview->employee_number; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">PIN Finger</td>
										<td>: <?php echo $preview->employee_number; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Nama Lengkap</td>
										<td>: <?php echo $preview->full_name; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Nomor KTP</td>
										<td>: <?php echo $preview->id_card; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Nomor Whatsapp</td>
										<td>: <?php echo $preview->phone_number; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Nama Bank</td>
										<td>: <?php echo $preview->bank_name; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Nomor Rekening</td>
										<td>: <?php echo $preview->bank_account; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Atas Nama Rekening</td>
										<td>: <?php echo $preview->bank_account_name; ?></td>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="window.close();">Tutup</button>
       	</div>
	</body>
</html>
<p style="text-align: center; font-family: 'Arial'; line-height: 1;">
	<span style="font-size: 12pt;">
		<strong>SURAT PERNYATAAN</strong>
	</span>
</p>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td colspan="3;">Yang bertanda tangan di bawah ini:</td>
		</tr>
		<tr>
			<td colspan="3;">&nbsp;</td>
		</tr>
		<tr>
			<td>Nama Lengkap</td>
			<td>:</td>
			<td>
				<strong>{NAMA_LENGKAP}</strong>
			</td>
		</tr>
		<tr>
			<td>Jenis Kelamin</td>
			<td>:</td>
			<td>{JENIS_KELAMIN}</td>
		</tr>
		<tr>
			<td>Tempat/Tanggal Lahir</td>
			<td>:</td>
			<td>{TEMPAT_LAHIR}, {TANGGAL_LAHIR}</td>
		</tr>
		<tr>
			<td>No. KTP</td>
			<td>:</td>
			<td>{NIK}</td>
		</tr>
		<tr>
			<td>Alamat Sesuai KTP</td>
			<td>:</td>
			<td>{ALAMAT_KTP}</td>
		</tr>
		<tr>
			<td colspan="3;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3;">Dengan ini, saya menyatakan bahwa saya setuju dan menerima semua yang tertuang di dalam Perjanjian Kerja Waktu Tertentu (PKWT) dan Perjanjian Bersama (PB) yang telah saya tanda tangani sebagai Pihak Kedua dengan PT Shelter Nusantara sebagai Pihak Pertama.</td>
		</tr>
		<tr>
			<td colspan="3;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3;">Demikian pernyataan ini saya buat dengan kesadaran penuh (sehat jasmani dan rohani) serta tanpa ada tekanan dan paksaan dari pihak manapun.</td>
		</tr>
		<tr>
			<td colspan="3;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3;">Ditandatangani di Kota/Kab. _____________________ pada tanggal _____________________ </td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td style="width: 50%; text-align: center;">Yang Membuat Pernyataan</td>
			<td style="width: 50%; text-align: center;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 50%; text-align: center;">
				<strong>{NAMA_LENGKAP}</strong>
			</td>
			<td style="width: 50%; text-align: center;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="width: 50%; text-align: center;">
				<strong>&nbsp;</strong>
			</td>
			<td style="width: 50%; text-align: center;">&nbsp;</td>
		</tr>
	</tbody>
</table>
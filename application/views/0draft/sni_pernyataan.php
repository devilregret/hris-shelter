<p style="text-align: center; font-family: 'Arial'; line-height: 1;">
	<span style="font-size: 12pt;">
		<strong>SURAT PERNYATAAN</strong>
	</span>
</p>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td colspan="3;">Yang bertanda tangan di bawah ini:</td>
		</tr>
		<tr>
			<td colspan="3;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 220px;">Nama Lengkap</td>
			<td style="width: 15px;">:</td>
			<td style="width: 390px;">
				<strong>{NAMA_LENGKAP}</strong>
			</td>
		</tr>
		<tr>
			<td style="width: 220px;">Jenis Kelamin</td>
			<td style="width: 15px;">:</td>
			<td style="width: 390px;">
				<strong>{JENIS_KELAMIN}</strong>
			</td>
		</tr>
		<tr>
			<td style="width: 220px;">Tempat/Tanggal Lahir</td>
			<td style="width: 15px;">:</td>
			<td style="width: 390px;">
				<strong>{TEMPAT_LAHIR}, {TANGGAL_LAHIR}</strong>
			</td>
		</tr>
		<tr>
			<td style="width: 220px;">No. KTP</td>
			<td style="width: 15px;">:</td>
			<td style="width: 390px;">
				<strong>{NIK}</strong>
			</td>
		</tr>
		<tr>
			<td style="width: 220px;">Alamat Sesuai KTP</td>
			<td style="width: 15px;">:</td>
			<td style="width: 390px;">
				<strong>{ALAMAT_KTP}</strong>
			</td>
		</tr>
		<tr>
			<td colspan="3;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3;">Dengan ini, saya menyatakan bahwa saya setuju dan menerima semua yang tertuang di dalam Perjanjian Kerja Waktu Tertentu (PKWT) dan Perjanjian Bersama (PB) yang telah saya tanda tangani sebagai Pihak Kedua dengan PT Shelter Nusa Indah sebagai Pihak Pertama.</td>
		</tr>
		<tr>
			<td colspan="3;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3;">Demikian pernyataan ini saya buat dengan kesadaran penuh (sehat jasmani dan rohani) serta tanpa ada tekanan dan paksaan dari pihak manapun.</td>
		</tr>
		<tr>
			<td colspan="3;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3;">Ditandatangani di Kota/Kab. _____________________ pada tanggal _____________________ </td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td style="width: 50%; text-align: center;">Yang Membuat Pernyataan</td>
			<td style="width: 50%; text-align: center;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 50%; text-align: center;">
				<strong>{NAMA_LENGKAP}</strong>
			</td>
			<td style="width: 50%; text-align: center;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="width: 50%; text-align: center;">
				<strong>&nbsp;</strong>
			</td>
			<td style="width: 50%; text-align: center;">&nbsp;</td>
		</tr>
	</tbody>
</table>
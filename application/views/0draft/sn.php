<p style="text-align: center; font-family: 'Arial'; line-height: 1;">
	<span style="font-size: 12pt;">
		<strong>
			<u>PERJANJIAN MITRA KERJA WAKTU TERTENTU</u>
		</strong>
	</span>
	<br />
	<span style="font-size: 12pt;">
		<strong>
			<u>NO : {NOMOR_KONTRAK}</u>
		</strong>
	</span>
</p>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td colspan="4;">Yang bertanda tangan dibawah ini kami, masing-masing :</td>
		</tr>
		<tr>
			<td colspan="4;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 20px;">1.</td>
			<td style="width: 200px;">Nama Lengkap</td>
			<td style="width: 15px;">:</td>
			<td style="width: 390px;">
				<strong>MIFTAKHUL ARIF</strong>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Jabatan</td>
			<td>:</td>
			<td>HR &amp; GM HC & IT</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Instansi</td>
			<td>:</td>
			<td>{NAMA_BISNIS_UNIT}</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Alamat Lengkap</td>
			<td>:</td>
			<td>{ALAMAT_BISNIS_UNIT}</td>
		</tr>
		<tr>
			<td colspan="4;">Bertindak untuk dan atas nama kuasa direksi <strong>{NAMA_BISNIS_UNIT}</strong> selanjutnya disebut sebagai <strong>PIHAK PERTAMA</strong></td>
		</tr>
		<tr>
			<td colspan="4;">&nbsp;</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Nama Lengkap</td>
			<td>:</td>
			<td>
				<strong>{NAMA_LENGKAP}</strong>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Jenis Kelamin</td>
			<td>:</td>
			<td>{JENIS_KELAMIN}</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Tempat/Tanggal Lahir</td>
			<td>:</td>
			<td>{TEMPAT_LAHIR}, {TANGGAL_LAHIR}</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>No. KTP</td>
			<td>:</td>
			<td>{NIK}</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Alamat Sesuai KTP</td>
			<td>:</td>
			<td>{ALAMAT_KTP}</td>
		</tr>
		<tr>
			<td colspan="4;">Bertindak untuk dan atas nama pribadi dan diri sendiri, selanjutnya disebut sebagai <strong>PIHAK KEDUA</strong></td>
		</tr>
		<tr>
			<td colspan="4;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4;"><strong>PIHAK PERTAMA</strong> dan <strong>PIHAK KEDUA</strong> selanjutnya disebut sebagai <strong>“PARA PIHAK”</strong></td>
		</tr>
		<tr>
			<td colspan="4;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4;"><strong>PARA PIHAK</strong> bersepakat untuk mengikatkan diri dalam kesepakatan kerja yang tertuang dalam pasal-pasal berikut, dan uraian dalam pasal-pasal selanjutnya dalam Kesepakatan Kerja ini selanjutnya disebut dengan <strong>Perjanjian Kerja Waktu Tertentu.</strong></td>
		</tr>
		<tr>
			<td colspan="4;">Bahwa sesuai ijin dari instansi terkait, Kapolri, Disnaker dan Disperindag, <strong>{NAMA_BISNIS_UNIT}</strong> adalah Badan Usaha Jasa Penyediaan Tenaga Pengamanan, yang dalam bidang usahanya melakukan kerja sama dengan perusahaan lain (pihak ketiga/klien/customer/perusahaan pengguna jasa) yang dalam perjanjian dengan pihak ketiga dilakukan setiap satu tahun sekali, dan untuk tahun berikutnya/selanjutnya belum tentu masih melakukan kerja sama dengan <strong>{NAMA_BISNIS_UNIT}</strong></td>
		</tr>
		<tr>
			<td colspan="4;">Bahwa oleh karena itu kedua belah pihak bersepakat untuk mengadakan Perjanjian <strong>Kerja Waktu Tertentu</strong> (PKWT) dengan ketentuan sebagai berikut:</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 1</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pengikatan Diri dan Jangka Waktu Perjanjian</strong>
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px">Sejak ditandatanganinya Perjanjian Kerja Waktu Tertentu ini, <strong>PIHAK KEDUA</strong> menyatakan:</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;">a.</td>
							<td style="width: 585px;">Mengikatkan diri sebagai <strong>Karyawan {NAMA_BISNIS_UNIT}</strong> di posisi <strong>{JABATAN}</strong> yang sudah memahami tugas dan pekerjaannya secara profesional dan jika dibutuhkan siap untuk bekerja lembur/over time.</td>
						</tr>
						<tr>
							<td>b.</td>
							<td>Tidak lagi terikat dengan Perjanjian Kerja dengan pihak lain.</td>
						</tr>
						<tr>
							<td>c.</td>
							<td>Bersedia ditempatkan di <strong>KOTA SURABAYA</strong> dan selanjutnya bersedia sewaktu-waktu dipindahkan (mutasi/rotasi), ditempatkan dimana saja sesuai kebutuhan, situasi dan kondisi serta kebijakan <strong>PIHAK PERTAMA.</strong></td>
						</tr>
						<tr>
							<td>d.</td>
							<td>Apabila <strong>PIHAK KEDUA</strong> tidak bersedia ditempatkan di perusahaan yang ditunjuk oleh <strong>PIHAK PERTAMA</strong>, maka <strong>PIHAK KEDUA</strong> dinyatakan mengundurkan diri dan tidak mendapatkan kompensasi dalam bentuk apapun dari <strong>PIHAK PERTAMA.</strong></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Perjanjian ini berlaku untuk jangka waktu tertentu terhitung mulai <strong>{KONTRAK_MULAI}</strong> sampai dengan <strong>{KONTRAK_SELESAI}</strong></td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Masa berlaku perjanjian ini dapat diperpanjang atas kesepakatan kedua belah pihak dengan tetap berpedoman pada peraturan/undang-undang yang berlaku.</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 2</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Upah</strong>
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px">Dalam  masa berlakunya perjanjian ini, <strong>PIHAK PERTAMA</strong> setuju untuk memberikan upah kepada <strong>PIHAK KEDUA</strong> dan <strong>PIHAK KEDUA</strong> menyatakan setuju pula atas besarnya nilai upah yang diberikan <strong>PIHAK PERTAMA</strong>.</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Adapun nilai upah yang diberikan <strong>PIHAK PERTAMA</strong> kepada <strong>PIHAK KEDUA</strong> adalah gaji pokok sebesar <strong>Rp {GAJI_POKOK}</strong>.</td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Periode perhitungan gaji (<i>cut off</i>) adalah tanggal 26 – tanggal 25 bulan berikutnya.</td>
		</tr>
		<tr>
			<td>4.</td>
			<td>Pembayaran upah kepada <strong>PIHAK KEDUA</strong> dilakukan pada tanggal <strong>25</strong> tiap bulannya dengan cara transfer bank. Bilamana hari pembayaran upah <strong>PIHAK KEDUA</strong> jatuh pada hari libur atau hari libur resmi yang telah ditetapkan oleh pemerintah, maka pembayaran upah <strong>PIHAK KEDUA</strong> dilakukan pada hari kerja setelah hari libur tersebut berakhir/ mundur.</td>
		</tr>
		<tr>
			<td>5.</td>
			<td><strong>PIHAK PERTAMA</strong> setuju untuk memberikan Tunjangan Hari Raya Keagamaan (THR) sesuai dengan agama yang dianut oleh <strong>PIHAK KEDUA</strong> yang akan dibayarkan menjelang Hari Raya Idul Fitri dengan ketentuan sebagai berikut:</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;">a)</td>
							<td style="width: 585px;">Jika <strong>PIHAK KEDUA</strong> memiliki masa kerja kurang dari 1 (satu) bulan, maka <strong>PIHAK KEDUA</strong> tidak mendapatkan THR</td>
						</tr>
						<tr>
							<td>b)</td>
							<td>Jika <strong>PIHAK KEDUA</strong> telah memiliki masa kerja lebih dari 1 (satu) bulan secara terus menerus tetapi kurang dari 1 (satu) tahun, maka <strong>PIHAK KEDUA</strong> akan menerima THR secara proporsional dengan perhitungan sebagai berikut: {(masa kerja : 12) x gaji pokok)}</td>
						</tr>
						<tr>
							<td>c)</td>
							<td>Jika <strong>PIHAK KEDUA</strong> telah memiliki masa kerja 12 (dua belas) bulan secara terus menerus, maka <strong>PIHAK KEDUA</strong> akan menerima THR sebesar 1 x gaji pokok</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td width="20px">6.</td>
			<td width="605px">Pajak penghasilan (PPh 21) dan pajak lain yang timbul akibat PKWT ini dibayar oleh <strong>PIHAK KEDUA</strong> dan disetor oleh <strong>PIHAK PERTAMA</strong> kepada Kas Negara sesuai dengan perundang-undangan yg berlaku.</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 3</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>BPJS Ketenagakerjaan dan BPJS Kesehatan</strong>
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px"><strong>PIHAK PERTAMA</strong> mendaftarkan <strong>PIHAK KEDUA</strong> dalam asuransi program BPJS Ketenagakerjaan dan BPJS Kesehatan dengan rincian sebagai berikut:</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;"><strong>a.</strong></td>
							<td style="width: 585px;"><strong>BPJS KETENAGAKERJAAN :</strong></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>
								<table table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
									<tbody>
										<tr>
											<td style="width: 25px; text-align: center;">1.</td>
											<td style="width: 560px; text-align: center;">Jaminan Kecelakaan Kerja (JKK)</td>
										</tr>
										<tr>
											<td>2.</td>
											<td>Jaminan Kematian (JKM)</td>
										</tr>
										<tr>
											<td>3.</td>
											<td>Jaminan Hari Tua (JHT)</td>
										</tr>
										<tr>
											<td>4.</td>
											<td>Jaminan Pensiun (JP)</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;"><strong>b.</strong></td>
							<td style="width: 585px;"><strong>BPJS KESEHATAN :</strong></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 4</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Penempatan</strong>
			</td>
		</tr>
		<tr>
			<td style="width: 20px;">1.</td>
			<td style="width: 605px;"><strong>PIHAK PERTAMA</strong> berhak melakukan penempatan kerja dimana saja pada <strong>PIHAK KEDUA</strong> dan penempatan tersebut bersifat sementara karena <strong>PIHAK PERTAMA</strong> sewaktu – waktu dapat melakukan perpindahan kerja kembali pada <strong>PIHAK KEDUA.</strong></td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Skema upah dan benefit yang diterima <strong>PIHAK KEDUA</strong> di penempatan baru menyesuaikan skema upah dan benefit yang telah disepakati antara <strong>PIHAK PERTAMA</strong> dengan klien/<i>customer</i>/perusahaan pengguna jasa.</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 5</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Ketidakhadiran</strong>
			</td>
		</tr>
		<tr>
			<td style="width: 20px;">1.</td>
			<td style="width: 605px;">Jika <strong>PIHAK KEDUA</strong> berhalangan hadir karena sakit, maka yang bersangkutan wajib memberitahukan kepada atasannya sekurang-kurangnya 4 (empat) jam sebelum masuk kerja dan menyerahkan surat keterangan dokter pada saat kembali masuk kerja.</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Bila <strong>PIHAK KEDUA</strong> tidak masuk kerja, maka upah pada hari itu tidak dibayarkan oleh <strong>PIHAK PERTAMA</strong> atau bersifat <i>No work no pay</i>.</td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Apabila <strong>PIHAK KEDUA</strong> berhalangan hadir untuk suatu urusan pribadi atau karena sebab lain yang sangat penting dan mendesak, maka <strong>PIHAK KEDUA</strong> harus mendapatkan persetujuan terlebih dahulu dari atasannya sekurang – kurangnya 1 (satu) hari sebelumnya .</td>
		</tr>
		<tr>
			<td>4.</td>
			<td>Apabila <strong>PIHAK KEDUA</strong> tidak hadir tanpa pemberitahuan atau suatu alasan yang dapat diterima oleh perusahaan selama 5 (lima) hari kerja berturut – turut dan telah dipanggil oleh <strong>PIHAK PERTAMA</strong> secara tertulis sebanyak 2 (dua) kali secara patut, maka <strong>PIHAK KEDUA</strong> dikualifikasikan mengundurkan diri dan perjanjian ini berakhir sebelum habis jangka berlakunya.</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 6</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Tata Tertib dan Syarat – Syarat Kerja</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: justify;" colspan="2;">
				<strong>PIHAK KEDUA</strong> telah menyetujui untuk memenuhi kewajiban dengan jujur, disiplin dan penuh rasa tanggung jawab serta akan patuh atas ketentuan – ketentuan sebagai berikut:
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px">Selalu memperhatikan kepentingan perusahaan dengan sebaik – baiknya, membaca, memahami dan mentaati setiap edaran, pengumuman dan perintah yang telah dikeluarkan oleh pimpinan perusahaan.</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Akan selalu menjaga rahasia perusahaan dan tidak akan membocorkan kepada pihak lain selama berlakunya dan sesudah berakhirnya perjanjian ini. Hal – hal yang bersifat rahasia semata – mata ditentukan oleh <strong>PIHAK PERTAMA</strong>.</td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Berperilaku sopan dan menggalang kerjasama yang baik dengan atasan dan sesama rekan kerja serta menciptakan ketenangan kerja.</td>
		</tr>
		<tr>
			<td>4.</td>
			<td>Menghormati hubungan kerja dengan klien/<i>customer</i>/perusahaan pengguna jasa <strong>PIHAK PERTAMA</strong> dengan cara:</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;">a.</td>
							<td style="width: 585px;">Selalu siap siaga di tempat tugas.</td>
						</tr>
						<tr>
							<td>b.</td>
							<td>Menghormati, menjaga sopan santun/tata krama dan bersikap ramah terhadap pengguna jasa.</td>
						</tr>
						<tr>
							<td>c.</td>
							<td>Mematuhi perintah klien/<i>customer</i>/perusahaan pengguna jasa sepanjang hal ini tidak bertentangan dengan peraturan perusahaan (<strong>PIHAK PERTAMA</strong>) dan atau hukum negara.</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>5.</td>
			<td>Bertindak dan berlaku jujur serta senantiasa menjaga nama baik diri sendiri, nama baik perusahaan dan nama baik klien/<i>customer</i>/perusahaan pengguna jasa.</td>
		</tr>
		<tr>
			<td>6.</td>
			<td>Pada saat bertugas <strong>PIHAK KEDUA</strong> diwajibkan untuk bersikap, berpenampilan sesuai ketentuan sebagai berikut:</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;">a.</td>
							<td style="width: 585px;">Mengenakan pakaian seragam yang telah ditentukan oleh POLRI dan <strong>PIHAK PERTAMA</strong> dalam keadaan bersih dan rapi.</td>
						</tr>
						<tr>
							<td>b.</td>
							<td>Rambut dipotong pendek dan disisir rapi dengan ukuran 2.1.0.</td>
						</tr>
						<tr>
							<td>c.</td>
							<td>Memelihara kebersihan dan keharuman badan.</td>
						</tr>
						<tr>
							<td>d.</td>
							<td>Mengenakan sepatu bersih dan terpelihara dengan baik.</td>
						</tr>
						<tr>
							<td>e.</td>
							<td>Atribut terpelihara dan dipakai dengan benar sesuai ketentuan.</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>7.</td>
			<td>Pada saat bertugas <strong>PIHAK KEDUA</strong> wajib untuk:</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;">a.</td>
							<td style="width: 585px;">Mematuhi semua peraturan dan SOP yang berlaku.</td>
						</tr>
						<tr>
							<td>b.</td>
							<td>Mematuhi <i>schedule</i> / hari kerja dan jam kerja yang berlaku, dimana <strong>PIHAK KEDUA</strong> ditempatkan.</td>
						</tr>
						<tr>
							<td>c.</td>
							<td>Tertib dan sopan menjunjung tinggi kehormatan diri.</td>
						</tr>
						<tr>
							<td>d.</td>
							<td>Tidak bekerja atau mengikatkan diri pada pihak ketiga/pihak lain.</td>
						</tr>
						<tr>
							<td>e.</td>
							<td>Disiplin atau mengikatkan diri dengan ketaatan.</td>
						</tr>
						<tr>
							<td>f.</td>
							<td>Melaksanakan tugas pokok sesuai uraian pekerjaan.</td>
						</tr>
						<tr>
							<td>g.</td>
							<td>Tidak meminta imbalan atau menerima hadiah dalam bentuk apapun untuk pelayanan yang diberikan.</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>8.</td>
			<td>Melaporkan kepada atasan dengan segera apabila terjadi sesuatu hal yang menyimpang atau akan dapat membahayakan diri atau kejadian – kejadian lainnya yang dapat menimbulkan kerugian baik secara langsung maupun tidak langsung bagi klien/<i>customer</i>/perusahaan pengguna jasa maupun perusahaan (<strong>PIHAK PERTAMA</strong>).</td>
		</tr>
		<tr>
			<td>9.</td>
			<td><strong>PIHAK PERTAMA</strong> dibebaskan dari segala tuntutan pembayaran piutang <strong>PIHAK KEDUA</strong> dalam bentuk apapun yang berkaitan dengan PIHAK KETIGA atau pihak lain.</td>
		</tr>
	</tbody>
</table>

<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 7</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Sanksi atas  Pelanggaran Tata Tertib dan Syarat – Syarat Kerja</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: justify;" colspan="2;">
				<strong>PIHAK PERTAMA</strong> akan menjatuhkan sanksi terhadap <strong>PIHAK KEDUA</strong> dalam bentuk teguran lisan atau surat peringatan pertama sampai dengan ketiga atau Pemutusan Hubungan Kerja (PHK), tergantung dari berat ringannya pelanggaran yang dilakukan oleh <strong>PIHAK KEDUA</strong>.
			</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 8</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pengunduran Diri dan Berakhirnya Perjanjian.</strong>
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px"><strong>PARA PIHAK</strong> sepakat untuk mengikatkan diri melalui <strong>Perjanjian  Kerja Waktu Tertentu</strong>  sesuai yang telah disepakati. Namun demikian apabila <strong>PIHAK KEDUA</strong> berniat memutuskan  Perjanjian Kerja sebelum waktunya, surat pemberitahuan harus sudah diserahkan kepada <strong>PIHAK PERTAMA</strong> oleh <strong>PIHAK KEDUA</strong>  selambat lambatnya 30 hari kerja, sebelum tanggal efektif pemutusan hubungan kerja.</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Perjanjian kerja  ini akan berakhir dengan sendirinya karena satu atau lebih sebab sebagai berikut:</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;">a.</td>
							<td style="width: 585px;">Masa berlakunya perjanjian ini telah berakhir dan salah satu pihak tidak bersedia memperpanjang.</td>
						</tr>
						<tr>
							<td>b.</td>
							<td>Apabila <strong>PIHAK KEDUA</strong> melakukan pelanggaran baik pelanggaran sejenis atau tidak dan telah diberi 3 (tiga) kali surat peringatan namun masih tidak mau memperbaiki diri.</td>
						</tr>
						<tr>
							<td>c.</td>
							<td>Apabila ikatan kerja antara <strong>PIHAK PERTAMA</strong> dengan klien/<i>customer</i>/perusahaan pengguna jasa habis masa berlakunya atau tidak bekerjasama kembali.</strong></td>
						</tr>
						<tr>
							<td>d.</td>
							<td>Apabila untuk kantor cabang klien/kustomer/pengguna jasa dinyatakan tutup.</td>
						</tr>
						<tr>
							<td>e.</td>
							<td>Apabila secara medis <strong>PIHAK KEDUA</strong> dinyatakan tidak sehat oleh dokter untuk bekerja sesuai  ketentuan perusahaan.</td>
						</tr>
						<tr>
							<td>f.</td>
							<td>Apabila <strong>PIHAK KEDUA</strong> dinilai kurang perform dalam kinerja oleh <strong>PIHAK PERTAMA</strong> maupun klien/<i>customer</i>/perusahaan pengguna jasa <strong>PIHAK PERTAMA</strong>.</td>
						</tr>
						<tr>
							<td>g.</td>
							<td>Apabila  salah satu pihak mengakhiri lebih awal sebelum perjanjian kontrak ini berakhir.</td>
						</tr>
						<tr>
							<td>h.</td>
							<td>Apabila belum ada kebutuhan personel atau pengurangan personel oleh klien/<i>customer</i>/perusahaan pengguna jasa.</td>
						</tr>
						<tr>
							<td>i.</td>
							<td>Apabila ternyata <strong>PIHAK KEDUA</strong> memberikan keterangan palsu atau tidak sesuai dengan kenyataan pada formulir/surat lamaran kerja.</td>
						</tr>
						<tr>
							<td>j.</td>
							<td>Apabila <strong>PIHAK KEDUA</strong> melakukan pelanggaran berat antara lain:</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>
								<table style="width: 100%;">
									<tbody>
										<tr>
											<td style="width: 20px;">1)</td>
											<td style="width: 565px;">Membocorkan rahasia perusahaan kepada pihak lain.</td>
										</tr>
										<tr>
											<td>2)</td>
											<td>Mencemarkan nama baik atasan atau nama baik perusahaan.</td>
										</tr>
										<tr>
											<td>3)</td>
											<td>Mengancam atau menganiaya secara fisik atau mental, menghina secara kasar pada atasan, teman sekerja atau klien.</td>
										</tr>
										<tr>
											<td>4)</td>
											<td>Penipuan, pencurian dan penggelapan uang/barang milik perusahaan, teman sekerja, teman pengusaha atau milik klien.</td>
										</tr>
										<tr>
											<td>5)</td>
											<td>Dengan ceroboh atau dengan sengaja merusak atau menyuruh merusak barang – barang milik perusahaan/klien.</td>
										</tr>
										<tr>
											<td>6)</td>
											<td>Meninggalkan tempat bekerja pada jam kerja tanpa ijin selama 5 (lima) hari kerja berturut – turut.</td>
										</tr>
										<tr>
											<td>7)</td>
											<td>Menimbulkan kerusuhan di tempat kerja.</td>
										</tr>
										<tr>
											<td>8)</td>
											<td>Dengan ceroboh atau sengaja merusak atau membiarkan diri atau teman sekerjanya dalam bahaya.</td>
										</tr>
										<tr>
											<td>9)</td>
											<td>Melakukan perbuatan asusila  di lingkungan perusahaan atau klien.</td>
										</tr>
										<tr>
											<td>10)</td>
											<td>Mabuk, minum minuman yang dapat memabukkan, madat, memakai obat bius, menyalahgunakan obat – obat terlarang atau obat perangsang lainnya, perjudian di tempat kerja, pemakai dan atau pengedar Narkoba.</td>
										</tr>
										<tr>
											<td>11)</td>
											<td><strong>PIHAK KEDUA</strong> ditahan oleh yang berwajib sebagai tersangka.</td>
										</tr>
										<tr>
											<td>12)</td>
											<td>Tidur saat bertugas.</td>
										</tr>
										<tr>
											<td>13)</td>
											<td>Membantah perintah atasan.</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td>k.</td>
							<td>Apabila secara hukum status/keberadaan perusahaan yang ditunjuk <strong>PIHAK PERTAMA</strong> sebagai tempat tugas <strong>PIHAK KEDUA</strong> dinyatakan tidak layak/tidak dapat lagi menjalankan fungsinya sebagai perusahaan.</td>
						</tr>
						<tr>
							<td>l.</td>
							<td>Apabila ikatan kerja antara <strong>PIHAK PERTAMA</strong> dengan klien/<i>customer</i>/perusahaan pengguna jasa habis masa berlakunya dan atau diakhiri sepihak dari klien/<i>customer</i>/perusahaan pengguna jasa.</td>
						</tr>
						<tr>
							<td>m.</td>
							<td>Apabila secara medis <strong>PIHAK KEDUA</strong> dinyatakan tidak sehat oleh dokter untuk bekerja sesuai  ketentuan perusahaan.</td>
						</tr>
						<tr>
							<td>n.</td>
							<td><strong>PIHAK KEDUA</strong> tidak melaksanakan atau tidak mematuhi SOP.</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 9</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>FORCE MAJEUR</strong>
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px">Keadaan memaksa (<i>force majeur</i>) adalah keadaan yang terjadi di luar kekuasaan kedua belah PIHAK yang mengakibatkan tertundanya atau tidak dapat terlaksananya kewajiban-kewajiban sesuai dengan surat kontrak, yang dicakup dalam <i>force majeur</i> antara lain : kerusakan fasilitas, bencana alam, kebakaran, huru-hara, perang dan gangguan-gangguan teknis lainnya di luar kemampuan kedua belah PIHAK.</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Ketika terjadinya <i>force majeur</i>, maka PIHAK yang mengalami harus memberitahu pada PIHAK lain, selambat-lambatnya satu minggu terhitung sejak terjadinya <i>force majeur.</i></td>
		</tr>
		<tr>
			<td>3.</td>
			<td><strong>PIHAK PERTAMA</strong> tidak mempunyai kewajiban untuk melakukan pembayaran pada <strong>PIHAK KEDUA</strong> apabila terjadi <i>force majeur.</i></td>
		</tr>
		<tr>
			<td>4.</td>
			<td><strong>PIHAK KEDUA</strong> juga bersedia dan sepakat tidak akan menuntut upah apabila terjadi <i>force majeur.</i></td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 10</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Ketentuan Lain-lain</strong>
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px"><strong>PIHAK KEDUA</strong> berkewajiban mengganti kerusakan material atau kerugian finansial yang diderita klien/<i>customer</i>/pengguna jasa <strong>PIHAK PERTAMA</strong> sebagai akibat kelalaian atau kecerobohan yang dilakukan <strong>PIHAK KEDUA</strong>.</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>PKWT ini hanya dapat dirubah atau direvisi berdasarkan kesepakatan tertulis antara <strong>PIHAK PERTAMA</strong> dan <strong>PIHAK KEDUA.</strong></td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 11</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Penutup</strong>
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px">Perjanjian Kerja Waktu Tertentu ini dibuat oleh kedua belah PIHAK atas dasar <strong>SUKARELA</strong>, tidak ada unsur paksaan atau penekanan oleh PIHAK manapun, telah dibaca secara seksama oleh kedua belah PIHAK dan telah dimengerti dan dipahami.</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Jika terdapat hal – hal yang kurang jelas atau belum cukup diatur di dalam Perjanjian Kerja Waktu Tertentu ini, maka hal itu akan dibicarakan oleh kedua belah PIHAK secara musyawarah untuk mencapai mufakat.</td>
		</tr>
		<tr>
			<td>3.</td>
			<td><strong>PIHAK KEDUA</strong> menyadari sepenuhnya bahwa pekerjaan yang diterima oleh <strong>PIHAK PERTAMA</strong> dari klien/<i>customer</i>/pengguna jasa adalah pekerjaan yang bersifat sekali selesai, sementara, untuk waktu tertentu saja. Oleh sebab itu  berdasarkan Perjanjian Kerja Waktu Tertentu ini <strong>PIHAK KEDUA</strong>  sepakat dan setuju serta telah menyadari tidak akan menuntut/memberikan tuntutan pada <strong>PIHAK PERTAMA</strong> berupa apapun, (material / imaterial) termasuk pembayaran sisa kontrak, kompensasi PKWT, uang pesangon, uang penghargaan, uang penggantian hak, uang pisah, tali asih, ganti rugi, dan lain-lain serta hal – hal yang berhubungan dengan status pemberian kerja  termasuk tuntutan untuk menjadi karyawan tetap baik karena hubungan kerja berakhir sesuai dengan  jangka waktu perjanjian ini atau berakhirnya hubungan kerja karena pemutusan perjanjian kerja sebelum perjanjian kerja ini berakhir. Dan dengan demikian <strong>PIHAK KEDUA</strong> tetap sanggup bekerja sebagai karyawan PKWT, hanya bekerja untuk waktu tertentu saja, sebagaimana yang telah ditetapkan dan disepakati dalam Perjanjian Kerja Waktu Tertentu ini.</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr><td colspan="2">Ditandatangani di Surabaya</td></tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td style="width: 50%; text-align: center;">PIHAK PERTAMA</td>
			<td style="width: 50%; text-align: center;">PIHAK KEDUA</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 50%; text-align: center;">
				<strong>
					<u>MIFTAKHUL ARIF</u>
				</strong>
			</td>
			<td style="width: 50%; text-align: center;">
				<strong>{NAMA_LENGKAP}</strong>
			</td>
		</tr>
		<tr>
			<td style="width: 50%; text-align: center;">
				<strong>Kuasa Direksi</strong>
			</td>
			<td style="width: 50%; text-align: center;">&nbsp;</td>
		</tr>
	</tbody>
</table>
<p style="text-align: center; font-family: 'Arial'; line-height: 1;">
	<span style="font-size: 12pt;">
		<strong>
			<u>PERJANJIAN MITRA KERJA WAKTU TERTENTU</u>
		</strong>
	</span>
	<br />
	<span style="font-size: 12pt;">
		<strong>
			<u>NOMOR : {NOMOR_KONTRAK}</u>
		</strong>
	</span>
</p>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td colspan="4;">Yang bertanda tangan dibawah ini kami, masing-masing :</td>
		</tr>
		<tr>
			<td colspan="4;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 20px;">1.</td>
			<td style="width: 200px;">Nama Lengkap</td>
			<td style="width: 15px;">:</td>
			<td style="width: 390px;">
				<strong>MIFTAKHUL ARIF</strong>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Jabatan</td>
			<td>:</td>
			<td>HR &amp; GM HC & IT</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Instansi</td>
			<td>:</td>
			<td>{NAMA_BISNIS_UNIT}</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Alamat Lengkap</td>
			<td>:</td>
			<td>{ALAMAT_BISNIS_UNIT}</td>
		</tr>
		<tr>
			<td colspan="4;">Bertindak untuk dan atas nama kuasa direksi <strong>{NAMA_BISNIS_UNIT}</strong> selanjutnya disebut sebagai <strong>PIHAK PERTAMA</strong></td>
		</tr>
		<tr>
			<td colspan="4;">&nbsp;</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Nama Lengkap</td>
			<td>:</td>
			<td>
				<strong>{NAMA_LENGKAP}</strong>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Jenis Kelamin</td>
			<td>:</td>
			<td>{JENIS_KELAMIN}</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Tempat/Tanggal Lahir</td>
			<td>:</td>
			<td>{TEMPAT_LAHIR}, {TANGGAL_LAHIR}</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>No. KTP</td>
			<td>:</td>
			<td>{NIK}</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Alamat Sesuai KTP</td>
			<td>:</td>
			<td>{ALAMAT_KTP}</td>
		</tr>
		<tr>
			<td colspan="4;">Bertindak untuk dan atas nama pribadi dan diri sendiri, selanjutnya disebut sebagai <strong>PIHAK KEDUA</strong></td>
		</tr>
		<tr>
			<td colspan="4;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4;">Bahwa <strong>berdasar order, adanya suatu pekerjaan yang bersifat sementara</strong> dari <strong>{NAMA_SITE}</strong> dengan <strong>PIHAK PERTAMA</strong>, yang dituangkan dalam surat perjanjian kerjasama / surat perintah kerja (SPK), maka <strong>PIHAK PERTAMA</strong> dan <strong>PIHAK KEDUA secara sukarela</strong> mengikatkan diri untuk mengerjakan tugas / pekerjaan tersebut dengan Perjanjian Kerja Waktu Tertentu ini.</td>
		</tr>
		<tr>
			<td colspan="4;">Bahwa <strong>PIHAK PERTAMA</strong> dan <strong>PIHAK KEDUA</strong> menyadari betul Perjanjian Kerja Waktu Tertentu ini <strong>sangat tergantung dari kondisi order / SPK,</strong> sehingga sesuai yang tersirat pada ketentuan pasal 9 ayat (3)  Peraturan Pemerintah Republik Indonesia Nomor 35 Tahun 2021 yang menyatakan apabila pekerjaan diselesaikan lebih cepat maka PKWT ini putus demi hukum pada saat selesainya pekerjaan.</td>
		</tr>
		<tr>
			<td colspan="4;">Atas dasar uraian diatas tersebut maka kedua belah pihak sepakat untuk mengadakan Perjanjian Kerja Waktu Tertentu (PKWT) dengan syarat dan ketentuan sebagai berikut :</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 1</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pengikatan Diri dan Jangka Waktu Perjanjian</strong>
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px">Sejak ditandatanganinya Perjanjian Kerja Waktu Tertentu ini, <strong>PIHAK KEDUA</strong> menyatakan:</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;">a.</td>
							<td style="width: 585px;">Mengikatkan diri sebagai <strong>Karyawan {NAMA_BISNIS_UNIT}</strong> di posisi <strong>{JABATAN}</strong> yang sudah memahami tugas dan pekerjaannya secara profesional dan jika dibutuhkan siap untuk bekerja lembur/over time.</td>
						</tr>
						<tr>
							<td>b.</td>
							<td>Tidak lagi terikat dengan Perjanjian Kerja dengan pihak lain.</td>
						</tr>
						<tr>
							<td>c.</td>
							<td>Bersedia ditempatkan di <strong>KOTA SURABAYA</strong> dan selanjutnya bersedia sewaktu-waktu dipindahkan (mutasi/rotasi), ditempatkan dimana saja sesuai kebutuhan, situasi dan kondisi serta kebijakan <strong>PIHAK PERTAMA.</strong></td>
						</tr>
						<tr>
							<td>d.</td>
							<td>Apabila <strong>PIHAK KEDUA</strong> tidak bersedia ditempatkan di perusahaan yang ditunjuk oleh <strong>PIHAK PERTAMA</strong>, maka <strong>PIHAK KEDUA</strong> dinyatakan mengundurkan diri dan tidak mendapatkan kompensasi dalam bentuk apapun dari <strong>PIHAK PERTAMA.</strong></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Perjanjian ini terhitung mulai tanggal <strong>{KONTRAK_MULAI}</strong> sampai dengan <strong>{KONTRAK_SELESAI}</strong></td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 2</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Upah</strong>
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px">Dalam  masa berlakunya perjanjian ini, upah yang diberikan <strong>PIHAK PERTAMA</strong> kepada <strong>PIHAK KEDUA</strong> adalah sebagai berikut :</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;">a.</td>
							<td style="width: 200px;">Gaji Pokok</td>
							<td style="width: 20px;">:</td>
							<td style="width: 365px;"><strong>Rp 4.375.479,19 per bulan</strong></td>
						</tr>
						<tr>
							<td style="width: 20px;">b.</td>
							<td style="width: 200px;">Tunjangan</td>
							<td style="width: 20px;">:</td>
							<td style="width: 365px;"><strong>-</strong></td>
						</tr>
						<tr>
							<td style="width: 20px;">c.</td>
							<td style="width: 200px;">Tunjangan</td>
							<td style="width: 20px;">:</td>
							<td style="width: 365px;"><strong>-</strong></td>
						</tr>
						<tr>
							<td style="width: 20px;">d.</td>
							<td style="width: 200px;">Tunjangan</td>
							<td style="width: 20px;">:</td>
							<td style="width: 365px;"><strong>-</strong></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Upah <strong>PIHAK KEDUA</strong> dapat berubah sesuai dengan penempatan karyawan.</td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Periode perhitungan gaji (<i>cut off</i>) adalah tanggal 01-30/31.</td>
		</tr>
		<tr>
			<td>4.</td>
			<td>Pembayaran upah kepada <strong>PIHAK KEDUA</strong> dilakukan pada tanggal <strong>10</strong> tiap bulannya. Bilamana hari pembayaran upah <strong>PIHAK KEDUA</strong> jatuh pada hari libur atau hari libur resmi yang telah ditetapkan oleh pemerintah, maka pembayaran upah <strong>PIHAK KEDUA</strong> dilakukan pada hari kerja setelah hari libur tersebut berakhir/ mundur.</td>
		</tr>
		<tr>
			<td>5.</td>
			<td>Bila <strong>PIHAK KEDUA</strong> tidak masuk kerja, maka upah pada hari itu tidak dibayarkan oleh <strong>PIHAK PERTAMA</strong> atau bersifat <i>no work no pay.</i></td>
		</tr>
		<tr>
			<td width="20px">6.</td>
			<td width="605px"><strong>PIHAK PERTAMA</strong> setuju untuk memberikan Tunjangan Hari Raya Keagamaan (THR) sesuai dengan agama yang dianut oleh <strong>PIHAK KEDUA</strong> yang akan dibayarkan menjelang Hari Raya Idul Fitri dengan ketentuan sebagai berikut:</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;">a.</td>
							<td style="width: 585px;">Jika <strong>PIHAK KEDUA</strong> memiliki masa kerja kurang dari 1 (satu) bulan, maka <strong>PIHAK KEDUA</strong> tidak mendapatkan THR</td>
						</tr>
						<tr>
							<td>b.</td>
							<td>Jika <strong>PIHAK KEDUA</strong> telah memiliki masa kerja lebih dari 1 (satu) bulan secara terus menerus tetapi kurang dari 1 (satu) tahun, maka <strong>PIHAK KEDUA</strong> akan menerima THR secara proporsional dengan perhitungan sebagai berikut: {(masa kerja : 12) x gaji pokok)}</td>
						</tr>
						<tr>
							<td>c.</td>
							<td>Jika <strong>PIHAK KEDUA</strong> telah memiliki masa kerja 12 (dua belas) bulan secara terus menerus, maka <strong>PIHAK KEDUA</strong> akan menerima THR sebesar 1 x gaji pokok</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td width="20px">7.</td>
			<td width="605px">Pajak penghasilan (PPh 21) dan pajak lain yang timbul akibat PKWT ini dibayar oleh <strong>PIHAK KEDUA</strong> dan disetor oleh <strong>PIHAK PERTAMA</strong> kepada Kas Negara sesuai dengan perundang-undangan yg berlaku.</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 4</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>BPJS Ketenagakerjaan dan BPJS Kesehatan</strong>
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px"><strong>PIHAK PERTAMA</strong> mendaftarkan <strong>PIHAK KEDUA</strong> dalam asuransi program BPJS Ketenagakerjaan dan BPJS Kesehatan dengan rincian sebagai berikut:</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;"><strong>a.</strong></td>
							<td style="width: 585px;"><strong>BPJS KETENAGAKERJAAN :</strong></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>
								<table border="1">
									<tbody>
										<tr>
											<td style="width: 25px; text-align: center;">
												<strong>No.</strong>
											</td>
											<td style="width: 360px; text-align: center;">
												<strong>Program BPJS Ketenagakerjaan</strong>
											</td>
											<td style="width: 100px; text-align: center;">
												<strong>Perusahaan </strong>
											</td>
											<td style="width: 100px; text-align: center;">
												<strong>Karyawan</strong>
											</td>
										</tr>
										<tr>
											<td style="text-align: center;">1.</td>
											<td style="text-align: left;">Jaminan Kecelakaan Kerja (JKK)</td>
											<td style="text-align: center;">0.24%</td>
											<td style="text-align: center;">-</td>
										</tr>
										<tr>
											<td style="text-align: center;">2.</td>
											<td style="text-align: left;">Jaminan Kematian (JKM)</td>
											<td style="text-align: center;">0.30%</td>
											<td style="text-align: center;">-</td>
										</tr>
										<tr>
											<td style="text-align: center;">3.</td>
											<td style="text-align: left;">Jaminan Hari Tua (JHT)</td>
											<td style="text-align: center;">3.7%</td>
											<td style="text-align: center;">2%</td>
										</tr>
										<tr>
											<td style="text-align: center;">4.</td>
											<td style="text-align: left;">Jaminan Pensiun (JP)</td>
											<td style="text-align: center;">2%</td>
											<td style="text-align: center;">1%</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;"><strong>b.</strong></td>
							<td style="width: 585px;"><strong>BPJS KESEHATAN :</strong></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>
								<table border="1">
									<tbody>
										<tr>
											<td style="width: 25px; text-align: center;">
												<strong>No.</strong>
											</td>
											<td style="width: 360px; text-align: center;">
												<strong>Program BPJS Kesehatan</strong>
											</td>
											<td style="width: 100px; text-align: center;">
												<strong>Perusahaan </strong>
											</td>
											<td style="width: 100px; text-align: center;">
												<strong>Karyawan</strong>
											</td>
										</tr>
										<tr>
											<td style="text-align: center;">1.</td>
											<td style="text-align: left;">Jaminan Kesehatan</td>
											<td style="text-align: center;">4%</td>
											<td style="text-align: center;">1%</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Iuran BPJS Ketenagakerjaan dan BPJS Kesehatan dihitung berdasarkan UMK.</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 4</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Ketidakhadiran</strong>
			</td>
		</tr>
		<tr>
			<td style="width: 20px;">1.</td>
			<td style="width: 605px;">Jika <strong>PIHAK KEDUA</strong> berhalangan hadir karena sakit, maka yang bersangkutan wajib memberitahukan kepada atasannya sekurang-kurangnya 4 (empat) jam sebelum masuk kerja dan menyerahkan surat keterangan dokter pada saat kembali masuk kerja.</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Apabila <strong>PIHAK KEDUA</strong> berhalangan hadir untuk suatu urusan pribadi atau karena sebab lain yang sangat penting dan mendesak, maka <strong>PIHAK KEDUA</strong> harus mendapatkan persetujuan terlebih dahulu dari atasannya sekurang – kurangnya 1 (satu) hari sebelumnya.</td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Apabila <strong>PIHAK KEDUA</strong> tidak hadir tanpa pemberitahuan atau suatu alasan yang dapat diterima, dinyatakan mangkir oleh perusahaan. Jika selama 5 (lima) hari kerja berturut – turut dan telah dipanggil secara layak sebanyak 2 (dua) kali, maka <strong>PIHAK KEDUA</strong> dikualifikasikan mengundurkan diri dan perjanjian ini batal demi hukum, dan bagi <strong>PIHAK KEDUA</strong> tetap dikenakan sanksi/kewajiban sesuai ketentuan <strong>PIHAK PERTAMA</strong> dan tanpa kompensasi apapun.</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 5</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Tata Tertib dan Syarat – Syarat Kerja</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: justify;" colspan="2;">
				<strong>PIHAK KEDUA</strong> telah menyetujui untuk memenuhi kewajiban dengan jujur, profesional, disiplin dan penuh rasa tanggung jawab serta patuh atas ketentuan sebagai berikut :
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px">Akan selalu menjaga rahasia perusahaan dan pengguna jasa, dan tidak akan membocorkan kepada PIHAK lain selama berlakunya dan sesudah berakhirnya perjanjian ini. Hal – hal yang bersifat rahasia semata – mata ditentukan oleh <strong>PIHAK PERTAMA.</strong></td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Menghormati hubungan kerja dengan perusahaan pengguna jasa <strong>PIHAK PERTAMA</strong> dengan cara:</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;">a.</td>
							<td style="width: 585px;">Selalu siap siaga di tempat tugas, di saat jam tugas maupun di luar jam tugas.</td>
						</tr>
						<tr>
							<td>b.</td>
							<td>Menghormati, menjaga sopan santun/tata krama dan bersikap ramah terhadap pengguna jasa.</td>
						</tr>
						<tr>
							<td>c.</td>
							<td>Mematuhi perintah perusahaan pengguna jasa sepanjang tidak bertentangan dengan peraturan perusahaan <strong>PIHAK PERTAMA</strong> dan atau hukum negara.</td>
						</tr>
						<tr>
							<td>d.</td>
							<td>Menaati dan patuh terhadap peraturan perusahaan pengguna jasa.</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Bertindak dan berlaku jujur serta senantiasa menjaga nama baik diri sendiri, nama baik perusahaan dan nama baik pengguna jasa.</td>
		</tr>
		<tr>
			<td>4.</td>
			<td>Pada saat bertugas <strong>PIHAK KEDUA</strong> wajib untuk :</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;">a.</td>
							<td style="width: 585px;">Disiplin dan mematuhi semua peraturan dan SOP yang berlaku.</td>
						</tr>
						<tr>
							<td>b.</td>
							<td>Mematuhi <i>schedule</i> / hari kerja dan jam kerja yang berlaku, dimana <strong>PIHAK KEDUA</strong> ditempatkan.</td>
						</tr>
						<tr>
							<td>c.</td>
							<td>Tidak diperbolehkan membawa <i>handphone</i>, rokok dan korek api kecuali ada peraturan perusahaan pengguna jasa yang memperbolehkan.</td>
						</tr>
						<tr>
							<td>d.</td>
							<td>Tertib dan sopan menjunjung tinggi kehormatan diri.</td>
						</tr>
						<tr>
							<td>e.</td>
							<td>Tidak bekerja atau mengikatkan diri pada PIHAK ketiga/PIHAK lain.</td>
						</tr>
						<tr>
							<td>f.</td>
							<td>Melaksanakan tugas pokok dan tugas kerja sesuai ketentuan.</td>
						</tr>
						<tr>
							<td>g.</td>
							<td>Tidak meminta imbalan atau menerima hadiah dalam bentuk apapun untuk pelayanan yang diberikan.</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>5.</td>
			<td>Melaporkan kepada atasan dengan segera apabila terjadi sesuatu hal yang menyimpang atau akan dapat membahayakan diri atau kejadian –kejadian lainnya yang dapat menimbulkan kerugian baik secara langsung maupun tidak langsung bagi perusahaan pengguna jasa maupun perusahaan <strong>PIHAK PERTAMA.</strong></td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 6</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pengunduran Diri dan Berakhirnya Perjanjian.</strong>
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px">Kedua belah PIHAK sepakat untuk mengikatkan diri melalui Perjanjian  Kerja Waktu Tertentu  sesuai yang telah disepakati. Namun demikian apabila <strong>PIHAK KEDUA</strong> berniat memutuskan  Perjanjian Kerja sebelum waktunya, surat pemberitahuan harus sudah diserahkan kepada <strong>PIHAK PERTAMA</strong> oleh <strong>PIHAK KEDUA</strong>  selambat lambatnya 30 hari kerja, sebelum tanggal efektif pemutusan hubungan kerja.</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Perjanjian kerja  ini akan berakhir dengan sendirinya karena satu atau lain hal sebagai berikut :</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px;">a.</td>
							<td style="width: 585px;">Masa berlakunya perjanjian ini telah berakhir.</td>
						</tr>
						<tr>
							<td>b.</td>
							<td>Apabila ternyata <strong>PIHAK KEDUA</strong> memberikan keterangan palsu atau tidak sesuai dengan kenyataan pada formulir/surat lamaran kerja.</td>
						</tr>
						<tr>
							<td>c.</td>
							<td>Apabila <strong>PIHAK KEDUA</strong> melakukan pelanggaran berat sesuai ketentuan <strong>PIHAK PERTAMA.</strong></td>
						</tr>
						<tr>
							<td>d.</td>
							<td>Apabila ikatan kerja antara <strong>PIHAK PERTAMA</strong> dengan klien/customer/pengguna jasa habis masa berlakunya dan tidak diperpanjang kembali.</td>
						</tr>
						<tr>
							<td>e.</td>
							<td>Apabila untuk kantor cabang klien/customer/pengguna jasa dinyatakan tutup.</td>
						</tr>
						<tr>
							<td>f.</td>
							<td>Apabila secara medis <strong>PIHAK KEDUA</strong> dinyatakan tidak sehat oleh dokter untuk bekerja sesuai  ketentuan perusahaan.</td>
						</tr>
						<tr>
							<td>g.</td>
							<td><strong>PIHAK KEDUA</strong> melakukan pelanggaran lagi setelah diberikan Surat Peringatan yang terakhir (SP III).</td>
						</tr>
						<tr>
							<td>h.</td>
							<td>Apabila <strong>PIHAK KEDUA</strong> dinilai kurang perform dalam kinerja oleh <strong>PIHAK PERTAMA</strong> maupun klien/customer/pengguna jasa <strong>PIHAK PERTAMA.</strong></td>
						</tr>
						<tr>
							<td>i.</td>
							<td>Apabila  salah satu pihak mengakhiri lebih awal sebelum perjanjian kontrak ini berakhir.</td>
						</tr>
						<tr>
							<td>j.</td>
							<td>Apabila belum ada / pengurangan kebutuhan tenaga kerja oleh klien/customer/pengguna jasa.</td>
						</tr>
						<tr>
							<td>k.</td>
							<td><strong>PIHAK KEDUA</strong> melanggar larangan sebagian atau keseluruhan sebagaimana tercantum dalam PKWT ini, seperti halnya dan tidak terbatas pada pelanggaran tata tertib kerja, mengabaikan ketentuan tentang integritas dan keamanan informasi yang berlaku di perusahaan <strong>PIHAK PERTAMA</strong> maupun klien/customer/pengguna jasa <strong>PIHAK PERTAMA</strong>, seperti halnya :</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>
								<table style="width: 100%;">
									<tbody>
										<tr>
											<td style="width: 20px;">&ndash;</td>
											<td style="width: 565px;">melakukan penipuan, pencurian, atau pengelapan baik berupa data, barang, uang, dan harta benda lainnya milik <strong>PIHAK PERTAMA</strong> atau klien/customer/pengguna jasa <strong>PIHAK PERTAMA</strong></td>
										</tr>
										<tr>
											<td>&ndash;</td>
											<td>memberi keterangan palsu atau yang dipalsukan sehingga merugikan <strong>PIHAK PERTAMA</strong> dan klien/customer/pengguna jasa <strong>PIHAK PERTAMA</strong></td>
										</tr>
										<tr>
											<td>&ndash;</td>
											<td>mabuk, meminum minuman keras yang memabukan, memakai dan atau mengedarkan narkotika, psikotropika, dan zat adiktif lainnya</td>
										</tr>
										<tr>
											<td>&ndash;</td>
											<td>melakukan  perbuatan asusila, amoral, kegiatan perjudian</td>
										</tr>
										<tr>
											<td>&ndash;</td>
											<td>membujuk teman sekerja atau <strong>PIHAK PERTAMA</strong> atau klien/customer/pengguna jasa <strong>PIHAK PERTAMA</strong> untuk melakukan perbuatan yang bertentangan dengan peraturan perundang-undangan</td>
										</tr>
										<tr>
											<td>&ndash;</td>
											<td>dengan ceroboh atau sengaja merusak atau membiarkan dalam keadaan bahaya barang milik <strong>PIHAK PERTAMA</strong> atau klien/customer/pengguna jasa <strong>PIHAK PERTAMA</strong> yang menimbulkan kerugian</td>
										</tr>
										<tr>
											<td>&ndash;</td>
											<td>dengan ceroboh atau sengaja membiarkan temen sekerja atau <strong>PIHAK PERTAMA</strong> atau klien/customer/pengguna jasa <strong>PIHAK PERTAMA</strong> dalam keadaan bahaya di tempat kerja</td>
										</tr>
										<tr>
											<td>&ndash;</td>
											<td>membongkar atau membocorkan rahasia perusahaan <strong>PIHAK PERTAMA</strong> atau klien/customer/pengguna jasa <strong>PIHAK PERTAMA</strong> yang seharusnya dirahasiakan kecuali untuk kepentingan umum</td>
										</tr>
										<tr>
											<td>&ndash;</td>
											<td>melakukan perbuatan lainnya yang diancam pidana.</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 7</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>FORCE MAJEUR</strong>
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px">Keadaan memaksa (force majeur) adalah keadaan yang terjadi di luar kekuasaan kedua belah PIHAK yang mengakibatkan tertundanya atau tidak dapat terlaksananya kewajiban-kewajiban sesuai dengan surat kontrak, yang dicakup dalam force majeur antara lain : kerusakan fasilitas, bencana alam, kebakaran, huru-hara, perang dan gangguan-gangguan teknis lainnya di luar kemampuan kedua belah PIHAK.</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Ketika terjadinya <i>force majeur</i>, maka PIHAK yang mengalami harus memberitahu pada PIHAK lain, selambat-lambatnya satu minggu terhitung sejak terjadinya <i>force majeur.</i></td>
		</tr>
		<tr>
			<td>3.</td>
			<td><strong>PIHAK PERTAMA</strong> tidak mempunyai kewajiban untuk melakukan pembayaran pada <strong>PIHAK KEDUA</strong> apabila terjadi <i>force majeur.</i></td>
		</tr>
		<tr>
			<td>4.</td>
			<td><strong>PIHAK KEDUA</strong> juga bersedia dan sepakat tidak akan menuntut upah apabila terjadi <i>force majeur.</i></td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 8</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Ketentuan Lain-lain</strong>
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px"><strong>PIHAK KEDUA</strong> berkewajiban mengganti kerusakan material atau kerugian finansial yang diderita klien/customer/pengguna jasa <strong>PIHAK PERTAMA</strong> sebagai akibat kelalaian atau kecerobohan yang dilakukan <strong>PIHAK KEDUA</strong>. <strong>PIHAK PERTAMA</strong> berhak memperhitungkannya dengan memotong upah bulanan <strong>PIHAK KEDUA</strong> hingga penggantian tersebut lunas.</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>PKWT ini hanya dapat dirubah atau direvisi berdasarkan kesepakatan tertulis antara <strong>PIHAK PERTAMA</strong> dan <strong>PIHAK KEDUA.</strong></td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Bahwa apabila perjanjian kerjasama antara <strong>{NAMA_SITE}</strong> dengan <strong>{NAMA_BISNIS_UNIT}</strong> tidak diperpanjang lagi, dan <strong>PIHAK KEDUA</strong> masih melakukan pekerjaan yang tetap sama di <strong>{NAMA_SITE}</strong>, maka <strong>PIHAK KEDUA</strong> akan mendapatkan pengalihan perlindungan hak-hak dari perusahaan penyedia jasa pengganti yang ditunjuk oleh <strong>{NAMA_SITE}</strong>.</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>&nbsp;</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Pasal &ndash; 9</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				<strong>Penutup</strong>
			</td>
		</tr>
		<tr>
			<td width="20px">1.</td>
			<td width="605px">Perjanjian Kerja Waktu Tertentu ini dibuat oleh kedua belah PIHAK atas dasar <strong>SUKARELA</strong>, tidak ada unsur paksaan atau penekanan oleh PIHAK manapun, telah dibaca secara seksama oleh kedua belah PIHAK dan telah dimengerti dan dipahami.</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Jika terdapat hal – hal yang kurang jelas atau belum cukup diatur di dalam Perjanjian Kerja Waktu Tertentu ini, maka hal itu akan dibicarakan oleh kedua belah PIHAK secara musyawarah untuk mencapai mufakat.</td>
		</tr>
		<tr>
			<td>3.</td>
			<td><strong>PIHAK KEDUA</strong> menyadari sepenuhnya bahwa pekerjaan yang diterima oleh <strong>PIHAK PERTAMA</strong> dari klien/customer/pengguna jasa adalah pekerjaan yang bersifat sekali selesai, sementara, untuk waktu tertentu saja. Oleh sebab itu  berdasarkan Perjanjian Kerja Waktu Tertentu ini <strong>PIHAK KEDUA</strong>  sepakat dan setuju serta telah menyadari tidak akan menuntut/memberikan tuntutan pada <strong>PIHAK PERTAMA</strong> berupa apapun, (material / imaterial) termasuk pembayaran sisa kontrak, kompensasi PKWT, uang pesangon, uang penghargaan, uang penggantian hak, uang pisah, tali asih, ganti rugi, dan lain-lain serta hal – hal yang berhubungan dengan status pemberian kerja  termasuk tuntutan untuk menjadi karyawan tetap baik karena hubungan kerja berakhir sesuai dengan  jangka waktu perjanjian ini atau berakhirnya hubungan kerja karena pemutusan perjanjian kerja sebelum perjanjian kerja ini berakhir. Dan dengan demikian <strong>PIHAK KEDUA</strong> tetap sanggup bekerja sebagai karyawan PKWT, hanya bekerja untuk waktu tertentu saja, sebagaimana yang telah ditetapkan dan disepakati dalam Perjanjian Kerja Waktu Tertentu ini.</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr><td colspan="2">Ditandatangani di Surabaya</td></tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td style="width: 50%; text-align: center;">PIHAK PERTAMA</td>
			<td style="width: 50%; text-align: center;">PIHAK KEDUA</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 50%; text-align: center;">
				<strong>
					<u>MIFTAKHUL ARIF</u>
				</strong>
			</td>
			<td style="width: 50%; text-align: center;">
				<strong>{NAMA_LENGKAP}</strong>
			</td>
		</tr>
		<tr>
			<td style="width: 50%; text-align: center;">
				<strong>Kuasa Direksi</strong>
			</td>
			<td style="width: 50%; text-align: center;">&nbsp;</td>
		</tr>
	</tbody>
</table>
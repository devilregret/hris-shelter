<p style="text-align: center; font-family: 'Arial'; line-height: 1;">
	<span style="font-size: 12pt;">
		<strong>PERJANJIAN BERSAMA</strong>
		<br/>
		antara :
		<br/>
		<strong>{NAMA_BISNIS_UNIT}</strong>
		<br/>
		dengan :
		<br/>
		<strong>{NAMA_LENGKAP}</strong>
		<br/>
		tentang
		<br/>
		<strong>Persetujuan Pelepasan Hak Uang Kompensasi PKWT</strong>
	</span>
</p>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td colspan="4;">Yang bertanda tangan di bawah ini:</td>
		</tr>
		<tr>
			<td colspan="4;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 20px;">1.</td>
			<td colspan="3;"><strong>{NAMA_BISNIS_UNIT}</strong>, berkedudukan di Semampir Selatan VA No.18 Surabaya, yang dalam hal ini di wakili oleh;</td>
		</tr>
		<tr>
			<td style="width: 20px;">&nbsp;</td>
			<td style="width: 200px;">Nama</td>
			<td style="width: 15px;">:</td>
			<td style="width: 390px;">
				<strong>MIFTAKHUL ARIF</strong>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Jabatan</td>
			<td>:</td>
			<td>HR &amp; GM HC & IT</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="3;">---------- Disebut juga sebagai <strong><u>Pihak Pertama.</u></strong></td>
		</tr>
		<tr>
			<td colspan="4;">&nbsp;</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Nama</td>
			<td>:</td>
			<td>
				<strong>{NAMA_LENGKAP}</strong>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Tempat & Tanggal Lahir</td>
			<td>:</td>
			<td>{TEMPAT_LAHIR}, {TANGGAL_LAHIR}</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Jenis Kelamin</td>
			<td>:</td>
			<td>{JENIS_KELAMIN}</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>NIK</td>
			<td>:</td>
			<td>{NIK}</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Alamat</td>
			<td>:</td>
			<td>{ALAMAT_KTP}</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="3;">---------- Disebut juga sebagai <strong><u>Pihak Kedua.</u></strong></td>
		</tr>
		<tr>
			<td colspan="4;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4;">Pihak Pertama dan Pihak Kedua sepakat dan saling mengikatkan diri menentukan isi Perjanjian Bersama mengenai Pelepasan Hak Uang Kompensasi PKWT, sebagai berikut :</td>
		</tr>
		<tr>
			<td colspan="4;">&nbsp;</td>
		</tr>
	</tbody>
</table>
<table style="text-align: justify; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr>
			<td width="20px">1.</td>
			<td width="605px">Bahwa, Pemberian Uang Kompensasi PKWT oleh Pengusaha diatur sebagaimana Pasal 15 Peraturan Pemerintah No 35 Tahun 2021;</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Bahwa, berdasarkan ketentuan tersebut maka Pihak Pertama dan Pihak Kedua bersepakat apabila pada saat berakhirnya Perjanjian Kerja Waktu Tertentu (PKWT),  Pihak Kedua setuju dan bersedia untuk melepaskan haknya dan/atau tidak menerima Uang Kompensasi PKWT;</td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Bahwa, Pihak Pertama dan Pihak Kedua sepakat dan setuju untuk mengesampingkan ketentuan Pasal 15 Peraturan Pemerintah No. 35  Tahun 2021 Tentang PKWT, Alih Daya, Waktu Kerja dan Waktu Istirahat dan Pemutusan Hubungan Kerja;</td>
		</tr>
		<tr>
			<td>4.</td>
			<td>Bahwa, Pihak Kedua berdasarkan perjanjian ini melepaskan semua haknya untuk melakukan upaya dan tindakan hukum secara perdata maupun pidana pada Pihak Pertama baik untuk sekarang dan di kemudian hari, untuk segala sesuatu yang berhubungan dengan status kekaryawanan, Uang Pesangon, Uang Penghargaan dan Uang Penggantian Hak maupun hak-hak Pihak Kedua lainnya yang dirasa belum dipenuhi Pihak pertama;</td>
		</tr>
		<tr>
			<td>5.</td>
			<td>Bahwa, Pihak Pertama dan Pihak Kedua sepakat untuk melaksanakan isi kesepakatan ini dengan penuh kesadaran dan tanggung jawab;</td>
		</tr>
		<tr>
			<td>6.</td>
			<td>Bahwa, Pihak Pertama dan Pihak Kedua sepakat, sama-sama mengakui hasil Perjanjian Bersama ini sebagai Perjanjian yang mempunyai kekuatan hukum mengikat bagi Para Pihak dalam perjanjian ini, dan mengesampingkan berlakunya ketentuan Pasal 1266 dan Pasal 1267 Kitab Undang-Undang Hukum Perdata Republik Indonesia.</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td style="text-align: justify;" colspan="2;">
				Demikian, Perjanjian Bersama ini dibuat tanpa ada paksaan dan tekanan dari pihak manapun juga serta dimengerti dan dipahami maksud dan dibuatnya perjanjian bersama ini.
			</td>
		</tr>
	</tbody>
</table>
<table style="text-align: right; font-family: 'Arial'; line-height: 1; font-size: 11pt; width: 100%;">
	<tbody>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr><td colspan="2">Surabaya, ……………………………………..</td></tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td style="width: 50%; text-align: center;">Pihak Kedua</td>
			<td style="width: 50%; text-align: center;">Pihak Pertama <br> {NAMA_BISNIS_UNIT},</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 50%; text-align: center;">
				<strong>
					<u>{NAMA_LENGKAP}</u>
				</strong>
			</td>
			<td style="width: 50%; text-align: center;">
				<strong>MIFTAKHUL ARIF</strong>
			</td>
		</tr>
		<tr>
			<td style="width: 50%; text-align: center;">
				<strong>&nbsp;</strong>
			</td>
			<td style="width: 50%; text-align: center;">
				<strong>GM HC & IT</strong></td>
		</tr>
	</tbody>
</table>
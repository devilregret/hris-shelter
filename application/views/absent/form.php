<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('absent/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Ubah Data Absensi</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("absent/form"); ?>">
							<?php
							$list_name = array('Mangkir', 'Cuti', 'Izin', 'Sakit');
							?>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Keterangan<span class="text-danger"> *</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<select class="form-control select2 note" required>
										<option value='Mangkir' <?php if("Mangkir"==$name): echo "selected"; endif;?>>Mangkir</option>
										<option value='Cuti' <?php if("Cuti"==$name): echo "selected"; endif;?>>Cuti</option>
										<option value='Izin' <?php if("Izin"==$name): echo "selected"; endif;?>>Izin</option>
										<option value='Sakit' <?php if("Sakit"==$name): echo "selected"; endif;?>>Sakit</option>
										<option value='Lainya' <?php if(!in_array($name, $list_name)): echo "selected"; endif;?>>Lainya</option>
									</select>
								</div>
							</div>
							<div class="form-group row form-note" <?php if(in_array($name, $list_name)): echo "hidden"; endif;?>>
								<label for="name" class="col-sm-3 col-form-label">&nbsp;<span class="text-danger"> </span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control name" name="name" value="<?php echo $name; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label"> Potongan<span class="text-danger"> *</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control numeric rupiah" placeholder="Nominal" name="value" value="<?php echo number_format($value,0,',','.'); ?>" required>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
<script type="text/javascript">
	$('.note').on('change', function() {
		if(this.value == 'Lainya'){
			$('.name').val('');
			$('.form-note').removeAttr('hidden');
		}else{
			$('.name').val(this.value);
			$('.form-note').attr('hidden');
		}
	});
</script>
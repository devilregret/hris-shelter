<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('vacation/site'); ?>">Daftar Site Bisnis</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Site Bisnis</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("vacation/detail_form"); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">NIK</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" value="<?php echo $id_card; ?>" disabled>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">No Karyawan</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" value="<?php echo $employee_number; ?>" disabled>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nama Karyawan</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" value="<?php echo $full_name; ?>" disabled>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Jumlah Cuti Tahunan</label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="employee_id" value="<?php echo $id; ?>">
									<input type="hidden" class="form-control" name="site_id" value="<?php echo $site_id; ?>">
									<input type="number" class="form-control" name="vacation" value="<?php echo $vacation; ?>">
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var last_valid_selection = null;
		$('#pic').change(function(event) {
			if ($(this).val().length > 3) {
				$(this).val(last_valid_selection);
			} else {
				last_valid_selection = $(this).val();
			}
		});
	});
</script>
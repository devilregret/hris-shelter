<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('struktur_jabatan/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Setting Approval Cuti</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("approval_cuti/form"); ?>">
							<div style = 'display: none' class="form-group row">
								<label class="col-sm-3 col-form-label">ID <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="number" readonly name="id" class="form-control" value="<?php echo $id; ?>">
								</div>
								<div class="col-sm-4"></div>
							</div>
						
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Posisi <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="position_id" required>
										<option value=""> &nbsp;-- PILIH POSISI --</option>
										<?php foreach ($list_position as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php echo ($item->id ==  $position_id) ? ' selected="selected"' : '';?> > <?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Approval 1 <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="approval_1" required>
										<option value=""> &nbsp;-- PILIH APPROVAL 1 --</option>
										<?php foreach ($list_position as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php echo ($item->id ==  $approval_1) ? ' selected="selected"' : '';?> > <?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Approval 2 <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="approval_2">
										<option value=""> &nbsp;-- PILIH APPROVAL 2 --</option>
										<?php foreach ($list_position as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php echo ($item->id ==  $approval_2) ? ' selected="selected"' : '';?> > <?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Approval 3 <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="approval_3">
										<option value=""> &nbsp;-- PILIH APPROVAL 3 --</option>
										<?php foreach ($list_position as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php echo ($item->id ==  $approval_3) ? ' selected="selected"' : '';?> > <?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>

							
							
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
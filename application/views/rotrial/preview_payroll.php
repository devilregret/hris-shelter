<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/css/adminlte.min.css"); ?>
		<?php echo asset_js("plugins/jquery/jquery.min.js"); ?>		
	</head>
	<body>
		<div class="wrapper">
			<section class="invoice">
				<div class="row">
					<div class="col-12">
						<h2 class="page-header">
							<img width="100%" src="<?php echo base_url('files/'.strtolower($employee->company_code).'.jpg'); ?>"/>
						</h2>
					</div>
				</div>
				<div class="row invoice-info m-4">
					<div class="col-sm-3 invoice-col">
						<address>
							PERIODE BULAN <br>
							NO REKENING <br>
							NAMA <br>
							NOMOR KARYAWAN <br>
							JABATAN <br>
							SITE <br>
							<?php if($payroll->attendance): ?>
							JUMLAH KEHADIRAN <br>
							<?php endif; ?>
							<?php if($payroll->overtime_hour): ?>
							JUMLAH JAM LEMBUR <br>
							<?php endif; ?>
						</address>
					</div>
					<div class="col-sm-6 invoice-col">
						<?php 
							$start_date = substr($payroll->periode_start, 8, 2).' '.get_month(substr($payroll->periode_start, 5, 2));
							$end_date 	= substr($payroll->periode_end, 8, 2).' '.get_month(substr($payroll->periode_end, 5, 2)).' '.substr($payroll->periode_end, 0, 4);
						?>
						<address>
							: <?php echo $start_date; ?> - <?php echo $end_date; ?><br>
							: <?php echo $employee->bank_account; ?><br>
							: <?php echo $employee->full_name; ?><br>
							: <?php echo $employee->employee_number; ?><br>
							: <?php echo $position->name; ?><br>
							: <?php echo $employee->site_name; ?><br>
							<?php if($payroll->attendance): ?>
							: <?php echo $payroll->attendance; ?> <br>
							<?php endif; ?>
							<?php if($payroll->overtime_hour): ?>
							: <?php echo $payroll->overtime_hour; ?> <br>
							<?php endif; ?>
						</address>
					</div>
				</div>
				<div class="row invoice-info">
					<div class="col-sm-6 invoice-col">
					&nbsp;
					</div>
					<div class="col-sm-6 invoice-col">
						<address >
							<strong>Gaji bersih diterima : <?php echo format_rupiah($payroll->salary); ?></strong>
						</address>
					</div>
				</div>
				<div class="row invoice-info m-4">
					<div class="col-sm-6 invoice-col">
						<address>
							<table style='border:none'>
								<tr><td><strong>Pendapatan</strong></td><td></td><td></td></tr>
								<?php if($payroll->basic_salary): ?>
								<tr><td>Gaji Pokok</td><td>: Rp.</td><td style='float:right;'><?php echo rupiah($payroll->basic_salary); ?></td></tr>
								<?php else: ?>
								<tr><td>Gaji Pokok</td><td>: Rp.</td><td style='float:right;'><?php echo rupiah(0); ?></td></tr>
								<?php endif; ?>
								<?php if($payroll->overtime_calculation): ?>
								<tr><td>Lembur</td><td>: Rp.</td><td style='float:right;'><?php echo rupiah($payroll->overtime_calculation); ?></td></tr>
								<?php endif; ?>
								<?php 
								foreach( $income AS $item):
									echo "<tr><td >".$item->name." &nbsp;&nbsp;</td><td>: Rp.</td><td style='float:right;'>".rupiah($item->value)."</td></tr>";
								endforeach;
								?>
        					</table>
        				</address>
      				</div>
      				<div class="col-sm-6 invoice-col">
      					<address>
      						<table style='border:none'>
      							<tr><td><strong>Potongan</strong></td><td></td><td></td></tr>
								<?php if($payroll->bpjs_ks): ?>
								<tr><td>BPJS Kesehatan</td><td>: Rp.</td><td style='float:right;'><?php echo rupiah($payroll->bpjs_ks); ?></td></tr>
								<?php else: ?>
								<tr><td>BPJS Kesehatan</td><td>: Rp.</td><td style='float:right;'><?php echo rupiah(0); ?></td></tr>
								<?php endif; ?>
      							<?php if($payroll->bpjs_jht): ?>
								<tr><td>BPJS Ketenagakerjaan</td><td>: Rp.</td><td style='float:right;'><?php echo rupiah($payroll->bpjs_jht); ?></td></tr>
								<?php else: ?>
								<tr><td>BPJS Ketenagakerjaan</td><td>: Rp.</td><td style='float:right;'><?php echo rupiah(0); ?></td></tr>
								<?php endif; ?>
<!--       						< ?php if($payroll->bpjs_jp): ?>
								<tr><td>BPJS JP</td><td>: Rp.</td><td style='float:right;'>< ?php echo rupiah($payroll->bpjs_jp); ?></td></tr>
								< ?php else: ?>
								<tr><td>BPJS JP</td><td>: Rp.</td><td style='float:right;'>< ?php echo rupiah(0); ?></td></tr>
								< ?php endif; ?> -->
      							<?php if($payroll->tax_calculation): ?>
								<tr><td>PPH21</td><td>: Rp.</td><td style='float:right;'><?php echo rupiah($payroll->tax_calculation); ?></td></tr>
								<?php else: ?>
								<tr><td>PPH21</td><td>: Rp.</td><td style='float:right;'><?php echo rupiah(0); ?></td></tr>
								<?php endif; ?>
      							<?php 
								foreach( $outcome AS $item):
									echo "<tr><td >".$item->name." &nbsp;&nbsp;</td><td>: Rp.</td><td style='float:right;'>".rupiah($item->value)."</td></tr>";
								endforeach;
								?>
							</table>
						</address>
					</div>
				</div>
				<div class="row invoice-info m-4" style="font-size: 12px;">
					<address >
						<strong>Catatan : </strong><?php echo $payroll->salary_note; ?>
					</address>
				</div>
			</section>
			<div class="page-break"></div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="printContent();">Print</button>
			<button type="button" class="btn btn-default" onclick="window.close();">Tutup</button>
		</div>
		<script type="text/javascript"> 
			function printContent(){
				$('.modal-footer').remove();
				var restorepage = $('body').html();
				var printcontent = $('.invoice').clone();
				$('.invoice').empty().html(printcontent);
				window.print();
				window.close();
			}
		</script>
	</body>
</html>

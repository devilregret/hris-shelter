<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('ropayroll/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;List Payroll Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<form id="payroll" method="POST" action="<?php echo base_url('rotrial/payroll_count'); ?>">
					<div class="row">
						<label class="col-sm-1 col-form-label">Tanggal Awal :</label>
						<div class="col-sm-3">
							<div class="form-group">
								<div class="input-group date" id="reservationdate" data-target-input="nearest">
									<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name='date_start' value="" required />
									<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>
							</div>
						</div>
						<label class="col-sm-1 col-form-label">Tanggal Akhir :</label>
						<div class="col-sm-3">
							<div class="input-group date" id="reservationdate2" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate2" name='date_finish' value="" required />
								<div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<button type="submit" class="btn btn-block btn-primary" style="color: white;"><i class="fa fa-cogs"></i> Hitung</button> 
						</div>
						<div class="col-sm-2">
							<!-- <a href="<?php echo base_url('files/formula/'.$site_id.'.xlsx'); ?>" class="btn btn-block btn-success" style="margin-right:10px;"><i class="fas fa-file-excel"></i> Export</a> -->
							<a href="<?php echo base_url('rotrial/export_payroll'); ?>" class="btn btn-block btn-success" style="margin-right:10px;"><i class="fas fa-file-excel"></i> Export</a>
						</div>
					</div>
					</form>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
						</thead>
					</table>
					<div class="row">
						<div class="col-md-6">
							<?php
								if(isset($resume_payroll)){
								$start_date = substr($resume_payroll->periode_start, 8, 2).' '.get_month(substr($resume_payroll->periode_start, 5, 2));
								$end_date 	= substr($resume_payroll->periode_end, 8, 2).' '.get_month(substr($resume_payroll->periode_end, 5, 2)).' '.substr($resume_payroll->periode_end, 0, 4);
							?>
							<table class="table table-striped">
								<thead>
									<tr>
										<td width="250px"><strong>Periode Pengajian Terakhir</strong></td>
										<td><strong>: <?php echo $start_date; ?> - <?php echo $end_date; ?></strong></td>
									</tr>
									<tr>
										<td width="250px"><strong>Total Karyawan Aktif</strong></td>
										<td><strong>: <?php echo $employee; ?> Karyawan</strong></td>
									</tr>
									<tr>
										<td width="250px"><strong>Total Karyawan terakhir digaji</strong></td>
										<td><strong>: <?php echo $resume_payroll->employee_payroll; ?> Karyawan</strong></td>
									</tr>
									<tr>
										<td width="250px"><strong>Total Penggajian Terakhir </strong></td>
										<td><strong>: <?php echo format_rupiah($resume_payroll->summary_payroll); ?></strong></td>
									</tr>
								</thead>
							</table>
							<?php } ?>
						</div>
						<div class="col-md-6"><?php
								if(isset($resume_approval)){
								$start_date = substr($resume_approval->periode_start, 8, 2).' '.get_month(substr($resume_approval->periode_start, 5, 2));
								$end_date 	= substr($resume_approval->periode_end, 8, 2).' '.get_month(substr($resume_approval->periode_end, 5, 2)).' '.substr($resume_approval->periode_end, 0, 4);
							?>
							<table class="table table-striped">
								<thead>
									<tr>
										<td width="300px"><strong>Periode Pengajuan Terakhir</strong></td>
										<td><strong>: <?php echo $start_date; ?> - <?php echo $end_date; ?></strong></td>
									</tr>
									<tr>
										<td width="300px"><strong>Total Karyawan Aktif</strong></td>
										<td><strong>: <?php echo $employee; ?> Karyawan</strong></td>
									</tr>
									<tr>
										<td width="300px"><strong>Total Karyawan terakhir Pengajuan</strong></td>
										<td><strong>: <?php echo $resume_approval->employee_payroll; ?> Karyawan</strong></td>
									</tr>
									<tr>
										<td width="300px"><strong>Total Pengajuan Terakhir </strong></td>
										<td><strong>: <?php echo format_rupiah($resume_approval->summary_payroll); ?></strong></td>
									</tr>
								</thead>
							</table>
							<?php } ?>
						</div>
					</div>
					<p>&nbsp;</p>
					<div class="row">
						<div class="col-sm-2">
							<strong>Pilih data payroll yang ditampilkan :</strong>
						</div>
						<div class="col-sm-3">
							<select class="form-control preview float-right">
								<option value="<?php echo base_url("rotrial/payroll/last");?>" <?php if($view == "last" ): echo "selected"; endif; ?>>Pengajuan/Draft Terakhir</option>
								<option value="<?php echo base_url("rotrial/payroll/all");?>" <?php if($view == "all" ): echo "selected"; endif; ?>>Semua </option>
							</select>
						</div>
						<div class="col-sm-7">
						</div>
					<p>&nbsp;</p>

					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th width="50px">No</th>
									<th id='employee_name'>Nama Karyawan</th>
									<th id='phone_number'>Nomor Telp/WA</th>
									<th id='periode_start'>Periode Mulai</th>
									<th id='periode_end'>Periode Selesai</th>
									<th id='bpjs_ks_company'>BPJS KS Perusahaan</th>
									<th id='bpjs_jht_company'>BPJS TK Perusahaan</th>
									<th id='salary'>Jumlah Pendapatan</th>
									<th id='payment'>Status Pembayaran</th>
									<th id='email'>Status Notifikasi</th>
									<th id='note'>Catatan</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<style type="text/css">
	input[type="text" i], input[type="email" i], input[type="select" i], input[type="date" i], .select2-container, textarea {
    	margin-left: -7px;
	}
	.vl {
		margin-top: -10px;
		margin-bottom: -25px;
		border-left: 3px solid #ffc107;
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		$('.preview').on('change', function() {
			window.location.href = this.value;
		});
		$( ".comma" ).keyup(function() {
			var value = $(this).val();
			console.log(value);
			if(value.includes(",,")){
				$(this).val(value.substring(0, value.length-1));
				alert('Terdapat koma dobel, silahkan cek lagi.');				
			}
		});
		$( ".line" ).keyup(function() {
			var value = $(this).val();
			if(!Number.isInteger(Number(value))){
				alert('Masukkan satu nomor baris saja.');
				$(this).val(value.substring(0, value.length-1));
			}
		});
		$( ".column" ).keyup(function() {
			var value = $(this).val();
			if((/[^a-zA-Z]/.test(value))){
				alert('Masukkan satu nama kolom saja.');
				$(this).val(value.substring(0, value.length-1));
			}
		});
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("rotrial/payroll_ajax/".$view);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'employee_name' },
				{ data: 'phone_number' },
				{ data: 'periode_start' },
				{ data: 'periode_end' },
				{ data: 'bpjs_ks_company' },
				{ data: 'bpjs_jht_company' },
				{ data: 'salary' },
				{ data: 'payment' },
				{ data: 'email' },
				{ data: 'note' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[4, 'desc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>1 && index<12){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".process").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".process").prop('hidden', false);
    		});
		});
		$("input:checkbox").change(function() {
			$(".process").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".process").prop('hidden', false);
    		});
		});
		$( ".btn.delete" ).click(function() {
			$("#process_type").val('delete');
			$("#note").val($('.note').val());
			$( "#process" ).submit();
		});
		$( ".btn.cancel" ).click(function() {
			$("#process_type").val('cancel');
			$("#note").val($('.note').val());
			$("#process").submit();
		});
		$( ".btn.submission" ).click(function() {
			$("#process_type").val('submission');
			$("#note").val($('.note').val());
			$("#process").submit();
		});
		$('#reservationdate').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false
		});
		$('#reservationdate2').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false
		});
	});
</script>
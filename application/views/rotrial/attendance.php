<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('rotrial/attendance'); ?>">Rekap Kehadiran</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Kehadiran Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>&nbsp;
				</div>
				<form method="post" action="<?php echo base_url("rotrial/import_attendance/".$date); ?>" enctype="multipart/form-data">
				<div class="card-header">
					<div class="row">
						<div class="form-group col-sm-2">
							<strong><br></strong>
							<a href="#" class="btn btn-success export" style="margin-right:10px;"><i class="fas fa-file-export" ></i> Export Data</a>
						</div>
						<div class="form-group col-sm-3">
							<strong>Periode Akhir Rekap Absensi :</strong>
							<div class="input-group date" id="period" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input field-search" data-target="#period"  data-toggle="datetimepicker" name='date' value="<?php echo $date; ?>" required/>
								<div class="input-group-append" data-target="#period" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar"></i>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group col-sm-7">
							<strong>File Rekap Absensi :</strong>
							<div class="input-group">
								<div class="custom-file">
									<input type="hidden" class="form-control" name="site_id" value="<?php echo $site->id; ?>">
									<input type="file" class="custom-file-input btn-sm" name="file" accept=".xlsx" required>
									<label class="custom-file-label">Import Data</label>
								</div>
								<div class="input-group-append">
									<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>
									<button class="btn btn-primary btn-search" style="width:100px;"><i class="fas fa-search" ></i> Cari</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				</form>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
							<tr>
								<td width="250px">Periode Rekap Terakhir</td>
								<td>: <?php echo $last_period; ?></td>
							</tr>
						</thead>
					</table>
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th id='id_card'>NIK KTP</th>
									<th id='employee_name'>Nama Karyawan</th>
									<th>Hari Kerja</th>
									<th>Masuk Kerja</th>
									<th>Shift Malam</th>
									<th>Hari Libur</th>
									<th>Izin</th>
									<th>Cuti</th>
									<th>Sakit</th>
									<th>Alpha</th>
									<th>Non Aktif</th>
									<th>Telat</th>
									<th>Lembur</th>
									<th>Uang Lembur</th>
									<th>Uang Makan</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){

		$('#period').datetimepicker({
			format: 'DD-MM-YYYY',
			pickTime: false
		});

		 $(".btn-search").click(function(){
		 	var date = $(".field-search").val();
			var url = '<?php echo base_url("rotrial/attendance/"); ?>'+date;
			window.location.href = url;
		 });

		 $( ".export" ).click(function() {
			var url = "<?php echo base_url('rotrial/export_attendance/'.$date); ?>"; 
			window.location = url;
		
		});

		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("rotrial/attendance_ajax/".$date);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id_card' },
				{ data: 'full_name' },
				{ data: 'hari_kerja' },
				{ data: 'masuk_kerja' },
				{ data: 'shift_malam' },
				{ data: 'hari_libur' },
				{ data: 'izin' },
				{ data: 'cuti' },
				{ data: 'sakit' },
				{ data: 'alpha' },
				{ data: 'non_aktif' },
				{ data: 'telat' },
				{ data: 'lembur' },
				{ data: 'uang_lembur', 'orderable':false, 'searchable':false  },
				{ data: 'uang_makan', 'orderable':false, 'searchable':false  },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[1, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>=0 && index<2){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
	});
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('salary/list'); ?>">Format Gaji</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Format Gaji Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-1">
							<a href="<?php echo base_url('salary/generate/'.$site->id); ?>" class="btn btn-danger btn-block"><i class="fas fa-cogs"></i> Generate</a>
						</div>
						<div class="col-sm-1">
							<a href="#" class="btn btn-success btn-block export"><i class="fas fa-file-export"></i> Export Data</a>
						</div>
						<div class="col-sm-2">
							&nbsp;
						</div>
						<div class="col-sm-8">
							<form method="post" action="<?php echo base_url("salary/import/".$site->id); ?>" enctype="multipart/form-data">
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input btn-sm" name="file" accept=".xlsx" required> 
										<label class="custom-file-label" for="exampleInputFile">Update Data</label>
									</div>
									<div class="input-group-append">
										<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>   
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
						</thead>
					</table>
					<p>&nbsp;</p>
					<form method="POST" id="save" action="<?php echo base_url('employeeshift/save'); ?>">
					<input type="hidden" class="form-control shift_id" name="shift_id" value="1">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="10px">NO</th>
									<th id='id_card'>NIK</th>
									<th id='employee_number'>NIP</th>
									<th id='full_name'>Nama Karyawan</th>
									<th id='position_name'>Posisi/Jabatan</th>
									<th id='basic_salary'>Gaji Pokok</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("salary/site_ajax/".$site->id);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'id_card' },
				{ data: 'employee_number' },
				{ data: 'full_name' },
				{ data: 'position_name' },
				{ data: 'basic_salary' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[3, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<6){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$( ".export" ).click(function() {
			var url = "<?php echo base_url('salary/export/'.$site->id); ?>" + "?id_card="+$("#s_id_card").val()+"&employee_number="+$("#s_employee_number").val()+"&full_name="+$("#s_full_name").val()+"&position_name="+$("#s_position_name").val()+"&basic_salary="+$("#s_basic_salary").val();
			window.location = url;
		
		});
	});
</script>
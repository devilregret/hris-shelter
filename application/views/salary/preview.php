<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>

	</head>
	<body>
		<section class="content">
			<div class="container-fluid">
				<div class="card card-primary card-outline col-12 mt-2">
					<div class="card-header">
						<h3 class="card-title"><i class="fas fa-eye"></i>&nbsp;Preview Format Gaji</h3>
					</div>
					<div class="row">
						<div class="col-12 table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Id</td>
										<td>: <?php echo $preview->id; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Nama Site Bisnis</td>
										<td>: <?php echo $preview->site_name; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Nama Jabatan</td>
										<td>: <?php echo $preview->position; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td colspan="2">Detail Format Gaji</td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td colspan="2">
										<table class="table table-bordered">
											<tr>
												<td>Kode</td>
												<td>Deskripsi</td>
												<td>Nominal</td>
											</tr>
											<?php 
											foreach ($list_detail as $item): 
											?>
											<tr>
												<td><?php echo $item->code; ?></td>
												<td><?php echo $item->name; ?></td>
												<td><?php 
												if($item->type == 'nominal'): 
													echo $item->value; 
												elseif($item->type == 'persentase'):
													echo $item->percentage.'% ';
													foreach ($list_detail as $reference): 
														if($reference->id == $item->salary_detail_id):
															echo "X ".$reference->value;
														endif;
													endforeach;
												endif;
												?></td>
											</tr>
											<?php 
											endforeach;
											?>
										</table>
										</td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Dibuat oleh</td>
										<td>: <?php echo $preview->creator; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Tanggal dibuat</td>
										<td>: <?php echo $preview->created_at; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Diubah oleh</td>
										<td>: <?php echo $preview->updater; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Tanggal diubah</td>
										<td>: <?php echo $preview->updated_at; ?></td>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="window.close();">Tutup</button>
       	</div>
	</body>
</html>
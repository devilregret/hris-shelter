<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('salary/site/'.$site_id); ?>">Gaji Karyawan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Gaji Karyawan</h3>
				</div>
				<div class="card-header">
					&nbsp;<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4 salary" method="post" action="<?php echo base_url("salary/form_employee/".$site_id."/".$employee_id); ?>">
							<input type="hidden" class="form-control" name="employee_id" value="<?php echo $employee_id; ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nama Karyawan <span class="text-danger"></span></label>
								<div class="col-sm-7">
									<input type="text" class="form-control" readonly value="<?php echo $full_name; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">NIK <span class="text-danger"></span></label> 
								<div class="col-sm-7">
									<input type="text" class="form-control" readonly value="<?php echo $id_card; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Gaji Pokok <span class="text-danger"></span></label> 
								<div class="col-sm-7">
									<input type="text" class="form-control numeric" name="basic_salary" value="<?php echo rupiah_round($basic_salary); ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">BPJS Kesehatan Perusahaan <span class="text-danger"></span></label> 
								<div class="col-sm-7">
									<input type="text" class="form-control" name="bpjs_ks" value="<?php echo $bpjs_ks; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">BPJS Ketenagakerjaan Perusahaan <span class="text-danger"></span></label> 
								<div class="col-sm-7">
									<input type="text" class="form-control" name="bpjs_tk" value="<?php echo $bpjs_tk; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Potongan BPJS Kesehatan <span class="text-danger"></span></label> 
								<div class="col-sm-7">
									<input type="text" class="form-control" name="bpjs_ks_employee" value="<?php echo $bpjs_ks_employee; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Potongan BPJS Ketenagakerjaan <span class="text-danger"></span></label> 
								<div class="col-sm-7">
									<input type="text" class="form-control" name="bpjs_tk_employee" value="<?php echo $bpjs_tk_employee; ?>">
								</div>
							</div>
							<div id="detail">
								<?php 
									foreach ($salary_detail as $item): 
								?>
								<div class="form-group row detail_salary">
									<div class="col-sm-3">
										<a class="btn btn-danger btn-block" onclick="remove(this);"><i class="fas fa-minus"></i> Hapus</a>
									</div>
									<div class="col-sm-5">
										<input type="text" class="form-control" placeholder="Keterangan" value="<?php echo $item->name; ?>" name="salary_detail_name[]" required>
									</div>
									<div class="col-sm-4">
										<input type="text" class="form-control" placeholder="Nominal" name="salary_detail_value[]" value="<?php echo $item->value; ?>">
									</div>
								</div>
								<?php 
								endforeach;
								?>
								<div class="form-group row">
									<div class="col-md-3">
										&nbsp;
									</div>
									<div class="col-sm-5">
										<input type="text" class="form-control" placeholder="Keterangan" name="salary_detail_name[]" >
									</div>
									<div class="col-sm-4">
										<input type="text" class="form-control numeric" placeholder="Nominal" name="salary_detail_value[]">
									</div>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-3">
									<a class="btn btn-success btn-block add-detail"><i class="fas fa-plus"></i>&nbsp;Tambah</a>
								</div>
							</div>
							<hr>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$(".add-detail").click(function() {
			$(".salary").submit();
		});
	});

	function remove(el){
		$(el).parent().parent().remove();
	}
</script>
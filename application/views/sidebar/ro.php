<li class="nav-item">
	<a href="<?php echo base_url("dashboard"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-tachometer-alt"></i><p><span>Dashboard</span></p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'account' || $_MENU_PARENT_ == 'development'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'account' || $_MENU_PARENT_ == 'development'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-check"></i>
		<p>Data Personal<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("structure/list"); ?>" class="nav-link <?php if($_MENU_ == 'structure'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-project-diagram"></i><p><span>Struktur Organisasi</span></p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/jobdesc"); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Deskripsi Pekerjaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/target"); ?>" class="nav-link <?php if($_MENU_ == 'personal_target'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-bullseye"></i><p>Sasaran Mutu</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/register"); ?>" class="nav-link <?php if($_MENU_ == 'register'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-signature"></i><p>Data Diri</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/activity"); ?>" class="nav-link <?php if($_MENU_ == 'activity'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Catatan Aktifitas</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("development/list"); ?>" class="nav-link <?php if($_MENU_ == 'development'): echo 'active'; endif; ?>">
				<i class="nav-icon fa fa-wrench"></i><p>Request</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("tindakanPersonalia/approval"); ?>" class="nav-link <?php if($_MENU_ == 'approval'): echo 'active'; endif; ?>">
	<i class="nav-icon fas fa-check-square"></i><p>Approval</p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-calendar"></i>
		<p>Perizinan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<!-- <li class="nav-item">
			<a href="<?php echo base_url(""); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Dinas Luar</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("tindakanPersonalia/cuti"); ?>" class="nav-link <?php if($_MENU_ == 'cuti'): echo 'active'; endif; ?>">	
				<i class="nav-icon fas fa-bullseye"></i><p>Izin / Cuti</p>
			</a>
		</li>
		
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'rotrial'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'rotrial'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-calendar"></i>
		<p>Trial Payroll<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("rotrial/attendance"); ?>" class="nav-link <?php if($_MENU_ == 'rotrial_attendance'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Rekap Kehadiran</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("rotrial/payroll"); ?>" class="nav-link <?php if($_MENU_ == 'rotrial_payroll'): echo 'active'; endif; ?>">	
				<i class="nav-icon fas fa-bullseye"></i><p>Payroll</p>
			</a>
		</li>
		
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'roemployee' || $_MENU_PARENT_ == 'other' || $_MENU_PARENT_ == 'payroll_employee'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'roemployee' || $_MENU_PARENT_ == 'other'|| $_MENU_PARENT_ == 'payroll_employee'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Karyawan Regular<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("roemployee/list"); ?>" class="nav-link <?php if($_MENU_ == 'roemployee'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-id-card"></i><p>Daftar Karyawan</p>
			</a>
		</li>
<!-- 		<li class="nav-item">
			<a href="< ?php echo base_url("roemployeeshift/list"); ?>" class="nav-link < ?php if($_MENU_ == 'roemployeeshift'): echo 'active'; endif; ?>">
				<i class="nav-icon  fas fa-calendar-alt"></i><p>Shift Karyawan</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("rokehadiran/list"); ?>" class="nav-link <?php if($_MENU_ == 'rokehadiran'): echo 'active'; endif; ?>">
				<i class="nav-icon  fas fa-calendar-alt"></i><p>Rekap Kehadiran</p>
			</a>
		</li>
		<!-- <li class="nav-item">
			<a href="< ?php echo base_url("roattendance/list"); ?>" class="nav-link < ?php if($_MENU_ == 'roattendance_regular'): echo 'active'; endif; ?>">
				<i class="nav-icon  fas fa-calendar-check"></i><p>Kehadiran Karyawan</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("ropayroll/list"); ?>" class="nav-link <?php if($_MENU_ == 'ropayroll'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-invoice-dollar"></i><p>Payroll</p>
			</a>
		</li>
		<li class="nav-item menu-close <?php if($_MENU_ == 'roresign' || $_MENU_ == 'resign'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
			<a href="#" class="nav-link <?php if($_MENU_ == 'roresign' || $_MENU_ == 'resign'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-times"></i>
				<p>Resign<i class="right fas fa-angle-left"></i></p>
			</a>
			<ul class="nav nav-treeview" >
				<li class="nav-item">
					<a href="<?php echo base_url("roresign/list"); ?>" class="nav-link <?php if($_MENU_ == 'roresign'): echo 'active'; endif; ?>">
						<i class="nav-icon fas fa-outdent"></i><p>Pengajuan Resign</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo base_url("resign/ro"); ?>" class="nav-link <?php if($_MENU_ == 'resign'): echo 'active'; endif; ?>">
						<i class="nav-icon fas fa-user-times"></i><p>Resign Disetujui</p>
					</a>
				</li>
			</ul>
		</li>
	</ul>
</li>
<!-- <li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'rocasual'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'rocasual'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Karyawan Casual<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("rocasual/department"); ?>" class="nav-link <?php if($_MENU_ == 'rodepartment'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-building"></i><p>Daftar Departemen</p>
			</a>
		</li>
 		<li class="nav-item">
			<a href="<?php echo base_url("rocasual/list"); ?>" class="nav-link <?php if($_MENU_ == 'rocasual'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-id-card"></i><p>Daftar Karyawan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("rocasual/attendance"); ?>" class="nav-link <?php if($_MENU_ == 'roattendance'): echo 'active'; endif; ?>">
				<i class="nav-icon  fas fa-calendar-alt"></i><p>Absensi</p>
			</a>
		</li>
		<li class="nav-item menu-close <?php if($_MENU_ == 'ropayroll_preview' || $_MENU_ == 'ropayroll_final'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
			<a href="#" class="nav-link">
				<i class="nav-icon fas fa-funnel-dollar"></i>
				<p>Payroll<i class="right fas fa-angle-left"></i></p>
			</a>
			<ul class="nav nav-treeview" >
				<li class="nav-item">
					<a href="<?php echo base_url("rocasual/payroll_preview"); ?>" class="nav-link <?php if($_MENU_ == 'ropayroll_preview'): echo 'active'; endif; ?>">
						<i class="nav-icon fas fa-file"></i><p>Payroll Preview</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo base_url("rocasual/payroll_final"); ?>" class="nav-link <?php if($_MENU_ == 'ropayroll_final'): echo 'active'; endif; ?>">
						<i class="nav-icon fas fa-file-invoice-dollar"></i><p>Payroll Final</p>
					</a>
				</li>
			</ul>
		</li>
	</ul>
</li> -->
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'setting'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'setting'): echo 'active'; endif; ?>">
		<i class="nav-icon fa fa-cogs"></i>
		<p>Pengaturan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("formula/list"); ?>" class="nav-link <?php if($_MENU_ == 'formula'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-money-bill-wave"></i><p>Formula Gaji</p>
			</a>
		</li>
		<!-- 
		<li class="nav-item">
			<a href="< ?php echo base_url("salary/list"); ?>" class="nav-link < ?php if($_MENU_ == 'salary'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-money-bill-wave"></i><p>Format Gaji</p>
			</a>
		</li>
		-->
		<li class="nav-item">
			<a href="<?php echo base_url("vacation/site"); ?>" class="nav-link <?php if($_MENU_ == 'vacation'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-plane"></i><p>Cuti Karyawan</p>
			</a>
		</li>
<!-- 		<li class="nav-item">
			<a href="< ?php echo base_url("workday/list"); ?>" class="nav-link < ?php if($_MENU_ == 'workday'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-briefcase"></i><p>Hari Kerja</p>
			</a>
		</li> -->
		<!-- <li class="nav-item">
			<a href="< ?php echo base_url("cutoff/list"); ?>" class="nav-link < ?php if($_MENU_ == 'cutoff'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-comments-dollar"></i><p>Cut Off</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("account/form"); ?>" class="nav-link <?php if($_MENU_ == 'account'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-user-cog"></i><p>Akun</p>
			</a>
		</li>
	</ul>
</li>
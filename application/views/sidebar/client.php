<!-- <li class="nav-item">
	<a href="< ?php echo base_url("dashboard"); ?>" class="nav-link < ?php if($_MENU_ == 'dashboard'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-tachometer-alt"></i><p><span>Dashboard</span></p>
	</a>
</li> -->
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'report'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'report'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-bar"></i>
		<p>Laporan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("client_report/payroll_regular"); ?>" class="nav-link <?php if($_MENU_ == 'payroll_regular'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-invoice-dollar"></i><p>Payroll Regular</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("client_report/payroll_casual"); ?>" class="nav-link <?php if($_MENU_ == 'payroll_casual'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-invoice-dollar"></i><p>Payroll Casual</p>
			</a>
		</li>
	</ul>
</li>
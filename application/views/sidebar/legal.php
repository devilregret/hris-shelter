<li class="nav-item">
	<a href="<?php echo base_url("dashboard"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-tachometer-alt"></i><p><span>Dashboard</span></p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'account' || $_MENU_PARENT_ == 'development'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'account' || $_MENU_PARENT_ == 'development'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-check"></i>
		<p>Data Personal<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("structure/list"); ?>" class="nav-link <?php if($_MENU_ == 'structure'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-project-diagram"></i><p><span>Struktur Organisasi</span></p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/jobdesc"); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Deskripsi Pekerjaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/target"); ?>" class="nav-link <?php if($_MENU_ == 'personal_target'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-bullseye"></i><p>Sasaran Mutu</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/register"); ?>" class="nav-link <?php if($_MENU_ == 'register'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-signature"></i><p>Data Diri</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/activity"); ?>" class="nav-link <?php if($_MENU_ == 'activity'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Catatan Aktifitas</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("development/list"); ?>" class="nav-link <?php if($_MENU_ == 'development'): echo 'active'; endif; ?>">
				<i class="nav-icon fa fa-wrench"></i><p>Request</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("tindakanPersonalia/approval"); ?>" class="nav-link <?php if($_MENU_ == 'approval'): echo 'active'; endif; ?>">
	<i class="nav-icon fas fa-check-square"></i><p>Approval</p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-calendar"></i>
		<p>Perizinan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<!-- <li class="nav-item">
			<a href="<?php echo base_url(""); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Dinas Luar</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("tindakanPersonalia/personalia_cuti"); ?>" class="nav-link <?php if($_MENU_ == 'personalia_cuti'): echo 'active'; endif; ?>">	
				<i class="nav-icon fas fa-bullseye"></i><p>Izin / Cuti</p>
			</a>
		</li>
		
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'cooperation'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'cooperation'): echo 'active'; endif; ?>">
		<i class="nav-icon fas  fa-handshake"></i>
		<p>Kerjasama<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("mou/legality"); ?>" class="nav-link <?php if($_MENU_ == 'legality'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Daftar Perizinan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("mou/contract/recontract"); ?>" class="nav-link <?php if($_MENU_ == 'recontract'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Rekontrak</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("mou/contract/registration"); ?>" class="nav-link <?php if($_MENU_ == 'registration'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Pendaftaran MOU</p>
			</a>
		</li>
	</ul>
</li>
<!-- <li class="nav-item has-treeview < ?php if($_MENU_PARENT_ == 'report'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link < ?php if($_MENU_PARENT_ == 'report'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-bar"></i>
		<p>Laporan Legal<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="< ?php echo base_url("legal_report/legality"); ?>" class="nav-link < ?php if($_MENU_ == 'report_legality'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Dokumen Legalitas</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="< ?php echo base_url("legal_report/contract"); ?>" class="nav-link < ?php if($_MENU_ == 'report_contract'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Dokumen Kerjasama</p>
			</a>
		</li>
	</ul>
</li> -->
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'setting'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'setting'): echo 'active'; endif; ?>">
		<i class="nav-icon fa fa-cogs"></i>
		<p>Pengaturan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("account/form"); ?>" class="nav-link <?php if($_MENU_ == 'account'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-user-cog"></i><p>Akun</p>
			</a>
		</li>
	</ul>
</li>
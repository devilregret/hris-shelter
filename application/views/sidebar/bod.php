<li class="nav-item">
	<a href="<?php echo base_url("dashboard/progress"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_progress'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Dashboard Progress</span></p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("dashboard/recruitment"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_recruitment'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Dashboard Rekrutmen</span></p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("dashboard/personalia"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_personalia'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Dashboard Personalia</span></p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("dashboard/payroll"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_payroll'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Dashboard Payroll</span></p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("dashboard/benefit"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_benefit'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Dashboard Benefit</span></p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("dashboard/contract"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_contract'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Monitoring Kontrak</span></p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("dashboard/sales"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_sales'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Monitoring Kandidat</span></p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("dashboard/development"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_development'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Monitoring Request</span></p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'account'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'account'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-check"></i>
		<p>Data Personal<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("structure/list"); ?>" class="nav-link <?php if($_MENU_ == 'structure'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-project-diagram"></i><p><span>Struktur Organisasi</span></p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/register"); ?>" class="nav-link <?php if($_MENU_ == 'register'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-signature"></i><p>Data Diri</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("tindakanPersonalia/approval"); ?>" class="nav-link <?php if($_MENU_ == 'approval'): echo 'active'; endif; ?>">
	<i class="nav-icon fas fa-check-square"></i><p>Approval</p>
	</a>
</li>
<!-- <li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-calendar"></i>
		<p>Perizinan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
			<a href="<?php echo base_url(""); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Dinas Luar</p>
			</a>
		<li class="nav-item">
			<a href="<?php echo base_url("tindakanPersonalia/cuti"); ?>" class="nav-link <?php if($_MENU_ == 'cuti'): echo 'active'; endif; ?>">	
				<i class="nav-icon fas fa-bullseye"></i><p>Izin / Cuti</p>
			</a>
		</li>
		
	</ul>
</li> -->
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'employee'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'employee'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-clipboard-list"></i><p>Daftar Karyawan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("employee/list/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-tie"></i><p>Karyawan Indirect</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("employee/list/direct"); ?>" class="nav-link <?php if($_MENU_ == 'direct'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-tie"></i><p>Karyawan Direct</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'report'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'report'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-bar"></i>
		<p>Laporan Legal<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("legal_report/legality"); ?>" class="nav-link <?php if($_MENU_ == 'report_legality'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Dokumen Legalitas</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("legal_report/contract"); ?>" class="nav-link <?php if($_MENU_ == 'report_contract'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Dokumen Kerjasama</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'setting'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'setting'): echo 'active'; endif; ?>">
		<i class="nav-icon fa fa-cogs"></i>
		<p>Pengaturan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("account/form"); ?>" class="nav-link <?php if($_MENU_ == 'account'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-user-cog"></i><p>Akun</p>
			</a>
		</li>
	</ul>
</li>
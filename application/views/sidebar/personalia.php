<li class="nav-item">
	<a href="<?php echo base_url("dashboard"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-tachometer-alt"></i><p><span>Dashboard</span></p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'account' || $_MENU_PARENT_ == 'development'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'account' || $_MENU_PARENT_ == 'development'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-check"></i>
		<p>Data Personal<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("structure/list"); ?>" class="nav-link <?php if($_MENU_ == 'structure'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-project-diagram"></i><p><span>Struktur Organisasi</span></p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/jobdesc"); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Deskripsi Pekerjaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/target"); ?>" class="nav-link <?php if($_MENU_ == 'personal_target'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-bullseye"></i><p>Sasaran Mutu</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/register"); ?>" class="nav-link <?php if($_MENU_ == 'register'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-signature"></i><p>Data Diri</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/activity"); ?>" class="nav-link <?php if($_MENU_ == 'activity'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Catatan Aktifitas</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("development/list"); ?>" class="nav-link <?php if($_MENU_ == 'development'): echo 'active'; endif; ?>">
				<i class="nav-icon fa fa-wrench"></i><p>Request</p>
			</a>
		</li>
	</ul>
</li>

<li class="nav-item">
	<a href="<?php echo base_url("tindakanPersonalia/approval"); ?>" class="nav-link <?php if($_MENU_ == 'approval'): echo 'active'; endif; ?>">
	<i class="nav-icon fas fa-check-square"></i><p>Approval</p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-calendar"></i>
		<p>Perizinan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<!-- <li class="nav-item">
			<a href="<?php echo base_url(""); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Dinas Luar</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("tindakanPersonalia/cuti"); ?>" class="nav-link <?php if($_MENU_ == 'personalia_cuti'): echo 'active'; endif; ?>">	
				<i class="nav-icon fas fa-bullseye"></i><p>Izin / Cuti</p>
			</a>
		</li>
		
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'direct'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'direct'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Direct<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("employee/submission/direct"); ?>" class="nav-link <?php if($_MENU_ == 'submission_direct'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-check"></i><p>Kandidat Diajukan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("employee_contract/list/direct"); ?>" class="nav-link <?php if($_MENU_ == 'employee_contract_direct'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-id-card"></i><p>Kontrak Karyawan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("monitoring/list/direct"); ?>" class="nav-link <?php if($_MENU_ == 'monitoring_direct'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-chart-line"></i><p>Monitoring Kontrak</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'indirect'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'indirect'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Indirect<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("employee/submission/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'submission_indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-check"></i><p>Kandidat Diajukan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("employee_contract/list/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'employee_contract_indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-id-card"></i><p>Kontrak Karyawan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("monitoring/list/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'monitoring_indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-chart-line"></i><p>Monitoring Kontrak</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'employee'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'employee'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-clipboard-list"></i><p>Daftar Karyawan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("employee/list/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-tie"></i><p>Karyawan Indirect</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("employee/list/direct"); ?>" class="nav-link <?php if($_MENU_ == 'direct'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-tie"></i><p>Karyawan Direct</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'benefit_data_employee'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'benefit_data_employee'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Benefit Karyawan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("benefit_employee/submission"); ?>" class="nav-link <?php if($_MENU_ == 'benefit_submission'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-check"></i><p>Pengajuan Benefit</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("benefit_employee/list"); ?>" class="nav-link <?php if($_MENU_ == 'benefit_employee'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-alt"></i><p>Data Lengkap</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("benefit_employee/notcomplete"); ?>" class="nav-link <?php if($_MENU_ == 'benefit_notcomplete'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file"></i><p>Belum Lengkap</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'other'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'other'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Non Job/Resign<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("resign/approval/direct"); ?>" class="nav-link <?php if($_MENU_ == 'resign_approval'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-outdent"></i><p>Pengajuan Resign</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("resign/list"); ?>" class="nav-link <?php if($_MENU_ == 'resign'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-times"></i><p>Karyawan Resign</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("nonjob/approval"); ?>" class="nav-link <?php if($_MENU_ == 'nonjob_approval'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-outdent"></i><p>Pengajuan Nonjob</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("nonjob/list"); ?>" class="nav-link <?php if($_MENU_ == 'nonjob'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-minus"></i><p>Karyawan Non Job</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("mutation/approval"); ?>" class="nav-link <?php if($_MENU_ == 'mutation_approval'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-random"></i><p>Pengajuan Mutasi</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'setting'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'setting'): echo 'active'; endif; ?>">
		<i class="nav-icon fa fa-cogs"></i>
		<p>Pengaturan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("evaluation/jobdesc"); ?>" class="nav-link <?php if($_MENU_ == 'jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Deskripsi Pekerjaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("evaluation/target"); ?>" class="nav-link <?php if($_MENU_ == 'target'): echo 'active'; endif; ?>">
				<i class="nav-icon  fas fa-bullseye"></i><p>Sasaran Mutu</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("formula/list"); ?>" class="nav-link <?php if($_MENU_ == 'formula'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-money-bill-wave"></i><p>Formula Gaji</p>
			</a>
		</li>
		<!-- 
		<li class="nav-item">
			<a href="< ?php echo base_url("salary/list"); ?>" class="nav-link < ?php if($_MENU_ == 'salary'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-money-bill-wave"></i><p>Format Gaji</p>
			</a>
		</li>
		-->
		<li class="nav-item">
			<a href="<?php echo base_url("vacation/site"); ?>" class="nav-link <?php if($_MENU_ == 'vacation'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-plane"></i><p>Cuti Karyawan</p>
			</a>
		</li>
		<!-- <li class="nav-item">
			<a href="< ?php echo base_url("workday/list"); ?>" class="nav-link < ?php if($_MENU_ == 'workday'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-briefcase"></i><p>Hari Kerja</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="< ?php echo base_url("cutoff/list"); ?>" class="nav-link < ?php if($_MENU_ == 'cutoff'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-comments-dollar"></i><p>Cut Off</p>
			</a>
		</li>
		-->
		<li class="nav-item">
			<a href="<?php echo base_url("resign/note"); ?>" class="nav-link <?php if($_MENU_ == 'resign_note'): echo 'active'; endif; ?>">
				<i class="nav-icon fa fa-sign-out-alt"></i><p>Alasan Resign</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("client/list"); ?>" class="nav-link <?php if($_MENU_ == 'client'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-sitemap"></i><p>Perusahaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("site/list"); ?>" class="nav-link <?php if($_MENU_ == 'site'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-handshake"></i><p>Site Bisnis</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("position/list"); ?>" class="nav-link <?php if($_MENU_ == 'position'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-chalkboard-teacher"></i><p>Jabatan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("contract/list"); ?>" class="nav-link <?php if($_MENU_ == 'contract'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-signature"></i><p>Kontrak & Adendum</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("work/list"); ?>" class="nav-link <?php if($_MENU_ == 'work'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-alt"></i><p>Surat Pengalaman</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("warning/list"); ?>" class="nav-link <?php if($_MENU_ == 'warning'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-ban"></i><p>Surat Peringatan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("email/list"); ?>" class="nav-link <?php if($_MENU_ == 'email'): echo 'active'; endif; ?>">
				<i class="nav-icon fa fa-mail-bulk "></i><p>Template Email</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("applicant/check"); ?>" class="nav-link <?php if($_MENU_ == 'check'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-search "></i><p>Cek NIK</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/form"); ?>" class="nav-link <?php if($_MENU_ == 'account'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-user-cog"></i><p>Akun</p>
			</a>
		</li>
	</ul>
</li>
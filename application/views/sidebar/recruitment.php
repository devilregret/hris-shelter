<li class="nav-item">
	<a href="<?php echo base_url("dashboard"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-tachometer-alt"></i><p><span>Dashboard</span></p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'account' || $_MENU_PARENT_ == 'development'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'account' || $_MENU_PARENT_ == 'development'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-check"></i>
		<p>Data Personal<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("structure/list"); ?>" class="nav-link <?php if($_MENU_ == 'structure'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-project-diagram"></i><p><span>Struktur Organisasi</span></p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/jobdesc"); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Deskripsi Pekerjaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/target"); ?>" class="nav-link <?php if($_MENU_ == 'personal_target'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-bullseye"></i><p>Sasaran Mutu</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/register"); ?>" class="nav-link <?php if($_MENU_ == 'register'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-signature"></i><p>Data Diri</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/activity"); ?>" class="nav-link <?php if($_MENU_ == 'activity'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Catatan Aktifitas</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("development/list"); ?>" class="nav-link <?php if($_MENU_ == 'development'): echo 'active'; endif; ?>">
				<i class="nav-icon fa fa-wrench"></i><p>Request</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("tindakanPersonalia/approval"); ?>" class="nav-link <?php if($_MENU_ == 'approval'): echo 'active'; endif; ?>">
	<i class="nav-icon fas fa-check-square"></i><p>Approval</p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-calendar"></i>
		<p>Perizinan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<!-- <li class="nav-item">
			<a href="<?php echo base_url(""); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Dinas Luar</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("tindakanPersonalia/cuti"); ?>" class="nav-link <?php if($_MENU_ == 'personalia_cuti'): echo 'active'; endif; ?>">	
				<i class="nav-icon fas fa-bullseye"></i><p>Izin / Cuti</p>
			</a>
		</li>
		
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'recruitment'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'recruitment'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-check"></i>
		<p>Kandidat<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("candidate/list"); ?>" class="nav-link <?php if($_MENU_ == 'candidate'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-alt"></i><p>Pelamar Regular</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("candidate_temporary/courier"); ?>" class="nav-link <?php if($_MENU_ == 'courier'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-alt"></i><p>Pelamar Kurir</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("candidate_temporary/casual"); ?>" class="nav-link <?php if($_MENU_ == 'casual'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-alt"></i><p>Pelamar Casual</p>
			</a>
		</li>
	</ul>
</li>

<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'recruitment_direct'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'recruitment_direct'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-check"></i>
		<p>Direct<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("submission/client"); ?>" class="nav-link <?php if($_MENU_ == 'client'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-check"></i><p>Pengajuan ke Client</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("submission/personalia"); ?>" class="nav-link <?php if($_MENU_ == 'personalia'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-check"></i><p>Pengajuan ke Personalia</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("candidate/approved/direct"); ?>" class="nav-link <?php if($_MENU_ == 'approved_direct'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-list"></i><p>Kandidat Diterima</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("candidate/rejected/direct"); ?>" class="nav-link <?php if($_MENU_ == 'rejected_direct'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-ban"></i><p>Kandidat Ditolak</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'recruitment_indirect'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'recruitment_indirect'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-check"></i>
		<p>Indirect<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("submission/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-check"></i><p>Pengajuan Indirect</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("candidate/approved/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'approved_indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-list"></i><p>Kandidat Diterima</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("candidate/rejected/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'rejected_indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-ban"></i><p>Kandidat Ditolak</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'other'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'other'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Non Job/Resign<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("resign/list"); ?>" class="nav-link <?php if($_MENU_ == 'resign'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-times"></i><p>Karyawan Resign</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("nonjob/list"); ?>" class="nav-link <?php if($_MENU_ == 'nonjob'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-minus"></i><p>Karyawan Non Job</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'cooperation'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'cooperation'): echo 'active'; endif; ?>">
		<i class="nav-icon fas  fa-handshake"></i>
		<p>Kerjasama/Partnership<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("agreement/list"); ?>" class="nav-link <?php if($_MENU_ == 'agreement'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>PKS</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'setting'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'setting'): echo 'active'; endif; ?>">
		<i class="nav-icon fa fa-cogs"></i>
		<p>Pengaturan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("position/list"); ?>" class="nav-link <?php if($_MENU_ == 'position'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-chalkboard-teacher"></i><p>Jabatan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("information/list"); ?>" class="nav-link <?php if($_MENU_ == 'information'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-info"></i><p>Informasi</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("vacancy/list"); ?>" class="nav-link <?php if($_MENU_ == 'vacancy'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-info"></i><p>Lowongan</p>
			</a>
		</li>
		<!-- <li class="nav-item">
			<a href="<?php echo base_url("history_tes/list"); ?>" class="nav-link <?php if($_MENU_ == 'master_tes'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Master Tes</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("history_tes/list"); ?>" class="nav-link <?php if($_MENU_ == 'history_tes'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>History Tes</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("preference/list"); ?>" class="nav-link <?php if($_MENU_ == 'preference'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-briefcase"></i><p>Preferensi Pekerjaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("email/list"); ?>" class="nav-link <?php if($_MENU_ == 'email'): echo 'active'; endif; ?>">
				<i class="nav-icon fa fa-mail-bulk "></i><p>Template Email</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("applicant/check"); ?>" class="nav-link <?php if($_MENU_ == 'check'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-search "></i><p>Cek NIK</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("applicant/list"); ?>" class="nav-link <?php if($_MENU_ == 'applicant'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-users-cog "></i><p>Akun Pelamar</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/form"); ?>" class="nav-link <?php if($_MENU_ == 'account'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-user-cog"></i><p>Akun</p>
			</a>
		</li>
	</ul>
</li>
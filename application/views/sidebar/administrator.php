<li class="nav-item">
	<a href="<?php echo base_url("dashboard/progress"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_progress'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Dashboard Progress</span></p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("dashboard/recruitment"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_recruitment'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Dashboard Rekrutmen</span></p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("dashboard/personalia"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_personalia'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Dashboard Personalia</span></p>
	</a>
</li>
<?php if(in_array($this->session->userdata('role'), array(3))): ?>
<li class="nav-item">
	<a href="<?php echo base_url("dashboard/payroll"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_payroll'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Dashboard Payroll</span></p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("dashboard/benefit"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_benefit'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Dashboard Benefit</span></p>
	</a>
</li>
<?php endif; ?>
<li class="nav-item">
	<a href="<?php echo base_url("dashboard/contract"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_contract'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Monitoring Kontrak</span></p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("dashboard/sales"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_sales'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Monitoring Kandidat</span></p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("dashboard/development"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard_development'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-line"></i><p><span>Monitoring Request</span></p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'account' || $_MENU_PARENT_ == 'development'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'account' || $_MENU_PARENT_ == 'development'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-check"></i>
		<p>Data Personal<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("structure/approval"); ?>" class="nav-link <?php if($_MENU_ == 'structure_approval'): echo 'active'; endif; ?>"><i class="nav-icon fas fa-check"></i><p><span>Struktur Approval</span></p>
		</a>
		<li class="nav-item">
			<a href="<?php echo base_url("structure/list"); ?>" class="nav-link <?php if($_MENU_ == 'structure'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-project-diagram"></i><p><span>Struktur Organisasi</span></p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/jobdesc"); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Deskripsi Pekerjaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/target"); ?>" class="nav-link <?php if($_MENU_ == 'personal_target'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-bullseye"></i><p>Sasaran Mutu</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/register"); ?>" class="nav-link <?php if($_MENU_ == 'register'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-signature"></i><p>Data Diri</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/activity"); ?>" class="nav-link <?php if($_MENU_ == 'activity'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Catatan Aktifitas</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("development/list"); ?>" class="nav-link <?php if($_MENU_ == 'development'): echo 'active'; endif; ?>">
				<i class="nav-icon fa fa-wrench"></i><p>Request</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("tindakanPersonalia/approval"); ?>" class="nav-link <?php if($_MENU_ == 'approval'): echo 'active'; endif; ?>">
	<i class="nav-icon fas fa-check-square"></i><p>Approval</p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-calendar"></i>
		<p>Perizinan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<!-- <li class="nav-item">
			<a href="<?php echo base_url("account/jobdesc"); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Dinas Luar</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("tindakanPersonalia/cuti"); ?>" class="nav-link <?php if($_MENU_ == 'cuti'): echo 'active'; endif; ?>">	
				<i class="nav-icon fas fa-bullseye"></i><p>Izin / Cuti</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'employee'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'employee'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-clipboard-list"></i><p>Daftar Karyawan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("employee/list/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-tie"></i><p>Karyawan Indirect</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("employee/list/direct"); ?>" class="nav-link <?php if($_MENU_ == 'direct'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-tie"></i><p>Karyawan Direct</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("whatsapp/list"); ?>" class="nav-link <?php if($_MENU_ == 'whatsapp'): echo 'active'; endif; ?>">
		<i class="nav-icon fab fa-whatsapp"></i><p><span>Whatsapp</span></p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'recruitment'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'recruitment'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-check"></i>
		<p>Daftar Kandidat<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("candidate/list"); ?>" class="nav-link <?php if($_MENU_ == 'candidate'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-alt"></i><p>Daftar Pelamar</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'recruitment_direct'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'recruitment_direct'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-check"></i>
		<p>Kandidat Direct<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("submission/client"); ?>" class="nav-link <?php if($_MENU_ == 'client'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-check"></i><p>Pengajuan Client</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("submission/personalia"); ?>" class="nav-link <?php if($_MENU_ == 'personalia'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-check"></i><p>Pengajuan Personalia</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("candidate/approved/direct"); ?>" class="nav-link <?php if($_MENU_ == 'approved_direct'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-list"></i><p>Kandidat Diterima</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("candidate/rejected/direct"); ?>" class="nav-link <?php if($_MENU_ == 'rejected_direct'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-ban"></i><p>Kandidat Ditolak</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'recruitment_indirect'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'recruitment_indirect'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-check"></i>
		<p>Kandidat Indirect<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("submission/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-check"></i><p>Pengajuan Indirect</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("candidate/approved/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'approved_indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-list"></i><p>Kandidat Diterima</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("candidate/rejected/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'rejected_indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-ban"></i><p>Kandidat Ditolak</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'direct'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'direct'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Personalia Direct<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("employee/submission/direct"); ?>" class="nav-link <?php if($_MENU_ == 'submission_direct'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-check"></i><p>Kandidat Diajukan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("employee_contract/list/direct"); ?>" class="nav-link <?php if($_MENU_ == 'employee_contract_direct'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-id-card"></i><p>Kontrak Karyawan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("monitoring/list/direct"); ?>" class="nav-link <?php if($_MENU_ == 'monitoring_direct'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-chart-line"></i><p>Monitoring Kontrak</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'indirect'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'indirect'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Personalia Indirect<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("employee/submission/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'submission_indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-check"></i><p>Kandidat Diajukan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("employee_contract/list/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'employee_contract_indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-id-card"></i><p>Kontrak Karyawan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("monitoring/list/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'monitoring_indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-chart-line"></i><p>Monitoring Kontrak</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'benefit_data_employee'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'benefit_data_employee'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Benefit Karyawan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("benefit_employee/submission"); ?>" class="nav-link <?php if($_MENU_ == 'benefit_submission'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-check"></i><p>Pengajuan Benefit</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("benefit_employee/list"); ?>" class="nav-link <?php if($_MENU_ == 'benefit_employee'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-alt"></i><p>Data Lengkap</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("benefit_employee/notcomplete"); ?>" class="nav-link <?php if($_MENU_ == 'benefit_notcomplete'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file"></i><p>Belum Lengkap</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'benefit_employee'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'benefit_employee'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Benefit<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("benefit/labor"); ?>" class="nav-link <?php if($_MENU_ == 'labor'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-shield-virus"></i><p>Ketenagakerjaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("benefit/health"); ?>" class="nav-link <?php if($_MENU_ == 'health'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-medkit"></i><p>Kesehatan</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'payroll_employee'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'payroll_employee'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-file-invoice-dollar"></i>
		<p>Payroll<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("payroll/list"); ?>" class="nav-link <?php if($_MENU_ == 'payroll'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-invoice-dollar"></i><p>Payroll</p>
			</a>
		</li>
		<!-- <li class="nav-item">
			<a href="< ?php echo base_url("payroll_allowance/list"); ?>" class="nav-link < ?php if($_MENU_ == 'allowance'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-invoice-dollar"></i><p>THR</p>
			</a>
		</li> -->
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'monitoring_benefit'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'monitoring_benefit'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-desktop"></i><p>Monitoring<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("monitoring_benefit/site"); ?>" class="nav-link <?php if($_MENU_ == 'monitoring_site'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-handshake"></i><p>Site Putus</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("monitoring_benefit/employee"); ?>" class="nav-link <?php if($_MENU_ == 'monitoring_employee'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-times"></i><p>Karyawan Resign</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("monitoring_benefit/login"); ?>" class="nav-link <?php if($_MENU_ == 'login'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-users"></i><p>Karyawan Login</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'other'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'other'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Non Job/Resign<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("resign/approval/direct"); ?>" class="nav-link <?php if($_MENU_ == 'resign_approval'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-outdent"></i><p>Pengajuan Resign</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("resign/list"); ?>" class="nav-link <?php if($_MENU_ == 'resign'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-times"></i><p>Karyawan Resign</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("nonjob/list"); ?>" class="nav-link <?php if($_MENU_ == 'nonjob'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-minus"></i><p>Karyawan Non Job</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'payroll_report'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'payroll_report'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-bar"></i>
		<p>Laporan Payroll<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("payroll_approval/list"); ?>" class="nav-link <?php if($_MENU_ == 'payroll_approval'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-check-double"></i><p>Pengajuan Payroll</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("payroll_report/site"); ?>" class="nav-link <?php if($_MENU_ == 'payroll_site'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-invoice-dollar"></i><p>Payroll</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'accurate_report'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'accurate_report'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-bar"></i>
		<p>Laporan Accurate<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("accurate_report/site"); ?>" class="nav-link <?php if($_MENU_ == 'payroll_site'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-invoice-dollar"></i><p>Payroll</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'evaluation'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'evaluation'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-sort-numeric-up-alt"></i>
		<p>Evaluasi<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("evaluation/activity"); ?>" class="nav-link <?php if($_MENU_ == 'activity_employee'): echo 'active'; endif; ?>">
			<i class="nav-icon fas fa-tasks"></i><p>Aktifitas Karyawan</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'cooperation'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'cooperation'): echo 'active'; endif; ?>">
		<i class="nav-icon fas  fa-handshake"></i>
		<p>Kerjasama<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("mou/legality"); ?>" class="nav-link <?php if($_MENU_ == 'legality'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Daftar Perizinan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("mou/contract/recontract"); ?>" class="nav-link <?php if($_MENU_ == 'recontract'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Rekontrak</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("mou/contract/registration"); ?>" class="nav-link <?php if($_MENU_ == 'registration'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Pendaftaran MOU</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("agreement/list"); ?>" class="nav-link <?php if($_MENU_ == 'agreement'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>PKS</p>
			</a>
		</li>
	</ul>
</li>

<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'grade'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'grade'): echo 'active'; endif; ?>">
		<i class="nav-icon fas  fa-edit"></i>
		<p>Grade<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<!-- <li class="nav-item">
			<a href="<?php echo base_url("grade/competency"); ?>" class="nav-link <?php if($_MENU_ == 'grade_competency'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Master Competency</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("grade/list"); ?>" class="nav-link <?php if($_MENU_ == 'grade'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Grade</p>
			</a>
		</li>
		<!--<li class="nav-item">
			<a href="<?php echo base_url("mou/contract/registration"); ?>" class="nav-link <?php if($_MENU_ == 'registration'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Pendaftaran MOU</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("agreement/list"); ?>" class="nav-link <?php if($_MENU_ == 'agreement'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>PKS</p>
			</a>
		</li> -->
	</ul>
</li>

<?php if(in_array($this->session->userdata('role'), array(3))): ?>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'report'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'report'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-bar"></i>
		<p>Laporan Legal<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("legal_report/legality"); ?>" class="nav-link <?php if($_MENU_ == 'report_legality'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Dokumen Legalitas</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("legal_report/contract"); ?>" class="nav-link <?php if($_MENU_ == 'report_contract'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Dokumen Kerjasama</p>
			</a>
		</li>
	</ul>
</li>
<?php endif; ?>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'setting'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'setting'): echo 'active'; endif; ?>">
		<i class="nav-icon fa fa-cogs"></i>
		<p>Pengaturan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("evaluation/jobdesc"); ?>" class="nav-link <?php if($_MENU_ == 'jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Deskripsi Pekerjaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("evaluation/target"); ?>" class="nav-link <?php if($_MENU_ == 'target'): echo 'active'; endif; ?>">
				<i class="nav-icon  fas fa-bullseye"></i><p>Sasaran Mutu</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("branch/list"); ?>" class="nav-link <?php if($_MENU_ == 'branch'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-code-branch"></i><p>Branch</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("information/list"); ?>" class="nav-link <?php if($_MENU_ == 'information'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-info"></i><p>Informasi</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("vacancy/list"); ?>" class="nav-link <?php if($_MENU_ == 'vacancy'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-info"></i><p>Lowongan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("province/list"); ?>" class="nav-link <?php if($_MENU_ == 'province'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-globe-asia"></i><p>Provinsi</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("city/list"); ?>" class="nav-link <?php if($_MENU_ == 'city'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-city"></i><p>Kota</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("company/list"); ?>" class="nav-link <?php if($_MENU_ == 'company'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-industry"></i><p>Bisnis Unit</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("client/list"); ?>" class="nav-link <?php if($_MENU_ == 'client'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-sitemap"></i><p>Perusahaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("site/list"); ?>" class="nav-link <?php if($_MENU_ == 'site'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-handshake"></i><p>Site Bisnis</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("position/list"); ?>" class="nav-link <?php if($_MENU_ == 'position'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-chalkboard-teacher"></i><p>Jabatan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("preference/list"); ?>" class="nav-link <?php if($_MENU_ == 'preference'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-briefcase"></i><p>Preferensi Pekerjaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("contract/list"); ?>" class="nav-link <?php if($_MENU_ == 'contract'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-signature"></i><p>Kontrak & Adendum</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("work/list"); ?>" class="nav-link <?php if($_MENU_ == 'work'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-alt"></i><p>Surat Pengalaman</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("warning/list"); ?>" class="nav-link <?php if($_MENU_ == 'warning'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-ban"></i><p>Surat Peringatan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("accident/list"); ?>" class="nav-link <?php if($_MENU_ == 'accident'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-alt"></i><p>Laporan Kecelekaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("salary/list"); ?>" class="nav-link <?php if($_MENU_ == 'salary'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-money-bill-wave"></i><p>Format Gaji</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("vacation/site"); ?>" class="nav-link <?php if($_MENU_ == 'vacation'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-plane"></i><p>Cuti Karyawan</p>
			</a>
		</li>
		<!-- <li class="nav-item">
			<a href="< ?php echo base_url("workday/list"); ?>" class="nav-link < ?php if($_MENU_ == 'workday'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-briefcase"></i><p>Hari Kerja</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="< ?php echo base_url("tax/list"); ?>" class="nav-link < ?php if($_MENU_ == 'tax'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-exclamation"></i><p>Pajak</p>
				<i class="nav-icon fa fa-triangle-exclamation"></i>
			</a>
		</li> -->

		<li class="nav-item">
			<a href="<?php echo base_url("cutoff/list"); ?>" class="nav-link <?php if($_MENU_ == 'cutoff'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-comments-dollar"></i><p>Cut Off</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("email/list"); ?>" class="nav-link <?php if($_MENU_ == 'email'): echo 'active'; endif; ?>">
				<i class="nav-icon fa fa-mail-bulk "></i><p>Template Email</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("upload/labor"); ?>" class="nav-link <?php if($_MENU_ == 'labor_setting'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-alt"></i><p>Upload KPJ</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("upload/health"); ?>" class="nav-link <?php if($_MENU_ == 'health_setting'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-upload"></i><p>Upload KIS</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("health_assurance/list"); ?>" class="nav-link <?php if($_MENU_ == 'health_assurance'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-upload"></i><p>Asuransi Kesehatan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("wages/list"); ?>" class="nav-link <?php if($_MENU_ == 'wages'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-money-bill-wave"></i><p>UMK</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("service/list"); ?>" class="nav-link <?php if($_MENU_ == 'service'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-hand-sparkles"></i> <p>Harga Jual Jasa</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("equipment/list"); ?>" class="nav-link <?php if($_MENU_ == 'equipment'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fas fa-box"></i><p>Harga Jual Barang</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("sales_template/list"); ?>" class="nav-link <?php if($_MENU_ == 'sales_template'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fas fa-file-signature"></i><p>Template Sales</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("report_template/list"); ?>" class="nav-link <?php if($_MENU_ == 'report_template'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file"></i><p>Template Laporan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("applicant/check"); ?>" class="nav-link <?php if($_MENU_ == 'check'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-search "></i><p>Cek NIK</p>
			</a>
		</li>
		<!-- <li class="nav-item">
			<a href="< ?php echo base_url("user/client"); ?>" class="nav-link < ?php if($_MENU_ == 'client'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-users-cog "></i><p>Akun Client</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="< ?php echo base_url("user/developer"); ?>" class="nav-link < ?php if($_MENU_ == 'developer'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-users-cog "></i><p>Akun Developer</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("user/list"); ?>" class="nav-link <?php if($_MENU_ == 'user'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-users-cog "></i><p>Akun Pengguna</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("user/indirect"); ?>" class="nav-link <?php if($_MENU_ == 'indirect'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-users-cog "></i><p>Akun Indirect</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("applicant/list"); ?>" class="nav-link <?php if($_MENU_ == 'applicant'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-users-cog "></i><p>Akun Karyawan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/form"); ?>" class="nav-link <?php if($_MENU_ == 'account'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-user-cog"></i><p>Akun</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("level_jabatan/list"); ?>" class="nav-link <?php if($_MENU_ == 'level_jabatan'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-user-cog"></i><p>Level Karyawan</p>
			</a>
		</li>
		<!-- <li class="nav-item">
			<a href="<?php echo base_url("struktur_jabatan/list"); ?>" class="nav-link <?php if($_MENU_ == 'struktur_approval'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-user-cog"></i><p>Struktur Approval</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("struktur_organisasi/list"); ?>" class="nav-link <?php if($_MENU_ == 'struktur_organisasi'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-user-cog"></i><p>Struktur Organisasi</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("approval_cuti/list"); ?>" class="nav-link <?php if($_MENU_ == 'approval_cuti'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-user-cog"></i><p>Approval Cuti</p>
			</a>
		</li>
	</ul>
</li>
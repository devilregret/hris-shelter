<li class="nav-item">
	<a href="<?php echo base_url("dashboard"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-tachometer-alt"></i><p><span>Dashboard</span></p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'account' || $_MENU_PARENT_ == 'development'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'account' || $_MENU_PARENT_ == 'development'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-check"></i>
		<p>Data Personal<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("structure/list"); ?>" class="nav-link <?php if($_MENU_ == 'structure'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-project-diagram"></i><p><span>Struktur Organisasi</span></p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/jobdesc"); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Deskripsi Pekerjaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/target"); ?>" class="nav-link <?php if($_MENU_ == 'personal_target'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-bullseye"></i><p>Sasaran Mutu</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/register"); ?>" class="nav-link <?php if($_MENU_ == 'register'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-signature"></i><p>Data Diri</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/activity"); ?>" class="nav-link <?php if($_MENU_ == 'activity'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Catatan Aktifitas</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("development/list"); ?>" class="nav-link <?php if($_MENU_ == 'development'): echo 'active'; endif; ?>">
				<i class="nav-icon fa fa-wrench"></i><p>Request</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("tindakanPersonalia/approval"); ?>" class="nav-link <?php if($_MENU_ == 'approval'): echo 'active'; endif; ?>">
	<i class="nav-icon fas fa-check-square"></i><p>Approval</p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-calendar"></i>
		<p>Perizinan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<!-- <li class="nav-item">
			<a href="<?php echo base_url("account/jobdesc"); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Dinas Luar</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("tindakanPersonalia/cuti"); ?>" class="nav-link <?php if($_MENU_ == 'cuti'): echo 'active'; endif; ?>">	
				<i class="nav-icon fas fa-bullseye"></i><p>Izin / Cuti</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'benefit_data_employee'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'benefit_data_employee'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Karyawan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("benefit_employee/approval"); ?>" class="nav-link <?php if($_MENU_ == 'benefit_approval'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-check"></i><p>Pengajuan Benefit</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("benefit_employee/list"); ?>" class="nav-link <?php if($_MENU_ == 'benefit_employee'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-alt"></i><p>Data Lengkap</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("benefit_employee/notcomplete"); ?>" class="nav-link <?php if($_MENU_ == 'benefit_notcomplete'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file"></i><p>Belum Lengkap</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'benefit_employee'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'benefit_employee'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Benefit<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("benefit/labor"); ?>" class="nav-link <?php if($_MENU_ == 'labor'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-shield-virus"></i><p>Ketenagakerjaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("benefit/health"); ?>" class="nav-link <?php if($_MENU_ == 'health'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-medkit"></i><p>Kesehatan</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'monitoring_benefit'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'monitoring_benefit'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-desktop"></i><p>Monitoring<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("monitoring_benefit/site"); ?>" class="nav-link <?php if($_MENU_ == 'monitoring_site'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-handshake"></i><p>Site Putus</p>
			</a>
		</li>
	</ul>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("monitoring_benefit/employee"); ?>" class="nav-link <?php if($_MENU_ == 'monitoring_employee'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-times"></i><p>Karyawan Resign</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'other'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'other'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Resign<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("resign/list"); ?>" class="nav-link <?php if($_MENU_ == 'resign'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-user-times"></i><p>Karyawan Resign</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'setting'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'setting'): echo 'active'; endif; ?>">
		<i class="nav-icon fa fa-cogs"></i>
		<p>Pengaturan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("upload/labor"); ?>" class="nav-link <?php if($_MENU_ == 'labor_setting'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-upload"></i><p>Upload KPJ</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("upload/health"); ?>" class="nav-link <?php if($_MENU_ == 'health_setting'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-upload"></i><p>Upload KIS</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("work/list"); ?>" class="nav-link <?php if($_MENU_ == 'work'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-alt"></i><p>Surat Pengalaman</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("accident/list"); ?>" class="nav-link <?php if($_MENU_ == 'accident'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-alt"></i><p>Surat Laporan Kecelakaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("health_assurance/list"); ?>" class="nav-link <?php if($_MENU_ == 'health_assurance'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-shield-virus"></i><p>Asuransi Kesehatan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("email/list"); ?>" class="nav-link <?php if($_MENU_ == 'email'): echo 'active'; endif; ?>">
				<i class="nav-icon fa fa-mail-bulk "></i><p>Template Email</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/form"); ?>" class="nav-link <?php if($_MENU_ == 'account'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-user-cog"></i><p>Akun</p>
			</a>
		</li>
	</ul>
</li>
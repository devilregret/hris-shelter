<li class="nav-item">
	<a href="<?php echo base_url("dashboard"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-tachometer-alt"></i><p><span>Dashboard</span></p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("account/register"); ?>" class="nav-link <?php if($_MENU_ == 'register'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-file-signature"></i><p>Data Diri</p>
	</a>
</li>
<?php if($this->session->userdata('is_employee')): ?>
<?php if($this->session->userdata('site_id') == 2): ?>
<li class="nav-item">
	<a href="<?php echo base_url("structure/approval"); ?>" class="nav-link <?php if($_MENU_ == 'structure_approval'): echo 'active'; endif; ?>"><i class="nav-icon fas fa-check"></i><p><span>Struktur Approval</span></p>
</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("structure/list"); ?>" class="nav-link <?php if($_MENU_ == 'structure'): echo 'active'; endif; ?>"><i class="nav-icon fas fa-project-diagram"></i><p><span>Struktur Organisasi</span></p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("account/jobdesc"); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-pencil-ruler"></i><p>Deskripsi Pekerjaan</p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("account/target"); ?>" class="nav-link <?php if($_MENU_ == 'personal_target'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-bullseye"></i><p>Sasaran Mutu</p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("account/activity"); ?>" class="nav-link <?php if($_MENU_ == 'activity'): echo 'active'; endif; ?>">
	<i class="nav-icon fas fa-tasks"></i><p>Catatan Aktifitas</p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("development/list"); ?>" class="nav-link <?php if($_MENU_ == 'development'): echo 'active'; endif; ?>">
		<i class="nav-icon fa fa-wrench"></i><p>Request</p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("tindakanPersonalia/approval"); ?>" class="nav-link <?php if($_MENU_ == 'approval'): echo 'active'; endif; ?>">
	<i class="nav-icon fas fa-check-square"></i><p>Approval</p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-calendar"></i>
		<p>Perizinan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<!-- <li class="nav-item">
			<a href="<?php echo base_url("account/jobdesc"); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Dinas Luar</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("tindakanPersonalia/cuti"); ?>" class="nav-link <?php if($_MENU_ == 'cuti'): echo 'active'; endif; ?>">	
				<i class="nav-icon fas fa-bullseye"></i><p>Izin / Cuti</p>
			</a>
		</li>
	</ul>
</li>
<?php endif; ?>
<?php if($this->session->userdata('site_id') > 2):?>
<li class="nav-item">
	<a href="<?php echo base_url("account/payroll"); ?>" class="nav-link <?php if($_MENU_ == 'payroll'): echo 'active'; endif; ?>">
	<i class="nav-icon fas fa-dollar-sign"></i><p>Slip Gaji</p>
	</a>
</li>
<?php endif; ?>
<?php else: ?>
<li class="nav-item">
	<a href="<?php echo base_url("appeal/vacancy"); ?>" class="nav-link <?php if($_MENU_ == 'vacancy'): echo 'active'; endif; ?>"><i class="nav-icon fa fa-table"></i><p><span>Daftar Lowongan</span></p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url("appeal/applied"); ?>" class="nav-link <?php if($_MENU_ == 'applied'): echo 'active'; endif; ?>"><i class="nav-icon fa fa-book"></i><p><span>Lowongan Dilamar</span></p>
	</a>
</li>
<?php endif; ?>
<li class="nav-item">
	<a href="<?php echo base_url("account/form"); ?>" class="nav-link <?php if($_MENU_ == 'account'): echo 'active'; endif; ?>">
		<i class="nav-icon fa  fa-user-cog"></i><p>Akun</p>
	</a>
</li>
<aside class="main-sidebar elevation-4 sidebar-light-primary">
	<a href="<?php echo base_url(); ?>" class="brand-link">
		<img src="<?php echo asset_img('shelter-logo.png'); ?>" alt="Logo Shelter" width="220">
		<span class="brand-text font-weight-light"></span>
	</a>
	<div class="user-panel mt-3 pb-3 mb-3 d-flex">
		<div class="image">
			<img src="<?php echo base_url('asset/img/candidate.png'); ?>" class="img-circle elevation-2" alt="User Image">
		</div>
		<div class="info">
			<a href="#" class="d-block"><?php echo $this->session->userdata('name') ?></a>
		</div>
	</div>
	<div class="sidebar">
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<?php
					switch ($_ROLE_){
						case 1: 
							$this->load->view("sidebar/candidate");
							break;
						case 2: 
						case 3: 
							$this->load->view("sidebar/administrator");
							break;
						case 4:
						case 6:
						case 8:
						case 9:
						case 10: 
							$this->load->view("sidebar/ro");
							break;
						case 5:
						case 7:
							$this->load->view("sidebar/ro_security");
							break;
						case 11:
						case 12: 
							$this->load->view("sidebar/payroll");
							break;
						case 13:
						case 14: 
							$this->load->view("sidebar/benefit");
							break;
						case 15:
							$this->load->view("sidebar/personalia");
							break;
						case 16:
						case 17:
						case 18:
						case 19:
						case 20:
						case 21:
							$this->load->view("sidebar/spv_personalia");
							break;
						case 22:
						case 23:
						case 24:
						case 25:
						case 26:
						case 27:
						case 28: 
							$this->load->view("sidebar/recruitment");
							break;
						case 29:
							$this->load->view("sidebar/sales");
							break;
						case 30:
						case 31:
						case 32:
						case 33:
						case 34:
						case 35:
							$this->load->view("sidebar/spv_sales");
							break;
						case 36:
						case 37:
						case 38:
						case 39:
							$this->load->view("sidebar/accounting");
							break;
						case 40:
						case 41:
						case 42:
							$this->load->view("sidebar/manager_accounting");
							break;
						case 43:
							$this->load->view("sidebar/legal");
							break;
						case 44:
						case 45:
						case 46:
						case 47:
							$this->load->view("sidebar/spv_legal");
							break;
						case 50: 
							$this->load->view("sidebar/client");
							break;
						case 98:
						case 99:
							$this->load->view("sidebar/bod");
							break;
						case 101:
							$this->load->view("sidebar/developer");
							break;
					}
				?>
				<li class="nav-item">
					<a href="<?php echo base_url("auth/logout"); ?>" class="nav-link">
						<i class="nav-icon fa fa-sign-out-alt"></i>
						<p><span>Keluar</span></p>
					</a>
				</li>
			</ul>
		</nav>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
	</div>
</aside>
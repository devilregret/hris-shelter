<li class="nav-item">
	<a href="<?php echo base_url("dashboard"); ?>" class="nav-link <?php if($_MENU_ == 'dashboard'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-tachometer-alt"></i><p><span>Dashboard</span></p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'account' || $_MENU_PARENT_ == 'development'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'account' || $_MENU_PARENT_ == 'development'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-check"></i>
		<p>Data Personal<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("structure/list"); ?>" class="nav-link <?php if($_MENU_ == 'structure'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-project-diagram"></i><p><span>Struktur Organisasi</span></p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/jobdesc"); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Deskripsi Pekerjaan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/target"); ?>" class="nav-link <?php if($_MENU_ == 'personal_target'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-bullseye"></i><p>Sasaran Mutu</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/register"); ?>" class="nav-link <?php if($_MENU_ == 'register'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-signature"></i><p>Data Diri</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/activity"); ?>" class="nav-link <?php if($_MENU_ == 'activity'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-tasks"></i><p>Catatan Aktifitas</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("development/list"); ?>" class="nav-link <?php if($_MENU_ == 'development'): echo 'active'; endif; ?>">
				<i class="nav-icon fa fa-wrench"></i><p>Request</p>
			</a>
		</li>
	</ul>
</li>

<li class="nav-item">
	<a href="<?php echo base_url("tindakanPersonalia/approval"); ?>" class="nav-link <?php if($_MENU_ == 'approval'): echo 'active'; endif; ?>">
	<i class="nav-icon fas fa-check-square"></i><p>Approval</p>
	</a>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'tindakanPersonalia'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-calendar"></i>
		<p>Perizinan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<!-- <li class="nav-item">
			<a href="<?php echo base_url(""); ?>" class="nav-link <?php if($_MENU_ == 'personal_jobdesc'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-pencil-ruler"></i><p>Dinas Luar</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("tindakanPersonalia/personalia_cuti"); ?>" class="nav-link <?php if($_MENU_ == 'personalia_cuti'): echo 'active'; endif; ?>">	
				<i class="nav-icon fas fa-bullseye"></i><p>Izin / Cuti</p>
			</a>
		</li>
		
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'payroll_employee' || $_MENU_PARENT_ == 'ro_security'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'payroll_employee' || $_MENU_PARENT_ == 'ro_security'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-user-tie"></i>
		<p>Karyawan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("payroll_employee/list"); ?>" class="nav-link <?php if($_MENU_ == 'payroll_employee'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-id-card"></i><p>Daftar Karyawan</p>
			</a>
		</li>
		<!-- <li class="nav-item">
			<a href="< ?php echo base_url("attendance/list"); ?>" class="nav-link < ?php if($_MENU_ == 'attendance'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-calendar-alt"></i><p>Jadwal Kerja</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="< ?php echo base_url("employeeshift/list"); ?>" class="nav-link < ?php if($_MENU_ == 'employeeshift'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-clipboard-check"></i><p>Shift Karyawan</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("payroll/list"); ?>" class="nav-link <?php if($_MENU_ == 'payroll'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-invoice-dollar"></i><p>Payroll Non Security</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("rosecuritypayroll/list"); ?>" class="nav-link <?php if($_MENU_ == 'ro_security_payroll'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-invoice-dollar"></i><p>Payroll Security</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("payroll/security"); ?>" class="nav-link <?php if($_MENU_ == 'payroll_security'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-invoice-dollar"></i><p>Approval Payroll Security</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("payroll/casual"); ?>" class="nav-link <?php if($_MENU_ == 'payroll_casual'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-invoice-dollar"></i><p>Payroll Casual</p>
			</a>
		</li>
		<!-- <li class="nav-item">
			<a href="< ?php echo base_url("payroll_allowance/list"); ?>" class="nav-link < ?php if($_MENU_ == 'allowance'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-invoice-dollar"></i><p>THR</p>
			</a>
		</li> -->
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'payroll_report'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'payroll_report'): echo 'active'; endif; ?>">
		<i class="nav-icon fas fa-chart-bar"></i>
		<p>Laporan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("payroll_approval/list"); ?>" class="nav-link <?php if($_MENU_ == 'payroll_approval'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-check-double"></i><p>Pengajuan Payroll</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("payroll_report/site"); ?>" class="nav-link <?php if($_MENU_ == 'payroll_site'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file-invoice-dollar"></i><p>Payroll</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview <?php if($_MENU_PARENT_ == 'setting'): echo'menu-open'; else: echo 'menu-close'; endif; ?>">
	<a href="#" class="nav-link <?php if($_MENU_PARENT_ == 'setting'): echo 'active'; endif; ?>">
		<i class="nav-icon fa fa-cogs"></i>
		<p>Pengaturan<i class="right fas fa-angle-left"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?php echo base_url("salary/list"); ?>" class="nav-link <?php if($_MENU_ == 'salary'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-money-bill-wave"></i><p>Formula Gaji</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("vacation/site"); ?>" class="nav-link <?php if($_MENU_ == 'vacation'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-plane"></i><p>Cuti Karyawan</p>
			</a>
		</li>
		<!-- <li class="nav-item">
			<a href="< ?php echo base_url("workday/list"); ?>" class="nav-link < ?php if($_MENU_ == 'workday'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-briefcase"></i><p>Hari Kerja</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url("cutoff/list"); ?>" class="nav-link <?php if($_MENU_ == 'cutoff'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-comments-dollar"></i><p>Cut Off</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("email/list"); ?>" class="nav-link <?php if($_MENU_ == 'email'): echo 'active'; endif; ?>">
				<i class="nav-icon fa fa-mail-bulk "></i><p>Template Email</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("report_template/list"); ?>" class="nav-link <?php if($_MENU_ == 'report_template'): echo 'active'; endif; ?>">
				<i class="nav-icon fas fa-file"></i><p>Template Laporan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("applicant/list"); ?>" class="nav-link <?php if($_MENU_ == 'applicant'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-users-cog "></i><p>Akun Karyawan</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url("account/form"); ?>" class="nav-link <?php if($_MENU_ == 'account'): echo 'active'; endif; ?>">
				<i class="nav-icon fa  fa-user-cog"></i><p>Akun</p>
			</a>
		</li>
	</ul>
</li>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('email/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Template Email</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4" method="post" action="<?php echo base_url("email/form"); ?>">
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Email Pengirim <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="email" class="form-control" placeholder="Email Pengirim" name="email_sender" value="<?php echo $email_sender; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Pengirim <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nama Pengirim" name="name_sender" value="<?php echo $name_sender; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Subject Email <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Subject Pengirim" name="subject" value="<?php echo $subject; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">CC E-Mail <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="email" class="form-control" placeholder="CC E-Mail" name="carbon_copy" value="<?php echo $carbon_copy; ?>">
								</div>
							</div>
							<?php if($type == 'payroll'): ?>
							<div class="form-group row" hidden="true">
								<label class="col-sm-3 col-form-label">Password PDF <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Password PDF" name="password" value="<?php echo $password; ?>" required>
								</div>
							</div>
							<?php endif; ?>
							<div class="alert alert-info alert-dismissible">
								<h5><i class="icon fas fa-info"></i> Catatan untuk Mail Merge</h5>
								<table class="table table-striped">
									<thead>
										<tr>
											<td width="20px">Kode</td>
											<td width="250px">Keterangan</td>
											<td width="20px">Kode</td>
											<td width="250px">Keterangan</td>
										</tr>
									</thead>
									<tbody>
										<?php if($type == 'payroll'): ?>
										<tr>
											<td>{NAMA_LENGKAP}</td>
											<td>Nama Lengkap Karyawan</td>
											<td>{PASSWORD}</td>
											<td>Password PDF payroll, diambil dari Nomor KTP</td>
										</tr>
										<tr>
											<td>{PERIODE}</td>
											<td>Periode Payroll</td>
											<td></td>
											<td></td>
										</tr>
										<?php endif; ?>
										<?php if($type == 'recruitment'): ?>
										<tr>
											<td>{NAMA_LENGKAP}</td>
											<td>Nama Lengkap Kandidat</td>
											<td>{USERNAME}</td>
											<td>Username untuk Login Kandidat</td>
										</tr>
										<tr>
											<td>{PASSWORD}</td>
											<td>Password untuk Login Kandidat</td>
											<td></td>
											<td></td>
										</tr>
										<?php endif; ?>
										<?php if($type == 'personalia'): ?>
										<tr>
											<td>{KONTRAK_KARYAWAN}</td>
											<td>Daftar Kontrak Karyawan</td>
											<td></td>
											<td></td>
										</tr>
										<?php endif; ?>
										<?php if($type == 'benefit'): ?>
										<tr>
											<td>{LAPORAN_BENEFIT}</td>
											<td>Daftar Site Habis, Kontrak Habis dan Karyawan yang belum terdaftar.</td>
											<td></td>
											<td></td>
										</tr>
										<?php endif; ?>
											
									</tbody>
								</table>
							</div>
							<div class="form-group row">
								<div class="col-sm-12">
									<textarea class='editor' name='content' required>
										<?php if(isset($content)){ echo $content; } ?>	
									</textarea>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" name="action" = valuse="save" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
<script>
	$(function () {
		tinymce.init({ 
			selector:'.editor',
			theme: 'modern',
			height: 600,
			menubar: true,
			lineheight_formats: "0pt 1pt 8pt 9pt 10pt 11pt 12pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 36pt",
			plugins: [
			'advlist autolink lists lineheight nonbreaking print preview anchor',
			'searchreplace visualblocks fullscreen textcolor colorpicker',
			'tabfocus table contextmenu powerpaste'
			],
			menubar: 'file edit view insert format tools table tc help',
			toolbar: 'undo redo | bold italic underline strikethrough | forecolor backcolor | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent nonbreaking lineheightselect | table | preview print',
			powerpaste_allow_local_images: true,
			powerpaste_word_import: 'prompt',
			powerpaste_html_import: 'prompt',
		});
	});
</script>
<style type="text/css">
	.note-group-select-from-files {
		display: none;
	}
</style>
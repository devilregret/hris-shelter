<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('employee_contract/list/'.$type); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Karyawan</h3>
				</div>
				<div class="card-body">
					<?php if(isset($message)): echo $message; endif; ?>
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4" method="post" action="<?php echo base_url("employee_contract/form/".$type); ?>" enctype="multipart/form-data">
							<div class="form-group row">
								<h2 class="card-title"><strong>A. DATA DIRI</strong></h2>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nomor KTP <span class="text-danger">*</span></label>
								<div class="col-sm-7">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="text" maxlength="16" pattern=".{16,16}"  title="16 number" class="form-control numeric id_card" placeholder="NIK" name="id_card" value="<?php echo $id_card; ?>" required>
								</div>
								<div class="col-sm-2">
									<a class="btn btn-warning btn-block check-card-id"><i class="fas fa-search"></i> Cek Nomor KTP</a>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Foto KTP <span class="text-danger"> <i class="text-xs "></i></span></label>
								<div class="custom-file col-sm-7 file-upload">
									<input type="file" class="custom-file-input upload-document" name="document_id_card" accept="image/png, image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_id_card != ''): ?>
										<a href="<?php echo $document_id_card; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">No Pendaftaran </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="registration_number" value="<?php echo $registration_number; ?>" readonly>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">No Induk Karyawan </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="employee_number" value="<?php echo $employee_number; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">preview </label>
								<div class="col-sm-9">
									<?php if($document_photo != ''): ?>
										<img id="preview" src="<?php echo $document_photo; ?>" width="200px" alt="Foto" />
									<?php else: ?>
										<img id="preview" src="<?php echo asset_img("candidate.png"); ?>" width="200px" alt="Foto" />
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Foto <span class="text-danger"> <i class="text-xs "></i></span></label>
								<div class="col-sm-7 file-upload">
									<input type="file" class="custom-file-input upload-photo upload-document" name="document_photo" accept="image/png, image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile" >Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_photo != ''): ?>
										<a href="<?php echo $document_photo; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Lengkap <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nama Lengkap" name="full_name" value="<?php echo $full_name; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tempat Lahir <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="city_birth_id">
										<option value="">-- PILIH KOTA --</option>
										<?php foreach ($list_city as $item) :  ?>
											<option value="<?php echo $item->id;?>" <?php if($item->id === $city_birth_id): echo "selected"; endif;?>><?php echo $item->name." - ".$item->province_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tanggal Lahir <span class="text-danger"></span></label>
								<div class="col-sm-2">
									<input type="date" name="date_birth" class="form-control datepicker" value="<?php echo $date_birth; ?>">
								</div>
								<div class="col-sm-7"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Alamat Asal <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<textarea class="form-control" name="address_card" rows="3" placeholder="Contoh : Jl. Raya Nginden No.102, RT.001/RW.03, Baratajaya, Kec. Gubeng 60284" style="text-transform:uppercase"><?php echo $address_card; ?></textarea>
									<span class="text-success">Contoh : Jl. Raya Nginden No.102, RT.001/RW.03, Baratajaya, Kec. Gubeng 60284</span>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Kota Asal <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="city_card_id">
										<option value="">-- PILIH KOTA --</option>
										<?php foreach ($list_city as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $city_card_id): echo "selected"; endif;?>><?php echo $item->name." - ".$item->province_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Alamat Domisili <i class="text-xs">(Diisi jika berbeda dengan KTP)</i><span class="text-danger"></span></label>
								<div class="col-sm-8">
									<textarea class="form-control" name="address_domisili" rows="3" placeholder="Contoh : Jl. Raya Nginden No.102, RT.001/RW.03, Baratajaya, Kec. Gubeng 60284" style="text-transform:uppercase"><?php echo $address_domisili; ?></textarea>
									<span class="text-success">Contoh : Jl. Raya Nginden No.102, RT.001/RW.03, Baratajaya, Kec. Gubeng 60284</span>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Kota Domisili <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="city_domisili_id">
										<option value="">-- PILIH KOTA --</option>
										<?php foreach ($list_city as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $city_domisili_id): echo "selected"; endif;?>><?php echo $item->name." - ".$item->province_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Status Tempat Tinggal <span class="text-danger"></span></label>
								<div class="col-sm-8">
									<select class="form-control select2" name="status_residance">
										<option value="MILIK SENDIRI" <?php if('MILIK SENDIRI' === $status_residance): echo "selected"; endif;?>>MILIK SENDIRI</option>
										<option value="ORANG TUA" <?php if('ORANG TUA' === $status_residance): echo "selected"; endif;?>>ORANG TUA</option>
										<option value="FAMILI" <?php if('FAMILI' === $status_residance): echo "selected"; endif;?>>FAMILI</option>
										<option value="KONTRAK" <?php if('KONTRAK' === $status_residance): echo "selected"; endif;?>>KONTRAK</option>
										<option value="KOS" <?php if('KOS' === $status_residance): echo "selected"; endif;?>>KOS</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nomor Telepon/HP <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" maxlength="14" pattern=".{10,14}"  title="10 to 14 number" class="form-control numeric" laceholder="Nomer Telepon" name="phone_number" value="<?php echo $phone_number; ?>">
									<span class="text-success">Nomor telepon dimulai dengan angka 0, Contoh : 081111111111</span>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Email <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="email" class="form-control" placeholder="Email" name="email" value="<?php echo $email; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Akun Sosial Media <span class="text-danger"></span></label>
								<div class="col-sm-3">
									<input type="text" class="form-control" placeholder="Facebook" name="socmed_fb" value="<?php echo $socmed_fb; ?>">
								</div>
								<div class="col-sm-3">
									<input type="text" class="form-control" placeholder="Instagram" name="socmed_ig" value="<?php echo $socmed_ig; ?>">
								</div>
								<div class="col-sm-3">
									<input type="text" class="form-control" placeholder="Twitter" name="socmed_tw" value="<?php echo $socmed_tw; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Agama <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="religion" required>
										<option value="ISLAM"  <?php if('ISLAM' === $religion): echo "selected"; endif;?>>ISLAM</option>
										<option value="KRISTEN" <?php if('KRISTEN' === $religion): echo "selected"; endif;?>>KRIST</option>
										<option value="KATOLIK" <?php if('KATOLIK' === $religion): echo "selected"; endif;?>>KATOLIK</option>
										<option value="HINDU" <?php if('HINDU' === $religion): echo "selected"; endif;?>>HINDU</option>
										<option value="BUDHA" <?php if('BUDHA' === $religion): echo "selected"; endif;?>>BUDHA</option>
										<option value="KONGHUCU" <?php if('KONGHUCU' === $religion): echo "selected"; endif;?>>KONGHUCU</option>
										<option value="ALIRAN KEPERCAYAAN" <?php if('ALIRAN KEPERCAYAAN' === $religion): echo "selected"; endif;?>>ALIRAN KEPERCAYAAN</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Status Pernikahan <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="marital_status" required>
										<option value="BELUM KAWIN" <?php if('BELUM KAWIN' === $marital_status): echo "selected"; endif;?>>BELUM KAWIN</option>
										<option value="KAWIN" <?php if('KAWIN' === $marital_status): echo "selected"; endif;?>>KAWIN</option>
										<option value="CERAI HIDUP" <?php if('CERAI HIDUP' === $marital_status): echo "selected"; endif;?>>CERAI HIDUP</option>
										<option value="CERAI MATI" <?php if('CERAI MATI' === $marital_status): echo "selected"; endif;?>>CERAI MATI</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Foto Surat Nikah <span class="text-danger"><i class="text-xs "></i></span></label>
								<div class="custom-file col-sm-7 file-upload">
									<input type="file" class="custom-file-input upload-document" name="document_marriage_certificate" accept="image/png, image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_marriage_certificate != ''): ?>
										<a href="<?php echo $document_marriage_certificate; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Jenis Kelamin <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="gender" required>
										<option value="LAKI LAKI" <?php if('LAKI LAKI' === $gender): echo "selected"; endif;?>>LAKI LAKI</option>
										<option value="PEREMPUAN" <?php if('PEREMPUAN' === $gender): echo "selected"; endif;?>>PEREMPUAN</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tinggi & Berat Badan <span class="text-danger"></span></label>
								<div class="col-sm-2">
									<input type="text" class="form-control numeric" placeholder="Tinggi Badan" name="heigh" value="<?php echo $heigh; ?>">
								</div>
								<label class="col-sm-3 col-form-label">CM</label>
								<div class="col-sm-2">
									<input type="text" class="form-control numeric" placeholder="Berat Badan" name="weigh" value="<?php echo $weigh; ?>">
								</div>
								<label class="col-sm-2 col-form-label">KG</label>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Ukuran Seragam & Sepatu<span class="text-danger"></span></label>
								<div class="col-sm-2">
									<select class="form-control select2" name="clothing_size">
										<option value="">-- UKURAN SERAGAM --</option>
										<option value="S" <?php if('S' === $clothing_size): echo "selected"; endif;?>>S</option>
										<option value="M" <?php if('M' === $clothing_size): echo "selected"; endif;?>>M</option>
										<option value="L" <?php if('L' === $clothing_size): echo "selected"; endif;?>>L</option>
										<option value="XL" <?php if('XL' === $clothing_size): echo "selected"; endif;?>>XL</option>
										<option value="XXL" <?php if('XXL' === $clothing_size): echo "selected"; endif;?>>XXL</option>
										<option value="3XL" <?php if('3XL' === $clothing_size): echo "selected"; endif;?>>3XL</option>
									</select>
								</div>
								<label class="col-sm-3 col-form-label"></label>
								<div class="col-sm-2">
									<select class="form-control select2" name="shoe_size">
										<option value="">-- UKURAN SEPATU --</option>
										<option value="35" <?php if('35' === $shoe_size): echo "selected"; endif;?>>35</option>
										<option value="36" <?php if('36' === $shoe_size): echo "selected"; endif;?>>36</option>
										<option value="37" <?php if('37' === $shoe_size): echo "selected"; endif;?>>37</option>
										<option value="38" <?php if('38' === $shoe_size): echo "selected"; endif;?>>38</option>
										<option value="39" <?php if('39' === $shoe_size): echo "selected"; endif;?>>39</option>
										<option value="40" <?php if('40' === $shoe_size): echo "selected"; endif;?>>40</option>
										<option value="41" <?php if('41' === $shoe_size): echo "selected"; endif;?>>41</option>
										<option value="42" <?php if('42' === $shoe_size): echo "selected"; endif;?>>42</option>
										<option value="43" <?php if('43' === $shoe_size): echo "selected"; endif;?>>43</option>
										<option value="44" <?php if('44' === $shoe_size): echo "selected"; endif;?>>44</option>
										<option value="45" <?php if('45' === $shoe_size): echo "selected"; endif;?>>45</option>
										<option value="46" <?php if('46' === $shoe_size): echo "selected"; endif;?>>46</option>
									</select>
								</div>
								<label class="col-sm-2 col-form-label"></label>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Pendidikan Terakhir <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="education_level">
										<option value="SD" <?php if('SD' === $education_level): echo "selected"; endif;?>>SD Sederajat</option>
										<option value="SMP" <?php if('SMP' === $education_level): echo "selected"; endif;?>>SMP Sederajat</option>
										<option value="SMA" <?php if('SMA' === $education_level): echo "selected"; endif;?>>SMA Sederajat</option>
										<option value="D1" <?php if('D1' === $education_level): echo "selected"; endif;?>>D1</option>
										<option value="D2" <?php if('D2' === $education_level): echo "selected"; endif;?>>D2</option>
										<option value="D3" <?php if('D3' === $education_level): echo "selected"; endif;?>>D3</option>
										<option value="D4" <?php if('D4' === $education_level): echo "selected"; endif;?>>D4</option>
										<option value="S1" <?php if('S1' === $education_level): echo "selected"; endif;?>>S1</option>
										<option value="S2" <?php if('S2' === $education_level): echo "selected"; endif;?>>S2</option>
										<option value="S3" <?php if('S3' === $education_level): echo "selected"; endif;?>>S3</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Jurusan </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Mesin/Perkantoran/Perhotelan" name="education_majors" value="<?php echo $education_majors; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<h2 class="card-title"><strong>B. PENEMPATAN<span class="text-danger"><i class="text-xs "></i></span></strong></h2>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Site Bisnis <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="site_id" required="">
										<!-- < ?php if($type == 'direct'): ?> -->
										<option value="">-- PILIH SITE BISNIS--</option>
										<option value="2" <?php if('2' === $site_id): echo "selected"; endif;?>>INDIRECT</option>
										<?php foreach ($list_site as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $site_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
										<!-- < ?php else: ?> -->
										<!-- < ?php endif; ?> -->
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Unit Kontrak <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="company_id" required="">
										<option value="">-- PILIH UNIT KONTRAK--</option>
										<?php foreach ($list_company as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $company_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Branch <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="branch_id">
										<option value="">-- PILIH BRANCH--</option>
										<?php foreach ($list_branch as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $branch_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Jabatan <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="position_id">
										<option value="">-- PILIH JABATAN--</option>
										<?php foreach ($list_position as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $position_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<h2 class="card-title"><strong>C. ANGGOTA KELUARGA<span class="text-danger"><i class="text-xs "></i></span></strong></h2>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Foto Kartu Keluarga</label>
								<div class="custom-file col-sm-7 file-upload">
									<input type="file" class="custom-file-input upload-document" name="document_family_card" accept="image/png, image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_family_card != ''): ?>
										<a href="<?php echo $document_family_card; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Ibu Kandung</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nama Ibu Kandung" name="family_mother" value="<?php echo $family_mother; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Pasangan </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nama Suami/Istri" name="family_mate" value="<?php echo $family_mate; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Anak </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nama Anak" name="family_child" value="<?php echo $family_child; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Kode PTKP </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="ptkp">
										<option value=""> </option>
										<?php foreach ($list_tax as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $ptkp): echo "selected"; endif;?>><?php echo $item->code." - ".$item->description; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<h2 class="card-title"><strong>D. BENEFIT</strong></h2>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Program BPJS <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<?php $labor = explode(",", $benefit_labor_note); ?>
									<select multiple class="form-control select2" name="benefit_labor_choice[]" placeholder="Program BPJS  Ketenagakerjaan">
										<option value="">&nbsp;</option>
										<option value="JKK" <?php if(in_array('JKK', $labor)): echo "selected"; endif;?>>JKK</option>
										<option value="JKM" <?php if(in_array('JKM', $labor)): echo "selected"; endif;?>>JKM</option>
										<option value="JHT" <?php if(in_array('JHT', $labor)): echo "selected"; endif;?>>JHT</option>
										<option value="JP" <?php if(in_array('JP', $labor)): echo "selected"; endif;?>>JP</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">BPJS Ketenagakerjaan</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nomor BPJS  Ketenagakerjaan" name="benefit_labor" value="<?php echo $benefit_labor; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Layanan Asuransi Kesehatan </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="BPJS Kesehatan" name="benefit_health_note" value="<?php echo $benefit_health_note; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nomor Asuransi Kesehatan </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nomor Asuransi Kesehatan" name="benefit_health" value="<?php echo $benefit_health; ?>">
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<h2 class="card-title"><strong>E. KEAHLIAN YANG DIKUASAI</strong></h2>
							</div>
							<div id="skill">
								<?php 
								if(count($list_skill) > 0):
									foreach ($list_skill as $item): 
								?>
								<div class="form-group row skill_delete">
									<div class="col-sm-1">
										<a class="btn btn-danger btn-block" onclick="remove_ajax(this, <?php echo $item->id; ?>, 'skill_delete');"><i class="fas fa-minus"></i></a>
										<input type="hidden" class="form-control" name="skill_id[]" value="<?php echo $item->id; ?>">
									</div>
									<div class="col-sm-6">
										<input type="text" class="form-control" placeholder="Nama Pelatihan" name="skill[]" value="<?php echo $item->certificate_name; ?>" style="text-transform:uppercase">
									</div>
									<div class="col-sm-5">
										<input type="text" class="form-control" placeholder="Nomor Sertifikat" name="certificate_number[]" value="<?php echo $item->certificate_number; ?>" style="text-transform:uppercase">
									</div>
									<div class="form-group col-sm-12 row mt-3">
										<div class="col-sm-1">
											<?php if($item->certificate_document != ''): ?>
												<a href="<?php echo $item->certificate_document; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
											<?php endif; ?>
										</div>
										<div class="custom-file col-sm-11">
											<input type="file" class="custom-file-input upload-document" name="skill_certificate[]" accept="image/png, image/jpeg, image/jpg">
											<label class="custom-file-label" for="customFile">Pilih file</label>
										</div>
									</div>
								</div>
								<?php 
								endforeach;
								else: 
								?>
								<div class="form-group row">
									<div class="col-sm-1">
										<input type="hidden" class="form-control" name="skill_id[]">
									</div>
									<div class="col-sm-6">
										<input type="text" class="form-control" placeholder="Nama Pelatihan" name="skill[]" value="" style="text-transform:uppercase">
									</div>
									<div class="col-sm-5">
										<input type="text" class="form-control" placeholder="Nomor Sertifikat" name="certificate_number[]" value="" style="text-transform:uppercase">
									</div>
									<div class="form-group col-sm-12 row mt-3">
										<div class="col-sm-1">
										</div>
										<div class="custom-file col-sm-11">
											<input type="file" class="custom-file-input upload-document" name="skill_certificate[]" accept="image/png, image/jpeg, image/jpg">
											<label class="custom-file-label" for="customFile">Pilih file</label>
										</div>
									</div>
								</div>
								<?php endif;?>
							</div>
							<div class="form-group row">
								<div class="col-sm-3">
									<a class="btn btn-success btn-block add-skill"><i class="fas fa-plus">&nbsp;</i>Tambah</a>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<h2 class="card-title"><strong>F. INFORMASI TAMBAHAN</strong></h2>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Bank</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nama Bank" name="bank_name" value="<?php echo $bank_name; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nomor Rekening</label>
								<div class="col-sm-9">
									<input type="text" class="form-control numeric" placeholder="Nomor Rekening" name="bank_account" value="<?php echo $bank_account; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Atas Nama Rekening</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Atas Nama Rekening" name="bank_account_name" value="<?php echo $bank_account_name; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nomor NPWP</label>
								<div class="col-sm-9">
									<input type="text" class="form-control numeric" placeholder="Nomor NPWP" name="tax_number" value="<?php echo $tax_number; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Foto NPWP <span class="text-danger"><i class="text-xs "></i></span></label>
								<div class="custom-file col-sm-7 file-upload">
									<input type="file" class="custom-file-input upload-document" name="document_tax_number" accept="image/png, image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_tax_number != ''): ?>
										<a href="<?php echo $document_tax_number; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<hr>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" name="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		$('.upload-document').imageUploadResizer({
			max_width: 1200, // Defaults 1000
			max_height: 800, // Defaults 1000
			quality: 1, // Defaults 1
			do_not_resize: ['gif', 'svg'], // Defaults []
		});
		$('#reservationtime').on( 'keyup change', function () {
			var range = $(this).val();

			var date1 = new Date(range.substring(3, 5)+"/"+range.substring(0, 2)+"/"+range.substring(6, 10)); 
			var date2 = new Date(range.substring(16, 18)+"/"+range.substring(13, 15)+"/"+range.substring(19, 23)); 

			var Difference_In_Time = date2.getTime() - date1.getTime(); 
			var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
			$('#longterm').val(Difference_In_Days+" Hari");
		});
		$(".check-card-id").click(function() {
			var id_card = $('.id_card').val();
			$.ajax({
				method:"POST",
				dataType: "json",
				url:"<?php echo base_url('employee_contract/card'); ?>",
				data: {id_card:id_card},
				success:function(result)
				{
					alert(result.description);
					if(result.id != '0'){
						var url = "<?php echo base_url("employee_contract/form/".$type."/"); ?>"+result.id;
						console.log(url);
						window.location.replace(url);
					}
				}
			});
		});
		$(".add-skill").click(function() {
			var content = 
			'<div class="form-group row"><div class="col-sm-1"><a class="btn btn-danger btn-block" onclick="remove(this);"><i class="fas fa-minus"></i></a><input type="hidden" class="form-control" name="skill_id[]"></div>' +
			'<div class="col-sm-6"><input type="text" class="form-control" placeholder="Nama Pelatihan" name="skill[]" value="" style="text-transform:uppercase"></div>' +
			'<div class="col-sm-5"><input type="text" class="form-control" placeholder="Nomor Sertifikat" name="certificate_number[]" value="" style="text-transform:uppercase"></div>' +
			'<div class="form-group col-sm-12 row mt-3"><div class="col-sm-1"></div>' +
			'<div class="custom-file col-sm-11"><input type="file" class="custom-file-input upload-document" name="skill_certificate[]" accept="image/png, image/jpeg, image/jpg, .pdf"><label class="custom-file-label" for="customFile">Choose file</label></div>' +
			'</div></div>';

			$("#skill").append(content);
		});
	});

	function remove(el){
		$(el).parent().parent().remove();
	}

	function remove_ajax(el, id, group){
		if (confirm('Anda yakin untuk menghapus data  ini?')) 
		{
			if($('.'+group).length <2){
				alert('Maaf Minimal harus ada 1 data');
			}else{
				$.ajax({
					url: "<?php echo base_url('account/'); ?>"+group,
					type: "POST",
					cache: false,
					data:{
						id: id
					},
					success: function(response){
						var result = JSON.parse(response);
						if(result.code==200){
							$(el).parent().parent().remove();
						}
					}
				});
			}
		}
		return false;
	}
</script>
<style type="text/css">
	input[type="text" i], input[type="email" i], input[type="select" i], input[type="date" i], .select2-container, textarea {
    	margin-left: -7px;
	}
</style>
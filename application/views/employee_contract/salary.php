<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-12">
					<h1>Format Gaji <?php echo $employee->full_name; ?></h1>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-list"></i>&nbsp;Kontrak <?php echo $employee->full_name; ?></h3>
				</div>
				<div class="card-body">
					<?php if(isset($message)): echo $message; endif; ?>
					<div class="row form-group contract mt-3">
						<div class="row col-12">
							<label class="col-sm-3">Nama Karyawan</label>
							<label class="col-sm-9">:&nbsp;<?php echo $employee->full_name; ?> </label>
						</div>
						<div class="row col-12">
							<label class="col-sm-3">ID Karyawan </label>
							<label class="col-sm-9">:&nbsp;<?php echo $employee->id_card; ?> </label>
						</div>
						<div class="row col-12">
							<label class="col-sm-3">Nomer Induk Karyawan </label>
							<label class="col-sm-9">:<?php echo $employee->employee_number; ?> </label>
						</div>
					</div>
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4 format-salary" method="post" action="">
							<input type="hidden" class="form-control" name="employee_id" value="<?php echo $employee->id; ?>">
							<div id="detail">
								<?php 
								if(count($salary) > 0):
									foreach ($salary as $item): 
								?>
								<div class="form-group row detail_salary_delete">
									<div class="col-sm-1">
										<a class="btn btn-danger btn-block" onclick="remove_ajax(this, <?php echo $item->id; ?>, 'detail_salary_delete');"><i class="fas fa-minus"></i></a>
										<input type="hidden" class="form-control" name="detail_salary_id[]" value="<?php echo $item->id; ?>">
									</div>
									<div class="col-md-2">
										<select class="form-control select2 " name="detail_salary_category[]">
											<option value="pendapatan" <?php if($item->category  == 'pendapatan'): echo 'selected'; endif;?>>Pendapatan</option>
											<option value="potongan" <?php if($item->category  == 'potongan'): echo 'selected'; endif;?>>Potongan</option>
										</select>
									</div>
									<div class="col-sm-2 code">
										<input type="text" class="form-control" placeholder="Kode" name="detail_salary_code[]" value="<?php echo $item->code; ?>" required readonly>
									</div>
									<div class="col-sm-2">
										<input type="text" class="form-control" placeholder="Keterangan" value="<?php echo $item->name; ?>" name="detail_salary_name[]" onkeyup="salaryName(this);" required>
									</div>
									<div class="col-sm-2">
										<select class="form-control select2 " onchange="salaryType(this);" name="detail_salary_type[]">
											<option value="nominal" <?php if($item->type == 'nominal'): echo 'selected'; endif;?>>Nominal</option>
											<option value="persentase" <?php if($item->type == 'persentase'): echo 'selected'; endif;?>>Persentase</option>
										</select>
									</div>
									<div class="col-sm-3 value" <?php if($item->type == 'persentase'): echo 'hidden'; endif;?>>
										<input type="text" class="form-control numeric rupiah" placeholder="Nominal" name="detail_salary_value[]" value="<?php echo $item->value; ?>">
									</div>
									<div class="col-sm-2 reference" <?php if($item->type == 'nominal'): echo 'hidden'; endif;?>>
										<select class="form-control select2 " name="detail_salary_reference[]">
											<?php foreach ($salary as $salary_list): ?>
											<option value="<?php echo $salary->id; ?>" <?php if($salary_list->id == $item->salary_detail_id): echo 'selected'; endif;?>><?php echo $salary_list->code."-".$salary_list->value; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
									<div class="col-sm-1 percentage" <?php if($item->type == 'nominal'): echo 'hidden'; endif;?>>
										<input type="text" class="form-control" placeholder="Persentase" name="detail_salary_percentage[]" value="<?php echo $item->percentage; ?>" onkeyup="salaryPercentage(this);">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-1">
										<input type="hidden" class="form-control" name="detail_salary_id[]">
									</div>
									<div class="col-md-2">
										<select class="form-control select2 " name="detail_salary_category[]">
											<option value="pendapatan">Pendapatan</option>
											<option value="potongan">Potongan</option>
										</select>
									</div>
									<div class="col-sm-2 code">
										<input type="text" class="form-control" placeholder="Kode" name="detail_salary_code[]" value="" required readonly>
									</div>
									<div class="col-sm-2">
										<input type="text" class="form-control" placeholder="Keterangan" value="" name="detail_salary_name[]" onkeyup="salaryName(this);" required>
									</div>
									<div class="col-sm-2">
										<select class="form-control select2 " onchange="salaryType(this);" name="detail_salary_type[]">
											<option value="nominal">Nominal</option>
											<option value="persentase">Persentase</option>
										</select>
									</div>
									<div class="col-sm-3 value">
										<input type="text" class="form-control numeric rupiah" placeholder="Nominal" name="detail_salary_value[]" required>
									</div>
									<div class="col-sm-2 reference" hidden>
										<select class="form-control select2 " name="detail_salary_reference[]">
											<?php foreach ($list_detail_salary as $item): ?>
											<option value="<?php echo $item->id; ?>"><?php echo $item->code."-".$item->value; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
									<div class="col-sm-1 percentage" hidden>
										<input type="text" class="form-control" placeholder="Persentase" name="detail_salary_percentage[]"  onkeyup="salaryPercentage(this);">
									</div>
								</div>
								<?php 
								endforeach;
								else: 
								?>
								<div class="form-group row">
									<div class="col-md-1">
										<input type="hidden" class="form-control" name="detail_salary_id[]">
									</div>
									<div class="col-md-2">
										<select class="form-control select2 " name="detail_salary_category[]">
											<option value="pendapatan">Pendapatan</option>
											<option value="potongan">Potongan</option>
										</select>
									</div>
									<div class="col-sm-2 code">
										<input type="text" class="form-control" placeholder="Kode" name="detail_salary_code[]" value="" required readonly>
									</div>
									<div class="col-sm-2">
										<input type="text" class="form-control" placeholder="Keterangan" value="" name="detail_salary_name[]" onkeyup="salaryName(this);" required>
									</div>
									<div class="col-sm-2">
										<select class="form-control select2 " onchange="salaryType(this);" name="detail_salary_type[]">
											<option value="nominal">Nominal</option>
											<option value="persentase">Persentase</option>
										</select>
									</div>
									<div class="col-sm-3 value">
										<input type="text" class="form-control numeric rupiah" placeholder="Nominal" name="detail_salary_value[]" required>
									</div>
									<div class="col-sm-2 reference" hidden>
										<select class="form-control select2 " name="detail_salary_reference[]">
											<?php foreach ($list_detail_salary as $item): ?>
											<option value="<?php echo $item->id; ?>"><?php echo $item->code."-".$item->value; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
									<div class="col-sm-1 percentage" hidden>
										<input type="text" class="form-control" placeholder="Persentase" name="detail_salary_percentage[]"  onkeyup="salaryPercentage(this);">
									</div>
								</div>
								<?php endif;?>
							</div>
							<div class="form-group row">
								<div class="col-sm-3">
									<a class="btn btn-success btn-block add-detail"><i class="fas fa-plus"></i>&nbsp;Tambah</a>
								</div>
							</div>
							<hr>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
				</form>
			</div>
		</div>
	</section>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$(".add-detail").click(function() {
			$(".format-salary").submit();
		});
	});
	
	function salaryType(el){
		var value = $(el).val();
		if(value == 'nominal'){
			$(el).parent().siblings('.value').removeAttr('hidden');
			$(el).parent().siblings('.reference').attr('hidden', 'true');
			$(el).parent().siblings('.percentage').attr('hidden', 'true');
		}else if(value == 'persentase'){
			$(el).parent().siblings('.value').attr('hidden', 'true');
			$(el).parent().siblings('.reference').removeAttr('hidden');
			$(el).parent().siblings('.percentage').removeAttr('hidden');
		}
	}

	function salaryName(el){
		var value = $(el).val();
		value = value.replace(/[^a-z0-9 ]+/gi, "");
		$(el).val(value);

		var code = value.split(' ').join('_');
		$(el).parent().siblings('.code').children().val(code.toUpperCase());
	}

	function salaryPercentage(el){
		var value = $(el).val();
		value = value.replace(/[^0-9.]+/gi, "");
		$(el).val(value);
	}
	
	function remove(el){
		$(el).parent().parent().remove();
	}

	function remove_ajax(el, id, group){
		if (confirm('Anda yakin untuk menghapus data  ini?')) 
		{
			if($('.'+group).length <2){
				alert('Maaf Minimal harus ada 1 data');
			}else{
				$.ajax({
					url: "<?php echo base_url('salary/'); ?>"+group,
					type: "POST",
					cache: false,
					data:{
						id: id
					},
					success: function(response){
						var result = JSON.parse(response);
						if(result.code==200){
							$(el).parent().parent().remove();
						}
					}
				});
			}
		}
		return false;
	}

	function preview_contract(el)
	{
		const url = $(el).attr('data-href');
	    const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
	    const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

	    const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	    const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	    const systemZoom = width / window.screen.availWidth;
	    const left = (width - 1000) / 2 / systemZoom + dualScreenLeft
	    const top = (height - 800) / 2 / systemZoom + dualScreenTop
	    const newWindow = window.open(url, 'preview', 
	      `
	      scrollbars=yes,
	      width=${1000 / systemZoom}, 
	      height=${800 / systemZoom}, 
	      top=${top}, 
	      left=${left}
	      `
	    )
	  return false;
	};

	function create_pdf(el)
	{
		const url = $(el).attr('data-href');
	    window.open(url);
	}
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('employee_contract/list/'.$type); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-10">
							<form method="post" action="<?php echo base_url("employee_contract/import/".$type); ?>" enctype="multipart/form-data">
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input btn-sm" name="file" accept=".xlsx" required> 
										<label class="custom-file-label" for="exampleInputFile">Import Data</label>
									</div>
									<div class="input-group-append">
										<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>   
									</div>
								</div>
							</form>
						</div>
						<div class="form-group col-sm-2">
							<a class="btn btn-success" href="<?php echo base_url('files/pengajuan.xlsx');?>">Template</i></a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-10">
							<form method="post" action="<?php echo base_url("employee_contract/import_contract/".$type); ?>" enctype="multipart/form-data">
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input btn-sm" name="file" accept=".xlsx" required> 
										<label class="custom-file-label" for="exampleInputFile">Import Kontrak</label>
									</div>
									<div class="input-group-append">
										<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>   
									</div>
								</div>
							</form>
						</div>
						<div class="form-group col-sm-2">
							<a class="btn btn-success" href="<?php echo base_url('files/template_kontrak.xlsx');?>">Template Kontrak</i></a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-2">
							<a href="<?php echo base_url("employee_contract/form/".$type); ?>" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah Karyawan</a>&nbsp;
							<!-- <a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp; -->
						</div>
						<div class="col-sm-2 submission" hidden="true">
							<select class="form-control select2" id="template" required>
								<?php foreach ($list_template as $item) :  ?>
								<option value="<?php echo $item->id;?>"><?php echo
									$item->title.' - '.strtoupper($item->branch_name); ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="col-sm-2">
							<input type="text" class="form-control float-right submission" id="reservationtime" required hidden="true">
						</div>
						<div class="col-sm-1">
							<input type="text" class="form-control float-right submission" id="longterm" readonly="" hidden="true">
						</div>
						<div class="col-sm-5 submission" hidden="true">
							<a href="#" class="btn btn-success export" style="color:white;width:110px;"><i class="fas fa-file-export"></i> Export</a>
							<a onclick="preview_contract(this)" class="btn btn-info" data-href="<?php echo base_url('employee_contract/preview_contract'); ?>" style="color:white;width:110px;"><i class="fas fa-eye"></i> Lihat</a>
							<a class="btn btn-default pdf" style="width:110px;"><i class="fas fa-file-pdf"></i> PDF</a>
							<a class="btn btn-danger nonjob" style="color:white;width:110px;"><i class="fas fa-ban"></i> NonJob</a>
							<a class="btn btn-primary save" style="color:white;width:100px;"><i class="fas fa-save"></i> Simpan</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-sm-2">
							<strong>Pilih data yang ditampilkan :</strong>
						</div>
						<div class="col-sm-3">
							<select class="form-control select2 expired float-right">
								<option value="<?php echo base_url("employee_contract/list/".$type."/");?>" <?php if($expired == "" ): echo "selected"; endif; ?>>Semua</option>
								<option value="<?php echo base_url("employee_contract/list/".$type."/expired");?>" <?php if($expired == "expired" ): echo "selected"; endif; ?>>Kontrak Habis</option>
								<?php
								for($i=1; $i <= 30;$i++):
								?>
									<option value="<?php echo base_url("employee_contract/list/".$type."/".$i);?>" <?php if($expired == $i ): echo "selected"; endif; ?>>Kontrak Kurang <?php echo $i; ?> Hari</option>
								<?php
								endfor;
								?>
							</select>
						</div>
						<div class="col-sm-7">
						</div>
					</div>
					<p>&nbsp; </p>
					<form method="POST" id="contract" action="<?php echo base_url('employee_contract/save_contract'); ?>">
					<input type="text" name="template_id" id="template_id" value="" hidden="">
					<input type="text" name="duration" id="duration" value="" hidden="">
						<div class="scroll-panel">
							<table id="data-table" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
										<th id='id_card'>Nomor KTP</th>
										<th id='employee_number'>ID Karyawan</th>
										<th id='full_name'>Nama Lengkap</th>
										<th id='company_name'>Unit Kontrak</th>
										<th id='site_name'>Site Bisnis</th>
										<th id='branch_name'>Branch</th>
										<th id='business_name'>Unit Bisnis</th>
										<th id='position'>Jabatan</th>
										<th id='ptkp'>PTKP</th>
										<th id='contract_type'>Tipe Kontrak</th>
										<th id='contract_end'>Akhir Kontrak</th>
										<th width="80px">#</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.expired').on('change', function() {
			window.location.href = this.value;
		});

		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("employee_contract/list_ajax/".$type."/".$expired);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'id_card'},
				{ data: 'employee_number' },
				{ data: 'full_name' },
				{ data: 'company_code' },
				{ data: 'site_name' },
				{ data: 'branch_name' },
				{ data: 'business_code' },
				{ data: 'position' },
				{ data: 'ptkp' },
				{ data: 'contract_type' },
				{ data: 'contract_end' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[3, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<11){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});
		$(document).on("change", ".check-id", function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});
		$("input:checkbox").change(function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});
		$( "#site_id" ).change(function() {
			$(".site_id").val($(this).val());
		});
		$( ".btn.submission" ).click(function() {
			$( "#submission" ).submit();
		});

		$('#reservationtime').on( 'keyup change', function () {
			var range = $(this).val();

			var date1 = new Date(range.substring(3, 5)+"/"+range.substring(0, 2)+"/"+range.substring(6, 10)); 
			var date2 = new Date(range.substring(16, 18)+"/"+range.substring(13, 15)+"/"+range.substring(19, 23)); 

			var Difference_In_Time = date2.getTime() - date1.getTime(); 
			var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
			$('#longterm').val(Difference_In_Days+" Hari");
		});

		$(".export" ).click(function() {
			$('#template_id').val($("#template").val());
			$('#duration').val($("#reservationtime").val());
			$('#contract').attr('action', "<?php echo base_url('employee_contract/export_contract/'.$type); ?>").submit();
		});

		$(".save" ).click(function() {
			$( "#template_id").val($( "#template").val());
			$( "#duration").val($( "#reservationtime").val());
			$( "#contract" ).attr('action', "<?php echo base_url('employee_contract/save_contract'); ?>").submit();
		});

		$(".pdf" ).click(function() {
			$( "#template_id").val($( "#template").val());
			$( "#duration").val($( "#reservationtime").val());
			$( "#contract" ).attr('action', "<?php echo base_url('employee_contract/pdf_contract/'.$type); ?>").submit();
		});

		$(".nonjob" ).click(function() {
			$( "#contract" ).attr('action', "<?php echo base_url('employee_contract/nonjob/'.$type); ?>").submit();
		});
	});

	function preview_contract(el)
	{
		var id = '';
		var i = 0;
		$("input:checkbox.check-id:checked").each(function(){
			if(i > 0){
				id = id + ','; 
			}
			id = id +''+$(this).val();
			i++;
		});
		var template = $('#template').val();
		var duration =  $('#reservationtime').val().replace(" ", "").replace(" ", "");
		const url = $(el).attr('data-href')+'?id='+id+'&duration='+duration+'&template='+template;
	    const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
	    const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

	    const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	    const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	    const systemZoom = width / window.screen.availWidth;
	    const left = (width - 1000) / 2 / systemZoom + dualScreenLeft
	    const top = (height - 800) / 2 / systemZoom + dualScreenTop
	    const newWindow = window.open(url, 'preview', 
	      `
	      scrollbars=yes,
	      width=${1000 / systemZoom}, 
	      height=${800 / systemZoom}, 
	      top=${top}, 
	      left=${left}
	      `
	    )
	  return false;
	};

	// function create_pdf(el)
	// {
	// 	if($('input[type="checkbox"]').length > 51){
	// 		alert("Maksimal 50 checklist ")
	// 		return false;
	// 	}
	// 	var id = '';
	// 	var i = 0;
	// 	$("input:checkbox.check-id:checked").each(function(){
	// 		if(i > 0){
	// 			id = id + ','; 
	// 		}
	// 		id = id +''+$(this).val();
	// 		i++;
	// 	});
	// 	var template = $('#template').val();
	// 	var duration =  $('#reservationtime').val().replace(" ", "").replace(" ", "");
	// 	const url = $(el).attr('data-href')+'?id='+id+'&duration='+duration+'&template='+template;
	//     window.open(url);
	// }

	// function nonjob(el)
	// {
	// 	var id = '';
	// 	var i = 0;
	// 	$("input:checkbox.check-id:checked").each(function(){
	// 		if(i > 0){
	// 			id = id + ','; 
	// 		}
	// 		id = id +''+$(this).val();
	// 		i++;
	// 	});
	// 	const url = $(el).attr('data-href')+'?id='+id
	//     window.location = url;
	// }
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('payroll_report/site'); ?>">Daftar Site Bisnis</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Site Bisnis</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("payroll_report/site_form"); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Bisnis Unit</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" value="<?php echo $company_code; ?>" disabled>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Kode Bisnis Unit</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" value="<?php echo $site_code; ?>" disabled>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nama Bisnis Unit</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" value="<?php echo $site_name; ?>" disabled>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Alamat Site Bisnis</label>
								<div class="col-sm-9">
									<textarea class="form-control" rows="3" placeholder="Alamat Site Bisnis" disabled><?php echo $site_address; ?></textarea>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">No Proyek</label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="site_id" value="<?php echo $site_id; ?>">
									<input type="text" class="form-control" name="project_number" value="<?php echo $project_number; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nama Pelanggan</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="customer_name" value="<?php echo $customer_name; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Kode Proyek</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="project_code" value="<?php echo $project_code; ?>">
								</div>
							</div>

							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var last_valid_selection = null;
		$('#pic').change(function(event) {
			if ($(this).val().length > 3) {
				$(this).val(last_valid_selection);
			} else {
				last_valid_selection = $(this).val();
			}
		});
	});
</script>
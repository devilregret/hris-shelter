<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
 		<?php echo asset_js("plugins/jquery/jquery.min.js"); ?>	
		<?php echo asset_css("css/adminlte.css"); ?>
		<?php echo asset_css("css/main.css"); ?>

	</head>
	<body>
		<section class="content">
			<div class="container-fluid">
				<div class="card card-primary card-outline col-12 mt-2">
					<div class="card-header">
						<h3 class="card-title"><i class="fas fa-eye"></i>&nbsp;History Pengajuan Pelamar</h3>
					</div>
					<div class="col-12 table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<td width="20px">&nbsp</td>
									<td width="200px">Id</td>
									<td>: <?php echo $employee->id; ?></td>
								</tr>
								<tr>
									<td width="20px">&nbsp</td>
									<td width="200px">NIK</td>
									<td>: <?php echo $employee->id_card; ?></td>
								</tr>
								<tr>
									<td>&nbsp</td>
									<td>Nama Lengkap</td>
									<td>: <?php echo $employee->full_name; ?></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="card-body">
						<div class="scroll-panel">
							<table id="data-table" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th width="50px">No</th>
										<th>Tanggal</th>
										<th>Status</th>
										<th>Catatan</th>
										<th>Bisnis Unit</th>
										<th>Site Bisnis</th>
										<th>Oleh</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php echo asset_js("plugins/datatables/jquery.dataTables.min.js"); ?>
		<?php echo asset_js("plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"); ?>
		<?php echo asset_js("plugins/datatables-responsive/js/dataTables.responsive.min.js"); ?>

		<script type="text/javascript">
			$(document).ready(function(){
				$('#data-table').DataTable({
					'autoWidth'		: false,
					'processing'	: true,
					'serverSide'	: true,
					'paging'		: false,
					'info'			: false,
					'serverMethod'	: 'post',
					'ajax': {
						'url':'<?php echo base_url("approval/approval_history_ajax/".$employee_id);?>'
					},
					'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
					'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
					'columns': [
						{ data: 'no', 'orderable':false, 'searchable':false },
						{ data: 'date_approval' },
						{ data: 'status_approval' },
						{ data: 'note' },
						{ data: 'company_name' },
						{ data: 'site_name' },
						{ data: 'submit_by' }
					],
					'order': [[1, 'desc']]
				});
			});
		</script>
	</body>
</html>
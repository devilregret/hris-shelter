<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('rocandidate/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;List Pengajuan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-8">
							<form method="post" action="<?php echo base_url("rocandidate/import"); ?>" enctype="multipart/form-data">
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input btn-sm" name="file" accept=".xlsx" required> 
										<label class="custom-file-label" for="exampleInputFile">Import Data</label>
									</div>
									<div class="input-group-append">
										<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>   
									</div>
								</div>
							</form>
						</div>
						<div class="form-group col-sm-2">
							<a class="btn btn-success" href="<?php echo base_url('files/pengajuan_ro.xlsx');?>">Template</i></a>
						</div>
						<div class="col-sm-2 text-right">
							<a href="#" class="btn btn-danger cancel" hidden="true"><i class="fas fa-window-close"></i> Batalkan</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
						</thead>
					</table>
					<p>&nbsp;</p>
					<form method="POST" id="approval" action="<?php echo base_url('rocandidate/approval'); ?>">
					<input type="hidden" class="form-control action" name="action" required="">
					<table id="data-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
								<th width="50px">No</th>
								<th id='full_name'>Nama Lengkap</th>
								<th id='id_card'>KTP</th>
								<th id='age'>Usia</th>
								<th id='city_birth'>Tempat Lahir</th>
								<th id='date_birth'>Tanggal Lahir</th>
								<th id='address_domisili'>Alamat Domisili</th>
								<th id='position'>Jabatan</th>
								<th id='company_name'>Bisnis Unit</th>
								<th id='branch_name'>Area</th>
								<th width="80px">#</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
					</table>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("rocandidate/list_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'full_name' },
				{ data: 'id_card' },
				{ data: 'age' },
				{ data: 'city_birth' },
				{ data: 'date_birth' },
				{ data: 'address_domisili' },
				{ data: 'position' },
				{ data: 'company_name' },
				{ data: 'branch_name' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[2, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>1 && index<12){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$( ".export" ).click(function() {
			$.post( "export" );
			var url = "<?php echo base_url('submission/export'); ?>" + "?full_name="+$("#s_full_name").val()+"&id_card="+$("#s_id_card").val()+"&age="+$("#s_age").val()+"&education_level="+$("#s_education_level").val()+"&education_majors="+$("#s_education_majors").val()+"&city_domisili="+$("#s_city_domisili").val()+"&site_name="+$("#s_site_name").val()+"&certificate="+$("#s_certificate").val()+"&company_name="+$("#s_company_name").val()+"&position="+$("#s_position").val()+"&status=pengajuan_ke_client";
			window.location = url;
		
		});

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".cancel").prop('hidden', true);
			$(".approve").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".cancel").prop('hidden', false);
				$(".approve").prop('hidden', false);
			});
		});
		
		$("input:checkbox").change(function() {
			$(".cancel").prop('hidden', true);
			$(".approve").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".cancel").prop('hidden', false);
				$(".approve").prop('hidden', false);
			});
		});

		$( ".cancel" ).click(function() {
			$( ".action").val("reject");
			$( "#approval" ).submit();
		});
	});
</script>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/css/adminlte.min.css"); ?>
		<?php echo asset_js("plugins/jquery/jquery.min.js"); ?>	
	</head>
	<body>
		<div class="wrapper">
			<section class="psikotes-print">
			<?php echo asset_js("plugins/chart.js/Chart.min.js"); ?>
			<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
			<style>
				.maintable { 
					border-spacing: 5px;
					border-collapse: separate;
				}
				.color-title {
					background-color:#341f97;
					-webkit-print-color-adjust:exact;
				}
				.color-sub-title {
					background-color:#2e86de;
					-webkit-print-color-adjust:exact;
				}
				.color-content {
					background-color:#48dbfb;
					-webkit-print-color-adjust:exact;
				}
				.color-detail {
					background-color:#c8d6e5;
					-webkit-print-color-adjust:exact;
				}
				.color-canvas {
					background-color:white;
					-webkit-print-color-adjust:exact;
				}
				.content-gk {
					text-align:center;
					width:33.33333%;
					font-size:12pt;
				}
				.content-gk-detail {
					color:#341f97;
					font-size:12pt
				}
				.content-detail {
					text-align:center;
					font-size:14pt;
					vertical-align:top;
				}
			</style>
				<div class="row">
					<table style="width:100%" class="maintable">
						<tr class="color-title">
							<td colspan="3">
								<h4 style="color:white;text-align:center">Personality System Graph Page</h4>
							</td>
						</tr>
						<tr>
							<td colspan="3" class="color-content">
								<table style="width:100%;font-size:16pt">
									<tr>
										<td>&nbsp;&nbsp;&nbsp;&nbsp;Name : <?php echo $data_psikotes[0]->nama ?></td>
										<td>Gender : <?php echo $data_psikotes[0]->jk ?></td>
										<td>Age : <?php echo $data_psikotes[0]->usia ?> Tahun</td>
										<td>Date : <?php echo $data_psikotes[0]->tgl_tes ?></td>
									</tr>
									<tr>
										
									</tr>
								</table>
							</td>
						</tr>
						<tr class="color-sub-title">
							<td colspan="3">
								<h4 style="color:white;text-align:center">Gambaran Karakter</h4>
							</td>
						</tr>
						<tr>
							<td class="color-content content-gk">
								Most
								<br>
								<u><b>Mask Public Self (Luar)</b></u>
								<br>
								Kepribadian Dimuka Umum
								<br>
								<i class="content-gk-detail">Gambaran bagaimana orang lain melihat diri kita. Persepsi tentang kita dilihat dari sisi luar.</i>
							</td>
							<td class="color-content content-gk">
								Least
								<br>
								<u><b>Core Private Self (Dalam)</b></u>
								<br>
								Kepribadian Saat Mendapat Tekanan
								<br>
								<i class="content-gk-detail">Gambaran utuh tentang diri inti kita sesungguhnya.</i>
							</td>
							<td class="color-content content-gk">
								Change
								<br>
								<u><b>Mirror Perceived Self</b></u>
								<br>
								Kepribadian Asli Yang Tersembunyi
								<br>
								<i class="content-gk-detail">Gambaran bagaimana kita melihat diri kita sendiri.</i>
							</td>
						</tr>
						<tr>
							<?php foreach ($data_psikotes_result as $key => $element) { ?>
							<td class="color-content">
								<h5 style="color:#341f97;text-align:center">
									<i><?php echo $element->kepribadian ?></i>
								</h5>
							</td>
							<?php } ?>
							
						</tr>
						<tr>
							<?php foreach ($data_psikotes_result as $key => $element) { ?>
								<td class="color-detail content-detail">
									<?php echo $element->kepribadian_detail ?>
								</td>
							<?php } ?>
						</tr>
						<tr>
							<?php foreach ($data_psikotes_result_line as $key => $element) { ?>
								<td>
									<canvas id="chart-<?php echo $element->pk ?>" height="250" class="color-canvas"></canvas>
								</td>

								<script>
									var ctx = document.getElementById("chart-<?php echo $element->pk ?>");
									var min = -4;
									var max = 8;
									var label = "";
									if ("<?php echo $element->pk ?>" =="P") {
										label = "Most ( Mask Public Self )";
									}else if ("<?php echo $element->pk ?>" =="K") {
										min = -8;
										max = 6;
										label = "Least ( Core Private Self )";
									}else if ("<?php echo $element->pk ?>" =="PK") {
										label = "Change ( Mirror Percelved Self )";
									}
									var myChart = new Chart(ctx, {
										type: 'line',
										lineTension:0, //straight lines
										data: {
											labels: ['D', 'I', 'S', 'C'],
											datasets: [{
												label: label,
												lineTension:0, //straight lines
												fill: false,
												borderColor: ['#2e86de'],
												data: [<?php echo $element->convert_d.",".$element->convert_i.",".$element->convert_s.",".$element->convert_s ?>],
											}]
										},
										options: {
											plugins: {
												// Change options for ALL labels of THIS CHART
												datalabels: {
													color: '#000000',
												}
											},
											scales: {
												yAxes: [{
													display: true,
													ticks: {
														stepSize: 0.5,
														max: max,
														min:min
													},
													scaleLabel: {
														display:true
													}
												}]
											},
										}
									});
								</script>
							<?php } ?>
						</tr>
						<tr>
							<td colspan="3">
								<table style="width:100%">
									<tr class="color-sub-title">
										<td colspan="3" style="padding:5px">
											<h4 style="color:white;text-align:center">Deskripsi Kepribadian</h4>
										</td>
									</tr>
									<tr>
										<td class="color-detail content-detail" style="padding-top:10px;padding-bottom:10px">
											<?php echo $data_psikotes[0]->deskripsi_kepribadian ?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<table style="width:100%">
									<tr class="color-sub-title">
										<td colspan="3" style="padding:5px">
											<h4 style="color:white;text-align:center">Job Match</h4>
										</td>
									</tr>
									<tr>
										<td class="color-detail content-detail" style="padding-top:10px;padding-bottom:10px">
											<?php echo $data_psikotes[0]->job_match ?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<br>
					<br>
				</div>
			</section>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="printPsikotes();">Print</button>
			<button type="button" class="btn btn-default" onclick="window.close();">Tutup</button>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.main-sidebar').remove();
				$('.main-header').remove();
			})

			function printPsikotes() {
				$('.modal-footer').remove();
				window.print();
				window.close();
			}
		</script>
	</body>
</html>

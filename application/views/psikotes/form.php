<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<style>
		.table-identitas tr td {
			padding:1px !important;
			font-size:12px;
			border: none !important;
		}
		.tabel-isian {
			font-size:12px;
			text-align:center;
			border: 1px solid black;
		}
		.tabel-isian tr th {
			border: 1px solid black;
			padding:5px !important;
		}
		.tabel-isian tr td {
			border: 1px solid black;
			padding:5px !important;
		}
		#countdown {
			text-align: center;
			font-size: 30px;
			font-weight:bold;
		}
	</style>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<div class="row">
						<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form DISC Online</h3>
					</div>
					<br>
					<div style="display:none">
						<p id="countdown"></p>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-2" method="post" action="<?php echo base_url("psikotes/saveform"); ?>" enctype="multipart/form-data" onsubmit="return validateForm()">
							<input type="hidden" name="full_name" value="<?php echo $data_employee[0]->full_name ?>">
							<input type="hidden" name="umur" value="<?php echo $data_employee[0]->umur ?>">
							<input type="hidden" name="jk" value ="<?php echo $data_employee[0]->jk ?>">
							<input type="hidden" name="vacancy_id" value ="<?php echo $vacancy_id ?>">
							<input type="hidden" name="tgl_tes" value="<?php echo $data_vacancy[0]->tgl_tes ?>">
							<input type="hidden" name="tgl_selesai_psikotes" value="<?php echo $data_vacancy[0]->tgl_selesai_psikotes ?>">
							<input type="hidden" name="tes_mulai" value="<?php echo date('Y-m-d H:i:s'); ?>">
							<input type="hidden" id="jumlah_soal" value="<?php echo count($list_nomor_psikotes); ?>">

							<div class="form-group row">
								<div class="col-sm-4" style="background-color:#74b9ff;font-size:12px;text-align:justify;padding-top:10px">
									<table class="table table-identitas">
										<tbody>
											<tr>
												<td style="width:25%"><b>Nama</b></td>
												<td><b>&nbsp;:&nbsp;</b></td>
												<td><?php echo $data_employee[0]->full_name ?></td>
											</tr>
											<tr>
												<td style="width:25%"><b>Usia</b></td>
												<td><b>&nbsp;:&nbsp;</b></td>
												<td>
													<?php 
														if ($data_employee[0]->umur==null) {
															echo '-';
														} else {
															echo $data_employee[0]->umur;
														} ?> Tahun
												</td>
											</tr>
											<tr>
												<td style="width:25%"><b>Jenis Kelamin</b></td>
												<td><b>&nbsp;:&nbsp;</b></td>
												<td><?php echo $data_employee[0]->jk ?></td>
											</tr>
											<tr>
												<td style="width:25%"><b>Tanggal Tes</b></td>
												<td><b>&nbsp;:&nbsp;</b></td>
												<td><?php echo $data_employee[0]->stgl_tes ?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-sm-8" style="background-color:#74b9ff;font-size:12px;text-align:justify;padding-top:10px">
									<span>
									<b>INSTRUKSI :</b> Setiap nomor di bawah ini memuat 4 (empat) kalimat. Tugas anda adalah : 
									</span><br>
									<span>
									1. Beri tanda <b>[x]</b> pada kolom di bawah huruf  <b>[P]</b> di samping kalimat yang <b>PALING menggambarkan diri</b> anda
									</span><br>
									<span>
									2. Beri tanda <b>[x]</b> pada kolom di bawah huruf  <b>[K]</b> di samping kalimat yang <b>PALING TIDAK menggambarkan diri</b> anda
									</span><br>
									<span>
									<b>PERHATIKAN :</b> Setiap nomor hanya ada 1 (satu) tanda <b>[x]</b> di bawah masing-masing kolom <b>P</b> dan <b>K</b>.
									</span>
								</div>
							</div>
							<div class="form-group row">
							<?php foreach ($list_nomor_psikotes as $key => $element) { ?>
								<div class="col-sm-4">
									<table  class="table tabel-isian" style="width:100%">
									<thead>
										<tr>
											<th style="background-color:#dfe6e9">No.</th>
											<th style="width:10%;background-color:#ffeaa7">Paling</th>
											<th style="width:10%;background-color:#81ecec;display:none"></th>
											<th style="width:10%;background-color:#55efc4">Kurang</th>
											<th style="width:10%;background-color:#81ecec;display:none"></th>
											<th style="background-color:#d63031;color:white">Gambaran Diri</th>
										</tr>
									</thead>
									<tbody>
									<?php 
									foreach ($list_master_psikotes as $keyd => $detail) {
										if($detail->nomor == $element->nomor){
									?>
										<tr>
											<?php if($detail->poin==1) { ?>
												<td rowspan="4" style="vertical-align:middle;text-align:center;background-color:#dfe6e9"><b><?php echo $element->nomor; ?></b></td>
											<?php } ?>
											<td style="background-color:#ffeaa7"><input type="radio" class="radio_p" name="poin_<?php echo $detail->nomor ?>_p" value="<?php echo $detail->id ?>" data-poin="<?php echo $detail->poin ?>" id="poin_<?php echo $detail->nomor ?>_p_<?php echo $detail->poin ?>"></td>
											<td style="background-color:#81ecec;display:none"><span class="radio_p_text">0</span></td>
											<td style="background-color:#55efc4"><input type="radio" class="radio_k" name="poin_<?php echo $detail->nomor ?>_k" value="<?php echo $detail->id ?>" data-poin="<?php echo $detail->poin ?>" id="poin_<?php echo $detail->nomor ?>_k_<?php echo $detail->poin ?>"></td>
											<td style="background-color:#81ecec;display:none"><span class="radio_k_text">0</span></td>
											<td><?php echo $detail->gambaran_diri ?></td>
										</tr>
									<?php
										}
									} 
									?>
									</tbody>
									</table>
								</div>
							<?php } ?>
							</div>
							<div class="form-group row  float-right">
								<button type="button" onclick="window.history.back();" class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>	
		</div>
	</section>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
	function validateForm() {
		let jumlahSoal = $('#jumlah_soal').val();
		let msg = "";
		for (let i = 1; i <= jumlahSoal; i++) {
			let radioPName = 'poin_'+i+'_p';
			let radioKName = 'poin_'+i+'_k';
			let isPCek = false;
			let isKCek = false;
			let poinP = 0;
			let poinK = 99;
			if($('input[name='+radioPName+']').is(':checked')) {
				isPCek = true;
				let poin = $('input[name='+radioPName+']:checked').data('poin');
				poinP = poin;
			}
			if($('input[name='+radioKName+']').is(':checked')) {
				isKCek = true;
				let poin = $('input[name='+radioKName+']:checked').data('poin');
				poinK = poin;
			}

			if (!isPCek || !isKCek) {
				let kolom = "";
				if (!isPCek && isKCek) {
					kolom = "P";
				}
				if (isPCek && !isKCek) {
					kolom = "K";
				}
				if (!isPCek && !isKCek) {
					kolom = "P dan K";
				}
				msg += "Nomor <b>"+i+"</b> kolom <b>"+kolom+"</b> belum anda isi <br>";
			}

			if (poinP==poinK) {
				msg += "Poin <b>P dan K</b> pada nomor <b>"+i+"</b> tidak boleh sama <br>";
			}
		}

		if (msg!="") {
			Swal.fire({
				icon: 'warning',
				title: 'Pemberitahuan',
				html: msg,
			})
			return false;
		}

		if (confirm("Apakah anda ingin menyimpan tes ini ? ")) {
			return true;	
		}else{
			return false;
		}
		return true;
	}
	
	$(document).ready(function(){
		$('.radio_k').change(function() {
			let valradio = $(this).data('poin');
			let valp = $(this).closest('tr').find('.radio_p_text').text();
			if(valp>0){
				Swal.fire({
					icon: 'warning',
					title: "Pemberitahuan",
					html: "Tidak boleh dalam 1 baris yang sama",
				});
				$(this).prop('checked', false);
			}else{
				$(this).closest('tbody').find('tr .radio_k_text').text("0");
				$(this).closest('tr').find('.radio_k_text').text(valradio);
			};
		});
		$('.radio_p').change(function() {
			let valradio = $(this).data('poin');
			let valk = $(this).closest('tr').find('.radio_k_text').text();
			if(valk>0){
				Swal.fire({
					icon: 'warning',
					title: "Pemberitahuan",
					html: "Tidak boleh dalam 1 baris yang sama",
				});
				$(this).prop('checked', false);
			}else{
				$(this).closest('tbody').find('tr .radio_p_text').text("0");
				$(this).closest('tr').find('.radio_p_text').text(valradio);
			};
		});
	});
</script>
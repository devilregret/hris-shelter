<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('applicant/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Pengguna</h3>
				</div>
				<div class="card-body">
					<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("applicant/form"); ?>">
						<div class="form-group row">
							<label for="name" class="col-sm-3 col-form-label">Nama Lengkap <span class="text-danger"></span></label>
							<div class="col-sm-9">
								<input type="text" class="form-control" value="<?php echo $account->full_name; ?>" disabled>
							</div>
						</div>
						<div class="form-group row">
							<label for="name" class="col-sm-3 col-form-label">NIK/Username <span class="text-danger"></span></label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="username"  value="<?php echo $account->username; ?>" required>
							</div>
						</div>
						
						<div class="form-group row">
							<label for="name" class="col-sm-3 col-form-label">Password <span class="text-danger">*</span></label>
							<div class="col-sm-9">
								<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
								<input type="password" class="form-control" placeholder="password" name="password" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 col-form-label">Ulang Password <span class="text-danger">*</span></label>
							<div class="col-sm-9">
								<input type="password" class="form-control" placeholder="Ulang Password" name="repassword" required>
							</div>
						</div>
						<hr>
						<div class="form-group row  float-right">
							<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
							<button type="submit" name="submit" class="btn btn-primary btn-form">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
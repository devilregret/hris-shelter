<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('applicant/check'); ?>">Cek NIK</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="card card-primary card-outline col-12">
			<div class="card-header">
				<h3 class="card-title"><i class="fas fa-search"></i>&nbsp;Cek NIK</h3>
			</div>
			<div class="card-body">
				<?php if(isset($message)): echo $message; endif; ?>
				<form class="form-horizontal col-sm-12 mt-4" method="post" action="<?php echo base_url("applicant/check"); ?>" >
					<div class="form-group row">
						<label class="col-sm-3 col-form-label">Nomor KTP <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<input type="text" maxlength="16" pattern=".{16,16}"  title="16 number" class="form-control numeric id_card" placeholder="NIK" name="id_card" value="<?php echo $id_card; ?>" required>
						</div>
						<div class="col-sm-2">
							<button type="submit" name="submit" class="btn btn btn-warning btn-block"><i class="fas fa-search"></i> Cek Nomor KTP</button>
						</div>
					</div>
				</form>
			</div>
		</div>	
	</section>
	<?php if($employee): ?>
	<section class="content">
		<div class="card card-primary card-outline col-12">
			<?php if($user): ?>
			<div class="card-header">
				<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Akun Karyawan</h3>
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Nama Lengkap </label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->full_name; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Username </label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->id_card; ?>" readonly>
					</div>
				</div>
				<?php 
					$status = 'Karyawan';
					if($employee->status_approval == 0){
						$status = 'Kandidat';
					}else if($employee->status_approval == 1){
						$status = 'Pengajuan Client';
					}else if($employee->status_approval == 2){
						$status = 'Pengajuan Personalia';
					}else if($employee->status_nonjob == 1){
						$status = 'Nonjob';
					}
				?>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Status Akun </label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $status; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-9 col-form-label">Username </label>
					<div class="col-sm-3">
						<a href="<?php echo base_url('applicant/form/'.$user->id); ?>" class="btn btn btn-danger btn-block">Reset Password</a>
					</div>
				</div>
			</div>
			<hr/>
			<?php endif; ?>
			<div class="card-header">
				<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Data Karyawan</h3>
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">preview </label>
					<div class="col-sm-9">
						<?php if($employee->document_photo != ''): ?>
							<img id="preview" src="<?php echo $employee->document_photo; ?>" width="200px" alt="Foto" />
						<?php else: ?>
							<img id="preview" src="<?php echo asset_img("candidate.png"); ?>" width="200px" alt="Foto" />
						<?php endif; ?>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Foto KTP</label>
					<div class="col-sm-2">
						<?php if($employee->document_id_card != ''): ?>
							<a href="<?php echo $employee->document_id_card; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
						<?php endif; ?>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">No Pendaftaran </label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->registration_number; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">No Induk Karyawan </label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->employee_number; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Nama Lengkap</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->full_name; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Tempat Lahir</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->city_birth; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Tanggal Lahir</label>
					<div class="col-sm-2">
						<input type="date" class="form-control datepicker" value="<?php echo $employee->date_birth; ?>" readonly>
					</div>
					<div class="col-sm-7"></div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Alamat Asal </label>
					<div class="col-sm-9">
						<textarea class="form-control" rows="3" readonly><?php echo $employee->address_card; ?></textarea>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Kota Asal </label>
					<div class="col-sm-9">
						<input type="text" class="form-control " value="<?php echo $employee->city_card; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Alamat Domisili</label>
					<div class="col-sm-8">
						<textarea class="form-control" readonly><?php echo $employee->address_domisili; ?></textarea>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Kota Domisili </label>
					<div class="col-sm-9">
						<input type="text" class="form-control " value="<?php echo $employee->city_domisili; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Status Tempat Tinggal</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->status_residance; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Nomor Telepon/HP</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->phone_number; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Email</label>
					<div class="col-sm-9">
						<input type="email" class="form-control" value="<?php echo $employee->email; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Akun Sosial Media</label>
					<div class="col-sm-3">
						<input type="text" class="form-control"value="<?php echo $employee->socmed_fb; ?>" readonly>
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control" value="<?php echo $employee->socmed_ig; ?>" readonly>
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control" value="<?php echo $employee->socmed_tw; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Agama </label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->religion; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Status Pernikahan </label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->marital_status; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Foto Surat Nikah </label>
					<div class="col-sm-2">
						<?php if($employee->document_marriage_certificate != ''): ?>
							<a href="<?php echo $employee->document_marriage_certificate; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
						<?php endif; ?>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Jenis Kelamin</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->gender; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Tinggi & Berat Badan </label>
					<div class="col-sm-2">
						<input type="text" class="form-control numeric" value="<?php echo $employee->heigh; ?>" readonly>
					</div>
					<label class="col-sm-3 col-form-label">CM</label>
					<div class="col-sm-2">
						<input type="text" class="form-control numeric" value="<?php echo $employee->weigh; ?>" readonly>
					</div>
					<label class="col-sm-2 col-form-label">KG</label>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Ukuran Seragam & Sepatu</label>
					<div class="col-sm-2">
						<input type="text" class="form-control numeric" value="<?php echo $employee->clothing_size; ?>" readonly>
					</div>
					<label class="col-sm-3 col-form-label"></label>
					<div class="col-sm-2">
						<input type="text" class="form-control numeric" value="<?php echo $employee->shoe_size; ?>" readonly>
					</div>
					<label class="col-sm-2 col-form-label"></label>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Pendidikan Terakhir</label>
					<div class="col-sm-9">
						<input type="text" class="form-control numeric" value="<?php echo $employee->education_level; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Jurusan </label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->education_majors; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Site Bisnis </label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->site_name; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Unit Kontrak </label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->company_name; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Branch </label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->branch_name; ?>" readonly>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-form-label">Jabatan </label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $employee->position; ?>" readonly>
					</div>
				</div>
			</div>
		</div>	
	</section>
	<?php endif; ?>
</div>
<style type="text/css">
	input[type="text" i], input[type="email" i], input[type="select" i], input[type="date" i], .select2-container, textarea {
    	margin-left: -7px;
	}
</style>
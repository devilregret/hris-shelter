<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('applicant/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;List Pengguna</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>
					</div>
				</div>
				<div class="card-body">
					<table id="data-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th width="50px">No</th>
								<th id='created_at'>Tanggal Daftar </th>
								<th id='username'>Username </th>
								<th id='email'>Email</th>
								<th id='full_name'>Nama Lengkap</th>
								<th id='phone_number'>Nomo Telepon</th>
								<th id='city_domisili'>Kota Domisili</th>
								<th id='province_name'>Provinsi</th>
								<th id='applied_position'>Posisi dilamar</th>
								<th id='applied_for'>Perusahaan dilamar</th>
								<th width="105px">#</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("applicant/notcomplete_ajax");?>'
			},
			'columns': [
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'date_regitration' },
				{ data: 'username' },
				{ data: 'email' },
				{ data: 'full_name' },
				{ data: 'phone_number' },
				{ data: 'city_domisili' },
				{ data: 'province_name' },
				{ data: 'applied_position' },
				{ data: 'applied_for' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'order': [[1, 'desc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<10){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
	});

	$( ".export" ).click(function() {
			$.post( "export" );
			var url = "<?php echo base_url('applicant/export/notcomplete'); ?>" + "?username="+$("#s_username").val()+"&email="+$("#s_email").val()+"&full_name="+$("#s_full_name").val()+"&phone_number="+$("#s_phone_number").val()+"&city_domisili="+$("#s_city_domisili").val()+"&created_at="+$("#s_created_at").val()+"&province_name="+$("#s_province_name").val();
			window.location = url;
		
		});
</script>
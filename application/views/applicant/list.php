<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('applicant/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Pengguna</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<a href="#" class="btn btn-success export" style="margin-right:10px;"><i class="fas fa-file-export" ></i> Export Data</a>
						<a href="<?php echo base_url("applicant/generate"); ?>" class="btn btn-warning" ><i class="fas fa-cogs"></i> Generate User</a>
					</div>
				</div>
				<div class="card-body">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="50px">No</th>
									<th id='id_card'>No KTP </th>
									<th id='email'>Email</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='phone_number'>Nomo Telepon</th>
									<th id='city_domisili'>Kota Domisili</th>
									<th id='company_code'>Bisnis Unit</th>
									<th id='site_name'>Penempatan</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("applicant/list_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'id_card' },
				{ data: 'email' },
				{ data: 'full_name' },
				{ data: 'phone_number' },
				{ data: 'city_domisili' },
				{ data: 'company_code' },
				{ data: 'site_name' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[1, 'desc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<9){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
	});

	$( ".export" ).click(function() {
			var url = "<?php echo base_url('applicant/export_employee?'); ?>" + "id_card="+$("#s_id_card").val()+ "&username="+$("#s_username").val()+"&email="+$("#s_email").val()+"&full_name="+$("#s_full_name").val()+"&phone_number="+$("#s_phone_number").val()+"&city_domisili="+$("#s_city_domisili").val()+"&company_code="+$("#s_company_code").val()+"&site_name="+$("#s_site_name").val();
			window.location = url;
		
		});
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('candidate_temporary/courier/'.$type.'/'.$branch); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Pelamar Kurir</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row" >
						<div class="col-sm-3">
							<div class="submission" hidden="true">
								<strong>Tipe Followup :</strong>
								<select class="form-control select2 " id="status-followup">
									<option value="Lihat">Lihat</option>
									<option value="Hubungi">Hubungi</option>
									<option value="Proses">Proses</option>
									<option value="Tolak">Tolak</option>
									<option value="Blokir">Blokir</option>
								</select>
							</div>
						</div>
						<div class="col-sm-7 submission" hidden="true">
							<strong>Keterangan Followup :</strong>
							<textarea id="note-followup"  rows="2" placeholder="Tulis Catatan Followup" style="width:100%"; ></textarea>
						</div>
						<div class="col-sm-2 submission" hidden="true">
							<strong>&nbsp;</strong>
							<a class="btn btn-success followup btn-block"><i class="fas fa-phone"></i> Follow Up</a>
						</div>
						&nbsp;
					</div>
					<hr>
					<div class="row">
						<div class="col-sm-3">
							<strong>Pilih data yang ditampilkan :</strong>
						</div>
						<div class="col-sm-3">
							<select class="form-control preview float-right" id="branch">
								<?php foreach ($list_branch as $item): ?>
								<option value="<?php echo $item->id;?>" <?php if($branch == $item->id ): echo "selected"; endif; ?>>
									<?php echo $item->name; ?>
								</option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="col-sm-3">
							<select class="form-control preview float-right" id="type">
								<option value="semua" <?php if($type == 'all' ): echo "selected"; endif; ?>>Semua</option>
								<option value="lihat" <?php if($type == 'lihat' ): echo "selected"; endif; ?>>Dilihat</option>
								<option value="hubungi" <?php if($type == 'hubungi' ): echo "selected"; endif; ?>>Dihubungi</option>
								<option value="proses" <?php if($type == 'proses' ): echo "selected"; endif; ?>>Diproses</option>
								<option value="tolak" <?php if($type == 'tolak' ): echo "selected"; endif; ?>>Ditolak</option>
								<option value="blokir" <?php if($type == 'blokir' ): echo "selected"; endif; ?>>Diblokir</option>
							</select>
						</div>
						<div class="col-sm-3">
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="POST" id="submission" action="" enctype="multipart/form-data">

					<div class="row" >
						<div class="col-sm-5 submission" hidden="true">
							<input type="text" placeholder="Silahkan tulis Pesan" class="form-control float-right" name="message-personal">
						</div>
						<div class="col-sm-5 submission" hidden="true">
							<input type="file" class="custom-file-input" name="message-image" accept="image/png, image/jpeg" required>
							<label class="custom-file-label">Pilih file</label>
						</div>
						<div class="col-sm-2 submission" hidden="true">
							<a class="btn btn-success save-personal btn-block"><i class="fas fa-paper-plane"></i> Kirim Whatsapp</a>
						</div>
						&nbsp;
					</div>
					<input type="hidden" class="form-control note-followup" name="note_followup" value="" >
					<input type="hidden" class="form-control status-followup" name="status_followup" value="" >
					<input type="hidden" class="form-control message-personal" name="message" value="" >
					<input type="hidden" class="form-control site_id" name="site_id" value="1">
					<input type="hidden" class="form-control company_id" name="company_id" value="1">
					<input type="hidden" class="form-control position_id" name="position_id" value="1">
					<input type="hidden" class="form-control placement_date" name="placement_date" value="1">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="50px"><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th id='created_at'>Tanggal Daftar</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='id_card'>Nomor KTP </th>
									<th id='driver_license'>Nomor SIM</th>
									<th id='phone_number'>Nomor Whatsapp</th>
									<th id='address'>Alamat Domisili</th>
									<th id='city_name'>Kota Domisili</th>
									<th id='followup_status'>Status Followup</th>
									<th id='followup_note'>Catatan Followup</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.preview').on('change', function() {
			var url = '<?php echo base_url("candidate_temporary/courier/"); ?>'+$("#type").val()+'/'+$("#branch").val();
			window.location.href = url;
		});
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("candidate_temporary/courier_ajax/".$type."/".$branch);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'created_at' },
				{ data: 'full_name' },
				{ data: 'id_card' },
				{ data: 'driver_license' },
				{ data: 'phone_number' },
				{ data: 'address' },
				{ data: 'city_name' },
				{ data: 'followup_status', 'orderable':false, 'searchable':false },
				{ data: 'followup_note', 'orderable':false, 'searchable':false },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[1, 'desc']],
			"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				if (aData['followup_status'].startsWith("Lihat")) {
					$('td', nRow).css('background-color', '#80ff80');
				} else if (aData['followup_status'].startsWith("Hubungi")) {
					$('td', nRow).css('background-color', '#8080ff');
				}else if (aData['followup_status'].startsWith("Proses")) {
					$('td', nRow).css('background-color', '#ffcc80');
				}else if (aData['followup_status'].startsWith("Tolak")) {
					$('td', nRow).css('background-color', '#ff8080');
				}else if (aData['followup_status'].startsWith("Blokir")) {
					$('td', nRow).css('background-color', '#bfbfbf');
				}
			}
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<8){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
    		});
		});
		$("input:checkbox").change(function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
    		});
		});
		
		$(".btn.save-personal" ).click(function() {
			$('#submission').attr('action', "<?php echo base_url('candidate_temporary/send/courier/'.$type.'/'.$branch); ?>").submit();
		});
		$(".btn.followup" ).click(function() {
			var message = $('#note-followup').val();
			var status = $('#status-followup').val();
			if(message == ''){
				alert('Catatan Followup. ');
			}else{
				$(".note-followup").val(message);
				$(".status-followup").val(status);
				$('#submission').attr('action', "<?php echo base_url('candidate_temporary/followup/courier/'.$type.'/'.$branch); ?>").submit();
			}
		});
	});
</script>
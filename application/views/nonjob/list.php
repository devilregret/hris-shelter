<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('nonjob/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Karyawan Nonjob</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-3">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
						<div class="col-sm-2">
							<div class="submission" hidden="true">
							<select class="form-control select2 " id="company_id" required>
								<option value="">-- Unit Kontrak --</option>
								<?php foreach ($list_company as $item) :  ?>
									<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="submission" hidden="true">
							<select class="form-control select2 " id="site_id" required>
								<option value="">-- Site Bisnis --</option>
								<?php foreach ($list_site as $item) :  ?>
									<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="submission" hidden="true">
								<select class="form-control select2 " id="position_id" required>
										<option value="">-- Pilih Jabatan --</option>
									<?php foreach ($list_position as $item) :  ?>
										<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-2 submission" hidden="true">
							<a href="#" class="btn btn-info process"><i class="fas fa-check"></i> Proses</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="POST" id="process" action="<?php echo base_url('nonjob/process'); ?>">
					<input type="hidden" class="form-control company_id" name="company_id" required="">
					<input type="hidden" class="form-control site_id" name="site_id" required="">
					<input type="hidden" class="form-control position_id" name="position_id" required="">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th id='id_card'>Nomor KTP</th>
									<th id='employee_number'>ID Karyawan</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='company_name'>Unit Kontrak</th>
									<th id='site_name'>Site Bisnis</th>
									<th id='position'>Jabatan</th>
									<th id='contract_type'>Tipe Kontrak</th>
									<th id='contract_end'>Akhir Kontrak</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("nonjob/list_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'id_card'},
				{ data: 'employee_number' },
				{ data: 'full_name' },
				{ data: 'company_name' },
				{ data: 'site_name' },
				{ data: 'position' },
				{ data: 'contract_type' },
				{ data: 'contract_end' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[3, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<9){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$(".export" ).click(function() {
			var url = "<?php echo base_url('nonjob/export?'); ?>" + "employee_number="+$("#s_employee_number").val()+"&full_name="+$("#s_full_name").val()+"&company_name="+$("#s_company_name").val()+"&site_name="+$("#s_site_name").val()+"&position="+$("#s_position").val()+"&contract_type="+$("#s_contract_type").val()+"&contract_end="+$("#s_contract_end").val()+"&id_card="+$("#s_id_card").val();
			window.location = url;
		
		});
		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});
		$(document).on("change", ".check-id", function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});
		$("input:checkbox").change(function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});
		$( "#site_id" ).change(function() {
			$(".site_id").val($(this).val());
		});

		$(".process" ).click(function() {
			if($( "#site_id").val() == '' || $( "#position_id").val() == '' || $( "#company_id").val() == ''){
				alert('Silahkan pilih Unit Kontrak, Site Bisnis dan Jabatan!!!');
			}else{
				$( ".site_id").val($( "#site_id").val());
				$( ".position_id").val($( "#position_id").val());
				$( ".company_id").val($( "#company_id").val());
				$( "#process" ).submit();
			}
		});
	});
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('whatsapp/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-list"></i>&nbsp;Whatsapp</h3>
				</div>
				<div class="card-header">
					&nbsp;<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
				</div>
				<div class="card card-primary card-tabs">
					<div class="card-header p-0 pt-1">
						<ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
							<li class="nav-item">
								<a class="nav-link <?php if ($tab==="personal"): echo 'active'; endif; ?>" id="custom-tabs-one-personal-tab" data-toggle="pill" href="#custom-tabs-one-personal" role="tab" aria-controls="custom-tabs-one-personal" aria-selected="true">Personal</a>
							</li>
							<li class="nav-item">
								<a class="nav-link <?php if ($tab==="group"): echo 'active'; endif; ?>" id="custom-tabs-one-group-tab" data-toggle="pill" href="#custom-tabs-one-group" role="tab" aria-controls="custom-tabs-one-group" aria-selected="false">Group</a>
							</li>
						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content" id="custom-tabs-one-tabContent">
							<div class="tab-pane fade <?php if ($tab==="personal"): echo 'show active'; endif; ?>" id="custom-tabs-one-personal" role="tabpanel" aria-labelledby="custom-tabs-one-personal-tab">
								<div class="row" >
									<div class="col-sm-10 whatsapp" hidden="true">
										<input type="text" placeholder="Silahkan tulis Pesan" class="form-control float-right" id="message-personal">
									</div>
									<div class="col-sm-2 whatsapp" hidden="true">
										<a class="btn btn-success save-personal"><i class="fas fa-paper-plane"></i> Kirim</a>
									</div>
									&nbsp;
								</div>
								<p>&nbsp;</p>
								<form method="POST" id="personal" action="<?php echo base_url('whatsapp/personal'); ?>">
									<input type="text" name="message" class="message-personal" value="" hidden="">
									<div class="scroll-panel">
										<table id="data-table" class="table table-bordered table-hover">
											<thead>
												<tr>
													<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
													<th id='id_card'>Nomor KTP</th>
													<th id='full_name'>Nama Lengkap</th>
													<th id='phone_number'>No Telepon/Whatsapp</th>
													<th id='site_name'>Site Bisnis</th>
													<th id='company_name'>Unit Bisnis</th>
													<th id='position'>Jabatan</th>
													<th width="80px">#</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th></th>
													<th></th>
													<th></th>
													<th></th>
													<th></th>
													<th></th>
													<th></th>
													<th></th>
												</tr>
											</tfoot>
										</table>
									</div>
								</form>
							</div>
							<div class="tab-pane fade <?php if ($tab==="group"): echo 'show active'; endif; ?>" id="custom-tabs-one-group" role="tabpanel" aria-labelledby="custom-tabs-one-group-tab">
								<div class="row" >
									<div class="col-sm-10 whatsapp" hidden="true">
										<input type="text" placeholder="Silahkan tulis Pesan" class="form-control float-right" id="message-group">
									</div>
									<div class="col-sm-2 whatsapp" hidden="true">
										<a class="btn btn-success save-group"><i class="fas fa-paper-plane"></i> Kirim</a>
									</div>
									&nbsp;
								</div>
								<p>&nbsp;</p>
								<form method="POST" id="group" action="<?php echo base_url('whatsapp/group'); ?>">
									<input type="text" name="message" class="message-group" value="" hidden="">
									<table class="table table-bordered table-hover">
										<thead>
											<tr>
												<th>
													<!-- <input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"> -->
												</th>
												<th id='group'>Nama Grup</th>
												<th id='group_code'>Kode Grup</th>
												<th width="80px">#</th>
											</tr>
										</thead>
										<tbody>
												<td><input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="GzE71VByxyEFbPrEcwL7pX"></td>
												<td>Kord Int HRIS shelter</td>
												<td>GzE71VByxyEFbPrEcwL7pX</td>
												<td></td>
										</tbody>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.expired').on('change', function() {
			window.location.href = this.value;
		});

		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("whatsapp/list_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'id_card'},
				{ data: 'full_name' },
				{ data: 'phone_number' },
				{ data: 'site_name' },
				{ data: 'company_name' },
				{ data: 'position' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[2, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<7){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".whatsapp").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".whatsapp").prop('hidden', false);
			});
		});
		$("input:checkbox").change(function() {
			$(".whatsapp").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".whatsapp").prop('hidden', false);
			});
		});
		$( "#message-personal" ).change(function() {
			$(".message-personal").val($(this).val());
		});
		$( ".btn.save-personal" ).click(function() {
			if($(".message-personal").val() == ""){
				alert("Pesan Tidak boleh kosong.");
			}else{
				$( "#personal" ).submit();
			}
		});
		$( "#message-group" ).change(function() {
			$(".message-group").val($(this).val());
		});
		$( ".btn.save-group" ).click(function() {
			if($(".message-group").val() == ""){
				alert("Pesan Tidak boleh kosong.");
			}else{
				$( "#group" ).submit();
			}
		});
	});
</script>
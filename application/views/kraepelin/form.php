<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<style>
		.table-identitas tr td {
			padding:1px !important;
			font-size:12px;
			border: none !important;
		}
		.td-isi {
			vertical-align:top;
			padding-left:20px;
			padding-right:20px;
		}
		.td-lompat {
			vertical-align:top;
			padding-left:10px;
			padding-right:10px;
			margin-top:30px;
			border: 1px solid black;
		}
		.td-jawaban {
			padding-left:10px;
			padding-right:10px;
		}
		.td-benar {
			border: 1px solid black;
			border-radius:30px;
		}

		/* style soal */
	.questionsBox {
		display: block;
		border: solid 1px #e3e3e3;
		padding: 10px 20px 0px;
		box-shadow: inset 0 0 30px rgba(000,000,000,0.1), inset 0 0 4px rgba(255,255,255,1);
		border-radius: 3px;
		margin: 0 10px;
	}.questions {
		background: #007fbe;
		color: #FFF;
		font-size: 22px;
		padding: 8px 30px;
		font-weight: 300;
		margin: 0 -30px 10px;
		position: relative;
	}
	.questions:after {
		background: url(../img/icon.png) no-repeat left 0;
		display: block;
		position: absolute;
		top: 100%;
		width: 9px;
		height: 7px;
		content: '.';
		left: 0;
		text-align: left;
		font-size: 0;
	}
	.questions:after {
		left: auto;
		right: 0;
		background-position: -10px 0;
	}
	.questions:before, .questions:after {
		background: black;
		display: block;
		position: absolute;
		top: 100%;
		width: 9px;
		height: 7px;
		content: '.';
		left: 0;
		text-align: left;
		font-size: 0;
	}
	.answerList {
		margin-bottom: 15px;
	}


	ol, ul {
		list-style: none;
	}
	.answerList li:first-child {
		border-top-width: 0;
	}

	.answerList li {
		padding: 3px 0;
	}
	.answerList label {
		display: block;
		padding: 6px;
		border-radius: 6px;
		border: solid 1px #dde7e8;
		font-weight: 400;
		font-size: 13px;
		cursor: pointer;
		font-family: Arial, sans-serif;
	}
	input[type=checkbox], input[type=radio] {
		margin: 4px 0 0;
		margin-top: 1px;
		line-height: normal;
	}
	.questionsRow {
		background: #dee3e6;
		margin: 0 -20px;
		padding: 10px 20px;
		border-radius: 0 0 3px 3px;
	}
	.button, .greyButton {
		background-color: #f2f2f2;
		color: #888888;
		display: inline-block;
		border: solid 3px #cccccc;
		vertical-align: middle;
		text-shadow: 0 1px 0 #ffffff;
		line-height: 27px;
		min-width: 160px;
		text-align: center;
		padding: 5px 20px;
		text-decoration: none;
		border-radius: 0px;
		text-transform: capitalize;
	}
	.questionsRow span {
		float: right;
		display: inline-block;
		line-height: 30px;
		border: solid 1px #aeb9c0;
		padding: 0 10px;
		background: #FFF;
		color: #007fbe;
	}
	.border {
		border:1px solid black;
	}

	.jawaban-terpilih {
		border:2px solid black;
		padding:5px;
		border-radius:30px
	}

	#countdown2 {
		text-align: center;
		font-size: 50px;
		font-weight:bold;
	}

	input[readonly] {
		background-color: #f6e58d;
	}
	.box-soal {
	}
	.tabel-soal {
		width:50px;
		margin-right:30px
	}
	.box-angka {
		width:50px;
		font-weight:bold;
		font-size:24px;
		padding-top:10px
	}
	.line-4 hr {
		height: 5px;border: 0;background: black;
	}

	</style>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<div class="row" style="justify-content:space-between">
						<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Tes Kraepelin</h3>
						<p id="countdown2"></p>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-2" method="post" action="<?php echo base_url("kraepelin/saveform"); ?>" enctype="multipart/form-data" onsubmit="return validateForm()">
							<input type="hidden" name="full_name" value="<?php echo $data_employee[0]->full_name ?>">
							<input type="hidden" name="umur" value="<?php echo $data_employee[0]->umur ?>">
							<input type="hidden" name="jk" value ="<?php echo $data_employee[0]->jk ?>">
							<input type="hidden" name="vacancy_id" value ="<?php echo $vacancy_id ?>">
							<input type="hidden" name="tgl_tes" value="<?php echo $data_employee[0]->tgl_tes ?>">
							<input type="hidden" name="tes_mulai" value="<?php echo date('Y-m-d H:i:s'); ?>">
							<input type="hidden" name="jumlah_kolom_kraepelin" id="jumlah_kolom_kraepelin" value="<?php echo $data_vacancy[0]->jumlah_kolom_kraepelin ?>">
							<input type="hidden" name="jumlah_baris_kraepelin" id="jumlah_baris_kraepelin" value="<?php echo $data_vacancy[0]->jumlah_baris_kraepelin ?>">
							<input type="hidden" id="kolom_saat_ini" value="0">
							<input type="hidden" id="baris_saat_ini" value="0">
							<input type="hidden" id="is_validate" value="1">
							<input type="hidden" id="id_interval" value="">

							<!-- <div class="form-group row">
								<div class="col-sm-4" style="background-color:#74b9ff;font-size:12px;text-align:justify;padding-top:10px">
									<table class="table table-identitas">
										<tbody>
											<tr>
												<td style="width:25%"><b>Nama</b></td>
												<td><b>&nbsp;:&nbsp;</b></td>
												<td><?php echo $data_employee[0]->full_name ?></td>
											</tr>
											<tr>
												<td style="width:25%"><b>Usia</b></td>
												<td><b>&nbsp;:&nbsp;</b></td>
												<td>
													<?php 
														if ($data_employee[0]->umur==null) {
															echo '-';
														} else {
															echo $data_employee[0]->umur;
														} ?> Tahun
												</td>
											</tr>
											<tr>
												<td style="width:25%"><b>Jenis Kelamin</b></td>
												<td><b>&nbsp;:&nbsp;</b></td>
												<td><?php echo $data_employee[0]->jk ?></td>
											</tr>
											<tr>
												<td style="width:25%"><b>Tanggal Tes</b></td>
												<td><b>&nbsp;:&nbsp;</b></td>
												<td><?php echo $data_employee[0]->stgl_tes ?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-sm-8" style="background-color:#74b9ff;font-size:12px;text-align:justify;padding-top:10px">
									<span>
									<b>INSTRUKSI :</b>
									</span><br>
									<span>
									
									</span>
									<br>
								</div>
							</div> -->
							<div class="row">
								<span>
									<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;SOAL</h3>
								</span>
							</div>
							<div class="row">
								<?php for ($k=0; $k < $data_vacancy[0]->jumlah_kolom_kraepelin; $k++) { ?>
									<div class="box-soal">
										<table class="tabel-soal">
										<?php 
										$rand_num = rand(0,9);
										$next_rand = rand(0,9);
										for ($b=0; $b < ($data_vacancy[0]->jumlah_baris_kraepelin+1); $b++) { 
										?>
											<tr>
												<td class="box-angka">
													<?php echo $rand_num ?>
													<input type="hidden" name="<?php echo 'soal_'.$k ?>[]" value="<?php echo $rand_num ?>">
													<input type="hidden" name="<?php echo 'next_soal_'.$k ?>[]" value="<?php echo $next_rand ?>">
												</td>
											</tr>
											<tr>
												<td></td>
												<td>
													<?php
													if($b<$data_vacancy[0]->jumlah_baris_kraepelin){
													?>
														<input type="number" class="isian <?php if(!($k==0&&$b==$data_vacancy[0]->jumlah_baris_kraepelin-1)){ echo "d-none"; }else{ echo "";};  ?>" <?php if(!($k==0&&$b==$data_vacancy[0]->jumlah_baris_kraepelin-1)){ echo "readonly"; }else{ echo "autofocus";};  ?> onkeyup="pindah_soal(<?php echo $k.','.$b ?>)" name="<?php echo 'kolom_'.$k ?>[]" id="<?php echo 'kolom_'.$k.'_baris_'.$b ?>" style="width:50px;text-align:center" >
													<?php
													}
													?>
												</td>
											</tr>
											<?php
												$rand_num = $next_rand;
												$next_rand = rand(0,9);
												}
											?>
										</table>
										<div class="line-4">
											<hr />
										</div>
									</div>
								<?php } ?>
							</div>
							<br>
							<br>
							<div class="form-group row  float-right">
								<button type="button" onclick="window.history.back();" class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>	
		</div>
	</section>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
	function pindah_soal(kolom,baris){
		const maxKolom = $('#jumlah_kolom_kraepelin').val();
		const maxBaris = $('#jumlah_baris_kraepelin').val();

		const idSoal = '#kolom_'+kolom+'_baris_'+baris;

		let kolomB = kolom;
		let BarisB = baris-1;

		if(BarisB < 0) {
			kolomB+=1;
			BarisB =maxBaris-1;
			clearInterval($('#id_interval').val());
			countdownPerSoal();
		}
		const idSoalB = '#kolom_'+kolomB+'_baris_'+BarisB;

		if($(idSoal).val()!=""){
			// $(idSoal).prop('readonly', true);
			$(idSoalB).prop('readonly', false);
			$(idSoalB).removeClass('d-none');
			$('#kolom_saat_ini').val(kolomB);
			$('#baris_saat_ini').val(BarisB);
			$(idSoalB).focus();
		}
	}

	function validateForm() {
		let isValidate = $("#is_validate").val();
		if(isValidate =="0"){
			return true;
		}

		let msg ="";
		let noBelumIsi = "";
		let jumlahSoal = $('#jumlah_soal').val();
		for (let i = 1; i <= jumlahSoal; i++) {
			let radioName = 'soal_'+i;
			let isCek = false;
			if($('input[name='+radioName+']').is(':checked')) {
				isCek = true;
			}

			if (!isCek) {
				noBelumIsi += i+",";
			}
		}

		noBelumIsi = noBelumIsi.slice(0, -1);
		if (noBelumIsi != "") {
			msg ="Terdapat nomor yang belum anda isi yaitu : nomor "+noBelumIsi+". Apakah anda ingin tetap menyimpan ?";
		}else{
			msg = "Apakah anda ingin menyimpan tes ini ?";
		}

		if (confirm(msg)) {
			return true;	
		}else{
			return false;
		}
		return true;
	}

	function pindahKolom() {
		$('.isian').prop('readonly', true);
		const kolomSaatIni = parseInt($('#kolom_saat_ini').val());
		const barisSaatIni = parseInt($('#baris_saat_ini').val());
		const maxKolom = $('#jumlah_kolom_kraepelin').val();
		const maxBaris = $('#jumlah_baris_kraepelin').val();

		const focusSaatIni = '#kolom_'+kolomSaatIni+'_baris_'+barisSaatIni;
		const idSoal = '#kolom_'+(kolomSaatIni+1)+'_baris_'+(maxBaris-1);
		$(idSoal).prop('readonly', false);
		$(idSoal).removeClass('d-none');
		$(focusSaatIni).prop('readonly', true);
		$(idSoal).focus();
		countdownPerSoal();

	}
	function countdownPerSoal (){
		var nowD = new Date();
		nowD.setSeconds(nowD.getSeconds() + <?php echo $data_vacancy[0]->detik_pindah_soal_kraepelin ?>);
		let durasi = nowD.getTime();
		var x = setInterval(function() {
			var now = new Date().getTime();
			var distance = durasi - now;
			var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);
			document.getElementById("countdown2").innerHTML =  seconds;
			if (distance < 0) {
				clearInterval(x);
				pindahKolom();
			}
			$('#id_interval').val(x);
		}, 1000);
	}

	$(document).ready(function () {
		countdownPerSoal();
		const maxBaris = $('#jumlah_baris_kraepelin').val();

		const nBaris = "kolom_0_baris_"+(maxBaris-1);
		var target = document.getElementById(nBaris);
		target.focus();
    	target.click();
	})

</script>
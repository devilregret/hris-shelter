<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/css/adminlte.min.css"); ?>
		<?php echo asset_js("plugins/jquery/jquery.min.js"); ?>	
		<style>
				.maintable { 
					border-spacing: 10px;
					border-collapse: separate;
				}
				.tabel-soal {
					border-spacing: 0px;
					border-collapse: separate;
				}
				.color-title {
					background-color:#341f97;
					-webkit-print-color-adjust:exact;
				}
				.color-content {
					background-color:#48dbfb;
					-webkit-print-color-adjust:exact;
				}
				.color-sub-title {
					background-color:#2e86de;
					-webkit-print-color-adjust:exact;
				}
				.jawaban-terpilih {
					border:2px solid black;
					padding:5px;
					border-radius:30px
				}
				.color-red {
					background-color:red;
					-webkit-print-color-adjust:exact;
				}
				.tabel-simpulan {
					border-spacing: 30px;
				}
				.tabel-simpulan tr td {
					border:1px solid black;
					padding-top:10px;
					padding-bottom:10px;
				}
				.jawaban-tidak-diisi {
					background-color:#ff6b6b;
					-webkit-print-color-adjust:exact;
				}
				.jawaban-salah {
					background-color:#feca57;
					-webkit-print-color-adjust:exact;
				}
				.jawaban-benar {
					background-color:#1dd1a1;
					-webkit-print-color-adjust:exact;
				}

			</style>
	</head>
	<body>
		<div class="wrapper">
			<section class="deret-angka-print">
			<?php echo asset_js("plugins/chart.js/Chart.min.js"); ?>
			<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
			<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

			<style>
				
			</style>
				<br>
				<div class="row">
					<table style="width:100%" class="maintable">
						<tr class="color-title">
							<td colspan="3" style="padding:10px">
								<h2 style="color:white;text-align:center">TES KRAEPELIN</h2>
							</td>
						</tr>
						<tr>
							<td colspan="3" class="color-content">
								<table style="width:100%">
									<tr>
										<td style="width:10%">Name</td>
										<td style="width:3%">:</td>
										<td style="width:87%"><?php echo $data_kraepelin[0]->nama ?></td>
									</tr>
									<tr>
										<td>Age</td>
										<td>:</td>
										<td><?php echo $data_kraepelin[0]->usia ?> Tahun</td>
									</tr>
									<tr>
										<td>Gender</td>
										<td>:</td>
										<td><?php echo $data_kraepelin[0]->jk ?></td>
									</tr>
									<tr>
										<td>Date</td>
										<td>:</td>
										<td><?php echo $data_kraepelin[0]->tgl_tes ?></td>
									</tr>
								</table>
							</td>
						</tr>
						<?php if($is_hanya_hasil==0) { ?>
						<tr class="color-sub-title">
							<td colspan="3" style="padding:5px">
								<h2 style="color:white;text-align:center">Soal</h2>
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<table class="tabel-soal" style="width:100%">
								<?php for ($b=0; $b < $data_kraepelin[0]->jml_baris; $b++) { ?>
									<tr>
										<?php for ($k=0; $k < $data_kraepelin[0]->jml_kolom; $k++) { 
											foreach ($data_kraepelin_d as $key => $value) {
												if($value->baris==$b&&$value->kolom==$k){
										?>
										<td style="width:2%" class="" colspan="2"><?php echo $value->angka_1 ?></td>
										<?php } } } ?>
										
									</tr>
									<tr>
										<?php for ($k=0; $k < $data_kraepelin[0]->jml_kolom; $k++) { 
											foreach ($data_kraepelin_d as $key => $value) {
												if($value->baris==$b&&$value->kolom==$k){
										?>
										<td></td>
										<td style="width:2%;font-weight:bold;padding-left:5px;padding-right:15px;color:<?php if($value->is_benar==1) {echo "green";} else{echo "red";} ?>"><?php echo $value->jawaban ?></td>
										<?php } } } ?>
									</tr>
								<?php } ?>
								</table>
							</td>
						</tr>
						<?php } ?>
						<tr class="color-sub-title">
							<td colspan="3" style="padding:5px">
								<h2 style="color:white;text-align:center">Graph</h2>
							</td>
						</tr>
						<tr>
							<td colspan="3" style="padding:5px">
								<canvas id="chart" height="70px" class="color-canvas"></canvas>

								<script>
									var ctx = document.getElementById("chart");
									var min = 0;
									var max = <?php echo $data_kraepelin[0]->jml_baris ?>;
									var kolom = <?php echo $data_kraepelin[0]->jml_kolom ?>;
									var label = "Grafik Kraepelin";
									var arrl = [];
									var arrd = [];
									
									<?php
									for ($k=0; $k < $data_kraepelin[0]->jml_kolom; $k++) {
										$jbenar = 0;
										foreach ($data_kraepelin_d as $key => $value) {
											if($value->kolom==$k){
												if($value->jawaban!=''){
													$jbenar++;
												}
											}
										}
									?>
									arrd.push(<?php echo $jbenar ?>);
									arrl.push(<?php echo $k+1 ?>);
									<?php } ?>
									
									var myChart = new Chart(ctx, {
										type: 'line',
										lineTension:0, //straight lines
										data: {
											labels: arrl,
											datasets: [{
												label: label,
												lineTension:0, //straight lines
												fill: false,
												borderColor: ['#2e86de'],
												data: arrd,
											}]
										},
										options: {
											plugins: {
												// Change options for ALL labels of THIS CHART
												datalabels: {
													color: '#000000',
												}
											},
											scales: {
												yAxes: [{
													display: true,
													ticks: {
														stepSize: 1,
														max: max,
														min:min
													},
													scaleLabel: {
														display:true
													}
												}]
											},
										}
									});
								</script>

							</td>
						</tr>
						<tr>
							<td>
								<table class="tabel-simpulan" style="width:100%;text-align:center">
									<tr class="color-sub-title" style="color:white;font-size:16px;font-weight:bold">
										<td>Kolom Tertinggi</td>
										<td>Kolom Terendah</td>
										<td>Penjumlahan Salah</td>
										<td>Tidak Dikerjakan</td>
										<td>Selisih Tertinggi & Terendah</td>
										<td>Petugas</td>
									</tr>
									<tr>
										<td><?php echo $data_kraepelin[0]->titik_tertinggi ?></td>
										<td><?php echo $data_kraepelin[0]->titik_terendah ?></td>
										<td><?php echo $data_kraepelin[0]->jawaban_salah ?></td>
										<td><?php echo $data_kraepelin[0]->tidak_diisi ?></td>
										<td><?php echo $data_kraepelin[0]->selisih_titik ?></td>
										<td><?php echo $data_kraepelin[0]->petugas ?></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table class="tabel-simpulan" style="width:100%;text-align:center">
									<tr class="color-sub-title" style="color:white;font-size:16px;font-weight:bold">
										<td>Kecepatan Kerja</td>
										<td>Ketelitian Kerja</td>
										<td>Keajegan Kerja ( Range )</td>
										<td>Keajegan Kerja ( Average D )</td>
										<td>Ketahanan Kerja</td>
										<td>Kategori</td>
									</tr>
									<tr>
										<td><?php echo $data_kraepelin[0]->kecepatan_kerja ?></td>
										<td><?php echo $data_kraepelin[0]->ketelitian_kerja ?></td>
										<td><?php echo $data_kraepelin[0]->keajegan_kerja_range ?></td>
										<td><?php echo $data_kraepelin[0]->keajegan_kerja_average_d ?></td>
										<td><?php echo $data_kraepelin[0]->ketahanan_kerja ?></td>
										<td><?php echo $data_kraepelin[0]->kategori ?></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<br>
				</div>
			</section>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="lolostidak()">Lolos / Tidak</button>
			<button type="button" class="btn btn-default" onclick="printDeretAngka();">Print</button>
			<button type="button" class="btn btn-default" onclick="window.close();">Tutup</button>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.main-sidebar').remove();
				$('.main-header').remove();
			})
			function lolostidak() {
				Swal.fire({
					title: 'Apakah Peserta ini Lolos / Tidak Lolos tes Kraepelin ?',
					showDenyButton: true,
					showCancelButton: true,
					confirmButtonText: 'Lolos',
					denyButtonText: `Tidak Lolos`,
					cancelButtonText:'Batal'
					}).then((result) => {
						if (result.isDenied||result.isConfirmed) {
							let url = "<?php echo base_url('kraepelin/updatelolos').'/'.$data_kraepelin[0]->id ?>";
							let kategori = "LOLOS";
							console.log(url)
							if(result.isDenied){
								kategori="TIDAK LOLOS"
							}
							$.ajax({
								url: url,
								type: "POST",
								cache: false,
								data:{
									kategori: kategori
								},
								success: function(response){
									var result = JSON.parse(response);
									if(result.code==200){
										window.location.reload();
									}
								}
							});

						}
					})
			}
			function printDeretAngka() {
				$('.modal-footer').remove();
				window.print();
				window.close();
			}
		</script>
	</body>
</html>

<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('mou/approval'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Penawaran</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row ">
						<div class="col-sm-9">
							&nbsp;
						</div>
						<div class="col-sm-3 process float-right" hidden="true">
							<a class="btn btn-danger reject" style="width: 200px;color: white;"><i class="fas fa-times-circle"></i> TOLAK</a>
							<a class="btn btn-success approve" style="width: 200px;color: white;"><i class="fas fa-times-circle"></i> SETUJU</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="POST" id="contract" action="<?php echo base_url('mou/process/cancel'); ?>">
					<table id="data-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th width="50px"></th>
								<th id='company_name'>Bisnis Unit </th>
								<th id='branch_name'>Branch</th>
								<th id='client_name'>Site Bisnis</th>
								<th id='sales_name'>Pembuat Penawaran</th>
								<th id='client_address'>Pekerjaan</th>
								<th id='status'>Status</th>
								<th width="105px">#</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
					</table>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("cooperation/approval_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'company_name' },
				{ data: 'branch_name' },
				{ data: 'site_name' },
				{ data: 'sales_name' },
				{ data: 'position' },
				{ data: 'status' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[1, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<5){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$(document).on("change", ".check-id", function() {
			$(".process").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".process").prop('hidden', false);
			});
		});

		$(".reject" ).click(function() {
			$('#contract').attr('action', "<?php echo base_url('mou/process/reject'); ?>").submit();
		});

		$(".approve" ).click(function() {
			$('#contract').attr('action', "<?php echo base_url('mou/process/approve'); ?>").submit();
		});
	});
</script>
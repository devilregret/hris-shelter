<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('mou/contract/'.$type); ?>">Daftar Kerjasama</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Kerjasama</h3>
				</div>
				<div class="card-body">
					<?php if(isset($message)): echo $message; endif; ?>
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4" method="post" action="<?php echo base_url("mou/contract_form/".$type."/".$id); ?>" enctype="multipart/form-data">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nama Perusahaan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="hidden" class="form-control" name="status_mou" value="<?php echo $status_mou; ?>">
									<input type="text" class="form-control" placeholder="Nama Perudahaan" name="company_name" value="<?php echo $company_name; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Unit Bisnis Awal <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="begining_company_id" required="">
										<option value="">-- PILIH UNIT BISNIS AWAL --</option>
										<?php foreach ($list_company as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $begining_company_id): echo "selected"; endif;?>><?php echo $item->code; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Unit Bisnis Saat Ini <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="now_company_id" required="">
										<option value="">-- PILIH UNIT BISNIS SAAT INI --</option>
										<?php foreach ($list_company as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $now_company_id): echo "selected"; endif;?>><?php echo $item->code; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Wilayah <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="province_id" required>
										<option value="">-- PILIH WILAYAH --</option>
										<?php foreach ($list_province as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $province_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Lokasi Site <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="city_id" required>
										<option value="">-- PILIH LOKASI SITE --</option>
										<?php foreach ($list_city as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $city_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Kontrak Mulai <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="date" name="contract_start" class="form-control datepicker" value="<?php echo $contract_start; ?>" required>
								</div>
								<div class="col-sm-7"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Kontrak Selesai <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="date" name="contract_end" class="form-control datepicker" value="<?php echo $contract_end; ?>" required>
								</div>
								<div class="col-sm-7"></div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Headcount <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="number" class="form-control" placeholder="Headcount" name="headcount" value="<?php echo $headcount; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">GP 2019 <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Gaji Pokok 2019" name="basic_salary" value="<?php echo $basic_salary; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">GP 2020 <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Gaji Pokok 2020" name="basic_salary2" value="<?php echo $basic_salary2; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">BPJS Ketenagakerjaan <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="BPJS Ketenagakerjaan" name="bpjs_tk" value="<?php echo $bpjs_tk; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">BPJS Kesehatan <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="BPJS Kesehatan" name="bpjs_ks" value="<?php echo $bpjs_ks; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Asuransi Ketenagakerjaan <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Asuransi Ketenagakerjaan" name="labor_insurance" value="<?php echo $labor_insurance; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Asuransi Kesehatan <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Asuransi Kesehatan" name="health_insurance" value="<?php echo $health_insurance; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">OHC <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="OHC" name="ohc" value="<?php echo $ohc; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">KAPORLAP <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="KAPORLAP" name="kaporlap" value="<?php echo $kaporlap; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Chemicals <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Chemicals" name="chemicals" value="<?php echo $chemicals; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Training <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="training">
										<option value="Tidak" <?php if($training=='Tidak'): echo 'selected'; endif;?>>Tidak</option>
										<option value="Ya" <?php if($training=='Ya'): echo 'selected'; endif;?>>Ya</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Kirim Invoice <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Kirim Invoice" name="date_invoice" value="<?php echo $date_invoice; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Time Of Payment <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Time Of Payment" name="date_payment" value="<?php echo $date_payment; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">CRM <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="CRM" name="crm" value="<?php echo $crm; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Status Pendaftaran<span class="text-danger"></span></label>
								<div class="col-sm-8">
									<textarea class="form-control" name="status_registration" rows="3" placeholder="Status Pendaftaran"><?php echo $status_registration; ?></textarea>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Dokumen <span class="text-danger"> <i class="text-xs "></i></span></label>
								<div class="custom-file col-sm-7 file-upload">
									<input type="file" class="custom-file-input" name="document" accept="image/png, image/jpeg, image/jpg, .pdf, .doc, .docx">
									<label class="custom-file-label">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($files != ''): ?>
										<a href="<?php echo $files; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" name="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		$('.upload-document').imageUploadResizer({
			max_width: 1200, // Defaults 1000
			max_height: 800, // Defaults 1000
			quality: 1, // Defaults 1
			do_not_resize: ['gif', 'svg'], // Defaults []
		});
		$('#reservationtime').on( 'keyup change', function () {
			var range = $(this).val();

			var date1 = new Date(range.substring(3, 5)+"/"+range.substring(0, 2)+"/"+range.substring(6, 10)); 
			var date2 = new Date(range.substring(16, 18)+"/"+range.substring(13, 15)+"/"+range.substring(19, 23)); 

			var Difference_In_Time = date2.getTime() - date1.getTime(); 
			var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
			$('#longterm').val(Difference_In_Days+" Hari");
		});
		$(".check-card-id").click(function() {
			var id_card = $('.id_card').val();
			$.ajax({
				method:"POST",
				dataType: "json",
				url:"<?php echo base_url('employee_contract/card'); ?>",
				data: {id_card:id_card},
				success:function(result)
				{
					alert(result.description);
					if(result.id != '0'){
						var url = "<?php echo base_url("employee_contract/form/".$type."/"); ?>"+result.id;
						console.log(url);
						window.location.replace(url);
					}
				}
			});
		});
		$(".add-skill").click(function() {
			var content = 
			'<div class="form-group row"><div class="col-sm-1"><a class="btn btn-danger btn-block" onclick="remove(this);"><i class="fas fa-minus"></i></a><input type="hidden" class="form-control" name="skill_id[]"></div>' +
			'<div class="col-sm-6"><input type="text" class="form-control" placeholder="Nama Pelatihan" name="skill[]" value=""></div>' +
			'<div class="col-sm-5"><input type="text" class="form-control" placeholder="Nomor Sertifikat" name="certificate_number[]" value=""></div>' +
			'<div class="form-group col-sm-12 row mt-3"><div class="col-sm-1"></div>' +
			'<div class="custom-file col-sm-11"><input type="file" class="custom-file-input upload-document" name="skill_certificate[]" accept="image/png, image/jpeg, image/jpg, .pdf"><label class="custom-file-label" for="customFile">Choose file</label></div>' +
			'</div></div>';

			$("#skill").append(content);
		});
	});

	function remove(el){
		$(el).parent().parent().remove();
	}

	function remove_ajax(el, id, group){
		if (confirm('Anda yakin untuk menghapus data  ini?')) 
		{
			if($('.'+group).length <2){
				alert('Maaf Minimal harus ada 1 data');
			}else{
				$.ajax({
					url: "<?php echo base_url('account/'); ?>"+group,
					type: "POST",
					cache: false,
					data:{
						id: id
					},
					success: function(response){
						var result = JSON.parse(response);
						if(result.code==200){
							$(el).parent().parent().remove();
						}
					}
				});
			}
		}
		return false;
	}
</script>
<style type="text/css">
	input[type="text" i], input[type="email" i], input[type="select" i], input[type="date" i], .select2-container, textarea {
    	margin-left: -7px;
	}
</style>
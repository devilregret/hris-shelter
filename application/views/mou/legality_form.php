<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('mou/legality'); ?>">Daftar Perizinan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Perizinan</h3>
				</div>
				<div class="card-body">
					<?php if(isset($message)): echo $message; endif; ?>
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4" method="post" action="<?php echo base_url("mou/legality_form/".$id); ?>" enctype="multipart/form-data">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nama Perizinan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="text" class="form-control" placeholder="Nama Perizinan" name="name" value="<?php echo $name; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Unit Bisnis <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="regulation" required="">
										<option value="">-- PILIH REGULASI--</option>
										<option value="SK Direksi" <?php if("SK Direksi" == $regulation): echo "selected"; endif;?>>SK Direksi</option>
										<option value="Internal Memo" <?php if("Internal Memo" == $regulation): echo "selected"; endif;?>>Internal Memo</option>
										<option value="Policy" <?php if("Policy" == $regulation): echo "selected"; endif;?>>Policy</option>
										<option value="Job Des" <?php if("Job Des" == $regulation): echo "selected"; endif;?>>Job Des</option>
										<option value="Prosedur (ISO)" <?php if("Prosedur (ISO)" == $regulation): echo "selected"; endif;?>>Prosedur (ISO)</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Unit Bisnis <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="company_id" required="">
										<option value="">-- PILIH UNIT BISNIS--</option>
										<?php foreach ($list_company as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $company_id): echo "selected"; endif;?>><?php echo $item->code; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Branch <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="branch_id">
										<option value="">-- PILIH BRANCH--</option>
										<?php foreach ($list_branch as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $branch_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Berlaku <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="date" name="date_start" class="form-control datepicker" value="<?php echo $date_start; ?>" required>
								</div>
								<div class="col-sm-7"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Berakhir <span class="text-danger"></span></label>
								<div class="col-sm-2">
									<input type="date" name="date_end" class="form-control datepicker" value="<?php echo $date_end; ?>">
								</div>
								<div class="col-sm-7"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Catatan<span class="text-danger"></span></label>
								<div class="col-sm-8">
									<textarea class="form-control" name="note" rows="3" placeholder="Catatan"><?php echo $note; ?></textarea>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Dokumen <span class="text-danger"> <i class="text-xs "></i></span></label>
								<div class="custom-file col-sm-7 file-upload">
									<input type="file" class="custom-file-input" name="document" accept="image/png, image/jpeg, image/jpg, .pdf, .doc, .docx">
									<label class="custom-file-label">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($files != ''): ?>
										<a href="<?php echo $files; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" name="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		$('.upload-document').imageUploadResizer({
			max_width: 1200, // Defaults 1000
			max_height: 800, // Defaults 1000
			quality: 1, // Defaults 1
			do_not_resize: ['gif', 'svg'], // Defaults []
		});
		$('#reservationtime').on( 'keyup change', function () {
			var range = $(this).val();

			var date1 = new Date(range.substring(3, 5)+"/"+range.substring(0, 2)+"/"+range.substring(6, 10)); 
			var date2 = new Date(range.substring(16, 18)+"/"+range.substring(13, 15)+"/"+range.substring(19, 23)); 

			var Difference_In_Time = date2.getTime() - date1.getTime(); 
			var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
			$('#longterm').val(Difference_In_Days+" Hari");
		});
		$(".check-card-id").click(function() {
			var id_card = $('.id_card').val();
			$.ajax({
				method:"POST",
				dataType: "json",
				url:"<?php echo base_url('employee_contract/card'); ?>",
				data: {id_card:id_card},
				success:function(result)
				{
					alert(result.description);
					if(result.id != '0'){
						var url = "<?php echo base_url("employee_contract/form/".$type."/"); ?>"+result.id;
						console.log(url);
						window.location.replace(url);
					}
				}
			});
		});
		$(".add-skill").click(function() {
			var content = 
			'<div class="form-group row"><div class="col-sm-1"><a class="btn btn-danger btn-block" onclick="remove(this);"><i class="fas fa-minus"></i></a><input type="hidden" class="form-control" name="skill_id[]"></div>' +
			'<div class="col-sm-6"><input type="text" class="form-control" placeholder="Nama Pelatihan" name="skill[]" value=""></div>' +
			'<div class="col-sm-5"><input type="text" class="form-control" placeholder="Nomor Sertifikat" name="certificate_number[]" value=""></div>' +
			'<div class="form-group col-sm-12 row mt-3"><div class="col-sm-1"></div>' +
			'<div class="custom-file col-sm-11"><input type="file" class="custom-file-input upload-document" name="skill_certificate[]" accept="image/png, image/jpeg, image/jpg, .pdf"><label class="custom-file-label" for="customFile">Choose file</label></div>' +
			'</div></div>';

			$("#skill").append(content);
		});
	});

	function remove(el){
		$(el).parent().parent().remove();
	}

	function remove_ajax(el, id, group){
		if (confirm('Anda yakin untuk menghapus data  ini?')) 
		{
			if($('.'+group).length <2){
				alert('Maaf Minimal harus ada 1 data');
			}else{
				$.ajax({
					url: "<?php echo base_url('account/'); ?>"+group,
					type: "POST",
					cache: false,
					data:{
						id: id
					},
					success: function(response){
						var result = JSON.parse(response);
						if(result.code==200){
							$(el).parent().parent().remove();
						}
					}
				});
			}
		}
		return false;
	}
</script>
<style type="text/css">
	input[type="text" i], input[type="email" i], input[type="select" i], input[type="date" i], .select2-container, textarea {
    	margin-left: -7px;
	}
</style>
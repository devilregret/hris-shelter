<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>

	</head>
	<body>
		<section class="content">
			<div class="container-fluid">
				<div class="card card-primary card-outline col-12 mt-2">
					<div class="card-header">
						<h3 class="card-title"><i class="fas fa-eye"></i>&nbsp;Preview Kerjasama</h3>
					</div>
					<div class="row">
						<div class="col-12 table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<td>&nbsp</td>
										<td>Nama Perusahaan</td>
										<td>: <strong><?php echo $preview->company_name; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Bendera Awal</td>
										<td>: <strong><?php echo $preview->old_company_code; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Bendera Saat Ini</td>
										<td>: <strong><?php echo $preview->company_code; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Wilayah</td>
										<td>: <strong><?php echo $preview->province_name; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Lokasi Site</td>
										<td>: <strong><?php echo $preview->city_name; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Awal Kontrak</td>
										<td>: <strong><?php echo $preview->contract_start; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Akhir Kontrak</td>
										<td>: <strong><?php echo $preview->contract_end; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Headcount</td>
										<td>: <strong><?php echo $preview->headcount; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>GP 2019</td>
										<td>: <strong><?php echo $preview->basic_salary; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>GP 2020</td>
										<td>: <strong><?php $preview->basic_salary2; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>BPJS Ketenagakerjaan</td>
										<td>: <strong><?php echo $preview->bpjs_tk; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>BPJS Kesehatan</td>
										<td>: <strong><?php echo $preview->bpjs_ks; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Asuransi Ketenagakerjaan</td>
										<td>: <strong><?php echo $preview->labor_insurance; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Asuransi Kesehatan</td>
										<td>: <strong><?php if($preview->health_insurance != ''): echo nominal_rupiah($preview->health_insurance); endif; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>OHC</td>
										<td>: <strong><?php if($preview->ohc != ''): echo nominal_rupiah($preview->ohc); endif; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>KAPORLAP</td>
										<td>: <strong><?php if($preview->kaporlap != ''): echo nominal_rupiah($preview->kaporlap); endif; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Chemicals</td>
										<td>: <strong><?php if($preview->chemicals != ''): echo nominal_rupiah($preview->chemicals); endif; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Training</td>
										<td>: <strong><?php echo $preview->training; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Kirim Invoice</td>
										<td>: <strong><?php echo $preview->date_invoice; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Time Of Payment</td>
										<td>: <strong><?php echo $preview->date_payment; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>CRM</td>
										<td>: <strong><?php echo $preview->crm; ?></strong></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Status Pendaftaran</td>
										<td>: <strong><?php echo $preview->status_registration; ?></strong></td>
									</tr>
									<?php
									$files = '';
									if($preview->files != ''):
										$files			= '<a href="'.$preview->files.'"" target="_blank">Dokumen</a>';
									endif;
									?>
									<tr>
										<td>&nbsp</td>
										<td>Dokumen Kontrak</td>
										<td>: <strong><?php echo $files; ?></strong></td>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="window.close();">Tutup</button>
       	</div>
	</body>
</html>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('struktur_jabatan/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Struktur Jabatan</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("struktur_jabatan/form"); ?>">
							<div style = 'display: none' class="form-group row">
								<label class="col-sm-3 col-form-label">ID <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="number" readonly name="id" class="form-control" value="<?php echo $id; ?>">
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Branch <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="branch_id" required>
										<option value=""> &nbsp;-- PILIH BRANCH --</option>
										<?php foreach ($list_branch as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php echo ($item->id ==  $branch_id) ? ' selected="selected"' : '';?> > <?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Posisi Level <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="level_id" required>
										<option value=""> &nbsp;-- PILIH Posisi Level --</option>
										<?php foreach ($list_posisi_level as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php echo ($item->id ==  $level_id) ? ' selected="selected"' : '';?> > <?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Parent <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="parent_id" required>
										<option value=""> &nbsp;-- PILIH Parent Struktur --</option>
										<?php foreach ($list_parent as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php echo ($item->id ==  $parent_id) ? ' selected="selected"' : '';?> > <?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Name <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" name="struktur_name" class="form-control" value="<?php echo $struktur_name; ?>">
								</div>
								<div class="col-sm-4"></div>
							</div>
							
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('accident/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Template Laporak Kecelakaan</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4" method="post" action="<?php echo base_url("accident/form"); ?>">
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Bisnis Unit <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" value="<?php echo $company_name; ?>" readonly>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Template <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="text" class="form-control" placeholder="Nama Template" name="title" value="<?php echo $title; ?>" required>
								</div>
							</div><div class="alert alert-info alert-dismissible">
								<h5><i class="icon fas fa-info"></i> Catatan untuk Mail Merge</h5>
								<table class="table table-striped">
									<thead>
										<tr>
											<td width="20px"><strong>Kode</strong></td>
											<td width="250px"><strong>Keterangan</strong></td>
											<td width="20px"><strong>Kode</strong></td>
											<td width="250px"><strong>Keterangan</strong></td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>{NOMOR_SURAT}</td>
											<td>Nomor Kontrak dibuat Otomatis</td>
											<td>{NOMOR_KARYAWAN}</td>
											<td>Nomor Induk Karyawan dibuat Otomatis</td>
										</tr>
										<tr>
											<td>{NOMOR_KESEHATAN}</td>
											<td>Nomor Asuransi Kesehatan</td>
											<td>{NOMOR_KETENAGAKERJAAN}</td>
											<td>Nomor Asuransi Ketenagakerjaan</td>
										</tr>
										<tr>
											<td>{NAMA_LENGKAP}</td>
											<td>Nama Lengkap Karyawan</td>
											<td>{NIK}</td>
											<td>Nomor KTP Karyawan</td>
										</tr>
										<tr>
											<td>{TANGGAL_LAHIR}</td>
											<td>Tanggal Lahir Karyawan</td>
											<td>{TEMPAT_LAHIR}</td>
											<td>Tempat Lahir Karyawan</td>
											
										</tr>
										<tr>
											<td>{ALAMAT_KTP}</td>
											<td>Alamat Lengkap Karyawan Sesuai KTP</td>
											<td>{JABATAN}</td>
											<td>Posisi/Jabatan Karyawan</td>
										</tr>
										<tr>
											<td>{NAMA_SITE}</td>
											<td>Nama Site Penempatan Karyawan</td>
											<td>{ALAMAT_SITE}</td>
											<td>Alamat Site Penempatan Karyawan</td>
										</tr>
											<td>{NAMA_BISNIS_UNIT}</td>
											<td>Nama Instansi Penempatan Karyawan</td>
											<td>{ALAMAT_BISNIS_UNIT}</td>
											<td>Alamat Instansi Penempatan Karyawan</td>
										</tr>
										<tr>
											<td>{TANGGAL_SEKARANG}</td>
											<td>Tanggal Sekarang</td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="form-group row">
								<div class="col-sm-12">
									<textarea class='editor' name='content'>
										<?php if(isset($content)){ echo $content; } ?>	
									</textarea>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" name="action" = valuse="save" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
<script>
	$(function () {
		tinymce.init({ 
			selector:'.editor',
			theme: 'modern',
			height: 600,
			menubar: true,
			lineheight_formats: "0pt 1pt 8pt 9pt 10pt 11pt 12pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 36pt",
			plugins: [
			'advlist autolink lists lineheight nonbreaking print preview anchor',
			'searchreplace visualblocks fullscreen textcolor colorpicker',
			'tabfocus table contextmenu powerpaste'
			],
			menubar: 'file edit view insert format tools table tc help',
			toolbar: 'undo redo | bold italic underline strikethrough | forecolor backcolor | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent nonbreaking lineheightselect | table | preview print',
			powerpaste_allow_local_images: true,
			powerpaste_word_import: 'prompt',
			powerpaste_html_import: 'prompt',
		});
	});
</script>
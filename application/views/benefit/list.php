<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('benefit/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Benefit Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-2">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
						<div class="col-sm-10">
							&nbsp;
						</div>
					</div>
				</div>
				<div class="card-body">
					<table id="data-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th id='periode_end'>Periode Pembayaran</th>
								<th id='employee_name'>Nama Karyawan</th>
								<th id='id_card'>Nomor KTP</th>
								<th id='benefit_labor_note'>Program BPJS Ketenagakerjaan</th>
								<th id='benefit_labor'>Nomor BPJS Ketenaga Kerjaan</th>
								<th id='benefit_health_note'>Asuransi Kesehatan</th>
								<th id='benefit_health'>Nomor Asuransi Kesehatan</th>
								<th id='bpjs_jht'>Potongan JHT</th>
								<th id='bpjs_jp'>Potongan JP</th>
								<th id='bpjs_ks'>Potongan Kesehatan</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("benefit/list_ajax/");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'periode_end'},
				{ data: 'employee_name' },
				{ data: 'id_card' },
				{ data: 'benefit_labor_note' },
				{ data: 'benefit_labor' },
				{ data: 'benefit_health_note' },
				{ data: 'benefit_health' },
				{ data: 'bpjs_jht' },
				{ data: 'bpjs_jp' },
				{ data: 'bpjs_ks' },
			],
			'order': [[0, 'desc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>=0 && index<11){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$( ".export" ).click(function() {
			$.post( "export" );
			var url = "<?php echo base_url('benefit/export'); ?>" + "?employee_name="+$("#s_employee_name").val()+"&id_card="+$("#s_id_card").val()+"&periode_end="+$("#s_periode_end").val()+"&benefit_labor_note="+$("#s_benefit_labor_note").val()+"&benefit_labor="+$("#s_benefit_labor").val()+"&benefit_health_note="+$("#s_benefit_health_note").val()+"&benefit_health="+$("#s_benefit_health").val()+"&bpjs_jht="+$("#s_bpjs_jht").val()+"&bpjs_jp="+$("#s_bpjs_jp").val()+"&bpjs_ks="+$("#s_bpjs_ks").val();
			window.location = url;
		
		});
	});
</script>
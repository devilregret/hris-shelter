<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('benefit/labor'); ?>">Asuransi Ketenagakerjaan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas  fa-shield-virus"></i>&nbsp;Asuransi Ketenagakerjaan</h3>
				</div>
				<div class="card-body">
					<form method="GET" action="<?php echo base_url('benefit/labor'); ?>">
						<div class="row">
							<label for="name" class="col-sm-2 col-form-label">Lihat berdasarkan :</label>
							<div class="col-sm-2" >
								<select class="form-control select2 filter" name="filter">
									<option value="" >-- Pilih Pencarian --</option>
									<option value="site" <?php if($filter == 'site'): echo 'selected'; endif;?>>Site Bisnis</option>
									<option value="npptk" <?php if($filter == 'npptk'): echo 'selected'; endif;?>>NPP Ketenagakerjaan</option>
								</select>
							</div>
							<div class="col-sm-6" >
								<div class="div-nppks" <?php if($filter != 'nppks'): echo 'hidden'; endif; ?> >
									<select class="form-control select2 nppks" name="nppks" <?php if($filter != 'nppks'): echo 'disable'; endif; ?>>
										<option value=""> &nbsp; </option>
										<?php foreach ($list_nppks as $item) :  ?>
											<option value="<?php echo $item->benefit_health_company;?>" <?php if($item->benefit_health_company == $nppks): echo 'selected'; endif;?>><?php echo
											$item->benefit_health_company; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="div-npptk" <?php if($filter != 'npptk'): echo 'hidden'; endif; ?>>
									<select class="form-control select2 npptk" name="npptk" <?php if($filter != 'npptk'): echo 'disable'; endif; ?> >
										<option value=""> &nbsp; </option>
										<?php foreach ($list_npptk as $item) :  ?>
											<option value="<?php echo $item->benefit_labor_company;?>" <?php if($item->benefit_labor_company == $npptk): echo 'selected'; endif;?>><?php echo
											$item->benefit_labor_company; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="div-site" <?php if($filter != 'site'): echo 'hidden'; endif; ?>>
									<select class="form-control select2 site" name="site" <?php if($filter != 'site'): echo 'disable'; endif; ?>>
										<option value=""> &nbsp; </option>
										<?php foreach ($list_site as $item) :  ?>
											<option value="<?php echo $item->id;?>" <?php if($item->id == $site_id): echo 'selected'; endif;?>><?php echo
											$item->name.' - '.$item->company_code; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="col-sm-2" >
								<button type="submit" class="btn btn-primary btn-block">Cari</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<form method="post" action="<?php echo base_url("benefit/labor_import"); ?>" enctype="multipart/form-data">
					<div class="row">
						<div class="col-sm-4">
							<a href="<?php echo base_url('files/template_ketenagakerjaan.xlsx'); ?>" target="_blank" class="btn btn-success"><i class="fas fa-file-excel"></i> Template Ketenagakerjaan</a>&nbsp;
						</div>
						<div class="col-sm-2">
							<input class="form-control" type="hidden" name="site_id" value="<?php echo $site_id; ?>" />
							<div class="form-group">
								<div class="input-group date" id="reservationdate" data-target-input="nearest">
									<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name='periode' />
									<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="input-group">
								<div class="custom-file">
									<input type="file" class="custom-file-input btn-sm" name="file" accept=".xlsx" required> 
									<label class="custom-file-label" for="exampleInputFile">upload Data</label>
								</div>
								<div class="input-group-append">
									<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>   
								</div>
							</div>
						</div>
					</div>
					</form>
					<?php if($failed): ?>
					<p>&nbsp;</p>
					<div class="row  float-right">
						<a target="_blank" class="btn btn-success" href="<?php echo base_url('benefit/export/'.$report_id); ?>" style="width:200px;"><i class="fas fa-file-csv"></i> Daftar Upload Gagal</a> &nbsp;
						<a onclick="confirm_del(this)" class="btn btn-danger" data-href="<?php echo base_url('benefit/delete/'.$report_id); ?>" style="width:200px;"><i class="fas fa-trash-alt"></i> Hapus</a>
					</div>
					<?php endif; ?>
				</div>
				<div class="card-body">
					<?php if($site_id): ?>
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
							<tr>
								<td width="250px">Jumlah Karyawan</td>									
								<td>: <?php echo $employee.' Karyawan'; ?></td>
							</tr>
						</thead>
					</table>
					<?php endif; ?>
					<p>&nbsp;</p>
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th id='id_card'>Periode</th>
									<th id='site_name'>Site Bisnis</th>
									<th id='npp'>NPP Ketenagakerjaan</th>
									<th id='full_name'>Jumlah Dibayarkan</th>
									<th id='site_name'>Total Pembayaran</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			 'searching'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("benefit/labor_ajax?site=".$site_id."&nppks=".$nppks."&npptk=".$npptk);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'periode' },
				{ data: 'site_name' },
				{ data: 'benefit_labor_company' },
				{ data: 'total_payment' },
				{ data: 'total_employee' },
				{ data: 'action', 'orderable':false, 'searchable':false  }
			],
			'order': [[0, 'desc']]
		});

		
		$('#reservationdate').datetimepicker({
			format: 'YYYY-MM'
		});
		
		$('.filter').on('change', function() {
			if(this.value == 'site'){
				$('.nppks').attr('disabled', 'disabled');
				$('.npptk').attr('disabled', 'disabled');
				$('.site').removeAttr('disabled');

				$('.div-nppks').attr('hidden', true);
				$('.div-npptk').attr('hidden', true);
				$('.div-site').removeAttr('hidden');
			}else if(this.value == 'npptk'){
				$('.nppks').attr('disabled', 'disabled');
				$('.site').attr('disabled', 'disabled');
				$('.npptk').removeAttr('disabled');

				$('.div-site').attr('hidden', true);
				$('.div-nppks').attr('hidden', true);
				$('.div-npptk').removeAttr('hidden');
			}else if(this.value == 'nppks'){
				$('.npptk').attr('disabled', 'disabled');
				$('.site').attr('disabled', 'disabled');
				$('.nppks').removeAttr('disabled');

				$('.div-site').attr('hidden', true);
				$('.div-npptk').attr('hidden', true);
				$('.div-nppks').removeAttr('hidden');
			}
		});
	});
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('benefit/health?site='); ?>">Asuransi Kesehatan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Asuransi Kesehatan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-4">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
						<div class="col-sm-8 process" hidden="true">
							<a href="#" class="btn btn-danger delete float-right"  style="margin-right:10px;"><i class="fas fa-trash"></i> Hapus</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<?php if($site_id): ?>
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
							<tr>
								<td width="250px">Periode Pembayaran</td>
								<td>: <?php echo get_month(substr($periode, 5, 2)).' '.substr($periode, 0, 4); ?></td>
							</tr>
							<tr>
								<td width="250px">Jumlah dibayarkan</td>									
								<td>: <?php echo $total_employee.' Karyawan'; ?></td>
							</tr>
							<tr>
								<td width="250px">Tota Pembayaran</td>									
								<td>: <?php echo $total_payment; ?></td>
							</tr>
						</thead>
					</table>
					<?php endif; ?>
					<p>&nbsp;</p>
					<form method="POST" id="process" action="<?php echo base_url('benefit/health_process'); ?>">
					<input type="hidden" name="nppks" value="<?php echo $nppks;  ?>">
					<input type="hidden" name="npptk" value="<?php echo $npptk;  ?>">
					<input type="hidden" name="site_id" value="<?php echo $site_id;  ?>">
					<input type="hidden" name="periode" value="<?php echo $periode;  ?>">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>
										<input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll">
									</th>
									<th id='id_card'>NIK KTP</th>
									<th id='full_name'>Nama Karyawan</th>
									<th id='card_number'>NO KIS</th>
									<th id='site_name'>Site Bisnis</th>
									<th id='npp'>NPP Kesehatan</th>
									<th id='employee_payment'>Pembayaran Karyawan</th>
									<th id='company_payment'>Pembayaran Perusahaan</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.preview').on('change', function() {
			window.location.href = this.value;
		});
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			 'searching'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("benefit/health_detail_ajax?site=".$site_id."&periode=".$periode."&nppks=".$nppks."&npptk=".$npptk);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'id_card' },
				{ data: 'full_name' },
				{ data: 'card_number' },
				{ data: 'site_name' },
				{ data: 'benefit_health_company' },
				{ data: 'employee_payment' },
				{ data: 'company_payment' }
			],
			'order': [[2, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<8){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$( ".export" ).click(function() {
			var url = "<?php echo base_url('benefit/health_export?site_id='.$site_id.'&is_health_company='.$nppks.'&is_labor_company='.$npptk.'&preview='.$periode); ?>" + "?id_card="+$("#s_id_card").val()+"&card_number="+$("#s_card_number").val()+"&full_name="+$("#s_full_name").val();
			window.location = url;
		});

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".process").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".process").prop('hidden', false);
    		});
		});
		$("input:checkbox").change(function() {
			$(".process").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".process").prop('hidden', false);
    		});
		});
		$( ".btn.delete" ).click(function() {
			$( "#process" ).submit();
		});
	});
</script>
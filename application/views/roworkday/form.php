<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('roworkday/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Ubah Data Site Bisnis</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("roworkday/form"); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label"> Hari Kerja<span class="text-danger"> *</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<select class="form-control select2" name="day" data-placeholder="Pilih Hari Kerja" required>
										<option value='Minggu' <?php if("Minggu"==$day): echo "selected"; endif;?>>Minggu</option>
										<option value='Senin' <?php if("Senin"==$day): echo "selected"; endif;?>>Senin</option>
										<option value='Selasa' <?php if("Selasa"==$day): echo "selected"; endif;?>>Selasa</option>
										<option value='Rabu' <?php if("Rabu"==$day): echo "selected"; endif;?>>Rabu</option>
										<option value='Kamis' <?php if("Kamis"==$day): echo "selected"; endif;?>>Kamis</option>
										<option value='Jumat' <?php if("Jumat"==$day): echo "selected"; endif;?>>Jumat</option>
										<option value='Sabtu' <?php if("Sabtu"==$day): echo "selected"; endif;?>>Sabtu</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Shift <span class="text-danger"> *</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="shift_id" required>
										<?php foreach ($shift as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $shift_id): echo "selected"; endif;?>><?php echo $item->shift; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Jam Mulai <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="hours_start" required>
										<option value="00:00" <?php if("00:00" === $hours_start): echo "selected"; endif;?>> 00:00</option>
										<option value="00:30" <?php if("00:30" === $hours_start): echo "selected"; endif;?>> 00:30</option>
										<option value="01:00" <?php if("01:00" === $hours_start): echo "selected"; endif;?>> 01:00</option>
										<option value="01:30" <?php if("01:30" === $hours_start): echo "selected"; endif;?>> 01:30</option>
										<option value="02:00" <?php if("02:00" === $hours_start): echo "selected"; endif;?>> 02:00</option>
										<option value="02:30" <?php if("02:30" === $hours_start): echo "selected"; endif;?>> 02:30</option>
										<option value="03:00" <?php if("03:00" === $hours_start): echo "selected"; endif;?>> 03:00</option>
										<option value="03:30" <?php if("03:30" === $hours_start): echo "selected"; endif;?>> 03:30</option>
										<option value="04:00" <?php if("04:00" === $hours_start): echo "selected"; endif;?>> 04:00</option>
										<option value="04:30" <?php if("04:30" === $hours_start): echo "selected"; endif;?>> 04:30</option>
										<option value="05:00" <?php if("05:00" === $hours_start): echo "selected"; endif;?>> 05:00</option>
										<option value="05:30" <?php if("05:30" === $hours_start): echo "selected"; endif;?>> 05:30</option>
										<option value="06:00" <?php if("06:00" === $hours_start): echo "selected"; endif;?>> 06:00</option>
										<option value="06:30" <?php if("06:30" === $hours_start): echo "selected"; endif;?>> 06:30</option>
										<option value="07:00" <?php if("07:00" === $hours_start): echo "selected"; endif;?>> 07:00</option>
										<option value="07:30" <?php if("07:30" === $hours_start): echo "selected"; endif;?>> 07:30</option>
										<option value="08:00" <?php if("08:00" === $hours_start): echo "selected"; endif;?>> 08:00</option>
										<option value="08:30" <?php if("08:30" === $hours_start): echo "selected"; endif;?>> 08:30</option>
										<option value="09:00" <?php if("09:00" === $hours_start): echo "selected"; endif;?>> 09:00</option>
										<option value="09:30" <?php if("09:30" === $hours_start): echo "selected"; endif;?>> 09:30</option>
										<option value="10:00" <?php if("10:00" === $hours_start): echo "selected"; endif;?>> 10:00</option>
										<option value="10:30" <?php if("10:30" === $hours_start): echo "selected"; endif;?>> 10:30</option>
										<option value="11:00" <?php if("11:00" === $hours_start): echo "selected"; endif;?>> 11:00</option>
										<option value="11:30" <?php if("11:30" === $hours_start): echo "selected"; endif;?>> 11:30</option>
										<option value="12:00" <?php if("12:00" === $hours_start): echo "selected"; endif;?>> 12:00</option>
										<option value="12:30" <?php if("12:30" === $hours_start): echo "selected"; endif;?>> 12:30</option>
										<option value="13:00" <?php if("13:00" === $hours_start): echo "selected"; endif;?>> 13:00</option>
										<option value="13:30" <?php if("13:30" === $hours_start): echo "selected"; endif;?>> 13:30</option>
										<option value="14:00" <?php if("14:00" === $hours_start): echo "selected"; endif;?>> 14:00</option>
										<option value="14:30" <?php if("14:30" === $hours_start): echo "selected"; endif;?>> 14:30</option>
										<option value="15:00" <?php if("15:00" === $hours_start): echo "selected"; endif;?>> 15:00</option>
										<option value="15:30" <?php if("15:30" === $hours_start): echo "selected"; endif;?>> 15:30</option>
										<option value="16:00" <?php if("16:00" === $hours_start): echo "selected"; endif;?>> 16:00</option>
										<option value="16:30" <?php if("16:30" === $hours_start): echo "selected"; endif;?>> 16:30</option>
										<option value="17:00" <?php if("17:00" === $hours_start): echo "selected"; endif;?>> 17:00</option>
										<option value="17:30" <?php if("17:30" === $hours_start): echo "selected"; endif;?>> 17:30</option>
										<option value="18:00" <?php if("18:00" === $hours_start): echo "selected"; endif;?>> 18:00</option>
										<option value="18:30" <?php if("18:30" === $hours_start): echo "selected"; endif;?>> 18:30</option>
										<option value="19:00" <?php if("19:00" === $hours_start): echo "selected"; endif;?>> 19:00</option>
										<option value="19:30" <?php if("19:30" === $hours_start): echo "selected"; endif;?>> 19:30</option>
										<option value="20:00" <?php if("20:00" === $hours_start): echo "selected"; endif;?>> 20:00</option>
										<option value="20:30" <?php if("20:30" === $hours_start): echo "selected"; endif;?>> 20:30</option>
										<option value="21:00" <?php if("21:00" === $hours_start): echo "selected"; endif;?>> 21:00</option>
										<option value="21:30" <?php if("21:30" === $hours_start): echo "selected"; endif;?>> 21:30</option>
										<option value="22:00" <?php if("22:00" === $hours_start): echo "selected"; endif;?>> 22:00</option>
										<option value="22:30" <?php if("22:30" === $hours_start): echo "selected"; endif;?>> 22:30</option>
										<option value="23:00" <?php if("23:00" === $hours_start): echo "selected"; endif;?>> 23:00</option>
										<option value="23:30" <?php if("23:30" === $hours_start): echo "selected"; endif;?>> 23:30</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Jam Selesai <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="hours_end" required>
										<option value="00:00" <?php if("00:00" === $hours_end): echo "selected"; endif;?>> 00:00</option>
										<option value="00:30" <?php if("00:30" === $hours_end): echo "selected"; endif;?>> 00:30</option>
										<option value="01:00" <?php if("01:00" === $hours_end): echo "selected"; endif;?>> 01:00</option>
										<option value="01:30" <?php if("01:30" === $hours_end): echo "selected"; endif;?>> 01:30</option>
										<option value="02:00" <?php if("02:00" === $hours_end): echo "selected"; endif;?>> 02:00</option>
										<option value="02:30" <?php if("02:30" === $hours_end): echo "selected"; endif;?>> 02:30</option>
										<option value="03:00" <?php if("03:00" === $hours_end): echo "selected"; endif;?>> 03:00</option>
										<option value="03:30" <?php if("03:30" === $hours_end): echo "selected"; endif;?>> 03:30</option>
										<option value="04:00" <?php if("04:00" === $hours_end): echo "selected"; endif;?>> 04:00</option>
										<option value="04:30" <?php if("04:30" === $hours_end): echo "selected"; endif;?>> 04:30</option>
										<option value="05:00" <?php if("05:00" === $hours_end): echo "selected"; endif;?>> 05:00</option>
										<option value="05:30" <?php if("05:30" === $hours_end): echo "selected"; endif;?>> 05:30</option>
										<option value="06:00" <?php if("06:00" === $hours_end): echo "selected"; endif;?>> 06:00</option>
										<option value="06:30" <?php if("06:30" === $hours_end): echo "selected"; endif;?>> 06:30</option>
										<option value="07:00" <?php if("07:00" === $hours_end): echo "selected"; endif;?>> 07:00</option>
										<option value="07:30" <?php if("07:30" === $hours_end): echo "selected"; endif;?>> 07:30</option>
										<option value="08:00" <?php if("08:00" === $hours_end): echo "selected"; endif;?>> 08:00</option>
										<option value="08:30" <?php if("08:30" === $hours_end): echo "selected"; endif;?>> 08:30</option>
										<option value="09:00" <?php if("09:00" === $hours_end): echo "selected"; endif;?>> 09:00</option>
										<option value="09:30" <?php if("09:30" === $hours_end): echo "selected"; endif;?>> 09:30</option>
										<option value="10:00" <?php if("10:00" === $hours_end): echo "selected"; endif;?>> 10:00</option>
										<option value="10:30" <?php if("10:30" === $hours_end): echo "selected"; endif;?>> 10:30</option>
										<option value="11:00" <?php if("11:00" === $hours_end): echo "selected"; endif;?>> 11:00</option>
										<option value="11:30" <?php if("11:30" === $hours_end): echo "selected"; endif;?>> 11:30</option>
										<option value="12:00" <?php if("12:00" === $hours_end): echo "selected"; endif;?>> 12:00</option>
										<option value="12:30" <?php if("12:30" === $hours_end): echo "selected"; endif;?>> 12:30</option>
										<option value="13:00" <?php if("13:00" === $hours_end): echo "selected"; endif;?>> 13:00</option>
										<option value="13:30" <?php if("13:30" === $hours_end): echo "selected"; endif;?>> 13:30</option>
										<option value="14:00" <?php if("14:00" === $hours_end): echo "selected"; endif;?>> 14:00</option>
										<option value="14:30" <?php if("14:30" === $hours_end): echo "selected"; endif;?>> 14:30</option>
										<option value="15:00" <?php if("15:00" === $hours_end): echo "selected"; endif;?>> 15:00</option>
										<option value="15:30" <?php if("15:30" === $hours_end): echo "selected"; endif;?>> 15:30</option>
										<option value="16:00" <?php if("16:00" === $hours_end): echo "selected"; endif;?>> 16:00</option>
										<option value="16:30" <?php if("16:30" === $hours_end): echo "selected"; endif;?>> 16:30</option>
										<option value="17:00" <?php if("17:00" === $hours_end): echo "selected"; endif;?>> 17:00</option>
										<option value="17:30" <?php if("17:30" === $hours_end): echo "selected"; endif;?>> 17:30</option>
										<option value="18:00" <?php if("18:00" === $hours_end): echo "selected"; endif;?>> 18:00</option>
										<option value="18:30" <?php if("18:30" === $hours_end): echo "selected"; endif;?>> 18:30</option>
										<option value="19:00" <?php if("19:00" === $hours_end): echo "selected"; endif;?>> 19:00</option>
										<option value="19:30" <?php if("19:30" === $hours_end): echo "selected"; endif;?>> 19:30</option>
										<option value="20:00" <?php if("20:00" === $hours_end): echo "selected"; endif;?>> 20:00</option>
										<option value="20:30" <?php if("20:30" === $hours_end): echo "selected"; endif;?>> 20:30</option>
										<option value="21:00" <?php if("21:00" === $hours_end): echo "selected"; endif;?>> 21:00</option>
										<option value="21:30" <?php if("21:30" === $hours_end): echo "selected"; endif;?>> 21:30</option>
										<option value="22:00" <?php if("22:00" === $hours_end): echo "selected"; endif;?>> 22:00</option>
										<option value="22:30" <?php if("22:30" === $hours_end): echo "selected"; endif;?>> 22:30</option>
										<option value="23:00" <?php if("23:00" === $hours_end): echo "selected"; endif;?>> 23:00</option>
										<option value="23:30" <?php if("23:30" === $hours_end): echo "selected"; endif;?>> 23:30</option>
									</select>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
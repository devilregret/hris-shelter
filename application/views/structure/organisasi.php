<?php echo asset_js("js/jquery.orgchart.js"); ?>
<?php echo asset_js("js/jquery.doubleScroll.js"); ?>
<?php echo asset_css("css/jquery.orgchart.css"); ?>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('structure/approval'); ?>">Struktur Organisasi</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Struktur Organisasi</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div id="edit-panel" class="view-state">
							<input type="text" id="key-word">
							<button type="button" class="btn btn-primary btn-sm" id="btn-filter-node">Filter</button>
							<button type="button" class="btn btn-danger btn-sm" id="btn-cancel">Cancel</button>
						</div>
					</div>
				</div>
				<div class="card-body scrollmenu">
					<div id="chart-container"></div>
				</div>
			</div>
		</div>
	</section>
</div>
<style type="text/css">
	html,body {
		height: 100%;
		margin: 0;
		padding: 0;
		padding: 1px;
		box-sizing: border-box;
	}
	body {
		font-family: Arial;
		color: #333333;
	}
	#chart-container {
		position: relative;
		border: 1px solid #aaa;
		margin: 0.5rem;
		overflow: auto;
		text-align: center;
	}
	.orgchart .node.matched { background-color: rgba(238, 217, 54, 0.5); }
	.orgchart .hierarchy.first-shown::before {
		left: calc(50% - 1px);
		width: calc(50% + 1px);
	}
	.orgchart .hierarchy.last-shown::before {
		width: calc(50% + 1px);
	}
	.orgchart .hierarchy.first-shown.last-shown::before {
		width: 2px;
	}

</style>
<script type="text/javascript">
	function loopChart ($hierarchy) {
		var $siblings = $hierarchy.children('.nodes').children('.hierarchy');
		if ($siblings.length) {
			$siblings.filter(':not(.hidden)').first().addClass('first-shown').end().last().addClass('last-shown');
		}
		$siblings.each(function(index, sibling) {
			loopChart($(sibling));
		});
	}
	function filterNodes(keyWord) {
		if(!keyWord.length) {
			window.alert('Silahkan Masukkan Kata pencarian.');
			return;
		} else {
			var $chart = $('.orgchart');
			$chart.addClass('noncollapsable');
			$chart.find('.node').filter(function(index, node) {
				return $(node).text().toLowerCase().indexOf(keyWord) > -1;
			}).addClass('matched').closest('.hierarchy').parents('.hierarchy').children('.node').addClass('retained');

			$chart.find('.matched,.retained').each(function(index, node) {
				$(node).removeClass('slide-up').closest('.nodes').removeClass('hidden').siblings('.hierarchy').removeClass('isChildrenCollapsed');
				var $unmatched = $(node).closest('.hierarchy').siblings().find('.node:first:not(.matched,.retained)').closest('.hierarchy').addClass('hidden');
			});
			$chart.find('.matched').each(function(index, node) {
				if (!$(node).siblings('.nodes').find('.matched').length) {
					$(node).siblings('.nodes').addClass('hidden').parent().addClass('isChildrenCollapsed');
				}
			});
			loopChart($chart.find('.hierarchy:first'));
		}
	}
	
	function clearFilterResult() {
		$('.orgchart').removeClass('noncollapsable').find('.node').removeClass('matched retained').end().find('.hidden, .isChildrenCollapsed, .first-shown, .last-shown').removeClass('hidden isChildrenCollapsed first-shown last-shown').end().find('.slide-up, .slide-left, .slide-right').removeClass('slide-up slide-right slide-left');
	}
	$(document).ready(function(){
	  	$('#chart-container').doubleScroll();
		$.ajax({
			url: "<?php echo base_url('structure/list_organisasi'); ?>",
			dataType: "json",
			success: function(data) {
				$('#chart-container').orgchart({
					'data' : data,
					'nodeContent': 'title'
				});
			}
		});
		
		$(function() {
			$('#btn-filter-node').on('click', function() {
				filterNodes($('#key-word').val());
			});
			$('#btn-cancel').on('click', function() {
				clearFilterResult();
			});
			$('#key-word').on('keyup', function(event) {
				if (event.which === 13) {
					filterNodes(this.value);
				} else if (event.which === 8 && this.value.length === 0) {
					clearFilterResult();
				}
			});
		});
	});
  </script>
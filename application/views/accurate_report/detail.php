<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('payroll_report/list/'.$site->id); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Laporan Payroll Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-12">
							<a href="<?php echo base_url('accurate_report/detail_export/'.$proyek_id.'/'.$site->id.'/'.$resume->periode_end); ?>" target="_blank" class="btn btn-success export"><i class="fas fa-file-export"></i> Export</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
							<?php
								if($resume){ 
								$start_date = substr($resume->periode_start, 8, 2).' '.get_month(substr($resume->periode_start, 5, 2));
								$end_date 	= substr($resume->periode_end, 8, 2).' '.get_month(substr($resume->periode_end, 5, 2)).' '.substr($resume->periode_end, 0, 4);
							?>
							<tr>
								<td width="250px"><strong>Periode Terakhir</strong></td>
								<td><strong>: <?php echo $start_date; ?> - <?php echo $end_date; ?></strong></td>
							</tr>
							<tr>
								<td width="250px"><strong>Total Karyawan Aktif</strong></td>
								<td><strong>: <?php echo $employee; ?> Karyawan</strong></td>
							</tr>
							<tr>
								<td width="250px"><strong>Total Karyawan digaji</strong></td>
								<td><strong>: <?php echo $resume->employee_payroll; ?> Karyawan</strong></td>
							</tr>
							<tr>
								<td width="250px"><strong>Total Payroll </strong></td>
								<td><strong>: <?php echo format_rupiah($resume->summary_payroll); ?></strong></td>
							</tr>
							<?php } ?>
						</thead>
					</table>
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="50px">No</th>
									<th>Nama</th>
									<th>Department</th>
									<th>Bank</th>
									<th>Atas Nama</th>
									<th>No Rekening</th>
									<th>THP</th>
								</tr>
							</thead>
							<tfoot>
								<?php 
								$i = 1;
								foreach ($payroll as $item):
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $item->employee_name; ?></td>
									<td><?php echo $item->position; ?></td>
									<td><?php echo $item->bank_name; ?></td>
									<td><?php echo $item->employee_name; ?></td>
									<td><?php echo $item->bank_account; ?></td>
									<td><?php echo format_rupiah($item->salary); ?></td>
								</tr>
								<?php
								$i++;
								endforeach;
								?>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<?php 
$ex = $this->employee_contract_model->gets_expired(array('expired' => 0, 'columns' => 'COUNT(A.employee_id) AS summary'));
$employee_expired = 0;
foreach($ex AS $item):
	$employee_expired = $employee_expired+$item->summary;
endforeach;
?>
<ul class="navbar-nav" style="width:100px;position:absolute;bottom:10px;right:100px;">
	<li class="nav-item dropdown">
		<a class="nav-link" data-toggle="dropdown" href="#">
			<i class="far fa-bell" style="font-size:30px;"></i>
			<span class="badge badge-danger navbar-badge" style="font-size:12px;"><?php echo $employee_expired; ?></span>
		</a>
		<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
			<span class="dropdown-item dropdown-header">Notifikasi</span>
			<div class="dropdown-divider"></div>
			<a href="<?php echo base_url('employee_contract/list/direct/expired'); ?>" class="dropdown-item">
				<i class="fas fa-envelope mr-2"></i> <?php echo $employee_expired; ?> Kontrak Karyawan Habis 
			</a>
		</div>
	</li>
</ul>
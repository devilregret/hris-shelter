<?php 
// $time = date("Y-m-d", strtotime("-7 days", $time));
$notif_regiter 	= $this->candidate_model->gets_register(array('columns' => 'A.id', 'status_approval' => 1, 'last_date' => date("Y-m-d")), TRUE);
?>
<ul class="navbar-nav" style="width:100px;position:absolute;bottom:10px;right:100px;">
	<li class="nav-item dropdown">
		<a class="nav-link" data-toggle="dropdown" href="#">
			<i class="far fa-bell" style="font-size:30px;"></i>
			<span class="badge badge-danger navbar-badge" style="font-size:12px;"><?php echo $notif_regiter; ?></span>
		</a>
		<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
			<span class="dropdown-item dropdown-header">Notifikasi</span>
			<div class="dropdown-divider"></div>
			<a href="<?php echo base_url('candidate/list'); ?>" class="dropdown-item">
				<i class="fas fa-envelope mr-2"></i> <?php echo $notif_regiter; ?> Pendaftar Hari ini 
			</a>
		</div>
	</li>
</ul>
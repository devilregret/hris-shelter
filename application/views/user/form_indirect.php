<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('user/indirect'); ?>">Daftar Akun Indirect</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Akun Indirect</h3>
				</div>
				<?php if($id == ''): ?>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("user/form_indirect"); ?>">
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Lengkap <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="text" class="form-control" placeholder="Nama Lengkap" name="full_name" value="<?php echo $full_name; ?>" readonly>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Username <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Username" name="username" value="<?php echo $username; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Email <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="email" class="form-control" placeholder="Email" name="email" value="<?php echo $email; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Bisnis Unit <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="company_id" required>
										<option value=""> &nbsp;-- PILIH BISNIS UNIT --</option>
										<?php foreach ($list_company as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $company_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Branch <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="branch_id" required>
										<option value=""> &nbsp;-- PILIH BRANCH --</option>
										<?php foreach ($list_branch as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $branch_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Hak Akses <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="role_id" required>
										<option value=""> &nbsp;-- PILIH HAK AKSES --</option>
										<?php foreach ($list_role as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $role_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Password <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="password" class="form-control" placeholder="password" name="password" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Ulang Password <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="password" class="form-control" placeholder="Ulang Password" name="repassword" required>
								</div>
							</div>
							<hr>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" name="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
				<?php else: ?>
				<div class="card card-primary card-tabs">
					<div class="card-header p-0 pt-1">
						<ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
							<li class="nav-item">
								<a class="nav-link <?php if ($tab==="profile"): echo 'active'; endif; ?>" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="true">Profile</a>
							</li>
							<li class="nav-item">
								<a class="nav-link <?php if ($tab==="password"): echo 'active'; endif; ?>" id="custom-tabs-one-password-tab" data-toggle="pill" href="#custom-tabs-one-password" role="tab" aria-controls="custom-tabs-one-password" aria-selected="false">Password</a>
							</li>
						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content" id="custom-tabs-one-tabContent">
							<div class="tab-pane fade <?php if ($tab==="profile"): echo 'show active'; endif; ?>" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
								<form class="form-horizontal col-sm-10 mt-2" method="post" action="<?php echo base_url("user/form_indirect"); ?>">
									<div class="form-group row">
										<label class="col-sm-3 col-form-label">Nama Lengkap <span class="text-danger">*</span></label>
										<div class="col-sm-9">
											<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
											<input type="text" class="form-control" placeholder="Nama Lengkap" name="full_name" value="<?php echo $full_name; ?>" readonly>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 col-form-label">Username <span class="text-danger">*</span></label>
										<div class="col-sm-9">
											<input type="text" class="form-control" placeholder="Username" name="username" value="<?php echo $username; ?>" required>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 col-form-label">Email <span class="text-danger"></span></label>
										<div class="col-sm-9">
											<input type="email" class="form-control" placeholder="Email" name="email" value="<?php echo $email; ?>">
										</div>
									</div>
									<div class="form-group row">
										<label for="name" class="col-sm-3 col-form-label">Bisnis Unit <span class="text-danger">*</span></label>
										<div class="col-sm-9">
											<select class="form-control select2" name="company_id" required>
												<option value=""> &nbsp;-- PILIH BISNIS UNIT --</option>
												<?php foreach ($list_company as $item) :  ?>
												<option value="<?php echo $item->id;?>" <?php if($item->id === $company_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="name" class="col-sm-3 col-form-label">Branch <span class="text-danger">*</span></label>
										<div class="col-sm-9">
											<select class="form-control select2" name="branch_id" required>
												<option value=""> &nbsp;-- PILIH BRANCH --</option>
												<?php foreach ($list_branch as $item) :  ?>
												<option value="<?php echo $item->id;?>" <?php if($item->id === $branch_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="name" class="col-sm-3 col-form-label">HAK AKSES <span class="text-danger">*</span></label>
										<div class="col-sm-9">
											<select class="form-control select2" name="role_id" required>
												<option value=""> &nbsp;-- PILIH HAK AKSES --</option>
												<?php foreach ($list_role as $item) :  ?>
												<option value="<?php echo $item->id;?>" <?php if($item->id === $role_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
									<hr>
									<div class="form-group row  float-right">
										<button type="reset" class="btn btn-default btn-form">Batal</button>&nbsp;
										<button type="submit" name="submit" class="btn btn-primary btn-form">Simpan</button>
									</div>
								</form>
							</div>
							<div class="tab-pane fade <?php if ($tab==="password"): echo 'show active'; endif; ?>" id="custom-tabs-one-password" role="tabpanel" aria-labelledby="custom-tabs-one-password-tab">
								<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("user/form_indirect"); ?>">
									<div class="form-group row">
										<label for="name" class="col-sm-3 col-form-label">Password <span class="text-danger">*</span></label>
										<div class="col-sm-9">
											<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
											<input type="password" class="form-control" placeholder="password" name="password" required>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 col-form-label">Ulang Password <span class="text-danger">*</span></label>
										<div class="col-sm-9">
											<input type="password" class="form-control" placeholder="Ulang Password" name="repassword" required>
										</div>
									</div>
									<hr>
									<div class="form-group row  float-right">
										<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
										<button type="submit" name="submit" class="btn btn-primary btn-form">Simpan</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
</div>
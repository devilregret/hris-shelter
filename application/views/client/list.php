<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('client/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Perusahaan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-12">
						<a href="<?php echo base_url("client/syncronize_customer"); ?>" class="btn btn-danger btn-sm"><i class="fas fa-sync"></i> Sinkronisasi Accurate</a>&nbsp;
						<a href="<?php echo base_url("client/form"); ?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Tambah Perusahaan</a>
						</div>
					</div>
				</div>
				<div class="card-body">					
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="50px">No</th>
									<th id='name'>Nama Perusahaan</th>
									<th id='address'>Alamat Perusahaan</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("client/list_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'columns': [
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'name' },
				{ data: 'address' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'order': [[1, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<11){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
	});
</script>
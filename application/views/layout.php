<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<link rel="shortcut icon" href="<?php echo asset_img('favicon.ico'); ?>" type="image/x-icon">
		<link rel="icon" href="<?php echo asset_img('favicon.ico'); ?>" type="image/x-icon">

		<?php echo asset_css("css/adminlte.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/select2/css/select2.min.css"); ?>
		<?php echo asset_css("plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
		<?php echo asset_css("css/adminlte.css"); ?>
		<?php echo asset_css("css/main.css"); ?>
 		<?php echo asset_js("plugins/jquery/jquery.js"); ?>	
		<?php echo asset_js("js/image-resizer.js"); ?>

		<?php echo asset_css("plugins/daterangepicker/daterangepicker.css"); ?>
		<?php echo asset_css("plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"); ?>
	</head>
	<body class="hold-transition sidebar-mini layout-fixed">
		<div class="wrapper">
			<nav class="main-header navbar navbar-expand navbar-white navbar-light">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
					</li>
				</ul>
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="#" onclick="history.back()"><i class="fa fa-backward"></i></a>
					</li>
				</ul>
				<?php $this->load->view("notification/notification"); ?>
				<ul class="navbar-nav" style="width:100px;position:absolute;bottom:10px;right:20px;">
					<li class="nav-item bg-danger" style="font-size:17px;">
						<a href="<?php echo base_url('auth/logout'); ?>" class="nav-link">
							<i class="nav-icon fa fa-sign-out-alt"></i><span>Keluar</span>
						</a>
					</li>
				</ul>
			</nav>
			<?php $this->load->view("sidebar/sidebar"); ?>
			<?php $this->load->view($_PAGE_); ?>
			<footer class="main-footer">
				<strong>Copyright &copy; 2021 <a href="<?php echo base_url(); ?>" target="_BLANK">Shelter</a>.</strong>
				All rights reserved.
			</footer>
		</div>
		<?php echo asset_js("plugins/bootstrap/js/bootstrap.bundle.min.js"); ?>
		<?php echo asset_js("plugins/select2/js/select2.full.min.js"); ?>
		<?php echo asset_js("plugins/moment/moment.min.js"); ?>
		<?php echo asset_js("plugins/daterangepicker/daterangepicker.js"); ?>
		<?php echo asset_js("plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"); ?>
		<?php echo asset_js("plugins/bs-custom-file-input/bs-custom-file-input.min.js"); ?>
		<?php echo asset_js("plugins/datatables/jquery.dataTables.js"); ?>
		<?php echo asset_js("plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"); ?>
		<?php echo asset_js("plugins/datatables-responsive/js/dataTables.responsive.min.js"); ?>
		<?php echo asset_js("plugins/datatables-responsive/js/responsive.bootstrap4.min.js"); ?>
		<?php echo asset_js("plugins/tinymce/tinymce.min.js"); ?>
		<?php echo asset_js("js/adminlte.js"); ?>
		<?php echo asset_js("js/demo.js"); ?>
		<?php echo asset_js("js/main.js"); ?>
	</body>
</html>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('rosecurity/employee'); ?>">Daftar Karyawan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-4">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
						<div class="col-sm-8">
							<form method="post" action="<?php echo base_url("rosecurity/employee_import"); ?>" enctype="multipart/form-data">
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input btn-sm" name="file" accept=".xlsx" required> 
										<label class="custom-file-label" for="exampleInputFile">Update Data</label>
									</div>
									<div class="input-group-append">
										<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>   
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
				<div class="card-body">
					<div class="alert alert-info alert-dismissible">
						<h5><i class="icon fas fa-info"></i> Informasi</h5>
						<ul>
							<li>gunakan data export untuk update data</li>
							<li>Update data hanya bisa digunakan untuk update nomor Telp/WA, kode ptkp, nama bank, nomor rekening, dan atas nama no rekening karyawan</li>
						</ul> 
					</div>
					<p>&nbsp;</p>
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th id='pin_finger'>PIN FInger</th>
									<th id='employee_number'>ID Karyawan</th>
									<th id='id_card'>KTP</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='phone_number'>Nomor Whatsapp</th>
									<th id='ptkp'>Kode PTKP</th>
									<th id='bank_name'>Nama Bank</th>
									<th id='bank_account'>No Rekening</th>
									<th id='bank_account_name'>Atas Nama</th>
									<th id='company_name'>Unit Kontrak</th>
									<th id='site_name'>Nama Site</th>
									<th id='position'>Jabatan</th>
									<th id='contract_type'>Tipe Kontrak</th>
									<th id='contract_start'>Tanggal Masuk</th>
									<th id='contract_end'>Akhir Kontrak</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			 'searching'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("rosecurity/employee_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'pin_finger' },
				{ data: 'employee_number' },
				{ data: 'id_card' },
				{ data: 'full_name' },
				{ data: 'phone_number' },
				{ data: 'ptkp' },
				{ data: 'bank_name' },
				{ data: 'bank_account' },
				{ data: 'bank_account_name' },
				{ data: 'company_name' },
				{ data: 'site_name' },
				{ data: 'position' },
				{ data: 'contract_type' },
				{ data: 'contract_start' },
				{ data: 'contract_end' },
				{ data: 'action', 'orderable':false, 'searchable':false  }
			],
			'order': [[1, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>=0 && index<12){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$( ".export" ).click(function() {
			var url = "<?php echo base_url('rosecurity/employee_export'); ?>" + "?pin_finger="+$("#s_pin_finger").val()+"&full_name="+$("#s_full_name").val()+"&employee_number="+$("#s_employee_number").val()+"&phone_number="+$("#s_phone_number").val()+"&company_name="+$("#s_company_name").val()+"&position="+$("#s_position").val()+"&contract_type="+$("#s_contract_type").val()+"&id_card="+$("#s_id_card").val()+"&bank_account="+$("#s_bank_account").val()+"&ptkp="+$("#s_ptkp").val()+"&bank_name="+$("#s_bank_name").val()+"&bank_account_name="+$("#s_bank_account_name").val();
			window.location = url;
		
		});

	});
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('rosecurity/employee'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Karyawan</h3>
				</div>
				<div class="card-body">
					<?php if(isset($message)): echo $message; endif; ?>
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4" method="post" action="<?php echo base_url("rosecurity/employee_form"); ?>" enctype="multipart/form-data">
							<div class="form-group row">
								<h2 class="card-title"><strong>A. DATA DIRI</strong></h2>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nomor KTP <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="text" class="form-control" value="<?php echo $id_card; ?>" readonly required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Foto KTP <span class="text-danger"></span></label>
								<div class="custom-file col-sm-7 file-upload">
									<input type="file" class="custom-file-input upload-document" name="document_id_card" accept="image/png, image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_id_card != ''): ?>
										<a href="<?php echo $document_id_card; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">No Pendaftaran </label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="text" class="form-control" value="<?php echo $registration_number; ?>" readonly>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">No Induk Karyawan </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" value="<?php echo $employee_number; ?>" readonly>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">preview </label>
								<div class="col-sm-9">
									<?php if($document_photo != ''): ?>
										<img id="preview" src="<?php echo $document_photo; ?>" width="200px" alt="Foto" />
									<?php else: ?>
										<img id="preview" src="<?php echo asset_img("candidate.png"); ?>" width="200px" alt="Foto" />
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Foto <span class="text-danger"></span></label>
								<div class="col-sm-7 file-upload">
									<input type="file" class="custom-file-input upload-photo upload-document" name="document_photo" accept="image/png, image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile" >Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_photo != ''): ?>
										<a href="<?php echo $document_photo; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Lengkap <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nama Lengkap" name="full_name" value="<?php echo $full_name; ?>" >
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tempat Lahir <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="city_birth_id">
										<option value="">-- PILIH KOTA --</option>
										<?php foreach ($list_city as $item) :  ?>
											<option value="<?php echo $item->id;?>" <?php if($item->id === $city_birth_id): echo "selected"; endif;?>><?php echo $item->name." - ".$item->province_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tanggal Lahir <span class="text-danger"></span></label>
								<div class="col-sm-2">
									<input type="date" name="date_birth" class="form-control datepicker" value="<?php echo $date_birth; ?>">
								</div>
								<div class="col-sm-7"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Alamat Asal <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<textarea class="form-control" name="address_card" rows="3" placeholder="Contoh : Jl. Raya Nginden No.102, RT.001/RW.03, Baratajaya, Kec. Gubeng 60284"><?php echo $address_card; ?></textarea>
									<span class="text-success">Contoh : Jl. Raya Nginden No.102, RT.001/RW.03, Baratajaya, Kec. Gubeng 60284</span>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Kota Asal <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="city_card_id">
										<option value="">-- PILIH KOTA --</option>
										<?php foreach ($list_city as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $city_card_id): echo "selected"; endif;?>><?php echo $item->name." - ".$item->province_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Alamat Domisili <i class="text-xs">(Diisi jika berbeda dengan KTP)</i><span class="text-danger"></span></label>
								<div class="col-sm-8">
									<textarea class="form-control" name="address_domisili" rows="3" placeholder="Contoh : Jl. Raya Nginden No.102, RT.001/RW.03, Baratajaya, Kec. Gubeng 60284"><?php echo $address_domisili; ?></textarea>
									<span class="text-success">Contoh : Jl. Raya Nginden No.102, RT.001/RW.03, Baratajaya, Kec. Gubeng 60284</span>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Kota Domisili <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="city_domisili_id">
										<option value="">-- PILIH KOTA --</option>
										<?php foreach ($list_city as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $city_domisili_id): echo "selected"; endif;?>><?php echo $item->name." - ".$item->province_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Status Tempat Tinggal <span class="text-danger"></span></label>
								<div class="col-sm-8">
									<select class="form-control select2" name="status_residance">
										<option value="milik sendiri" <?php if('milik sendiri' === $status_residance): echo "selected"; endif;?>>Milik Sendiri</option>
										<option value="orang tua" <?php if('orang tua' === $status_residance): echo "selected"; endif;?>>Orang Tua</option>
										<option value="famili" <?php if('famili' === $status_residance): echo "selected"; endif;?>>Famili</option>
										<option value="kontrak" <?php if('kontrak' === $status_residance): echo "selected"; endif;?>>Kontrak</option>
										<option value="kos" <?php if('kos' === $status_residance): echo "selected"; endif;?>>Kos</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nomor Telepon/HP <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" maxlength="14" pattern=".{10,14}"  title="10 to 14 number" class="form-control numeric" laceholder="Nomer Telepon" name="phone_number" value="<?php echo $phone_number; ?>">
									<span class="text-success">Nomor telepon dimulai dengan angka 0, Contoh : 081111111111</span>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Email <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="email" class="form-control" placeholder="Email" name="email" value="<?php echo $email; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Akun Sosial Media <span class="text-danger"></span></label>
								<div class="col-sm-3">
									<input type="text" class="form-control" placeholder="Facebook" name="socmed_fb" value="<?php echo $socmed_fb; ?>">
								</div>
								<div class="col-sm-3">
									<input type="text" class="form-control" placeholder="Instagram" name="socmed_ig" value="<?php echo $socmed_ig; ?>">
								</div>
								<div class="col-sm-3">
									<input type="text" class="form-control" placeholder="Twitter" name="socmed_tw" value="<?php echo $socmed_tw; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Agama <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="religion" required>
										<option value="Islam"  <?php if('Islam' === $religion): echo "selected"; endif;?>>Islam</option>
										<option value="Kristen" <?php if('Kristen' === $religion): echo "selected"; endif;?>>Kristen</option>
										<option value="Katolik" <?php if('Katholik' === $religion): echo "selected"; endif;?>>Katholik</option>
										<option value="Hindu" <?php if('Hindu' === $religion): echo "selected"; endif;?>>Hindu</option>
										<option value="Buddha" <?php if('Buddha' === $religion): echo "selected"; endif;?>>Buddha</option>
										<option value="Konghucu" <?php if('Konghucu' === $religion): echo "selected"; endif;?>>Konghucu</option>
										<option value="Aliran Kepercayaan" <?php if('Aliran Kepercayaan' === $religion): echo "selected"; endif;?>>Aliran Kepercayaan</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Status Pernikahan <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="marital_status" required>
										<option value="belum menikah" <?php if('Belum Kawin' === $marital_status): echo "selected"; endif;?>>Belum Kawin</option>
										<option value="Kawin" <?php if('Kawin' === $marital_status): echo "selected"; endif;?>>Kawin</option>
										<option value="Cerai Hidup" <?php if('Cerai Hidup' === $marital_status): echo "selected"; endif;?>>Cerai Hidup</option>
										<option value="Cerai Mati" <?php if('Cerai Mati' === $marital_status): echo "selected"; endif;?>>Cerai Mati</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Foto Surat Nikah <span class="text-danger"><i class="text-xs "></i></span></label>
								<div class="custom-file col-sm-7 file-upload">
									<input type="file" class="custom-file-input upload-document" name="document_marriage_certificate" accept="image/png, image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_marriage_certificate != ''): ?>
										<a href="<?php echo $document_marriage_certificate; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Jenis Kelamin <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="gender">
										<option value="laki-laki" <?php if('Laki-laki' === $gender): echo "selected"; endif;?>>Laki laki</option>
										<option value="perempuan" <?php if('perempuan' === $gender): echo "selected"; endif;?>>Perempuan</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tinggi & Berat Badan <span class="text-danger"></span></label>
								<div class="col-sm-2">
									<input type="text" class="form-control numeric" placeholder="Tinggi Badan" name="heigh" value="<?php echo $heigh; ?>">
								</div>
								<label class="col-sm-3 col-form-label">CM</label>
								<div class="col-sm-2">
									<input type="text" class="form-control numeric" placeholder="Berat Badan" name="weigh" value="<?php echo $weigh; ?>">
								</div>
								<label class="col-sm-2 col-form-label">KG</label>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Pendidikan Terakhir <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="education_level">
										<option value="SD" <?php if('SD' === $education_level): echo "selected"; endif;?>>SD Sederajat</option>
										<option value="SMP" <?php if('SMP' === $education_level): echo "selected"; endif;?>>SMP Sederajat</option>
										<option value="SMA" <?php if('SMA' === $education_level): echo "selected"; endif;?>>SMA Sederajat</option>
										<option value="D1" <?php if('D1' === $education_level): echo "selected"; endif;?>>D1</option>
										<option value="D2" <?php if('D2' === $education_level): echo "selected"; endif;?>>D2</option>
										<option value="D3" <?php if('D3' === $education_level): echo "selected"; endif;?>>D3</option>
										<option value="D4" <?php if('D4' === $education_level): echo "selected"; endif;?>>D4</option>
										<option value="S1" <?php if('S1' === $education_level): echo "selected"; endif;?>>S1</option>
										<option value="S2" <?php if('S2' === $education_level): echo "selected"; endif;?>>S2</option>
										<option value="S3" <?php if('S3' === $education_level): echo "selected"; endif;?>>S3</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Jurusan </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Mesin/Perkantoran/Perhotelan" name="education_majors" value="<?php echo $education_majors; ?>">
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<h2 class="card-title"><strong>B. ANGGOTA KELUARGA<span class="text-danger"><i class="text-xs "></i></span></strong></h2>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Foto Kartu Keluarga</label>
								<div class="custom-file col-sm-7 file-upload">
									<input type="file" class="custom-file-input upload-document" name="document_family_card" accept="image/png, image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_family_card != ''): ?>
										<a href="<?php echo $document_family_card; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Ibu Kandung</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nama Ibu Kandung" name="family_mother" value="<?php echo $family_mother; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Pasangan </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nama Suami/Istri" name="family_mate" value="<?php echo $family_mate; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Anak </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nama Anak" name="family_child" value="<?php echo $family_child; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Kode PTKP </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="ptkp">
										<option value=""> </option>
										<?php foreach ($list_tax as $item) :  ?>
										<option value="<?php echo $item->code;?>" <?php if($item->code === $ptkp): echo "selected"; endif;?>><?php echo $item->code." - ".$item->description; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<h2 class="card-title"><strong>C. BENEFIT</strong></h2>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Program BPJS <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<?php $labor = explode(",", $benefit_labor_note); ?>
									<select multiple class="form-control select2" name="benefit_labor_choice[]" placeholder="Program BPJS  Ketenagakerjaan">
										<option value="">&nbsp;</option>
										<option value="JKK" <?php if(in_array('JKK', $labor)): echo "selected"; endif;?>>JKK</option>
										<option value="JKM" <?php if(in_array('JKM', $labor)): echo "selected"; endif;?>>JKM</option>
										<option value="JHT" <?php if(in_array('JHT', $labor)): echo "selected"; endif;?>>JHT</option>
										<option value="JP" <?php if(in_array('JP', $labor)): echo "selected"; endif;?>>JP</option>
										
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">BPJS Ketenagakerjaan</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nomor BPJS  Ketenagakerjaan" name="benefit_labor" value="<?php echo $benefit_labor; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Layanan Asuransi Kesehatan </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="BPJS Kesehatan" name="benefit_health_note" value="<?php echo $benefit_health_note; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nomor Asuransi Kesehatan </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nomor Asuransi Kesehatan" name="benefit_health" value="<?php echo $benefit_health; ?>">
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<h2 class="card-title"><strong>D. INFORMASI TAMBAHAN</strong></h2>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Bank</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nomor Bank" name="bank_name" value="<?php echo $bank_name; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nomor Rekening</label>
								<div class="col-sm-9">
									<input type="text" class="form-control numeric" placeholder="Nomor Rekening" name="bank_account" value="<?php echo $bank_account; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Atas Nama Rekening</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Atas Nama Rekening" name="bank_account_name" value="<?php echo $bank_account_name; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nomor NPWP</label>
								<div class="col-sm-9">
									<input type="text" class="form-control numeric" placeholder="Nomor NPWP" name="tax_number" value="<?php echo $tax_number; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Foto NPWP <span class="text-danger"><i class="text-xs "></i></span></label>
								<div class="custom-file col-sm-7 file-upload">
									<input type="file" class="custom-file-input upload-document" name="document_tax_number" accept="image/png, image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_tax_number != ''): ?>
										<a href="<?php echo $document_tax_number; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<hr>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" name="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
	$(document).ready(function () {
		$('.upload-document').imageUploadResizer({
			max_width: 1200, // Defaults 1000
			max_height: 800, // Defaults 1000
			quality: 1, // Defaults 1
			do_not_resize: ['gif', 'svg'], // Defaults []
		});
	});
</script>
<style type="text/css">
	input[type="text" i], input[type="email" i], input[type="select" i], input[type="date" i], .select2-container, textarea {
    	margin-left: -7px;
	}
</style>
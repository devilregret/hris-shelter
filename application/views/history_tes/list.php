<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('vacancy/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<style>
			#data-table tr th {
				text-align:center;
			};
		</style>
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar History Tes</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<button class="btn btn-success btn-sm" id="download-excel"><i class="fas fa-file-excel"></i> &nbsp; Download Rekap Excel</button>&nbsp;
						<button class="btn btn-success btn-sm" id="download-hasil"><i class="fas fa-file-pdf"></i> &nbsp; Download Hasil</button>&nbsp;
					</div>
				</div>
				<div class="card-body">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th id='id'>ID</th>
									<th id='no'>No.</th>
									<th id='tes'>Tes</th>
									<th id='nama'>Nama</th>
									<th id='usia'>Usia</th>
									<th id='jk'>JK</th>
									<th id='tgl_tes'>Tgl Kerjakan</th>
									<th id='kategori'>Kategori</th>
									<th id='judul_lowongan'>Lowongan</th>
									<th id='position_name'>Posisi</th>
									<th id='site_name'>Site Bisnis</th>
									<th id='company_name'>Bisnis Unit</th>
									<th id='branch_name'>Branch</th>
									<th id='province_name'>Provinsi</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<div class="modal fade" id="modalDownloadHasil" aria-labelledby="modalDownloadHasil" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-titles">Download Hasil</h5>
        <button data-bs-dismiss="modal" aria-label="Close" class="close-modal btn btn-secondary"><i class="fas fa-times"></i></button>
      </div>
      <div class="modal-body">
		<div class="form-group">
			<label>Jenis Tes</label>
			<select multiple data-allow-clear="1" id="select-tes" class="select2">
				<option value="psikotes" selected>DISC</option>
				<option value="deret_angka" selected>Deret Angka</option>
				<option value="hitung_cepat" selected>Hitung Cepat</option>
				<option value="ketelitian" selected>Ketelitian</option>
				<option value="kraepelin" selected>Kraepelin</option>
			</select>
		</div>
		<div class="form-group">
			<label>Bisnis Unit</label>
			<select id="select-company" class="select2">
				<option value="all" selected>Semua Data</option>
				<?php foreach ($list_company as $item) :  ?>
				<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label>Site	Bisnis</label>
			<select id="select-site" class="select2">
				<option value="all" selected>Semua Data</option>
				<?php foreach ($list_site as $item) :  ?>
				<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label>Jabatan</label>
			<select id="select-position" class="select2">
				<option value="all" selected>Semua Data</option>
				<?php foreach ($list_position as $item) :  ?>
				<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label>Provinsi</label>
			<select id="select-province" class="select2">
				<option value="all" selected>Semua Data</option>
				<?php foreach ($list_province as $item) :  ?>
				<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label>Judul Lowongan</label>
			<select id="select-lowongan" class="select2">
				<option value="all" selected>Semua Data</option>
				<?php foreach ($list_lowongan as $item) :  ?>
				<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label>Peserta</label>
			<select id="select-employee" class="select2">
				<option value="all" selected>Semua Data</option>
				<?php foreach ($list_employee as $item) :  ?>
				<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label>Urutkan Berdasarkan</label>
			<select id="select-order" class="select2">
				<option value="peserta" selected>Peserta</option>
				<option value="tes">Tes</option>
			</select>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary close-modal" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn-download-hasil">Download</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalRekapExcel" aria-labelledby="modalRekapExcel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-titles">Download Rekap Excel</h5>
        <button data-bs-dismiss="modal" aria-label="Close" class="close-excel btn btn-secondary"><i class="fas fa-times"></i></button>
      </div>
      <div class="modal-body">
		<div class="form-group">
			<label>Jenis Tes</label>
			<select multiple data-allow-clear="1" id="excel-tes" class="select2">
				<option value="psikotes" selected>DISC</option>
				<option value="deret_angka" selected>Deret Angka</option>
				<option value="hitung_cepat" selected>Hitung Cepat</option>
				<option value="ketelitian" selected>Ketelitian</option>
				<option value="kraepelin" selected>Kraepelin</option>
			</select>
		</div>
		<div class="form-group">
			<label>Bisnis Unit</label>
			<select id="excel-company" class="select2">
				<option value="all" selected>Semua Data</option>
				<?php foreach ($list_company as $item) :  ?>
				<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label>Site	Bisnis</label>
			<select id="excel-site" class="select2">
				<option value="all" selected>Semua Data</option>
				<?php foreach ($list_site as $item) :  ?>
				<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label>Jabatan</label>
			<select id="excel-position" class="select2">
				<option value="all" selected>Semua Data</option>
				<?php foreach ($list_position as $item) :  ?>
				<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label>Provinsi</label>
			<select id="excel-province" class="select2">
				<option value="all" selected>Semua Data</option>
				<?php foreach ($list_province as $item) :  ?>
				<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label>Judul Lowongan</label>
			<select id="excel-lowongan" class="select2">
				<option value="all" selected>Semua Data</option>
				<?php foreach ($list_lowongan as $item) :  ?>
				<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label>Peserta</label>
			<select id="excel-employee" class="select2">
				<option value="all" selected>Semua Data</option>
				<?php foreach ($list_employee as $item) :  ?>
				<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label>Urutkan Berdasarkan</label>
			<select id="excel-order" class="select2">
				<option value="peserta" selected>Peserta</option>
				<option value="tanggal">Tanggal</option>
				<option value="tes">Tes</option>
			</select>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary close-excel" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn-download-excel">Download</button>
      </div>
    </div>
  </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
	$(document).ready(function(){	
		$('#download-hasil').click(function(){
			$('#modalDownloadHasil').modal('show');
		})
		$('.close-modal').click(function(){
			$('#modalDownloadHasil').modal('hide');
		})

		$('#download-excel').click(function(){
			$('#modalRekapExcel').modal('show');
		})
		$('.close-excel').click(function(){
			$('#modalRekapExcel').modal('hide');
		})

		$('.select2').select2();

		$('#btn-download-excel').click(function(){
			let vCompany = $('#excel-company').val();
			let vSite = $('#excel-site').val();
			let vPosition = $('#excel-position').val();
			let vProvince = $('#excel-province').val();
			let vLowongan = $('#excel-lowongan').val();
			let vEmployee = $('#excel-employee').val();
			let vOrder = $('#excel-order').val();
			let vTes = $('#excel-tes').val();

			if(vTes ==""){
				Swal.fire({
					icon: 'warning',
					title: 'Pemberitahuan',
					html: "Belum memilih Tes",
				})
				return;
			}
			Swal.fire({
				icon:'question',
				title: 'Apakah anda ingin mendownload rekap tes dalam bentuk excel ?',
				confirmButtonText: 'Download',
				showDenyButton: true,
				denyButtonText: `Batal`,
			}).then((result) => {
				if (result.isConfirmed) {
					$('#modalRekapExcel').modal('hide');
					let url = "export?tes="+vTes+"&company_id="+vCompany+"&site_id="+vSite+"&position_id="+vPosition+"&province_id="+vProvince+"&vacancy_id="+vLowongan+"&employee_id="+vEmployee+"&order="+vOrder;
					window.location.href= url;
				}else{
					$('#modalRekapExcel').modal('hide');
				}
			})
		});

		$('#btn-download-hasil').click(function(){
			let vCompany = $('#select-company').val();
			let vSite = $('#select-site').val();
			let vPosition = $('#select-position').val();
			let vProvince = $('#select-province').val();
			let vLowongan = $('#select-lowongan').val();
			let vEmployee = $('#select-employee').val();
			let vOrder = $('#select-order').val();
			let vTes = $('#select-tes').val();

			if(vTes ==""){
				Swal.fire({
					icon: 'warning',
					title: 'Pemberitahuan',
					html: "Belum memilih Tes",
				})
				return;
			}

			let url = "result?tes="+vTes+"&company_id="+vCompany+"&site_id="+vSite+"&position_id="+vPosition+"&province_id="+vProvince+"&vacancy_id="+vLowongan+"&employee_id="+vEmployee+"&order="+vOrder;
			Swal.fire({
				icon:'question',
				title: 'Apakah anda ingin melihat hasil lengkap ?',
				showDenyButton: true,
				confirmButtonText: 'Soal + Hasil',
				denyButtonText: `Hanya Hasil`,
			}).then((result) => {
				if (result.isConfirmed) {
					url = url+"&is_hanya_hasil=0";
				}else if(result.isDenied){
					url = url+"&is_hanya_hasil=1";
				}
						
			const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
			const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;
			const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
			const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

			const systemZoom = width / window.screen.availWidth;
			const left = (width - 800) / 2 / systemZoom + dualScreenLeft
			const top = (height - 600) / 2 / systemZoom + dualScreenTop
			const newWindow = window.open(url, 'preview', 
						`
						scrollbars=yes,
						width=${800 / systemZoom}, 
						height=${600 / systemZoom}, 
						top=${top}, 
						left=${left}
						`)
			})
			$('#modalDownloadHasil').modal('hide');

		})

		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("history_tes/list_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'searchable':false ,visible:false},
				{ data: 'no', 'searchable':false },
				{ data: 'stes' },	
				{ data: 'nama' },
				{ data: 'usia' ,'className':'text-center', 'orderable':false, 'searchable':false },
				{ data: 'jk','className':'text-center', 'orderable':false, 'searchable':false  },
				{ data: 'tgl_tes' },
				{ data: 'kategori' },
				{ data: 'title' },
				{ data: 'position_name' },
				{ data: 'site_name' },
				{ data: 'company_name' },
				{ data: 'branch_name' },
				{ data: 'province_name' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[0, 'desc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index==1||index==2||index==5||index==6||index==7||index==8||index==9||index==10||index==11||index==12){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
	});

function preview_chart(el)
{
    const url = $(el).attr('data-href');
    const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
    const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

    const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    const systemZoom = width / window.screen.availWidth;
    const left = (width - 1000) / 2 / systemZoom + dualScreenLeft
    const top = (height - 600) / 2 / systemZoom + dualScreenTop
    const newWindow = window.open(url, 'preview', 
      `
      scrollbars=yes,
      width=${1000 / systemZoom}, 
      height=${600 / systemZoom}, 
      top=${top}, 
      left=${left}
      `
    )
    // if (window.focus) new Window.focus();
}

function previewHasil(el){
		let url = $(el).attr('data-href');
		Swal.fire({
			icon:'question',
			title: 'Apakah anda ingin melihat hasil lengkap ?',
			showDenyButton: true,
			confirmButtonText: 'Soal + Hasil',
			denyButtonText: `Hanya Hasil`,
		}).then((result) => {
			if (result.isConfirmed) {
				url = url+"&is_hanya_hasil=0";
			}else if(result.isDenied){
				url = url+"&is_hanya_hasil=1";
			}
	                
		const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
		const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;
		const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

		const systemZoom = width / window.screen.availWidth;
		const left = (width - 800) / 2 / systemZoom + dualScreenLeft
		const top = (height - 600) / 2 / systemZoom + dualScreenTop
		const newWindow = window.open(url, 'preview', 
	                `
	                scrollbars=yes,
	                width=${800 / systemZoom}, 
	                height=${600 / systemZoom}, 
	                top=${top}, 
	                left=${left}
	                `)
		})
	}
</script>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/css/adminlte.min.css"); ?>
		<?php echo asset_js("plugins/jquery/jquery.min.js"); ?>	
        <?php echo asset_js("plugins/chart.js/Chart.min.js"); ?>
        <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
        <style>
            /* STYLE PSIKOTES */
            .maintable { 
                border-spacing: 5px;
                border-collapse: separate;
            }
            .color-title {
                background-color:#341f97;
                -webkit-print-color-adjust:exact;
            }
            .color-sub-title {
                background-color:#2e86de;
                -webkit-print-color-adjust:exact;
            }
            .color-content {
                background-color:#48dbfb;
                -webkit-print-color-adjust:exact;
            }
            .color-detail {
                background-color:#c8d6e5;
                -webkit-print-color-adjust:exact;
            }
            .color-canvas {
                background-color:white;
                -webkit-print-color-adjust:exact;
            }
            .content-gk {
                text-align:center;
                width:33.33333%;
                font-size:12pt;
            }
            .content-gk-detail {
                color:#341f97;
                font-size:12pt
            }
            .content-detail {
                text-align:center;
                font-size:14pt;
                vertical-align:top;
            }

            /* END OF STYLE PSIKOTES */

            /* DERET ANGKA STYLE  */
            .tabel-soal {
                border-spacing: 10px;
                border-collapse: separate;
            }
            .jawaban-terpilih {
                border:2px solid black;
                padding:5px;
                border-radius:30px
            }
            .color-red {
                background-color:red;
                -webkit-print-color-adjust:exact;
            }
            .tabel-simpulan {
                border-spacing: 30px;
            }
            .tabel-simpulan tr td {
                border:1px solid black;
                padding-top:10px;
                padding-bottom:10px;
            }
            .jawaban-tidak-diisi {
                background-color:#ff6b6b;
                -webkit-print-color-adjust:exact;
            }
            .jawaban-salah {
                background-color:#feca57;
                -webkit-print-color-adjust:exact;
            }
            .jawaban-benar {
                background-color:#1dd1a1;
                -webkit-print-color-adjust:exact;
            }
            /* END OF  DERET ANGKA STYLE */
            
            /* HITUNG CEPAT STYLE

            END HITUNG CEPAT STYLE/ */

            @media print {
                .pagebreak { page-break-before: always; } /* page-break-after works, as well */
            }
        </style>
	</head>
	<body>
        <div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="printPsikotes();">Print</button>
			<button type="button" class="btn btn-default" onclick="window.close();">Tutup</button>
		</div>
		<div class="wrapper">
            <?php foreach ($list_data as $i => $data) { ?>
                <?php if($data->tes=='psikotes') {
                    $psi = $data->result;
                ?>
                    <div class="pagebreak"> </div>
                    <section class="psikotes-print">
                        <div class="row">
                            <table style="width:100%" class="maintable">
                                <tr class="color-title">
                                    <td colspan="3">
                                        <h4 style="color:white;text-align:center">Personality System Graph Page</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="color-content">
                                        <table style="width:100%;font-size:16pt">
                                            <tr>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;Name : <?php echo $psi->psikotes[0]->nama ?></td>
                                                <td>Gender : <?php echo $psi->psikotes[0]->jk ?></td>
                                                <td>Age : <?php echo $psi->psikotes[0]->usia ?> Tahun</td>
                                                <td>Date : <?php echo $psi->psikotes[0]->tgl_tes ?></td>
                                            </tr>
                                            <tr>
                                                
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="color-sub-title">
                                    <td colspan="3">
                                        <h4 style="color:white;text-align:center">Gambaran Karakter</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="color-content content-gk">
                                        Most
                                        <br>
                                        <u><b>Mask Public Self (Luar)</b></u>
                                        <br>
                                        Kepribadian Dimuka Umum
                                        <br>
                                        <i class="content-gk-detail">Gambaran bagaimana orang lain melihat diri kita. Persepsi tentang kita dilihat dari sisi luar.</i>
                                    </td>
                                    <td class="color-content content-gk">
                                        Least
                                        <br>
                                        <u><b>Core Private Self (Dalam)</b></u>
                                        <br>
                                        Kepribadian Saat Mendapat Tekanan
                                        <br>
                                        <i class="content-gk-detail">Gambaran utuh tentang diri inti kita sesungguhnya.</i>
                                    </td>
                                    <td class="color-content content-gk">
                                        Change
                                        <br>
                                        <u><b>Mirror Perceived Self</b></u>
                                        <br>
                                        Kepribadian Asli Yang Tersembunyi
                                        <br>
                                        <i class="content-gk-detail">Gambaran bagaimana kita melihat diri kita sendiri.</i>
                                    </td>
                                </tr>
                                <tr>
                                    <?php foreach ($psi->psikotes_result as $key => $element) { ?>
                                    <td class="color-content">
                                        <h5 style="color:#341f97;text-align:center">
                                            <i><?php echo $element->kepribadian ?></i>
                                        </h5>
                                    </td>
                                    <?php } ?>
                                    
                                </tr>
                                <tr>
                                    <?php foreach ($psi->psikotes_result as $key => $element) { ?>
                                        <td class="color-detail content-detail">
                                            <?php echo $element->kepribadian_detail ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <?php foreach ($psi->psikotes_result_line as $key => $element) { ?>
                                        <td>
                                            <canvas id="chart-<?php echo $i ?>-<?php echo $element->pk ?>" height="250" class="color-canvas"></canvas>
                                        </td>

                                        <script>
                                            var ctx = document.getElementById("chart-<?php echo $i ?>-<?php echo $element->pk ?>");
                                            var min = -4;
                                            var max = 8;
                                            var label = "";
                                            if ("<?php echo $element->pk ?>" =="P") {
                                                label = "Most ( Mask Public Self )";
                                            }else if ("<?php echo $element->pk ?>" =="K") {
                                                min = -8;
                                                max = 6;
                                                label = "Least ( Core Private Self )";
                                            }else if ("<?php echo $element->pk ?>" =="PK") {
                                                label = "Change ( Mirror Percelved Self )";
                                            }
                                            var myChart = new Chart(ctx, {
                                                type: 'line',
                                                lineTension:0, //straight lines
                                                data: {
                                                    labels: ['D', 'I', 'S', 'C'],
                                                    datasets: [{
                                                        label: label,
                                                        lineTension:0, //straight lines
                                                        fill: false,
                                                        borderColor: ['#2e86de'],
                                                        data: [<?php echo $element->convert_d.",".$element->convert_i.",".$element->convert_s.",".$element->convert_s ?>],
                                                    }]
                                                },
                                                options: {
                                                    plugins: {
                                                        // Change options for ALL labels of THIS CHART
                                                        datalabels: {
                                                            color: '#000000',
                                                        }
                                                    },
                                                    scales: {
                                                        yAxes: [{
                                                            display: true,
                                                            ticks: {
                                                                stepSize: 0.5,
                                                                max: max,
                                                                min:min
                                                            },
                                                            scaleLabel: {
                                                                display:true
                                                            }
                                                        }]
                                                    },
                                                }
                                            });
                                        </script>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table style="width:100%">
                                            <tr class="color-sub-title">
                                                <td colspan="3" style="padding:5px">
                                                    <h4 style="color:white;text-align:center">Deskripsi Kepribadian</h4>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="color-detail content-detail" style="padding-top:10px;padding-bottom:10px">
                                                    <?php echo $psi->psikotes[0]->deskripsi_kepribadian ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table style="width:100%">
                                            <tr class="color-sub-title">
                                                <td colspan="3" style="padding:5px">
                                                    <h4 style="color:white;text-align:center">Job Match</h4>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="color-detail content-detail" style="padding-top:10px;padding-bottom:10px">
                                                    <?php echo $psi->psikotes[0]->job_match ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <br>
                        </div>
                    </section>
                <?php } else if($data->tes=='deret_angka') {
                    $der = $data->result;
                ?>
                    <div class="pagebreak"> </div>
                    <section class="deret-angka-print">
                        <br>
                        <div class="row">
                            <table style="width:100%" class="maintable">
                                <tr class="color-title">
                                    <td colspan="3" style="padding:10px">
                                        <h2 style="color:white;text-align:center">TES DERET ANGKA</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="color-content">
                                        <table style="width:100%">
                                            <tr>
                                                <td style="width:10%">Name</td>
                                                <td style="width:3%">:</td>
                                                <td style="width:87%"><?php echo $der->deret_angka[0]->nama ?></td>
                                            </tr>
                                            <tr>
                                                <td>Age</td>
                                                <td>:</td>
                                                <td><?php echo $der->deret_angka[0]->usia ?> Tahun</td>
                                            </tr>
                                            <tr>
                                                <td>Gender</td>
                                                <td>:</td>
                                                <td><?php echo $der->deret_angka[0]->jk ?></td>
                                            </tr>
                                            <tr>
                                                <td>Date</td>
                                                <td>:</td>
                                                <td><?php echo $der->deret_angka[0]->tgl_tes ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <?php if($is_hanya_hasil==0) { ?>
                                <tr class="color-sub-title">
                                    <td colspan="3" style="padding:5px">
                                        <h2 style="color:white;text-align:center">Soal</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="tabel-soal" style="width:100%">
                                            <?php foreach ($der->deret_angka_d as $key => $value) { ?>
                                                <?php
                                                $label = 'jawaban-tidak-diisi';
                                                
                                                if($value->m_deret_angka_jawaban_id!=null){
                                                    if($value->is_benar ==1){
                                                        $label = 'jawaban-benar';
                                                    }else{
                                                        $label = 'jawaban-salah';
                                                    };
                                                };
                                                ?>
                                                <tr>
                                                    <td style="width:2%" class="<?php echo $label; ?>">&nbsp;</td>
                                                    <td>[<?php echo $value->no_soal ?>] </td>
                                                    <?php 
                                                        $jmlKol = 0; 
                                                        foreach ($der->deret_angka_list_deret as $keyd => $valued) {
                                                            if($valued->m_deret_angka_id == $value->m_deret_angka_id){
                                                                $jmlKol++;
                                                    ?>
                                                        <td><?php echo $valued->deret ?></td>
                                                    <?php
                                                            }
                                                        }
                                                    ?>
                                                    <?php for ($i=0; $i < 9-$jmlKol ; $i++) { ?> 
                                                        <td></td>
                                                    <?php } ?>
                                                    
                                                    <?php 
                                                        foreach ($der->deret_angka_list_jawaban as $keyj => $valuej) { 
                                                            if($valuej->m_deret_angka_id == $value->m_deret_angka_id){
                                                    ?>
                                                        <td>
                                                            <?php
                                                                if ($value->m_deret_angka_jawaban_id==$valuej->id) {
                                                                    echo '<span class="jawaban-terpilih">'.$valuej->alpabet.'.</span> '.$valuej->jawaban;	
                                                                }else{
                                                                    echo $valuej->alpabet.". ".$valuej->jawaban;
                                                                };
                                                            ?>
                                                        </td>
                                                    <?php ;}; } ?>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    </td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td>
                                        <table class="tabel-simpulan" style="width:100%;text-align:center">
                                            <tr class="color-sub-title" style="color:white;font-size:16px;font-weight:bold">
                                                <td>Betul</td>
                                                <td>Salah</td>
                                                <td>Tidak Diisi</td>
                                                <td>%</td>
                                                <td>Petugas</td>
                                                <td>Kategori</td>
                                            </tr>
                                            <tr>
                                                <td><?php echo $der->deret_angka[0]->jawaban_benar ?></td>
                                                <td><?php echo $der->deret_angka[0]->jawaban_salah ?></td>
                                                <td><?php echo $der->deret_angka[0]->tidak_diisi ?></td>
                                                <td><?php echo $der->deret_angka[0]->persentase ?> %</td>
                                                <td><?php echo $petugas ?></td>
                                                <td><?php echo $der->deret_angka[0]->kategori ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <?php if($is_hanya_hasil==0) { ?>
                                <tr>
                                    <td>
                                        <table style="width:100%">
                                            <tr>
                                                <td colspan="2">Note :</td>
                                            </tr>
                                            <tr>
                                                <td class="jawaban-tidak-diisi" style="width:20%;">&nbsp;</td>
                                                <td>: Jawaban tidak diisi</td>
                                            </tr>
                                            <tr>
                                            <td class ="jawaban-salah" style="width:20%;">&nbsp;</td>
                                                <td>: Jawaban salah</td>
                                            </tr>
                                            <tr>
                                            <td class="jawaban-benar" style="width:20%;">&nbsp;</td>
                                                <td>: Jawaban benar</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <?php } ?>
                            </table>
                            <br>
                        </div>
                    </section>
                <?php } else if($data->tes=='hitung_cepat') {
                    $hit = $data->result;
                ?>
                    <div class="pagebreak"> </div>
                    <section class="hitung-cepat-print">
                        <br>
                        <div class="row">
                            <table style="width:100%" class="maintable">
                                <tr class="color-title">
                                    <td colspan="3" style="padding:10px">
                                        <h2 style="color:white;text-align:center">TES HITUNG CEPAT</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="color-content">
                                        <table style="width:100%">
                                            <tr>
                                                <td style="width:10%">Name</td>
                                                <td style="width:3%">:</td>
                                                <td style="width:87%"><?php echo $hit->hitung_cepat[0]->nama ?></td>
                                            </tr>
                                            <tr>
                                                <td>Age</td>
                                                <td>:</td>
                                                <td><?php echo $hit->hitung_cepat[0]->usia ?> Tahun</td>
                                            </tr>
                                            <tr>
                                                <td>Gender</td>
                                                <td>:</td>
                                                <td><?php echo $hit->hitung_cepat[0]->jk ?></td>
                                            </tr>
                                            <tr>
                                                <td>Date</td>
                                                <td>:</td>
                                                <td><?php echo $hit->hitung_cepat[0]->tgl_tes ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <?php if($is_hanya_hasil==0) { ?>
                                <tr class="color-sub-title">
                                    <td colspan="3" style="padding:5px">
                                        <h2 style="color:white;text-align:center">Soal</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="tabel-soal" style="width:100%">
                                            <?php foreach ($hit->hitung_cepat_d as $key => $value) { ?>
                                                <?php
                                                $label = 'jawaban-tidak-diisi';
                                                
                                                if($value->m_hitung_cepat_jawaban_id!=null){
                                                    if($value->is_benar ==1){
                                                        $label = 'jawaban-benar';
                                                    }else{
                                                        $label = 'jawaban-salah';
                                                    };
                                                };
                                                ?>
                                                <tr>
                                                    <td style="width:2%" class="<?php echo $label; ?>">&nbsp;</td>
                                                    <td>[<?php echo $value->no_soal ?>] </td>
                                                    <td><?php echo $value->soal ?></td>
                                                    <?php 
                                                        foreach ($hit->hitung_cepat_list_jawaban as $keyj => $valuej) { 
                                                            if($valuej->m_hitung_cepat_id == $value->m_hitung_cepat_id){
                                                    ?>
                                                        <td>
                                                            <?php
                                                                if ($value->m_hitung_cepat_jawaban_id==$valuej->id) {
                                                                    echo '<span class="jawaban-terpilih">'.$valuej->alpabet.'.</span> '.$valuej->jawaban;	
                                                                }else{
                                                                    echo $valuej->alpabet.". ".$valuej->jawaban;
                                                                };
                                                            ?>
                                                        </td>
                                                    <?php ;}; } ?>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    </td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td>
                                        <table class="tabel-simpulan" style="width:100%;text-align:center">
                                            <tr class="color-sub-title" style="color:white;font-size:16px;font-weight:bold">
                                                <td>Betul</td>
                                                <td>Salah</td>
                                                <td>Tidak Diisi</td>
                                                <td>%</td>
                                                <td>Petugas</td>
                                                <td>Kategori</td>
                                            </tr>
                                            <tr>
                                                <td><?php echo $hit->hitung_cepat[0]->jawaban_benar ?></td>
                                                <td><?php echo $hit->hitung_cepat[0]->jawaban_salah ?></td>
                                                <td><?php echo $hit->hitung_cepat[0]->tidak_diisi ?></td>
                                                <td><?php echo $hit->hitung_cepat[0]->persentase ?> %</td>
                                                <td><?php echo $petugas ?></td>
                                                <td><?php echo $hit->hitung_cepat[0]->kategori ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <?php if($is_hanya_hasil==0) { ?>
                                <tr>
                                    <td>
                                        <table style="width:100%">
                                            <tr>
                                                <td colspan="2">Note :</td>
                                            </tr>
                                            <tr>
                                                <td class="jawaban-tidak-diisi" style="width:20%;">&nbsp;</td>
                                                <td>: Jawaban tidak diisi</td>
                                            </tr>
                                            <tr>
                                            <td class ="jawaban-salah" style="width:20%;">&nbsp;</td>
                                                <td>: Jawaban salah</td>
                                            </tr>
                                            <tr>
                                            <td class="jawaban-benar" style="width:20%;">&nbsp;</td>
                                                <td>: Jawaban benar</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <?php } ?>
                            </table>
                            <br>
                        </div>
                    </section>
                <?php } else if($data->tes=='ketelitian') {
                    $ket = $data->result;
                ?>
                    <div class="pagebreak"> </div>
                    <section class="ketelitian-print">
                        <br>
                        <table style="width:100%" class="maintable">
                            <tr class="color-title">
                                <td colspan="5" style="padding:10px">
                                    <h2 style="color:white;text-align:center">TES KETELITIAN</h2>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" class="color-content">
                                    <table style="width:100%">
                                        <tr>
                                            <td style="width:10%">Name</td>
                                            <td style="width:3%">:</td>
                                            <td style="width:40%"><?php echo $ket->ketelitian[0]->nama ?></td>
                                            <td>Gender</td>
                                            <td>:</td>
                                            <td><?php echo $ket->ketelitian[0]->jk ?></td>
                                        </tr>
                                        <tr>
                                            <td>Age</td>
                                            <td>:</td>
                                            <td><?php echo $ket->ketelitian[0]->usia ?> Tahun</td>
                                            <td>Date</td>
                                            <td>:</td>
                                            <td><?php echo $ket->ketelitian[0]->tgl_tes ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <?php if($is_hanya_hasil==0) { ?>
                            <tr class="color-sub-title">
                                <td colspan="5" style="padding:5px">
                                    <h2 style="color:white;text-align:center">Soal</h2>
                                </td>
                            </tr>
                            <?php foreach ($ket->ketelitian_d as $key => $value) { 
                                $label = 'jawaban-tidak-diisi';
                                
                                if($value->jawaban!=null){
                                    if($value->is_benar ==1){
                                        $label = 'jawaban-benar';
                                    }else{
                                        $label = 'jawaban-salah';
                                    };
                                };
                                ?>
                                <tr>
                                    <td style="width:2%" class="<?php echo $label; ?>">&nbsp;</td>
                                    <td style="width:5%">[<?php echo $value->no_soal ?>] </td>
                                    <td style="width:30%"><?php echo $value->soal ?></td>
                                    <td style="width:10%;text-align:center">
                                        <?php
                                            if ($value->jawaban=="S") {
                                                echo '<span class="jawaban-terpilih">Sama</span> ';	
                                            }else{
                                                echo 'Sama';
                                            };
                                        ?>
                                    </td>
                                    <td style="width:10%;text-align:center">
                                        <?php
                                            if ($value->jawaban=="T") {
                                                echo '<span class="jawaban-terpilih">Tidak Sama</span> ';	
                                            }else{
                                                echo 'Tidak Sama';
                                            };
                                        ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <?php } ?>
                            <tr>
                                <td colspan="5">
                                    <table class="tabel-simpulan" style="width:100%;text-align:center">
                                        <tr class="color-sub-title" style="color:white;font-size:16px;font-weight:bold">
                                            <td>Betul</td>
                                            <td>Salah</td>
                                            <td>Tidak Diisi</td>
                                            <td>%</td>
                                            <td>Petugas</td>
                                            <td>Kategori</td>
                                        </tr>
                                        <tr>
                                            <td><?php echo $ket->ketelitian[0]->jawaban_benar ?></td>
                                            <td><?php echo $ket->ketelitian[0]->jawaban_salah ?></td>
                                            <td><?php echo $ket->ketelitian[0]->tidak_diisi ?></td>
                                            <td><?php echo $ket->ketelitian[0]->persentase ?> %</td>
                                            <td><?php echo $petugas ?></td>
                                            <td><?php echo $ket->ketelitian[0]->kategori ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <?php if($is_hanya_hasil==0) { ?>
                            <tr>
                                <td colspan="5">
                                    <table style="width:100%">
                                        <tr>
                                            <td colspan="2">Note :</td>
                                        </tr>
                                        <tr>
                                            <td class="jawaban-tidak-diisi" style="width:20%;">&nbsp;</td>
                                            <td>: Jawaban tidak diisi</td>
                                        </tr>
                                        <tr>
                                        <td class ="jawaban-salah" style="width:20%;">&nbsp;</td>
                                            <td>: Jawaban salah</td>
                                        </tr>
                                        <tr>
                                        <td class="jawaban-benar" style="width:20%;">&nbsp;</td>
                                            <td>: Jawaban benar</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <?php } ?>
                        </table>
                        <br>
                    </section>
                <?php } else if($data->tes=='kraepelin') {
                    $kra = $data->result;
                ?>
                    <div class="pagebreak"> </div>
                    <section class="kraepelin-print">
                        <br>
                        <div class="row">
                            <table style="width:100%" class="maintable">
                                <tr class="color-title">
                                    <td colspan="3" style="padding:10px">
                                        <h2 style="color:white;text-align:center">TES KRAEPELIN</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="color-content">
                                        <table style="width:100%">
                                            <tr>
                                                <td style="width:10%">Name</td>
                                                <td style="width:3%">:</td>
                                                <td style="width:87%"><?php echo $kra->kraepelin[0]->nama ?></td>
                                            </tr>
                                            <tr>
                                                <td>Age</td>
                                                <td>:</td>
                                                <td><?php echo $kra->kraepelin[0]->usia ?> Tahun</td>
                                            </tr>
                                            <tr>
                                                <td>Gender</td>
                                                <td>:</td>
                                                <td><?php echo $kra->kraepelin[0]->jk ?></td>
                                            </tr>
                                            <tr>
                                                <td>Date</td>
                                                <td>:</td>
                                                <td><?php echo $kra->kraepelin[0]->tgl_tes ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <?php if($is_hanya_hasil==0) { ?>
                                <tr class="color-sub-title">
                                    <td colspan="3" style="padding:5px">
                                        <h2 style="color:white;text-align:center">Soal</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="tabel-soal" style="width:100%">
                                        <?php for ($b=0; $b < $kra->kraepelin[0]->jml_baris; $b++) { ?>
                                            <tr>
                                                <?php for ($k=0; $k < $kra->kraepelin[0]->jml_kolom; $k++) { 
                                                    foreach ($kra->kraepelin_d as $key => $value) {
                                                        if($value->baris==$b&&$value->kolom==$k){
                                                ?>
                                                <td style="width:2%" class="" colspan="2"><?php echo $value->angka_1 ?></td>
                                                <?php } } } ?>
                                                
                                            </tr>
                                            <tr>
                                                <?php for ($k=0; $k < $kra->kraepelin[0]->jml_kolom; $k++) { 
                                                    foreach ($kra->kraepelin_d as $key => $value) {
                                                        if($value->baris==$b&&$value->kolom==$k){
                                                ?>
                                                <td></td>
                                                <td style="width:2%;font-weight:bold;padding-left:5px;padding-right:15px;color:<?php if($value->is_benar==1) {echo "green";} else{echo "red";} ?>"><?php echo $value->jawaban ?></td>
                                                <?php } } } ?>
                                            </tr>
                                        <?php } ?>
                                        </table>
                                    </td>
                                </tr>
                                <?php } ?>
                                <tr class="color-sub-title">
                                    <td colspan="3" style="padding:5px">
                                        <h2 style="color:white;text-align:center">Graph</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="padding:5px">
                                        <canvas id="chart-kra-<?php echo $i ?>" height="70px" class="color-canvas"></canvas>

                                        <script>
                                            var ctx = document.getElementById("chart-kra-<?php echo $i ?>");
                                            var min = 0;
                                            var max = <?php echo $kra->kraepelin[0]->jml_baris ?>;
                                            var kolom = <?php echo $kra->kraepelin[0]->jml_kolom ?>;
                                            var label = "Grafik Kraepelin";
                                            var arrl = [];
                                            var arrd = [];
                                            
                                            <?php
                                            for ($k=0; $k < $kra->kraepelin[0]->jml_kolom; $k++) {
                                                $jbenar = 0;
                                                foreach ($kra->kraepelin_d as $key => $value) {
                                                    if($value->kolom==$k){
                                                        if($value->jawaban!=''){
                                                            $jbenar++;
                                                        }
                                                    }
                                                }
                                            ?>
                                            arrd.push(<?php echo $jbenar ?>);
                                            arrl.push(<?php echo $k+1 ?>);
                                            <?php } ?>
                                            
                                            var myChart = new Chart(ctx, {
                                                type: 'line',
                                                lineTension:0, //straight lines
                                                data: {
                                                    labels: arrl,
                                                    datasets: [{
                                                        label: label,
                                                        lineTension:0, //straight lines
                                                        fill: false,
                                                        borderColor: ['#2e86de'],
                                                        data: arrd,
                                                    }]
                                                },
                                                options: {
                                                    plugins: {
                                                        // Change options for ALL labels of THIS CHART
                                                        datalabels: {
                                                            color: '#000000',
                                                        }
                                                    },
                                                    scales: {
                                                        yAxes: [{
                                                            display: true,
                                                            ticks: {
                                                                stepSize: 1,
                                                                max: max,
                                                                min:min
                                                            },
                                                            scaleLabel: {
                                                                display:true
                                                            }
                                                        }]
                                                    },
                                                }
                                            });
                                        </script>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="tabel-simpulan" style="width:100%;text-align:center">
                                            <tr class="color-sub-title" style="color:white;font-size:16px;font-weight:bold">
                                                <td>Kolom Tertinggi</td>
                                                <td>Kolom Terendah</td>
                                                <td>Penjumlahan Salah</td>
                                                <td>Tidak Dikerjakan</td>
                                                <td>Selisih Tertinggi & Terendah</td>
                                                <td>Petugas</td>
                                            </tr>
                                            <tr>
                                                <td><?php echo $kra->kraepelin[0]->titik_tertinggi ?></td>
                                                <td><?php echo $kra->kraepelin[0]->titik_terendah ?></td>
                                                <td><?php echo $kra->kraepelin[0]->jawaban_salah ?></td>
                                                <td><?php echo $kra->kraepelin[0]->tidak_diisi ?></td>
                                                <td><?php echo $kra->kraepelin[0]->selisih_titik ?></td>
                                                <td><?php echo $kra->kraepelin[0]->petugas ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="tabel-simpulan" style="width:100%;text-align:center">
                                            <tr class="color-sub-title" style="color:white;font-size:16px;font-weight:bold">
                                                <td>Kecepatan Kerja</td>
                                                <td>Ketelitian Kerja</td>
                                                <td>Keajegan Kerja ( Range )</td>
                                                <td>Keajegan Kerja ( Average D )</td>
                                                <td>Ketahanan Kerja</td>
                                                <td>Kategori</td>
                                            </tr>
                                            <tr>
                                                <td><?php echo $kra->kraepelin[0]->kecepatan_kerja ?></td>
                                                <td><?php echo $kra->kraepelin[0]->ketelitian_kerja ?></td>
                                                <td><?php echo $kra->kraepelin[0]->keajegan_kerja_range ?></td>
                                                <td><?php echo $kra->kraepelin[0]->keajegan_kerja_average_d ?></td>
                                                <td><?php echo $kra->kraepelin[0]->ketahanan_kerja ?></td>
                                                <td rowspan="2"><?php echo $kra->kraepelin[0]->kategori ?></td>
                                            </tr>
                                            <tr>
                                                <td><?php echo $kra->kraepelin[0]->nilai_kecepatan_kerja ?></td>
                                                <td><?php echo $kra->kraepelin[0]->nilai_ketelitian_kerja ?></td>
                                                <td><?php echo $kra->kraepelin[0]->nilai_keajegan_kerja_range ?></td>
                                                <td><?php echo $kra->kraepelin[0]->nilai_keajegan_kerja_average_d ?></td>
                                                <td><?php echo $kra->kraepelin[0]->nilai_ketahanan_kerja ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <br>
                        </div>
                    </section>
                <?php } ?>
            <?php } ?>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.main-sidebar').remove();
				$('.main-header').remove();
                $('.main-footer').remove();
			})

			function printPsikotes() {
				$('.modal-footer').remove();
				window.print();
				window.close();
			}
		</script>
	</body>
</html>

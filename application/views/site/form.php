<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('site/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Site Bisnis</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("site/form"); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nomor Proyek <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<select multiple class="form-control select2" name="list_proyek[]" required>
										<?php 
										foreach ($list_proyek as $item) :  
											$BUName = '';
											if($item->db_id == 423160){
												$BUName = 'SN'; 
											}else if($item->db_id == 433997){
												$BUName = 'SNI';
											}else if($item->db_id == 491420){
												$BUName = 'ION';
											}

											$branch = '';
											if($item->branch_id == 50){
												$branch = ' - Head Office';
											}else if($item->branch_id == 101 ||  $item->branch_id == 1801){
												$branch = ' - Central';
											}else if($item->branch_id == 150 ||  $item->branch_id == 1751 ){
												$branch = ' - East';
											}else if($item->branch_id == 250){
												$branch = ' - Makasar';
											}else if($item->branch_id == 52 ||  $item->branch_id == 1750){
												$branch = ' - West';
											}
										?>
											<option value="<?php echo $item->id;?>" <?php if(in_array($item->id, $proyek)): echo "selected"; endif;?>><?php echo $BUName.' - '.$item->nomor_proyek.' - '.$item->nama_proyek.$branch; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nama Site Bisnis <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nama Site Bisnis" name="name" value="<?php echo $name; ?>" style="text-transform:uppercase" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Alamat Site Bisnis</label>
								<div class="col-sm-9">
									<textarea class="form-control" name="address" rows="3" placeholder="Alamat Site Bisnis" style="text-transform:uppercase"><?php echo $address; ?></textarea>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nama Perusahaan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="client_id" required>
										<option value=""> &nbsp;-- PILIH PERUSAHAAN --</option>
										<?php foreach ($list_client as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $client_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Kota Site Bisnis <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="city_id" required>
										<option value=""> &nbsp;-- PILIH KOTA --</option>
										<?php foreach ($list_city as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $city_id): echo "selected"; endif;?>><?php echo $item->name." - ". $item->province_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Branch <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="branch_id" required>
										<option value=""> &nbsp;-- PILIH BRANCH --</option>
										<?php foreach ($list_branch as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $branch_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Bisnis Unit <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="company_id" required>
										<option value=""> &nbsp;-- PILIH BISNIS UNIT --</option>
										<?php foreach ($list_company as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $company_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">RO Site <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select multiple class="form-control select2" id="pic" name="pic_id[]" data-placeholder="Pilih RO">
										<?php foreach ($list_user as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $pic_id_1 || $item->id === $pic_id_2 || $item->id === $pic_id_3): echo "selected"; endif;?>><?php echo $item->full_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Supervisor RO <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="supervisor_id" required>
										<option value=""> &nbsp;-- PILIH SUPERVISOR RO --</option>
										<?php foreach ($list_supervisor as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $supervisor_id): echo "selected"; endif;?>><?php echo $item->full_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tanggal Mulai Kontrak <span class="text-danger"></span></label>
								<div class="col-sm-2">
									<input type="date" name="contract_start" class="form-control datepicker" value="<?php echo $contract_start; ?>">
								</div>
								<div class="col-sm-7"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tanggal Selesai Kontrak <span class="text-danger"></span></label>
								<div class="col-sm-2">
									<input type="date" name="contract_end" class="form-control datepicker" value="<?php echo $contract_end; ?>">
								</div>
								<div class="col-sm-7"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tanggal Kontrak Dihentikan<span class="text-danger"></span></label>
								<div class="col-sm-2">
									<input type="date" name="contract_terminated" class="form-control datepicker" value="<?php echo $contract_terminated; ?>">
								</div>
								<div class="col-sm-7"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Catatan Kontrak dihentikan </label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Catatan kontrak dihentikan" name="note_terminated" value="<?php echo $note_terminated; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Status Kontrak <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="contract_status" required>
										<option value="Penawaran" <?php if($contract_status === 'Penawaran'): echo "selected"; endif;?> >Penawaran</option>
										<option value="Aktif" <?php if($contract_status === 'Aktif'): echo "selected"; endif;?> >Aktif</option>
										<option value="Habis" <?php if($contract_status === 'Habis'): echo "selected"; endif;?> >Habis</option>
										<option value="Dihentikan" <?php if($contract_status === 'Dihentikan'): echo "selected"; endif;?> >Dihentikan</option>
									</select>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var last_valid_selection = null;
		$('#pic').change(function(event) {
			if ($(this).val().length > 3) {
				$(this).val(last_valid_selection);
			} else {
				last_valid_selection = $(this).val();
			}
		});
	});
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('site/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Site Bisnis</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="form-group col-sm-12">
							<form method="post" action="<?php echo base_url("site/import"); ?>" enctype="multipart/form-data">
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input btn-sm" name="file" accept=".xlsx" required>
										<label class="custom-file-label" for="exampleInputFile">Import Data</label>
									</div>
									<div class="input-group-append">
										<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>   
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<a href="<?php echo base_url("site/syncronize_site"); ?>" class="btn btn-danger btn"><i class="fas fa-sync"></i> Sinkronisasi Accurate</a>
							<a href="<?php echo base_url("site/form"); ?>" class="btn btn-primary btn"><i class="fas fa-plus"></i> Tambah Site Bisnis</a>
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th id='nomor_proyek'>Nomor Proyek</th>
									<th id='code'>Kode</th>
									<th id='name'>Nama</th>
									<th id='client_name'>Nama Perusahaan</th>
									<th id='company_name'>Bisnis Unit</th>
									<th id='city_name'>Kota</th>
									<th id='address'>Alamat</th>
									<th id='ro'>RO Site</th>
									<th id='supervisor'>Superviosr RO</th>
									<th id='branch'>Branch</th>
									<th id='contract_status'>Status Kontrak</th>
									<th id='count_employee'>Jumlah Karyawan</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("site/list_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'nomor_proyek', 'orderable':false, 'searchable':false },
				{ data: 'code' },
				{ data: 'name' },
				{ data: 'client_name' },
				{ data: 'company_code' },
				{ data: 'city_name' },
				{ data: 'address' },
				{ data: 'ro' },
				{ data: 'supervisor' },
				{ data: 'branch' },
				{ data: 'contract_status' },
				{ data: 'count_employee' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[1, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<11){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$( ".export" ).click(function() {
			var url = "<?php echo base_url('site/export'); ?>" + "?code="+$("#s_code").val()+"&name="+$("#s_name").val()+"&client_name="+$("#s_client_name").val()+"&address="+$("#s_address").val()+"&ro="+$("#s_ro").val()+"&supervisor="+$("#s_supervisor").val()+"&branch="+$("#s_branch").val()+"&contract_status="+$("#s_contract_status").val();
			window.location = url;
		});

	});
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('ropayroll/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<!-- < ?php if($site->id != 71): ?> -->
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-cogs"></i>&nbsp;Pengaturan Import Payroll</h3>
				</div>
				<div class="card-body">
					<div class="alert alert-info alert-dismissible">
						<h5><i class="icon fas fa-info"></i> Informasi</h5>
						<ul>
							<li>Pastikan masing-masing kolom ada nama kolomnya</li>
						</ul> 
					</div>
					<form method="post" action="<?php echo base_url("ropayroll/import"); ?>" enctype="multipart/form-data">
						<div class="card card-danger card-outline">					
							<div class="input-group col-md-12 mt-2">
								<label for="name" class="col-md-3 col-form-label">Silahkan Pilih Template yang akan di gunakan :<br> &nbsp;</label>
								<div class="col-md-2">
									<select class="form-control select2 config-template">
										<option value="new">TEMPLATE BARU</option>
										<?php foreach ($list_config as $item) :  ?>
											<option value="<?php echo $item->id;?>" <?php if($item->id == $site->config_id): echo "selected"; endif;?>>
												<?php echo $item->config_name; ?>
											</option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-md-4">
									<input type="hidden" class="form-control" name="config_id" value="<?php echo $site->config_id; ?>">
									<input type="text" class="form-control" name="config_name" value="<?php echo $site->config_name; ?>" required>
								</div>
								<div class="col-sm-3">
									<a onclick="confirm_del(this)" class="btn btn-danger btn text-white" style="width: 100px; cursor:pointer;" data-href="<?php echo base_url("ropayroll/delete_config/".$site->config_id); ?>"> HAPUS &nbsp;</a>
								</div>
							</div>
						</div>
						<div class="card card-warning card-outline">
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-2 col-form-label">Nomor Baris Awal Payroll <span class="text-danger">*</span></label>
								<div class="col-sm-1">
									<input type="hidden" class="form-control" name="site_id" value="<?php echo $site->site_id?>">
									<input type="text" class="form-control line" name="row_start" value="<?php echo $site->row_start; ?>" required>
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Nomor Baris Akhir Payroll <span class="text-danger">*</span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control line" name="row_end" value="<?php echo $site->row_end; ?>" required>
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Nomor Baris Nama Rincian <span class="text-danger">*</span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control line" name="row_title" value="<?php echo $site->row_title; ?>" required>
								</div>
							</div>
							<br>
						</div>
						<div class="card card-warning card-outline">
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Jumlah Hari Kerja<span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_attendance" value="<?php echo $site->column_attendance; ?>" style="text-transform:uppercase">
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Jam Lembur <span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_overtime_hour" value="<?php echo $site->column_overtime_hour; ?>" style="text-transform:uppercase">
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Perhitungan Lembur <span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_overtime_calculation" value="<?php echo $site->column_overtime_calculation; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Gaji Pokok <span class="text-danger">*</span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_basic_salary" value="<?php echo $site->column_basic_salary; ?>" style="text-transform:uppercase" required>
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Pendapatan Lain-Lain <span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control comma" name="column_income" value="<?php echo $site->column_income; ?>" style="text-transform:uppercase">
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
							</div>
							<br>
						</div>
						<div class="card card-warning card-outline">
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Potongan BPJS TK <span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_bpjs_jht" value="<?php echo $site->column_bpjs_jht; ?>" style="text-transform:uppercase">
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
<!-- 								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Potongan BPJS JP <span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_bpjs_jp" value="< ?php echo $site->column_bpjs_jp; ?>" style="text-transform:uppercase">
								</div>
								<div class="col-sm-1">&nbsp;</div> -->
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Potongan BPJS KS <span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_bpjs_ks" value="<?php echo $site->column_bpjs_ks; ?>" style="text-transform:uppercase">
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div> 
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Potongan PPH21<span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_tax_calculation" value="<?php echo $site->column_tax_calculation; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom BPJS TK Perusahaan<span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_bpjs_jht_company" value="<?php echo $site->column_bpjs_jht_company; ?>" style="text-transform:uppercase">
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom BPJS KS Perusahaan <span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_bpjs_ks_company" value="<?php echo $site->column_bpjs_ks_company; ?>" style="text-transform:uppercase">
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom Potongan Lain <span class="text-danger"></span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control" name="column_outcome" value="<?php echo $site->column_outcome; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<br>
						</div>
						<div class="card card-warning card-outline">
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-2 col-form-label">Nama Kolom NIK <span class="text-danger">*</span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_employee" value="<?php echo $site->column_employee; ?>" style="text-transform:uppercase" required>
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="text" class="col-sm-2 col-form-label column">Nama Kolom THP <span class="text-danger">*</span></label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_net" value="<?php echo $site->column_net; ?>" style="text-transform:uppercase" required>
								</div>
								<div class="col-sm-1">&nbsp;</div>
								<div class="vl"></div>
								<label for="text" class="col-sm-2 col-form-label column">Nama Kolom Catatan </label>
								<div class="col-sm-1">
									<input type="text" class="form-control column" name="column_note" value="<?php echo $site->column_note; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-2 col-form-label">Periode Awal <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="date" class="form-control datepicker" name="periode_start" required>
								</div>
								<div class="vl"></div>
								<label for="name" class="col-sm-2 col-form-label">Periode Selesai <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="date" class="form-control datepicker" name="periode_end" required>
								</div>
								<div class="vl"></div>
							</div>
							<br>
						</div>
						<div class="card card-warning card-outline">
							<div class="input-group col-sm-12 mt-2">
								<label for="name" class="col-sm-3 col-form-label">Pilih File Payroll <span class="text-danger">*</span></label>
								<div class="custom-file col-sm-9">
									<input type="file" class="custom-file-input" name="file" accept=".xlsx" required>
									<label class="custom-file-label">Pilih file</label>
								</div>
								<div class="input-group-append">
									<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>    
								</div>
							</div>
							<br>
						</div>
					</form>
				</div>
			</div>
			<!-- < ?php endif; ?> -->
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;List Payroll Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<form id="payroll" method="POST" action="<?php echo base_url('ropayroll/payroll_count'); ?>">
					<div class="row">
						<label class="col-sm-1 col-form-label">Tanggal Awal :</label>
						<div class="col-sm-3">
							<div class="form-group">
								<div class="input-group date" id="reservationdate" data-target-input="nearest">
									<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name='date_start' value="" required />
									<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>
							</div>
						</div>
						<label class="col-sm-1 col-form-label">Tanggal Akhir :</label>
						<div class="col-sm-3">
							<div class="input-group date" id="reservationdate2" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate2" name='date_finish' value="" required />
								<div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<button type="submit" class="btn btn-block btn-primary" style="color: white;"><i class="fa fa-cogs"></i> Hitung</button> 
						</div>
						<div class="col-sm-2">
							<!-- <a href="<?php echo base_url('files/formula/'.$site_id.'.xlsx'); ?>" class="btn btn-block btn-success" style="margin-right:10px;"><i class="fas fa-file-excel"></i> Export</a> -->
							<a href="<?php echo base_url('ropayroll/export'); ?>" class="btn btn-block btn-success" style="margin-right:10px;"><i class="fas fa-file-excel"></i> Export</a>
						</div>
					</div>
					</form>
				</div>
				<div class="card-header">
					<div class="row">
						<div class="col-sm-2">
							&nbsp;
						</div>
						<div class="col-sm-7 process" hidden="true">
							<input type="text" class="form-control note float-right" placeholder="Catatan Pengajuan" name="note" value="">
						</div>
						<div class="col-sm-3 process" hidden="true">
							<a href="#" class="btn btn-danger delete  float-right"  style="margin-right:10px;"><i class="fas fa-trash"></i> Hapus</a>
							<a href="#" class="btn btn-warning cancel float-right" style="margin-right:10px;"><i class="fas fa-window-close"></i> Batalkan</a>
							<a href="#" class="btn btn-info submission float-right" style="margin-right:10px;"><i class="fas fa-check"></i> Ajukan</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
						</thead>
					</table>
					<div class="row">
						<div class="col-md-6">
							<?php
								if(isset($resume_payroll)){
								$start_date = substr($resume_payroll->periode_start, 8, 2).' '.get_month(substr($resume_payroll->periode_start, 5, 2));
								$end_date 	= substr($resume_payroll->periode_end, 8, 2).' '.get_month(substr($resume_payroll->periode_end, 5, 2)).' '.substr($resume_payroll->periode_end, 0, 4);
							?>
							<table class="table table-striped">
								<thead>
									<tr>
										<td width="250px"><strong>Periode Pengajian Terakhir</strong></td>
										<td><strong>: <?php echo $start_date; ?> - <?php echo $end_date; ?></strong></td>
									</tr>
									<tr>
										<td width="250px"><strong>Total Karyawan Aktif</strong></td>
										<td><strong>: <?php echo $employee; ?> Karyawan</strong></td>
									</tr>
									<tr>
										<td width="250px"><strong>Total Karyawan terakhir digaji</strong></td>
										<td><strong>: <?php echo $resume_payroll->employee_payroll; ?> Karyawan</strong></td>
									</tr>
									<tr>
										<td width="250px"><strong>Total Penggajian Terakhir </strong></td>
										<td><strong>: <?php echo format_rupiah($resume_payroll->summary_payroll); ?></strong></td>
									</tr>
								</thead>
							</table>
							<?php } ?>
						</div>
						<div class="col-md-6"><?php
								if(isset($resume_approval)){
								$start_date = substr($resume_approval->periode_start, 8, 2).' '.get_month(substr($resume_approval->periode_start, 5, 2));
								$end_date 	= substr($resume_approval->periode_end, 8, 2).' '.get_month(substr($resume_approval->periode_end, 5, 2)).' '.substr($resume_approval->periode_end, 0, 4);
							?>
							<table class="table table-striped">
								<thead>
									<tr>
										<td width="300px"><strong>Periode Pengajuan Terakhir</strong></td>
										<td><strong>: <?php echo $start_date; ?> - <?php echo $end_date; ?></strong></td>
									</tr>
									<tr>
										<td width="300px"><strong>Total Karyawan Aktif</strong></td>
										<td><strong>: <?php echo $employee; ?> Karyawan</strong></td>
									</tr>
									<tr>
										<td width="300px"><strong>Total Karyawan terakhir Pengajuan</strong></td>
										<td><strong>: <?php echo $resume_approval->employee_payroll; ?> Karyawan</strong></td>
									</tr>
									<tr>
										<td width="300px"><strong>Total Pengajuan Terakhir </strong></td>
										<td><strong>: <?php echo format_rupiah($resume_approval->summary_payroll); ?></strong></td>
									</tr>
								</thead>
							</table>
							<?php } ?>
						</div>
					</div>
					<p>&nbsp;</p>
					<div class="row">
						<div class="col-sm-2">
							<strong>Pilih data payroll yang ditampilkan :</strong>
						</div>
						<div class="col-sm-3">
							<select class="form-control preview float-right">
								<option value="<?php echo base_url("ropayroll/list/last");?>" <?php if($view == "last" ): echo "selected"; endif; ?>>Pengajuan/Draft Terakhir</option>
								<option value="<?php echo base_url("ropayroll/list/all");?>" <?php if($view == "all" ): echo "selected"; endif; ?>>Semua </option>
							</select>
						</div>
						<div class="col-sm-7">
						</div>
					<p>&nbsp;</p>

					<div class="scroll-panel">
					<form method="POST" id="process" action="<?php echo base_url('ropayroll/process'); ?>">
					<input type="hidden" id="process_type" name="process_type" value="">
					<input type="hidden" id="note" name="note" value="">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th width="50px">No</th>
									<th id='employee_name'>Nama Karyawan</th>
									<th id='phone_number'>Nomor Telp/WA</th>
									<th id='periode_start'>Periode Mulai</th>
									<th id='periode_end'>Periode Selesai</th>
									<th id='bpjs_ks_company'>BPJS KS Perusahaan</th>
									<th id='bpjs_jht_company'>BPJS TK Perusahaan</th>
									<th id='salary'>Jumlah Pendapatan</th>
									<th id='payment'>Status Pembayaran</th>
									<th id='email'>Status Notifikasi</th>
									<th id='note'>Catatan</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<style type="text/css">
	input[type="text" i], input[type="email" i], input[type="select" i], input[type="date" i], .select2-container, textarea {
    	margin-left: -7px;
	}
	.vl {
		margin-top: -10px;
		margin-bottom: -25px;
		border-left: 3px solid #ffc107;
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		$('.preview').on('change', function() {
			window.location.href = this.value;
		});
		$('.config-template').on('change', function() {
			window.location.href = '<?php echo base_url("ropayroll/list/".$view."/");?>'+this.value;
		});
		$( ".comma" ).keyup(function() {
			var value = $(this).val();
			console.log(value);
			if(value.includes(",,")){
				$(this).val(value.substring(0, value.length-1));
				alert('Terdapat koma dobel, silahkan cek lagi.');				
			}
		});
		$( ".line" ).keyup(function() {
			var value = $(this).val();
			if(!Number.isInteger(Number(value))){
				alert('Masukkan satu nomor baris saja.');
				$(this).val(value.substring(0, value.length-1));
			}
		});
		$( ".column" ).keyup(function() {
			var value = $(this).val();
			if((/[^a-zA-Z]/.test(value))){
				alert('Masukkan satu nama kolom saja.');
				$(this).val(value.substring(0, value.length-1));
			}
		});
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("ropayroll/list_ajax/".$view);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'employee_name' },
				{ data: 'phone_number' },
				{ data: 'periode_start' },
				{ data: 'periode_end' },
				{ data: 'bpjs_ks_company' },
				{ data: 'bpjs_jht_company' },
				{ data: 'salary' },
				{ data: 'payment' },
				{ data: 'email' },
				{ data: 'note' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[4, 'desc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>1 && index<12){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".process").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".process").prop('hidden', false);
    		});
		});
		$("input:checkbox").change(function() {
			$(".process").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".process").prop('hidden', false);
    		});
		});
		$( ".btn.delete" ).click(function() {
			$("#process_type").val('delete');
			$("#note").val($('.note').val());
			$( "#process" ).submit();
		});
		$( ".btn.cancel" ).click(function() {
			$("#process_type").val('cancel');
			$("#note").val($('.note').val());
			$("#process").submit();
		});
		$( ".btn.submission" ).click(function() {
			$("#process_type").val('submission');
			$("#note").val($('.note').val());
			$("#process").submit();
		});
		$('#reservationdate').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false
		});
		$('#reservationdate2').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false
		});
		// $(".count" ).click(function() {
		// 	$('#payroll').attr('action', "<?php echo base_url('ropayroll/payroll_count'); ?>").submit();
		// });
	});
</script>
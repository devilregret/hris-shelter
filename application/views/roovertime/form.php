<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('roovertime/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Ubah Pengaturan Lembur</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("roovertime/form"); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Keterangan<span class="text-danger"> *</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<select class="form-control select2" name="name" required>
										<option value='Flat' <?php if("Flat"==$name): echo "selected"; endif;?>>Flat</option>
										<option value='Normatif' <?php if("Normatif"==$name): echo "selected"; endif;?>>Normatif</option>
									</select>
								</div>
							</div>
							<div class="form-group row normatif <?php if($name == 'Normatif'): echo "hidden"; endif; ?>">
								<label for="name" class="col-sm-3 col-form-label">Jam Telat/Lembur<span class="text-danger"> *</span></label>
								<div class="col-sm-1">
									<select class="form-control select2" name="time" required>
										<?php for($i=1; $i<=24; $i++): ?>
											<option value='<?php echo $i; ?>' <?php if($i==$time): echo "selected"; endif;?>><?php echo $i; ?></option>
										<?php endfor; ?>
									</select>
								</div>
								<label for="name" class="col-sm-8 col-form-label">Jam</label>
							</div>
							<div class="form-group row normatif <?php if($name == 'Normatif'): echo "hidden"; endif; ?>">
								<label for="name" class="col-sm-3 col-form-label"> Nominal<span class="text-danger"> *</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control numeric rupiah" placeholder="Nominal" name="value" value="<?php echo number_format($value,0,',','.'); ?>" required>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
<script type="text/javascript">
$('.select2').on('change', function() {
	if(this.value == 'Normatif'){
		$('.normatif').addClass('hidden');
	}else{
		$('.normatif').removeClass('hidden');
	}
});
</script>
<style type="text/css">
.hidden {
	visibility: hidden;
	display: none;
}
</style>
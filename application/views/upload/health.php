<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('upload/health'); ?>">Upload Kartu Asuransi Kesehatan </a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Upload Kartu Asuransi Kesehatan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?> &nbsp;
					<?php if($failed): ?>
					<div class="row  float-right">
						<a target="_blank" class="btn btn-success" href="<?php echo base_url('upload/export/'.$report_id); ?>" style="width:200px;"><i class="fas fa-file-csv"></i> Daftar Upload Gagal</a> &nbsp;
						<a onclick="confirm_del(this)" class="btn btn-danger" data-href="<?php echo base_url('upload/delete/'.$report_id); ?>" style="width:200px;"><i class="fas fa-trash-alt"></i> Hapus</a>
					</div>
					<?php endif; ?>
				</div>
				<div class="card-body">
					<div class="alert alert-info alert-dismissible">
						<h5><i class="icon fas fa-info"></i> Informasi</h5>
						<ul>
							<li>gunakan Nomor KIS untuk penamaan kartu Asuransi Kesehatan</li>
							<li>compress kartu ketenagakerjaan dalam bentuk zip</li>
							<li>compress semua kartu ketenagakerjaan secara langsung, jangan compress pada folder</li>
						</ul> 
					</div>
					<p>&nbsp;</p>
					<div class="row">
						<div class="col-sm-12">
							<form method="post" action="<?php echo base_url("upload/health"); ?>" enctype="multipart/form-data">
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input btn-sm" name="file" accept=".zip" required> 
										<label class="custom-file-label" for="exampleInputFile">Update Data</label>
									</div>
									<div class="input-group-append">
										<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>   
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
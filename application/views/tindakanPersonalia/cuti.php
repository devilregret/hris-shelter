<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('tindakanPersonalia/cuti'); ?>">Cuti</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Cuti Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 ">Sisa Cuti Tahunan</div>
        				<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12 "> : <?php echo $sisa_cuti?></div>
					</div>
					<div class="row">
						<div class="col-lg-2  col-sm-3 col-xs-12 ">Total Cuti Khusus</div>
        				<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12 "> : <?php echo $cuti_normatif?></div>
					</div>
					<div class="row">
						<div class="col-lg-2  col-sm-3 col-xs-12 ">Total Cuti Non Normatif</div>
        				<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12 "> : <?php echo $cuti_non_normatif?></div>
					</div>
					<div class="row"><p></p></div>
					<div class="row">
						<a href="<?php echo base_url("tindakanPersonalia/cuti_form"); ?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Ajukan Cuti</a>
					</div>
				</div>
				<div class="card-body">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width='3%' id='id'>ID</th>	
									<th id='employee_name'>Nama</th>
									<th id='created_at'>Diajukan</th>
									<th id='start_date'>Mulai</th>
									<th id='end_date'>Akhir</th>
									<th width='5%' id='verifikasi_1'>Verifikasi 1</th>
									<th width='5%' id='verifikasi_2'>Verifikasi 2</th>
									<th width='5%' id='verifikasi_3'>Verifikasi 3</th>
									<th width='12%' id='keterangan'>Keterangan</th>
									<!-- <th id='file_pendukung'>File</th> -->
									<th width='8%' id='Action'>Action</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-upload">
  Launch demo modal
</button> -->

<!-- Modal -->
<div class="modal fade" id="modal-upload" tabindex="-1" role="dialog" aria-labelledby="modal-upload">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title" id="myModalLabel">Modal title</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
	  	<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("tindakanPersonalia/cuti_update_document"); ?>" enctype="multipart/form-data">
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Id <span class="text-danger"></span></label>
				<div class="col-sm-9">
					<input type="text" readonly name="form-id" id="form-id" class="form-control">
				</div>
			</div>
			<div class="form-group row" style="display: none">
				<label class="col-sm-3 col-form-label">Start Date <span class="text-danger"></span></label>
				<div class="col-sm-9">
					<input type="text" readonly name="form-start-date" id="form-start-date" class="form-control">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Dokumen <span class="text-danger"></span></label>
				<div class="col-sm-5">
					<input type="file" class="upload-document" name="dokumen_pendukung" />
				</div>
			</div>	
			<div class="modal-footer ">
				<button type="submit" class="btn btn-primary btn-form" >Simpan</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>

<script type="text/javascript">
	function setValueId(val1, val2){
		$("#form-id").val(val1);
		$("#form-start-date").val(val2);
	}

	$(document).ready(function(){
		// $(function() {
		// 	$('#modal-upload').on('show.bs.modal',function(){
		// 		$("#form-id").val('10');
		// 		// alert('ok');
		// 	});
		// });

		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("tindakanPersonalia/cuti_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id' },
				{ data: 'employee_name' },
				{ data: 'created_at' },
				{ data: 'start_date' },
				{ data: 'end_date' },
				{ data: 'verifikasi_1' },
				{ data: 'verifikasi_2' },
				{ data: 'verifikasi_3' },
				{ data: 'keterangan' },
				// { data: 'file_pendukung' },
				{ data: 'action' }
			],
			'order': [[3, 'DESC']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>=1 && index<9){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
	});
</script>
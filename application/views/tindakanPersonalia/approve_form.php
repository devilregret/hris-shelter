<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
	<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('tindakanPersonalia/cuti_form'); ?>">Pengajuan Cuti</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Pengajuan Cuti</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("tindakanPersonalia/cuti_update_approve"); ?>">
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Id <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" readonly name="id" class="form-control datepicker" value="<?php echo $data->id; ?>">
								</div>
							</div>
							<div style="display: none;" class="form-group row">
								<label class="col-sm-3 col-form-label">Approval Level <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" readonly name="approval_level" class="form-control datepicker" value="<?php echo $level; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Nama Karyawan <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="text" readonly name="employee_name" class="form-control datepicker" value="<?php echo $data->employee_name; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tanggal Mulai <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="date" readonly name="employee_name" class="form-control datepicker" value="<?php echo $data->start_date; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Alasan <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<textarea rows=3 id="keterangan" class="form-control" readonly><?php echo $data->keterangan; ?></textarea>	
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Status <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select name="status" class="form-control" id="status" required>
										<option value="">Pilih Status</option>	
										<option value="1">Approve</option>
										<option value="0">Reject</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Keterangan <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<textarea rows=4 name="note_1" class="form-control" required></textarea>	
								</div>
							</div>

							<div class="modal-footer  float-right">
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
								<button type="reset"  onclick="window.close();" class="btn btn-default btn-form">Batal</button>&nbsp;
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>

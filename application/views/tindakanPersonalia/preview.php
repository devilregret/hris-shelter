<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
	</head>
	<body>
		<section class="content">
			<div class="container-fluid">
				<div class="card card-primary card-outline col-12 mt-2">
					<div class="card-header">
						<h3 class="card-title"><i class="fas fa-eye"></i>&nbsp;Detail Cuti</h3>
					</div>
					<div class="row">
						<div class="col-12 table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Nama Karyawan</td>
										<td>: <?php echo $preview->employee_name; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Kategori</td>
										<td>: <?php echo $preview->kategori; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Type</td>
										<td>: <?php echo $preview->type; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Cuti Mulai</td>
										<td>: <?php echo $preview->start_date; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Cuti Sampai</td>
										<td>: <?php echo $preview->end_date; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Jumlah Hari</td>
										<td>: <?php echo $preview->jumlah_hari; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Keterangan</td>
										<td>: <?php echo $preview->keterangan; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Verifikasi Status</td>
										<td>: <?php if ($preview->status1 == "Approve" ){ echo "<span class='badge badge-success'>".$preview->status1."</span>";}else if($preview->status1 == "Reject"){echo "<span class='badge badge-danger'>".$preview->status1."</span>";}else echo "<span class='badge badge-warning'>".$preview->status1."</span>"; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Verifikasi Oleh</td>
										<td>: <?php echo $preview->verifikasi_by_name; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Verifikasi Pada</td>
										<td>: <?php echo $preview->verifikasi_at; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Disetujui Status</td>
										<td>: <?php if ($preview->struktur_2 == 0){echo "<span class='badge badge-info'>Tidak ada</span>" ;} else if($preview->status2 == "Approve" ){ echo "<span class='badge badge-success'>".$preview->status2."</span>";}else if($preview->status2 == "Reject"){echo "<span class='badge badge-danger'>".$preview->status2."</span>";}else {echo "<span class='badge badge-warning'>".$preview->status2."</span>";} ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Disetujui Oleh</td>
										<td>: <?php echo $preview->disetujui_by_name; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Disetujui Pada</td>
										<td>: <?php echo $preview->disetujui_at; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Diketahui Status</td>
										<td>: <?php if ($preview->struktur_3 == 0){echo "<span class='badge badge-info'>Tidak ada</span>" ;} else if ($preview->status3 == "Approve" ){ echo "<span class='badge badge-success'>".$preview->status3."</span>";}else if($preview->status3 == "Reject"){echo "<span class='badge badge-danger'>".$preview->status3."</span>";}else echo "<span class='badge badge-warning'>".$preview->status3."</span>"; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Diketahui Oleh</td>
										<td>: <?php echo $preview->diketahui_by_name; ?></td>
									</tr>
									<tr>
										<td width="20px">&nbsp</td>
										<td width="250px">Diketahui Pada</td>
										<td>: <?php echo $preview->diketahui_at; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Tanggal dibuat</td>
										<td>: <?php echo $preview->created_at; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>File Pendukung</td>
										<td>: <?php if ($preview->dokumen_pendukung != null ){ echo "<a href='".base_url($preview->dokumen_pendukung)."' target=”_blank”>Open</a>";} else{echo "-"; }?></td>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="window.close();">Tutup</button>
       	</div>
	</body>
</html>
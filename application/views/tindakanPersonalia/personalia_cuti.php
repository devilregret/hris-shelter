<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('tindakanPersonalia/personalia_cuti'); ?>">Personalia Cuti</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Cuti Seluruh Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<a href="<?php echo base_url("tindakanPersonalia/personalia_cuti_form"); ?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Tambah cuti untuk karyawan</a>
					</div>
				</div>
				<div class="card-body">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th id='id'>ID</th>	
									<th id='employee_name'>Nama</th>
									<th id='created_at'>Diajukan</th>
									<th id='start_date'>Mulai</th>
									<th id='end_date'>Akhir</th>
									<th id='verifikasi_1'>Verifikasi 1</th>
									<th id='verifikasi_2'>Verifikasi 2</th>
									<th id='verifikasi_3'>Verifikasi 3</th>
									<th id='keterangan'>Keterangan</th>
									<th id='Action'>Action</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<!-- <th></th>
									<th></th>
									<th></th> -->
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("tindakanPersonalia/personalia_cuti_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id' },
				{ data: 'employee_name' },
				{ data: 'created_at' },
				{ data: 'start_date' },
				{ data: 'end_date' },
				{ data: 'verifikasi_1' },
				{ data: 'verifikasi_2' },
				{ data: 'verifikasi_3' },
				{ data: 'keterangan' },
				{ data: 'action' }
			],
			'order': [[3, 'DESC']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>=0 && index<9){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
	});
</script>
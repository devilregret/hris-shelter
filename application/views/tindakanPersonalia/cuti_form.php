<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
	<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('tindakanPersonalia/cuti_form'); ?>">Pengajuan Cuti</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Pengajuan Cuti</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("tindakanPersonalia/cuti_form"); ?>" enctype="multipart/form-data">
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Kategori <span class="text-danger"></span></label>
								<div class="col-sm-5">
									<select name="kategori" id="kategori" class="form-control" id="status" required onchange="pilihKategori()">
										<option value="">::: Pilih Kategori :::</option>	
										<option value="1">Tahunan</option>
										<option value="2">Cuti Khusus</option>
										<option value="3">Sakit</option>
										<option value="4">Lain - Lain</option>
									</select>
								</div>
							</div>	
							<div id = "div_type" style="display: none" class="form-group row">
								<label class="col-sm-3 col-form-label">Type <span class="text-danger"></span></label>
								<div class="col-sm-5">
									<select name="type" class="form-control" id="type">
										<option value="">::: Pilih Type ::: </option>	
										<?php foreach($m_type_cuti as $row)
										{ 
											echo '<option value="'.$row->id.'::'.$row->nama.'">'.$row->nama.'</option>';
										}?>
									</select>
								</div>
							</div>	
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Dokumen Pendukung <span class="text-danger"></span></label>
								<div class="col-sm-5">
									<input type="file" class="upload-document" name="dokumen_pendukung" />
									<!-- <input type="file" class="custom-file-input upload-photo upload-document" name="document_photo" accept="image/jpeg, image/jpg"> -->
								</div>
							</div>	
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tanggal Mulai <span class="text-danger"></span></label>
								<div class="col-sm-5">
									<input type="date" name="start_date" class="form-control datepicker" value="<?php echo $start_date; ?>">
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tanggal Selesai <span class="text-danger"></span></label>
								<div class="col-sm-5">
									<input type="date" name="end_date" class="form-control datepicker" value="<?php echo $end_date; ?>">
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Keterangan<span class="text-danger"></span></label>
								<div class="col-sm-9">
									<textarea class="form-control" name="keterangan" rows="3" placeholder="Keterangan Cuti"><?php echo $keterangan; ?></textarea>
								</div>
							</div>
							<br>
							<div class="form-group row">
							<label style="font-size:12px;" class="col-sm-11 col-form-label">Note : Cuti yang sudah habis / bagi Karyawan yang belum mendapatkan jatah cuti tahunan, maka sifatnya pinjam cuti<span class="text-danger"></span></label>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>


<script>
	function pilihKategori() {
		if (document.getElementById("kategori").value == 2){
			document.getElementById('div_type').style.display = "";
			// document.getElementById('div_file').style.display = "none";
		}else if(document.getElementById("kategori").value == 3){
			document.getElementById('div_type').style.display = "none";
			// document.getElementById('div_file').style.display = "";
		}else{
			// document.getElementById('div_file').style.display = "none";
			document.getElementById('div_type').style.display = "none";
		}        
	}
</script>
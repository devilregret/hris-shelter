<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('attendance/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-calendar-week"></i>&nbsp;Jadwal Kerja</h3>
				</div>
				<div class="card-body">
					<form method="POST" action="<?php echo base_url('attendance/site'); ?>">
						<div class="row">
							<label for="name" class="col-sm-2 col-form-label">Pilih Site :</label>
							<div class="col-sm-8" >
								<select class="form-control select2" name="site_id">
									<option value=""> &nbsp; </option>
									<?php foreach ($list_site as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id == $site_id): echo 'selected'; endif;?>><?php echo
										$item->name.' - '.$item->company_code; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-sm-2" >
								<button type="submit" class="btn btn-primary btn-block">Simpan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<?php if($site_id): ?>
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Jadwal Kerja</h3>
				</div>
				<?php
					$prev 	= date('m', strtotime("-1 months"));
					$current= date('m');
					$next 	= date('m', strtotime("1 months"));
					$year 	= date('Y');
				?>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-2">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
						<div class="col-sm-10">
							<form method="post" action="<?php echo base_url("attendance/import/".$view); ?>" enctype="multipart/form-data">
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input btn-sm" name="file" accept=".xlsx" required> 
										<label class="custom-file-label">Update Data</label>
									</div>
									<div class="input-group-append">
										<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>   
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="card-header">
					<form method="POST" id="schedule" action="<?php echo base_url('attendance/generate'); ?>">		
					<div class="row">
						<label class="col-sm-2 col-form-label text-right">Buat Jadwal bulan :</label>
						<div class="col-sm-5">
							<select class="form-control select2" name="periode">
								<option value="<?php echo $prev; ?>" <?php if($view == $prev ): echo "selected"; endif; ?>>
									<?php echo get_month($prev).' '.$year; ?>
								</option>
								<option value="<?php echo $current; ?>" <?php if($view == $current ): echo "selected"; endif; ?>>
									<?php echo get_month($current).' '.$year; ?>
								</option>
								<option value="<?php echo $next; ?>" <?php if($view == $next ): echo "selected"; endif; ?>>
									<?php echo get_month($next).' '.$year; ?>
								</option>
							</select>
						</div>
						<label class="col-sm-1 col-form-label text-right">ID Mesin :</label>
						<div class="col-sm-2">
							<input type="text" name="attendance_machine" class="form-control" value="<?php echo $site->attendance_machine; ?>">
						</div>
						<div class="col-sm-1">
							<button type="submit" class="btn btn-success btn-block">Proses</button>
						</div>
						<div class="col-sm-1">
							<button type="submit" class="btn btn-warning btn-block sync">Sinkronisasi</button>
						</div>
					</div>
					</form>
				</div>
				<div class="card-header">
					<div class="row save" hidden="true">
						<label class="col-sm-1 col-form-label text-right">Jadwal Masuk :</label>
						<div class="col-sm-1">
							<div class="input-group date" id="hours_start" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input hours_start_val" data-target="#hours_start" name='hours_start' value=""/>
								<div class="input-group-append" data-target="#hours_start" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
						</div>
						<label class="col-sm-1 col-form-label text-right">Jadwal Pulang :</label>
						<div class="col-sm-1">
							<div class="input-group date" id="hours_end" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input hours_end_val" data-target="#check_out" name='hours_end' value="" />
								<div class="input-group-append" data-target="#hours_end" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
						</div>
						<label class="col-sm-1 col-form-label text-right">Masuk :</label>
						<div class="col-sm-1">
							<div class="input-group date" id="hours_come" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input hours_come_val" data-target="#hours_come" name='hours_come' value=""/>
								<div class="input-group-append" data-target="#hours_come" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
						</div>
						<label class="col-sm-1 col-form-label text-right">Pulang :</label>
						<div class="col-sm-1">
							<div class="input-group date" id="hours_return" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input hours_return_val" data-target="#hours_return" name='hours_return' value="" />
								<div class="input-group-append" data-target="#hours_return" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
						</div>
						<label class="col-sm-2 col-form-label text-right">Jenis Izin :</label>
						<div class="col-sm-2">
							<select class="form-control select2" id="type">
								<option value=''>Pilih Izin</option>
								<option value='Izin Cuti'>Izin Cuti</option>
								<option value='Izin Terlambat'>Izin Terlambat</option>
								<option value='Izin Pulang'>Izin Pulang</option>
							</select>
						</div>
					</div>
					&nbsp;
					<div class="row save" hidden="true">
						<div class="col-sm-10">
							<input type="text" id="description" class="form-control" placeholder="Keterangan">
						</div>
						<div class="col-sm-2">
							<a href="#" class="btn btn-warning process btn-block"><i class="fas fa-check"></i> Atur Ulang</a>
						</div>
					</div>
					&nbsp;
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
							<tr>
								<td width="250px"><strong>Total Karyawan Aktif</strong></td>
								<td><strong>: <?php echo $employee; ?> Karyawan</strong></td>
							</tr>
							<?php if($cutoff): ?>
							<tr>
								<td width="250px"><strong>Tanggal Cut Off</strong></td>
								<td><strong>: <?php echo $cutoff->start_date.' - '.$cutoff->end_date; ?></strong></td>
							</tr>
							<tr>
								<td width="250px"><strong>Formula Perhitunga Payroll</strong></td>
								<td><strong>: <?php echo $cutoff->formula; ?></strong></td>
							</tr>
						<?php endif; ?>
						</thead>
					</table>
					<p>&nbsp;</p>
					<div class="row">
						<div class="col-sm-2">
							<strong>Pilih data periode yang ditampilkan :</strong>
						</div>
						<div class="col-sm-2">
							<select class="form-control preview float-right">
								<option value="<?php echo base_url("attendance/list/".$prev);?>" <?php if($view == $prev ): echo "selected"; endif; ?>>
									<?php echo get_month($prev).' '.$year; ?>
								</option>
								<option value="<?php echo base_url("attendance/list/".$current);?>" <?php if($view == $current ): echo "selected"; endif; ?>>
									<?php echo get_month($current).' '.$year; ?>
								</option>
								<option value="<?php echo base_url("attendance/list/".$next);?>" <?php if($view == $next ): echo "selected"; endif; ?>>
									<?php echo get_month($next).' '.$year; ?>
								</option>
							</select>
						</div>
						<div class="col-sm-8">
							&nbsp;
						</div>
					<p>&nbsp;</p>
					</div>
					<form method="POST" id="process" action="<?php echo base_url('attendance/process/'.$view); ?>">
					<input type="hidden" class="form-control type" name="type" value="">
					<input type="hidden" class="form-control hours_start" name="hours_start" value="">
					<input type="hidden" class="form-control hours_end" name="hours_end" value="">
					<input type="hidden" class="form-control hours_come" name="hours_come" value="">
					<input type="hidden" class="form-control hours_return" name="hours_return" value="">
					<input type="hidden" class="form-control description" name="description" value="">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="10px"><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th id='employee_number'>NIP</th>
									<th id='id_card'>NIK</th>
									<th id='full_name'>Nama Karyawan</th>
									<th id='position'>Jabatan</th>
									<th id='type'>Izin</th>
									<th id='description'>Keterangan</th>
									<th id='date'>Tanggal</th>
									<th id='day'>Hari</th>
									<th id='schedule_start'>Jadwal Masuk</th>
									<th id='schedule_end'>Jadwal Pulang</th>
									<th id='attendance_start'>Karyawan Masuk</th>
									<th id='attendance_end'>Karyawan Pulang</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.preview').on('change', function() {
			window.location.href = this.value;
		});
		<?php if($site_id): ?>
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("attendance/list_ajax/".$view);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'employee_number' },
				{ data: 'id_card' },
				{ data: 'full_name' },
				{ data: 'position' },
				{ data: 'type' },
				{ data: 'description' },
				{ data: 'date' },
				{ data: 'day', 'orderable':false, 'searchable':false },
				{ data: 'schedule_start', 'orderable':false, 'searchable':false },
				{ data: 'schedule_end', 'orderable':false, 'searchable':false },
				{ data: 'attendance_start', 'orderable':false, 'searchable':false },
				{ data: 'attendance_end', 'orderable':false, 'searchable':false },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[7, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<8){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
		<?php endif; ?>
		
		$( ".export" ).click(function() {
			var url = "<?php echo base_url('attendance/export/'.$view); ?>" + "?employee_number="+$("#s_employee_number").val()+"&id_card="+$("#s_id_card").val()+"&full_name="+$("#s_full_name").val()+"&position="+$("#s_position").val()+"&type="+$("#s_type").val()+"&description="+$("#s_description").val()+"&date="+$("#s_date").val();
			window.location = url;
		
		});

		$('#checkAll').click(function () {
			$('input:checkbox').prop('checked', this.checked);
		});

		$(document).on("change", ".check-id", function() {
			$(".save").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".save").prop('hidden', false);
			});
		});

		$("input:checkbox").change(function() {
			$(".save").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".save").prop('hidden', false);
			});
		});

		$( ".btn.process" ).click(function() {
			// if($( "#hours_start").val() == '' || $( "#hours_end").val() == ''){
			// 	alert('Silahkan pilih jam masuk & jam pulang !!!');
			// }else{
				$( ".type").val($( "#type").val());
				$( ".hours_start").val($( ".hours_start_val").val());
				$( ".hours_end").val($( ".hours_end_val").val());
				$( ".hours_come").val($( ".hours_come_val").val());
				$( ".hours_return").val($( ".hours_return_val").val());
				$( ".description").val($( "#description").val());
				$( "#process" ).submit();
			// }
		});

		$(".sync").on("click", function(e){
			var url = "<?php echo base_url('attendance/syncronize'); ?>";
			$('#schedule').attr('action', url).submit();
		});

		$('#hours_start').datetimepicker({
			format: 'HH:mm',
			pickDate: false,
			pickSeconds: false,
			pick12HourFormat: false  
		});
		$('#hours_end').datetimepicker({
			format: 'HH:mm',
			pickDate: false,
			pickSeconds: false,
			pick12HourFormat: false  
		});

		$('#hours_come').datetimepicker({
			format: 'HH:mm:ss',
			pickDate: false,
			pickSeconds: false,
			pick12HourFormat: false  
		});
		$('#hours_return').datetimepicker({
			format: 'HH:mm:ss',
			pickDate: false,
			pickSeconds: false,
			pick12HourFormat: false  
		});
	});
</script>
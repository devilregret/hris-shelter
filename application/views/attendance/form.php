<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('roattendance2/list'); ?>">Kehadiran Karyawan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Kehadiran <?php echo $employee->full_name; ?></h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("roattendance2/form/".$employee->id); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nama Karyawan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="employee_id" value="<?php echo $employee->id; ?>">
									<input type="text" class="form-control" placeholder="Nama Karyawan"  value="<?php echo $employee->full_name; ?>" readonly>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nomor Induk Karyawan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nomor Induk Karyawan"  value="<?php echo $employee->employee_number; ?>" readonly>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Tanggal <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="date" name="date" class="form-control datepicker" value="<?php echo $date; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Keterangan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="note" required>
										<option value='Masuk' <?php if("Masuk"==$note): echo "selected"; endif;?>>Masuk</option>
										<option value='Mangkir' <?php if("Mangkir"==$note): echo "selected"; endif;?>>Mangkir</option>
										<option value='Cuti' <?php if("Cuti"==$note): echo "selected"; endif;?>>Cuti</option>
										<option value='Izin' <?php if("Izin"==$note): echo "selected"; endif;?>>Izin</option>
										<option value='Sakit' <?php if("Sakit"==$note): echo "selected"; endif;?>>Sakit</option>
										<option value='Telat' <?php if("Telat"==$note): echo "selected"; endif;?>>Telat</option>
										<option value='Lembur' <?php if("Lembur"==$note): echo "selected"; endif;?>>Lembur</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Jam Telat/Lembur <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="text" name="hours" class="form-control" value="<?php echo $hours; ?>" required>
								</div>
								<div class="col-sm-7">
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
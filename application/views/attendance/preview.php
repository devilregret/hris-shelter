<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>

	</head>
	<body>
		<section class="content">
			<div class="container-fluid">
				<div class="card card-primary card-outline col-12 mt-2">
					<div class="card-header">
						<h3 class="card-title"><i class="fas fa-eye"></i>&nbsp;Preview Cuti</h3>
					</div>
					<div class="row">
						<div class="col-12 table-responsive">
							<?php if($preview): ?>
							<table class="table table-striped">
								<thead>
									<tr>
										<td>&nbsp</td>
										<td>NIP</td>
										<td>: <?php echo $preview->employee_number; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>NIK</td>
										<td>: <?php echo $preview->id_card; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Nama Karyawan</td>
										<td>: <?php echo $preview->full_name; ?></td>
									</tr>
									<tr>
										<td>&nbsp</td>
										<td>Jumlah Cuti</td>
										<td>: <?php echo $preview->vacation; ?></td>
									</tr>
								</thead>
							</table>
							<?php else: ?>
								<div class="text-center text-danger" style="font-size:20px;">
									Cuti Belum diatur
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="window.close();">Tutup</button>
       	</div>
	</body>
</html>
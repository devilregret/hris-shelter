<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('monitoring/list/'.$type); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Monitoring Kontrak Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-3">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
						<div class="col-sm-2 submission" hidden="true">
							<select class="form-control select2" id="template" required>
								<?php foreach ($list_template as $item) :  ?>
								<option value="<?php echo $item->id;?>"><?php echo
									$item->title; ?></option>
								<?php endforeach; ?>
							</select>
							<!-- <label class="btn btn-secondary submission" hidden="true">Durasi Kontrak :</label> -->
						</div>
						<div class="col-sm-2">
							<input type="text" class="form-control float-right submission" id="reservationtime" required hidden="true">
						</div>
						<div class="col-sm-1">
							<input type="text" class="form-control float-right submission" id="longterm" readonly="" hidden="true">
						</div>
						<div class="col-sm-4 submission" hidden="true">
							<a onclick="preview_contract(this)" class="btn btn-info" data-href="<?php echo base_url('employee_contract/preview_contract'); ?>" style="color:white;width:110px;"><i class="fas fa-eye"></i> Lihat</a>
							<a class="btn btn-default pdf" style="width:110px;"><i class="fas fa-file-pdf"></i> PDF</a>
							<a class="btn btn-danger nonjob" style="color:white;width:110px;"><i class="fas fa-ban"></i> NonJob</a>
							<a class="btn btn-primary save" style="color:white;width:110px;"><i class="fas fa-save"></i> Simpan</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="post" action="<?php echo base_url("monitoring/import"); ?>" enctype="multipart/form-data">
						<div class="card card-warning card-outline">
							<div class="input-group col-sm-12 mt-2">
								<div class="custom-file col-sm-9">
									<input type="file" class="custom-file-input" name="file" accept=".xlsx" required>
									<label class="custom-file-label">Pilih file</label>
								</div>
								<div class="input-group-append">
									<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>    
								</div>
							</div>
							<br>
						</div>
					</form>
				</div>
				<div class="card-body">
					<form method="POST" id="contract" action="<?php echo base_url('monitoring/adendum'); ?>">
					<input type="text" name="template_id" id="template_id" value="" hidden="">
					<input type="text" name="duration" id="duration" value="" hidden="">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th id='id_card'>Nomor KTP</th>
									<th id='employee_number'>ID Karyawan</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='company_name'>Unit Kontrak</th>
									<th id='site_name'>Site Bisnis</th>
									<th id='business_name'>Unit Bisnis</th>
									<th id='position'>Jabatan</th>
									<th id='warning_letter'>Peringatan</th>
									<th id='contract_end'>Akhir Kontrak</th>
									<th id='ro'>RO</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("monitoring/list_ajax/".$type);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'id_card'},
				{ data: 'employee_number' },
				{ data: 'full_name' },
				{ data: 'company_name' },
				{ data: 'site_name' },
				{ data: 'business_name' },
				{ data: 'position' },
				{ data: 'warning_letter' },
				{ data: 'contract_end' },
				{ data: 'ro' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[3, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<11){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$(".export" ).click(function() {
			var url = "<?php echo base_url('employee_contract/export?type='.$type); ?>" + "&employee_number="+$("#s_employee_number").val()+"&full_name="+$("#s_full_name").val()+"&company_name="+$("#s_company_name").val()+"&site_name="+$("#s_site_name").val()+"&position="+$("#s_position").val()+"&warning_letter="+$("#s_warning_letter").val()+"&contract_end="+$("#s_contract_end").val()+"&status=kontrak_karyawan"+"&id_card="+$("#s_id_card").val()+$("#s_business_name").val();
			window.location = url;
		
		});
		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});
		$("input:checkbox").change(function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});
		$( "#site_id" ).change(function() {
			$(".site_id").val($(this).val());
		});
		$( ".btn.submission" ).click(function() {
			$( "#submission" ).submit();
		});

		$('#reservationtime').on( 'keyup change', function () {
			var range = $(this).val();

			var date1 = new Date(range.substring(3, 5)+"/"+range.substring(0, 2)+"/"+range.substring(6, 10)); 
			var date2 = new Date(range.substring(16, 18)+"/"+range.substring(13, 15)+"/"+range.substring(19, 23)); 

			var Difference_In_Time = date2.getTime() - date1.getTime(); 
			var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
			$('#longterm').val(Difference_In_Days+" Hari");
		});

		$(".save" ).click(function() {
			$( "#template_id").val($( "#template").val());
			$( "#duration").val($( "#reservationtime").val());
			$( "#contract" ).submit();
		});

		$(".pdf" ).click(function() {
			$( "#template_id").val($( "#template").val());
			$( "#duration").val($( "#reservationtime").val());
			$( "#contract" ).attr('action', "<?php echo base_url('employee_contract/pdf_contract/'.$type); ?>").submit();
		});

		$(".nonjob" ).click(function() {
			$( "#contract" ).attr('action', "<?php echo base_url('monitoring/nonjob/'.$type); ?>").submit();
		});
	});

	function preview_contract(el)
	{
		var id = '';
		var i = 0;
		$("input:checkbox.check-id:checked").each(function(){
			if(i > 0){
				id = id + ','; 
			}
			id = id +''+$(this).val();
			i++;
		});
		var template = $('#template').val();
		var duration =  $('#reservationtime').val().replace(" ", "").replace(" ", "");
		const url = $(el).attr('data-href')+'?id='+id+'&duration='+duration+'&template='+template;
	    const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
	    const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

	    const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	    const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	    const systemZoom = width / window.screen.availWidth;
	    const left = (width - 1000) / 2 / systemZoom + dualScreenLeft
	    const top = (height - 800) / 2 / systemZoom + dualScreenTop
	    const newWindow = window.open(url, 'preview', 
	      `
	      scrollbars=yes,
	      width=${1000 / systemZoom}, 
	      height=${800 / systemZoom}, 
	      top=${top}, 
	      left=${left}
	      `
	    )
	  return false;
	};
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6"></div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-list"></i>&nbsp;Peringatan <?php echo $employee->full_name; ?></h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<a href="<?php echo base_url("monitoring/form_warning/".$type."/".$employee->id); ?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Tambah Peringatan</a>
					</div>
				</div>
				<div class="card-body">
					<div class="row form-group contract mt-3">
						<div class="row col-12">
							<label class="col-sm-3">Nama Karyawan</label>
							<label class="col-sm-9">:&nbsp;<?php echo $employee->full_name; ?> </label>
						</div>
						<div class="row col-12">
							<label class="col-sm-3">ID Karyawan </label>
							<label class="col-sm-9">:&nbsp;<?php echo $employee->id_card; ?> </label>
						</div>
						<div class="row col-12">
							<label class="col-sm-3">Alamat Karyawan </label>
							<label class="col-sm-9">:<?php echo $employee->address_card; ?> </label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Surat Peringatan</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4" method="post" action="<?php echo base_url("monitoring/form_warning/".$type."/".$employee_id); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Template Surat Peringatan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="hidden" class="form-control" name="employee_id" value="<?php echo $employee_id; ?>">
									<select class="form-control select2" name="warning_template_id" id="warning_template_id" required>
										<option value=""> &nbsp;-- PILIH PROVINSI --</option>
										<?php foreach ($list_warning as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $warning_template_id): echo "selected"; endif;?>><?php echo $item->title; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Catatan Peringatan<span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Catatan Peringatan" id="warning_note" name="warning_note" value="<?php echo $warning_note; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Sanksi Peringatan<span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Sanksi" id="penalty" name="penalty" value="<?php echo $penalty; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tanggal Peringatan <span class="text-danger"></span></label>
								<div class="col-sm-2">
									<input type="date" name="warning_date" class="form-control datepicker" id="warning_date" value="<?php echo $warning_date; ?>">
								</div>
								<div class="col-sm-7"></div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Tipe Kontrak <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" id="warning_type" name="warning_type" required>
										<option value="SP1">SP1</option>
										<option value="SP2">SP2</option>
										<option value="SP3">SP3</option>
									</select>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" name="action" = valuse="save" class="btn btn-primary btn-form">Simpan</button>
								&nbsp;&nbsp;&nbsp;
								<a onclick="preview_warning(this)" class="btn btn-info btn-form"  style="cursor:pointer;" data-href="<?php echo base_url('monitoring/preview_warning/'.$employee_id); ?>">Preview</a>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
<script type="text/javascript">
	function preview_warning(el)
	{;
		var warning_type 	= $('#warning_type').val();
		var warning_date 	= $('#warning_date').val();
		var penalty 		=  $('#penalty').val();
		var warning_note 	=  $('#warning_note').val();
		var warning_template_id 	=  $('#warning_template_id').val();
		if(warning_type == "" || warning_date == "" || penalty == "" || warning_note == "" || warning_template_id == ""){
			alert ("Silahkan lengkapi form isian terlebih dahulu.")
			return false;
		}
		const url = $(el).attr('data-href')+'?warning_type='+warning_type+'&warning_date='+warning_date+'&penalty='+penalty+'&warning_note='+warning_note+'&template='+warning_template_id;
	    const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
	    const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

	    const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	    const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	    const systemZoom = width / window.screen.availWidth;
	    const left = (width - 1000) / 2 / systemZoom + dualScreenLeft
	    const top = (height - 800) / 2 / systemZoom + dualScreenTop
	    const newWindow = window.open(url, 'preview', 
	      `
	      scrollbars=yes,
	      width=${1000 / systemZoom}, 
	      height=${800 / systemZoom}, 
	      top=${top}, 
	      left=${left}
	      `
	    )
	  return false;
	};
</script>
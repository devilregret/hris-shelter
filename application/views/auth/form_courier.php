<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<title><?php echo $_TITLE_; ?></title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<?php echo asset_css("css/adminlte.min.css"); ?>
    	<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<link rel="shortcut icon" href="<?php echo asset_img('favicon.ico'); ?>" type="image/x-icon">
		<link rel="icon" href="<?php echo asset_img('favicon.ico'); ?>" type="image/x-icon">

		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/select2/css/select2.min.css"); ?>
		<?php echo asset_css("plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css"); ?>
		<?php echo asset_js("plugins/jquery/jquery.min.js"); ?>
	</head>
	<body class="hold-transition sidebar-mini">
		<div class="wrapper">
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-md-12">
							<h1>
								<a href="<?php echo base_url(); ?>"><b style="font-family: Bookman Old Style;">SHELTER</b></a>
							</h1>
						</div>
					</div>
				</div>
			</section>
			<section class="content">
				<div class="container-fluid">
					<div class="card card-primary card-outline col-12">
						<div class="card-header">
							<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Pendaftaran Kurir</h3>
						</div>
						<div class="card-header">
							&nbsp;
							<?php if (isset($message)): echo $message; endif; ?>
							<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
						</div>
						<div class="card-body">
							<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("auth/form_courier"); ?>">
								<div class="form-group row">
									<label class="col-sm-3 col-form-label">Nama Lengkap <span class="text-danger">*</span></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" placeholder="Nama Lengkap" name="full_name" value="<?php echo $full_name; ?>" required>
									</div>
								</div>
								<div class="form-group row">
									<label for="name" class="col-sm-3 col-form-label">Nomor KTP <span class="text-danger">*</span></label>
									<div class="col-sm-9">
										<input type="text" class="form-control numeric" placeholder="Nomor KTP" name="id_card" maxlength="16" pattern=".{16,16}"  title="16 number" value="<?php echo $id_card; ?>" required>
									</div>
								</div>
								<div class="form-group row">
									<label for="name" class="col-sm-3 col-form-label">Nomor SIM <span class="text-danger">*</span></label>
									<div class="col-sm-9">
										<input type="text" class="form-control numeric" placeholder="Nomor SIM" name="driver_license" value="<?php echo $driver_license; ?>" required>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label">Nomor Whatsapp <span class="text-danger">*</span></label>
									<div class="col-sm-9">
										<input type="text" class="form-control numeric" placeholder="081111111111" name="phone_number" value="<?php echo $phone_number; ?>" required>
									</div>
								</div>
								<div class="form-group row">
									<label for="name" class="col-sm-3 col-form-label">Alamat Domisili <span class="text-danger">*</span></label>
									<div class="col-sm-9">
										<textarea class="form-control" name="address" rows="3" placeholder="Jl. Semampir Sel. V A No.18" required><?php echo $address; ?></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label for="name" class="col-sm-3 col-form-label">Kota Domisili <span class="text-danger">*</span></label>
									<div class="col-sm-9">
										<select class="form-control select2" name="city_id" required>
											<option value=""> &nbsp;-- PILIH KOTA --</option>
											<?php foreach ($list_city as $item) :  ?>
											<option value="<?php echo $item->id;?>" <?php if($item->id === $city_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<hr>
								<div class="form-group row  float-right">
									<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
									<button type="submit" name="submit" class="btn btn-primary btn-form">Simpan</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</section>
		</div>
		<style type="text/css">
			.select2-selection{
				border: 1px solid #ced4da !important;
				height: 37px !important;
			}
		</style>
		<?php echo asset_js("plugins/bootstrap/js/bootstrap.bundle.min.js"); ?>
		<?php echo asset_js("plugins/select2/js/select2.full.min.js"); ?>
		<?php echo asset_js("plugins/moment/moment.min.js"); ?>
		<?php echo asset_js("plugins/daterangepicker/daterangepicker.js"); ?>
		<?php echo asset_js("plugins/jquery/jquery.mask.min.js"); ?>
		<?php echo asset_js("plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"); ?>
		<?php echo asset_js("plugins/bs-custom-file-input/bs-custom-file-input.min.js"); ?>
		<?php echo asset_js("plugins/datatables/jquery.dataTables.min.js"); ?>
		<?php echo asset_js("plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"); ?>
		<?php echo asset_js("plugins/datatables-responsive/js/dataTables.responsive.min.js"); ?>
		<?php echo asset_js("plugins/datatables-responsive/js/responsive.bootstrap4.min.js"); ?>
		<?php echo asset_js("js/adminlte.js"); ?>
		<?php echo asset_js("js/demo.js"); ?>
		<?php echo asset_js("js/main.js"); ?>
	</body>
</html>
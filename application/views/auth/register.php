<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<title><?php echo $_TITLE_; ?></title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<?php echo asset_css("css/adminlte.min.css"); ?>
    	<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<link rel="shortcut icon" href="<?php echo asset_img('favicon.ico'); ?>" type="image/x-icon">
		<link rel="icon" href="<?php echo asset_img('favicon.ico'); ?>" type="image/x-icon">
		
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/select2/css/select2.min.css"); ?>
		<?php echo asset_css("plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css"); ?>
		<?php echo asset_js("plugins/jquery/jquery.min.js"); ?>

		<?php echo asset_css("plugins/daterangepicker/daterangepicker.css"); ?>
		<?php echo asset_css("plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"); ?>
	</head>
	<body class="hold-transition sidebar-mini">
		<div class="wrapper">
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-md-12">
							<h1>
								<a href="https://shelter.co.id"><b style="font-family: Bookman Old Style;">SHELTER</b></a>
							</h1>
						</div>
					</div>
				</div>
			</section>
			<section class="content">
				<div class="container-fluid">
					<div class="row">
					 	<div class="col-md-3">
					 		<div class="card card-primary card-outline">
					 			<div class="card-body box-profile">
					 				<?php if(isset($warning)): echo $warning; endif; ?>
									<form action="<?php echo base_url('auth/register')?>" method="post">
										<div class="input-group mb-3">
											<input type="text" class="form-control" name="full_name" placeholder="Nama Lengkap" value="<?php echo $full_name; ?>"; required>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-user-tie"></span>
												</div>
											</div>
										</div>
										<div class="input-group mb-3">
											<input type="text" maxlength="16" pattern=".{16,16}"  title="16 number" class="form-control numeric" placeholder="Nomor Induk Kependudukan (KTP)" name="id_card" value="<?php echo $id_card; ?>" required>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-id-card"></span>
												</div>
											</div>
										</div>
										<!-- <div class="input-group mb-3">
											<input type="text" class="form-control" name="username" placeholder="Username" value="< ?php echo $username; ?>"; required>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-user"></span>
												</div>
											</div>
										</div> -->
										<div class="input-group mb-3">
											<input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo $email; ?>"; >
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-envelope"></span>
												</div>
											</div>
										</div>
										<div class="input-group mb-3">
											<input type="password" class="form-control password" name="password" placeholder="Password" required>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-key"></span>
												</div>
											</div>
										</div>
										<div class="input-group mb-3">
											<input type="password" class="form-control password" name="repassword" placeholder="Ulangi password" required>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-key"></span>
												</div>
											</div>
										</div>
										<div class="input-group form-check mb-3">
											<input class="form-check-input show-password" type="checkbox" >
											<label class="form-check-label">Show password</label>
										</div>
										<div class="input-group mb-3">
											<input type="text" maxlength="14" pattern=".{10,14}"  title="10 to 14 number" class="form-control numeric" name="phone_number"  placeholder="Nomor HP/WA contoh: 081xxxxxxxxx" value="<?php echo $phone_number; ?>" required>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-mobile-alt"></span>
												</div>
											</div>
										</div>
										<div class="input-group mb-3">
											<select class="form-control select2" name="gender" required>
												<option value="Laki-laki" <?php if('Laki-laki' === $gender): echo "selected"; endif;?>>Laki-laki</option>
												<option value="Perempuan" <?php if('Perempuan' === $gender): echo "selected"; endif;?>>Perempuan</option>
											</select>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-venus-mars"></span>
												</div>
											</div>
										</div>
										<div class="input-group mb-3">
											<div class="input-group date" id="date_birth" data-target-input="nearest">
												<input type="text" class="form-control datetimepicker-input" data-target="#date_birth" name='date_birth' value="<?php echo $date_birth; ?>" placeholder="Tanggal Lahir"  data-toggle="datetimepicker"/>
												<div class="input-group-append" data-target="#date_birth" data-toggle="datetimepicker">
													<div class="input-group-text"><i class="fa fa-calendar"></i></div>
												</div>
											</div>
											<!-- <input type="date" name="date_birth" id="date_birth" class="form-control datepicker" value="<?php echo $date_birth; ?>" required>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-birthday-cake"></span>
												</div>
											</div> -->
										</div>
										<div class="input-group mb-3">
											<select class="form-control select2" name="education_level" required>
												<option value="SD" <?php if('SD' === $education_level): echo "selected"; endif;?>>SD Sederajat</option>
												<option value="SMP" <?php if('SMP' === $education_level): echo "selected"; endif;?>>SMP Sederajat</option>
												<option value="SMA" <?php if('SMA' === $education_level): echo "selected"; endif;?>>SMA Sederajat</option>
												<option value="D1" <?php if('D1' === $education_level): echo "selected"; endif;?>>D1</option>
												<option value="D2" <?php if('D2' === $education_level): echo "selected"; endif;?>>D2</option>
												<option value="D3" <?php if('D3' === $education_level): echo "selected"; endif;?>>D3</option>
												<option value="D4" <?php if('D4' === $education_level): echo "selected"; endif;?>>D4</option>
												<option value="S1" <?php if('S1' === $education_level): echo "selected"; endif;?>>S1</option>
												<option value="S2" <?php if('S2' === $education_level): echo "selected"; endif;?>>S2</option>
												<option value="S3" <?php if('S3' === $education_level): echo "selected"; endif;?>>S3</option>
											</select>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-user-graduate"></span>
												</div>
											</div>
										</div>
										<div class="input-group mb-3">
											<input type="text" class="form-control" placeholder="Jurusan, Contoh: Mesin/Perkantoran/Perhotelan" name="education_majors" value="<?php echo $education_majors; ?>">
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-user-md"></span>
												</div>
											</div>
										</div>
										<div class="input-group mb-3">
											<input type="text" class="form-control" placeholder="Pengalaman Kerja" name="experience" value="<?php echo $experience; ?>" required>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-industry"></span>
												</div>
											</div>
										</div>
										<div class="input-group mb-3">
											<select class="form-control select2" name="preference_job1" required>
												<option value=""> &nbsp;-- PILIH POSISI YANG DILAMAR --</option>
												<?php foreach ($list_job as $item) :  ?>
												<option value="<?php echo $item->id;?>" <?php if($item->id === $preference_job1 ): echo "selected"; endif;?>><?php echo $item->name; ?></option>
												<?php endforeach; ?>
											</select>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-user-astronaut"></span>
												</div>
											</div>
										</div>
										<div class="input-group mb-3">
											<input type="text" class="form-control" placeholder="Perusahaan yang dilamar" name="site_preference" value="<?php echo $site_preference; ?>">
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-building"></span>
												</div>
											</div>
										</div>
										<!-- <div class="input-group mb-3">
											<select class="form-control select2" name="site_id" required>
												<option value=""> &nbsp;-- PILIH PERUSAHAAN YANG DILAMAR --</option>
												< ?php foreach ($list_site as $item) :  ?>
												<option value="< ?php echo $item->id;?>" < ?php if($item->id === $site_id ): echo "selected"; endif;?>>< ?php echo $item->name.' - '.$item->city_name; ?></option>
												< ?php endforeach; ?>
											</select>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-building"></span>
												</div>
											</div>
										</div> -->
										<div class="input-group mb-3">
											<select class="form-control select2" name="city_domisili_id" required>
												<option value=""> &nbsp;-- PILIH KOTA DOMISILI --</option>
												<?php foreach ($list_city as $item) :  ?>
												<option value="<?php echo $item->id;?>" <?php if($item->id === $city_domisili_id): echo "selected"; endif;?>><?php echo $item->name." - ". $item->province_name; ?></option>
												<?php endforeach; ?>
											</select>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-map-marked-alt"></span>
												</div>
											</div>
										</div><div class="input-group mb-3">
											<select class="form-control select2" name="destination_city" required>
												<option value=""> &nbsp;-- PILIH KOTA YANG DITUJU --</option>
												<?php foreach ($list_city as $item) :  ?>
												<option value="<?php echo $item->id;?>" <?php if($item->id === $destination_city): echo "selected"; endif;?>><?php echo $item->name." - ". $item->province_name; ?></option>
												<?php endforeach; ?>
											</select>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-map-marked-alt"></span>
												</div>
											</div>
										</div><div class="input-group mb-3">
											<select class="form-control select2" name="recommendation" required>
												<option value=""> &nbsp;-- PILIH SUMBER INFORMASI --</option> 
												<option value="WEBSITE SHELTER" <?php if('WEBSITE SHELTER' === $recommendation): echo "selected"; endif;?>>WEBSITE SHELTER</option>
												<option value="FACEBOOK SHELTER" <?php if('FACEBOOK SHELTER' === $recommendation): echo "selected"; endif;?>>FACEBOOK SHELTER</option>
												<option value="INSTAGRAM SHELTER" <?php if('INSTAGRAM SHELTER' === $recommendation): echo "selected"; endif;?>>INSTAGRAM SHELTER</option>
												<option value="TIKTOK SHELTER" <?php if('TIKTOK SHELTER' === $recommendation): echo "selected"; endif;?>>TIKTOK SHELTER</option>
												<option value="LINKEDIN" <?php if('LINKEDIN' === $recommendation): echo "selected"; endif;?>>LINKEDIN</option>
												<option value="JOB PORTAL LAIN" <?php if('JOB PORTAL LAIN' === $recommendation): echo "selected"; endif;?>>JOB PORTAL LAIN</option>
												<option value="REFERENSI TEMAN, SAUDARA" <?php if('REFERENSI TEMAN, SAUDARA' === $recommendation): echo "selected"; endif;?>>REFERENSI TEMAN, SAUDARA</option>
												<option value="YOUTUBE" <?php if('YOUTUBE' === $recommendation): echo "selected"; endif;?>>YOUTUBE</option>
												<option value="GOOGLE" <?php if('GOOGLE' === $recommendation): echo "selected"; endif;?>>GOOGLE</option>
												<option value="WA/TELEGRAM" <?php if('WA/TELEGRAM' === $recommendation): echo "selected"; endif;?>>WA/TELEGRAM</option>
											</select>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fa fa-newspaper"></span>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-8">
											</div>
											<div class="col-sm-4">
												<button type="submit" class="btn btn-primary btn-block btn-form">Daftar</button>
											</div>
										</div>
									</form>
									<p class="mb-1 mt-4">
										<a href="<?php echo base_url('auth/login')?>"><b>Login</b></a>
									</p>
									<p class="mb-0">
										<a href="<?php echo base_url('auth/reset')?>"><b>Lupa Password</b></a>
									</p>
									<p>
										&nbsp;
									</p>
									<div>
										<img src="<?php echo asset_img('header-login.png'); ?>" alt="Logo Shelter" width="100%">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-9">
							
							<?php if($information): ?>
							<div class="card card-danger">
								<div class="card-header">
									<h5 class="card-title m-0"><b>Informasi</b></h5>
								</div>
								<?php foreach ($information AS $item): ?>
								<div class="card">
									<div class="card-header  bg-light color-palette">
										<h3 class="card-title"><?php echo $item->title; ?></h3>
									</div>
									<div class="card-body">
										<?php echo $item->content; ?>
									</div>
									<div class="card-footer">
										<?php if($item->branch_name): echo $item->branch_name; endif;?> &nbsp;
										<?php if($item->id == 2 ): ?>
										<a href='<?php echo base_url('auth/form_courier'); ?>' style='position:absolute; right: 0; bottom: 4px;width: 200px;' class='btn btn-primary'>Lamar Sekarang</a>
										<?php elseif($item->id == 3): ?>
										<a href='<?php echo base_url('auth/form_casual'); ?>' style='position:absolute; right: 0; bottom: 4px;width: 200px;' class='btn btn-primary'>Lamar Sekarang</a>
										<?php endif; ?>
									</div>
								</div>
								<?php endforeach; ?>	
							</div>	
							<?php endif; ?>
							<div class="card card-info">
								<div class="card-header">
									<h5 class="card-title m-0"><b>Lowongan</b></h5>
									<p>&nbsp;</p>
									<form action="<?php echo base_url('auth/login')?>" method="get">
										<div class="input-group">
											<select class="form-control select2" name="province" required>
												<option value=""> &nbsp;-- PILIH PROVINSI --</option>
												<?php foreach ($list_province as $item) :  ?>
													<option value="<?php echo $item->id;?>" <?php if($item->id === $province_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
												<?php endforeach; ?>
											</select>
											<div class="input-group-append">
												<button type="submit" class="btn btn-danger btn-flat">Cari</button>
											</div>
										</div>
									</form>
								</div>
								<?php 
								foreach ($vacancy AS $item): 
									$date = get_day($item->day).", ".substr($item->date, 0, 2)." ".get_month(substr($item->date, 3,2))." ".substr($item->date, 6,4);
								?>
								<div class="card">
									<div class="card-header bg-light color-palette">
										<a onclick="show(<?php echo $item->id; ?>)" style="cursor: pointer;">
											<h3 class="card-title"><strong><?php echo $item->province_name; ?></strong>, <?php echo $item->title; ?> (Paling Lambat <strong> <?php echo $date; ?></strong>)</h3>
											<span class="show-detail"><i class="fas fa-arrow-circle-down"></i></span>
										</a>
									</div>
									<div class="content-detail detail-<?php echo $item->id; ?>" hidden="true">
										<div class="card-body">
											<?php echo $item->content; ?>
										</div>
										<?php if($item->flyer != ''): ?>
											<img src="<?php echo $item->flyer; ?>" width="100%" alt="Lowongan" />
										<?php endif; ?>
										<div class="card-footer">
											<?php if($item->branch_name): echo $item->branch_name; endif;?>
											<a href='<?php echo base_url('auth/apply/register'); ?>' style='position:absolute; right: 0; bottom: 4px;width: 200px;' class='btn btn-primary'>Lamar Sekarang</a>
										</div>
									</div>
								</div>
								<?php endforeach; ?>	
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<a href="http://wa.me/6282228098110" target="_blank"><img src="<?php echo asset_img('whatsapp.png'); ?>" style="bottom:20px;right:20px;position:fixed;padding-left: 20px;" width="250px"/></a>
		<style type="text/css">
			.select2-selection{
				border: 1px solid #ced4da !important;
				height: 37px !important;
			}
			.select2{
				width: 89% !important;
			}
			.show-detail{
				float: right;
				font-size: 20px;
				color: #007bff;
				padding-right: 20px;
			}
		</style>
		<script type="text/javascript">
			$("#date_birth").change(function() {
				if(calculate_age(this.value) < 18){
					alert("Maaf Usia Anda masih kurang dari 18 Tahun");
					$(".btn-form").prop('disabled', true);
				}else{
					$(".btn-form").prop('disabled', false);
				}
			});
			
			function calculate_age(dob) { 
				var today = new Date();
			    var birth_date = new Date(dob);
			    var age = today.getFullYear() - birth_date.getFullYear();
			    var m = today.getMonth() - birth_date.getMonth();
			    if (m < 0 || (m === 0 && today.getDate() < birth_date.getDate())) {
			        age--;
			    }
			    return age;
			}
			
			function show(id){
				$('.content-detail').attr('hidden', true);
				$('.detail-'+id).attr('hidden', false);
			}


			$(document).ready(function(){		
				$('.show-password').click(function(){
					if($(this).is(':checked')){
						$('.password').attr('type','text');
					}else{
						$('.password').attr('type','password');
					}
				});
				$('#date_birth').datetimepicker({
					format: 'DD/MM/YYYY',
					pickTime: false  
				});
			});
		</script>
		<?php echo asset_js("plugins/bootstrap/js/bootstrap.bundle.min.js"); ?>
		<?php echo asset_js("plugins/select2/js/select2.full.min.js"); ?>
		<?php echo asset_js("plugins/moment/moment.min.js"); ?>
		<?php echo asset_js("plugins/daterangepicker/daterangepicker.js"); ?>
		<?php echo asset_js("plugins/jquery/jquery.mask.min.js"); ?>
		<?php echo asset_js("plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"); ?>
		<?php echo asset_js("plugins/bs-custom-file-input/bs-custom-file-input.min.js"); ?>
		<?php echo asset_js("plugins/datatables/jquery.dataTables.min.js"); ?>
		<?php echo asset_js("plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"); ?>
		<?php echo asset_js("plugins/datatables-responsive/js/dataTables.responsive.min.js"); ?>
		<?php echo asset_js("plugins/datatables-responsive/js/responsive.bootstrap4.min.js"); ?>
		<?php echo asset_js("js/adminlte.js"); ?>
		<?php echo asset_js("js/demo.js"); ?>
		<?php echo asset_js("js/main.js"); ?>
	</body>
</html>
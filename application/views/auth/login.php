<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<title><?php echo $_TITLE_; ?></title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<?php echo asset_css("css/adminlte.min.css"); ?>
    	<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<link rel="shortcut icon" href="<?php echo asset_img('favicon.ico'); ?>" type="image/x-icon">
		<link rel="icon" href="<?php echo asset_img('favicon.ico'); ?>" type="image/x-icon">
		
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/select2/css/select2.min.css"); ?>
		<?php echo asset_css("plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css"); ?>
		<?php echo asset_js("plugins/jquery/jquery.min.js"); ?>
	</head>
	<body class="hold-transition sidebar-mini">
		<div class="wrapper">
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-md-12">
							<h1>
								<a href="https://shelter.co.id"><b style="font-family: Bookman Old Style;">SHELTER</b></a>
							</h1>
						</div>
					</div>
				</div>
			</section>
			<section class="content">
				<div class="container-fluid">
					<div class="row">
					 	<div class="col-md-3">
					 		<div class="card card-primary card-outline">
					 			<div class="card-body box-profile">
					 				<div class="text-center">
					 					<?php if(isset($warning)): echo $warning; endif; ?>
										<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
									</div>
									<form action="<?php echo base_url('auth/login')?>" method="post">
										<div class="input-group mb-3">
											<input type="text" class="form-control" name="username" placeholder="NIK/email" value="<?php echo $username; ?>" required>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-envelope"></span>
												</div>
											</div>
										</div>
										<div class="input-group mb-3">
											<input type="password" name="password" class="form-control password" placeholder="Password" required>
											<div class="input-group-append">
												<div class="input-group-text">
													<span class="fas fa-lock"></span>
												</div>
											</div>
										</div>
										<div class="input-group form-check mb-3">
											<input class="form-check-input show-password" type="checkbox" >
											<label class="form-check-label">Show password</label>
										</div>
										<div class="row">
											<div class="col-8">
											</div>
											<div class="col-sm-4">
												<button type="submit" class="btn btn-primary btn-block">Masuk</button>
											</div>
										</div>
									</form>
									<p class="mb-1 mt-4">
										<a href="<?php echo base_url('auth/register')?>"><b>Daftar</b></a>
									</p>
									<p class="mb-0">
										<a href="<?php echo base_url('auth/reset')?>"><b>Lupa Password</b></a>
									</p>
									<p>
										&nbsp;
									</p>
									<div>
										<img src="<?php echo asset_img('header-login.png'); ?>" alt="Logo Shelter" width="100%">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-9">
							<?php if($information): ?>
							<div class="card card-danger">
								<div class="card-header">
									<h5 class="card-title m-0"><b>Informasi</b></h5>
								</div>
								<?php foreach ($information AS $item): ?>
								<div class="card">
									<div class="card-header  bg-light color-palette">
										<h3 class="card-title"><?php echo $item->title; ?></h3>
									</div>
									<div class="card-body">
										<?php echo $item->content; ?>
									</div>
									<div class="card-footer">
										<?php if($item->branch_name): echo $item->branch_name; endif;?> &nbsp;
										<?php if($item->id == 2 ): ?>
										<a href='<?php echo base_url('auth/form_courier'); ?>' style='position:absolute; right: 0; bottom: 4px;width: 200px;' class='btn btn-primary'>Lamar Sekarang</a>
										<?php elseif($item->id == 3): ?>
										<a href='<?php echo base_url('auth/form_casual'); ?>' style='position:absolute; right: 0; bottom: 4px;width: 200px;' class='btn btn-primary'>Lamar Sekarang</a>
										<?php endif; ?>
									</div>
								</div>
								<?php endforeach; ?>	
							</div>	
							<?php endif; ?>
							<div class="card card-info">
								<div class="card-header">
									<h5 class="card-title m-0"><b>Lowongan</b></h5>
									<p>&nbsp;</p>
									<form action="<?php echo base_url('auth/login')?>" method="get" class="col-md-12">
										<div class="input-group">
											<select class="form-control select2" name="province" required>
												<option value=""> &nbsp;-- PILIH PROVINSI --</option>
												<?php foreach ($list_province as $item) :  ?>
												<option value="<?php echo $item->id;?>" <?php if($item->id === $province_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
												<?php endforeach; ?>
											</select>
											<div class="input-group-append">
												<button type="submit" class="btn btn-danger btn-flat">Cari</button>
											</div>
										</div>
									</form>
								</div>
								<?php 
								foreach ($vacancy AS $item): 
									$date = get_day($item->day).", ".substr($item->date, 0, 2)." ".get_month(substr($item->date, 3,2))." ".substr($item->date, 6,4);
								?>
								<div class="card">
									<div class="card-header bg-light color-palette">
										<a onclick="show(<?php echo $item->id; ?>)" style="cursor: pointer;">
											<h3 class="card-title"><strong><?php echo $item->province_name; ?></strong>, <?php echo $item->title; ?> (Paling Lambat <strong> <?php echo $date; ?></strong>)</h3>
											<span class="show-detail"><i class="fas fa-arrow-circle-down"></i></span>
										</a>
									</div>
									<div class="content-detail detail-<?php echo $item->id; ?>" hidden="true">
										<div class="card-body">
											<?php echo $item->content; ?>
										</div>
										<?php if($item->flyer != ''): ?>
											<img src="<?php echo $item->flyer; ?>" width="100%" alt="Lowongan" />
										<?php endif; ?>
										<div class="card-footer">
											<?php if($item->branch_name): echo $item->branch_name; endif;?>
											<a href='<?php echo base_url('auth/apply/login'); ?>' style='position:absolute; right: 0; bottom: 4px;width: 200px;' class='btn btn-primary'>Lamar Sekarang</a>
										</div>
									</div>
								</div>
								<?php endforeach; ?>	
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<a href="http://wa.me/6282228098110" target="_blank"><img src="<?php echo asset_img('whatsapp.png'); ?>" style="bottom:20px;right:20px;position:fixed;padding-left: 20px;" width="250px"/></a>
		<style type="text/css">
			.show-detail{
				float: right;
				font-size: 20px;
				color: #007bff;
				padding-right: 20px;
			}
		</style>
		<script type="text/javascript">
			function show(id){
				if($('.detail-'+id).attr('hidden')){
					$('.detail-'+id).attr('hidden', false);
				}else{
					$('.detail-'+id).attr('hidden', true);
				}
			}

			$(document).ready(function(){		
				$('.show-password').click(function(){
					if($(this).is(':checked')){
						$('.password').attr('type','text');
					}else{
						$('.password').attr('type','password');
					}
				});
			});
		</script>
		<?php echo asset_js("plugins/jquery/jquery.min.js"); ?>
		<?php echo asset_js("plugins/bootstrap/js/bootstrap.bundle.min.js"); ?>
		<?php echo asset_js("js/adminlte.min.js"); ?>
	</body>
</html>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('payroll_allowance/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-handshake"></i>&nbsp;Site THR</h3>
				</div>
				<div class="card-body">
					<form method="POST" action="<?php echo base_url('payroll_allowance/site'); ?>">
						<div class="row">
							<label for="name" class="col-sm-2 col-form-label">Pilih Site :</label>
							<div class="col-sm-8" >
								<select class="form-control select2" name="site_id">
									<option value=""> &nbsp; </option>
									<?php foreach ($list_site as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id == $site_id): echo 'selected'; endif;?>><?php echo
										$item->name.' - '.$item->company_code.' - '.$item->branch_name; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-sm-2" >
								<button type="submit" class="btn btn-primary btn-block">Simpan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<?php if($site_id): ?>
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-cogs"></i>&nbsp;Pengaturan Import THR</h3>
				</div>
				<div class="card-body">
					<form method="post" action="<?php echo base_url("payroll_allowance/import"); ?>" enctype="multipart/form-data">
						<div class="input-group col-sm-12 mt-2">
							<label for="name" class="col-sm-2 col-form-label">Nomor Baris Awal THR <span class="text-danger">*</span></label>
							<div class="col-sm-4">
								<input type="hidden" class="form-control" name="site_id" value="<?php echo $site->site_id?>">
								<input type="number" class="form-control" name="row_start" placeholder="2" value="<?php echo $site->row_start?>" required>
							</div>
							<label for="name" class="col-sm-2 col-form-label">Nomor Baris Akhir THR <span class="text-danger">*</span></label>
							<div class="col-sm-4">
								<input type="number" class="form-control" name="row_end" placeholder="15" value="<?php echo $site->row_end?>" required>
							</div>
						</div>
						<div class="input-group col-sm-12 mt-2">
							<label for="name" class="col-sm-2 col-form-label">Nomor Baris Nama Rincian <span class="text-danger">*</span></label>
							<div class="col-sm-4">
								<input type="number" class="form-control" name="row_title" placeholder="1" value="<?php echo $site->row_title?>" required>
							</div>
							<label for="name" class="col-sm-2 col-form-label">Nama Kolom ID Karyawan <span class="text-danger">*</span></label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="column_employee" placeholder="B" value="<?php echo $site->column_employee?>" style="text-transform:uppercase" required>
							</div>
						</div>
						<div class="input-group col-sm-12 mt-2">
							<label for="text" class="col-sm-2 col-form-label">Nama Kolom Gaji Bersih <span class="text-danger">*</span></label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="column_net" placeholder="O" value="<?php echo $site->column_net?>" style="text-transform:uppercase" required>
							</div>
							<label for="name" class="col-sm-2 col-form-label">Nama Kolom Kehadiran</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="column_attendance" placeholder="J" value="<?php echo $site->column_attendance?>" style="text-transform:uppercase" >
							</div>
						</div>
						<div class="input-group col-sm-12 mt-2">
							<label for="name" class="col-sm-2 col-form-label">Nama Kolom Pendapatan <span class="text-danger">*</span></label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="column_income" placeholder="C,D,E,F" value="<?php echo $site->column_income?>" style="text-transform:uppercase" required>
							</div>
							<label for="name" class="col-sm-2 col-form-label">Nama Kolom Potongan <span class="text-danger">*</span></label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="column_outcome" placeholder="G,H,I,J" value="<?php echo $site->column_outcome?>" style="text-transform:uppercase" required>
							</div>
						</div>
						<div class="input-group col-sm-12 mt-2">
							<label for="name" class="col-sm-2 col-form-label">Periode Awal <span class="text-danger">*</span></label>
							<div class="col-sm-4">
								<input type="date" class="form-control datepicker" name="periode_start" required>
							</div>
							<label for="name" class="col-sm-2 col-form-label">Periode Selesai <span class="text-danger">*</span></label>
							<div class="col-sm-4">
								<input type="date" class="form-control datepicker" name="periode_end" required>
							</div>
						</div>
						<div class="input-group col-sm-12 mt-2">
							<label for="name" class="col-sm-2 col-form-label">Pilih File THR <span class="text-danger">*</span></label>
							<div class="custom-file col-sm-10">
								<input type="file" class="custom-file-input" name="file" accept=".xlsx" required>
								<label class="custom-file-label">Pilih file</label>
							</div>
							<div class="input-group-append">
								<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>    
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;List THR Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-2">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
						<div class="col-sm-10">
							<a href="#" class="btn btn-info submission float-right" style="margin-right:10px;" hidden="true" ><i class="fas fa-check"></i> Proses Pembayaran</a>
							<a href="#" onclick="send_mail(this)" class="btn btn-info email float-right" style="margin-right:10px;" hidden="true" data-href="<?php echo base_url('payroll_allowance/mail'); ?>"><i class="fas fa-paper-plane"></i> Kirim Email</a>
							<a href="#" onclick="preview_payroll(this)" class="btn btn-info preview float-right" style="margin-right:10px;" hidden="true" data-href="<?php echo base_url('payroll_allowance/preview_payroll'); ?>"><i class="fas fa-eye"></i> Preview THR</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
						</thead>
					</table>
					<p>&nbsp;</p>
					<form method="POST" id="submission" action="<?php echo base_url('payroll_allowance/process'); ?>">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th width="50px">No</th>
									<th id='employee_name'>Nama Karyawan</th>
									<th id='employee_email'>Alamat email</th>
									<th id='periode_start'>Periode Mulai</th>
									<th id='periode_start'>Periode Selesai</th>
									<th id='salary'>Jumlah Pendapatan</th>
									<th id='payment'>Status Pembayaran</th>
									<th id='email'>Pengiriman email</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		<?php endif; ?>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("payroll_allowance/list_ajax/");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'employee_name' },
				{ data: 'employee_email' },
				{ data: 'periode_start' },
				{ data: 'periode_end' },
				{ data: 'salary' },
				{ data: 'payment' },
				{ data: 'email' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[4, 'desc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>1 && index<9){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$( ".export" ).click(function() {
			var url = "<?php echo base_url('payroll_allowance/export'); ?>" + "?employee_name="+$("#s_employee_name").val()+"&employee_email="+$("#s_employee_email").val()+"&periode_start="+$("#s_periode_start").val()+"&periode_end="+$("#s_periode_end").val()+"&salary="+$("#s_salary").val()+"&payment="+$("#s_payment").val()+"&email="+$("#s_email").val();
			window.location = url;
		
		});

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".submission").prop('hidden', true);
			$(".email").prop('hidden', true);
			$(".preview").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
				$(".email").prop('hidden', false);
				$(".preview").prop('hidden', false);
    		});
		});
		$("input:checkbox").change(function() {
			$(".submission").prop('hidden', true);
			$(".email").prop('hidden', true);
			$(".preview").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
				$(".email").prop('hidden', false);
				$(".preview").prop('hidden', false);
    		});
		});
		$( ".btn.submission" ).click(function() {
			$( "#submission" ).submit();
		});
	});

	function preview_payroll(el)
	{
		var id = '';
		var i = 0;
		$("input:checkbox.check-id:checked").each(function(){
			if(i > 0){
				id = id + ','; 
			}
			id = id +''+$(this).val();
			i++;
		});
		const url = $(el).attr('data-href')+'?id='+id;
		const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
		const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

		const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

		const systemZoom = width / window.screen.availWidth;
		const left = (width - 1000) / 2 / systemZoom + dualScreenLeft
		const top = (height - 800) / 2 / systemZoom + dualScreenTop
		const newWindow = window.open(url, 'preview',
			`
			scrollbars=yes,
			width=${1000 / systemZoom}, 
			height=${800 / systemZoom}, 
			top=${top}, 
			left=${left}
			`
		)
		return false;
	};

	function send_mail(el)
	{
		var id = '';
		var i = 0;
		$("input:checkbox.check-id:checked").each(function(){
			if(i > 0){
				id = id + ','; 
			}
			id = id +''+$(this).val();
			i++;
		});
		const url = $(el).attr('data-href')+'?id='+id;
		window.location.href = url;
	};
</script>
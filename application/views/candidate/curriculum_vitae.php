<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
		<?php echo asset_js("plugins/jquery/jquery.min.js"); ?>	
		<?php echo asset_js("js/main.js"); ?>	
	</head>
	<body>
		<section class="content print">
			<div class="container-fluid">
				<div class="card card-primary card-outline col-12 mt-2">
					<div class="row">
						<div class="col-12 table-responsive">
							<table class="table">
								<thead>
									<tr>
										<td colspan="3" style="text-align: center;font-size: 20px;"><strong>CURRICULUM VITAE</strong></td>
									</tr>
									<?php if($preview->document_photo !== ''): ?>
									<img id="preview" style="float: left;position:absolute;right:0;top:50px;" src="<?php echo $preview->document_photo; ?>" width="300px" alt="Foto" />
									<?php endif; ?>
									<tr>
										<td colspan="3"><strong>A. DATA DIRI</strong></td>
									</tr>
									<tr>
										<td style="width: 200px;">Nama Lengkap</td>
										<td>: <?php echo $preview->full_name; ?></td>
										<td>&nbsp</td>
									</tr>
									<tr>
										<td>Nomor KTP</td>
										<td>: <?php echo $preview->id_card; ?></td>
										<td>&nbsp</td>
									</tr>
									<tr>
										<td>Masa Berlaku KTP</td>
										<td>: <?php echo $preview->id_card_period; ?></td>
										<td>&nbsp</td>
									</tr>
									<tr>
										<td>Tempat & Tanggal Lahir</td>
										<td>: <?php echo $preview->date_city_birth; ?></td>
										<td>&nbsp</td>
									</tr>
									<tr>
										<td>Provinsi</td>
										<td>: <?php echo $preview->province_domisili; ?></td>
										<td>&nbsp</td>
									</tr>
									<tr>
										<td>Kota</td>
										<td>: <?php echo $preview->city_domisili; ?></td>
										<td>&nbsp</td>
									</tr>
									<tr>
										<td>Alamat</td>
										<td>: <?php echo $preview->address_domisili; ?></td>
										<td>&nbsp</td>
									</tr>
									<!-- <tr>
										<td>Nomor Handphone</td>
										<td>: < ?php echo $preview->phone_number; ?></td>
										<td>&nbsp</td>
									</tr> 
									<tr>
										<td>Email</td>
										<td>: < ?php echo $preview->email; ?></td>
										<td>&nbsp</td>
									</tr> -->
									<tr>
										<td>Agama</td>
										<td>: <?php echo $preview->religion; ?></td>
										<td>&nbsp</td>
									</tr>
									<tr>
										<td>Status </td>
										<td>: <?php echo $preview->marital_status; ?></td>
										<td>&nbsp</td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td>: <?php echo $preview->gender; ?></td>
										<td>&nbsp</td>
									</tr>
									<tr>
										<td>Tinggi & Berat Badan</td>
										<td>: <?php echo $preview->heigh.' CM / '.$preview->weigh." KG"; ?></td>
										<td>&nbsp</td>
									</tr>
									<?php if($preview->clothing_size != ''): ?>
									<tr>
										<td>&nbsp</td>
										<td>Ukuran Seragam</td>
										<td>: <?php echo $preview->clothing_size; ?></td>
									</tr>
									<?php endif; ?>
									<?php if($preview->shoe_size > 0): ?>
									<tr>
										<td>&nbsp</td>
										<td>Ukuran Sepatu</td>
										<td>: <?php echo $preview->shoe_size; ?></td>
									</tr>
									<?php endif; ?>
									<tr>
										<td>Pendidikan Terakhir</td>
										<td>: <?php echo $preview->education_level; ?></td>
										<td>&nbsp</td>
									</tr>
									<tr>
										<td>Jurusan</td>
										<td>: <?php echo $preview->education_majors; ?></td>
										<td>&nbsp</td>
									</tr>
									<tr>
										<td colspan="3"><strong>B. RIWAYAT PENDIDIKAN</strong></td>
									</tr>
									<tr>
										<td colspan="3">
										<table class="table table-bordered">
											<tr>
												<th>Nama Sekolah/Universitas</th>
												<th>Mulai</th>
												<th>Selesai</th>
											</tr>
											<?php 
											foreach ($list_education as $item): 
											?>
											<tr>
												<td><?php echo $item->institute; ?></td>
												<td><?php echo $item->start; ?></td>
												<td><?php echo $item->end; ?></td>
											</tr>
											<?php 
											endforeach;
											?>
										</table>
										</td>
									</tr>
									<tr>
										<td colspan="3"><strong>C. KEMAMPUAN BAHASA</strong></td>
									</tr>
									<tr>
										<td colspan="3">
										<table class="table table-bordered">
											<tr>
												<th>Bahasa</th>
												<th>Lisan</th>
												<th>Tulisan</th>
											</tr>
											<?php 
											foreach ($list_language as $item): 
											?>
											<tr>
												<td><?php echo $item->language; ?></td>
												<td><?php echo $item->verbal; ?></td>
												<td><?php echo $item->write; ?></td>
											</tr>
											<?php 
											endforeach;
											?>
										</table>
										</td>
									</tr>
									<tr>
										<td colspan="3"><strong>D. KEAHLIAN</strong></td>
									</tr>
									<tr>
										<td colspan="3">
										<table class="table table-bordered">
											<tr>
												<th>Pelatihan</th>
												<th>Nomor Sertifikat</th>
											</tr>
											<?php 
											foreach ($list_skill as $item): 
											?>
											<tr>
												<td><?php echo $item->certificate_name; ?></td>
												<td><?php echo $item->certificate_number; ?></td>
											</tr>
											<?php 
											endforeach;
											?>
										</table>
										</td>
									</tr>
									<tr>
										<td colspan="3"><strong>E. RIWAYAT PEKERJAAN</strong></td>
									</tr>
									<tr>
										<td colspan="3">
										<table class="table table-bordered">
											<tr>
												<th>Nama Perusahaan</th>
												<th>Posisi Terakhir</th>
												<th>Tahun</th>
												<th>Gaji</th>
												<th>Alamat Perusahaan</th>
												<th>Alasan Resign</th>
											</tr>
											<?php 
											foreach ($list_history as $item): 
											?>
											<tr>
												<td><?php echo $item->company_name; ?></td>
												<td><?php echo $item->position; ?></td>
												<td><?php echo $item->period; ?></td>
												<td><?php echo $item->salary; ?></td>
												<td><?php echo $item->company_address; ?></td>
												<td><?php echo $item->resign_note; ?></td>
											</tr>
											<?php 
											endforeach;
											?>
										</table>
										</td>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="printContent();">Print</button>
			<button type="button" class="btn btn-default" onclick="window.close();">Tutup</button>
       	</div>
	</body>
</html>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('candidate/nonjob'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Karyawan Nonjob</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-3">
							<!-- <a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a> -->
							&nbsp;
						</div>
						<div class="col-sm-2">
							<div class="submission" hidden="true">
								<strong>Tanggal Penempatan :</strong>
								<input type="date" id="placement_date" class="form-control datepicker">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="submission" hidden="true">
							<strong>Bisnis Unit :</strong>
							<select class="form-control select2 " id="company_id" required>
								<option value="">-- Bisnis Unit --</option>
								<?php foreach ($list_company as $item) :  ?>
									<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="submission" hidden="true">
							<strong>Site Bisnis :</strong>
							<select class="form-control select2 " id="site_id" required>
								<option value="">-- Site Bisnis --</option>
								<?php foreach ($list_site as $item) :  ?>
									<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="submission" hidden="true">
								<strong>Jabatan :</strong>
								<select class="form-control select2 " id="position_id" required>
										<option value="">-- Pilih Jabatan --</option>
									<?php foreach ($list_position as $item) :  ?>
										<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-1">
							<br>
							<a href="#" class="btn btn-info submission btn-block" hidden="true" ><i class="fas fa-check"></i> Ajukan</a>
						</div>
					</div>
					<p>&nbsp;</p>
					<div class="row" >
						<div class="col-sm-11 submission" hidden="true">
							<input type="text" placeholder="Silahkan tulis Pesan" class="form-control float-right" id="message-personal">
						</div>
						<div class="col-sm-1 submission" hidden="true">
							<a class="btn btn-success save-personal btn-block"><i class="fas fa-paper-plane"></i> Kirim</a>
						</div>
						&nbsp;
					</div>
				</div>

				<div class="card-body">
					<form method="POST" id="submission" action="<?php echo base_url('candidate/submit/nonjob'); ?>">
					<input type="hidden" class="form-control message-personal" name="message" value="" >
					<input type="hidden" class="form-control site_id" name="site_id" value="1">
					<input type="hidden" class="form-control company_id" name="company_id" value="1">
					<input type="hidden" class="form-control position_id" name="position_id" value="1">
					<input type="hidden" class="form-control placement_date" name="placement_date" value="1">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th width="50px">No</th>
									<th id='created_at'>Tanggal Daftar</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='id_card'>KTP</th>
									<th id='age'>Usia</th>
									<th id='gender'>Jenis Kelamin</th>
									<th id='education_level'>Pendidikan</th>
									<th id='education_majors'>Jurusan</th>
									<th id='phone_number'>Nomor Telepon</th>
									<th id='city_domisili'>Domisili</th>
									<th id='province_domisili'>Provinsi</th>
									<th id='preference_job'>Pilihan Pekerjaan</th>
									<th id='site_preference'>Pilihan Penempatan</th>
									<th id='certificate'>Sertifikat</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("candidate/nonjob_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'date_registration' },
				{ data: 'full_name' },
				{ data: 'id_card' },
				{ data: 'age' },
				{ data: 'gender' },
				{ data: 'education_level' },
				{ data: 'education_majors' },
				{ data: 'phone_number' },
				{ data: 'city_domisili' },
				{ data: 'province_domisili' },
				{ data: 'preference_job' },
				{ data: 'site_preference' },
				{ data: 'certificate' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[2, 'desc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>1 && index<14){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$( ".export" ).click(function() {
			var url = "<?php echo base_url('candidate/export'); ?>" + "?full_name="+$("#s_full_name").val()+"&id_card="+$("#s_id_card").val()+"&age="+$("#s_age").val()+"&education_level="+$("#s_education_level").val()+"&education_majors="+$("#s_education_majors").val()+"&city_domisili="+$("#s_city_domisili").val()+"&preference_job="+$("#s_preference_job").val()+"&certificate="+$("#s_certificate").val()+"&status=pelamar_non_aktif&phone_number="+$("#s_phone_number").val()+"&site_preference="+$("#s_site_preference").val()+"&created_at="+$("#s_created_at").val()+"&gender="+$("#s_gender").val();
			window.location = url;
		
		});

		
		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
    		});
		});
		$("input:checkbox").change(function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
    		});
		});
		$( ".btn.submission" ).click(function() {
			if($( "#site_id").val() == '' || $( "#position_id").val() == '' || $( "#company_id").val() == ''|| $( "#placement_date").val() == ''){
				alert('Silahkan pilih Tanggal Penempatan, Bisnis Unit, Site Bisnis dan Jabatan!!!');
			}else{
				$( ".site_id").val($( "#site_id").val());
				$( ".position_id").val($( "#position_id").val());
				$( ".company_id").val($( "#company_id").val());
				$( ".placement_date").val($( "#placement_date").val());
				$( "#submission" ).submit();
			}
		});
		// $( "#message-personal" ).change(function() {
		// 	$(".message-personal").val($(this).val());
		// });
		$(".btn.save-personal" ).click(function() {
			var message = $('#message-personal').val();
			if(message == ''){
				alert('Pesan harap diisi. ');
			}else{
				$(".message-personal").val(message);
				$('#submission').attr('action', "<?php echo base_url('candidate/send/nonjob'); ?>").submit();
			}
		});

	});
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('candidate/list/'.$type.'/'.$branch); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Pelamar</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-2">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Candidate</a>&nbsp;
						</div>
						<div class="col-sm-2">
							<a href="#" class="btn btn-danger export-block"><i class="fas fa-file-export"></i> Export Candidate</a>&nbsp;
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-sm-2">
							<div class="submission" hidden="true">
								<strong>Tanggal Penempatan :</strong> 
								<input type="date" id="placement_date" class="form-control datepicker" >
							</div>
						</div>
						<div class="col-sm-3">
							<div class="submission" hidden="true">
							<strong>Lowongan :</strong>
							<select class="form-control select2 " id="vacancy_id" required>
								<option value="">-- Lowongan --</option>
								<?php foreach ($list_vacancy as $item) :  ?>
									<option value="<?php echo $item->id;?>"><?php echo $item->title.' - '.$item->province_name; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="submission" hidden="true">
							<strong>Bisnis Unit :</strong>
							<select class="form-control select2 " id="company_id" required>
								<option value="">-- Bisnis Unit --</option>
								<?php foreach ($list_company as $item) :  ?>
									<option value="<?php echo $item->id;?>"><?php echo $item->code; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="submission" hidden="true">
							<strong>Site Bisnis :</strong>
							<select class="form-control select2 " id="site_id" required>
								<option value="">-- Site Bisnis --</option>
								<?php foreach ($list_site as $item) :  ?>
									<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="submission" hidden="true">
							<strong>Jabatan :</strong>
								<select class="form-control select2 " id="position_id" required>
										<option value="">-- Pilih Jabatan --</option>
									<?php foreach ($list_position as $item) :  ?>
										<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-1">
							<br>
							<a href="#" class="btn btn-info submission btn-block" hidden="true" ><i class="fas fa-check"></i> Ajukan</a>
						</div>
					</div>
					<hr>
					<div class="row" >
						<div class="col-sm-3">
							<div class="submission" hidden="true">
								<strong>Tipe Followup :</strong>
								<select class="form-control select2 " id="status-followup">
									<option value="Lihat">Lihat</option>
									<option value="Hubungi">Hubungi</option>
									<option value="Proses">Proses</option>
									<option value="Tolak">Tolak</option>
									<option value="Blokir">Blokir</option>
								</select>
							</div>
						</div>
						<div class="col-sm-7 submission" hidden="true">
							<strong>Keterangan Followup :</strong>
							<textarea id="note-followup"  rows="2" placeholder="Tulis Catatan Followup" style="width:100%"; ></textarea>
						</div>
						<div class="col-sm-2 submission" hidden="true">
							<strong>&nbsp;</strong>
							<a class="btn btn-success followup btn-block"><i class="fas fa-phone"></i> Follow Up</a>
						</div>
						&nbsp;
					</div>
					<hr>
					<!-- <div class="row" >
						<div class="col-sm-11 submission" hidden="true">
							<textarea id="message-personal" name="message-personal" rows="4"placeholder="Silahkan tulis Pesan" style="width:100%"; ></textarea>
						</div>
						<div class="col-sm-1 submission" hidden="true">
							<a class="btn btn-success save-personal btn-block"><i class="fas fa-paper-plane"></i> Kirim Whatsapp</a>
						</div>
						&nbsp;
					</div> -->
				</div>
					<p>&nbsp;</p>
					<div class="row">
						<div class="col-sm-2">
							<strong>Pilih data yang ditampilkan :</strong>
						</div>
						<div class="col-sm-2">
							<select class="form-control preview float-right" id="branch">
								<?php foreach ($list_branch as $item): ?>
								<option value="<?php echo $item->id;?>" <?php if($branch == $item->id ): echo "selected"; endif; ?>>
									<?php echo $item->name; ?>
								</option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="col-sm-2">
							<select class="form-control preview float-right" id="type">
								<option value="all" <?php if($type == 'all' ): echo "selected"; endif; ?>>Semua Pelamar (<?php echo $candidate_all; ?>)</option>
								<option value="active" <?php if($type == 'active' ): echo "selected"; endif; ?>>Pelamar Aktif <strong>(<?php echo $candidate_active; ?>)</strong></option>
								<option value="nonactive" <?php if($type == 'nonactive' ): echo "selected"; endif; ?>>Pelamar Non Aktif (<?php echo $candidate_nonactive; ?>)</option>
								<option value="notcomplete" <?php if($type == 'notcomplete' ): echo "selected"; endif; ?>>Belum Lengkap (<?php echo $candidate_notcomplete; ?>)</option>
								<option value="nonjob" <?php if($type == 'nonjob' ): echo "selected"; endif; ?>>Karyawan Nonjob (<?php echo $candidate_nonjob; ?>)</option>
								<option value="not_followup" <?php if($type == 'not_followup' ): echo "selected"; endif; ?>>Belum diproses (<?php echo $candidate_notprocess; ?>)</option>
								<option value="lihat" <?php if($type == 'lihat' ): echo "selected"; endif; ?>>Dilihat (<?php echo $candidate_view; ?>)</option>
								<option value="hubungi" <?php if($type == 'hubungi' ): echo "selected"; endif; ?>>Dihubungi (<?php echo $candidate_call; ?>)</option>
								<option value="proses" <?php if($type == 'proses' ): echo "selected"; endif; ?>>Diproses (<?php echo $candidate_process; ?>)</option>
								<option value="tolak" <?php if($type == 'tolak' ): echo "selected"; endif; ?>>Ditolak (<?php echo $candidate_reject; ?>)</option>
								<option value="blokir" <?php if($type == 'blokir' ): echo "selected"; endif; ?>>Diblokir (<?php echo $candidate_block; ?>)</option>
							</select>
						</div>
						<div class="col-sm-7">
						</div>
					<p>&nbsp;</p>
					</div>
				<div class="card-body">
					<form method="POST" id="submission" action="<?php echo base_url('candidate/submit/'.$type.'/'.$branch); ?>" enctype="multipart/form-data">

					<div class="row" >
						<div class="col-sm-5 submission" hidden="true">
							<input type="text" placeholder="Silahkan tulis Pesan" class="form-control float-right" name="message-personal">
						</div>
						<div class="col-sm-5 submission" hidden="true">
							<input type="file" class="custom-file-input" name="message-image" accept="image/png, image/jpeg" required>
							<label class="custom-file-label">Pilih file</label>
						</div>
						<div class="col-sm-2 submission" hidden="true">
							<a class="btn btn-success save-personal btn-block"><i class="fas fa-paper-plane"></i> Kirim Whatsapp</a>
						</div>
						&nbsp;
					</div>

					<!-- <input type="file" class="form-control message-image" name="message-image" value="" > -->


					<input type="hidden" class="form-control note-followup" name="note_followup" value="" >
					<input type="hidden" class="form-control status-followup" name="status_followup" value="" >
					<input type="hidden" class="form-control message-personal" name="message" value="" >
					<input type="hidden" class="form-control vacancy_id" name="vacancy_id" value="1">
					<input type="hidden" class="form-control site_id" name="site_id" value="1">
					<input type="hidden" class="form-control company_id" name="company_id" value="1">
					<input type="hidden" class="form-control position_id" name="position_id" value="1">
					<input type="hidden" class="form-control placement_date" name="placement_date" value="1">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="50px"><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th id='created_at'>Tanggal Daftar</th>
									<th id='updated_at'>Login Teakhir</th>
									<th id='registration_number'>Nomor Pendaftaran</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='id_card'>KTP</th>
									<th id='age'>Usia</th>
									<th id='gender'>Jenis Kelamin</th>
									<th id='education_level'>Pendidikan</th>
									<th id='education_majors'>Jurusan</th>
									<th id='phone_number'>Nomor Telepon</th>
									<th id='city_domisili'>Domisili</th>
									<th id='province_domisili'>Provinsi</th>
									<th id='preference_job'>Pilihan Pekerjaan</th>
									<th id='site_preference'>Pilihan Penempatan</th>
									<th id='certificate'>Sertifikat</th>
									<th id='candidate_status'>Status Kelengkapan</th>
									<th id='document_cv'>Dokumen</th>
									<th id='followup_status'>Status Followup</th>
									<th id='followup_note'>Catatan Followup</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.preview').on('change', function() {
			var url = '<?php echo base_url("candidate/list/"); ?>'+$("#type").val()+'/'+$("#branch").val();
			window.location.href = url;
		});
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("candidate/list_ajax/".$type."/".$branch);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'date_registration' },
				{ data: 'last_update' },
				{ data: 'registration_number' },
				{ data: 'full_name' },
				{ data: 'id_card' },
				{ data: 'age' },
				{ data: 'gender' },
				{ data: 'education_level' },
				{ data: 'education_majors' },
				{ data: 'phone_number' },
				{ data: 'city_domisili' },
				{ data: 'province_domisili' },
				{ data: 'preference_job' },
				{ data: 'site_preference' },
				{ data: 'certificate' },
				{ data: 'candidate_status', 'orderable':false, 'searchable':false },
				{ data: 'document_cv', 'orderable':false, 'searchable':false },
				{ data: 'followup_status', 'orderable':false, 'searchable':false },
				{ data: 'followup_note', 'orderable':false, 'searchable':false },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[2, 'desc']],
			"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				if (aData['followup_status'].startsWith("Lihat")) {
					$('td', nRow).css('background-color', '#80ff80');
				} else if (aData['followup_status'].startsWith("Hubungi")) {
					$('td', nRow).css('background-color', '#8080ff');
				}else if (aData['followup_status'].startsWith("Proses")) {
					$('td', nRow).css('background-color', '#ffcc80');
				}else if (aData['followup_status'].startsWith("Tolak")) {
					$('td', nRow).css('background-color', '#ff8080');
				}else if (aData['followup_status'].startsWith("Blokir")) {
					$('td', nRow).css('background-color', '#bfbfbf');
				}
			}
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<15){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$( ".export" ).click(function() {
			var url = "<?php echo base_url('candidate/export/'.$type.'/'.$branch); ?>" + "?full_name="+$("#s_full_name").val()+"&id_card="+$("#s_id_card").val()+"&age="+$("#s_age").val()+"&education_level="+$("#s_education_level").val()+"&education_majors="+$("#s_education_majors").val()+"&city_domisili="+$("#s_city_domisili").val()+"&preference_job="+$("#s_preference_job").val()+"&certificate="+$("#s_certificate").val()+"&phone_number="+$("#s_phone_number").val()+"&site_preference="+$("#s_site_preference").val()+"&gender="+$("#s_gender").val()+"&registration_number="+$("#s_registration_number").val(); 
			window.location = url;
		
		});

		$( ".export-block" ).click(function() {
			var url = "<?php echo base_url('candidate/export_status/blokir') ?>"; 
			window.location = url;
		
		});
		
		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
    		});
		});
		$("input:checkbox").change(function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
    		});
		});
		$( ".btn.submission" ).click(function() {
			if($( "#site_id").val() == '' || $( "#position_id").val() == '' || $( "#company_id").val() == ''|| $( "#placement_date").val() == ''){
				alert('Silahkan pilih Tanggal Penempatan, Bisnis Unit, Site Bisnis dan Jabatan!!!');
			}else{
				$( ".vacancy_id").val($("#vacancy_id").val());
				$( ".site_id").val($("#site_id").val());
				$( ".position_id").val($("#position_id").val());
				$( ".company_id").val($("#company_id").val());
				$( ".placement_date").val($("#placement_date").val());
				$( "#submission" ).submit();
			}
		});
		$(".btn.save-personal" ).click(function() {
			// var message = $('#message-personal').val();
			// var image = $('#message-image').val();
			// if(message == ''){
			// 	alert('Pesan harap diisi. ');
			// }else{
			// 	$(".message-personal").val(message);
			// 	$(".message-image").val(image);
				$('#submission').attr('action', "<?php echo base_url('candidate/send/'.$type.'/'.$branch); ?>").submit();
			// }
		});
		$(".btn.followup" ).click(function() {
			var message = $('#note-followup').val();
			var status = $('#status-followup').val();
			if(message == ''){
				alert('Catatan Followup. ');
			}else{
				$(".note-followup").val(message);
				$(".status-followup").val(status);
				$('#submission').attr('action', "<?php echo base_url('candidate/followup/'.$type.'/'.$branch); ?>").submit();
			}
		});
	});
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('cooperation/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Penawaran</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?> &nbsp;
				</div>
				<div class="card-body">
					<?php 
					$minimum_offer = (10/100);
					if($cooperation): 
						$minimum_offer = ($cooperation->minimum_offer/100);
					?>
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Bisnis Unit</td>
								<td>: <?php echo $cooperation->company_name; ?></td>
							</tr>
							<tr>
								<td width="250px">Branch</td>									
								<td>: <?php echo $cooperation->branch_name; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $cooperation->site_name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $cooperation->site_address; ?></td>
							</tr>
							<tr>
								<td width="250px">Pekerjaan</td>
								<td>: <?php echo $position->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Minimal Penawaran</td>
								<td>: <?php echo $cooperation->minimum_offer; ?> % diatas HPP</td>
							</tr>
						</thead>
					</table>
					<?php endif; ?>
					<p>&nbsp;</p>
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4 component" method="post" action="<?php echo base_url("cooperation/form_detail/".$cooperation_id."/".$position->id); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Headcount <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="hidden" class="form-control" name="cooperation_id" value="<?php echo $cooperation_id; ?>">
									<input type="hidden" class="form-control" name="position_id" value="<?php echo $position->id; ?>">
									<input type="number" class="form-control" placeholder="Headcount" name="headcount" value="<?php echo $headcount; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Gaji Pokok <span class="text-danger">*</span></label>
								<div class="col-sm-4">
									<select class="form-control select2 city" name="city_id" required id="0">
										<option value="0" data-salary="" <?php if(0 === $city_id): echo "selected"; endif;?>>Atur Gaji Pokok</option>
										<?php foreach ($list_city as $item) :  ?>
										<option value="<?php echo $item->id;?>" data-salary="<?php echo number_format($item->minimum_salary,0,',','.'); ?>" <?php if($item->id == $city_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-sm-5">
									<input type="text" class="form-control salary numeric rupiah" placeholder="Gaji Pokok" name="salary" value="<?php echo number_format($salary,0,',','.'); ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-12 col-form-label">Komponen Penawaran <span class="text-danger"></span></label>
							</div>
							<?php $no = 1; ?>
							<hr>
							<?php foreach ($list_component as $item): ?>
							<div class="form-group row">
								<div class="col-sm-1">
									<a class="btn btn-danger btn-block" onclick="remove_ajax(this, <?php echo $item->id; ?>);"><i class="fas fa-minus"></i></a>
									<input type="hidden" class="form-control" name="component_id[]" value="<?php echo $item->id; ?>">
								</div>
								<label for="name" class="col-sm-1 col-form-label">Komponen <span class="text-danger"></span></label>
								<div class="col-sm-4">
									<select class="form-control select2 selling-price" id="<?php echo $no; ?>" name="selling_price_id[]">
										<?php foreach ($list_price as $price): ?>
										<option value="<?php echo $item->id; ?>" data-minimum="<?php echo number_format(($price->price + ($price->price * $minimum_offer)),0,',','.'); ?>" <?php if($item->selling_price_id == $price->id): echo 'selected'; endif; ?>><?php echo $price->name."- HPP : ".format_rupiah($price->price); ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<label for="name" class="col-sm-2 col-form-label vl">Harga Penawaran :<span class="text-danger"></span></label>
								<div class="col-sm-4">
									<input type="hidden" class="form-control" id="minimum-offer-<?php echo $no;?>" value="<?php echo number_format($item->offering_price,0,',','.'); ?>">
									<input type="text" class="form-control numeric rupiah offer" data-no="<?php echo $no;?>" id="offer-<?php echo $no;?>" placeholder="Harga yang ditawarkan" name="offering_price[]" value="<?php echo number_format($item->offering_price,0,',','.'); ?>">
								</div>
							</div>
							<?php $no++; endforeach; ?>	
							<div class="form-group row">
								<div class="col-md-1">
									<input type="hidden" class="form-control" name="component_id[]">
								</div>
								<label for="name" class="col-sm-1 col-form-label">Komponen <span class="text-danger"></span></label>
								<div class="col-sm-4">
									<select class="form-control select2 selling-price" id="<?php echo $no; ?>" name="selling_price_id[]">
										<option value=""> --SILAHKAN PILIH KOMPONEN--</option>
										<?php foreach ($list_price as $item): ?>
										<option value="<?php echo $item->id; ?>" data-minimum="<?php echo number_format(($item->price + ($item->price * $minimum_offer)),0,',','.'); ?>"><?php echo $item->name."- HPP : ".format_rupiah($item->price); ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<label for="name" class="col-sm-2 col-form-label vl">Harga Penawaran :<span class="text-danger"></span></label>
								<div class="col-sm-4">
									<input type="hidden" class="form-control" id="minimum-offer-<?php echo $no;?>" value="">
									<input type="text" class="form-control numeric rupiah offer" data-no="<?php echo $no;?>" id="offer-<?php echo $no;?>" placeholder="Harga yang ditawarkan" name="offering_price[]">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-3">
									<a class="btn btn-success btn-block add-component"><i class="fas fa-plus"></i>&nbsp;Tambah</a>
								</div>
							</div>
							<hr>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" name="action" = valuse="save" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
<script>	
	$(document).ready(function () {
		$(".add-component").click(function() {
			$(".component").submit();
		});

		$(".city").change(function(){
			$(".salary").val($(this).find(':selected').attr('data-salary'));
		});

		$(".selling-price").change(function(e){
			var id_number = e.target.id;
			$("#minimum-offer-"+id_number).val($(this).find(':selected').attr('data-minimum'));
			$("#offer-"+id_number).val($(this).find(':selected').attr('data-minimum'));
		});

		$(".offer").change(function(e){
			var id_number = $(this).data('no');
			var minimum = $("#minimum-offer-"+id_number).val().replace('/./g', '');
			if($(this).val().replace('/./g', '') < minimum){
				alert("Minimal Penawaran yang di perbolehkan : "+minimum);
				$(this).val(minimum);
				$(this).focus();
			}
		});
	});

	function remove_ajax(el, id, group){
		if (confirm('Anda yakin untuk menghapus data  ini?')) 
		{
			$.ajax({
				url: "<?php echo base_url('cooperation/delete_component'); ?>",
				type: "POST",
				cache: false,
				data:{
					id: id
				},
				success: function(response){
					var result = JSON.parse(response);
					if(result.code==200){
						$(el).parent().parent().remove();
					}
				}
			});
		}
		return false;
	}
</script>
<style type="text/css">
	.note-group-select-from-files {
		display: none;
	}
	.vl {
		margin-top: 0px;
		margin-bottom: -5px;
		border-left: 3px solid #ffc107;
	}
</style>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('cooperation/detail/'.$cooperation_id); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Penawaran</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?> &nbsp;
				</div>
				<div class="card-body">
					<?php 
					$minimum_offer = (10/100);
					if($cooperation): 
						$minimum_offer = ($cooperation->minimum_offer/100);
					?>
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Bisnis Unit</td>
								<td>: <?php echo $cooperation->company_name; ?></td>
							</tr>
							<tr>
								<td width="250px">Branch</td>									
								<td>: <?php echo $cooperation->branch_name; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $cooperation->site_name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $cooperation->site_address; ?></td>
							</tr>
							<tr>
								<td width="250px">Pekerjaan</td>
								<td>: <?php echo $position->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Minimal Penawaran</td>
								<td>: <?php echo $cooperation->minimum_offer; ?> % diatas HPP</td>
							</tr>
						</thead>
					</table>
					<?php endif; ?>
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4 component" method="post" action="<?php echo base_url("cooperation/form_detail/".$cooperation_id."/".$position->id); ?>">
							<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
							<input type="hidden" class="form-control" name="cooperation_id" value="<?php echo $cooperation_id; ?>">
							<input type="hidden" class="form-control" name="position_id" value="<?php echo $position->id; ?>">
							<table class="table table-bordered">
								<tbody>
									<tr>
										<th class="col-sm-3 col-form-label">Headcount <span class="text-danger"></span></th>
										<th class="col-sm-9" colspan="3">
											<input type="number" class="form-control" placeholder="Headcount" name="headcount" value="<?php echo $headcount; ?>" required>
										</th>
									</tr>
									<tr>
										<th class="col-sm-3">Kota/Kabupaten <span class="text-danger"></span></th>
										<th class="col-sm-3">UMR <span class="text-danger"></span></th>
										<th class="col-sm-3">Gaji Pokok <span class="text-danger"> *</span></th>
										<th class="col-sm-3">Selisih<span class="text-danger"></span></th>
									</tr>
									<tr>
										<td class="col-sm-3 col-form-label">
											<select class="form-control select2 city" name="city_id" required id="0">
												<option value="0" data-salary="" <?php if(0 === $city_id): echo "selected"; endif;?>>Pilih Gaji Pokok</option>
												<?php foreach ($list_city as $item) :  ?>
												<option value="<?php echo $item->id;?>" data-salary="<?php echo number_format($item->minimum_salary,0,',','.'); ?>" <?php if($item->id == $city_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
												<?php endforeach; ?>
											</select>
										</td>
										<td class="col-sm-3 col-form-label">
											<input type="text" class="form-control salary-standard" name="salary_standard" value="<?php echo $salary_standard; ?>" readonly>
										</td>
										<td class="col-sm-3 col-form-label">
											<input type="text" class="form-control salary numeric rupiah" placeholder="Gaji Pokok" name="salary" value="<?php echo number_format($salary,0,',','.'); ?>" required>
										</td>
										<td class="col-sm-3 col-form-label">
											<input type="text" class="form-control salary-percentage" name="salary_percentage" value="<?php echo $salary_percentage; ?>" style="color: <?php if(str_replace(" %","",$salary_percentage) < -10): echo "#dc3545"; elseif(str_replace(" %","",$salary_percentage) < 0): echo "#ffc107"; else: echo "#28a745"; endif; ?>;" readonly>
										</td>
									</tr>
								</tbody>
							</table>
							<table class="table table-bordered">
								<tbody>
									<tr>
										<th class="col-sm-1"># <span class="text-danger"></span></th>
										<th class="col-sm-3">Komponen <span class="text-danger"></span></th>
										<th class="col-sm-1">Amortisasi<span class="text-danger"></span></th>
										<th class="col-sm-3">HPP<span class="text-danger"></span></th>
										<th class="col-sm-4">Harga Penawaran<span class="text-danger"></span></th>
										<th class="col-sm-1">Persentase<span class="text-danger"></span></th>
									</tr>
									<?php $no = 1; ?>
									<?php foreach ($list_component as $item): ?>
									<tr>
										<td class="col-sm-1">
											<a class="btn btn-danger btn-block" onclick="remove_ajax(this, <?php echo $item->id; ?>);"><i class="fas fa-minus"></i></a>
											<input type="hidden" class="form-control" name="component_id[]" value="<?php echo $item->id; ?>">
										</td>
										<td class="col-sm-3">
											<select class="form-control select2 selling-price" id="<?php echo $no; ?>" name="selling_price_id[]">
												<option value=""> --SILAHKAN PILIH KOMPONEN--</option>
												<?php 
												foreach ($list_price as $item_price): 
													$price = $item_price->amortization_price;
													if($item_price->amortization_price == ''){
														$price = $item_price->price;
													}
												?>
												<option <?php if($item->selling_price_id == $item_price->id): echo 'selected'; endif; ?> value="<?php echo $item_price->id; ?>" data-price="<?php echo number_format($price,0,',','.'); ?>" data-amortization="<?php echo $item_price->amortization; ?>">
													<?php echo $item_price->name; ?>		
												</option>
												<?php endforeach; ?>
											</select>
										</td>
										<td class="col-sm-1">
											<input type="text" class="form-control" id="amortization-<?php echo $no;?>" name="amortization[]" value="<?php echo $item->amortization; ?>" readonly>
										</td>
										<td class="col-sm-3">
											<input type="text" class="form-control" id="standard-price-<?php echo $no;?>" name="standard_price[]" value="<?php echo number_format($item->standard_price,0,',','.'); ?>" readonly>
										</td>
										<td class="col-sm-4">
											<input type="text" class="form-control numeric rupiah price" id="price-<?php echo $no;?>" data-id="<?php echo $no; ?>" placeholder="Harga Penawaran" name="offering_price[]" value="<?php echo number_format($item->offering_price,0,',','.'); ?>" required>
										</td>
										<td class="col-sm-1">
											<input type="text" class="form-control" id="percentage-price-<?php echo $no;?>" name="percentage_price[]" value="<?php echo $item->percentage_price; ?>" style="color: <?php if(str_replace(" %","",$item->percentage_price) < 0): echo "#dc3545"; elseif(str_replace(" %","",$item->percentage_price) < 10): echo "#ffc107"; else: echo "#28a745"; endif; ?>;" readonly>
										</td>
									</tr>
									<?php $no++; endforeach; ?>
									<tr>
										<td class="col-sm-1">
											<input type="hidden" class="form-control" name="component_id[]">
										</td>
										<td class="col-sm-3">
											<select class="form-control select2 selling-price" id="<?php echo $no; ?>" name="selling_price_id[]">
												<option value=""> --SILAHKAN PILIH KOMPONEN--</option>
												<?php 
												foreach ($list_price as $item): 
													$price = $item->amortization_price;
													if($item->amortization_price == ''){
														$price = $$item->price;
													}
												?>
												<option value="<?php echo $item->id; ?>" data-price="<?php echo number_format($price,0,',','.'); ?>" data-amortization="<?php echo $item->amortization; ?>">
													<?php echo $item->name; ?>		
												</option>
												<?php endforeach; ?>
											</select>
										</td>
										<td class="col-sm-1">
											<input type="text" class="form-control" id="amortization-<?php echo $no;?>" name="amortization[]" value="" readonly>
										</td>
										<td class="col-sm-3">
											<input type="text" class="form-control" id="standard-price-<?php echo $no;?>" name="standard_price[]" value="" readonly>
										</td>
										<td class="col-sm-4">
											<input type="text" class="form-control numeric rupiah price" id="price-<?php echo $no;?>" data-id="<?php echo $no; ?>" placeholder="Harga Penawaran" name="offering_price[]" value="" >
										</td>
										<td class="col-sm-1">
											<input type="text" class="form-control" id="percentage-price-<?php echo $no;?>" name="percentage_price[]" value="" readonly>
										</td>
									</tr>
									<tr>
										<td class="col-sm-1">
											<a class="btn btn-success btn-block add-component"><i class="fas fa-plus"></i></a>
										</td>
										<td class="col-sm-11" colspan="5">

										</td>
									</tr>

								<div class="col-sm-3">
								</div>
								</tbody>
							</table>
							<hr>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" name="action" = valuse="save" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
<script>	
	$(document).ready(function () {
		$( ".salary" ).keyup(function() {
			var salary = $(this).val();
			var standard = $(".salary-standard").val().replace(/\D/g,''); 
			var percent = ((salary-standard)/standard) * 100;
			$(".salary-percentage").val(percent.toFixed(2)+" %");
			if(percent < -10){
				$(".salary-percentage").css("color","#dc3545");
			}else if(percent < 0){
				$(".salary-percentage").css("color","#ffc107");
			}else{
				$(".salary-percentage").css("color","#28a745");
			}
    	});

		$(".add-component").click(function() {
			$(".component").submit();
		});

		$(".city").change(function(){
			var salary = $(this).find(':selected').attr('data-salary');
			$(".salary").val(salary);
			$(".salary-standard").val(salary);
			$(".salary-percentage").val("0 %");
			$(".salary-percentage").css("color","#28a745");
		});

		$(".selling-price").change(function(e){
			var id_number = e.target.id;
			var price = $(this).find(':selected').attr('data-price');
			var amortization = $(this).find(':selected').attr('data-amortization');
			$("#amortization-"+id_number).val(amortization);
			$("#standard-price-"+id_number).val(price);
			$("#price-"+id_number).val(price);
			$("#percentage-price-"+id_number).val("0 %");
			$("#percentage-price-"+id_number).css("color","#28a745");
		});

		$( ".price" ).keyup(function() {
			var id_number = $(this).data("id")
			var price = $(this).val();
			var standard = $("#standard-price-"+id_number).val().replace(/\D/g,'');
			var percent = ((price-standard)/standard) * 100;
			$("#percentage-price-"+id_number).val(percent.toFixed(2)+" %");
			if(percent < -10){
				$("#percentage-price-"+id_number).css("color","#dc3545");
			}else if(percent < 0){
				$("#percentage-price-"+id_number).css("color","#ffc107");
			}else{
				$("#percentage-price-"+id_number).css("color","#28a745");
			}
		});
	});

	function remove_ajax(el, id, group){
		if (confirm('Anda yakin untuk menghapus data  ini?')) 
		{
			$.ajax({
				url: "<?php echo base_url('cooperation/delete_component'); ?>",
				type: "POST",
				cache: false,
				data:{
					id: id
				},
				success: function(response){
					var result = JSON.parse(response);
					if(result.code==200){
						$(el).parent().parent().remove();
					}
				}
			});
		}
		return false;
	}
</script>
<style type="text/css">
	.note-group-select-from-files {
		display: none;
	}
	.vl {
		margin-top: 0px;
		margin-bottom: -5px;
		border-left: 3px solid #ffc107;
	}
</style>
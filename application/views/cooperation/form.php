<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('cooperation/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Penawaran</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4" method="post" action="<?php echo base_url("cooperation/form"); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Bisnis Unit <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="hidden" class="form-control" name="sales_template_id" value="<?php echo $sales_template_id; ?>">
									<select class="form-control select2" name="company_id" required>
										<?php foreach ($list_company as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $company_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Branch <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="branch_id" required>
										<?php foreach ($list_branch as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $branch_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Site Bisnis <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="site_id" required>
										<?php foreach ($list_site as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $site_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Pekerjaan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select multiple class="form-control select2" name="position_id[]" data-placeholder="Pilih Pekerjaan" required>
										<?php 
										$job = explode(",",$position_id);
										foreach ($list_job as $item) : 
										?>
										<option value="<?php echo $item->id;?>" <?php if(in_array($item->id, $job)): echo "selected"; endif;?>><?php echo $item->name; ?></option> -->
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Management Fee <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="number" class="form-control" placeholder="Management Fee" name="management_fee" value="<?php echo $management_fee; ?>" required>
								</div>
								<label for="name" class="col-sm-7 col-form-label"><span class="text-success">%</span></label>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">PPN <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="number" class="form-control" placeholder="ppn" name="ppn" value="<?php echo $ppn; ?>" required>
								</div>
								<label for="name" class="col-sm-7 col-form-label"><span class="text-success">%</span></label>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">PPN Management Fee<span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="number" class="form-control" placeholder="ppn Management Fee" name="management_fee_ppn" value="<?php echo $management_fee_ppn; ?>" required>
								</div>
								<label for="name" class="col-sm-7 col-form-label"><span class="text-success">%</span></label>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Minimal Penawaran <span class="text-danger">*</span></label>
								<?php if(in_array($this->session->userdata('role'), $this->config->item('sales'))): ?>
									<div class="col-sm-2">
										<input type="number" class="form-control" placeholder="Minimal Penawaran" name="minimum_offer" value="<?php echo $minimum_offer; ?>" required>
									</div>
									<label for="name" class="col-sm-7 col-form-label"><span class="text-success">% diatas HPP</span></label>
								<?php else: ?>
									<label for="name" class="col-sm-9 col-form-label"><span class="text-success"><?php echo $minimum_offer; ?> % diatas HPP</span></label>
								<?php endif;?>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Status <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="status" required>
										<option <?php if($status == 'Penawaran'): echo 'selected'; endif; ?> value="Penawaran">Penawaran</option>
									</select>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" name="action" = valuse="save" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
<script>
	$(function () {
		tinymce.init({ 
			selector:'.editor',
			theme: 'modern',
			height: 600,
			menubar: true,
			lineheight_formats: "0pt 1pt 8pt 9pt 10pt 11pt 12pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 36pt",
			plugins: [
			'advlist autolink lists lineheight nonbreaking print preview anchor',
			'searchreplace visualblocks fullscreen textcolor colorpicker',
			'tabfocus table contextmenu powerpaste'
			],
			menubar: 'file edit view insert format tools table tc help',
			toolbar: 'undo redo | bold italic underline strikethrough | forecolor backcolor | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent nonbreaking lineheightselect | table | preview print',
			powerpaste_allow_local_images: true,
			powerpaste_word_import: 'prompt',
			powerpaste_html_import: 'prompt',
		});
	});
</script>
<style type="text/css">
	.note-group-select-from-files {
		display: none;
	}
</style>
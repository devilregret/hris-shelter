<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('cooperation/list'); ?>">Daftar Penawaran</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>&nbsp;
				</div>
				<div class="card-body">
					<?php if($cooperation): ?>
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Bisnis Unit</td>
								<td>: <?php echo $cooperation->company_name; ?></td>
							</tr>
							<tr>
								<td width="250px">Branch</td>									
								<td>: <?php echo $cooperation->branch_name; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $cooperation->site_name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $cooperation->site_address; ?></td>
							</tr>
						</thead>
					</table>
					<?php endif; ?>
					<p>&nbsp;</p>
					<table id="data-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th id='position'>Pekerjaan</th>
								<th id='headcount'>Headcount</th>
								<th id='salary'>Gaji Pokok</th>
								<th width="80px">#</th>
							</tr>
						</thead>
						<tfoot>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			 'searching'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("cooperation/detail_ajax/".$cooperation_id);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'position_name' },
				{ data: 'headcount' },
				{ data: 'salary' },
				{ data: 'action', 'orderable':false, 'searchable':false  }
			],
			'order': [[0, 'desc']]
		});
	});
</script>
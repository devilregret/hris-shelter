<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $_TITLE_?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php echo asset_css("css/adminlte.min.css"); ?>
	<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
	<?php echo asset_css("plugins/css/adminlte.min.css"); ?>
	<?php echo asset_js("plugins/jquery/jquery.min.js"); ?>		
</head>
<body>
<div class="wrapper">
	<div class="print" style="margin: 25px;">
		<?php echo str_replace('white-space: pre;', '', $content); ?>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" onclick="printContent();">Print</button>
	<button type="button" class="btn btn-default" onclick="window.close();">Tutup</button>
</div>

<script type="text/javascript"> 
	function printContent(){
		var restorepage = $('body').html();
		var printcontent = $('.print').clone();
		$('body').empty().html(printcontent);
		window.print();
		$('body').html(restorepage);
	}
</script>
</body>
</html>

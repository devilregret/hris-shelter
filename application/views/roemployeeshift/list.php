<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('roemployeeshift/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;List Shift Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-6">
							&nbsp;
						</div>
						<div class="col-sm-4">
							<div class="save" hidden="true">
								<select class="form-control select2 " id="shift_id" required>
									<option value="Shift 1">Shift 1</option>
									<option value="Shift 2">Shift 2</option>
									<option value="Shift 3">Shift 3</option>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<a href="#" class="btn btn-info save" hidden="true" ><i class="fas fa-check"></i> Simpan</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
						</thead>
					</table>
					<p>&nbsp;</p>
					<form method="POST" id="save" action="<?php echo base_url('roemployeeshift/save'); ?>">
					<input type="hidden" class="form-control shift_id" name="shift_id" value="1">
					<table id="data-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th width="10px"><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
								<th width="50px">No</th>
								<th id='shift'>Nama Karyawan</th>
								<th id='hours_start'>Nomer Induk Karyawan</th>
								<th id='position'>Posisi/Jabatan</th>
								<th id='hours_end'>Shift</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
					</table>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("roemployeeshift/list_ajax/");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'full_name' },
				{ data: 'employee_number' },
				{ data: 'position_name' },
				{ data: 'shift' }
			],
			'order': [[2, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>1 && index<6){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});
		$(document).on("change", ".check-id", function() {
			$(".save").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".save").prop('hidden', false);
    		});
		});
		$("input:checkbox").change(function() {
			$(".save").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".save").prop('hidden', false);
    		});
		});
		$( ".btn.save" ).click(function() {
			if($( "#shift_id").val() == ''){
				alert('Silahkan pilih Shift !!!');
			}else{
				$( ".shift_id").val($( "#shift_id").val());
				$( "#save" ).submit();
			}
		});
	});
</script>
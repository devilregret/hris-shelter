<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('roemployeeshift/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Ubah Data Shift</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("roemployeeshift/form/".$employee->id); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nama Karyawan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="hidden" class="form-control" name="employee_id" value="<?php echo $employee->id; ?>">
									<input type="text" class="form-control" placeholder="Nama Karyawan"  value="<?php echo $employee->full_name; ?>" readonly>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nomor Induk Karyawan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nomor Induk Karyawan"  value="<?php echo $employee->employee_number; ?>" readonly>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Posisi/Jabatan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Posisi/Jabatan"  value="<?php echo $position; ?>" readonly>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Shift <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="shift_id" required>
										<?php foreach($shift as $item): ?>
											<option value="<?php echo $item->id; ?>" <?php if($item->shift == $shift): echo "selected"; endif;?>> <?php echo $item->shift; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
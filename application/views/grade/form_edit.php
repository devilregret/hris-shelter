<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('grade/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Job Grading</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("grade/form"); ?>">
							<div style = 'display: none' class="form-group row">
								<label class="col-sm-3 col-form-label">ID <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="number" readonly name="id" class="form-control" value="<?php echo $id; ?>">
								</div>
								<div class="col-sm-4"></div>
							</div>
						
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Employee Name </label>
								<div class="col-sm-9">
									<div class="col-sm-9">
										<input type="text" readonly name="employee_name" class="form-control" value="<?php echo $full_name; ?>">
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Periode </label>
								<div class="col-sm-9">
									<div class="col-sm-9">
										<input type="text" readonly name="periode" class="form-control" value="<?php echo $periode; ?>">
									</div>
								</div>
							</div>

							<?php foreach ($list_competency as $competency) :  ?>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label"> <?php echo $competency->name ?> <span class="text-danger">*</span></label>
									<div class="col-sm-9">
										<input type="number" name="<?php echo $competency->id; ?>" class="form-control" value="<?php echo $competency->value; ?>">
									</div>
									<div class="col-sm-4"></div>
								</div>
							<?php endforeach; ?>
							
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
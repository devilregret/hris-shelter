<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('struktur_jabatan/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Job Grading</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("grade/form"); ?>">
							<div style = 'display: none' class="form-group row">
								<label class="col-sm-3 col-form-label">ID <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="number" readonly name="id" class="form-control" value="<?php echo $id; ?>">
								</div>
								<div class="col-sm-4"></div>
							</div>

							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Periode <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="periode" required>
										<option value=""> &nbsp;-- PILIH PERIODE --</option>
										<option value="2023"> &nbsp;2023</option>
										<option value="2024"> &nbsp;2024</option>
										<option value="2025"> &nbsp;2025</option>
										
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Posisi <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select onchange='listEmployee(this)' class="form-control select2" name="position_id" required>
										<option value=""> &nbsp;-- PILIH POSISI --</option>
										<?php foreach ($list_position as $item) :  ?>
										<option value="<?php echo $item->id;?>"> <?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Employee <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="employee_id" id="employee_id" required>
										<option value=""> &nbsp;-- PILIH EMPLOYEE --</option>
									</select>
								</div>
							</div>
							
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>

<script type="text/javascript">
	function listEmployee(positionId) {
		$.ajax({
			url: "<?php echo base_url('struktur_organisasi/list_employee_by_position'); ?>",
			type: "POST",
			cache: false,
			data:{
				position_id: positionId.value
			},
			success: function(response){
				console.log(response);
				$.each(response.aaData, function(i, obj){
					var div_data="<option value="+obj.id+">"+obj.full_name+"</option>";
					// alert(div_data);
					$(div_data).appendTo('#employee_id'); 
				});  
			}
		});
	}
</script>
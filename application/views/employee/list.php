<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('employee/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-12">
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export Data</a>&nbsp;
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="50px">No</th>
									<th id='employee_number'>ID Karyawan</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='branch_name'>Branch</th>
									<th id='employee_company'>Unit Kontrak</th>
									<th id='site_name'>Site Bisnis</th>
									<th id='site_company'>Unit Bisnis</th>
									<th id='position_name'>Jabatan</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("employee/list_ajax/".$type."/".$company."/".$branch);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'employee_number' },
				{ data: 'full_name' },
				{ data: 'branch_name' },
				{ data: 'employee_company' },
				{ data: 'site_name' },
				{ data: 'site_company' },
				{ data: 'position_name' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[2, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<8){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
	});

	$(".export" ).click(function() {
			var url = "<?php echo base_url('employee/export?type='.$type); ?>" + "&employee_number="+$("#s_employee_number").val()+"&full_name="+$("#s_full_name").val()+"&branch_name="+$("#s_branch_name").val()+"&employee_company="+$("#s_employee_company").val()+"&site_name="+$("#s_site_name").val()+"&site_company="+$("#s_site_company").val()+"&position_name="+$("#s_position_name").val();
			window.location = url;
		
		});

	function preview_contract(el)
	{
		var id = '';
		var i = 0;
		$("input:checkbox.check-id:checked").each(function(){
			if(i > 0){
				id = id + ','; 
			}
			id = id +''+$(this).val();
			i++;
		});
		var template = $('#template').val();
		var duration =  $('#reservationtime').val().replace(" ", "").replace(" ", "");
		const url = $(el).attr('data-href')+'?id='+id+'&duration='+duration+'&template='+template;
	    const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
	    const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

	    const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	    const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	    const systemZoom = width / window.screen.availWidth;
	    const left = (width - 1000) / 2 / systemZoom + dualScreenLeft
	    const top = (height - 800) / 2 / systemZoom + dualScreenTop
	    const newWindow = window.open(url, 'preview', 
	      `
	      scrollbars=yes,
	      width=${1000 / systemZoom}, 
	      height=${800 / systemZoom}, 
	      top=${top}, 
	      left=${left}
	      `
	    )
	  return false;
	};
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('employee/submission/'.$type); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Pelamar</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-3">
							<div class="submission" hidden="true">
							<input type="text" class="form-control" id="note" placeholder="Catatan">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="submission" hidden="true">
							<select class="form-control select2 " id="company_id" required>
								<option value="">-- SESUAI PENGAJUAN --</option>
								<?php foreach ($list_company as $item) :  ?>
									<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="submission" hidden="true">
							<select class="form-control select2 " id="site_id" required>
								<option value="">-- SESUAI PENGAJUAN --</option>
								<?php foreach ($list_site as $item) :  ?>
									<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="submission" hidden="true">
								<select class="form-control select2 " id="position_id" required>
										<option value="">-- SESUAI PENGAJUAN --</option>
									<?php foreach ($list_position as $item) :  ?>
										<option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-2 submission">
							<a href="#" class="btn btn-info submission approve" hidden="true" ><i class="fas fa-check"></i> Terima</a>
							<a href="#" class="btn btn-danger submission reject" hidden="true"><i class="fas fa-window-close"></i> Tolak</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="POST" id="submission" action="<?php echo base_url('employee/approval'); ?>">
					<input type="hidden" class="form-control company_id" name="company_id" required="">
					<input type="hidden" class="form-control site_id" name="site_id" required="">
					<input type="hidden" class="form-control position_id" name="position_id" required="">
					<input type="hidden" class="form-control action" name="action" required="">
					<input type="hidden" class="form-control note" name="note" required="">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th width="50px">No</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='id_card'>KTP</th>
									<th id='age'>Usia</th>
									<th id='education_level'>Pendidikan</th>
									<th id='education_majors'>Jurusan</th>
									<th id='city_domisili'>Domisili</th>
									<th id='certificate'>Sertifikat</th>
									<th id='site_name'>Site Bisnis</th>
									<th id='company_name'>Unit Kontrak</th>
									<th id='position'>Jabatan</th>
									<th id='placement_date'>Tanggal Penempatan</th>
									<th id='submitted_by'>Diajukan Oleh</th>
									<th id='document_cv'>Dokumen</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("employee/submission_ajax/".$type);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'full_name' },
				{ data: 'id_card' },
				{ data: 'age' },
				{ data: 'education_level' },
				{ data: 'education_majors' },
				{ data: 'city_domisili' },
				{ data: 'certificate' },
				{ data: 'site_name' },
				{ data: 'company_name'},
				{ data: 'position' },
				{ data: 'placement_date' },
				{ data: 'submitted_by' },
				{ data: 'document_cv', 'orderable':false, 'searchable':false },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[2, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>1 && index<14){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});
		
		$("input:checkbox").change(function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
			});
		});

		$(".approve" ).click(function() {
			$( ".action").val("accept");
			$( ".site_id").val($( "#site_id").val());
			$( ".position_id").val($( "#position_id").val());
			$( ".company_id").val($( "#company_id").val());
			$( ".note").val($("#note").val());
			$( "#submission" ).submit();
		});

		$(".reject").click(function() {
			$( ".note").val($("#note").val());
			$( ".action").val("reject");
			$( "#submission" ).submit();
		});
	});
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('formula/list'); ?>">Form Formula Gaji</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Format Gaji</h3>
				</div>
				<div class="card-header">
					&nbsp;<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
						</thead>
					</table>
					<p>&nbsp;</p>
					<form class="form-horizontal col-sm-12 mt-4 formula" method="POST" action="<?php echo base_url("formula/form"); ?>" enctype="multipart/form-data">
						<div class="row">
							<div class="col-sm-9">
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input btn-sm" name="formula" accept=".xlsx" <?php if(!$formula): echo "required"; endif; ?>> <label class="custom-file-label">Update Data</label>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<?php if($formula): ?>
									<a href="<?php echo $url_formula; ?>" class="btn btn-success" style="width:200px";><i class="fas fa-integral"></i> Formula Gaji</a>
								<?php endif; ?>
							</div>
						</div>
						<p>&nbsp;</p>
						<div class="row">
							<input type="hidden" class="form-control" name="site_id" value="<?php echo $site->id; ?>">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th width="150px;">#</th>
										<th width="100px;">Nama Kolom</th>
										<th>Nama Komponen</th>
										<th width="150px;">Tipe</th>
										<th width="150px;">Kategori</th>
										<th>Nominal (Rp.)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th></th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[0]->kolom)) ? $formula[0]->kolom : ''; ?>" name="kolom[]">
											<input type="hidden" value="1" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="HARI KERJA" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="PENDAPATAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th>&nbsp; </th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[1]->kolom)) ? $formula[1]->kolom : ''; ?>" name="kolom[]">
											<input type="hidden" value="2" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="MASUK KERJA" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="PENDAPATAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th>&nbsp; </th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[2]->kolom)) ? $formula[2]->kolom : ''; ?>" name="kolom[]">
											<input type="hidden" value="3" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="SHIFT MALAM" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="PENDAPATAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th>&nbsp; </th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[3]->kolom)) ? $formula[3]->kolom : ''; ?>" name="kolom[]">
											<input type="hidden" value="4" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="TIDAK MASUK" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="PENDAPATAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th>&nbsp; </th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[4]->kolom)) ? $formula[4]->kolom : ''; ?>" name="kolom[]">
											<input type="hidden" value="5" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="TERLAMBAT" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="PENDAPATAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th>&nbsp; </th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[5]->kolom)) ? $formula[5]->kolom : ''; ?>" name="kolom[]">
											<input type="hidden" value="6" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="JAM LEMBUR" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="PENDAPATAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th>&nbsp; </th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[6]->kolom)) ? $formula[6]->kolom : ''; ?>" name="kolom[]">
											<input type="hidden" value="7" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="UANG LEMBUR" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="PENDAPATAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th>&nbsp; </th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[7]->kolom)) ? $formula[7]->kolom : ''; ?>" name="kolom[]">
											<input type="hidden" value="8" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="UANG MAKAN" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="PENDAPATAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th><span class="text-danger"></span></th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[8]->kolom)) ? $formula[8]->kolom : ''; ?>" name="kolom[]">
											<input type="hidden" value="9" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="STATUS PTKP" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="POTONGAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th><span class="text-danger"></span></th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[9]->kolom)) ? $formula[9]->kolom : ''; ?>" name="kolom[]">
											<input type="hidden" value="10" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="PTKP" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="POTONGAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th><span class="text-danger"></span></th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[10]->kolom)) ? $formula[10]->kolom : ''; ?>" name="kolom[]">
											<input type="hidden" value="11" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="PPH21" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="POTONGAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th>&nbsp; </th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[11]->kolom)) ? $formula[11]->kolom : ''; ?>" name="kolom[]">
											<input type="hidden" value="12" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="BPJS TK PERUSAHAAN" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="PENDAPATAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th>&nbsp; </th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[12]->kolom)) ? $formula[12]->kolom : ''; ?>" name="kolom[]">
											<input type="hidden" value="13" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="BPJS KS PERUSAHAAN" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="PENDAPATAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th>&nbsp; </th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[13]->kolom)) ? $formula[13]->kolom : ''; ?>" name="kolom[]">
											<input type="hidden" value="14" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="BPJS TK KARYAWAN" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="POTONGAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th>&nbsp; </th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[14]->kolom)) ? $formula[14]->kolom : ''; ?>" name="kolom[]">
											<input type="hidden" value="15" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="BPJS KS KARYAWAN" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="POTONGAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th><span class="text-danger">Kolom Harus diisi</span></th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[15]->kolom)) ? $formula[15]->kolom : ''; ?>" name="kolom[]" required>
											<input type="hidden" value="16" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="GAJI BERSIH (THP)" name="nama[]" required readonly>
										</th>
										<th>
											<input type="hidden" value="FORMULA" name="tipe[]" required>
										</th>
										<th>
											<input type="hidden" value="PENDAPATAN" name="kategori[]" required>
										</th>
										<th>
											<input type="hidden" value="" name="nilai[]" required>
										</th>
									</tr>
									<tr>
										<th><span class="text-danger">Kolom Harus diisi</span></th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo (isset($formula[16]->kolom)) ? $formula[16]->kolom : ''; ?>" name="kolom[]" required>
											<input type="hidden" value="17" name="no[]" required>
										</th>
										<th>
											<input type="text" class="form-control" value="GAJI POKOK" name="nama[]" required readonly>
										</th>
										<th>
											<select class="form-control select2" name="tipe[]" required>
												<option value="NOMINAL" selected> NOMINAL</option>
											</select>
										</th>
										<th>
											<select class="form-control select2" name="kategori[]" required>
												<option value="PENDAPATAN" selected> PENDAPATAN</option>
											</select>
										</th>
										<th>
											<input type="text" class="form-control numeric rupiah" placeholder="Nominal" name="nilai[]" value="<?php echo (isset($formula[16]->nilai)) ? rupiah_round($formula[16]->nilai) : ''; ?>" required>
										</th>
									</tr>
									<?php $no = 18; for ($i = 17; $i < count($formula); $i++): ?>
									<tr>
										<th>
											<a class="btn btn-danger btn-block" onclick="remove(this);"><i class="fas fa-minus"></i> Hapus</a>
										</th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="<?php echo $formula[$i]->kolom ?>" name="kolom[]">
											<input type="hidden" value="<?php echo $no; ?>" name="no[]">
										</th>
										<th>
											<input type="text" class="form-control text-uppercase" value="<?php echo $formula[$i]->nama ?>" name="nama[]" >
										</th>
										<th>
											<select class="form-control select2" name="tipe[]">
												<option value="NOMINAL" <?php echo (($formula[$i]->tipe == 'NOMINAL')? 'selected' : ''); ?>> NOMINAL</option>
												<option value="FORMULA" <?php echo (($formula[$i]->tipe == 'FORMULA')? 'selected' : ''); ?>> FORMULA</option>
											</select>
										</th>
										<th>
											<select class="form-control select2" name="kategori[]">
												<option value="PENDAPATAN" <?php echo (($formula[$i]->kategori == 'PENDAPATAN')? 'selected' : ''); ?>> PENDAPATAN</option>
												<option value="POTONGAN" <?php echo (($formula[$i]->kategori == 'POTONGAN')? 'selected' : ''); ?>> POTONGAN</option>
											</select>
										</th>
										<th>
											<input type="text" class="form-control numeric rupiah" placeholder="Nominal" name="nilai[]" value="<?php echo (isset($formula[$i]->nilai)) ? rupiah_round($formula[$i]->nilai) : ''; ?>">
										</th>
									</tr>
									<?php $no++; endfor; ?>
									<tr>
										<th>
											<a class="btn btn-danger btn-block" onclick="remove(this);"><i class="fas fa-minus"></i> Hapus</a>
										</th>
										<th>
											<input type="text" class="form-control text-uppercase alphabetic" value="" name="kolom[]">
											<input type="hidden" value="<?php echo $no; ?>" name="no[]">
										</th>
										<th>
											<input type="text" class="form-control text-uppercase" value="" name="nama[]" >
										</th>
										<th>
											<select class="form-control select2" name="tipe[]">
												<option value="FORMULA"> FORMULA</option>
												<option value="NOMINAL"> NOMINAL</option>
											</select>
										</th>
										<th>
											<select class="form-control select2" name="kategori[]">
												<option value="PENDAPATAN"> PENDAPATAN</option>
												<option value="POTONGAN"> POTONGAN</option>
											</select>
										</th>
										<th>
											<input type="text" class="form-control numeric rupiah" placeholder="Nominal" name="nilai[]" value="">
										</th>
									</tr>
								</tbody>
							</table>
						</div>
							<div class="row">
								<div class="col-sm-3">
								<button type="submit" class="btn btn-success btn-block add-detail">Tambah</button>
								</div>
							</div>
							<hr>
							<div class="row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
					</form>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>

<script type="text/javascript">
	$( ".rupiah" ).on( "change", function() {
		var angka 		= $(this).val();
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
		split   		= number_string.split(','),
		sisa     		= split[0].length % 3,
		rupiah     		= split[0].substr(0, sisa),
		ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
	 
		// tambahkan titik jika yang di input sudah menjadi angka ribuan
		if(ribuan){
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}
	 
		return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	});
	function formatRupiah(angka, prefix){
		
		return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

	function remove(el){
		$(el).parent().parent().remove();
	}

	$('.alphabetic').on('input', function (event) { 
    	this.value = this.value.replace(/[^a-zA-Z^\-]/g, '');
	});
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('rocutoff/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Ubah Data Cut Off</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("rocutoff/form"); ?>">
							
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Keterangan</label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="text" class="form-control" placeholder="Keterangan" name="description"  value="<?php echo $description; ?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label"> Tanggal Mulai<span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="start_date" required>
										<?php for ($i=1; $i<=31; $i++) {  ?>
										<option value="<?php echo $i;?>" <?php if($start_date == $i): echo "selected"; endif;?>><?php echo $i; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label"> Tanggal Selesai<span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="end_date" required>
										<?php for ($i=1; $i<=31; $i++) {  ?>
										<option value="<?php echo $i;?>" <?php if($end_date == $i): echo "selected"; endif;?>><?php echo $i; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
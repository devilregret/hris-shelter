<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('roattendance/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Kehadiran Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>&nbsp;
				</div>
				<div class="card-header">
					<form id="attendance" method="POST" action="<?php echo base_url('roattendance/syncronize/'.$sdate); ?>">
					<div class="row">
						<label class="col-sm-1 col-form-label">Tanggal Awal :</label>
						<div class="col-sm-3">
							<div class="form-group">
								<div class="input-group date" id="reservationdate" data-target-input="nearest">
									<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" data-toggle="datetimepicker" name='date_start' value="<?php echo $start_date; ?>" />
									<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>
							</div>
						</div>
						<label class="col-sm-1 col-form-label">Tanggal Akhir :</label>
						<div class="col-sm-3">
							<div class="input-group date" id="reservationdate2" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate2"  data-toggle="datetimepicker" name='date_finish' value="<?php echo $end_date; ?>" />
								<div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
						</div>
						<label class="col-sm-1 col-form-label">ID Mesin :</label>
						<div class="col-sm-1">
							<input type="text" name="attendance_machine" class="form-control" value="<?php echo $site->attendance_machine; ?>">
						</div>
						<div class="col-sm-2">
							<input class="btn btn-warning" type="submit" value="Sinkronkan" style="color: white;">
						</div>
					</div>
					</form>
				</div>
				<div class="card-header">
					<div class="row">
						<div class="form-group col-sm-12">
							<form method="post" action="<?php echo base_url("roattendance/import/".$sdate); ?>" enctype="multipart/form-data">
								<div class="input-group">
									<div class="custom-file">
										<input type="hidden" class="form-control" name="site_id" value="<?php echo $site->id; ?>">
										<input type="file" class="custom-file-input btn-sm" name="file" accept=".xlsx" required>
										<label class="custom-file-label" for="exampleInputFile">Import Data</label>
									</div>
									<div class="input-group-append">
										<button type="submit" name="setting" class="input-group-text" value="upload">Upload</button>   
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Site Id</td>									
								<td>: <?php echo $site->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Nama Site Bisnis</td>
								<td>: <?php echo $site->name; ?></td>
							</tr>
							<tr>
								<td width="250px">Alamat Site Bisnis</td>
								<td>: <?php echo $site->address; ?></td>
							</tr>
							<tr>
								<td width="250px">Periode Cut off</td>
								<td>: <?php echo $start_date.' - '.$end_date; ?></td>
							</tr>
						</thead>
					</table>

					<p>&nbsp;</p>
					<div class="row">
						<div class="col-sm-2">
							<strong>Pilih data yang ditampilkan :</strong>
						</div>
						<div class="col-sm-2">
							<select class="form-control preview float-right" id="date">
								<?php 
								for($i=0; $i<6; $i++) :
									// $cdate = date("Y-m", strtotime("-".$i." month"));
									$pdate = date("M Y", strtotime("-".$i." months"));
								?>
									<option value="<?php echo $i?>" <?php if($i == $sdate ): echo "selected"; endif; ?>>
									<?php echo $pdate; ?>
								<?php 
								endfor;
								?>
							</select>
						</div>
					</div>
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="50px">No</th>
									<th id='pin_finger'>PIN finger</th>
									<th id='employee_number'>Nomer Karyawan</th>
									<th id='employee_name'>Nama Karyawan</th>
									<th>Jumlah Masuk</th>
									<th>Jumlah Tidak Masuk</th>
									<th>Jumlah Lembur</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#reservationdate').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false
		});
		$('#reservationdate2').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false
		});

		$('.preview').on('change', function() {
			var url = '<?php echo base_url("roattendance/list/"); ?>'+$("#date").val();
			window.location.href = url;
		});
		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("roattendance/list_ajax/".$sdate);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'pin_finger' },
				{ data: 'employee_number' },
				{ data: 'full_name' },
				{ data: 'present' },
				{ data: 'not_present' },
				{ data: 'overtime' },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[1, 'asc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<4){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
	});
</script>
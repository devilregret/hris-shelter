<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('roattendance/list'); ?>">Kehadiran Karyawan</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('roattendance/site/'.$employee->site_id); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Kehadiran <?php echo $employee->full_name; ?></h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("roattendance/form/".$employee->id); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nama Karyawan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="employee_id" value="<?php echo $employee->id; ?>">
									<input type="text" class="form-control" placeholder="Nama Karyawan"  value="<?php echo $employee->full_name; ?>" readonly>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Nomor Induk Karyawan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Nomor Induk Karyawan"  value="<?php echo $employee->employee_number; ?>" readonly>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Tanggal <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="date" name="date" class="form-control datepicker" value="<?php echo $date; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Jam Mulai <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hours_start" name="hours_start" class="form-control" value="<?php echo $hours_start; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Jam Selesai <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="hours_end" name="hours_end" class="form-control" value="<?php echo $hours_end; ?>" required>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
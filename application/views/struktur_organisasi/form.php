<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('struktur_organisasi/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Struktur Organisasi</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("struktur_organisasi/form"); ?>">
							<div style = 'display: none' class="form-group row">
								<label class="col-sm-3 col-form-label">ID <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="number" readonly name="id" class="form-control" value="<?php echo $id; ?>">
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Parent Position<span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="parent_id" required>
										<option value=""> &nbsp;-- PILIH PARENT ORGANISASI --</option>
										<?php foreach ($list_parent as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php echo ($item->id ==  $parent_id) ? ' selected="selected"' : '';?> > <?php echo $item->position_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Position<span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select onchange='listEmployeeAtasan(this)' class="form-control select2" name="position_id" required>
										<option value=""> &nbsp;-- PILIH POSISI --</option>
										<?php foreach ($list_posisi as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php echo ($item->id == $position_id) ? ' selected="selected"' : '';?> > <?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Employee<span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select multiple class="form-control select2" name="employee_id[]" id="employee_id" data-placeholder="Pilih Atasan">
										<option value=""> &nbsp;-- PILIH EMPLOYEE --</option>	
									</select>
									<!-- <select class="form-control select2" name="atasan_id" id="atasan_id" required>
										<option value=""> &nbsp;-- PILIH EMPLOYEE --</option>
									</select> -->
								</div>
							</div>
							
							<!-- <div class="form-group row">
								<label class="col-sm-3 col-form-label">Name <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" name="struktur_name" class="form-control" value="<?php echo $struktur_name; ?>">
								</div>
								<div class="col-sm-4"></div>
							</div> -->
							
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>

<script type="text/javascript">
	// $(document).ready(function(){
	// 	alert('aaaa')
	// })

	function listEmployeeAtasan(positionId) {
		// alert('Posisi : ' + positionId.value)
		// var mySelect = $('#atasan_id');
		$.ajax({
			url: "<?php echo base_url('struktur_organisasi/list_employee_by_position'); ?>",
			type: "POST",
			cache: false,
			data:{
				position_id: positionId.value
			},
			success: function(response){
				console.log(response);
				$.each(response.aaData, function(i, obj){
					var div_data="<option value="+obj.id+">"+obj.full_name+"</option>";
					// alert(div_data);
					$(div_data).appendTo('#employee_id'); 
				});  
			}
		});
		// if (document.getElementById("kategori").value == 2){
		// 	document.getElementById('div_type').style.display = "";
		// 	// document.getElementById('div_file').style.display = "none";
		// }else if(document.getElementById("kategori").value == 3){
		// 	document.getElementById('div_type').style.display = "none";
		// 	// document.getElementById('div_file').style.display = "";
		// }else{
		// 	// document.getElementById('div_file').style.display = "none";
		// 	document.getElementById('div_type').style.display = "none";
		// }        
	}
</script>
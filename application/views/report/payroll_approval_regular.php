<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('report/payroll_approval_regular'); ?>">Pengajuan Payroll</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-body">
					<form class="form-horizontal col-sm-12 mt-4" method="post" action="<?php echo base_url("report/payroll_approval_regular"); ?>" enctype="multipart/form-data">
					<div class="row">
						<label class="col-sm-1 col-form-label text-right">Tipe :</label>
						<div class="col-sm-2">
							<select class="form-control float-right" name="view">
								<option value="release" <?php if($view == "release" ): echo "selected"; endif; ?>>Release Payroll & Slipgaji</option>
								<option value="notrelease" <?php if($view == "notrelease" ): echo "selected"; endif; ?>>Belum Release Payroll</option>
								<option value="reject" <?php if($view == "reject" ): echo "selected"; endif; ?>>Pengajuan Payroll di tolak</option>
							</select>
						</div>
						<label class="col-sm-1 col-form-label text-right">Tanggal Awal :</label>
						<div class="col-sm-2">
							<div class="input-group date" id="reservationdate" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name='date_start' id="start_date" value="<?php echo $start_date; ?>" />
								<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
							<!-- <input type="date" name="start_date" id="start_date" class="form-control datepicker" value="< ?php echo $start_date; ?>" required> -->
						</div>
						<label class="col-sm-1 col-form-label text-right">Tanggal Akhir :</label>
						<div class="col-sm-2">
							<div class="input-group date" id="reservationdate2" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate2" name='end_date' id="end_date" value="<?php echo $end_date; ?>" />
								<div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
							<!-- <input type="date" name="end_date" id="end_date" class="form-control datepicker" value="< ?php echo $end_date; ?>" required> -->
						</div>
						<div class="col-sm-1">
							<button type="submit" name="submit" class="btn btn-primary btn-form btn-block">Simpan</button>
						</div>
						<div class="col-sm-2"></div>
					<p>&nbsp;</p>
					</div>
					</form>
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="50px">No</th>
									<th id='site_name'>Site Bisnis</th>
									<th id='company_name'>Bisni Unit</th>
									<th id='periode_start'>Periode Mulai</th>
									<th id='periode_end'>Periode Selesai</th>
									<th>Jumlah Karyawan</th>
									<th>Jumlah Release</th>
									<th>Jumlah Ditolak</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#reservationdate').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false
		});
		$('#reservationdate2').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false
		});

		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("report/payroll_approval_regular_ajax/".$view."/".$start_date."/".$end_date);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'no', 'orderable':false, 'searchable':false },
				{ data: 'site_name' },
				{ data: 'company_name' },
				{ data: 'periode_start', 'orderable':false, 'searchable':false },
				{ data: 'periode_end', 'orderable':false, 'searchable':false },
				{ data: 'total_employee', 'orderable':false, 'searchable':false },
				{ data: 'total_approval', 'orderable':false, 'searchable':false },
				{ data: 'total_reject', 'orderable':false, 'searchable':false },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[2, 'desc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<3){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
	});
</script>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('account/activity'); ?>">Catatan Aktivitas</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Catatan Aktivitas</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("account/activity_form"); ?>">
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Tanggal <span class="text-danger"></span></label>
								<div class="col-sm-5">
									<input type="date" name="date" class="form-control datepicker" value="<?php echo $date; ?>">
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Jam Mulai <span class="text-danger"></span></label>
								<div class="col-sm-5">
									<input type="time" name="time_start" class="form-control datepicker" value="<?php echo $time_start; ?>">
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Jam Selesai <span class="text-danger"></span></label>
								<div class="col-sm-5">
									<input type="time" name="time_finish" class="form-control datepicker" value="<?php echo $time_finish; ?>">
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Aktivitas<span class="text-danger"></span></label>
								<div class="col-sm-9">
									<textarea class="form-control" name="activity" rows="3" placeholder="Aktifitas yang dilakukan"><?php echo $activity; ?></textarea>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Hasil Pekerjaan<span class="text-danger"></span></label>
								<div class="col-sm-9">
									<textarea class="form-control" name="result" rows="3" placeholder="Berkas, Data, Report dsb"><?php echo $result; ?></textarea>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
		 	   	</div>
			</div>	
		</div>
	</section>
</div>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('account/payroll'); ?>">Payroll</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Laporan Payroll Karyawan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-12">
							&nbsp;
						</div>
					</div>
				</div>
				<div class="card-body">
					<?php if($employee->status_completeness == 0 && $this->session->userdata('site_id') != 951): ?>
					<div class="alert alert-danger alert-dismissible">
						<h5><i class="icon fas fa-info"></i> Informasi</h5>
						<strong> <a href="<?php echo base_url('account/register'); ?>">Silahkan Lengkapi data diri untuk melihat slip gaji</a>
					</div>
					<?php else : ?>
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="50px">No</th>
									<th>Nama Karyawan</th>
									<th>NIK</th>
									<th>Periode Mulai</th>
									<th>Periode Selesai</th>
									<th>Gaji Pokok</th>
									<th>Lembur</th>
									<th>BPJS Kesehatan</th>
									<th>BPJS Ketenagakerjaan</th>
									<th>PPH21</th>
									<th>THP</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<?php 
								$i = 1;
								foreach ($payroll as $item):
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $item->employee_name; ?></td>
									<td><?php echo $item->id_card; ?></td>
									<td><?php echo $item->periode_start; ?></td>
									<td><?php echo $item->periode_end; ?></td>
									<td><?php echo format_rupiah((int) $item->basic_salary); ?></td>
									<td><?php echo format_rupiah((int) $item->overtime_calculation); ?></td>
									<td><?php echo format_rupiah((int) $item->bpjs_ks); ?></td>
									<td><?php echo format_rupiah((int) $item->bpjs_tk); ?></td>
									<td><?php echo format_rupiah((int) $item->tax_calculation); ?></td>
									<td><?php echo format_rupiah((int) $item->salary); ?></td>
									<td>
										<a onclick="preview(this)" class="btn-sm btn-info btn-action"  style="cursor:pointer;" data-href="<?php echo base_url("account/preview_payroll/".$item->id); ?>"><i class="fas fa-eye text-white"></i></a>
										<a href="<?php echo base_url('account/pdf_payroll/' . $item->id); ?>" style="cursor:pointer;" class="btn-sm btn-warning" target="_blank"><i class="fas fa-file-pdf"></i></a>
									</td>
								</tr>
								<?php
								$i++;
								endforeach;
								?>
							</tfoot>
						</table>
					</div>
				<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
</div>
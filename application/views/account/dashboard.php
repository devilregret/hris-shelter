<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6"><h1><?php echo $_TITLE_ ?></h1></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $_TITLE_ ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">	
		<div class="container-fluid">
			<div class="card card-default">
				<div class="card-header">
					<h3 class="card-title">
						Selamat Datang <strong><?php echo $this->session->userdata('name');?></strong>, silahkan login secara berkala (Maksimal 3 Bulan) agar akun anda terdeteksi masih aktif mencari pekerjaan.
						<p>Daftar menu berada pada pojok kiri atas garis tiga.</p>
					</h3>
				</div>
				<br>
				<?php if($candidate): ?>
					<?php if($candidate->status_completeness ==  0): ?>
					<div class="card">
						<!-- <div class="card-header">
							<h5><i class="icon fas fa-info"></i> Informasi</h5>
							<h3 class="card-title">Silahkan lengkapi data anda agar lamaran anda segera diproses</h3>
						</div> -->
						<div class="card-body">
							<div class="alert alert-info alert-dismissible">
								<h5><i class="icon fas fa-info"></i> Informasi</h5>
								Mohon di siapkan File Dokumen berikut :
								<ul>
									<li><a href="<?php echo base_url('account/register'); ?>"><span class="text">Pas Foto</span></a></li>
									<li><a href="<?php echo base_url('account/register'); ?>"><span class="text">Foto E-KTP</span></a></li>
									<li><a href="<?php echo base_url('account/register'); ?>"><span class="text">Foto Ijazah</span></a></li>
									<li><a href="<?php echo base_url('account/register'); ?>"><span class="text">Foto Kartu Keluarga</span></a></li>
								</ul>
							</div>
							<div class="card-header">
								<h3 class="card-title">Silahkan lengkapi data-data berikut agar lamaran anda segera diproses : </h3>
							</div>
							<ul class="todo-list" data-widget="todo-list">
								<?php if(count($list_language) < 2): ?>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<span class="text"><a href="<?php echo base_url('account/register'); ?>"><span class="text">Bahasa Yang Dikuasai, Minimal 2 Bahasa</span></a></span>
								</li>
								<?php endif; ?>
								<?php if(count($list_emergency) < 1): ?>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<span class="text"><a href="<?php echo base_url('account/register'); ?>"><span class="text">Kontak yang bisa dihubungi saat kondisi darurat</span></a></span>
								</li>
								<?php endif; ?>
								<?php if($candidate->document_photo == ""): ?>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<span class="text"><a href="<?php echo base_url('account/register'); ?>"><span class="text">Foto Diri</span></a></span>
								</li>
								<?php endif; ?>
								<?php if($candidate->document_id_card == ""): ?>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<span class="text"><a href="<?php echo base_url('account/register'); ?>"><span class="text">Foto KTP</span></a></span>
								</li>
								<?php endif; ?>
								<?php if($candidate->document_family_card == ""): ?>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<span class="text"><a href="<?php echo base_url('account/register'); ?>"><span class="text">Scan/Foto Kartu Keluarga</span></a></span>
								</li>
								<?php endif; ?>
								<?php if($candidate->document_certificate_education == ""): ?>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<span class="text"><a href="<?php echo base_url('account/register'); ?>"><span class="text">Scan/Foto Ijazah Terakhir</span></a></span>
								</li>
								<?php endif; ?>
							</ul>
							<?php if(!empty($list_belum_tes)) { ?> 
								<br>
								<div class="card-header">
									<h3 class="card-title">Terdapat tes yang belum anda kerjakan pada lamaran anda : </h3>
								</div>
								<ul class="todo-list" data-widget="todo-list">
									<?php foreach ($list_belum_tes as $key => $element) { ?>
										<li>
											<span class="handle">
												<i class="fas fa-ellipsis-v"></i>
											</span>
											<span class="text">
												<a href="<?php echo base_url('appeal/applied'); ?>">
													<span class="text">Lowongan : <?php echo $element->title ?> , Tes : <?php
														$txt = "<strong>";
														if ($element->count_psikotes==1) { 
															$txt .="DISC Online,";
														};
														if ($element->count_deret_angka==1) { 
															$txt .="Deret Angka,";
														};
														if ($element->count_hitung_cepat==1) {
															 $txt .="Hitung Cepat,";
															};
														if ($element->count_ketelitian==1) {
															 $txt .="Ketelitian,";
															};
														if ($element->count_kraepelin==1) { 
															$txt .="Kraepelin,";
														};

														$txt = substr($txt, 0, -1);
														$txt = $txt."</strong>";
														echo $txt;
													?>
													</span>
												</a>
											</span>
										</li>
									<?php } ?>
								</ul>
							<?php } ?>
							<br>
						</div>
					</div>
					<?php else : ?>
					<div class="card">
						<div class="card-header">
							<h5><i class="icon fas fa-info"></i> Informasi</h5>
							<?php if($candidate->status_approval <  3): ?>
								<h3 class="card-title">Data anda sudah diproses silahkan lengkapi dokumen berikut untuk memudahkan proses</h3>
							<?php else: ?>
								<h3 class="card-title">Silahkan lengkapi data berikut : </h3>
							<?php endif; ?>
						</div>
						<div class="card-body">
							<ul class="todo-list" data-widget="todo-list">
								<?php if(count($list_education) < 3): ?>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<a href="<?php echo base_url('account/register'); ?>"><span class="text">Riwayat Pendidikan, Minimal 3 Pendidikan Terakhir</span></a>
								</li>
								<?php endif; ?>
								<?php if(count($list_language) < 2): ?>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<a href="<?php echo base_url('account/register'); ?>"><span class="text">Bahasa Yang Dikuasai, Minimal 2 Bahasa</span></a>
								</li>
								<?php endif; ?>
								<?php if(count($list_emergency) < 1): ?>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<a href="<?php echo base_url('account/register'); ?>"><span class="text">Kontak yang bisa dihubungi saat kondisi darurat</span></a>
								</li>
								<?php endif; ?>
								<?php if(count($list_history) < 1): ?>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<a href="<?php echo base_url('account/register'); ?>"><span class="text">Riwayat Pekerjaan Jika Ada</span></a>
								</li>
								<?php endif; ?>
								<?php if(count($list_reference) < 1): ?>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<a href="<?php echo base_url('account/register'); ?>"><span class="text">Referensi Pimpinan terakhir anda bekerja Jika Ada</span></a>
								</li>
								<?php endif; ?>
								<?php if(count($list_skill) < 1): ?>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<a href="<?php echo base_url('account/register'); ?>"><span class="text">Keahlian/Sertifikat Jika Ada</span></a>
								</li>
								<?php endif; ?>
								<?php if($candidate->tax_number == ""): ?>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<a href="<?php echo base_url('account/register'); ?>"><span class="text">Nomor NPWP Jika Ada</span></a>
								</li>
								<?php endif; ?>
								<?php if($candidate->document_tax_number == ""): ?>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<a href="<?php echo base_url('account/register'); ?>"><span class="text">Foto NPWP Jika Ada</span></a>
								</li>
								<?php endif; ?>
								<?php if($candidate->document_police_certificate == ""): ?>
								<li>
									<span class="handle">
										<i class="fas fa-ellipsis-v"></i>
										<i class="fas fa-ellipsis-v"></i>
									</span>
									<span class="text"><a href="<?php echo base_url('account/register'); ?>"><span class="text">Scan/Foto SKCK</span></a></span>
								</li>
								<?php endif; ?>
							</ul>
						</div>
					</div>	
					<?php endif; ?>

				<?php else: ?>
					<div class="card-body">
						<div class="alert alert-info alert-dismissible">
							<h5><i class="icon fas fa-info"></i> Informasi</h5>
							Silahkan Isi dan Lengkapi <a href="<?php echo base_url('account/register'); ?>">Form Pendaftaran.</a>
							<p>&nbsp;</p>
							Mohon di siapkan File Dokumen Dengan ukuran Maksimal 500 kb untuk kompress ukuran foto bisa menggunakan (<a href="https://www.imgonline.com.ua/eng/compress-image-size.php" target="_blank">https://www.imgonline.com.ua/eng/compress-image-size.php</a>) :
							<ul>
								<li>Pas Foto</li>
								<li>Foto E-KTP</li>
								<li>Foto Ijazah</li>
								<li>Foto Kartu Keluarga</li>
							</ul>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
	$(document).ready(function(){
		<?php 
			if(!empty($list_belum_tes)){
			foreach ($list_belum_tes as $key => $element) {
				$url 		= "";
				$instruksi 	= "";
				$title		= "";

				if ($element->count_psikotes==1) { 
					$url 		= base_url('psikotes/form?vacancy='.$element->vacancy_id);
					$instruksi 	= $element->instruksi_psikotes;
					$instruksi 	= str_replace("[durasi]",$element->durasi_psikotes." Menit",$instruksi);
					$title		= "INSTRUKSI TES DISC";
				};
				if ($element->count_deret_angka==1) { 
					$url 		= base_url('deret_angka/form?vacancy='.$element->vacancy_id);
					$instruksi 	= $element->instruksi_deret_angka;
					$instruksi 	= str_replace("[durasi]",$element->durasi_deret_angka." Menit",$instruksi);
					$title		= "INSTRUKSI TES DERET ANGKA";
				};
				if ($element->count_hitung_cepat==1) {
					$url 		= base_url('hitung_cepat/form?vacancy='.$element->vacancy_id);
					$instruksi 	= $element->instruksi_hitung_cepat;
					$instruksi 	= str_replace("[durasi]",$element->durasi_hitung_cepat." Menit",$instruksi);
					$title		= "INSTRUKSI TES HITUNG CEPAT";
				};
				if ($element->count_ketelitian==1) {
					$url 		= base_url('ketelitian/form?vacancy='.$element->vacancy_id);
					$instruksi 	= $element->instruksi_ketelitian;
					$instruksi 	= str_replace("[durasi]",$element->durasi_ketelitian." Menit",$instruksi);
					$title		= "INSTRUKSI TES KETELITIAN";
				};
				if ($element->count_kraepelin==1) { 
					$url 		= base_url('kraepelin/form?vacancy='.$element->vacancy_id);
					$instruksi 	= $element->instruksi_kraepelin;
					$instruksi 	= str_replace("[durasi]",$element->durasi_kolom_kraepelin." Detik",$instruksi);
					$title		= "INSTRUKSI TES KRAEPELIN";
				};
		?>
				Swal.fire({
						icon:'info',
						title:"<?php echo $title ?>",
						html: `<?php echo $instruksi ?>`,
						width:1000,
						showDenyButton: true,
						confirmButtonText: 'Kerjakan',
						denyButtonText: `Batal`,
					}).then((result) => {
					if (result.isConfirmed) {
						window.location.href = "<?php echo $url ?>"
					} 
					})
		<?php break;} } ?>
	})

</script>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/css/adminlte.min.css"); ?>
		<?php echo asset_js("plugins/jquery/jquery.min.js"); ?>		
	</head>
	<body>
		<div class="wrapper">
			<?php echo $payroll->preview; ?>
			<div class="page-break"></div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="printContent();">Print</button>
			<button type="button" class="btn btn-default" onclick="window.close();">Tutup</button>
		</div>
		<script type="text/javascript"> 
			function printContent(){
				$('.modal-footer').remove();
				var restorepage = $('body').html();
				var printcontent = $('.invoice').clone();
				$('.invoice').empty().html(printcontent);
				window.print();
				window.close();
			}
		</script>
	</body>
</html>

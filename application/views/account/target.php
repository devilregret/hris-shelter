<?php echo asset_js("js/jquery.orgchart.js"); ?>
<?php echo asset_js("js/jquery.doubleScroll.js"); ?>
<?php echo asset_css("css/jquery.orgchart.css"); ?>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('account/target'); ?>">Sasaran Mutu</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-bullseye"></i>&nbsp;<?php echo $name; ?></h3>
				</div>
				<div class="card-body">
					<?php echo $description;?>
				</div>
			</div>
		</div>
	</section>
</div>
<style type="text/css">
	table{
		width: 100% !important;
	}
</style>
<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('account/register'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Pendaftaran</h3>
				</div>
				<div class="card-body">
					<div class="alert alert-warning alert-dismissible">
						<h5><i class="icon fas fa-warning"></i> Catatan</h5>
						<strong>Catatan : 
							Tanda <span class="text-danger">*</span> Wajib diisi<br>
							<!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tanda <span class="text-primary">*</span> Kelengkapan data -->
						</strong>
					</div>
					<?php if(isset($message)): echo $message; endif; ?>
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<form class="form-horizontal col-sm-12 mt-4" method="post" action="<?php echo base_url("account/register"); ?>" enctype="multipart/form-data">
							<div class="form-group row">
								<h2 class="card-title"><strong>A. DATA DIRI</strong></h2>
							</div>
							<?php if($registration_number != ''): ?>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">No Pendaftaran </label>
								<div class="col-sm-8">
									<input type="text" class="form-control" value="<?php echo $registration_number; ?>" readonly>
								</div>
							</div>
						<?php endif; ?>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">preview </label>
								<div class="col-sm-8">
									<?php if($document_photo != ''): ?>
										<img id="preview" src="<?php echo $document_photo; ?>" width="200px" alt="Foto" />
									<?php else: ?>
										<img id="preview" src="<?php echo asset_img("candidate.png"); ?>" width="200px" alt="Foto" />
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Foto <?php if($document_photo == ''): echo '<span class="text-danger">*</span>';  endif; ?></label>
								<div class="col-sm-5 file-upload">
									<input type="file" class="custom-file-input upload-photo upload-document" name="document_photo" accept="image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile" >Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_photo != ''): ?>
										<a href="<?php echo $document_photo; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Nama Lengkap <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<?php if($this->session->userdata('is_employee')): ?>
									<input type="text" class="form-control" placeholder="Nama Lengkap" name="full_name" value="<?php echo $full_name; ?>" style="text-transform:uppercase" readonly>
									<?php else: ?>
									<input type="text" class="form-control" placeholder="Nama Lengkap" name="full_name" value="<?php echo $full_name; ?>" style="text-transform:uppercase" required>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Nama Ibu Kandung <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<input type="text" class="form-control" placeholder="Nama Ibu Kandung" name="family_mother" value="<?php echo $family_mother; ?>" style="text-transform:uppercase" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Foto KTP <?php if($document_id_card == ''): echo '<span class="text-danger">*</span>';  endif; ?></label>
								<div class="custom-file col-sm-5 file-upload">
									<input type="file" class="custom-file-input upload-document" name="document_id_card" accept="image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_id_card != ''): ?>
										<a href="<?php echo $document_id_card; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Nomor KTP <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<!-- < ?php if($this->session->userdata('is_employee')): ?> -->
									<input type="text" maxlength="16" pattern=".{16,16}"  title="16 number" class="form-control numeric" placeholder="NIK" name="id_card" value="<?php echo $id_card; ?>" readonly>
									<!-- < ?php else: ?> -->
									<!-- <input type="text" maxlength="16" pattern=".{16,16}"  title="16 number" class="form-control numeric" placeholder="NIK" name="id_card" value="<?php echo $id_card; ?>" required> -->
									<!-- < ?php endif; ?> -->
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Foto Kartu Keluarga <?php if($document_family_card == ''): echo '<span class="text-danger">*</span>';  endif; ?></label>
								<div class="custom-file col-sm-5">
									<input type="file" class="custom-file-input upload-document" name="document_family_card" accept="image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_family_card != ''): ?>
										<a href="<?php echo $document_family_card; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Tempat Lahir <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<select class="form-control select2" name="city_birth_id" required>
										<option value="">-- PILIH KOTA --</option>
										<?php foreach ($list_city as $item) :  ?>
											<option value="<?php echo $item->id;?>" <?php if($item->id === $city_birth_id): echo "selected"; endif;?>><?php echo $item->name." - ".$item->province_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Tanggal Lahir <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<div class="input-group date" id="date_birth" data-target-input="nearest">
										<input type="text" class="form-control datetimepicker-input" data-target="#date_birth" name='date_birth' value="<?php echo $date_birth; ?>" placeholder="Tanggal Lahir"  data-toggle="datetimepicker"/>
										<div class="input-group-append" data-target="#date_birth" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="fa fa-calendar"></i></div>
										</div>
									</div>
									<!-- <input type="date" name="date_birth" id ="date_birth" class="form-control datepicker" value="<?php echo $date_birth; ?>" required> -->
								</div>
								<div class="col-sm-6"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Alamat Asal <i class="text-xs">(Sesuai KTP) </i><span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<textarea class="form-control" name="address_card" rows="3" placeholder="Contoh : Jl. Raya Nginden No.102, RT.001/RW.03, Baratajaya, Kec. Gubeng 60284" style="text-transform:uppercase" required><?php echo $address_card; ?></textarea>
									<span class="text-success">Contoh : Jl. Raya Nginden No.102, RT.001/RW.03, Baratajaya, Kec. Gubeng 60284</span>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Kota Asal <i class="text-xs">(Sesuai KTP) </i><span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<select class="form-control select2" name="city_card_id" required>
										<option value="">-- PILIH KOTA --</option>
										<?php foreach ($list_city as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $city_card_id): echo "selected"; endif;?>><?php echo $item->name." - ".$item->province_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Alamat Domisili <i class="text-xs">(Diisi jika berbeda dengan KTP)</i><span class="text-danger"></span></label>
								<div class="col-sm-8">
									<textarea class="form-control" name="address_domisili" rows="3" placeholder="Contoh : Jl. Raya Nginden No.102, RT.001/RW.03, Baratajaya, Kec. Gubeng 60284" style="text-transform:uppercase"><?php echo $address_domisili; ?></textarea>
									<span class="text-success">Contoh : Jl. Raya Nginden No.102, RT.001/RW.03, Baratajaya, Kec. Gubeng 60284</span>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Kota Domisili <i class="text-xs">(Diisi jika berbeda dengan KTP)</i><span class="text-danger"></span></label>
								<div class="col-sm-8">
									<select class="form-control select2" name="city_domisili_id">
										<option value="">-- PILIH KOTA --</option>
										<?php foreach ($list_city as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $city_domisili_id): echo "selected"; endif;?>><?php echo $item->name." - ".$item->province_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Status Tempat Tinggal <span class="text-danger"></span></label>
								<div class="col-sm-8">
									<select class="form-control select2" name="status_residance">
										<option value="MILIK SENDIRI" <?php if('MILIK SENDIRI' === $status_residance): echo "selected"; endif;?>>MILIK SENDIRI</option>
										<option value="ORANG TUA" <?php if('ORANG TUA' === $status_residance): echo "selected"; endif;?>>ORANG TUA</option>
										<option value="FAMILI" <?php if('FAMILI' === $status_residance): echo "selected"; endif;?>>FAMILI</option>
										<option value="KONTRAK" <?php if('KONTRAK' === $status_residance): echo "selected"; endif;?>>KONTRAK</option>
										<option value="KOS" <?php if('KOS' === $status_residance): echo "selected"; endif;?>>KOS</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Nomor HP/WA <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<input type="text" maxlength="14" pattern=".{10,14}"  title="10 to 14 number" class="form-control numeric" laceholder="08111111111" name="phone_number" value="<?php echo $phone_number; ?>" required>
									<span class="text-success">Nomor telepon dimulai dengan angka 0, Contoh : 081111111111</span>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Email <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<input type="email" class="form-control" placeholder="Email" name="email" value="<?php echo $email; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Akun Sosial Media <span class="text-danger"></span></label>
								<div class="col-sm-2">
									<input type="text" class="form-control" placeholder="Facebook" name="socmed_fb" value="<?php echo $socmed_fb; ?>">
								</div>
								<div class="col-sm-2">
									<input type="text" class="form-control" placeholder="Instagram" name="socmed_ig" value="<?php echo $socmed_ig; ?>">
								</div>
								<div class="col-sm-2">
									<input type="text" class="form-control" placeholder="Twitter" name="socmed_tw" value="<?php echo $socmed_tw; ?>">
								</div>
								<div class="col-sm-2">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Agama <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<select class="form-control select2" name="religion" required>
										<option value="ISLAM"  <?php if('ISLAM' === $religion): echo "selected"; endif;?>>ISLAM</option>
										<option value="KRISTEN" <?php if('KRISTEN' === $religion): echo "selected"; endif;?>>KRIST</option>
										<option value="KATOLIK" <?php if('KATOLIK' === $religion): echo "selected"; endif;?>>KATOLIK</option>
										<option value="HINDU" <?php if('HINDU' === $religion): echo "selected"; endif;?>>HINDU</option>
										<option value="BUDHA" <?php if('BUDHA' === $religion): echo "selected"; endif;?>>BUDHA</option>
										<option value="KONGHUCU" <?php if('KONGHUCU' === $religion): echo "selected"; endif;?>>KONGHUCU</option>
										<option value="ALIRAN KEPERCAYAAN" <?php if('ALIRAN KEPERCAYAAN' === $religion): echo "selected"; endif;?>>ALIRAN KEPERCAYAAN</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Status Pernikahan <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<select class="form-control select2" name="marital_status" required>
										<option value="BELUM KAWIN" <?php if('BELUM KAWIN' === $marital_status): echo "selected"; endif;?>>BELUM KAWIN</option>
										<option value="KAWIN" <?php if('KAWIN' === $marital_status): echo "selected"; endif;?>>KAWIN</option>
										<option value="CERAI HIDUP" <?php if('CERAI HIDUP' === $marital_status): echo "selected"; endif;?>>CERAI HIDUP</option>
										<option value="CERAI MATI" <?php if('CERAI MATI' === $marital_status): echo "selected"; endif;?>>CERAI MATI</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Foto Surat Nikah </label>
								<div class="custom-file col-sm-5 file-upload">
									<input type="file" class="custom-file-input upload-document" name="document_marriage_certificate" accept="image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_marriage_certificate != ''): ?>
										<a href="<?php echo $document_marriage_certificate; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Jenis Kelamin <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<select class="form-control select2" name="gender" required>
										<option value="LAKI LAKI" <?php if('LAKI LAKI' === $gender): echo "selected"; endif;?>>LAKI LAKI</option>
										<option value="PEREMPUAN" <?php if('PEREMPUAN' === $gender): echo "selected"; endif;?>>PEREMPUAN</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Tinggi & Berat Badan <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="text" maxlength="3" pattern=".{2,3}"  title="2 to 3 number" class="form-control numeric" placeholder="Tinggi Badan" name="heigh" value="<?php echo $heigh; ?>" required>
								</div>
								<label class="col-sm-2 col-form-label">CM</label>
								<div class="col-sm-2">
									<input type="text" maxlength="3" pattern=".{2,3}"  title="2 to 3 number" class="form-control numeric" placeholder="Berat Badan" name="weigh" value="<?php echo $weigh; ?>" required>
								</div>
								<label class="col-sm-2 col-form-label">KG</label>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Ukuran Seragam & Sepatu<span class="text-danger"></span></label>
								<div class="col-sm-2">
									<select class="form-control select2" name="clothing_size">
										<option value="">-- UKURAN SERAGAM --</option>
										<option value="S" <?php if('S' === $clothing_size): echo "selected"; endif;?>>S</option>
										<option value="M" <?php if('M' === $clothing_size): echo "selected"; endif;?>>M</option>
										<option value="L" <?php if('L' === $clothing_size): echo "selected"; endif;?>>L</option>
										<option value="XL" <?php if('XL' === $clothing_size): echo "selected"; endif;?>>XL</option>
										<option value="XXL" <?php if('XXL' === $clothing_size): echo "selected"; endif;?>>XXL</option>
										<option value="3XL" <?php if('3XL' === $clothing_size): echo "selected"; endif;?>>3XL</option>
									</select>
								</div>
								<label class="col-sm-2 col-form-label"></label>
								<div class="col-sm-2">
									<select class="form-control select2" name="shoe_size">
										<option value="">-- UKURAN SEPATU --</option>
										<option value="35" <?php if('35' === $shoe_size): echo "selected"; endif;?>>35</option>
										<option value="36" <?php if('36' === $shoe_size): echo "selected"; endif;?>>36</option>
										<option value="37" <?php if('37' === $shoe_size): echo "selected"; endif;?>>37</option>
										<option value="38" <?php if('38' === $shoe_size): echo "selected"; endif;?>>38</option>
										<option value="39" <?php if('39' === $shoe_size): echo "selected"; endif;?>>39</option>
										<option value="40" <?php if('40' === $shoe_size): echo "selected"; endif;?>>40</option>
										<option value="41" <?php if('41' === $shoe_size): echo "selected"; endif;?>>41</option>
										<option value="42" <?php if('42' === $shoe_size): echo "selected"; endif;?>>42</option>
										<option value="43" <?php if('43' === $shoe_size): echo "selected"; endif;?>>43</option>
										<option value="44" <?php if('44' === $shoe_size): echo "selected"; endif;?>>44</option>
										<option value="45" <?php if('45' === $shoe_size): echo "selected"; endif;?>>45</option>
										<option value="46" <?php if('46' === $shoe_size): echo "selected"; endif;?>>46</option>
									</select>
								</div>
								<label class="col-sm-2 col-form-label"></label>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Pendidikan Terakhir <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<select class="form-control select2" name="education_level" required>
										<option value="SD" <?php if('SD' === $education_level): echo "selected"; endif;?>>SD Sederajat</option>
										<option value="SMP" <?php if('SMP' === $education_level): echo "selected"; endif;?>>SMP Sederajat</option>
										<option value="SMA" <?php if('SMA' === $education_level): echo "selected"; endif;?>>SMA Sederajat</option>
										<option value="D1" <?php if('D1' === $education_level): echo "selected"; endif;?>>D1</option>
										<option value="D2" <?php if('D2' === $education_level): echo "selected"; endif;?>>D2</option>
										<option value="D3" <?php if('D3' === $education_level): echo "selected"; endif;?>>D3</option>
										<option value="D4" <?php if('D4' === $education_level): echo "selected"; endif;?>>D4</option>
										<option value="S1" <?php if('S1' === $education_level): echo "selected"; endif;?>>S1</option>
										<option value="S2" <?php if('S2' === $education_level): echo "selected"; endif;?>>S2</option>
										<option value="S3" <?php if('S3' === $education_level): echo "selected"; endif;?>>S3</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Jurusan </label>
								<div class="col-sm-8">
									<input type="text" class="form-control" placeholder="Mesin/Perkantoran/Perhotelan" name="education_majors" value="<?php echo $education_majors; ?>" style="text-transform:uppercase">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Foto Ijazah Terakhir <?php if($document_certificate_education == ''): echo '<span class="text-danger">*</span>';  endif; ?></label><p><br>&nbsp;</p>
								<div class="custom-file col-sm-5">
									<input type="file" class="custom-file-input upload-document" name="document_certificate_education" accept="image/jpeg, image/jpg">
									<label class="custom-file-label" for="customFile">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_certificate_education != ''): ?>
										<a href="<?php echo $document_certificate_education; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Posisi Yang Dilamar <i>Maksimal 3</i> <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<select multiple class="form-control select2" id="preference_job" name="preference_job[]" data-placeholder="Pilih Minat Pekerjaan" required>
										<?php foreach ($list_job as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $preference_job1 || $item->id === $preference_job2 || $item->id === $preference_job3): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Preferensi Penempatan <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<select class="form-control select2" name="preference_placement" required>
										<option value="">-- PILIH KOTA --</option>
										<?php foreach ($list_city as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $preference_placement): echo "selected"; endif;?>><?php echo $item->name." - ".$item->province_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Mendapatkan Informasi Lowongan kerja dari </label>
								<div class="col-sm-8">
									<select class="form-control select2" name="recommendation" required>
										<option value="WEBSITE SHELTER" <?php if('WEBSITE SHELTER' === $recommendation): echo "selected"; endif;?>>WEBSITE SHELTER</option>
										<option value="FACEBOOK SHELTER" <?php if('FACEBOOK SHELTER' === $recommendation): echo "selected"; endif;?>>FACEBOOK SHELTER</option>
										<option value="INSTAGRAM SHELTER" <?php if('INSTAGRAM SHELTER' === $recommendation): echo "selected"; endif;?>>INSTAGRAM SHELTER</option>
										<option value="TIKTOK SHELTER" <?php if('TIKTOK SHELTER' === $recommendation): echo "selected"; endif;?>>TIKTOK SHELTER</option>
										<option value="LINKEDIN" <?php if('LINKEDIN' === $recommendation): echo "selected"; endif;?>>LINKEDIN</option>
										<option value="JOB PORTAL LAIN" <?php if('JOB PORTAL LAIN' === $recommendation): echo "selected"; endif;?>>JOB PORTAL LAIN</option>
										<option value="REFERENSI TEMAN, SAUDARA" <?php if('REFERENSI TEMAN, SAUDARA' === $recommendation): echo "selected"; endif;?>>REFERENSI TEMAN, SAUDARA</option>
										<option value="YOUTUBE" <?php if('YOUTUBE' === $recommendation): echo "selected"; endif;?>>YOUTUBE</option>
										<option value="GOOGLE" <?php if('GOOGLE' === $recommendation): echo "selected"; endif;?>>GOOGLE</option>
										<option value="WA/TELEGRAM" <?php if('WA/TELEGRAM' === $recommendation): echo "selected"; endif;?>>WA/TELEGRAM</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Golongan Darah <span class="text-danger"></span></label>
								<div class="col-sm-8">
									<select class="form-control select2" name="blood_group">
										<option value="">&nbsp;</option>
										<option value="A" <?php if('A' === $blood_group): echo "selected"; endif;?>>A</option>
										<option value="B" <?php if('B' === $blood_group): echo "selected"; endif;?>>B</option>
										<option value="AB" <?php if('AB' === $blood_group): echo "selected"; endif;?>>AB</option>
										<option value="D1" <?php if('O' === $blood_group): echo "selected"; endif;?>>O</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Vaksin Covid-19 <span class="text-danger"></span></label>
								<div class="col-sm-8">
									<select class="form-control select2" name="covid_vaccine">
										<option value="BELUM" <?php if('BELUM' === $covid_vaccine): echo "selected"; endif;?>>BELUM</option>
										<option value="DOSIS 1" <?php if('DOSIS 1' === $covid_vaccine): echo "selected"; endif;?>>DOSIS 1</option>
										<option value="DOSIS 2" <?php if('DOSIS 2' === $covid_vaccine): echo "selected"; endif;?>>DOSIS 2</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Riwayat Hidup <i>(Curriculum Vitae)</i> <?php if($document_cv == ''): echo '<span class="text-danger"></span>';  endif; ?></label>
								<div class="col-sm-5 file-upload">
									<input type="file" class="custom-file-input upload-document" name="document_cv" accept="application/pdf">
									<label class="custom-file-label" for="customFile" >Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($document_cv != ''): ?>
										<a href="<?php echo $document_cv; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									<?php endif; ?>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<h2 class="card-title"><strong>B. KONTAK DARURAT ( KELUARGA/TEMAN YANG DEKAT ) <i class="text-xs ">(Minimal satu kontak)</i> <span class="text-danger">*</span></strong></h2>
							</div>
							<div id="emergency">
								<?php 
								if(count($list_emergency) > 0):
									foreach ($list_emergency as $item): 
								?>
								<div class="form-group row emergency_delete">
									<div class="col-sm-1">
										<a class="btn btn-danger btn-block" onclick="remove_ajax(this, <?php echo $item->id; ?>, 'emergency_delete');"><i class="fas fa-minus"></i></a>
										<input type="hidden" class="form-control" name="emergency_contact_id[]" value="<?php echo $item->id; ?>">
									</div>
									<div class="col-sm-6">
										<input type="text" class="form-control" placeholder="Nama" name="emergency_contact_name[]" value="<?php echo $item->name; ?>" style="text-transform:uppercase" required>
									</div>
									<div class="col-sm-3">
										<input type="text" class="form-control" placeholder="Hubungan" name="emergency_contact_relation[]" value="<?php echo $item->relation; ?>" style="text-transform:uppercase" required>
									</div>
									<div class="col-sm-2">
										<input type="text" maxlength="14" pattern=".{10,14}"  title="10 to 14 number" class="form-control numeric" placeholder="Nomer Telepon" name="emergency_contact_phone[]" value="<?php echo $item->phone; ?>" required>
									</div>
									<div class="col-sm-12 mt-3">
										<textarea class="form-control" name="emergency_contact_address[]" rows="3" placeholder="Alamat Kontak" style="text-transform:uppercase" required><?php echo $item->address; ?></textarea>
									</div>
								</div>
								<?php 
								endforeach;
								else: 
								?>
								<div class="form-group row">
									<div class="col-sm-1">
										<input type="hidden" class="form-control" name="emergency_contact_id[]">
									</div>
									<div class="col-sm-6">
										<input type="text" class="form-control" placeholder="Nama" name="emergency_contact_name[]" style="text-transform:uppercase" required>
									</div>
									<div class="col-sm-3">
										<input type="text" class="form-control" placeholder="Hubungan" name="emergency_contact_relation[]" style="text-transform:uppercase" required>
									</div>
									<div class="col-sm-2">
										<input type="text" maxlength="14" pattern=".{10,14}"  title="10 to 14 number" class="form-control numeric" placeholder="Nomer Telepon" name="emergency_contact_phone[]" required>
									</div>
									<div class="col-sm-12 mt-3">
										<textarea class="form-control" name="emergency_contact_address[]" rows="3" placeholder="Alamat Kontak" style="text-transform:uppercase" required></textarea>
									</div>
								</div>
								<?php endif;?>
							</div>
							<div class="form-group row">
								<div class="col-sm-3">
									<a class="btn btn-success btn-block add-emergency"><i class="fas fa-plus"></i>&nbsp;Tambah</a>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<h2 class="card-title"><strong>C. RIWAYAT PENDIDIKAN <i class="text-xs ">(Diurutkan dari yang terakhir)</i> <span class="text-danger">*</span></strong></h2>
							</div>
							<div id="education">
								<?php 
								if(count($list_education) > 0):
									foreach ($list_education as $item): 
								?>
								<div class="form-group row education_delete">
									<div class="col-sm-1">
										<a class="btn btn-danger btn-block" onclick="remove_ajax(this, <?php echo $item->id; ?>, 'education_delete');"><i class="fas fa-minus"></i></a>
										<input type="hidden" class="form-control" name="education_id[]" value="<?php echo $item->id; ?>">
									</div>
									<div class="col-sm-7">
										<input type="text" class="form-control" placeholder="Nama Sekolah/Universitas" name="education[]" value="<?php echo $item->institute; ?>" style="text-transform:uppercase">
									</div>
									<div class="col-sm-2">
										<input type="text" maxlength="4" pattern=".{4,4}"  title="4 number" class="form-control numeric"  placeholder="Mulai" name="education_start[]" value="<?php echo $item->start; ?>">
									</div>
									<div class="col-sm-2">
										<input type="text" maxlength="4" pattern=".{4,4}"  title="4 number" class="form-control numeric"  placeholder="Selesai" name="education_end[]" value="<?php echo $item->end; ?>">
									</div>
								</div>
								<?php 
								endforeach;
								else: 
								?>
								<div class="form-group row">
									<div class="col-sm-1">
										<input type="hidden" class="form-control" name="education_id[]">
									</div>
									<div class="col-sm-7">
										<input type="text" class="form-control" placeholder="Nama Sekolah/Universitas" name="education[]" style="text-transform:uppercase">
									</div>
									<div class="col-sm-2">
										<input type="text" maxlength="4" pattern=".{4,4}"  title="4 number" class="form-control numeric"  placeholder="Mulai" name="education_start[]" >
									</div>
									<div class="col-sm-2">
										<input type="text" maxlength="4" pattern=".{4,4}"  title="4 number" class="form-control numeric"  placeholder="Selesai" name="education_end[]">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-sm-1">
										<input type="hidden" class="form-control" name="education_id[]">
									</div>
									<div class="col-sm-7">
										<input type="text" class="form-control" placeholder="Nama Sekolah/Universitas" name="education[]" style="text-transform:uppercase">
									</div>
									<div class="col-sm-2">
										<input type="text" maxlength="4" pattern=".{4,4}"  title="4 number" class="form-control numeric"  placeholder="Mulai" name="education_start[]" >
									</div>
									<div class="col-sm-2">
										<input type="text" maxlength="4" pattern=".{4,4}"  title="4 number" class="form-control numeric"  placeholder="Selesai" name="education_end[]">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-sm-1">
										<input type="hidden" class="form-control" name="education_id[]">
									</div>
									<div class="col-sm-7">
										<input type="text" class="form-control" placeholder="Nama Sekolah/Universitas" name="education[]" style="text-transform:uppercase">
									</div>
									<div class="col-sm-2">
										<input type="text" maxlength="4" pattern=".{4,4}"  title="4 number" class="form-control numeric"  placeholder="Mulai" name="education_start[]" >
									</div>
									<div class="col-sm-2">
										<input type="text" maxlength="4" pattern=".{4,4}"  title="4 number" class="form-control numeric"  placeholder="Selesai" name="education_end[]">
									</div>
								</div>
								<?php endif;?>
							</div>
							<div class="form-group row">
								<div class="col-sm-3">
									<a class="btn btn-success btn-block add-education"><i class="fas fa-plus">&nbsp;</i>Tambah</a>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<div class="col-sm-10">
									<h2 class="card-title"><strong>D. BAHASA YANG DIKUASAI</strong> <span class="text-danger">*</span></h2>
								</div>
							</div>
							<div id="all-languange">
								<div id="languange">
									<?php 
									if(count($list_language) > 0):
										$i = 0;
										foreach ($list_language as $item): 
									?>
									<div class="form-group row languange_delete">
										<div class="col-sm-1">
											<?php if($i>1): ?>
											<a class="btn btn-danger btn-block" onclick="remove_ajax(this, <?php echo $item->id; ?>, 'languange_delete');"><i class="fas fa-minus"></i></a>
											<?php endif;?> 
											<input type="hidden" class="form-control" name="languange_id[]" value="<?php echo $item->id; ?>">
										</div>
										<div class="col-sm-7">
											<input type="text" class="form-control" placeholder="BAHASA INDONESIA" name="languange[]" value="<?php echo $item->language; ?>" <?php if($i<2): echo "readonly"; endif;?> >
										</div>
										<div class="col-sm-2">
											<select class="form-control select2" name="languange_verbal[]" required>
												<option value="">-Lisan-</option>
												<option value="KURANG" <?php if('KURANG' === $item->verbal): echo "selected"; endif;?>>KURANG</option>
												<option value="CUKUP" <?php if('CUKUP' === $item->verbal): echo "selected"; endif;?>>CUKUP</option>
												<option value="BAIK" <?php if('BAIK' === $item->verbal): echo "selected"; endif;?>>BAIK</option>
											</select>
										</div>
										<div class="col-sm-2">
											<select class="form-control select2" name="languange_write[]" required>
												<option value="">-Tulis-</option>
												<option value="KURANG" <?php if('KURANG' === $item->write): echo "selected"; endif;?>>KURANG</option>
												<option value="CUKUP" <?php if('CUKUP' === $item->write): echo "selected"; endif;?>>CUKUP</option>
												<option value="BAIK" <?php if('BAIK' === $item->write): echo "selected"; endif;?>>BAIK</option>
											</select>
										</div>
									</div>
									<?php 
									 $i++;
									endforeach;
									else: 
									?>
									<div class="form-group row">
										<div class="col-sm-1">
											<input type="hidden" class="form-control" name="languange_id[]">
										</div>
										<div class="col-sm-7">
											<input type="text" class="form-control" placeholder="BAHASA INDONESIA" name="languange[]" value="BAHASA INDONESIA" readonly>
										</div>
										<div class="col-sm-2">
											<select class="form-control select2" name="languange_verbal[]" required>
												<option value="">-Lisan-</option>
												<option value="KURANG">KURANG</option>
												<option value="CUKUP">CUKUP</option>
												<option value="BAIK">BAIK</option>
											</select>
										</div>
										<div class="col-sm-2">
											<select class="form-control select2" name="languange_write[]" required>
												<option value="">-Tulis-</option>
												<option value="KURANG">KURANG</option>
												<option value="CUKUP">CUKUP</option>
												<option value="BAIK">BAIK</option>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-1">
											<input type="hidden" class="form-control" name="languange_id[]">
										</div>
										<div class="col-sm-7">
											<input type="text" class="form-control" placeholder="BAHASA INGGRISS" name="languange[]" value="BAHASA INGGRIS" readonly>
										</div>
										<div class="col-sm-2">
											<select class="form-control select2" name="languange_verbal[]" required>
												<option value="">-Lisan-</option>
												<option value="KURANG">KURANG</option>
												<option value="CUKUP">CUKUP</option>
												<option value="BAIK">BAIK</option>
											</select>
										</div>
										<div class="col-sm-2">
											<select class="form-control select2" name="languange_write[]" required>
												<option value="">-Tulis-</option>
												<option value="KURANG">KURANG</option>
												<option value="CUKUP">CUKUP</option>
												<option value="BAIK">BAIK</option>
											</select>
										</div>
									</div>
									<?php endif;?>
								</div>
								<div class="form-group row">
									<div class="col-sm-3">
										<a class="btn btn-success btn-block add-language"><i class="fas fa-plus">&nbsp;</i>Tambah</a>
									</div>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<div class="col-sm-10">
									<h2 class="card-title"><strong>E. KEAHLIAN YANG DIKUASAI</strong></h2>
								</div>
								<div class="col-sm-2 float-right">
									<div class="row float-right">
										<button type="button" class="btn btn-primary btn" style="margin-right: 17px;" data-page="all-skill" onclick="showhide(this);">Lihat</button>
									</div>
								</div>
							</div>
							<div id="all-skill" hidden="true">
								<div id="skill">
									<?php 
									if(count($list_skill) > 0):
										foreach ($list_skill as $item): 
									?>
									<div class="form-group row skill_delete">
										<div class="col-sm-1">
											<a class="btn btn-danger btn-block" onclick="remove_ajax(this, <?php echo $item->id; ?>, 'skill_delete');"><i class="fas fa-minus"></i></a>
											<input type="hidden" class="form-control" name="skill_id[]" value="<?php echo $item->id; ?>">
										</div>
										<div class="col-sm-6">
											<input type="text" class="form-control" placeholder="Nama Pelatihan" name="skill[]" value="<?php echo $item->certificate_name; ?>" style="text-transform:uppercase">
										</div>
										<div class="col-sm-5">
											<input type="text" class="form-control" placeholder="Nomor Sertifikat" name="certificate_number[]" value="<?php echo $item->certificate_number; ?>" style="text-transform:uppercase">
										</div>
										<div class="form-group col-sm-12 row mt-3">
											<div class="col-sm-1">
												<?php if($item->certificate_document != ''): ?>
													<a href="<?php echo $item->certificate_document; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
												<?php endif; ?>
											</div>
											<div class="custom-file col-sm-11">
												<input type="file" class="custom-file-input upload-document" name="skill_certificate[]" accept="image/jpeg, image/jpg">
												<label class="custom-file-label" for="customFile">Pilih file</label>
											</div>
										</div>
									</div>
									<?php 
									endforeach;
									else: 
									?>
									<div class="form-group row">
										<div class="col-sm-1">
											<input type="hidden" class="form-control" name="skill_id[]">
										</div>
										<div class="col-sm-6">
											<input type="text" class="form-control" placeholder="Nama Pelatihan" name="skill[]" value="" style="text-transform:uppercase">
										</div>
										<div class="col-sm-5">
											<input type="text" class="form-control" placeholder="Nomor Sertifikat" name="certificate_number[]" value="" style="text-transform:uppercase">
										</div>
										<div class="form-group col-sm-12 row mt-3">
											<div class="col-sm-1">
											</div>
											<div class="custom-file col-sm-11">
												<input type="file" class="custom-file-input upload-document" name="skill_certificate[]" accept="image/jpeg, image/jpg">
												<label class="custom-file-label" for="customFile">Pilih file</label>
											</div>
										</div>
									</div>
									<?php endif;?>
								</div>
								<div class="form-group row">
									<div class="col-sm-3">
										<a class="btn btn-success btn-block add-skill"><i class="fas fa-plus">&nbsp;</i>Tambah</a>
									</div>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<div class="col-sm-10">
									<h2 class="card-title"><strong>F. RIWAYAT PEKERJAAN <i class="text-xs ">(Diurutkan dari yang terakhir)</i></strong></h2>
								</div>
								<div class="col-sm-2 float-right">
									<div class="row float-right">
										<button type="button" class="btn btn-primary btn" style="margin-right: 17px;" data-page="all-history" onclick="showhide(this);">Lihat</button>
									</div>
								</div>
							</div>
							<div id="all-history" hidden="true">
								<div id="history">
									<?php 
									if(count($list_history) > 0):
										foreach ($list_history as $item): 
									?>
									<div class="form-group row history_delete">
										<div class="col-sm-1">
											<a class="btn btn-danger btn-block" onclick="remove_ajax(this,<?php echo $item->id; ?>, 'history_delete');"><i class="fas fa-minus"></i></a>
											<input type="hidden" class="form-control" name="history_id[]" value="<?php echo $item->id; ?>">
										</div>
										<div class="col-sm-5">
											<input type="text" class="form-control" placeholder="Nama Perusahaan" name="history_company[]" value="<?php echo $item->company_name?>" style="text-transform:uppercase">
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" placeholder="Posisi Terakhir" name="history_company_position[]" value="<?php echo $item->position?>" style="text-transform:uppercase">
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control"  placeholder="Lama Bekerja" name="history_company_period[]" value="<?php echo $item->period?>" style="text-transform:uppercase">
										</div>
										<div class="col-sm-2">
											<input type="text" maxlength="15" pattern=".{0,15}" class="form-control numeric rupiah" placeholder="Gaji Terakhir" name="history_company_salary[]" value="<?php echo $item->salary?>">
										</div>
										<div class="row col-sm-12 mt-3">
											<div class="col-sm-6">
												<textarea class="form-control" name="history_company_address[]" rows="3" placeholder="Alamat/Kota Perusahaan" style="text-transform:uppercase"><?php echo $item->company_address?></textarea>
											</div>
											<div class="col-sm-6">
												<textarea class="form-control" name="history_company_resign_note[]" rows="3" placeholder="Alasan Keluar (Resign/PHK)"><?php echo $item->resign_note?></textarea>
											</div>
										</div>
									</div>
									<?php 
									endforeach;
									else: 
									?>
									<div class="form-group row">
										<div class="col-sm-1">
											<input type="hidden" class="form-control" name="history_id[]">
										</div>
										<div class="col-sm-5">
											<input type="text" class="form-control" placeholder="Nama Perusahaan" name="history_company[]" value="" style="text-transform:uppercase">
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" placeholder="Posisi Terakhir" name="history_company_position[]" value="" style="text-transform:uppercase">
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control"  placeholder="Lama Bekerja" name="history_company_period[]" value="" style="text-transform:uppercase">
										</div>
										<div class="col-sm-2">
											<input type="text" maxlength="15" pattern=".{0,15}" class="form-control numeric rupiah" placeholder="Gaji Terakhir" name="history_company_salary[]" value="">
										</div>
										<div class="row col-sm-12 mt-3">
											<div class="col-sm-6">
												<textarea class="form-control" name="history_company_address[]" rows="3" placeholder="Alamat/Kota Perusahaan" style="text-transform:uppercase"></textarea>
											</div>
											<div class="col-sm-6">
												<textarea class="form-control" name="history_company_resign_note[]" rows="3" placeholder="Alasan Keluar (Resign/PHK)"></textarea>
											</div>
										</div>
									</div>
									<?php endif;?>
								</div>
								<div class="form-group row">
									<div class="col-sm-3">
										<a class="btn btn-success btn-block add-history"><i class="fas fa-plus">&nbsp;</i>Tambah</a>
									</div>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<div class="col-sm-10">
									<h2 class="card-title"><strong>G. REFERENSI PIMPINAN TERAKHIR BEKERJA</strong></h2>
								</div>
								<div class="col-sm-2 float-right">
									<div class="row float-right">
										<button type="button" class="btn btn-primary btn" style="margin-right: 17px;" data-page="all-reference" onclick="showhide(this);">Lihat</button>
									</div>
								</div>
							</div>
							<div id="all-reference" hidden="true">
								<div id="reference">
									<?php 
									if(count($list_reference) > 0):
										foreach ($list_reference as $item): 
									?>
									<div class="form-group row reference_delete">
										<div class="col-sm-1">
											<a class="btn btn-danger btn-block" onclick="remove_ajax(this, <?php echo $item->id; ?>, 'reference_delete');"><i class="fas fa-minus"></i></a>
											<input type="hidden" class="form-control" name="reference_id[]" value="<?php echo $item->id; ?>">
										</div><div class="col-sm-3">
											<input type="text" class="form-control" placeholder="Nama" name="reference_name[]" value="<?php echo $item->name; ?>" style="text-transform:uppercase">
										</div>
										<div class="col-sm-3">
											<input type="text" class="form-control" placeholder="Nama Perusahaan" name="reference_company[]" value="<?php echo $item->company_name; ?>" style="text-transform:uppercase">
										</div>
										<div class="col-sm-3">
											<input type="text" class="form-control" placeholder="Posisi" name="reference_position[]" value="<?php echo $item->position; ?>">
										</div>
										<div class="col-sm-2">
											<input type="text" maxlength="14" pattern=".{10,14}"  title="10 to 14 number" class="form-control numeric" placeholder="Nomor Telepon" name="reference_phone[]" value="<?php echo $item->phone; ?>">
										</div>
									</div>
									<?php 
									endforeach;
									else: 
									?>
									<div class="form-group row">
										<div class="col-sm-1">
											<input type="hidden" class="form-control" name="reference_id[]">
										</div>
										<div class="col-sm-3">
											<input type="text" class="form-control" placeholder="Nama" name="reference_name[]" value="" style="text-transform:uppercase">
										</div>
										<div class="col-sm-3">
											<input type="text" class="form-control" placeholder="Nama Perusahaan" name="reference_company[]" value="" style="text-transform:uppercase">
										</div>
										<div class="col-sm-3">
											<input type="text" class="form-control" placeholder="Posisi" name="reference_position[]" value="" style="text-transform:uppercase">
										</div>
										<div class="col-sm-2">
											<input type="text" maxlength="14" pattern=".{10,14}"  title="10 to 14 number" class="form-control numeric" placeholder="Nomor Telepon" name="reference_phone[]" value="">
										</div>
									</div>
									<?php endif;?>
								</div>
								<div class="form-group row">
									<div class="col-sm-3">
										<a class="btn btn-success btn-block add-reference"><i class="fas fa-plus">&nbsp;</i>Tambah</a>
									</div>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<div class="col-sm-10">
									<h2 class="card-title"><strong>H. RIWAYAT KESEHATAN</strong></h2>
								</div>
								<div class="col-sm-2 float-right">
									<div class="row float-right">
										<button type="button" class="btn btn-primary btn" style="margin-right: 17px;" data-page="all-health" onclick="showhide(this);">Lihat</button>
									</div>
								</div>
							</div>
							<div id="all-health" hidden="true">
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Penyakit Ringan yang sering dialami? <span class="text-danger"></span></label>
									<div class="col-sm-8">
										<textarea class="form-control" name="illness_minor" rows="3" placeholder="Sakit yang dialami"><?php echo $illness_minor; ?></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Penyakit Berat yang sering dialami? <span class="text-danger"></span></label>
									<div class="col-sm-8">
										<textarea class="form-control" name="illness_serious" rows="3" placeholder="Perawatan yang pernah dilakukan"><?php echo $illness_serious; ?></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Pernah dirawat di Rumah Sakit? <span class="text-danger"></span></label>
									<div class="col-sm-8">
										<textarea class="form-control" name="illness_treated" rows="3" placeholder="Perawatan yang pernah dilakukan"><?php echo $illness_treated; ?></textarea>
									</div>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<div class="col-sm-10">
									<h2 class="card-title"><strong>I. INFORMASI TAMBAHAN</strong></h2>
								</div>
								<div class="col-sm-2 float-right">
									<div class="row float-right">
										<button type="button" class="btn btn-primary btn" style="margin-right: 17px;" data-page="all-additional" onclick="showhide(this);">Lihat</button>
									</div>
								</div>
							</div>
							<div id="all-additional" hidden="true">
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Pernah terlibat urusan hukum dengan pihak berwajib? <span class="text-danger"></span></label>
									<div class="col-sm-8">
										<textarea class="form-control" name="crime_notes" rows="3" placeholder="Kesalahan yang dilakukan"><?php echo $crime_notes; ?></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Memiliki Rekening Bank Mandiri ? <span class="text-danger"></span></label>
									<div class="col-sm-2">
										<select class="form-control select2 bank">
											<option value="ya" <?php if('' != $bank_account): echo "selected"; endif;?>>Ya</option>
											<option value="tidak" <?php if('' == $bank_account): echo "selected"; endif;?>>Tidak</option>
										</select>
									</div>
									<div class="col-sm-6"></div>
								</div>
								<div class="form-group row open-mandiri" <?php if('' != $bank_account): echo "hidden"; endif;?>>
									<label class="col-sm-4 col-form-label">Bersedia Membuka Rekening Bank Mandiri ? <span class="text-danger"></span></label>
									<div class="col-sm-2">
										<select class="form-control select2" name="bank_account_create">
											<option value="ya" <?php if('ya' == $bank_account_create): echo "selected"; endif;?>>Ya</option>
											<option value="tidak" <?php if('tidak' == $bank_account_create): echo "selected"; endif;?>>Tidak</option>
										</select>
									</div>
									<div class="col-sm-6"></div>
								</div>
								<div class="form-group row close-mandiri" <?php if('' == $bank_account): echo "hidden"; endif;?>>
									<label class="col-sm-4 col-form-label">Nomor Rekening</label>
									<div class="col-sm-8">
										<?php if($this->session->userdata('is_employee')): ?>
										<input type="text" class="form-control numeric" placeholder="Nomor Rekening" name="bank_account" value="<?php echo $bank_account; ?>" readonly>
										<?php else: ?>
										<input type="text" class="form-control numeric" placeholder="Nomor Rekening" name="bank_account" value="<?php echo $bank_account; ?>">
										<?php endif; ?>
									</div>
								</div>
								<div class="form-group row close-mandiri" <?php if('' == $bank_account): echo "hidden"; endif;?>>
									<label class="col-sm-4 col-form-label">Atas Nama Rekening</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" placeholder="Atas Nama Rekening" name="bank_account_name" value="<?php echo $bank_account_name; ?>" style="text-transform:uppercase">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Nomor NPWP</label>
									<div class="col-sm-8">
										<input type="text" class="form-control numeric" placeholder="Nomor NPWP" name="tax_number" value="<?php echo $tax_number; ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Foto NPWP </label>
									<div class="custom-file col-sm-5 file-upload">
										<input type="file" class="custom-file-input upload-document" name="document_tax_number" accept="image/jpeg, image/jpg">
										<label class="custom-file-label" for="customFile">Pilih file</label>
									</div>
									<div class="col-sm-2">
										<?php if($document_tax_number != ''): ?>
											<a href="<?php echo $document_tax_number; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
										<?php endif; ?>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Apakah Anda Mempunyai BPJS Kesehatan? <span class="text-danger"></span></label>
									<div class="col-sm-2">
										<select class="form-control select2" name="health_insurance">
											<option value="tidak" <?php if('tidak' === $health_insurance): echo "selected"; endif;?>>Tidak</option>
											<option value="ya" <?php if('ya' === $health_insurance): echo "selected"; endif;?>>Ya</option>
										</select>
									</div>
									<div class="col-sm-6"></div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Jaminan Kesehatan apa yang anda miliki? </label>
									<div class="col-sm-8">
										<input type="text" class="form-control" placeholder="PBI/Jamkesda/Tidak Memiliki" name="health_insurance_note" value="<?php echo $health_insurance_note; ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Bersediakah saudara/i mengikuti BPJS sesuia peratura  Pemerintah? <span class="text-danger"></span></label>
									<div class="col-sm-2">
										<select class="form-control select2" name="health_insurance_company">
											<option value="ya" <?php if('ya' === $health_insurance_company): echo "selected"; endif;?>>Ya</option>
											<option value="tidak" <?php if('tidak' === $health_insurance_company): echo "selected"; endif;?>>Tidak</option>
										</select>
									</div>
									<div class="col-sm-6"></div>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<div class="col-sm-10">
									<h2 class="card-title"><strong>J. KELENGKAPAN DOKUMEN</strong></h2>
								</div>
								<div class="col-sm-2 float-right">
									<div class="row float-right">
										<button type="button" class="btn btn-primary btn" style="margin-right: 17px;" data-page="all-document" onclick="showhide(this);">Lihat</button>
									</div>
								</div>
							</div>
							<div id="all-document" hidden="true">
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Surat Sehat </label>
									<div class="custom-file col-sm-5">
										<input type="file" class="custom-file-input upload-document" name="document_doctor_note" accept="image/jpeg, image/jpg">
										<label class="custom-file-label" for="customFile">Pilih file</label>
									</div>
									<div class="col-sm-2">
										<?php if($document_doctor_note != ''): ?>
											<a href="<?php echo $document_doctor_note; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
										<?php endif; ?>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Masa Berlaku Surat Sehat s/d <span class="text-danger"></span></label>
									<div class="col-sm-2">
										<input type="date" name="period_doctor_note" class="form-control datepicker" value="<?php echo $period_doctor_note; ?>">
									</div>
									<div class="col-sm-6"></div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">SKCK </label>
									<div class="custom-file col-sm-5">
										<input type="file" class="custom-file-input upload-document" name="document_police_certificate" accept="image/jpeg, image/jpg">
										<label class="custom-file-label" for="customFile">Pilih file</label>
									</div>
									<div class="col-sm-2">
										<?php if($document_police_certificate != ''): ?>
											<a href="<?php echo $document_police_certificate; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<hr>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" name="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		$('#date_birth').datetimepicker({
			format: 'DD/MM/YYYY',
			pickTime: false  
		});

		$('.upload-document').imageUploadResizer({
			max_width: 1200, // Defaults 1000
			max_height: 800, // Defaults 1000
			quality: 1, // Defaults 1
			do_not_resize: ['gif', 'svg'], // Defaults []
		});
		
		var last_valid_selection = null;
		$('#preference_job').change(function(event) {
			if ($(this).val().length > 3) {
				$(this).val(last_valid_selection);
			} else {
				last_valid_selection = $(this).val();
			}
		});

		$(".add-emergency").click(function() {
			var content =
			'<div class="form-group row"><div class="col-sm-1"> <a class="btn btn-danger btn-block" onclick="remove(this);"><i class="fas fa-minus"></i></a> <input type="hidden" class="form-control" name="emergency_contact_id[]"> </div>' +
			'<div class="col-sm-6"><input type="text" class="form-control" placeholder="Nama" name="emergency_contact_name[]" style="text-transform:uppercase" required></div>' +
			'<div class="col-sm-3"><input type="text" class="form-control" placeholder="Hubungan" name="emergency_contact_relation[]" style="text-transform:uppercase" required></div>' +
			'<div class="col-sm-2"><input type="text" maxlength="14" pattern=".{10,14}"  title="10 to 14 number" class="form-control numeric" placeholder="Nomer Telepon" name="emergency_contact_phone[]" required></div>' +
			'<div class="col-sm-12 mt-3"><textarea class="form-control" name="emergency_contact_address[]" rows="3" placeholder="Alamat Kontak" style="text-transform:uppercase" required></textarea></div></div>';
			$("#emergency").append(content);
		});

		$(".add-education").click(function() {
			var content = 
			'<div class="form-group row"><div class="col-sm-1"><a class="btn btn-danger btn-block" onclick="remove(this);"><i class="fas fa-minus"></i></a> <input type="hidden" class="form-control" name="education_id[]"></div>' +
			'<div class="col-sm-7"><input type="text" class="form-control" placeholder="Nama Sekolah/Universitas" name="education[]" style="text-transform:uppercase"></div>' +
			'<div class="col-sm-2"><input type="text" maxlength="4" pattern=".{4,4}"  title="4 number" class="form-control numeric"  placeholder="Mulai" name="education_start[]" ></div>' +
			'<div class="col-sm-2"><input type="text" maxlength="4" pattern=".{4,4}"  title="4 number" class="form-control numeric"  placeholder="Selesai" name="education_end[]"></div></div>';
			$("#education").append(content);
		});

		$(".add-language").click(function() {
			var content = 
			'<div class="form-group row"><div class="col-sm-1"><a class="btn btn-danger btn-block" onclick="remove(this);"><i class="fas fa-minus"></i></a> <input type="hidden" class="form-control" name="languange_id[]"></div>' +
			'<div class="col-sm-7"><input type="text" class="form-control" placeholder="Bahasa yang dikuasai" name="languange[]" value="" style="text-transform:uppercase"></div>' +
			'<div class="col-sm-2"><select class="form-control select2" name="languange_verbal[]"><option value="">-Lisan-</option><option value="KURANG">KURANG</option><option value="CUKUP">CUKUP</option><option value="BAIK">BAIK</option></select></div>' +
			'<div class="col-sm-2"><select class="form-control select2" name="languange_write[]"><option value="">-Tulis-</option><option value="KURANG">KURANG</option><option value="CUKUP">CUKUP</option><option value="BAIK">BAIK</option></select></div></div>';

			$("#languange").append(content);
		});

		$(".add-skill").click(function() {
			var content = 
			'<div class="form-group row"><div class="col-sm-1"><a class="btn btn-danger btn-block" onclick="remove(this);"><i class="fas fa-minus"></i></a><input type="hidden" class="form-control" name="skill_id[]"></div>' +
			'<div class="col-sm-6"><input type="text" class="form-control" placeholder="Nama Pelatihan" name="skill[]" value="" style="text-transform:uppercase"></div>' +
			'<div class="col-sm-5"><input type="text" class="form-control" placeholder="Nomor Sertifikat" name="certificate_number[]" value="" style="text-transform:uppercase"></div>' +
			'<div class="form-group col-sm-12 row mt-3"><div class="col-sm-1"></div>' +
			'<div class="custom-file col-sm-11"><input type="file" class="custom-file-input upload-document" name="skill_certificate[]" accept="image/jpeg, image/jpg"><label class="custom-file-label" for="customFile">Choose file</label></div>' +
			'</div></div>';

			$("#skill").append(content);
		});

		$(".add-history").click(function() {
			var content = 
			'<div class="form-group row"><div class="col-sm-1"><a class="btn btn-danger btn-block" onclick="remove(this);"><i class="fas fa-minus"></i></a> <input type="hidden" class="form-control" name="history_id[]"></div>' +
			'<div class="col-sm-5"><input type="text" class="form-control" placeholder="Nama Perusahaan" name="history_company[]" value="" style="text-transform:uppercase"></div>' +
			'<div class="col-sm-2"><input type="text" class="form-control" placeholder="Posisi Terakhir" name="history_company_position[]" value="" style="text-transform:uppercase"></div>' +
			'<div class="col-sm-2"><input type="text" class="form-control"  placeholder="Lama Bekerja" name="history_company_period[]" value=""></div>' +
			'<div class="col-sm-2"><input type="text" maxlength="15" pattern=".{0,15}" class="form-control numeric rupiah" placeholder="Gaji Terakhir" name="history_company_salary[]" value=""></div>' +
			'<div class="row col-sm-12 mt-3"><div class="col-sm-6"><textarea class="form-control" name="history_company_address[]" rows="3" placeholder="Alamat/Kota Perusahaan" style="text-transform:uppercase"></textarea></div>' +
			'<div class="col-sm-6"><textarea class="form-control" name="history_company_resign_note[]" rows="3" placeholder="Alasan Keluar (Resign/PHK)"></textarea></div></div></div>' ;

			$("#history").append(content);
		});

		$(".add-reference").click(function() {
			var content = 
			'<div class="form-group row"><div class="col-sm-1"><a class="btn btn-danger btn-block" onclick="remove(this);"><i class="fas fa-minus"></i></a> <input type="hidden" class="form-control" name="reference_id[]"></div>' +
			'<div class="col-sm-3"><input type="text" class="form-control" placeholder="Nama" name="reference_name[]" value="" style="text-transform:uppercase"></div>' +
			'<div class="col-sm-3"><input type="text" class="form-control" placeholder="Nama Perusahaan" name="reference_company[]" value="" style="text-transform:uppercase"></div>' +
			'<div class="col-sm-3"><input type="text" class="form-control" placeholder="Posisi" name="reference_position[]" value="" style="text-transform:uppercase"></div>' +
			'<div class="col-sm-2"><input type="text" maxlength="14" pattern=".{10,14}"  title="10 to 14 number" class="form-control numeric" placeholder="Nomor Telepon" name="reference_phone[]" value=""></div></div>' ;

			$("#reference").append(content);
		});

		$( ".bank" ).change(function() {
			if($(this).val() == 'ya'){
				$(".close-mandiri").prop('hidden', false);
				$(".open-mandiri").prop('hidden', true);
			}else{
				$(".close-mandiri").prop('hidden', true);
				$(".open-mandiri").prop('hidden', false);
			}
		});

	});

	function remove(el){
		$(el).parent().parent().remove();
	}

	function remove_ajax(el, id, group){
		if (confirm('Anda yakin untuk menghapus data  ini?')) 
		{
			if($('.'+group).length <2){
				alert('Maaf Minimal harus ada 1 data');
			}else{
				$.ajax({
					url: "<?php echo base_url('account/'); ?>"+group,
					type: "POST",
					cache: false,
					data:{
						id: id
					},
					success: function(response){
						var result = JSON.parse(response);
						if(result.code==200){
							$(el).parent().parent().remove();
						}
					}
				});
			}
		}
		return false;
	}

	$("#date_birth").change(function() {
		if(calculate_age(this.value) < 18){
			alert("Maaf Usia Anda masih kurang dari 18 Tahun");
			$(".btn-form").prop('disabled', true);
		}else{
			$(".btn-form").prop('disabled', false);
		}
	});
	function calculate_age(dob) { 
		var today = new Date();
	    var birth_date = new Date(dob);
	    var age = today.getFullYear() - birth_date.getFullYear();
	    var m = today.getMonth() - birth_date.getMonth();
	    if (m < 0 || (m === 0 && today.getDate() < birth_date.getDate())) {
	        age--;
	    }
	    return age;
	}
	function showhide(el){
		var condition = $(el).html();
		var page = $(el).data('page');
		if(condition == 'Lihat'){
			$('#'+page).removeAttr('hidden');
			$(el).html('Tutup');
		}else{
			$('#'+page).attr('hidden', 'true');
			$(el).html('Lihat');
		}
		console.log(page);
	}
</script>
<style type="text/css">
	input[type="text" i], input[type="email" i], input[type="select" i], input[type="date" i], .select2-container, textarea {
    	margin-left: -7px;
	}
</style>
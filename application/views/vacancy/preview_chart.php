<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $_TITLE_?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo asset_css("css/adminlte.min.css"); ?>
		<?php echo asset_css("plugins/fontawesome-free/css/all.min.css"); ?>
		<?php echo asset_css("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"); ?>
		<?php echo asset_css("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"); ?>
		<?php echo asset_css("css/adminlte.css"); ?>
		<?php echo asset_css("css/main.css"); ?>
 		<?php echo asset_js("plugins/jquery/jquery.min.js"); ?>	
		<?php echo asset_js("js/image-resizer.js"); ?>
	</head>
	<body>
		<section class="content">
			<div class="container-fluid">
				<div class="card card-primary card-outline col-12 mt-2">
					<div class="card-header">
						<h3 class="card-title"><i class="fas fa-eye"></i>&nbsp;Riwayat Pengajuan</h3>
					</div>
					<div class="row">
						<div class="col-12 table-responsive">
							<div class="card">
								<div class="card-header border-0">
									<h3 class="card-title">
										<i class="fas fa-th mr-1"></i>
										Resume Pelamar
									</h3>
								</div>
								<div class="card-body">
									<canvas class="chart" id="applicant-chart" style="min-height: 300px; height: 300px; max-height: 250px; max-width: 100%;"></canvas>
								</div>
								<div class="d-flex flex-row justify-content-end">
									<span class="mr-2">
										<i class="fas fa-square" style="color:#ff0000;"></i> Kebutuhan
									</span>
									<span>
										<i class="fas fa-square" style="color:#006600;"></i> Diterima
									</span>
									<span>
										<i class="fas fa-square" style="color:#ff9900;"></i> Diajukan
									</span>
									<span>
										<i class="fas fa-square" style="color:#0044ff;"></i> Pelamar
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="window.close();">Tutup</button>
       	</div>
	</body>
</html>

<?php echo asset_js("plugins/chart.js/Chart.min.js"); ?>
<script>
$(document).ready(function() {
	$.ajax({
		url: "<?php echo base_url('vacancy/chart_applicant/'.$vacancy_id); ?>",
		dataType: "json",
		success: function(data) {

			var areaChartData = {
			labels  : data.labels,
				datasets: [{
					label: 'Pelamar',
					fill: false,
					borderWidth: 2,
					lineTension: 0,
					spanGaps: true,
					borderColor: '#0044ff',
					pointRadius: 3,
					pointHoverRadius: 7,
					pointColor: '#0044ff',
					pointBackgroundColor: '#0044ff',
					data : data.candidate
				},
				{
					label: 'Diajukan',
					fill: false,
					borderWidth: 2,
					lineTension: 0,
					spanGaps: true,
					borderColor: '#ff9900',
					pointRadius: 3,
					pointHoverRadius: 7,
					pointColor: '#ff9900',
					pointBackgroundColor: '#ff9900',
					data : data.submision
				},
				{
					label: 'Diterima',
					fill: false,
					borderWidth: 2,
					lineTension: 0,
					spanGaps: true,
					borderColor: '#006600',
					pointRadius: 3,
					pointHoverRadius: 7,
					pointColor: '#006600',
					pointBackgroundColor: '#006600',
					data : data.accept
				},
				{
					label: 'Kebutuhan',
					fill: false,
					borderWidth: 2,
					lineTension: 0,
					spanGaps: true,
					borderColor: '#ff0000',
					pointRadius: 3,
					pointHoverRadius: 7,
					pointColor: '#ff0000',
					pointBackgroundColor: '#ff0000',
					data : data.needs
				}]
			}

			canvas = document.getElementById('canvas');
			var barChartCanvas = $('#applicant-chart').get(0).getContext('2d')
			var barChartData = jQuery.extend(true, {}, areaChartData)
			var temp0 = areaChartData.datasets[0]

			barChartData.datasets[0] = temp0


			var barChartOptions = {
				maintainAspectRatio: false,
				responsive: true,
				legend: {
					display: false
				},
				scales: {
					xAxes: [{
						ticks: {
							fontColor: '#0d0400'
						},
						gridLines: {
							display: false,
							color: '#0d0400',
							drawBorder: false
						}
					}],
					yAxes: [{
						ticks: {
							stepSize: 500,
							fontColor: '#0d0400'
						},
						gridLines: {
							display: true,
							color: '#0d0400',
							drawBorder: false
						}
					}]
				},
				hover: {
					animationDuration: 0
				},
				animation: {
					duration: 1,
					onComplete: function () {
						var chartInstance = this.chart,
						ctx = chartInstance.ctx;
						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
						ctx.textAlign = 'center';
						ctx.textBaseline = 'bottom';
						this.data.datasets.forEach(function (dataset, i) {
							var meta = chartInstance.controller.getDatasetMeta(i);
							meta.data.forEach(function (bar, index) {
								var data = dataset.data[index];
								ctx.fillText(data, bar._model.x, bar._model.y - 5);
							});
						});
					}
				}
			}

			var barChart = new Chart(barChartCanvas, {
				type: 'line', 
				data: barChartData,
				options: barChartOptions
			});
		}
	});

});
</script>
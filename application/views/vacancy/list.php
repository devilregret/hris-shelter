<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('vacancy/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<style>
			#data-table tr th {
				text-align:center;
			};
		</style>
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Lowongan</h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<a href="<?php echo base_url("vacancy/form"); ?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Tambah Lowongan</a> &nbsp;
					</div>
				</div>
				<div class="card-body">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th id='title'>Judul</th>
									<th id='start_date'>Mulai Tayang</th>
									<th id='end_date '>Selesai Tayang</th>
									<th id='province'>Provinsi</th>
									<th id='branch'>Branch</th>
									<th id='company_name'>Bisnis Unit</th>
									<th id='site_name'>Site Bisnis</th>
									<th id='position_name'>Posisi</th>
									<th id='needs'>Jumlah Kebutuhan</th>
									<th id='applicant'>Pelamar</th>
									<th id='submission'>Diajukan</th>
									<th id='accepted'>Diterima</th>
									<th id='daftar_tes'>Daftar Tes</th>
									<th width="105px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<div class="modal fade" id="modalDownloadHasil" aria-labelledby="modalDownloadHasil" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-titles">Download Hasil</h5>
        <button data-bs-dismiss="modal" aria-label="Close" class="close-modal btn btn-secondary"><i class="fas fa-times"></i></button>
      </div>
      <div class="modal-body">
		<input type="hidden" id="select-company" value="all">
		<input type="hidden" id="select-site" value="all">
		<input type="hidden" id="select-position" value="all">
		<input type="hidden" id="select-province" value="all">
		<input type="hidden" id="select-lowongan" value="all">
		<input type="hidden" id="select-employee" value="all">

		<div class="form-group">
			<label>Jenis Tes</label>
			<select multiple data-allow-clear="1" id="select-tes" class="select2">
				<option value="psikotes" selected>DISC</option>
				<option value="deret_angka" selected>Deret Angka</option>
				<option value="hitung_cepat" selected>Hitung Cepat</option>
				<option value="ketelitian" selected>Ketelitian</option>
				<option value="kraepelin" selected>Kraepelin</option>
			</select>
		</div>
		<div class="form-group">
			<label>Urutkan Berdasarkan</label>
			<select id="select-order" class="select2">
				<option value="peserta" selected>Peserta</option>
				<option value="tes">Tes</option>
			</select>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary close-modal" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn-download-hasil">Download</button>
      </div>
    </div>
  </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
		function rekap_hasil (element) {
			let id = $(element).data('id');
			$('#modalDownloadHasil').modal('show');
			$('#select-lowongan').val(id);
		}

	$(document).ready(function(){
		$('.close-modal').click(function(){
			$('#modalDownloadHasil').modal('hide');
		})

		$('#btn-download-hasil').click(function(){
			let vCompany = $('#select-company').val();
			let vSite = $('#select-site').val();
			let vPosition = $('#select-position').val();
			let vProvince = $('#select-province').val();
			let vLowongan = $('#select-lowongan').val();
			let vEmployee = $('#select-employee').val();
			let vOrder = $('#select-order').val();
			let vTes = $('#select-tes').val();

			if(vTes ==""){
				Swal.fire({
				icon: 'warning',
				title: 'Pemberitahuan',
				html: "Belum memilih Tes",
			})
			}

			let url = "/history_tes/result?tes="+vTes+"&company_id="+vCompany+"&site_id="+vSite+"&position_id="+vPosition+"&province_id="+vProvince+"&vacancy_id="+vLowongan+"&employee_id="+vEmployee+"&order="+vOrder;
			Swal.fire({
				icon:'question',
				title: 'Apakah anda ingin melihat hasil lengkap ?',
				showDenyButton: true,
				confirmButtonText: 'Soal + Hasil',
				denyButtonText: `Hanya Hasil`,
			}).then((result) => {
				if (result.isConfirmed) {
					url = url+"&is_hanya_hasil=0";
				}else if(result.isDenied){
					url = url+"&is_hanya_hasil=1";
				}
						
			const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
			const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;
			const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
			const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

			const systemZoom = width / window.screen.availWidth;
			const left = (width - 800) / 2 / systemZoom + dualScreenLeft
			const top = (height - 600) / 2 / systemZoom + dualScreenTop
			const newWindow = window.open(url, 'preview', 
						`
						scrollbars=yes,
						width=${800 / systemZoom}, 
						height=${600 / systemZoom}, 
						top=${top}, 
						left=${left}
						`)
			})
			$('#modalDownloadHasil').modal('hide');

		})

		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("vacancy/list_ajax");?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'title' },
				{ data: 'start_date' },
				{ data: 'end_date' },
				{ data: 'province_name' },
				{ data: 'branch_name' },
				{ data: 'company_name' },
				{ data: 'site_name' },
				{ data: 'position_name' },
				{ data: 'needs','className':'text-center', 'orderable':false, 'searchable':false },
				{ data: 'applicant', 'orderable':false, 'searchable':false },
				{ data: 'submission', 'orderable':false, 'searchable':false },
				{ data: 'accepted', 'orderable':false, 'searchable':false },
				{ data: 'daftar_tes', 'orderable':false, 'searchable':false },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[1, 'desc']]
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>=0 && index<=7){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );
	});

function preview_chart(el)
{
    const url = $(el).attr('data-href');
    const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
    const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

    const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    const systemZoom = width / window.screen.availWidth;
    const left = (width - 1000) / 2 / systemZoom + dualScreenLeft
    const top = (height - 600) / 2 / systemZoom + dualScreenTop
    const newWindow = window.open(url, 'preview', 
      `
      scrollbars=yes,
      width=${1000 / systemZoom}, 
      height=${600 / systemZoom}, 
      top=${top}, 
      left=${left}
      `
    )
    // if (window.focus) new Window.focus();
}
</script>
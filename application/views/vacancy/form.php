<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('vacancy/list'); ?>"><?php echo $_TITLE_; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Lowongan</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("vacancy/form"); ?>" enctype="multipart/form-data">
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Mulai Tayang <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="date" name="start_date" class="form-control datepicker" value="<?php echo $start_date; ?>" required>
								</div>
								<div class="col-sm-7"></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Selesai Tayang <span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<input type="date" name="end_date" class="form-control datepicker" value="<?php echo $end_date; ?>" required>
								</div>
								<div class="col-sm-7"></div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Bisnis Unit <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="company_id">
										<option value=""> -- PILIH BISNIS UNIT -- </option>
										<?php foreach ($list_company as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $company_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Site Bisnis <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="site_id">
										<option value=""> -- PILIH SITE BISNIS -- </option>
										<?php foreach ($list_site as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $site_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Jabatan <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="position_id">
										<option value=""> -- PILIH JABATAN -- </option>
										<?php foreach ($list_position as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $position_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Provinsi <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<select class="form-control select2" name="province_id" required>
										<option value=""> -- PILIH PROVINSI -- </option>
										<?php foreach ($list_province as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id === $province_id): echo "selected"; endif;?>><?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Judul Lowongan <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" placeholder="Judul Lowongan" name="title" value="<?php echo $title; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Kebutuhan <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="number" class="form-control" placeholder="Jumlah Kebutuhan" name="needs" value="<?php echo $needs; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Tes DISC<span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<select class="form-control select2" name="is_tes_psikotes" id="is_tes_psikotes" required>
										<option value="0" <?php if($is_tes_psikotes === "0"): echo "selected"; endif;?>>Tidak</option>
										<option value="1" <?php if($is_tes_psikotes === "1"): echo "selected"; endif;?>>Ya</option>
									</select>
								</div>
								<div class="form-group form-inline col-sm-3 frm-durasi-psikotes" style="<?php if($is_tes_psikotes === "0"): echo "display:none"; endif;?>">
									<label for="durasi_psikotes" class="col-sm-8 col-form-label">Durasi (Menit)<span class="text-danger">* &nbsp;&nbsp;</span></label>
									<input type="number" class="col-sm-4 form-control" placeholder="Durasi Psikotes" id="durasi_psikotes" name="durasi_psikotes" value="<?php echo $durasi_psikotes; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Tes Deret Angka<span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<select class="form-control select2" name="is_tes_deret_angka" id="is_tes_deret_angka" required>
									<option value="0" <?php if($is_tes_deret_angka === "0"): echo "selected"; endif;?>>Tidak</option>
										<option value="1" <?php if($is_tes_deret_angka === "1"): echo "selected"; endif;?>>Ya</option>
									</select>
								</div>
								<div class="form-group form-inline col-sm-3 frm-durasi-deret-angka" style="<?php if($is_tes_deret_angka === "0"): echo "display:none"; endif;?>">
									<label for="durasi_deret_angka" class="col-sm-8 col-form-label">Durasi (Menit)<span class="text-danger">* &nbsp;&nbsp;</span></label>
									<input type="number" class="col-sm-4 form-control" placeholder="Durasi Deret Angka" id="durasi_deret_angka" name="durasi_deret_angka" value="<?php echo $durasi_deret_angka; ?>" required>
								</div>
								<div class="form-group form-inline col-sm-4 frm-durasi-deret-angka" style="<?php if($is_tes_deret_angka === "0"): echo "display:none"; endif;?>">
									<label for="min_lolos_deret_angka" class="col-sm-7 col-form-label">% Minimal <span class="text-danger">* &nbsp;&nbsp;</span></label>
									<input type="number" class="col-sm-5 form-control" placeholder="Min Lolos Deret Angka" id="min_lolos_deret_angka" step="any" name="min_lolos_deret_angka" value="<?php echo $min_lolos_deret_angka; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Tes Hitung Cepat<span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<select class="form-control select2" name="is_tes_hitung_cepat" id="is_tes_hitung_cepat" required>
										<option value="0" <?php if($is_tes_hitung_cepat === "0"): echo "selected"; endif;?>>Tidak</option>
										<option value="1" <?php if($is_tes_hitung_cepat === "1"): echo "selected"; endif;?>>Ya</option>
									</select>
								</div>
								<div class="form-group form-inline col-sm-3 frm-durasi-hitung-cepat" style="<?php if($is_tes_hitung_cepat === "0"): echo "display:none"; endif;?>">
									<label for="durasi_hitung_cepat" class="col-sm-8 col-form-label">Durasi (Menit)<span class="text-danger">* &nbsp;&nbsp;</span></label>
									<input type="number" class="col-sm-4 form-control" placeholder="Durasi Hitung Cepat" id="durasi_hitung_cepat" name="durasi_hitung_cepat" value="<?php echo $durasi_hitung_cepat; ?>" required>
								</div>
								<div class="form-group form-inline col-sm-4 frm-durasi-hitung-cepat" style="<?php if($is_tes_hitung_cepat === "0"): echo "display:none"; endif;?>">
									<label for="min_lolos_hitung_cepat" class="col-sm-7 col-form-label">% Minimal<span class="text-danger">* &nbsp;&nbsp;</span></label>
									<input type="number" class="col-sm-5 form-control" placeholder="Min Lolos Hitung Cepat" id="min_lolos_hitung_cepat" step="any" name="min_lolos_hitung_cepat" value="<?php echo $min_lolos_hitung_cepat; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Tes Ketelitian<span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<select class="form-control select2" name="is_ketelitian" id="is_ketelitian" required>
										<option value="0" <?php if($is_ketelitian === "0"): echo "selected"; endif;?>>Tidak</option>
										<option value="1" <?php if($is_ketelitian === "1"): echo "selected"; endif;?>>Ya</option>
									</select>
								</div>
								<div class="form-group form-inline col-sm-3 frm-durasi-ketelitian" style="<?php if($is_ketelitian === "0"): echo "display:none"; endif;?>">
									<label for="durasi_ketelitian" class="col-sm-8 col-form-label">Durasi (Menit)<span class="text-danger">* &nbsp;&nbsp;</span></label>
									<input type="number" class="col-sm-4 form-control" placeholder="Durasi Ketelitian" id="durasi_ketelitian" name="durasi_ketelitian" value="<?php echo $durasi_ketelitian; ?>" required>
								</div>
								<div class="form-group form-inline col-sm-4 frm-durasi-ketelitian" style="<?php if($is_ketelitian === "0"): echo "display:none"; endif;?>">
									<label for="min_lolos_ketelitian" class="col-sm-7 col-form-label">% Minimal<span class="text-danger">* &nbsp;&nbsp;</span></label>
									<input type="number" class="col-sm-5 form-control" placeholder="Min Lolos Ketelitian" id="min_lolos_ketelitian" step="any" name="min_lolos_ketelitian" value="<?php echo $min_lolos_ketelitian; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Tes Kraepelin<span class="text-danger">*</span></label>
								<div class="col-sm-2">
									<select class="form-control select2" name="is_kraepelin" id="is_kraepelin" required>
										<option value="0" <?php if($is_kraepelin === "0"): echo "selected"; endif;?>>Tidak</option>
										<option value="1" <?php if($is_kraepelin === "1"): echo "selected"; endif;?>>Ya</option>
									</select>
								</div>
								<div class="form-group form-inline col-sm-3 frm-durasi-kraepelin" style="<?php if($is_kraepelin === "0"): echo "display:none"; endif;?>">
									<label for="durasi_kolom_kraepelin" class="col-sm-7 col-form-label">Durasi (Detik)<span class="text-danger">* &nbsp;&nbsp;</span></label>
									<input type="number" class="col-sm-5 form-control" placeholder="Durasi Kraepelin" id="durasi_kolom_kraepelin" name="durasi_kolom_kraepelin" value="<?php echo $durasi_kolom_kraepelin; ?>" required>
								</div>
								<div class="form-group form-inline col-sm-2 frm-durasi-kraepelin" style="<?php if($is_kraepelin === "0"): echo "display:none"; endif;?>">
									<label for="jumlah_kolom_kraepelin" class="col-sm-7 col-form-label">Kolom<span class="text-danger">* &nbsp;&nbsp;</span></label>
									<input type="number" class="col-sm-5 form-control" id="jumlah_kolom_kraepelin" name="jumlah_kolom_kraepelin" value="<?php echo $jumlah_kolom_kraepelin; ?>" required>
								</div>
								<div class="form-group form-inline col-sm-2 frm-durasi-kraepelin" style="<?php if($is_kraepelin === "0"): echo "display:none"; endif;?>">
									<label for="jumlah_baris_kraepelin" class="col-sm-7 col-form-label">Baris<span class="text-danger">* &nbsp;&nbsp;</span></label>
									<input type="number" class="col-sm-5 form-control" id="jumlah_baris_kraepelin" name="jumlah_baris_kraepelin" value="<?php echo $jumlah_baris_kraepelin; ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Flyer <span class="text-danger"></span></label>
								<div class="custom-file col-sm-9 file-upload">	
									<input type="file" class="custom-file-input" name="flyer" accept="image/jpeg, image/jpg, image/png" ><label class="custom-file-label" style="margin-left: 7px;">Pilih file</label>
								</div>
								<div class="col-sm-2">
									<?php if($flyer != ''): ?>
										<a href="<?php echo $flyer; ?>" class="btn btn-info btn-block" target="_blank"><i class="fas fa-download"></i> Unduh</a>
									 <?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-12">
									 <textarea id="editor" name="content" class="form-control editor" style="min-height: 500px">
									 	<?php echo $content; ?>
									 </textarea>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>	
		</div>
	</section>
</div>
<script>
	$(document).ready(function () {
		$('#is_tes_psikotes').on('change', function() {
			if(this.value=="0"){
				$('#durasi_psikotes').val("0");
				$('.frm-durasi-psikotes').hide();
			}else{
				$('.frm-durasi-psikotes').show();
			}
		});

		$('#is_tes_deret_angka').on('change', function() {
			if(this.value=="0"){
				$('#durasi_deret_angka').val("0");
				$('.frm-durasi-deret-angka').hide();
			}else{
				$('.frm-durasi-deret-angka').show();
			}
		});

		$('#is_tes_hitung_cepat').on('change', function() {
			if(this.value=="0"){
				$('#durasi_hitung_cepat').val("0");
				$('.frm-durasi-hitung-cepat').hide();
			}else{
				$('.frm-durasi-hitung-cepat').show();
			}
		});

		$('#is_ketelitian').on('change', function() {
			if(this.value=="0"){
				$('#durasi_ketelitian').val("0");
				$('.frm-durasi-ketelitian').hide();
			}else{
				$('.frm-durasi-ketelitian').show();
			}
		});

		$('#is_kraepelin').on('change', function() {
			if(this.value=="0"){
				$('#durasi_kolom_kraepelin').val("0");
				$('#jumlah_baris_kraepelin').val("0");
				$('#jumlah_kolom_kraepelin').val("0");

				$('.frm-durasi-kraepelin').hide();
			}else{
				$('.frm-durasi-kraepelin').show();
			}
		});

	});

	$(function () {
		tinymce.init({ 
			selector:'.editor',
			theme: 'modern',
			height: 600,
			menubar: true,
			lineheight_formats: "0pt 1pt 8pt 9pt 10pt 11pt 12pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 36pt",
			plugins: [
			'advlist autolink lists lineheight nonbreaking print preview anchor',
			'searchreplace visualblocks fullscreen textcolor colorpicker',
			'tabfocus table contextmenu powerpaste'
			],
			menubar: 'file edit view insert format tools table tc help',
			toolbar: 'undo redo | bold italic underline strikethrough | forecolor backcolor | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent nonbreaking lineheightselect | table | preview print',
			powerpaste_allow_local_images: true,
			powerpaste_word_import: 'prompt',
			powerpaste_html_import: 'prompt',
		});
	});
</script>

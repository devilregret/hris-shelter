<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('vacancy/list'); ?>">Daftar Lowongan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Daftar Pelamar : <?php if($vacancy): echo '<strong>'.$vacancy->title.'</strong>'; endif;?></h3>
				</div>
				<div class="card-header">
					<?php if($this->session->flashdata('message')): echo $this->session->flashdata('message'); endif; ?>
					<div class="row">
						<div class="col-sm-1">
							<br>
							<a href="#" class="btn btn-success export"><i class="fas fa-file-export"></i> Export </a>&nbsp;
						</div>
						<div class="col-sm-2">
							<div class="submission" hidden="true">
								<strong>Tanggal Penempatan :</strong> 
								<input type="date" id="placement_date" class="form-control datepicker" >
							</div>
						</div>
						<div class="col-sm-2">
							<div class="submission" hidden="true">
							<strong>Bisnis Unit :</strong>
							<select class="form-control select2 " id="company_id" required>
								<option value="">-- Bisnis Unit --</option>
								<?php foreach ($list_company as $item) :  ?>
									<option value="<?php echo $item->id;?>" <?php if($item->id == $vacancy->company_id): echo 'selected'; endif; ?>><?php echo $item->code; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="submission" hidden="true">
							<strong>Site Bisnis :</strong>
							<select class="form-control select2 " id="site_id" required>
								<option value="">-- Site Bisnis --</option>
								<?php foreach ($list_site as $item) :  ?>
									<option value="<?php echo $item->id;?>" <?php if($item->id == $vacancy->site_id): echo 'selected'; endif; ?>><?php echo $item->name; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="submission" hidden="true">
							<strong>Jabatan :</strong>
								<select class="form-control select2 " id="position_id" required>
										<option value="">-- Pilih Jabatan --</option>
									<?php foreach ($list_position as $item) :  ?>
										<option value="<?php echo $item->id;?>" <?php if($item->id == $vacancy->position_id): echo 'selected'; endif; ?>><?php echo $item->name; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<br>
							<a href="#" class="btn btn-info submission btn-block" hidden="true" ><i class="fas fa-check"></i> Ajukan</a>
						</div>
					</div>
					<hr>
					<div class="row" >
						<div class="col-sm-3">
							<div class="submission" hidden="true">
								<strong>Tipe Followup :</strong>
								<select class="form-control select2 " id="status-followup">
									<option value="Lihat">Lihat</option>
									<option value="Hubungi">Hubungi</option>
									<option value="Proses">Proses</option>
									<option value="Tolak">Tolak</option>
									<option value="Blokir">Blokir</option>
								</select>
							</div>
						</div>
						<div class="col-sm-7 submission" hidden="true">
							<strong>Keterangan Followup :</strong>
							<textarea id="note-followup"  rows="2" placeholder="Tulis Catatan Followup" style="width:100%"; ></textarea>
						</div>
						<div class="col-sm-2 submission" hidden="true">
							<strong>&nbsp;</strong>
							<a class="btn btn-success followup btn-block"><i class="fas fa-phone"></i> Follow Up</a>
						</div>
						&nbsp;
					</div>
				</div>
				<p>&nbsp;</p>
				<div class="card-body">
					<form method="POST" id="submission" action="<?php echo base_url('vacancy/submit/'.$vacancy_id); ?>"  enctype="multipart/form-data">
					<div class="row" >
						<div class="col-sm-5 submission" hidden="true">
							<input type="text" placeholder="Silahkan tulis Pesan" class="form-control float-right" name="message-personal">
						</div>
						<div class="col-sm-5 submission" hidden="true">
							<input type="file" class="custom-file-input" name="message-image" accept="image/png, image/jpeg" required>
							<label class="custom-file-label">Pilih file</label>
						</div>
						<div class="col-sm-2 submission" hidden="true">
							<a class="btn btn-success save-personal btn-block"><i class="fas fa-paper-plane"></i> Kirim Whatsapp</a>
						</div>
						&nbsp;
					</div>
					<input type="hidden" class="form-control note-followup" name="note_followup" value="" >
					<input type="hidden" class="form-control status-followup" name="status_followup" value="" >
					<input type="hidden" class="form-control message-personal" name="message" value="" >
					<input type="hidden" class="form-control site_id" name="site_id" value="1">
					<input type="hidden" class="form-control company_id" name="company_id" value="1">
					<input type="hidden" class="form-control position_id" name="position_id" value="1">
					<input type="hidden" class="form-control placement_date" name="placement_date" value="1">
					<div class="scroll-panel">
						<table id="data-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="50px"><input type="checkbox"  class="form-check-input" style="position:revert; margin:0 auto;" id="checkAll"></th>
									<th id='created_at'>Tanggal Daftar</th>
									<th id='full_name'>Nama Lengkap</th>
									<th id='id_card'>KTP</th>
									<th id='age'>Usia</th>
									<th id='gender'>Jenis Kelamin</th>
									<th id='education_level'>Pendidikan</th>
									<th id='education_majors'>Jurusan</th>
									<th id='phone_number'>Nomor Telepon</th>
									<th id='address_domisili'>Domisili</th>
									<th id='candidate_status'>Status Kelengkapan</th>
									<th id='status_tes'>Status Tes</th>
									<th id='status_followup'>Status Followup</th>
									<th id='followup_description'>Keterangan Followup</th>
									<th width="80px">#</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
	$(document).ready(function(){

		$('#data-table').DataTable({
			'autoWidth'		: false,
			'processing'	: true,
			'serverSide'	: true,
			'serverMethod'	: 'post',
			'ajax': {
				'url':'<?php echo base_url("vacancy/applicant_ajax/".$vacancy_id."/".$vacancy->site_id);?>'
			},
			'lengthMenu'	: [[10, 25, 50, 100, 200, 500, 1000, 1000000000], [10, 25, 50, 100, 200, 500, 1000, "semua"]],
			'dom'			:  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
			'columns': [
				{ data: 'id', 'orderable':false, 'searchable':false },
				{ data: 'date_registration' },
				{ data: 'full_name' },
				{ data: 'id_card' },
				{ data: 'age' },
				{ data: 'gender' },
				{ data: 'education_level' },
				{ data: 'education_majors' },
				{ data: 'phone_number' },
				{ data: 'city_domisili' },
				{ data: 'status_completeness', 'orderable':false, 'searchable':false },
				{ data: 'status_tes', 'orderable':false, 'searchable':false },
				{ data: 'followup_status', 'orderable':false, 'searchable':false },
				{ data: 'followup_description', 'orderable':false, 'searchable':false },
				{ data: 'action', 'orderable':false, 'searchable':false }
			],
			'order': [[1, 'desc']],
			"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				if (aData['followup_status'].startsWith("Lihat")) {
					$('td', nRow).css('background-color', '#80ff80');
				} else if (aData['followup_status'].startsWith("Hubungi")) {
					$('td', nRow).css('background-color', '#8080ff');
				}else if (aData['followup_status'].startsWith("Proses")) {
					$('td', nRow).css('background-color', '#ffcc80');
				}else if (aData['followup_status'].startsWith("Tolak")) {
					$('td', nRow).css('background-color', '#ff8080');
				}else if (aData['followup_status'].startsWith("Blokir")) {
					$('td', nRow).css('background-color', '#bfbfbf');
				}
			}
		});

		$('#data-table tfoot th').each( function () { 
			var index = $(this).index();
			if(index>0 && index<10){
				var title = $('#data-table thead th').eq( $(this).index() ).text(); 
				var id = $('#data-table thead th').eq( $(this).index() ).attr('id'); 	
				$(this).html( '<input type="text" style="width:100%;" id="s_'+id+'" placeholder="Cari '+title+'" />' ); 	
			}
		}); 
		
		var table = $('#data-table').DataTable();
		table.columns().eq(0).each( function ( colIdx ) { $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () { table .column( colIdx ) .search( this.value ) .draw(); } ); } );

		$('#checkAll').click(function () {
		    $('input:checkbox').prop('checked', this.checked);  
		});

		$(document).on("change", ".check-id", function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
    		});
		});
		$("input:checkbox").change(function() {
			$(".submission").prop('hidden', true);
			$("input:checkbox.check-id:checked").each(function(){
				$(".submission").prop('hidden', false);
    		});
		});
		$( ".btn.submission" ).click(function() {
			if($( "#site_id").val() == '' || $( "#position_id").val() == '' || $( "#company_id").val() == ''|| $( "#placement_date").val() == ''){
				alert('Silahkan pilih Tanggal Penempatan, Bisnis Unit, Site Bisnis dan Jabatan!!!');
			}else{
				$( ".site_id").val($( "#site_id").val());
				$( ".position_id").val($( "#position_id").val());
				$( ".company_id").val($( "#company_id").val());
				$( ".placement_date").val($( "#placement_date").val());
				$( "#submission" ).submit();
			}
		});
		$(".btn.save-personal" ).click(function() {
			var message = $('#message-personal').val();
			if(message == ''){
				alert('Pesan harap diisi. ');
			}else{
				$(".message-personal").val(message);
				$('#submission').attr('action', "<?php echo base_url('vacancy/send/'.$vacancy_id); ?>").submit();
			}
		});
		$(".btn.followup" ).click(function() {
			var message = $('#note-followup').val();
			var status = $('#status-followup').val();
			if(message == ''){
				alert('Catatan Followup. ');
			}else{
				$(".note-followup").val(message);
				$(".status-followup").val(status);
				$('#submission').attr('action', "<?php echo base_url('vacancy/followup/'.$vacancy_id.'/'.$vacancy->site_id.'/'.$vacancy->position_id.'/'.$vacancy->company_id); ?>").submit();
			}
		});

		$( ".export" ).click(function() {
			var url = "<?php echo base_url('vacancy/applicant_export/'.$vacancy_id); ?>"; 
			window.location = url;
		
		});
	});

	function previewHasil(el){
		let url = $(el).attr('data-href');
		Swal.fire({
			icon:'question',
			title: 'Apakah anda ingin melihat hasil lengkap ?',
			showDenyButton: true,
			confirmButtonText: 'Soal + Hasil',
			denyButtonText: `Hanya Hasil`,
		}).then((result) => {
			if (result.isConfirmed) {
				url = url+"&is_hanya_hasil=0";
			}else if(result.isDenied){
				url = url+"&is_hanya_hasil=1";
			}
	                
		const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
		const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;
		const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

		const systemZoom = width / window.screen.availWidth;
		const left = (width - 800) / 2 / systemZoom + dualScreenLeft
		const top = (height - 600) / 2 / systemZoom + dualScreenTop
		const newWindow = window.open(url, 'preview', 
	                `
	                scrollbars=yes,
	                width=${800 / systemZoom}, 
	                height=${600 / systemZoom}, 
	                top=${top}, 
	                left=${left}
	                `)
		})
	}
</script>
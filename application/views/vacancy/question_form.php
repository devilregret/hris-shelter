<div class="content-wrapper" style="min-height: 1416.81px;">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-1">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url('vacancy/question/'.$vacancy_id); ?>">Daftar Pertanyaan</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline col-12">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-edit"></i>&nbsp;Form Lowongan</h3>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<td width="250px">Id Lowongan</td>									
								<td>: <?php echo $vacancy->id; ?></td>
							</tr>
							<tr>
								<td width="250px">Judul Lowongan</td>
								<td>: <?php echo $vacancy->title; ?></td>
							</tr>
						</thead>
					</table>
					<div class="row">
						<form class="form-horizontal col-sm-10 mt-4" method="post" action="<?php echo base_url("vacancy/question_form/".$vacancy_id); ?>">
							<div class="form-group row">
								<label for="name" class="col-sm-3 col-form-label">Pertanyaan <span class="text-danger"></span></label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
									<input type="hidden" class="form-control" name="vacancy_id" value="<?php echo $vacancy_id; ?>">
									<textarea name="question" class="form-control" style="min-height: 100px"><?php echo $question; ?></textarea>
								</div>
							</div>
							<div class="form-group row  float-right">
								<button type="reset"  class="btn btn-default btn-form">Batal</button>&nbsp;
								<button type="submit" class="btn btn-primary btn-form">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>	
		</div>
	</section>
</div>
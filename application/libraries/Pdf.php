<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH."/third_party/TCPDF/tcpdf.php";
class Pdf extends TCPDF { 
    private $customFooterText = "";
    private $customFooterTextContract = "";

    public function setCustomFooterText($customFooterText)
    {
        $this->customFooterText = $customFooterText;
    }

    public function setCustomFooterTextContract($customFooterTextContract)
    {
        $this->customFooterTextContract = $customFooterTextContract;
    }

   public function Header() {
        $headerData = $this->getHeaderData();
        $this->SetFont('helvetica', 'B', 8);
        $this->writeHTML($headerData['string']);
    }

    public function Footer() {
        $this->writeHTML("<hr>", true, false, false, false, '');
        if($this->customFooterText != ''){
            $this->SetY(-10);
            $this->Cell(0, 5, 'Halaman : '.$this->getAliasNumPage().' dari : '.$this->getAliasNbPages().' '.$this->customFooterText, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
        if($this->customFooterTextContract != ''){
            $this->SetY(-15);
            $this->Cell(0, 5, '         '.$this->customFooterTextContract.'                                                                                             page '.$this->getAliasNumPage().' of '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
    }
}
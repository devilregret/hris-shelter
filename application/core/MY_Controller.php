<?php
ini_set("memory_limit","512M");
if(!defined('BASEPATH'))
{
	exit('No direct script access allowed');
}

class Frontend_Controller extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		date_default_timezone_set('Asia/Jakarta');
		if(empty($this->session->userdata('is_login'))){
			redirect('auth/login');
		}
	}

	public function view($data = array(), $return = FALSE, $layout = 'layout')
	{
		$this->load->model(array('candidate_model', 'legality_model'));
		$data['_PAGE_'] = isset($data['_PAGE_'])?$data['_PAGE_']:'home';        
		$data['_TITLE_'] = isset($data['_TITLE_'])?$data['_TITLE_']:'PT SHELTER NUSANTARA';
		$data['_MENU_'] = isset($data['_MENU_'])?$data['_MENU_']:'home';
		$data['_MENU_PARENT_'] = isset($data['_MENU_PARENT_'])?$data['_MENU_PARENT_']:'home';
		$data['_ROLE_'] = $this->session->userdata('role');
		
		if ($return){
			return $this->load->view($layout, $data, $return);
		}else{
			$this->load->view($layout, $data);
		}
	}
}
ALTER TABLE `m_vacancy` ADD `company_id` INT(11) NULL DEFAULT NULL AFTER `end_date`, ADD INDEX (`company_id`);
ALTER TABLE `m_vacancy` ADD `site_id` BIGINT(20) NULL DEFAULT NULL AFTER `company_id`, ADD INDEX (`site_id`);
ALTER TABLE `m_vacancy` ADD `position_id` INT(11) NULL DEFAULT NULL AFTER `site_id`, ADD INDEX (`position_id`);
ALTER TABLE `m_vacancy` ADD `needs` INT(11) NOT NULL DEFAULT '0' AFTER `content`, ADD INDEX (`needs`);
ALTER TABLE `m_legality` ADD `regulation` VARCHAR(100) NOT NULL AFTER `note`, ADD INDEX (`regulation`);


ALTER TABLE `m_vacancy` ADD `flyer` VARCHAR(255) NOT NULL AFTER `needs`;

CREATE TABLE `m_cooperation_agreement` (
  `id` bigint(20) NOT NULL,
  `agency_name` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `start_contract` date NOT NULL,
  `end_contract` date NOT NULL,
  `document` varchar(255) NOT NULL,
  `is_active` smallint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `m_cooperation_agreement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `agency_name` (`agency_name`),
  ADD KEY `start_contract` (`start_contract`),
  ADD KEY `end_contract` (`end_contract`),
  ADD KEY `is_active` (`is_active`),
  ADD KEY `city_id` (`city_id`);

ALTER TABLE `m_cooperation_agreement`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
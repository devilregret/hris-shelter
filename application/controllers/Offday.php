<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offday extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!in_array($this->session->userdata('role'), array(2,5,6))){
			redirect(base_url());
		}
	}

	public function list(){
		$data['_TITLE_'] 		= 'Hari Libur';
		$data['_PAGE_'] 		= 'offday/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'offday';

		$this->view($data);
	}

	public function list_ajax($type = FALSE){

		$this->load->model(array('offday_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, DATE_FORMAT(A.date, '%d %M %Y') AS date, A.type, A.description";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['date']			= $_POST['columns'][1]['search']['value'];
		$params['type']			= $_POST['columns'][2]['search']['value'];
		$params['description']	= $_POST['columns'][3]['search']['value'];
		
		$list 	= $this->offday_model->gets($params);
		$total 	= $this->offday_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{			
			$result['no'] 			= $i;
			$result['date']			= $item->date;
			$result['type']			= $item->type;
			$result['description']	= $item->description;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action"  style="cursor:pointer;" data-href="'. base_url("offday/preview/".$item->id).'"><i class="fas fa-eye text-white"></i></a>
				<a class="btn-sm btn-success" href="'.base_url("offday/form/".$item->id).'"><i class="fas fa-edit"></i></a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger" style="cursor:pointer;" data-href="'.base_url("offday/delete/".$item->id).'"><i class="far fa-trash-alt text-white"></i></a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function form($offday_id = FALSE)
	{
		$this->load->model(array('offday_model'));

		$data['id'] 		= '';
		$data['date']		= '';
		$data['type'] 		= '';
		$data['description']= '';
		if($this->input->post()){
			$data['id'] 	= $this->input->post('id');
			$data['date'] 	= $this->input->post('date');
			$data['type']	= $this->input->post('type');
			$data['description']	= $this->input->post('description');

			$this->form_validation->set_rules('date', '', 'required');
			$this->form_validation->set_rules('type', '', 'required');
			$this->form_validation->set_rules('description', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$insert = array('id' => $data['id'], 'date' => $data['date'],'type' => $data['type'], 'description' => $data['description']);
				$save_id	 = $this->offday_model->save($insert);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('offday/list'));
		}

		if ($offday_id)
		{
			$data = (array) $this->offday_model->get(array('id' => $offday_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('offday/list'));
			}
		}

		$data['_TITLE_'] 		= 'Hari Libur';
		$data['_PAGE_'] 		= 'offday/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'offday';
		return $this->view($data);
	}

	public function preview($offday_id=FALSE)
	{
		$this->load->model('offday_model');
		$data['_TITLE_'] 		= 'Preview Hari Libur';
		$data['_PAGE_']	 		= 'offday/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'offday';

		$data['id'] = $offday_id;

		
		if (!$offday_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('offday/list'));
		}

		$data['preview'] = $this->offday_model->preview(array('id' => $offday_id));
		$this->load->view('offday/preview', $data);
	}

	public function delete($offday_id = false)
	{
		$this->load->model('offday_model');
		if ($offday_id)
		{
			$data =  $this->offday_model->get(array('id' => $offday_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $offday_id, 'is_active' => 0);
				$result = $this->offday_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('offday/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('offday/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('offday/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('offday/list'));
		}
	}
}
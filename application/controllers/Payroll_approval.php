<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payroll_approval extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		$role 	= array_merge($role, $this->config->item('accounting'));
		$role 	= array_merge($role, $this->config->item('directur'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function list(){
		$data['_TITLE_'] 		= 'Laporan Pengajuan Payroll';
		$data['_PAGE_'] 		= 'payroll_approval/list';
		$data['_MENU_PARENT_'] 	= 'payroll_report';
		$data['_MENU_'] 		= 'payroll_approval';
		
		if($this->input->post()){
			$data['view']		= $this->input->post('view');
			$data['start_date']	= $this->input->post('start_date');
			$data['end_date']	= $this->input->post('end_date');
		}else{
			$data['view']		= 'release';
			$data['start_date']	= date('Y-m-01');
			$data['end_date']	= date('Y-m-d');
		}
		$this->view($data);
	}

	public function list_ajax($view = false, $start_date = false, $end_date = false){
		$this->load->model(array('payroll_model', 'employee_model'));

		$column_index 			= $_POST['order'][0]['column']; 
		$params['site_name']	= $_POST['columns'][1]['search']['value'];
		$params['company_code']	= $_POST['columns'][2]['search']['value'];

		if($view == 'release'){
			$params['payment']		= 'Selesai';
		}else if($view == 'notrelease'){
			$params['notrelease']	= true;
		}else{
			$params['payment']		= 'Ditolak';
		}

		$params['start_date']	= $start_date;
		$params['end_date']		= $end_date;
		$params['groupby']		= 'A.id';
		$params['columns'] 		= 'A.id';
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		$total					= COUNT($this->payroll_model->resume_approval($params));
		
		$params['columns'] 		= 'A.id AS site_id, A.name AS site_name, D.name AS company_name, DATE_FORMAT(MIN(C.periode_start), "%d/%m/%Y") AS periode_start, DATE_FORMAT(MAX(C.periode_end), "%d/%m/%Y") AS periode_end';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$list 	= $this->payroll_model->resume_approval($params);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['no'] 				= $i;
			$result['site_name']		= $item->site_name;
			$result['company_name']		= $item->company_name;
			$result['periode_start']	= $item->periode_start;
			$result['periode_end']		= $item->periode_end;
			$result['total_approval']	= $this->payroll_model->resume_approval(array('site_id' => $item->site_id, 'payment' => 'Selesai', 'start_date' => $params['start_date'], 'end_date' => $params['end_date']), TRUE);
			$result['total_employee']	= $this->employee_model->gets(array('status_approval' => 3, 'site_id' => $item->site_id), TRUE);
			$result['total_reject'] 	= $this->payroll_model->resume_approval(array('site_id' => $item->site_id, 'payment' => 'Ditolak', 'start_date' => $params['start_date'], 'end_date' => $params['end_date']), TRUE);

			if($view == 'release'){
				$result['action']		= '<a class="btn-sm btn-success btn-block" href="'.base_url("payroll_approval/detail/release/".$item->site_id."/".$start_date."/".$end_date).'">Diterima</a>';
			}else if($view == 'reject'){
				$result['action']		= '<a class="btn-sm btn-success btn-block" href="'.base_url("payroll_approval/detail/reject/".$item->site_id."/".$start_date."/".$end_date).'">Ditolak</a>';
			}else{
				$result['action']		= '';	
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function detail($type = FALSE, $site_id = FALSE, $start_date = FALSE, $end_date = FALSE){
		$this->load->model(array('site_model'));
		$data['_TITLE_'] 		= 'Laporan Pengajuan Payroll';
		$data['_PAGE_'] 		= 'payroll_approval/detail';
		$data['_MENU_PARENT_'] 	= 'payroll_report';
		$data['_MENU_'] 		= 'payroll_approval';
		$data['start_date']		= $start_date;
		$data['end_date']		= $end_date;
		$data['site_id']		= $site_id;
		$data['type']			= $type;
		$data['site']			= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.id, A.name, A.address'));
		$this->view($data);
	}

	public function detail_ajax($type = false, $site_id = false, $start_date = false, $end_date = false){
		$this->load->model(array('payroll_model'));
		$column_index 				= $_POST['order'][0]['column']; 
		$params['employee_name']	= $_POST['columns'][1]['search']['value'];
		$params['id_card']			= $_POST['columns'][2]['search']['value'];
		$params['periode_start']	= $_POST['columns'][3]['search']['value'];
		$params['periode_end']		= $_POST['columns'][4]['search']['value'];
		$params['salary']			= $_POST['columns'][5]['search']['value'];
		$params['start_date']		= $start_date;
		$params['end_date']			= $end_date;
		$params['site_id']			= $site_id;
		if($type == 'release'){
			$params['payment']		= 'Selesai';
		}else if($type == 'reject'){
			$params['payment']		= 'Ditolak';
		}
		
		$params['columns'] 		= 'A.id AS site_id, B.full_name AS employee_name, B.id_card, DATE_FORMAT(C.periode_start, "%d/%m/%Y") AS periode_start, DATE_FORMAT(C.periode_end, "%d/%m/%Y") AS periode_end, C.salary';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$list 					= $this->payroll_model->resume_approval($params);
		$total					= $this->payroll_model->resume_approval($params, TRUE);
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['no'] 				= $i;
			$result['employee_name']	= $item->employee_name;
			$result['id_card']			= $item->id_card;
			$result['periode_start']	= $item->periode_start;
			$result['periode_end']		= $item->periode_end;
			$result['salary']			= nominal_rupiah( (int) $item->salary);
			$result['action']			= '';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}
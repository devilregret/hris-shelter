<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payroll_employee extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function list(){
		$this->load->model(array('site_model'));
		$data['_TITLE_'] 		= 'Karyawan';
		$data['_PAGE_'] 		= 'payroll_employee/list';
		$data['_MENU_PARENT_'] 	= 'payroll_employee';
		$data['_MENU_'] 		= 'payroll_employee';
		$this->view($data);
	}

	public function list_ajax(){
		$this->load->model(array('employee_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.full_name, A.email, A.phone_number, A.bank_account, B.name AS site_name, C.name AS company_name, E.name AS position, D.contract_type, DATE_FORMAT(D.contract_end, '%d/%m/%Y') AS contract_end";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']		= 1;
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		
		$params['status_approval'] 		= 3;
		$params['employee_number']		= $_POST['columns'][1]['search']['value'];
		$params['id_card']				= $_POST['columns'][2]['search']['value'];
		$params['full_name']			= $_POST['columns'][3]['search']['value'];
		$params['email']				= $_POST['columns'][4]['search']['value'];
		$params['phone_number']			= $_POST['columns'][5]['search']['value'];
		$params['bank_account']			= $_POST['columns'][6]['search']['value'];
		$params['company_name']			= $_POST['columns'][7]['search']['value'];
		$params['site_name']			= $_POST['columns'][8]['search']['value'];
		$params['position']				= $_POST['columns'][9]['search']['value'];

		$list 	= $this->employee_model->gets($params);
		$total 	= $this->employee_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 					= $i;
			$result['employee_number']		= $item->employee_number;
			$result['id_card']				= $item->id_card;
			$result['full_name']			= $item->full_name;
			$result['email']				= $item->email;
			$result['phone_number']			= $item->phone_number;
			$result['bank_account']			= $item->bank_account;
			$result['company_name']			= $item->company_name;
			$result['site_name']			= $item->site_name;
			$result['position']				= $item->position;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("payroll_employee/preview/".$item->id).'">Lihat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function preview($employee_id=FALSE)
	{
		$this->load->model(array('employee_model','employee_education_model', 'employee_emergency_model', 'employee_history_model', 'employee_language_model', 'employee_reference_model', 'employee_skill_model'));
		$data['_TITLE_'] 		= 'Preview Karyawan';
		$data['_PAGE_']	 		= 'employee/preview';
		$data['_MENU_PARENT_'] 	= 'personalia';
		$data['_MENU_'] 		= 'employee';

		$data['id'] = $employee_id;

		if (!$employee_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('payroll_employee/list'));
		}

		$data['list_education'] = $this->employee_education_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.institute, A.start, A.end'));
		$data['list_emergency'] = $this->employee_emergency_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.relation, A.phone, A.address'));
		$data['list_history'] 	= $this->employee_history_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.company_name, A.position, A.period,  A.salary, A.company_address, A.resign_note'));
		$data['list_language'] 	= $this->employee_language_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.language, A.verbal, A.write'));
		$data['list_reference'] = $this->employee_reference_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.company_name, A.position, A.phone'));
		$data['list_skill'] 	= $this->employee_skill_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));

		$data['preview'] = $this->employee_model->preview(array('id' => $employee_id));
		$data['preview']->date_birth = format_date_ina($data['preview']->date_birth);
		$data['preview']->city_birth = str_replace(array('Kota', 'Kabupaten'), '', $data['preview']->city_birth);
		$this->load->view('payroll_employee/preview', $data);
	}

	public function export()
	{
		$this->load->model(array('employee_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = "Data Karyawan";
		$title = 'daftar_karyawan';
		$files = glob(FCPATH."files/".$title."/*");

		$params = $this->input->get();
		$params['columns'] 		= "A.id, A.employee_number, A.full_name, A.id_card, B.name AS site_name, C.name AS company_name, A.email, A.phone_number, A.bank_account, E.name AS position, D.contract_type, D.contract_end";
		$params['orderby']		= "A.full_name";
		$params['order']		= "ASC";
		$params['status']		= 1;
		$params['status_approval'] 	= 3;
		$params['site_id']			=  $this->session->userdata('site');


		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Nomor Induk Karyawan");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("C".$i, "Nomor Identitas (KTP/SIM)");
		$excel->getActiveSheet()->setCellValue("D".$i, "Unit Kontrak");
		$excel->getActiveSheet()->setCellValue("E".$i, "Site Bisnis");
		$excel->getActiveSheet()->setCellValue("F".$i, "Email");
		$excel->getActiveSheet()->setCellValue("G".$i, "Nomor Whatsapp");
		$excel->getActiveSheet()->setCellValue("H".$i, "No Rekening");
		$excel->getActiveSheet()->setCellValue("I".$i, "Jabatan");
		$excel->getActiveSheet()->setCellValue("J".$i, "Tipe Kontrak");
		$excel->getActiveSheet()->setCellValue("K".$i, "Akhir Kontrak");

		$list_candidate = $this->employee_model->gets($params);
		$i=2;
		foreach ($list_candidate as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->employee_number);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->company_name);
			$excel->getActiveSheet()->setCellValue("E".$i, $item->site_name);
			$excel->getActiveSheet()->setCellValue("F".$i, $item->email);
			$excel->getActiveSheet()->setCellValueExplicit("G".$i, $item->phone_number);
			$excel->getActiveSheet()->setCellValueExplicit("H".$i, $item->bank_account);
			$excel->getActiveSheet()->setCellValue("I".$i, $item->position);
			$excel->getActiveSheet()->setCellValue("J".$i, $item->contract_type);
			$date = new DateTime($item->contract_end);
			$excel->getActiveSheet()->setCellValue("K".$i, PHPExcel_Shared_Date::PHPToExcel($date));
			$excel->getActiveSheet()->getStyle("K".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			$i++;
		}
		$excel->getActiveSheet()->setTitle('Daftar Karyawan');
		
		$excel->createSheet();
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function resign($employee_id = FALSE){
		$this->load->model(array('employee_model'));
		
		if($this->input->post()){
			$employee_id= $this->input->post('employee_id');
			$id_card 	= $this->input->post('id_card');

			$url_document = 'files/document/'.$id_card;
			$document_path = FCPATH.$url_document;
			if(!is_dir($document_path)){
				mkdir($document_path, 0755, TRUE);
			}
			
			$config['upload_path'] 	= $document_path;
			$config['overwrite']  	= TRUE;
			$config['allowed_types']= 'jpg|jpeg|png|pdf|doc|docx';
			$config['max_size'] 	= 1000;
			if($_FILES['document_resign']['name'] != ""){
				$config['file_name'] 	= 'resign';
				$this->load->library('upload', $config, 'document_resign');

				if (!$this->document_resign->do_upload('document_resign')) {
					$data['message']= message_box('<strong>Gagal!</strong> '.$this->document_resign->display_errors().'.','danger');
				} else {
					$document 		= $this->document_resign->data();
					$url_document 	= base_url($url_document.'/'.$document['file_name']);
				}
			}

			$note = $this->input->post('note');
			$this->employee_model->save(array('id' => $employee_id, 'status_approval' => 4, 'document_resign' => $url_document, 'resign_note' => $note));
			
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			redirect(base_url('payroll_employee/list'));
		}
		
		$data['employee'] 	= $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.id, A.employee_number, A.full_name, A.id_card, A.address_card, DATE_FORMAT(A.date_birth, "%d/%m/%Y") AS date_birth, A.company_id'));
		$data['_TITLE_'] 		= 'Karyawan '.$data['employee']->full_name;
		$data['_PAGE_'] 		= 'payroll_employee/resign';
		$data['_MENU_PARENT_'] 	= 'payroll_employee';
		$data['_MENU_'] 		= 'payroll_employee';
		
		$this->view($data);
	}

	public function import(){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('employee_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {
				$employee_number	= replace_null($sheet->getCell('A'.$row)->getValue());
				$site_id			= $this->session->userdata('site');
				$candidate 			= $this->employee_model->get(array('employee_number' => $employee_number, 'site_id' => $site_id, 'columns' => 'A.id'));
				if(!$candidate){
					continue;
				}
				$data['id'] 					= $candidate->id;
				$data['email']					= replace_null($sheet->getCell('F'.$row)->getValue());
				$data['phone_number']			= replace_null($sheet->getCell('G'.$row)->getValue());
				$data['bank_account']			= preg_replace("/[^0-9]/", "", replace_null($sheet->getCell('H'.$row)->getValue()));
				$save_id = $this->employee_model->save($data);
			}

			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
					redirect(base_url('payroll_employee/list'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
					redirect(base_url('payroll_employee/list'));
		}
	}
}
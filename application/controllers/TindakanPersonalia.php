<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class TindakanPersonalia extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		require APPPATH.'third_party/PHPMailer/Exception.php';
		require APPPATH.'third_party/PHPMailer/PHPMailer.php';
		require APPPATH.'third_party/PHPMailer/SMTP.php';
	}

	public function approval(){
		$data['_TITLE_'] 		= 'Approval';
		$data['_PAGE_'] 		= 'tindakanPersonalia/approval';
		$data['_MENU_PARENT_'] 	= 'tindakanPersonalia/approval';
		$data['_MENU_'] 		= 'approval';

		$this->view($data);
	}

	public function cuti(){
		$this->load->model('vacation_model');
		$this->load->model('tindakanPersonalia_model');
		
		$params['employee_id'] = $this->session->userdata('employee_id');
		$params['columns'] = 'B.vacation';
		$list 	= $this->vacation_model->get($params);
		$sisa_cuti = null != $list ? get_object_vars($list) : null;
		$sisa_cuti_normatif = get_object_vars($this->tindakanPersonalia_model->getCountCuti(2))['jumlah_hari'] == null ? 0 : get_object_vars($this->tindakanPersonalia_model->getCountCuti(2))['jumlah_hari'];
		$sisa_cuti_non_normatif = get_object_vars($this->tindakanPersonalia_model->getCountCuti(3))['jumlah_hari'] == null ? 0 : get_object_vars($this->tindakanPersonalia_model->getCountCuti(3))['jumlah_hari'];
		

		$data['_TITLE_'] 			= 'Cuti';
		$data['sisa_cuti'] 			= null != $sisa_cuti ? $sisa_cuti['vacation'] : 0;
		$data['cuti_normatif'] 		= $sisa_cuti_normatif;
		$data['cuti_non_normatif'] 	= $sisa_cuti_non_normatif;

		$data['_PAGE_'] 		= 'tindakanPersonalia/cuti';
		$data['_MENU_PARENT_'] 	= 'tindakanPersonalia';
		$data['_MENU_'] 		= 'cuti';

		$this->view($data);
	}

	public function personalia_cuti(){
		$this->load->model('vacation_model');
		$this->load->model('tindakanPersonalia_model');
		
	
		$data['_TITLE_'] 			= 'Cuti Personalia';

		$data['_PAGE_'] 		= 'tindakanPersonalia/personalia_cuti';
		$data['_MENU_PARENT_'] 	= 'tindakanPersonalia';
		$data['_MENU_'] 		= 'personalia_cuti';

		$this->view($data);
	}

	function convValue($data){
		if($data == 1){
			return "Approve";
		}else if($data == 0){
			return "Reject";
		}else{
			return "Belum";
		}
	}

	function convStatus($adaApproval, $disetujuiOleh, $status, $nama){
		if($adaApproval == 0){
			return '<span class="badge badge-info">Tidak ada</span>';
		}else if($disetujuiOleh == null){
			return '<span class="badge badge-warning">Belum</span>';
		}else if($status == 0){
			return '<span class="badge badge-danger">Reject</span>';
		}else if($status == 1){
			return '<span class="badge badge-success">Approve By</span>'.'<span class="badge badge-success">'.$nama.'</span>';;
		}
	}
	
	public function cuti_ajax(){
		$this->load->model(array('tindakanPersonalia_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, B.full_name as employee_name, A.created_at, A.start_date, A.end_date, A.verifikasi_by, A.verifikasi_at, A.disetujui_by, 
		A.disetujui_at, A.diketahui_by, A.diketahui_at, A.keterangan, A.verifikasi_status, A.disetujui_status, A.diketahui_status, A.dokumen_pendukung, A.struktur_2, 
		A.struktur_3, C.full_name as ver_by, D.full_name as setujui_by, E.full_name as diketahui_by ';
		
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['employee_id']	= $this->session->userdata('employee_id');
		
		$list 	= $this->tindakanPersonalia_model->gets_cuti($params);
		$total 	= $this->tindakanPersonalia_model->gets_cuti($params, TRUE);
		
		$data 	= array();
		$i = 1 ;
		foreach($list as $item)
		{
			$result['id']			= $i;
			$result['employee_name']= $item->employee_name;
			$result['created_at']	= $item->created_at;
			$result['start_date'] 	= $item->start_date;
			$result['end_date'] 	= $item->end_date;
			// print_r($this->convValue($item->verifikasi_status));
			// die();
			$result['verifikasi_1'] = $item->verifikasi_at != null ? $item->verifikasi_status == 1 ? '<span display: inline-block; class="badge badge-success">Approve By </span>' . '<span display: inline-block; class="badge badge-success">'.$item->ver_by.'</span>'  :'<span class="badge badge-danger">Reject</span>' : '<span class="badge badge-warning">Belum</span>';
			$result['verifikasi_2'] = $this->convStatus($item->struktur_2, $item->disetujui_at, $item->disetujui_status, $item->setujui_by) ;
			$result['verifikasi_3'] = $this->convStatus($item->struktur_3, $item->diketahui_at, $item->diketahui_status, $item->diketahui_by) ;
			$result['file_pendukung'] = $item->dokumen_pendukung != null ? '<a href="'.base_url($item->dokumen_pendukung).'" target=”_blank”>Open</a>' : "-";
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action"  style="cursor:pointer;" data-href="'. base_url("tindakanPersonalia/preview/".$item->id).'"><i class="fas fa-eye text-white"></i></a>' .
				// <a onclick="confirm_del(this)" class="btn-sm btn-danger" style="cursor:pointer;" data-href="'.base_url("tindakanPersonalia/delete/".$item->id).'"><i class="far fa-trash-alt text-white"></i></a>
				'<a onclick="setValueId('.$item->id.',\''.$item->start_date.'\')" class="btn-sm btn-success btn-action" data-toggle="modal" data-target="#modal-upload"  style="cursor:pointer;"><i class="fas fa-upload text-white"></i></a>';
				// <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-upload">
			$result['keterangan']	= $item->keterangan;

			if(null == $item->verifikasi_by){
				$result['action'] = $result['action'] .
				'<a onclick="confirm_del(this)" class="btn-sm btn-danger" style="cursor:pointer;" data-href="'.base_url("tindakanPersonalia/delete/".$item->id).'"><i class="far fa-trash-alt text-white"></i></a>';
			}

			// onclick="uploadFileIGCSE(
			// 	document.querySelector('#year').value, 
			// 	document.querySelector('#grade').value, 
			// 	document.querySelector('#subject').value)"
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function personalia_cuti_ajax(){
		$this->load->model(array('tindakanPersonalia_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, B.full_name as employee_name, A.created_at, A.start_date, A.end_date, A.verifikasi_by, A.verifikasi_at, A.disetujui_by, 
		A.disetujui_at, A.diketahui_by, A.diketahui_at, A.keterangan, A.verifikasi_status, A.disetujui_status, A.diketahui_status, A.struktur_2, A.struktur_3, C.full_name as ver_by, D.full_name as setujui_by, E.full_name as diketahui_by ';
		
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['employee_id']	= $this->session->userdata('employee_id');
		
		$list 	= $this->tindakanPersonalia_model->personalia_gets_cuti($params);
		$total 	= $this->tindakanPersonalia_model->personalia_gets_cuti($params, TRUE);
		
		$data 	= array();
		$i = 1;
		foreach($list as $item)
		{
			$result['id']			= $i;
			$result['employee_name']= $item->employee_name;
			$result['created_at']	= $item->created_at;
			$result['start_date'] 	= $item->start_date;
			$result['end_date'] 	= $item->end_date;
			// print_r($this->convValue($item->verifikasi_status));
			// die();

			$result['verifikasi_1'] = $item->verifikasi_at != null ? $item->verifikasi_status == 1 ? '<span display: inline-block; class="badge badge-success">Approve By </span>' . '<span display: inline-block; class="badge badge-success">'.$item->ver_by.'</span>'  :'<span class="badge badge-danger">Reject</span>' : '<span class="badge badge-warning">Belum</span>';
			$result['verifikasi_2'] = $this->convStatus($item->struktur_2, $item->disetujui_at, $item->disetujui_status, $item->setujui_by) ;
			$result['verifikasi_3'] = $this->convStatus($item->struktur_3, $item->diketahui_at, $item->diketahui_status, $item->diketahui_by) ;
			
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action"  style="cursor:pointer;" data-href="'. base_url("tindakanPersonalia/preview/".$item->id).'"><i class="fas fa-eye text-white"></i></a>';

			$result['keterangan']	= $item->keterangan;

			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function preview($cuti_id=FALSE)
	{
		$this->load->model('tindakanPersonalia_model');
		$data['_TITLE_'] 		= 'Detail Cuti';
		$data['_PAGE_']	 		= 'tindakanPersonalia/preview';
		$data['_MENU_PARENT_'] 	= 'tindakanPersonalia';
		$data['_MENU_'] 		= 'cuti';
		
		if (!$cuti_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('tindakanPersonalia/cuti'));
		}

		$params['columns'] 		= 'A.id, B.full_name as employee_name, A.created_at, A.start_date, A.end_date, A.verifikasi_by, A.verifikasi_at, 
		A.disetujui_by, A.disetujui_at, A.diketahui_by, A.diketahui_at, A.keterangan, A.jumlah_hari, 
		if(A.verifikasi_status = 1, "Approve", if(A.verifikasi_status = 0, "Reject", "Belum")) as status1, 
		if(A.disetujui_status= 1, "Approve", if(A.disetujui_status= 0, "Reject", "Belum")) as status2, 
		if(A.diketahui_status= 1, "Approve", if(A.diketahui_status= 0, "Reject", "Belum")) as status3,
		if(A.kategori_id = 1, "Tahunan", if (A.kategori_id = 2, "Normatif", if(A.kategori_id = 3, "Sakit", if(A.kategori_id = 2, "Lain-lain", "-")))) as kategori,
        if(C.nama is not null, C.nama, "-") as type, D.full_name as verifikasi_by_name,
		E.full_name as disetujui_by_name,
		F.full_name as diketahui_by_name, A.dokumen_pendukung, A.struktur_1, A.struktur_2, A.struktur_3 ';
		$params['id'] 		= $cuti_id;

		$data['preview'] = $this->tindakanPersonalia_model->preview($params);
		$this->load->view('tindakanPersonalia/preview', $data);
	}

	public function approve()
	{
	
		$cuti_id 		= $this->input->get('id');

		$this->load->model('tindakanPersonalia_model');
		$data['_TITLE_'] 		= 'Approve Cuti';
		$data['_PAGE_']	 		= 'tindakanPersonalia/approve_form';
		$data['_MENU_PARENT_'] 	= 'tindakanPersonalia/approve_form';
		$data['_MENU_'] 		= 'approval';
		
		$params['columns'] 		= 'A.id, B.full_name as employee_name, A.created_at, A.start_date, A.end_date, A.verifikasi_by, A.verifikasi_at, A.disetujui_by, A.disetujui_at, A.diketahui_by, A.diketahui_at, A.keterangan, A.jumlah_hari';
		$params['id'] 			= $cuti_id;

		// print_r($params);
		// die();
		$data['data'] = $this->tindakanPersonalia_model->preview($params);
		$data['level'] = $this->input->get('level');
		// $data['data'] = $data;

		return $this->view($data);
	}

	public function delete($request_id = false)
	{
		$this->load->model('tindakanPersonalia_model');
		
		if ($request_id)
		{
			$data =  $this->tindakanPersonalia_model->get(array('id' => $request_id, 'columns' => 'A.id'));
			if ($data)
			{
				
				$insert = array('id' => $request_id, 'is_active' => 0);
				$result = $this->tindakanPersonalia_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('tindakanPersonalia/cuti'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('tindakanPersonalia/cuti'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('tindakanPersonalia/cuti'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('tindakanPersonalia/cuti'));
		}
	}

	public function cuti_update_approve()
	{
		$this->load->model('tindakanPersonalia_model');
		$this->load->model('vacation_model');
		$this->load->model('position_model');

		$data['id'] 		= $this->input->post('id');
		if($this->input->post('approval_level') == 'aprove_1'){
			$data['verifikasi_by'] 		= $this->session->userdata('employee_id');
			$data['verifikasi_at'] 		= date('Y-m-d H:i:s');
			$data['verifikasi_status'] 	= $this->input->post('status');
			$data['note_1']		= $this->input->post('note_1');
			$dataStruktur =  $this->tindakanPersonalia_model->get(array('id' => $data['id'], 'columns' => 'B.phone_number, B.position_id, B.full_name, A.struktur_2 as struktur, A.kategori_id, A.type_id, A.start_date, A.end_date, A.employee_id'));
		}else if($this->input->post('approval_level') == 'aprove_2'){
			$data['disetujui_by'] 		= $this->session->userdata('employee_id');
			$data['disetujui_at'] 		= date('Y-m-d H:i:s');
			$data['disetujui_status'] 	= $this->input->post('status');
			$data['note_2']		= $this->input->post('note_1');
			$dataStruktur =  $this->tindakanPersonalia_model->get(array('id' => $data['id'], 'columns' => 'B.phone_number, B.position_id, B.full_name, A.struktur_3 as struktur, A.kategori_id, A.type_id, A.start_date, A.end_date, A.employee_id'));
		}else if($this->input->post('approval_level') == 'aprove_3'){
			$data['diketahui_by'] 		= $this->session->userdata('employee_id');
			$data['diketahui_at'] 		= date('Y-m-d H:i:s');
			$data['diketahui_status'] 	= $this->input->post('status');
			$data['note_3']		= $this->input->post('note_1');
			$dataStruktur =  $this->tindakanPersonalia_model->get(array('id' => $data['id'], 'columns' => 'B.phone_number, B.position_id, B.full_name, A.struktur_3 as struktur, A.kategori_id, A.type_id, A.start_date, A.end_date, A.employee_id'));
		}
		$paramsSisa['employee_id']  = $dataStruktur->employee_id;
		$paramsSisa['columns'] 		= 'B.vacation';
		$listSisa 					= $this->vacation_model->get($paramsSisa);
		$sisa_cuti 				= null != $listSisa ? get_object_vars($listSisa) : null;

		$this->load->model(array('wappin_model'));
		$config_id = 1;
		$wappin 		= $this->wappin_model->get(array('id' => $config_id, 'columns' => 'A.access_token, A.base_url'));
		if($wappin){
			$dataPesan['token'] 	= "Authorization: Bearer ".$wappin->access_token;
			$dataPesan['base_url']= $wappin->base_url;
		}


		$save_id	 	= $this->tindakanPersonalia_model->approve($data);
		if ($save_id) {
			if($this->input->post('status') == '1'){
				//KIRIM ATASAN
				if($dataStruktur->struktur > 0){
					$paramAtasan['position_id'] = $dataStruktur->struktur;
					$listAtasan = $this->tindakanPersonalia_model->list_atasanV2($paramAtasan);

					foreach($listAtasan as $item)
					{
						if($dataStruktur->kategori_id == 1){
							$dataPesan['nama_cuti'] = "Tahunan";
						}else if($dataStruktur->kategori_id == 2){
							// Cuti Khusus
							$dataTypeCuti['id'] = $dataStruktur->struktur;
							$dataTypeCuti['columns'] = 'A.nama';
							$dataPesan['nama_cuti'] = $this->tindakanPersonalia_model->getMasterTypeCutiById($dataTypeCuti)->nama;
						}else if($dataStruktur->kategori_id == 3){
							$dataPesan['nama_cuti'] = "Sakit";
						}else{
							$dataPesan['nama_cuti'] = "Lain -lain";
						}

						$dataPesan['nama_atasan'] = $item->full_name . " - " . $item->position_name;
						$dataPesan['nama_karyawan'] = $dataStruktur->full_name;
						$dataPesan['tanggal_1'] = $dataStruktur->start_date;
						$dataPesan['tanggal_2'] = $dataStruktur->end_date;
						$dataPesan['cuti_tahunan'] = $sisa_cuti['vacation'];
						$dataPesan['klik_tautan'] = 'https://hris.shelterapp2.co.id/tindakanPersonalia/approval';
						$dataPesan['phone_number'] = $item->phone_number;
						
						$this->send_message_atasan($dataPesan);
					}
				}
			}
			
			$paramJabatan['id'] = $this->session->userdata('position_id');;
			$paramJabatan['columns'] = 'A.name';
			$jabatanApproval = $this->position_model->get($paramJabatan);
			
			//KIRIM NOTIFIKASI KE PEMOHON
			$dataPemohon['nama_pemohon'] = $dataStruktur->full_name;
			$dataPemohon['tanggal_1'] = $dataStruktur->start_date;
			$dataPemohon['tanggal_2'] = $dataStruktur->end_date;
			$dataPemohon['status'] = $this->input->post('status') == '1' ? 'Setujui' : 'Tolak';
			$dataPemohon['sisa_cuti'] = $sisa_cuti['vacation'];
			$dataPemohon['nama_approval'] = $this->session->userdata('full_name');
			$dataPemohon['jabatan_approval'] = $jabatanApproval->name;
			$dataPemohon['phone_number'] = $dataStruktur->phone_number;
			$dataPemohon['link'] = 'https://hris.shelterapp2.co.id/tindakanPersonalia/cuti';
			
			// print_r($dataPemohon);
			// die();
			$this->send_message_pemohon($dataPemohon);

			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>Cuti Berhasil diapprove','success'));
			redirect(base_url('tindakanPersonalia/approval'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Cuti gagal di approve','danger'));
			redirect(base_url('tindakanPersonalia/approval'));
		}
	}

	public function cuti_update_document()
	{
		$this->load->model('tindakanPersonalia_model');

		$data['id'] 		= $this->input->post('form-id');
		if($_FILES['dokumen_pendukung']['name'] != ""){
			$url_document = 'files/document/cuti/'.$this->session->userdata('employee_id');
			$document_path = FCPATH.$url_document;
			$dname = explode(".", $_FILES['dokumen_pendukung']['name']);
			$ext = end($dname);

			if(!is_dir($document_path)){
				mkdir($document_path, 0755, TRUE);
			}

			$config['upload_path'] 	= $document_path;
			$config['overwrite']  	= TRUE;
			$config['allowed_types']= 'jpg|jpeg|png|pdf|doc|docx';

			// $config['file_name'] 	= $this->input->post('form-start-date');
			$config['file_name'] 	= $data['id'];

			// print_r($config);
			// die();
			$this->load->library('upload', $config, 'dokumen_pendukung');
			if (!$this->dokumen_pendukung->do_upload('dokumen_pendukung')) {
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Proses upload dokumen pendukung.','danger'));
			} else {
				$document 	= $this->dokumen_pendukung->data();
				$data['dokumen_pendukung'] = $url_document.'/'.$data['id'].'.'.$ext;
			}
			
			$save_id	 	= $this->tindakanPersonalia_model->updateDocument($data);
		}

		if ($save_id) {
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>Dokumen berhasil di unggah','success'));
			redirect(base_url('tindakanPersonalia/cuti'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Dokumen gagal di unggah','danger'));
			redirect(base_url('tindakanPersonalia/cuti'));
		}
	}

	public function approval_ajax(){
		$this->load->model(array('tindakanPersonalia_model'));

		$column_index = $_POST['order'][0]['column'];

		$params['columns'] = 'A.id, A.created_at, A.start_date, A.end_date, A.verifikasi_by, A.verifikasi_at, A.disetujui_by, A.disetujui_at, A.diketahui_by, A.diketahui_at, 
	A.keterangan, A.verifikasi_status, A.disetujui_status, A.diketahui_status, A.struktur_2, A.struktur_3,
    B.full_name as employee_name, if(A.struktur_1 = '.$this->session->userdata('position_id').', "aprove_1", if(A.struktur_2 = '.$this->session->userdata('position_id').', 
	"aprove_2", if(A.struktur_3 = '.$this->session->userdata('position_id').', "aprove_3", "-"))) as level_approve, C.full_name as ver_by, 
	D.full_name as setujui_by, E.full_name as diketahui_by';
	
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['employee_id']	= $this->session->userdata('employee_id');
		
		$list 	= $this->tindakanPersonalia_model->gets_approval($params);
		$total 	= $this->tindakanPersonalia_model->gets_approval($params, TRUE);
		
		$data 	= array();
		$i = 1;
		foreach($list as $item)
		{
			// $result['id']			= $item->id;
			$result['id']			= $i;
			$result['employee_name']= $item->employee_name;
			$result['created_at']	= $item->created_at;
			$result['start_date'] 	= $item->start_date;
			$result['end_date'] 	= $item->end_date;
			$result['verifikasi_by']= $item->verifikasi_by;
			$result['verifikasi_at']= $item->verifikasi_at;
			$result['disetujui_by']	= $item->disetujui_by;
			$result['disetujui_at'] = $item->disetujui_at;
			$result['diketahui_by']	= $item->diketahui_by;
			$result['diketahui_at']	= $item->diketahui_at;
			$result['keterangan']	= $item->keterangan;

			$result['verifikasi_1'] = $item->verifikasi_at != null ? $item->verifikasi_status == 1 ? '<span display: inline-block; class="badge badge-success">Approve By </span>' . '<span display: inline-block; class="badge badge-success">'.$item->ver_by.'</span>'  :'<span class="badge badge-danger">Reject</span>' : '<span class="badge badge-warning">Belum</span>';
			$result['verifikasi_2'] = $this->convStatus($item->struktur_2, $item->disetujui_at, $item->disetujui_status, $item->setujui_by) ;
			$result['verifikasi_3'] = $this->convStatus($item->struktur_3, $item->diketahui_at, $item->diketahui_status, $item->diketahui_by) ;
			
			// <a disable href="'.base_url("tindakanPersonalia/approve?id=".$item->id."&level=".$item->level_approve).'" class="btn-sm btn-warning btn-action"  style="cursor:pointer;"><i class="fas fa-pencil-alt text-white"></i></a>'
			$result['action'] 		=
			'<a onclick="preview(this)" class="btn-sm btn-info btn-action"  style="cursor:pointer;" data-href="'. base_url("tindakanPersonalia/preview/".$item->id).'"><i class="fas fa-eye text-white"></i></a>';
			 $result['level_approve'] 		= $item->level_approve;
			
			if($item->level_approve == 'aprove_1'){
				//  print_r($item->level_approve);
				if(null == $item->verifikasi_by){
					$result['action'] = $result['action'] .
					'<a disable href="'.base_url("tindakanPersonalia/approve?id=".$item->id."&level=".$item->level_approve).'" class="btn-sm btn-warning btn-action"  style="cursor:pointer;"><i class="fas fa-pencil-alt text-white"></i></a>';
				}	
			}else if($item->level_approve == 'aprove_2'){
				if(null == $item->disetujui_by && null != $item->verifikasi_by && $item->verifikasi_status = 1){
					$result['action'] = $result['action'] .
					'<a disable href="'.base_url("tindakanPersonalia/approve?id=".$item->id."&level=".$item->level_approve).'" class="btn-sm btn-warning btn-action"  style="cursor:pointer;"><i class="fas fa-pencil-alt text-white"></i></a>';
				}
			}else if($item->level_approve == 'aprove_3'){
				if(null == $item->diketahui_by && null != $item->disetujui_by && $item->disetujui_status = 1){
					$result['action'] = $result['action'] .
					'<a disable href="'.base_url("tindakanPersonalia/approve?id=".$item->id."&level=".$item->level_approve).'" class="btn-sm btn-warning btn-action"  style="cursor:pointer;"><i class="fas fa-pencil-alt text-white"></i></a>';
				}
			}
			
			 
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function cuti_form()
	{
		$this->load->model(array('tindakanPersonalia_model'));
		$this->load->model('vacation_model');
		$this->load->model('tindakanPersonalia_model');
		
		$data['employee_id']		= $this->session->userdata('employee_id');
		$data['start_date']			= '';
		$data['end_date'] 			= '';
		$data['keterangan'] 		= '';
		$data['dokumen_pendukung']	= '';

		if($this->input->post()){
			$data['start_date']		= $this->input->post('start_date');
			$data['end_date']		= $this->input->post('end_date');
			$data['keterangan'] 	= $this->input->post('keterangan');
			$data['kategori_id'] 	= $this->input->post('kategori');
			if($this->input->post('kategori') != 2){
				$data['type_id'] = '';
			}else{
				$data['type_id'] = explode("::",$this->input->post('type'))[0];
			}
			
			$this->form_validation->set_rules('start_date', '', 'required');
			$this->form_validation->set_rules('end_date', '', 'required');
			$this->form_validation->set_rules('keterangan', '', 'required');
	
			$date1=new DateTime($data['start_date']);
			$date2=new DateTime($data['end_date']);
		
			/*
			ambil data approve dari struktur id V1
			*/
			// $params['columns'] 	= 'SJ.id as approve_request,apr1.id as approve_1,apr2.id as approve_2,apr3.id as approve_3';
			// $struktur			= $this->tindakanPersonalia_model->get_struktur($params);
			// $data['struktur_1'] = $struktur->approve_1;
			// $data['struktur_2'] = $struktur->approve_2;
			// $data['struktur_3'] = $struktur->approve_3;

			/*
			ambil data approve dari struktur id V2
			*/
			$params['columns'] 	= ' A.approval_1, A.approval_2, A.approval_3 ';
			$struktur			= $this->tindakanPersonalia_model->get_strukturV2($params);
			$data['struktur_1'] = $struktur->approval_1;
			$data['struktur_2'] = $struktur->approval_2;
			$data['struktur_3'] = $struktur->approval_3;
			
			if($date2 < $date1){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Tanggal Selesai tidak boleh < dari Tanggal Mulai','danger'));
			}else{
				if ($this->form_validation->run() == FALSE)
				{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
				}else{

					if($_FILES['dokumen_pendukung']['name'] != ""){
						$url_document = 'files/document/cuti/'.$data['employee_id'];
						$document_path = FCPATH.$url_document;
						$dname = explode(".", $_FILES['dokumen_pendukung']['name']);
						$ext = end($dname);

						if(!is_dir($document_path)){
							mkdir($document_path, 0755, TRUE);
						}
		
						$config['upload_path'] 	= $document_path;
						$config['overwrite']  	= TRUE;
						$config['allowed_types']= 'jpg|jpeg|png|pdf|doc|docx';
		
						$config['file_name'] 	= $data['start_date'];
						$this->load->library('upload', $config, 'dokumen_pendukung');
						if (!$this->dokumen_pendukung->do_upload('dokumen_pendukung')) {
							$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Proses upload dokumen pendukung.','danger'));
						} else {
							$document 	= $this->dokumen_pendukung->data();
							$data['dokumen_pendukung'] = $url_document.'/'.$data['start_date'].'.'.$ext;
						}
					}

					$params['employee_id'] = $this->session->userdata('employee_id');
					$params['columns'] 		= 'B.vacation';
					$list 					= $this->vacation_model->get($params);
					$sisa_cuti 				= null != $list ? get_object_vars($list) : null;
					$data_cuti['sisa_cuti'] 		= $sisa_cuti;

					$save_id	 	= $this->tindakanPersonalia_model->save($data, $data_cuti);
					if ($save_id) {
						// Notifikasi Approval 1
						/*
						nama_atasan (nama atasan)
						nama_cuti
						nama_karyawan / pemohon
						tanggal_1 / mulai	
						tanggal_2 / sampai
						cuti_tahunan / sisa cuti tahunan
						klik_tautan
						*/
						$paramAtasan['position_id'] = $struktur->approval_1;
						// print_r($paramAtasan);
						// 	die();
						$listAtasan = $this->tindakanPersonalia_model->list_atasanV2($paramAtasan);
						foreach($listAtasan as $item)
						{
							if($data['kategori_id'] == 1){
								$dataPesan['nama_cuti'] = "Tahunan";
							}else if($data['kategori_id'] == 2){
								// Cuti Khusus
								$dataPesan['nama_cuti'] = explode("::",$this->input->post('type'))[1];
							}else if($data['kategori_id'] == 3){
								$dataPesan['nama_cuti'] = "Sakit";
							}else{
								$dataPesan['nama_cuti'] = "Lain -lain";
							}
							//phone = $item->phone_number;

							$dataPesan['nama_atasan'] = $item->full_name . " - " . $item->position_name;
							$dataPesan['nama_karyawan'] = $this->session->userdata('full_name');
							$dataPesan['tanggal_1'] = $data['start_date'];
							$dataPesan['tanggal_2'] = $data['end_date'];
							$dataPesan['cuti_tahunan'] = $sisa_cuti['vacation'];
							$dataPesan['klik_tautan'] = 'https://hris.shelterapp2.co.id/tindakanPersonalia/approval';
							$dataPesan['phone_number'] = $item->phone_number;
							$this->send_message_atasan($dataPesan);
						}
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
						redirect(base_url('tindakanPersonalia/cuti'));
					}else{
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
						redirect(base_url('tindakanPersonalia/cuti_form'));
					}
				}
			}
		}

		$data['m_type_cuti'] = $this->tindakanPersonalia_model->getMasterTypeCuti();
		$data['_TITLE_'] 		= 'Pengajuan Cuti';
		$data['_PAGE_'] 		= 'tindakanPersonalia/cuti_form';
		$data['_MENU_PARENT_'] 	= 'tindakanPersonalia';
		$data['_MENU_'] 		= 'cuti';
		return $this->view($data);
	}

	public function personalia_cuti_form()
	{
		$this->load->model(array('tindakanPersonalia_model'));
		$this->load->model('vacation_model');
		$this->load->model('tindakanPersonalia_model');
		
		$data['employee_id']		= '';
		$data['start_date']			= '';
		$data['end_date'] 			= '';
		$data['keterangan'] 		= '';

		if($this->input->post()){
			$data['start_date']		= $this->input->post('start_date');
			$data['end_date']		= $this->input->post('end_date');
			$data['keterangan'] 	= $this->input->post('keterangan');
			$data['kategori_id'] 	= $this->input->post('kategori');
			$data['type_id'] 		= $this->input->post('type');
			$data['employee_id'] 	= $this->input->post('employee_id');
			
			if($this->input->post('kategori') != 2){
				$data['type_id'] = '';
			}
			
			$this->form_validation->set_rules('start_date', '', 'required');
			$this->form_validation->set_rules('end_date', '', 'required');
			$this->form_validation->set_rules('keterangan', '', 'required');
	
			$date1=new DateTime($data['start_date']);
			$date2=new DateTime($data['end_date']);
		
			/*
			ambil data approve dari struktur id
			*/
			$params['columns'] 	= 'SJ.id as approve_request,apr1.id as approve_1,apr2.id as approve_2,apr3.id as approve_3';
			$params['employee_id']	= $this->input->post('employee_id');
			$struktur			= $this->tindakanPersonalia_model->getPersonaliaStruktur($params);
			$data['struktur_1'] = $struktur->approve_1;
			$data['struktur_2'] = $struktur->approve_2;
			$data['struktur_3'] = $struktur->approve_3;
			
			if($date2 < $date1){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Tanggal Selesai tidak boleh < dari Tanggal Mulai','danger'));
			}else{
				if ($this->form_validation->run() == FALSE)
				{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
				}else{
					// $params['employee_id'] = $this->input->post('employee_id');
					// $params['columns'] 		= 'B.vacation';
					// $list 					= $this->vacation_model->get($params);
					// $sisa_cuti 				= null != $list ? get_object_vars($list) : null;
					// $data_cuti['sisa_cuti'] 		= $sisa_cuti;

					$save_id	 	= $this->tindakanPersonalia_model->saveCutiByPersonalia($data);
					if ($save_id) {
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
						redirect(base_url('tindakanPersonalia/personalia_cuti'));
					}else{
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
						redirect(base_url('tindakanPersonalia/personalia_cuti_form'));
					}
				}
			}
		}

		$data['m_type_cuti'] = $this->tindakanPersonalia_model->getMasterTypeCuti();
		$data['m_karyawan'] = $this->tindakanPersonalia_model->getMasterEmployee();
		$data['_TITLE_'] 		= 'Cuti untuk karyawan';
		$data['_PAGE_'] 		= 'tindakanPersonalia/personalia_cuti_form';
		$data['_MENU_PARENT_'] 	= 'tindakanPersonalia';
		$data['_MENU_'] 		= 'cuti';
		return $this->view($data);
	}

	private function send_message_atasan($data = array()){
		$url 		= $data['base_url'].'/v1/messages';
		$request 	= array(
			'to' => $data['phone_number'],
			'type'	=> 'template',
			'template' => array(
				'name'	=> $this->config->item('cuti_atasan_name'),
				'namespace' => $this->config->item('cuti_atasan_namespace'),
				'language'	=> array(
					'policy' => 'deterministic',
					'code'	=> 'id'
				),
				'components' => array(
					array(
						'type' => 'body',
						'parameters'=> array(
							array(
								'type' => 'text',
								'text'	=> $data['nama_atasan']
							),
							array(
								'type' => 'text',
								'text'	=> $data['nama_cuti']
							),
							array(
								'type' => 'text',
								'text'	=> $data['nama_karyawan']
							),
							array(
								'type' => 'text',
								'text'	=> $data['tanggal_1']
							),
							array(
								'type' => 'text',
								'text'	=> $data['tanggal_2']
							),
							array(
								'type' => 'text',
								'text'	=> $data['cuti_tahunan']
							),
							array(
								'type' => 'text',
								'text'	=> $data['klik_tautan']
							)
						)
					)
				)
			),
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $data['token']));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
		$response = curl_exec($ch);
		$result = json_decode($response, true);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if($httpcode == 200){
			return true;
		}else{
			return false;
		}
	}

	private function send_message_pemohon($data = array()){	
		$url 		= $data['base_url'].'/v1/messages';
		$request 	= array(
			'to' => $data['phone_number'],
			'type'	=> 'template',
			'template' => array(
				'name'	=> $this->config->item('cuti_atasan_name'),
				'namespace' => $this->config->item('cuti_atasan_namespace'),
				'language'	=> array(
					'policy' => 'deterministic',
					'code'	=> 'id'
				),
				'components' => array(
					array(
						'type' => 'body',
						'parameters'=> array(
							array(
								'type' => 'text',
								'text'	=> $data['nama_pemohon']
							),
							array(
								'type' => 'text',
								'text'	=> $data['tanggal_1']
							),
							array(
								'type' => 'text',
								'text'	=> $data['tanggal_2']
							),
							array(
								'type' => 'text',
								'text'	=> $data['status']
							),
							array(
								'type' => 'text',
								'text'	=> $data['nama_approval']
							),
							array(
								'type' => 'text',
								'text'	=> $data['jabatan_approval']
							),
							array(
								'type' => 'text',
								'text'	=> $data['sisa_cuti']
							),
							array(
								'type' => 'text',
								'text'	=> $data['link']
							)
						)
					)
				)
			),
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $data['token']));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
		$response = curl_exec($ch);
		$result = json_decode($response, true);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if($httpcode == 200){
			return true;
		}else{
			return false;
		}
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branch extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52))){
			redirect(base_url());
		}
	}
	
	public function delete($branch_id = false)
	{
		$this->load->model('branch_model');
		if ($branch_id)
		{
			$data =  $this->branch_model->get(array('id' => $branch_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $branch_id, 'is_active' => 0);
				$result = $this->branch_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('branch/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('branch/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('branch/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('branch/list'));
		}
	}

	public function preview($branch_id=FALSE)
	{
		$this->load->model('branch_model');
		$data['_TITLE_'] 		= 'Preview Branch';
		$data['_PAGE_']	 		= 'branch/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'branch';

		$data['id'] = $branch_id;

		
		if (!$branch_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('branch/list'));
		}

		$data['preview'] = $this->branch_model->preview(array('id' => $branch_id));
		$this->load->view('branch/preview', $data);
	}

	public function form($branch_id = FALSE)
	{
		$this->load->model(array('branch_model'));

		$data['id'] 		= '';
		$data['name']		= '';
		$data['description']= '';

		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['name'] 		= $this->input->post('name');
			$data['description']= $this->input->post('description');
			
			$this->form_validation->set_rules('name', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$insert 	= array('id' => $data['id'], 'name' => $data['name'], 'description' => $data['description']);
				$save_id	= $this->branch_model->save($insert);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('branch/list'));
		}

		if ($branch_id)
		{
			$data = (array) $this->branch_model->get(array('id' => $branch_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('branch/list'));
			}
		}

		$data['_TITLE_'] 		= 'Branch';
		$data['_PAGE_'] 		= 'branch/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'branch';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Branch';
		$data['_PAGE_'] 		= 'branch/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'branch';

		$this->view($data);
	}

	public function list_ajax(){
		$this->load->model(array('branch_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.name, A.description';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['name']			= $_POST['columns'][1]['search']['value'];
		$params['description']	= $_POST['columns'][2]['search']['value'];
		
		$list 	= $this->branch_model->gets($params);
		$total 	= $this->branch_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['name'] 		= $item->name;
			$result['description']	= $item->description;
			if($item->id == 1){
				$result['action'] 		=
					'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("branch/preview/".$item->id).'">Lihat</a>';
				}else{
					$result['action'] 		=
					'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("branch/preview/".$item->id).'">Lihat</a>
					<a class="btn-sm btn-success btn-block" href="'.base_url("branch/form/".$item->id).'">Ubah</a>
					<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("branch/delete/".$item->id).'">Hapus</a>';
				}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}
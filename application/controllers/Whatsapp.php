<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Whatsapp extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52))){
			redirect(base_url());
		}
	}

	public function list($tab = 'personal'){
		$data['_TITLE_'] 		= 'Whatsapp';
		$data['_PAGE_'] 		= 'whatsapp/list';
		$data['_MENU_PARENT_'] 	= 'whatsapp';
		$data['_MENU_'] 		= 'whatsapp';
		$data['tab']			= $tab;
		return $this->view($data);
	}

	public function list_ajax(){
		$this->load->model(array('employee_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.id_card, A.full_name, A.phone_number, C.name AS company_name, B.name AS site_name, E.name AS position";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']		= 1;
		$params['status_nonjob'] 	= 0;
		$params['status_approval'] 	= 3;
		$params['branch_id']	= '';
		$params['site_id']		= 2;
		
		$params['id_card']			= $_POST['columns'][1]['search']['value'];
		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['phone_number']		= $_POST['columns'][3]['search']['value'];
		$params['site_name']		= $_POST['columns'][4]['search']['value'];
		$params['company_name']		= $_POST['columns'][5]['search']['value'];
		$params['position']			= $_POST['columns'][6]['search']['value'];
		
		$list 	= $this->employee_model->gets($params);
		$total 	= $this->employee_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{			
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['id_card']				= $item->id_card;
			$result['full_name']			= $item->full_name;
			$result['phone_number']			= $item->phone_number;
			$result['site_name']			= $item->site_name;
			$result['company_name']			= $item->company_name;
			$result['position']				= $item->position;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("employee_contract/preview/".$item->id).'">Lihat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function personal(){
		if($this->input->post()){
			$this->load->model(array('employee_model'));
			$id 		= $this->input->post('id');
			$message 	= $this->input->post('message');
			foreach ($id as $employee_id) {
				$employee 	= $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.phone_number'));
				if($employee){
					if($employee->phone_number != ''){
						$start = substr($employee->phone_number, 0, 1 );
						if($start == '0'){
							$employee->phone_number = '62'.substr($employee->phone_number, 1,strlen($employee->phone_number));
						}else if($start == '+'){
							$employee->phone_number = substr($employee->phone_number, 1,strlen($employee->phone_number));
						}
						$this->send_personal(array('phone_number' => $employee->phone_number, 'message' => $message));
					}
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
			redirect(base_url('whatsapp/list/personal'));
		}
	}

	private function send_personal($data = array()){
		$data_sending = Array();
		$data_sending["api_key"] 		= $this->config->item('api_key');
		$data_sending["number_key"] 	= $this->config->item('number_key');
		$data_sending["phone_no"] 		= $data['phone_number'];
		$data_sending["message"] 		= $data['message'];
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $this->config->item('api').'send_message',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => json_encode($data_sending),
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/json'
		  ),
		));
		$response = curl_exec($curl);
		curl_close($curl);
		// echo $response;
	}

	public function group(){
		if($this->input->post()){
			// $this->load->model(array('employee_model'));
			$id 		= $this->input->post('id');
			$message 	= $this->input->post('message');
			foreach ($id as $group_id) {
				$this->send_group(array('group_id' => $group_id, 'message' => $message));
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
			redirect(base_url('whatsapp/list/group'));
		}
	}

	private function send_group($data = array()){
		$data_sending = Array();
		$data_sending["api_key"] 	= $this->config->item('api_key');
		$data_sending["number_key"] = $this->config->item('number_key');
		$data_sending["group_id"] 	= $data['group_id'];
		$data_sending["message"] 	= $data['message'];
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $this->config->item('api').'send_message_group',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => json_encode($data_sending),
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/json'
		  ),
		));
		print_r($data_sending);
		$response = curl_exec($curl);
		curl_close($curl);
		echo $response;
		die();
	}
}
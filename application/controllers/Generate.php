<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Generate extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function generate_user(){
		
		$this->load->model(array('generate_model', 'user_model'));

		$list_user = $this->generate_model->generate_user();
		foreach($list_user AS $user){
			$save['id'] = $user->user_id;
			$save['username'] = $user->id_card;
			$save['full_name'] = $user->employee_name;
			$this->user_model->save($save);
			print_prev($save);
		}
	}

	public function generate_payroll(){
		$this->load->model(array('payroll_model', 'employee_model'));

		$list_payroll	= $this->payroll_model->generate_payroll(array(
		// 'submission' => array('Draft'), 
		// 'site_id' => $this->session->userdata('site'), 
		'not_preview' => true,
		'columns' => 'A.id, A.employee_id, A.attendance, A.overtime_hour, A.basic_salary, A.overtime_calculation, A.bpjs_ks, A.bpjs_jht, A.tax_calculation, A.salary, A.periode_start, A.periode_end, A.salary_note, B.full_name, B.employee_number, B.bank_account, C.code AS company_code, D.name AS site_name, E.name AS position'));

		foreach ($list_payroll as $payroll) {	
			$payroll_id 	= $payroll->id;
			$income			= $this->payroll_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'pendapatan', 'columns' => 'A.name, A.value'));
			$outcome		= $this->payroll_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'potongan', 'columns' => 'A.name, A.value'));

			$start_date = substr($payroll->periode_start, 8, 2).' '.get_month(substr($payroll->periode_start, 5, 2));
			$end_date 	= substr($payroll->periode_end, 8, 2).' '.get_month(substr($payroll->periode_end, 5, 2)).' '.substr($payroll->periode_end, 0, 4);

			$kehadiran 		  = "";
			$jumlah_kehadiran = "";
			if($payroll->attendance){
				$kehadiran 			= 'JUMLAH KEHADIRAN <br>';
				$jumlah_kehadiran 	= ': '.$payroll->attendance;
			}
			$lembur 		= "";
			$jumlah_lembur 	= "";
			if($payroll->overtime_hour){
				$lembur 		= 'JUMLAH JAM LEMBUR <br>';
				$jumlah_lembur 	= $payroll->overtime_hour;
			}
			$gaji_pokok 	= "0";
			if($payroll->basic_salary){
				$gaji_pokok = $payroll->basic_salary;
			}
			$pendapatan = "";
			if($payroll->overtime_calculation){
				$pendapatan .= '<tr><td>Lembur</td><td>: Rp.</td><td style="float:right;">'.rupiah($payroll->overtime_calculation).'</td></tr>';
			}
			foreach( $income AS $item){
				if($item->name != ''){
				    $pendapatan .= "<tr><td >".$item->name." &nbsp;&nbsp;</td><td>: Rp.</td><td style='float:right;'>".rupiah($item->value)."</td></tr>";
				}
			}
			$potongan 	= "";
			$potongan .= '<tr><td>BPJS Kesehatan</td><td>: Rp.</td><td style="float:right;">'.rupiah($payroll->bpjs_ks).'</td></tr>';
			$potongan .= '<tr><td>BPJS Ketenagakerjaan</td><td>: Rp.</td><td style="float:right;">'.rupiah($payroll->bpjs_jht).'</td></tr>';
			$potongan .= '<tr><td>PPH21</td><td>: Rp.</td><td style="float:right;">'.rupiah($payroll->tax_calculation).'</td></tr>';
			
			foreach( $outcome AS $item){
				if($item->name != ''){
				    $potongan .= "<tr><td >".$item->name." &nbsp;&nbsp;</td><td>: Rp.</td><td style='float:right;'>".rupiah($item->value)."</td></tr>";
				}
			}

			$image_url = 'files/'.strtolower($payroll->company_code).'.jpg';
			$preview = '<section class="invoice">
				<div class="row">
					<div class="col-12">
						<h2 class="page-header">
							<img width="100%" src="'.base_url($image_url).'"/>
						</h2>
					</div>
				</div>
				<div class="row invoice-info m-4">
					<div class="col-sm-3 invoice-col">
						<address>
							PERIODE BULAN <br>
							NO REKENING <br>
							NAMA <br>
							NOMOR KARYAWAN <br>
							JABATAN <br>
							SITE <br>
							'.$kehadiran.'
							'.$lembur.'
						</address>
					</div>
					<div class="col-sm-6 invoice-col">
						<?php 
							
						?>
						<address>
							: '.$start_date.' - '.$end_date.'<br>
							: '.$payroll->bank_account.'<br>
							: '.$payroll->full_name.'<br>
							: '.$payroll->employee_number.'<br>
							: '.$payroll->position.'<br>
							: '.$payroll->site_name.'<br>
							  '.$jumlah_kehadiran.'<br>
							  '.$jumlah_lembur.'<br>
						</address>
					</div>
				</div>
				<div class="row invoice-info">
					<div class="col-sm-6 invoice-col">
					&nbsp;
					</div>
					<div class="col-sm-6 invoice-col">
						<address >
							<strong>Gaji bersih diterima : '.format_rupiah($payroll->salary).'</strong>
						</address>
					</div>
				</div>
				<div class="row invoice-info m-4">
					<div class="col-sm-6 invoice-col">
						<address>
							<table style="border:none">
								<tr><td><strong>Pendapatan</strong></td><td></td><td></td></tr>
								<tr><td>Gaji Pokok</td><td>: Rp.</td><td style="float:right;">'.rupiah($gaji_pokok).'</td></tr>
								'.$pendapatan.'
        					</table>
        				</address>
      				</div>
      				<div class="col-sm-6 invoice-col">
      					<address>
      						<table style="border:none">
      							<tr><td><strong>Potongan</strong></td><td></td><td></td></tr>
								'.$potongan.'
							</table>
						</address>
					</div>
				</div>
				<div class="row invoice-info m-4" style="font-size: 12px;">
					<address >
						<strong>Catatan : </strong>'.$payroll->salary_note.'
					</address>
				</div>
			</section>';

			$pdf  = '<table width="100%">';
			$pdf .= '<tr><td width="25%">PERIODE BULAN</td><td> : '.$start_date.' - '.$end_date.'</td></tr>
				<tr><td>NO REKENING</td><td> : '.$payroll->bank_account.'</td></tr>
				<tr><td>NAMA</td><td> : '.$payroll->full_name.'</td></tr>
				<tr><td>NOMOR KARYAWAN</td><td> : '.$payroll->employee_number.'</td></tr>
				<tr><td>JABATAN</td><td> : '.$payroll->position.'</td></tr>
				<tr><td>SITE</td><td> : '.$payroll->site_name.'</td></tr>';
			if($payroll->attendance){
				$pdf .= '<tr><td>JUMLAH KEHADIRAN</td><td> : '.$payroll->attendance.'</td></tr>';
			}
			if($payroll->overtime_hour){
				$pdf .= '<tr><td>JUMLAH JAM LEMBUR</td><td> : '.$payroll->overtime_hour.'</td></tr>';
			}

			$pdf .= '</table><table width="100%">';
			$pdf .= '<tr><td width="60%">&nbsp;</td><td><strong>Gaji Bersih diterima : '.format_rupiah($payroll->salary).'</strong></td></tr>';
			$pdf .= '</table><br><br><table width="100%">';
			$pdf .= '<tr><td width="50%"><table>';
			$pdf .= '<tr><td><strong>Pendapatan</strong></td><td></td><td></td></tr>';
			if($payroll->basic_salary){
				$pdf .= "<tr><td>Gaji Pokok</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->basic_salary)."</td></tr>";
			}else{
				$pdf .= "<tr><td>Gaji Pokok</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
			}
			if($payroll->overtime_calculation){
				$pdf .= "<tr><td>Lembur</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->overtime_calculation)."</td></tr>";
			}
			foreach( $income AS $item):
				if($item->name != ''){
				    $pdf .= "<tr><td >".$item->name."</td><td>: Rp.</td><td style='float:right'>".rupiah($item->value)."</td></tr>";
				}
			endforeach;
			$pdf .= '</table></td>';
			$pdf .= '<td width="50%"><table>';
			$pdf .= '<tr><td><strong>Potongan</strong></td><td></td><td></td></tr>';
			if($payroll->bpjs_ks){
				$pdf .= "<tr><td>BPJS Kesehatan</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->bpjs_ks)."</td></tr>";
			}else{
				$pdf .= "<tr><td>BPJS Kesehatan</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
			}
			if($payroll->bpjs_jht){
				$pdf .= "<tr><td>BPJS Ketenagakerjaan</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->bpjs_jht)."</td></tr>";
			}else{
				$pdf .= "<tr><td>BPJS Ketenagakerjaan</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
			}
			if($payroll->tax_calculation){
				$pdf .= "<tr><td>PPH21</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->tax_calculation)."</td></tr>";
			}else{
				$pdf .= "<tr><td>PPH21</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
				
			}
			foreach($outcome AS $item):
				if($item->name != ''){
				    $pdf .= "<tr><td >".$item->name."</td><td>: Rp.</td><td style='float:right'>".rupiah($item->value)."</td></tr>";
				}
			endforeach;
			$pdf .= '</table></td></tr>';
			$pdf .= '</table>';
			$pdf .= '</table></td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr></table>';
			$pdf .= '<table width="100%"><tr><td><strong>Catatan : </strong>'.$payroll->salary_note.'</td></tr></table><br>';
			$pdf .= '<div syle="width=100%"></hr></div><br pagebreak="true"/>';

			$this->payroll_model->save(array('id' => $payroll->id, 'preview' => $preview, 'pdf' => $pdf, 'company_code' => $payroll->company_code));
			// print_prev($preview);
		}
	}
}
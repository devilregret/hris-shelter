<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Structure extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
	}

	public function list(){
		$this->load->model(array('site_model', 'company_model', 'position_model'));
		$data['_TITLE_'] 		= 'Struktur Organisasi';
		$data['_PAGE_'] 		= 'structure/organisasi';
		$data['_MENU_PARENT_'] 	= 'account';
		$data['_MENU_'] 		= 'structure';
		$this->view($data);
	}

	public function approval(){
		$this->load->model(array('site_model', 'company_model', 'position_model'));
		$data['_TITLE_'] 		= 'Struktur Approval';
		$data['_PAGE_'] 		= 'structure/approval';
		$data['_MENU_PARENT_'] 	= 'account';
		$data['_MENU_'] 		= 'structure_approval';
		$this->view($data);
	}

	public function list_ajax(){
		$structure = array(
			'name' 		=> 'DIREKTUR UTAMA',
			'title' 	=> 'AKHMAD SETIADI',
			'children' 	=> array(
				0 => array(
					'name' 		=> 'DIREKTUR OPERASIONAL',
					'title' 	=> 'HARI WAHYUDIN',
					'children' 	=> array(
						0 => array(
							'name' 		=> '',
							'title' 	=> '',
							'children'	=> array(
								0 => array(
									'name' 		=> '',
									'title' 	=> '',
									'children'	=> array(
										0 => array(
											'name' 		=> 'MANAGER OPS SN',
											'title'		=> 'MOHAMMAD FUADI',
											'children'	=> array(
												0 => array(
													'name'		=> 'ASMEN OPS SN',
													'title'		=> 'SISWANTO',
													'children'	=> array(
														0 => array(
															'name'		=> 'SPV OPS SN',
															'title'		=> 'NANANGA. R.<br>MASHADI YUSUF<br>MARIONO<br>FRAMMUDYO HERMAWAN',
															'children'	=> array(
																0 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER OPS SN',
																			'title' => 'JATMIKO'
																		)
																	)
																)
															)
														)
													)
												)
											)
										)
									)
								)
							)
						),
						1 => array(
							'name' 		=> '',
							'title' 	=> '',
							'children'	=> array(
								0 => array(
									'name' 		=> '',
									'title' 	=> '',
									'children'	=> array(
										0 => array(
											'name' 		=> 'MANAGER OPS SNI',
											'title'		=> 'MARIN RISTANTI',
											'children'	=> array(
												0 => array(
													'name'		=> '',
													'title'		=> '',
													'children'	=> array(
														0 => array(
															'name'		=> 'SPV CLEANING',
															'title'		=> 'SUTAJI KENCANA'
														),
														1 => array(
															'name'		=> 'SPV OPS SNI',
															'title'		=> 'NURYONO HARIADI<br>M. EFENDI P.<br>KUKUH AMIRUDIN',
															'children'	=> array(
																0 => array(
																	'name'		=> 'KOORDINATOR OPS SNI',
																	'title' 	=> 'FERRY HERDIANTO',
																	'children'	=> array(
																		0 => array(
																			'name' 	=> 'RELATION OFFICER',
																			'title'	=> 'LATIFUL ARDHI<br>FUAD ROSYADI<br>YUDI EKO PRASETYO<br>ANDINA IZDIHAR PUSPITO<br>ANDREAN IHSAN HUSADA<br>HADZIQ NUR HUMAIDI<br>GUNAWAN EFENDI<br>LEY VASA NURSYAFAAT<br>FATHIMAH<br>MUH SHOLEHUDIN<br>AKHMAD FIRDAUS<br>ROSTYANTA<br>FAJAR CAHYA GUNAWAN<br>ASSYFA RINDANG CHUSNIA<br>ADE FIRMAN ZAH<br>ACHMAD SOFIRUDIN<br>PRAMUDYA CAHYA WIDODO<br>BAYU SURYADI<br>ANGGA ROYAN<br>FADLI MAHARDIYANTO<br>M. JEFFREY FAKHRUROZY<br>ANDIKA HERFIANA<br>IRMA PRIHATINI<br>ARIS CHOIRUR RAHMAN<br>RITMA EGI MAIDAH'
																		)
																	)
																)
															)
														)
													)
												)
											)
										)
									)
								)
							)
						),
						2 => array(
							'name' 		=> '',
							'title' 	=> '',
							'children'	=> array(
								0 => array(
									'name' 		=> '',
									'title' 	=> '',
									'children'	=> array(
										0 => array(
											'name' 		=> 'MANAGER SDT',
											'title'		=> 'IWAN SUJATMIKO',
											'children'	=> array(
												0 => array(
													'name'		=> 'ASMEN SDT',
													'title'		=> 'AGUS MULYONO',
													'children'	=> array(
														0 => array(
															'name'		=> 'SPV DIKLAT',
															'title'		=> 'AMIR HIZBULLOH AMIN',
															'children'	=> array(
																0 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER DIKLAT',
																			'title' => 'M. CHOIRIL APRINDA<br>M. NUR KHOLIS'
																		)
																	)
																)
															)
														),
														1 => array(
															'name'		=> 'SPV TECHNICAL SUPPORT',
															'title'		=> 'M. KHUSAINI',
															'children'	=> array(
																0 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER TS',
																			'title' => 'KHOIRUL MUTTAQIN'
																		),
																		1 => array(
																			'name'	=> 'OFFICER TS CLEANING',
																			'title' => 'MUCHAMMAD ZAINUL'
																		),
																		2 => array(
																			'name'	=> 'CONTROLLER',
																			'title' => 'AMIN TOHARI
'
																		)
																	)
																)
															)
														),
														2 => array(
															'name'		=> 'SPV HARKAM',
															'title'		=> 'IMAM BUDI UTOMO',
															'children'	=> array(
																0 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER HARKAM',
																			'title' => 'M. IKHSAN FIRDAUS'
																		)
																	)
																)
															)
														)
													)
												),
												1 => array(
													'name'		=> '',
													'title'		=> '',
													'children'	=> array(
														0 => array(
															'name'		=> '',
															'title'		=> '',
															'children'	=> array(
																0 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER CRM',
																			'title' => 'MURTININGSIH<br>DEWI KURNIASARI<br>ELVIN SETIA P. S.'
																		)
																	)
																)
															)
														)
													)
												)
											)
										)
									)
								)
							)
						),
						3 => array(
							'name' 		=> 'GM IT & HCM',
							'title' 	=> 'MIFTAKHUL ARIF',
							'children'	=> array(
								0 => array(
									'name' 		=> '',
									'title' 	=> '',
									'children'	=> array(
										0 => array(
											'name' 		=> 'MANAGER HR & GA',
											'title'		=> 'ARRY HERWINDO',
											'children'	=> array(
												0 => array(
													'name'		=> '',
													'title'		=> '',
													'children'	=> array(
														0 => array(
															'name'		=> 'SPV PERSONALIA',
															'title'		=> 'CANDRA SETYA BUANA',
															'children'	=> array(
																0 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER PERSONALIA SN',
																			'title' => 'VACANT'
																		),
																		1 => array(
																			'name'	=> 'OFFICER PERSONALIA SNI',
																			'title' => 'HIRU MANGUN INDRA JAY'
																		),
																		2 => array(
																			'name'	=> 'OFFICER BENEFIT SNI',
																			'title' => 'ANDIKA KHISNA DANI
'
																		)
																	)
																),
																1 => array(
																	'name'		=> 'KOORDINATOR PAYROLL',
																	'title'		=> 'NURUL EFENDI',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER P-ROLL & BENEFIT SN',
																			'title' => 'M. NUR BAGASKARA F.'
																		)
																	)
																)
															)
														)
													)
												),
												1 => array(
													'name'		=> '',
													'title'		=> '',
													'children'	=> array(
														0 => array(
															'name'		=> '',
															'title'		=> '',
															'children'	=> array(
																0 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'		=> 'OFFICER GA',
																			'title' 	=> 'ISFANDI SOFI’I',
																			'children'	=> array(
																				0 => array(
																					'name'		=> 'OFFICE BOY',
																					'title' 	=> 'YUSUF FIKY FENDI<br>MUKHLIS EFFENDI',
																				)
																			)
																		)
																	)
																)
															)
														)
													)
												),
												2 => array(
													'name'		=> '',
													'title'		=> '',
													'children'	=> array(
														0 => array(
															'name'		=> '',
															'title'		=> '',
															'children'	=> array(
																0 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'		=> 'OFFICER LEGAL',
																			'title' 	=> 'BUDIONO'
																		)
																	)
																)
															)
														)
													)
												)
											)
										)
									)
								),
								1 => array(
									'name' 		=> '',
									'title' 	=> '',
									'children'	=> array(
										0 => array(
											'name' 		=> '',
											'title'		=> '',
											'children'	=> array(
												0 => array(
													'name'		=> '',
													'title'		=> '',
													'children'	=> array(
														0 => array(
															'name'		=> '',
															'title'		=> '',
															'children'	=> array(
																0 => array(
																	'name'		=> 'KOORDINATOR IT SUPPORT',
																	'title'		=> 'AHMAD ROBBAHUL BARRA',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER PENGOLAHAN DATA',
																			'title' => 'AGUNG PURWANTO'
																		)
																	)
																)
															)
														)
													)
												)
											)
										)
									)
								)
							)
						),
						4 => array(
							'name' 		=> 'GM FINANCE & ACT',
							'title' 	=> 'ALIVIAN LAZUARDI',
							'children'	=> array(
								0 => array(
									'name' 		=> '',
									'title' 	=> '',
									'children'	=> array(
										0 => array(
											'name' 		=> 'MANAGER FINANCE',
											'title'		=> 'FIFIT NOFIH',
											'children'	=> array(
												0 => array(
													'name'		=> '',
													'title'		=> '',
													'children'	=> array(
														0 => array(
															'name'		=> 'SPV FINANCE',
															'title'		=> 'NOVARIA INDAH SUKANTO',
															'children'	=> array(
																0 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER AR',
																			'title' => 'MAULUDIN FADIL FADILAH'
																		)
																	)
																),
																1 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER INVOICING',
																			'title' => 'MARETTA DWI P.'
																		)
																	)
																),
																2 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER LOGISTIC',
																			'title' => 'LAKSAMANA RAMADHAN'
																		)
																	)
																)
															)
														)
													)
												)
											)
										)
									)
								),
								1 => array(
									'name' 		=> '',
									'title' 	=> '',
									'children'	=> array(
										0 => array(
											'name' 		=> 'MANAGER ACCOUNTING',
											'title'		=> 'SAIFUDDIN ZUHRI',
											'children'	=> array(
												0 => array(
													'name'		=> 'ASMEN ACCOUNTING',
													'title'		=> 'M. SYAIFUL ROZIKIN',
													'children'	=> array(
														0 => array(
															'name'		=> '',
															'title'		=> '',
															'children'	=> array(
																0 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER PAJAK',
																			'title' => 'WINDIYA CHARISMA SARI'
																		)
																	)
																)
															)
														),
														1 => array(
															'name'		=> '',
															'title'		=> '',
															'children'	=> array(
																0 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER ACCOUNTING SN',
																			'title' => 'NUR AINIA WIDIASARI'
																		)
																	)
																)
															)
														),
														2 => array(
															'name'		=> '',
															'title'		=> '',
															'children'	=> array(
																0 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER ACCOUNTING SNI',
																			'title' => 'ARISSANDY'
																		)
																	)
																)
															)
														)
													)
												)
											)
										)
									)
								)
							)
						),
						5 => array(
							'name' 		=> '',
							'title' 	=> '',
							'children'	=> array(
								0 => array(
									'name' 		=> 'BRANCH MANAGER EAST',
									'title' 	=> 'IWAN SUJATMIKO',
									'children'	=> array(
										0 => array(
											'name' 		=> 'MANAGER SALES',
											'title'		=> 'ASMUNIR',
											'children'	=> array(
												0 => array(
													'name'		=> '',
													'title'		=> '',
													'children'	=> array(
														0 => array(
															'name'		=> 'SPV SALES EAST',
															'title'		=> 'RYAN RACHMANTO<br>NATALIA DWI SHASANTI',
															'children'	=> array(
																0 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'SALES EAST',
																			'title' => 'M. NURHADI KURNIAWAN<br>WARIH PRABOWO<br>WURI ARIYANTI<br>ACHMAD FIRMANTO<br>ZAMALUDDIN'
																		)
																	)
																),
																1 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'TELE SALES',
																			'title' => 'DEWI AMELIA'
																		)
																	)
																)
															)
														)
													)
												)
											)
										)
									)
								)
							)
						),
						6 => array(
							'name' 		=> '',
							'title' 	=> '',
							'children'	=> array(
								0 => array(
									'name' 		=> 'BRANCH MANAGER CENTRAL',
									'title' 	=> 'THERESIA PRAMESTI W.',
									'children'	=> array(
										0 => array(
											'name' 		=> '',
											'title'		=> '',
											'children'	=> array(
												0 => array(
													'name'		=> 'ASMEN SALES CENTRAL',
													'title'		=> 'DEDI IRAWAN',
													'children'	=> array(
														0 => array(
															'name'		=> 'SPV BRANCH CENTRAL',
															'title'		=> 'SUJARWADI<br>CATUR DONI J. W.<br>EDI BINTORO',
															'children'	=> array(
																0 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER BRANCH CENTRAL',
																			'title' => 'RIZKA AURELIA<br>NUR IMAMAH HAWA'
																		)
																	)
																),
																1 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'SALES CENTRAL',
																			'title' => 'MARATUN NASHRIYYAH<br>FENTY HALIM<br>DEVI KURNIAWATI'
																		)
																	)
																),
																2 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'CRM CENTRAL',
																			'title' => 'CHARA KHARISMA'
																		)
																	)
																)
															)
														)
													)
												)
											)
										)
									)
								)
							)
						),
						7 => array(
							'name' 		=> '',
							'title' 	=> '',
							'children'	=> array(
								0 => array(
									'name' 		=> 'BRANCH MANAGER WEST',
									'title' 	=> 'UWI SAFRUDIN',
									'children'	=> array(
										0 => array(
											'name' 		=> 'MGR SALES BRANCH WEST',
											'title'		=> 'SURYO ARIE SETYAWAN',
											'children'	=> array(
												0 => array(
													'name'		=> '',
													'title'		=> '',
													'children'	=> array(
														0 => array(
															'name'		=> 'SPV BRANCH WEST',
															'title'		=> 'ERI KUSUMAH',
															'children'	=> array(
																0 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'OFFICER BRANCH WEST',
																			'title' => 'YANTO<br>SISWANTO<br>EKA PAMUNGKAS<br>NOVA AGUS KUSTIANA<br>RAMADHANY<br>YANU SETIANINGSIH<br>HARIANTI<br>AULIA DAIE NICHEN<br>MUHAMMAD ABDUL AZIS<br>HADI PERMANA<br>SITI NURPADILAH<br>ARIF JAMALUDIN<br>SAPTO HERMAWAN<br>YUDI SETIAWAN<br>ALVIN KURNIAWAN'
																		)
																	)
																),
																1 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'SALES WEST',
																			'title' => 'RIA SARI OKTAVIANI<br>HENDI GUNAWAN'
																		)
																	)
																),
																2 => array(
																	'name'		=> '',
																	'title'		=> '',
																	'children'	=> array(
																		0 => array(
																			'name'	=> 'CRM WEST',
																			'title' => 'SHILVIA AULIA DEWI'
																		)
																	)
																)
															)
														)
													)
												)
											)
										)
									)
								)
							)
						),
						8 => array(
							'name' 		=> '',
							'title' 	=> '',
							'children'	=> array(
								0 => array(
									'name' 		=> '',
									'title' 	=> 'MANAGER',
									'children'	=> array(
										0 => array(
											'name' 		=> 'MANAGER',
											'title'		=> 'FRANS HIDAYAT'
										)
									)
								)
							)
						)
					)
				),
				1 => array(
					'name' 		=> 'DIREKTUR SALES',
					'title' 	=> 'M. NINO MAYVI DIAN',
					'children' 	=> array(
						0 => array(
							'name' 		=> '',
							'title' 	=> '',
							'children'	=> array(						
								0 => array(
									'name' 		=> '',
									'title' 	=> '',
									'children'	=> array(						
										0 => array(
											'name' 		=> 'MANAGER MARCOMM',
											'title' 	=> 'ISNAN FADEL',
											'children' 	=> array(
												0 => array(
													'name' 		=> 'OFFICER MARCOMM',
													'title' 	=> 'ICHSAN EFENDI P. R.'
												)
											)
										)
									)
								)
							)
						)
					)
				)
			)
		);

		// foreach()
		echo json_encode($structure);
	}

	public function getChild($parent_id = 0){
		$this->load->model(array('struktur_jabatan_model'));
	
		$params['parent_id'] 		= $parent_id;
		$list 	= $this->struktur_jabatan_model->getStrukturByParent($params);
		$data 	= array();
		foreach($list as $item)
		{
			$result['name'] 		= $item->name;
			$result['title'] 		= $item->full_name;
			if($item->children > 0){
				$result['children'] = $this->getChild($item->id);
			}else{
				$result['children'] = array();
			}

			array_push($data, $result);
		}
		return $data;
	}
	
	public function list_approval(){
		$this->load->model(array('struktur_jabatan_model'));
	
		$params['parent_id'] 		= 0;
		$list 	= $this->struktur_jabatan_model->getStrukturByParent($params);
		$data 	= array();
		foreach($list as $item)
		{
			$result['name'] 		= $item->name;
			$result['title'] 		= $item->full_name;
			if($item->children > 0){
				$result['children'] = $this->getChild($item->id);
			}else{
				$result['children'] = array();
			}

			// array_push($data, $result);
		}
		echo json_encode($result);
	}

	public function list_organisasi(){
		$this->load->model(array('struktur_organisasi_model'));
	
		$params['parent_id'] 		= 0;
		$list 	= $this->struktur_organisasi_model->getStrukturByParent($params);
		$data 	= array();
		foreach($list as $item)
		{
			$result['name'] 		= $item->position_name;
			$result['title'] 		= $item->full_name;
			if($item->children > 0){
				$result['children'] = $this->getChildOrganisasi($item->id);
			}else{
				$result['children'] = array();
			}

			// array_push($data, $result);
		}
		echo json_encode($result);
	}

	public function getChildOrganisasi($parent_id = 0){
		$this->load->model(array('struktur_organisasi_model'));
	
		$params['parent_id'] 		= $parent_id;
		$list 	= $this->struktur_organisasi_model->getStrukturByParent($params);
		$data 	= array();
		foreach($list as $item)
		{
			$result['name'] 		= $item->position_name;
			$result['title'] 		= $item->full_name;
			if($item->children > 0){
				$result['children'] = $this->getChildOrganisasi($item->id);
			}else{
				$result['children'] = array();
			}

			array_push($data, $result);
		}
		return $data;
	}
}
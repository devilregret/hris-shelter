<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}
	public function apply($function = FALSE){
		$this->session->set_flashdata('message', message_box('Silahkan Login terlebih dahulu, jika belum memiliki akun silahkan daftar terlebih dahulu <a href="'.base_url("auth/register").'"> DAFTAR DISINI </a>.','danger'));
		redirect(base_url('auth/login'));
	}

	public function register()
	{

		$this->load->model(array('auth_model', 'information_model', 'vacancy_model', 'site_model', 'city_model', 'province_model', 'preference_model', 'site_model', 'candidate_model', 'email_model', 'province_model'));

		$data['full_name']			= '';
		$data['id_card']			= '';
		$data['username'] 			= '';
		$data['email'] 				= '';
		$data['password'] 			= '';
		$data['repassword'] 		= '';
		$data['phone_number'] 		= '';
		$data['gender'] 			= '';
		$data['date_birth'] 		= '';
		$data['education_level'] 	= '';
		$data['education_majors'] 	= '';
		$data['experience'] 		= '';
		$data['destination_city'] 	= '';
		$data['preference_job1'] 	= '';
		$data['site_preference'] 	= '';
		$data['city_domisili_id'] 	= '';
		$data['recommendation'] 	= '';

		if($this->input->post()){

			$data['password'] 			= $this->input->post('password');
			$data['repassword'] 		= $this->input->post('repassword');
			
			$params['full_name'] 		= $data['full_name'] 		= $this->input->post('full_name');
			$params['id_card'] 			= $data['id_card'] 			= $this->input->post('id_card');
			$params['username'] 		= $data['username'] 		= $this->input->post('id_card');
			$params['email']			= $data['email']			= $this->input->post('email');
			$params['phone_number']		= $data['phone_number']		= $this->input->post('phone_number');
			$params['gender']			= $data['gender'] 			= $this->input->post('gender');
			$params['date_birth'] 		= $data['date_birth'] 		= $this->input->post('date_birth');
			$params['education_level'] 	= $data['education_level'] 	= $this->input->post('education_level');
			$params['education_majors'] = $data['education_majors'] = $this->input->post('education_majors');
			$params['experience'] 		= $data['experience'] 		= $this->input->post('experience');
			$params['destination_city'] = $data['destination_city'] = $this->input->post('destination_city');
			$params['preference_job1'] = $data['preference_job1'] 	= $this->input->post('preference_job1');
			$params['site_preference'] 	= $data['site_preference'] 	= $this->input->post('site_preference');
			$params['city_domisili_id']	= $data['city_domisili_id']	= $this->input->post('city_domisili_id');
			$params['recommendation']	= $data['recommendation']	= $this->input->post('recommendation');
			$params['role_id']			= 1;
			
			$this->form_validation->set_rules('id_card', 'NIK', 'required');
			$this->form_validation->set_rules('id_card', 'Nomor KTP', 'required|numeric|min_length[16]|max_length[17]');

			if ($this->form_validation->run() == FALSE){
				$data['warning'] = message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger');
			}else{
				$params['password']			= generate_password($data['password']);
				$params['full_name'] 		= $data['full_name'] 		= $this->input->post('full_name');
				$params['id_card'] 			= $data['id_card'] 			= $this->input->post('id_card');
				$params['username'] 		= $data['username'] 		= $this->input->post('id_card');
				$params['email']			= $data['email']			= $this->input->post('email');
				$params['phone_number']		= $data['phone_number']		= $this->input->post('phone_number');
				$params['gender']			= $data['gender'] 			= $this->input->post('gender');
				$params['date_birth'] 		= $data['date_birth'] 		= $this->input->post('date_birth');
				$params['education_level'] 	= $data['education_level'] 	= $this->input->post('education_level');
				$params['education_majors'] = $data['education_majors'] = $this->input->post('education_majors');
				$params['experience'] 		= $data['experience'] 		= $this->input->post('experience');
				$params['destination_city'] = $data['destination_city'] = $this->input->post('destination_city');
				$params['preference_job1'] = $data['preference_job1'] 	= $this->input->post('preference_job1');
				$params['site_id'] 			= $data['site_id'] 			= $this->input->post('site_id');
				$params['city_domisili_id']	= $data['city_domisili_id']	= $this->input->post('city_domisili_id');
				$params['recommendation']	= $data['recommendation']	= $this->input->post('recommendation');
					
				$employee['full_name'] 				= $params['full_name'];
				$employee['id_card']				= $params['id_card'];
				$employee['email']					= $params['email'];
				$employee['phone_number']			= $params['phone_number'];
				$employee['gender']					= $params['gender'];
				$employee['date_birth']				= date("Y-m-d", strtotime(str_replace('/', '-', $params['date_birth'])));
				$employee['education_level']		= $params['education_level'];
				$employee['education_majors']		= $params['education_majors'];
				$employee['experience']				= $params['experience'];
				$employee['preference_placement']	= $params['destination_city'];
				$employee['preference_job1']		= $params['preference_job1'];
				$employee['site_preference']		= $params['site_preference'];
					// $employee['site_id']				= $params['site_id'];
				$employee['city_domisili_id']		= $params['city_domisili_id'];
				$employee['recommendation']			= $params['recommendation'];
				$employee['id']						= '';
				$employee['status']					= 0;
				$employee['status_approval']		= 0;
				$employee['status_completeness']	= 0;

				$employee['branch_id']				= 2;
				$preference = $this->city_model->get(array('id' => $employee['preference_placement'], 'columns' => 'A.province_id'));
				if($preference){
					$branch = $this->province_model->get(array('id' => $preference->province_id, 'columns' => 'A.branch_id'));
						$employee['branch_id'] = $branch->branch_id;
				}

				$exist = $this->auth_model->validate_employee(array('id_card' => $data['id_card'], 'columns' => 'A.id, A.employee_id, A.email, B.is_active, A.username, B.id_card' ));
				if($exist){
					if($exist->is_active == 0){
						
						$employee['id']			= $exist->employee_id;
						$user['id'] 			= $exist->id;
						$user['employee_id'] 	= $this->candidate_model->save($employee);
						$user['username'] 		= $params['username'];
						$user['full_name'] 		= $params['full_name'];
						$user['password'] 		= $params['password'];
						$user['branch_id'] 		= 2;
						$user['role_id']		= 1;

						$save_id = $this->auth_model->save($user);
						if ($save_id) {
							$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Akun berhasil terdaftar.<br>Silahkan login kembali pada akun anda untuk melengkapi berkas lamaran, terima kasih.','success'));

							$phone_number = explode ("/", $employee['phone_number']);
							$phone_number = preg_replace("/[^0-9]/", "", $phone_number[0]);
							$start = substr($phone_number, 0, 1 );
							if($start == '0'){
								$phone_number = '62'.substr($phone_number, 1,strlen($phone_number));
							}else if($start == '+'){
								$phone_number = substr($phone_number, 1,strlen($phone_number));
							}
							// $this->send_message_register(array('phone_number' => $phone_number, 'full_name' => $params['full_name'], 'username' => $params['username']));
							redirect(base_url('auth/login'));
						}else{
							$data['message'] = message_box('<strong>Gagal!</strong> data gagal disimpan.','danger');
						}
					}else{
						if($data['email'] == $exist->email && $data['email'] != ''){
							$data['warning'] = message_box('<strong>Gagal!</strong> email yang anda masukkan sudah terdaftar.','danger');
						}else if($data['id_card'] == $exist->id_card){
							$data['warning'] = message_box('<strong>Gagal!</strong> Nomor KTP yang anda masukkan sudah terdaftar.','danger');	
						}
					}
				}else{
					$user['employee_id'] 	= $this->candidate_model->save($employee);
					$user['username'] 		= $params['username'];
					$user['full_name'] 		= $params['full_name'];
					$user['password'] 		= $params['password'];
					$user['branch_id'] 		= 2;
					$user['role_id']		= 1;

					$save_id = $this->auth_model->save($user);
					if ($save_id) {
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Akun berhasil terdaftar.<br>Silahkan login kembali pada akun anda untuk melengkapi berkas lamaran, terima kasih.','success'));

						$phone_number = explode ("/", $employee['phone_number']);
						$phone_number = preg_replace("/[^0-9]/", "", $phone_number[0]);
						$start = substr($phone_number, 0, 1 );
						if($start == '0'){
							$phone_number = '62'.substr($phone_number, 1,strlen($phone_number));
						}else if($start == '+'){
							$phone_number = substr($phone_number, 1,strlen($phone_number));
						}
						// $this->send_message_register(array('phone_number' => $phone_number, 'full_name' => $params['full_name'], 'username' => $params['username']));
						redirect(base_url('auth/login'));
					}else{
						$data['message'] = message_box('<strong>Gagal!</strong> data gagal disimpan.','danger');
					}
				}
			}
		}

		$data['province_id']	= '';

		if($this->input->get()){
			$vacancy['province_id'] = $data['province_id']	= $this->input->get('province');
		}
		
		$vacancy['active'] 		= $information['active']		= true;
		$information['columns']	= 'A.id, A.title, A.content, B.name AS branch_name';
		$vacancy['columns'] 	= 'A.id, A.title, A.content, B.name AS province_name, C.name AS branch_name, DATE_FORMAT(A.end_date, "%W") AS day, DATE_FORMAT(A.end_date, "%d %m %Y") AS date, A.flyer';

		$data['information']	= $this->information_model->gets($information);
		$data['vacancy']		= $this->vacancy_model->gets($vacancy);
		$data['list_province']	= $this->province_model->gets(array('columns' => 'A.id, A.name', 'order' => 'ASC', 'orderby' => 'A.name'));
		$data['list_city'] 		= $this->city_model->list(array('columns' => 'A.id, A.name', 'order' => 'ASC', 'orderby' => 'A.name'));
		$data['list_job'] 		= $this->preference_model->gets(array('columns' => 'A.id, A.name', 'order' => 'ASC', 'orderby' => 'A.name'));

		$data['_TITLE_']		= 'LOKER & KARIR - SHELTER';
		$data['_MENU_']			= 'auth/register';
		$this->load->view('auth/register', $data);
	}

	public function reset()
	{	

		$this->load->model(array('auth_model', 'user_model', 'information_model', 'vacancy_model', 'information_model', 'province_model'));

		$data['phone_number'] 		= '';
		$data['id_card'] 		= '';

		if($this->input->post()){
			$this->load->model(array('wappin_model'));
			$config_id = 1;
			$wappin 		= $this->wappin_model->get(array('id' => $config_id, 'columns' => 'A.access_token, A.base_url'));
			if(!$wappin){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong>  Notifikasi Whatsapp sedang dalam perbaikan.','danger'));
				redirect(base_url('auth/login'));
				die();
			}else{
				$data['phone_number']	= $this->input->post('phone_number');
				$data['id_card']		= $this->input->post('id_card');
				// $exist 			        = $this->auth_model->check(array('phone_number' => $data['phone_number'], 'id_card' => $data['id_card'], 'columns' => 'A.id, A.full_name, A.username'));
				$exist 			        = $this->auth_model->check(array('id_card' => $data['id_card'], 'columns' => 'A.id, A.full_name, A.username'));
			
				if($exist){
					$permitted_chars 	= '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
					// $generated 			= substr(str_shuffle($permitted_chars), 0, 15);

					$generated 			= $data['id_card'];

					$phone_number = explode ("/", $data['phone_number']);
					$phone_number = preg_replace("/[^0-9]/", "", $phone_number[0]);
					$start = substr($phone_number, 0, 1 );
					if($start == '0'){
						$phone_number = '62'.substr($phone_number, 1,strlen($phone_number));
					}else if($start == '+'){
						$phone_number = substr($phone_number, 1,strlen($phone_number));
					}

					$send = $this->send_message_reset(array('phone_number' => $phone_number, 'full_name' => $exist->full_name, 'password' => $generated, 'token' => "Authorization: Bearer ".$wappin->access_token, 'base_url' => $wappin->base_url));
					if($send){
					    $user['id'] 	        = $exist->id;
        				$user['password'] 		= generate_password($generated);
        				$save_id = $this->auth_model->save($user);

					    $this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> kata sandi baru, telah dikirim ke nomor whatsapp anda.','success'));
					}else{
		    			$data['warning'] 	= message_box('<strong>Gagal!</strong> Maaf, Terjadi kesalahan saat pengiriman.','danger');
					}
				}else if ($this->form_validation->run() == FALSE){
					$data['warning'] 	= message_box('<strong>Gagal!</strong> Maaf, nomor whatsapp atau NIK yang anda masukkan tidak ditemukan.','danger');
				}
				redirect(base_url('auth/login'));
			}
		}

		$data['province_id']	= '';

		if($this->input->get()){
			$vacancy['province_id'] = $data['province_id']	= $this->input->get('province');
		}
		

		$vacancy['active'] 		= $information['active']		= true;
		$information['columns']	= 'A.id, A.title, A.content, B.name AS branch_name';
		$vacancy['columns'] 	= 'A.id, A.title, A.content, B.name AS province_name, C.name AS branch_name, DATE_FORMAT(A.end_date, "%W") AS day, DATE_FORMAT(A.end_date, "%d %m %Y") AS date, A.flyer';

		$data['information']	= $this->information_model->gets($information);
		$data['vacancy']		= $this->vacancy_model->gets($vacancy);
		$data['list_province']	= $this->province_model->gets(array('columns' => 'A.id, A.name'));

		$data['_TITLE_']	= 'LOKER & KARIR - SHELTER';
		$data['_MENU_']		= 'auth/reset'; 
		$this->load->view('auth/reset', $data);
	}

	public function login()
	{	
		$this->load->model(array('auth_model', 'candidate_model', 'information_model', 'vacancy_model', 'province_model'));
		$data['username'] 	= '';

		if($this->input->post()){
			$data['username']	= $this->input->post('username');
			$password			= $this->input->post('password');
			$exist 				= $this->auth_model->get(array('username' => $data['username']));
			if($exist){
				$valid 			= $this->auth_model->get(array('username' => $data['username'], 'password' => generate_password($password), 'columns' => 'A.id, A.employee_id, A.role_id, B.id_card AS username, B.email AS email, B.branch_id, B.site_id, B.position_id, B.full_name, B.phone_number, B.status_approval'));
				if($valid){
					$this->session->set_userdata('user_id',$valid->id);
					$this->session->set_userdata('employee_id',$valid->employee_id);
					$this->session->set_userdata('username',$valid->username);
					$this->session->set_userdata('email',$valid->email);
					$this->session->set_userdata('name',$valid->full_name);
					$this->session->set_userdata('role',$valid->role_id);
					$this->session->set_userdata('branch',$valid->branch_id);
					$this->session->set_userdata('is_login',TRUE);
					$this->session->set_userdata('client_site',$valid->site_id);
					// $this->session->set_userdata('struktur_id',$valid->struktur_id);
					$this->session->set_userdata('full_name',$valid->full_name);
					$this->session->set_userdata('phone_number',$valid->phone_number);

					$is_employee = FALSE;
					$site_id 	 = 0;
					$position_id = 0;
					// $candidate = $this->candidate_model->get(array('id' => $valid->employee_id, 'columns' => 'A.id, A.status_approval, A.site_id, A.position_id'));
					$this->auth_model->save(array('is_active' => 1, 'id' => $exist->id));
					if ($valid->employee_id){
						$this->candidate_model->save(array('is_active' => 1, 'id' => $valid->employee_id));
						if($valid->status_approval > 2){
							$is_employee =TRUE;
							$site_id = $valid->site_id;
							$position_id = $valid->position_id;
						}
					}

					$this->session->set_userdata('site_id',$site_id);
					$this->session->set_userdata('position_id',$position_id);
					$this->session->set_userdata('is_employee',$is_employee);
					redirect(base_url());
				}else{
					$data['warning']= message_box('<strong>Gagal!</strong> password yang anda masukkan salah.','danger');
				}
			}else{
				$data['warning'] 	= message_box('<strong>Gagal!</strong> NIK belum terdaftar.','danger');
			}
		}
		$data['province_id']	= '';

		if($this->input->get()){
			$vacancy['province_id'] = $data['province_id']	= $this->input->get('province');
		}
		

		$vacancy['active'] 		= $information['active']		= true;
		$information['columns']	= 'A.id, A.title, A.content, B.name AS branch_name';
		$vacancy['columns'] 	= 'A.id, A.title, A.content, B.name AS province_name, C.name AS branch_name, DATE_FORMAT(A.end_date, "%W") AS day, DATE_FORMAT(A.end_date, "%d %m %Y") AS date, A.flyer';

		$data['information']	= $this->information_model->gets($information);
		$data['vacancy']		= $this->vacancy_model->gets($vacancy);
		$data['list_province']	= $this->province_model->gets(array('columns' => 'A.id, A.name, B.name AS province_name'));
		$data['_TITLE_']	= 'LOKER & KARIR - SHELTER';
		$data['_MENU_']		= 'login'; 
		$this->load->view('auth/login', $data);
	}

	public function logout()
	{
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('employee_id');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('role');
		$this->session->unset_userdata('branch');
		$this->session->unset_userdata('is_login');
		$this->session->unset_userdata('is_employee');
		$this->session->unset_userdata('site');
		redirect('auth/login');
	}

	
	public function form_courier()
	{
		$this->load->model(array('city_model', 'candidate_temporary_model'));
		
		$data['id']					= '';
		$data['id_card']			= '';
		$data['full_name']			= '';
		$data['phone_number'] 		= '';
		$data['address']			= '';
		$data['city_id'] 			= '';
		$data['driver_license'] 	= '';
		$data['type'] 				= 'Kurir';
		$data['followup_date'] 		= date('Y-m-d H:i:s');

		if($this->input->post()){
			$this->form_validation->set_rules('full_name', 'Nama Lengkap', 'required');
			if ($this->form_validation->run() == FALSE){
				$data['message'] = message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger');
			}else{

				$data['id_card']		= $this->input->post('id_card');
				$data['full_name']		= $this->input->post('full_name');
				$data['phone_number'] 	= $this->input->post('phone_number');
				$data['address']		= $this->input->post('address');
				$data['city_id'] 		= $this->input->post('city_id');
				$data['driver_license'] = $this->input->post('driver_license');
				
				$result = $this->candidate_temporary_model->get(array('id_card' => $data['id_card']));
				if($result){
					$data['message']	= message_box('<strong>Gagal!</strong> Nomor KTP '.$data['id_card'].' sudah terdaftar.','danger');
				}else{
					$this->candidate_temporary_model->save($data);
					$data['message']	= message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success');
				}
			}
		}

		$data['list_city']	= $this->city_model->gets(array('columns' => 'A.id, A.name', 'order' => 'ASC', 'orderby' => 'A.name'));
		$data['_TITLE_']	= 'Form Pendaftaran Kurir';
		$data['_MENU_']		= 'login'; 
		$this->load->view('auth/form_courier', $data);
	}

	public function form_casual()
	{
		$this->load->model(array('city_model', 'candidate_temporary_model'));
		
		$data['id']					= '';
		$data['id_card']			= '';
		$data['full_name']			= '';
		$data['phone_number'] 		= '';
		$data['address']			= '';
		$data['city_id'] 			= '';
		$data['vaccine_covid'] 		= '';
		$data['type'] 				= 'Casual';
		$data['followup_date'] 		= date('Y-m-d H:i:s');

		if($this->input->post()){
			$this->form_validation->set_rules('full_name', 'Nama Lengkap', 'required');
			if ($this->form_validation->run() == FALSE){
				$data['message'] = message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger');
			}else{

				$data['id_card']		= $this->input->post('id_card');
				$data['full_name']		= $this->input->post('full_name');
				$data['phone_number'] 	= $this->input->post('phone_number');
				$data['address']		= $this->input->post('address');
				$data['city_id'] 		= $this->input->post('city_id');
				$data['vaccine_covid'] 	= $this->input->post('vaccine_covid');
				
				$result = $this->candidate_temporary_model->get(array('id_card' => $data['id_card']));
				if($result){
					$data['message']	= message_box('<strong>Gagal!</strong> Nomor KTP '.$data['id_card'].' sudah terdaftar.','danger');
				}else{
					$this->candidate_temporary_model->save($data);
					$data['message']	= message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success');
				}
			}
		}

		$data['list_city']	= $this->city_model->gets(array('columns' => 'A.id, A.name', 'order' => 'ASC', 'orderby' => 'A.name'));
		$data['_TITLE_']	= 'Form Pendaftaran Casual';
		$data['_MENU_']		= 'login'; 
		$this->load->view('auth/form_casual', $data);
	}
	
	private function send_message_register($data = array()){

		$this->load->model(array('wappin_model'));
		$config_id = 1;
		$wappin 		= $this->wappin_model->get(array('id' => $config_id, 'columns' => 'A.access_token, A.base_url'));
		if($wappin){
			$url 		= $wappin->base_url.'/v1/messages';
			$request 	= array(
				'to' => $data['phone_number'],
				'type'	=> 'template',
				'template' => array(
					'name'	=> $this->config->item('template_registration_name'),
					'namespace' => $this->config->item('template_registration_namespace'),
					'language'	=> array(
						'policy' => 'deterministic',
						'code'	=> 'id'
					),
					'components' => array(
						array(
							'type' => 'body',
							'parameters'=> array(
								array(
									'type' => 'text',
									'text'	=> $data['full_name']
								),
								array(
									'type' => 'text',
									'text'	=> $data['username']
								)
							)
						)
					)
				),
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $data['token']));
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
			$response = curl_exec($ch);
			$result = json_decode($response, true);
			$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			if($httpcode == 200){
				return true;
			}else{
				return false;
			}
		}
	}
	
	private function send_message_reset($data = array()){
		$url 		= $data['base_url'].'/v1/messages';
		$request 	= array(
			'to' => $data['phone_number'],
			'type'	=> 'template',
			'template' => array(
				'name'	=> $this->config->item('reset_password_name'),
				'namespace' => $this->config->item('reset_password_namespace'),
				'language'	=> array(
					'policy' => 'deterministic',
					'code'	=> 'id'
				),
				'components' => array(
					array(
						'type' => 'body',
						'parameters'=> array(
							array(
								'type' => 'text',
								'text'	=> $data['full_name']
							),
							array(
								'type' => 'text',
								'text'	=> $data['password']
							)
						)
					)
				)
			),
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $data['token']));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
		$response = curl_exec($ch);
		$result = json_decode($response, true);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if($httpcode == 200){
			return true;
		}else{
			return false;
		}
	}
}
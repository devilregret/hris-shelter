<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rosecuritypayroll extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer_security'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function list($view = 'last', $config_id = FALSE){

		$this->load->model(array('payroll_config_model', 'payroll_model', 'employee_model'));
		$data['_TITLE_'] 		= 'Payroll Karyawan';
		$data['_PAGE_'] 		= 'rosecuritypayroll/list';
		$data['_MENU_PARENT_'] 	= 'ro_security';
		$data['_MENU_'] 		= 'ro_security_payroll';
		$data['list_config']	= $this->payroll_config_model->gets(array('site_id' => 1, 'columns' => 'A.id, A.config_name'));
		$data['view']			= $view;
		
		$config 				= array('columns' => 'A.id, A.name, A.address, B.id AS config_id, B.config_name, B.site_id, B.row_title, B.row_start, B.row_end, B.column_employee, B.column_income, B.column_outcome, B.column_net, B.column_attendance, B.column_basic_salary, B.column_overtime_hour, B.column_overtime_calculation, B.column_bpjs_jht, B.column_bpjs_jp, B.column_bpjs_ks, B.column_tax_calculation, B.column_note, B.column_bpjs_ks_company, B.column_bpjs_jp_company, B.column_bpjs_jht_company');
		
		if($config_id){
			if($config_id == 'new'){
				$config['site_id'] = 1;
			}else{
				$config['id']	= $config_id;
			}
		}else{
			$config['site_id']	= 1;
		}
		$data['site']	= $this->payroll_config_model->get($config);
		$this->view($data);
	}

	public function list_ajax($view = FALSE){

		$this->load->model(array('payroll_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, B.full_name AS employee_name, B.phone_number, C.name AS site_name, D.name AS position_name, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.bpjs_ks_company, A.bpjs_jht_company, A.salary, A.payment, A.email, A.note";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['position_type']= 'Security';

		$params['employee_name']	= $_POST['columns'][2]['search']['value'];
		$params['phone_number']		= $_POST['columns'][3]['search']['value'];
		$params['site_name']		= $_POST['columns'][4]['search']['value'];
		$params['position_name']	= $_POST['columns'][5]['search']['value'];
		$params['periode_start']	= $_POST['columns'][6]['search']['value'];
		$params['periode_end']		= $_POST['columns'][7]['search']['value'];
		$params['bpjs_ks_company']	= $_POST['columns'][8]['search']['value'];
		$params['bpjs_jht_company']	= $_POST['columns'][9]['search']['value'];
		$params['salary']			= $_POST['columns'][10]['search']['value'];
		$params['payment']			= $_POST['columns'][11]['search']['value'];
		$params['email']			= $_POST['columns'][12]['search']['value'];
		$params['note']				= $_POST['columns'][13]['search']['value'];
		
		// $params['site_id']			= $this->session->userdata('site');
		if($view == 'last'){
			$params['in_payment'] 	= array('Menunggu', 'Draft', 'Ditolak');
		}
		if($this->session->userdata('branch') > 1){
			$params['branch_id'] 	= $this->session->userdata('branch');
		}

		$list 	= $this->payroll_model->gets($params);
		$total 	= $this->payroll_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 				= $i;
			$result['employee_name']	= $item->employee_name;
			$result['phone_number']		= $item->phone_number;
			$result['site_name']		= $item->site_name;
			$result['position_name']	= $item->position_name;
			$result['periode_start']	= $item->periode_start;
			$result['periode_end']		= $item->periode_end;
			$result['bpjs_ks_company']	= format_rupiah((double) $item->bpjs_ks_company);
			$result['bpjs_jht_company']	= format_rupiah((double) $item->bpjs_jht_company);
			$result['salary']			= format_rupiah((double) $item->salary);
			$result['note']				= $item->note;
			$result['payment']			= '<p class="text-danger">'.$item->payment.'</p>';
			if($item->payment ==  'Selesai'){
				$result['payment']		= '<p class="text-success">'.$item->payment.'</p>';
				$result['id']			= "";
			}
			if($item->payment ==  'Menunggu'){
				$result['payment']		= '<p class="text-warning">'.$item->payment.'</p>';
			}

			$result['email']			= '<p class="text-warning">'.$item->email.'</p>';
			if($item->email ==  'Terkirim'){
				$result['email']		= '<p class="text-success">'.$item->email.'</p>';
			}else if($item->email == 'Gagal'){
				$result['email']		= '<p class="text-danger">'.$item->email.'</p>';
			}
			
			$result['action'] 			=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("rosecuritypayroll/preview/".$item->id).'">Lihat</a>
				<a href="'.base_url('rosecuritypayroll/pdf/' . $item->id).'" style="cursor:pointer;" class="btn-sm btn-warning btn-block" target="_blank">PDF</a>';
			if($item->payment == 'Draft'){
				$result['action'] .= 
				'<a href="'.base_url('rosecuritypayroll/form/' . $item->id).'" style="cursor:pointer;" class="btn-sm btn-success btn-block">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("rosecuritypayroll/delete/".$item->id).'">Hapus</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}

	public function form($payroll_id = FALSE)
	{
		$this->load->model(array('payroll_model', 'employee_model'));

		if($this->input->post()){
			$data['id'] 					= $this->input->post('payroll_id');
			$data['salary']					= $this->input->post('salary');
			$data['overtime_hour']			= $this->input->post('overtime_hour');
			$data['overtime_calculation']	= $this->input->post('overtime_calculation');
			$data['attendance']				= $this->input->post('attendance');
			$data['basic_salary']			= $this->input->post('basic_salary');
			$data['bpjs_ks']				= $this->input->post('bpjs_ks');
			$data['bpjs_jht']				= $this->input->post('bpjs_jht');
			$data['bpjs_ks_company']		= $this->input->post('bpjs_ks_company');
			$data['bpjs_jht_company']		= $this->input->post('bpjs_jht_company');
			$data['tax_calculation']		= $this->input->post('tax_calculation');
			$data['salary_note']			= $this->input->post('salary_note');

			$this->form_validation->set_rules('payroll_id', '', 'required');
			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$this->payroll_model->save($data);
				$this->payroll_model->delete_detail(array('payroll_id' => $data['id']));
				
				$payroll_detail_name		= $this->input->post('payroll_detail_name');
				$payroll_detail_category	= $this->input->post('payroll_detail_category');
				$payroll_detail_value		= $this->input->post('payroll_detail_value');
				foreach ($payroll_detail_name AS $key => $value ) {
					if($payroll_detail_value[$key] == ''){
						continue;
					}
					$data_detail['payroll_id']	= $data['id'];
					$data_detail['name'] 		= $payroll_detail_name[$key];
					$data_detail['category'] 	= $payroll_detail_category[$key];
					$data_detail['value']		= $payroll_detail_value[$key];
					
					$this->payroll_model->save_detail($data_detail);

				}
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			}
			redirect(base_url('rosecuritypayroll/form/'.$data['id']));
			exit();
		}

		if ($payroll_id)
		{
			$data = (array) $this->payroll_model->get(array('id' => $payroll_id, 'columns' => 'A.id, A.employee_id, A.periode_start, A.periode_end, A.salary, A.overtime_hour, A.overtime_calculation, A.attendance, A.basic_salary, A.bpjs_ks, A.bpjs_jht, A.bpjs_ks_company, A.bpjs_jht_company, A.tax_calculation, A.note, A.salary_note'));
	
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('rosecuritypayroll/list'));
		}

		$data['employee']		= $this->employee_model->get(array('id' => $data['employee_id'], 'columns' => 'A.id_card, A.full_name'));
		if(!$data['employee']){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data karyawan tidak ditemukan.','danger'));
			redirect(base_url('rosecuritypayroll/list'));
		}

		$data['payroll_detail'] = $this->payroll_model->gets_detail(array('payroll_id' => $payroll_id));
		$data['_TITLE_'] 		= 'Gaji Karyawan';
		$data['_PAGE_'] 		= 'rosecuritypayroll/form_payroll';
		$data['_MENU_PARENT_'] 	= 'ro_security';
		$data['_MENU_'] 		= 'ro_security_payroll';
		return $this->view($data);
	}

	public function delete($payroll_id = false)
	{
		$this->load->model('payroll_model');
		if ($payroll_id)
		{
			$data =  $this->payroll_model->get(array('id' => $payroll_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $payroll_id, 'is_active' => 0);
				$result = $this->payroll_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil dihapus.','success'));
					redirect(base_url('rosecuritypayroll/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal dihapus.','danger'));
					redirect(base_url('rosecuritypayroll/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('rosecuritypayroll/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
			redirect(base_url('rosecuritypayroll/list'));
		}
	}

	public function delete_config($config_id = false)
	{
		$this->load->model('payroll_config_model');
		if ($config_id)
		{
			$data =  $this->payroll_config_model->get(array('id' => $config_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $config_id, 'is_active' => 0);
				$result = $this->payroll_config_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil dihapus.','success'));
					redirect(base_url('rosecuritypayroll/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal dihapus.','danger'));
					redirect(base_url('rosecuritypayroll/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('rosecuritypayroll/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
			redirect(base_url('rosecuritypayroll/list'));
		}
	}

	public function process()
	{
		foreach ($this->input->post('id') as $payroll_id)
		{
			$this->load->model(array('payroll_model'));
			$data =  $this->payroll_model->get(array('id' => $payroll_id, 'columns' => 'A.id'));
			if (empty($data)){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('rosecuritypayroll/list'));
			}else{
				$process_type = $this->input->post('process_type');
				$note 		= $this->input->post('note');
				if($process_type == 'submission'){
					$insert = array('id' => $payroll_id, 'payment' => 'Menunggu', 'note' => $note);	
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diajukan.','success'));
		
				}else if($process_type == 'cancel'){
					$insert = array('id' => $payroll_id, 'payment' => 'Draft', 'note' => $note);	
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil dibatalkan.','success'));
				}else{
					$insert = array('id' => $payroll_id, 'is_active' => 0, 'note' => $note);	
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil dihapus.','success'));
		
				}
				$result = $this->payroll_model->save($insert);

				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal diproses.','danger'));
					redirect(base_url('rosecuritypayroll/list'));
				}
			}
		}
		redirect(base_url('rosecuritypayroll/list'));
	}

	public function preview($payroll_id=FALSE)
	{
		$this->load->model(array('payroll_model', 'employee_model', 'position_model'));
		$data['payroll']		= $this->payroll_model->get(array('id' => $payroll_id, 'columns' => 'A.preview'));

		$data['_TITLE_'] 		= 'Preview Gaji Karyawan';
		$data['_PAGE_']	 		= 'rosecuritypayroll/preview';
		$data['_MENU_PARENT_'] 	= 'ro_security';
		$data['_MENU_'] 		= 'ro_security_payroll';
		$this->load->view('rosecuritypayroll/preview', $data);
	}

	public function pdf($payroll_id=FALSE)
	{
		$this->load->model(array('payroll_model', 'employee_model', 'position_model'));
		$payroll		= $this->payroll_model->get(array('id' => $payroll_id, 'columns' => 'A.id, A.employee_id, A.pdf, A.company_code'));
		if (!$payroll_id){
			$data["message"] = message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger');
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
			redirect(base_url('ropayroll/list'));
		}

		
		$data['content'] 	= $payroll->pdf; 
		$data['title'] 		= $payroll->employee_id.'_payroll';
		$data['basepath'] 	= FCPATH.'files/';
		$data['image']		= strtolower($payroll->company_code).'.jpg';
        $this->generate_PDF($data);
       	redirect(base_url('files/'.strtolower(str_replace(" ", "_",$data['title'])).'.pdf'));
	}

	private function generate_PDF($data = array())
	{	
		$this->load->library("Pdf");
		ob_start();
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetHeaderData($data['image'], '181', '', null);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shelter');
		$pdf->SetTitle($data['title']);
		$pdf->SetSubject($data['title']);
		$pdf->SetKeywords('Shelter');

		// $pdf->setPrintHeader(true);
		// $pdf->setPrintFooter(false);

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$pdf->SetFont('times', '', 10);
		$pdf->AddPage();
		$pdf->writeHTML($data['content'], true, false, true, false, '');

		$base_path  = $data['basepath'];
		if(!is_dir($base_path)){
			mkdir($base_path, 0777, true);
		}
		return $pdf->Output($base_path.strtolower(str_replace(" ", "_",$data['title'])).'.pdf', 'F');
	}

	public function import(){
		$this->load->model(array('payroll_config_model', 'employee_model', 'payroll_model'));
		if($_FILES['file']['name'] != ""){
			
			$config['id'] 							= $this->input->post('config_id');
			$config['config_name'] 					= $this->input->post('config_name');
			$config['site_id'] 						= 1;//$this->session->userdata('site');
			$config['row_start'] 					= $this->input->post('row_start');
			$config['row_end'] 						= $this->input->post('row_end');
			$config['row_title'] 					= $this->input->post('row_title');
			$config['column_employee'] 				= strtoupper($this->input->post('column_employee'));
			$config['column_income'] 				= strtoupper($this->input->post('column_income'));
			$config['column_outcome'] 				= strtoupper($this->input->post('column_outcome'));
			$config['column_net'] 					= strtoupper($this->input->post('column_net'));
			$config['column_attendance']			= strtoupper($this->input->post('column_attendance'));
			$config['column_overtime_hour']			= strtoupper($this->input->post('column_overtime_hour'));
			$config['column_overtime_calculation']	= strtoupper($this->input->post('column_overtime_calculation'));
			$config['column_basic_salary']			= strtoupper($this->input->post('column_basic_salary'));
			$config['column_bpjs_ks']				= strtoupper($this->input->post('column_bpjs_ks'));
			$config['column_bpjs_jht']				= strtoupper($this->input->post('column_bpjs_jht'));
			$config['column_bpjs_jp']				= strtoupper($this->input->post('column_bpjs_jp'));
			$config['column_bpjs_ks_company']		= strtoupper($this->input->post('column_bpjs_ks_company'));
			$config['column_bpjs_jht_company']		= strtoupper($this->input->post('column_bpjs_jht_company'));
			$config['column_bpjs_jp_company']		= strtoupper($this->input->post('column_bpjs_jp_company'));
			$config['column_tax_calculation']		= strtoupper($this->input->post('column_tax_calculation'));
			$config['column_note'] 					= strtoupper($this->input->post('column_note'));
			$this->payroll_config_model->save($config);

			$periode_start				= $this->input->post('periode_start');
			$periode_end				= $this->input->post('periode_end');
			
			$this->load->library("Excel");
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);

			$title 			= array();
			$column_income	= array();
			$column_outcome	= array();
			if($config['column_income']){
				$column_income  = explode(",", preg_replace("/[^A-Z,]/", "", $config['column_income']));
			}
			if($config['column_outcome']){
				$column_outcome = explode(",", preg_replace("/[^A-Z,]/", "", $config['column_outcome']));
			}

			foreach($column_income AS $item){
				$title[$item] = replace_null($sheet->getCell($item.$config['row_title'])->getCalculatedValue());
			}
			foreach($column_outcome AS $item){
				$title[$item] = replace_null($sheet->getCell($item.$config['row_title'])->getCalculatedValue());
			}
			$total 				= $config['row_end'] - $config['row_start'];
			$success 			= 0;
			$list_not_register 	= '';
			$candidate 			= '';
			$approval 			= '';
			$payment 			= '';
			for ($row = $config['row_start']; $row <= $config['row_end']; $row++) {
				$employee_number 		= replace_null($sheet->getCell($config['column_employee'].$row)->getValue());
				if($this->validate($employee_number)){
					$employee_number 	= replace_null($sheet->getCell($config['column_employee'].$row)->getOldCalculatedValue());
				}
				if($employee_number == ""){
					continue;
				}
				$employee_number = preg_replace("/[^0-9,]/", "", $employee_number);
				$employee = $this->employee_model->get(array('id_card' => $employee_number, 'columns' => 'A.id, A.status_approval'));

				// $employee = $this->employee_model->get(array('id_card' => $employee_number, 'site_id' => $this->session->userdata('site'), 'columns' => 'A.id'));
				if($employee){
					$payroll = $this->payroll_model->get(array('employee_id' => $employee->id, 'periode_start' => $periode_start, 'periode_end' => $periode_end, 'columns' => 'A.id, A.payment'));
			
					if($payroll){
						if($payroll->payment == 'Selesai' || $payroll->payment == 'Menunggu' ){
						$payment = $payment." ".$employee_number.",";
							continue;
						}
						$this->payroll_model->save(array('id' => $payroll->id, 'is_active' => 0));
						$this->payroll_model->delete_detail(array('payroll_id' => $payroll->id));
					}
					if($employee->status_approval == 0){
						$candidate = $candidate." ".$employee_number.",";
						continue;
					}
					if($employee->status_approval == 1 || $employee->status_approval == 1){
						$approval = $approval." ".$employee_number.",";
						continue;
					}
					$success = $success +1;


					$salary_net 		= replace_null($sheet->getCell($config['column_net'].$row)->getValue());
					if($this->validate($salary_net)){
						$salary_net 	= replace_null($sheet->getCell($config['column_net'].$row)->getOldCalculatedValue());
					}

					if($config['column_attendance'] != ""){
						$attendance 			= replace_null($sheet->getCell($config['column_attendance'].$row)->getValue());
						if($this->validate($attendance)){
							$attendance 		= replace_null($sheet->getCell($config['column_attendance'].$row)->getOldCalculatedValue());
						}
						$insert['attendance'] 	=  $attendance;
					}

					if($config['column_overtime_hour'] != ""){
						$overtime_hour 			= replace_null($sheet->getCell($config['column_overtime_hour'].$row)->getValue());
						if($this->validate($overtime_hour)){
							$overtime_hour 		= replace_null($sheet->getCell($config['column_overtime_hour'].$row)->getOldCalculatedValue());
						}
						$insert['overtime_hour']=  $overtime_hour;
					}

					if($config['column_overtime_calculation'] != ""){
						$overtime_calculation 			= replace_null($sheet->getCell($config['column_overtime_calculation'].$row)->getValue());
						if($this->validate($overtime_calculation)){
							$overtime_calculation 		= replace_null($sheet->getCell($config['column_overtime_calculation'].$row)->getOldCalculatedValue());
						}
						$insert['overtime_calculation'] =  $overtime_calculation;
					}
					
					if($config['column_basic_salary'] != ""){
						$basic_salary 			= replace_null($sheet->getCell($config['column_basic_salary'].$row)->getValue());
						if($this->validate($basic_salary)){
							$basic_salary 		= replace_null($sheet->getCell($config['column_basic_salary'].$row)->getOldCalculatedValue());
						}
						$insert['basic_salary'] =  $basic_salary;
					}
					
					if($config['column_bpjs_ks'] != ""){
						$bpjs_ks 			= replace_null($sheet->getCell($config['column_bpjs_ks'].$row)->getValue());
						if($this->validate($bpjs_ks)){
							$bpjs_ks 		= replace_null($sheet->getCell($config['column_bpjs_ks'].$row)->getOldCalculatedValue());
						}
						$insert['bpjs_ks'] 	=  $bpjs_ks;
					}
					
					if($config['column_bpjs_jht'] != ""){
						$bpjs_jht 			= replace_null($sheet->getCell($config['column_bpjs_jht'].$row)->getValue());
						if($this->validate($bpjs_jht)){
							$bpjs_jht 		= replace_null($sheet->getCell($config['column_bpjs_jht'].$row)->getOldCalculatedValue());
						}
						$insert['bpjs_jht'] =  $bpjs_jht;
					}
					
					if($config['column_bpjs_jp'] != ""){
						$bpjs_jp 			= replace_null($sheet->getCell($config['column_bpjs_jp'].$row)->getValue());
						if($this->validate($bpjs_jp)){
							$bpjs_jp 		= replace_null($sheet->getCell($config['column_bpjs_jp'].$row)->getOldCalculatedValue());
						}
						$insert['bpjs_jp'] 	=  $bpjs_jp;
					}

					if($config['column_bpjs_ks_company'] != ""){
						$bpjs_ks_company 			= replace_null($sheet->getCell($config['column_bpjs_ks_company'].$row)->getValue());
						if($this->validate($bpjs_ks_company)){
							$bpjs_ks_company 		= replace_null($sheet->getCell($config['column_bpjs_ks_company'].$row)->getOldCalculatedValue());
						}
						$insert['bpjs_ks_company'] 	=  $bpjs_ks_company;
					}
					
					if($config['column_bpjs_jht_company'] != ""){
						$bpjs_jht_company 			= replace_null($sheet->getCell($config['column_bpjs_jht_company'].$row)->getValue());
						if($this->validate($bpjs_jht_company)){
							$bpjs_jht_company 		= replace_null($sheet->getCell($config['column_bpjs_jht_company'].$row)->getOldCalculatedValue());
						}
						$insert['bpjs_jht_company'] 	=  $bpjs_jht_company;
					}
					
					if($config['column_bpjs_jp_company'] != ""){
						$bpjs_jp_company 			= replace_null($sheet->getCell($config['column_bpjs_jp_company'].$row)->getValue());
						if($this->validate($bpjs_jp_company)){
							$bpjs_jp_company 		= replace_null($sheet->getCell($config['column_bpjs_jp_company'].$row)->getOldCalculatedValue());
						}
						$insert['bpjs_jp_company'] 	=  $bpjs_jp_company;
					}
					
					if($config['column_tax_calculation'] != ""){
						$tax_calculation 			= replace_null($sheet->getCell($config['column_tax_calculation'].$row)->getValue());
						if($this->validate($tax_calculation)){
							$tax_calculation 		= replace_null($sheet->getCell($config['column_tax_calculation'].$row)->getOldCalculatedValue());
						}
						$insert['tax_calculation'] 	=  $tax_calculation;
					}

					if($config['column_note'] != ""){
						$salary_note 		= replace_null($sheet->getCell($config['column_note'].$row)->getValue());
						$insert['salary_note'] 	=  $salary_note;
					}

					$insert['id']				= '';	
					$insert['employee_id'] 		=  $employee->id;
					$insert['periode_start'] 	=  $periode_start;
					$insert['periode_end'] 		=  $periode_end;
					$insert['salary'] 			=  $salary_net;
					$insert['payment'] 			=  'Draft';
					$insert['email'] 			=  'Menunggu';
					$payroll_id = $this->payroll_model->save($insert);
					foreach($column_income AS $item){
						$insert_detail['payroll_id']= $payroll_id;
						$insert_detail['name'] 		= $title[$item];
						$insert_detail['category'] 	= 'pendapatan';
						$insert_detail['value'] 			= replace_null($sheet->getCell($item.$row)->getValue());
						if($this->validate($insert_detail['value'])){
							$insert_detail['value'] 		= replace_null($sheet->getCell($item.$row)->getOldCalculatedValue());
						}
						$this->payroll_model->save_detail($insert_detail);
					}

					foreach($column_outcome AS $item){
						$insert_detail['payroll_id']= $payroll_id;
						$insert_detail['name'] 		= $title[$item];
						$insert_detail['category'] 	= 'potongan';
						$insert_detail['value'] 			= replace_null($sheet->getCell($item.$row)->getValue());
						if($this->validate($insert_detail['value'])){
							$insert_detail['value'] 		= replace_null($sheet->getCell($item.$row)->getOldCalculatedValue());
						}
						$this->payroll_model->save_detail($insert_detail);
					}
				}else{
					$list = $list." ".$employee_number.",";
				}
			}
			if($payment != '' || $list_not_register != '' || $candidate != '' || $approval 	!= ''){
				$this->session->set_flashdata('message', message_box('<strong>Peringatan!</strong> Data yang berhasil diinputkan : '.$success.' dari '.$total.', pastikan NIK yang diimport sesuai dengan data NIK di HRIS. <br> Berikut daftar NIK yang gagal diimport : <br> <strong>1. NIK Belum Terdaftar : '.$list.'<br>2. NIK Masih Proses Pengajuan : '.$approval.'<br>3. NIK masih didaftar Kandidat : '.$candidate.'<br>3. NIK Payroll Sudah terbayar : '.$payment.'</strong>','warning'));
			}else if($success == 0){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan, silahkan cek kolom NIK untuk memastikan NIK karywan sudah benar.','danger'));
			}else if($success < $total){
				$this->session->set_flashdata('message', message_box('<strong>Peringatan!</strong> Data yang berhasil diinputkan : '.$success.' dari '.$total.', pastikan NIK yang diimport sesuai dengan data NIK di HRIS. <br> Berikut daftar NIK yang gagal diimport : <br> <strong>1. NIK Belum Terdaftar : '.$list.'<br>2. NIK Masih Proses Pengajuan : '.$approval.'<br>3. NIK masih didaftar Kandidat : '.$candidate.'<br>3. NIK Payroll Sudah terbayar : '.$payment.'</strong>','warning'));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			}
			$this->generate_payroll();
			redirect(base_url('rosecuritypayroll/list'));
		}
	}

	public function payroll_count(){
		$this->load->model(array('employee_model', 'payroll_model', 'cutoff_model', 'employee_salary_model', 'attendance_model'));
		$params['site_id']		= $this->session->userdata('site');
		$params['date_start']	= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_start'))));
		$params['date_end']		= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_finish'))));

		$list_employee = $this->employee_model->gets_ptkp(array('site_id' => $params['site_id'], 'status' => 1, 'status_approval' => 3, 'columns' => 'A.id, B.minimal, A.resign_submit'));
		$list_resign   = $this->employee_model->gets_ptkp(array('site_id' => $params['site_id'], 'resign_submit' => date('Y-m'), 'columns' => 'A.id, B.minimal, A.resign_submit'));
		$list_employee = (object) array_merge((array) $list_employee, (array) $list_resign);
		foreach($list_employee AS $employee){
			$payroll_employee['id'] 			= '';
			$payroll = $this->payroll_model->get(array('employee_id' => $employee->id, 'periode_start' => $params['date_start'], 'periode_end' => $params['date_end'], 'columns' => 'A.id, A.payment'));
			if($payroll){
				if($payroll->payment == 'Pengajuan' || $payroll->payment == 'Selesai'){
					continue;
				}
				$payroll_employee['id'] = $payroll->id;	
			}

			$payroll_employee['employee_id'] 			= $employee->id;
			$payroll_employee['periode_start'] 			= $params['date_start'];
			$payroll_employee['periode_end'] 			= $params['date_end'];
			$payroll_employee['is_active'] 				= 1;
			$payroll_employee['payment'] 				= 'Draft';
			$payroll_employee['email'] 					= 'Menunggu';
			$payroll_employee['salary'] 				= 0;
			$payroll_employee['overtime_hour'] 			= 0;
			$payroll_employee['overtime_calculation'] 	= 0;
			$payroll_employee['attendance'] 			= 0;
			$payroll_employee['basic_salary'] 			= 0;
			$payroll_employee['bpjs_ks'] 				= 0;
			$payroll_employee['bpjs_jht'] 				= 0;
			$payroll_employee['bpjs_jp'] 				= 0;
			$payroll_employee['bpjs_ks_company'] 		= 0;
			$payroll_employee['bpjs_jht_company'] 		= 0;
			$payroll_employee['bpjs_jp_company'] 		= 0;
			$payroll_employee['tax_calculation'] 		= 0;
			$payroll_employee['note'] 					= '';
			$payroll_employee['salary_note'] 			= '';
			$salary = $this->employee_salary_model->get_salary(array('employee_id' => $employee->id, 'columns' => 'A.basic_salary, A.bpjs_ks, A.bpjs_tk, A.bpjs_ks_employee, A.bpjs_tk_employee, A.join_date'));
			
			if(!$salary){
				continue;
			}

			$basic_salary = evalmath($salary->basic_salary);
			$payroll_employee['basic_salary'] = $basic_salary;

			$bpjs_ks = evalmath($salary->bpjs_ks);
			$payroll_employee['bpjs_ks_company'] = str_replace("-", "", $bpjs_ks);

			$bpjs_tk = evalmath($salary->bpjs_tk);
			$payroll_employee['bpjs_jht_company'] = str_replace("-", "", $bpjs_tk);

			$bpjs_ks_employee = evalmath($salary->bpjs_ks_employee);
			$payroll_employee['bpjs_ks'] = str_replace("-", "", $bpjs_ks_employee);

			$bpjs_tk_employee = evalmath($salary->bpjs_tk_employee);
			$payroll_employee['bpjs_jht'] = str_replace("-", "", $bpjs_tk_employee);

			$payroll_id = $this->payroll_model->save($payroll_employee);
			$this->payroll_model->delete_detail(array('payroll_id' => $payroll_id));
				
			$detail_potongan = 0;
			$detail_pendapatan = 0;
			$salary_detail = $this->employee_salary_model->gets_detail(array('employee_id' => $employee->id, 'columns' => 'A.name, A.value'));

			foreach($salary_detail AS $detail){
				$payroll_employee_detail['payroll_id']	= $payroll_id;
				$payroll_employee_detail['name'] 		= $detail->name;
				$payroll_employee_detail['is_active'] 	= 1;	
					
				if($detail->value == '' || $detail->value == 0){
					continue;
				}

				if(substr($detail->value, 0, 1) == '-'){
					$payroll_employee_detail['category']= 'potongan';
					$payroll_employee_detail['value']   = substr($detail->value, 1);
					$detail_potongan 					= $detail_potongan + $payroll_employee_detail['value'];
				}else{
					$payroll_employee_detail['category']= 'pendapatan';
					$payroll_employee_detail['value']   = $detail->value;
					$detail_pendapatan 					= $detail_pendapatan + $payroll_employee_detail['value'];
				}
				
				$this->payroll_model->save_detail($payroll_employee_detail);
			}

			$timeStart 	= strtotime($salary->join_date);
			$timeEnd 	= strtotime($params['date_end']);
			$monthservice 	= 1 + (date("Y",$timeEnd)-date("Y",$timeStart))*12;
			$monthservice 	+= date("m",$timeEnd)-date("m",$timeStart);
			$yearService = $this->yearService($monthservice);
			$this->payroll_model->save_detail(array('payroll_id' => $payroll_id, 'name' => 'MASA KERJA', 'is_active' => 1, 'category' => 'pendapatan', 'value' => $yearService));
			$gross 		= $basic_salary + $detail_pendapatan + $yearService + $bpjs_ks + $bpjs_tk;
			$feePosition= $this->feePosition($gross);
			$grossYear 	= ($gross-$feePosition-($bpjs_ks_employee+$bpjs_tk_employee))*12;
			$tax 		= $this->getTax(array('ptkp' => $employee->minimal, 'grossYear'  => $grossYear));
			
			$this->payroll_model->save(array('id' => $payroll_id, 'tax_calculation' => $tax));
			$full_salary= $gross - ($bpjs_ks_employee + $bpjs_tk_employee + $tax);

			$params['employee_id'] 	= $employee->id;
			$params['resign_submit']= $employee->resign_submit;
			$params['join_date']	= '';
			if($monthservice == 0){
				$params['join_date']	= $salary->join_date;	
			}

			$attendance = $this->checkAttendance($params);
			$reduction 	= $full_salary/30;
			if($params['resign_submit'] == '' || $params['join_date'] == ''){
				$full_salary = ($full_salary/25) * $attendance['present'];
			}

			$payroll_config = $this->cutoff_model->get(array('site_id' => $params['site_id'], 'columns' => 'IFNULL(0, A.late) AS late, IFNULL(0, A.absent) AS absent, IFNULL(0, A.overtime) AS overtime '));
			$reduction_late = 0;
			if($attendance['late'] > 0){
				$reduction_late = $attendance['late'] * $payroll_config->late;
				$this->payroll_model->save_detail(array('payroll_id' => $payroll_id, 'name' => 'Potongan Keterlambatan', 'is_active' => 1, 'category' => 'potongan', 'value' => $reduction_late));
			}

			$reduction_absent = 0;
			if($attendance['not_present'] > 0){
				$reduction_absent = $attendance['not_present'] * $payroll_config->absent;
				$this->payroll_model->save_detail(array('payroll_id' => $payroll_id, 'name' => 'Potongan Tidak Masuk Kerja', 'is_active' => 1, 'category' => 'potongan', 'value' => $reduction_absent));
			}

			$overtime_calculation = 0;
			if($attendance['overtime'] > 0){
				$overtime_calculation = $attendance['overtime'] * $payroll_config->overtime;
			}

			$salary = $full_salary - ($reduction_absent + $reduction_late + $detail_potongan) + $overtime_calculation;
			$this->payroll_model->save(array('id' => $payroll_id, 'salary' => $salary, 'overtime_hour' => $attendance['overtime'], 'overtime_calculation' => $overtime_calculation,'attendance' => $attendance['present']));
		}
		$this->generate_payroll();
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Gaji Karyawan berhasil diproses.','success'));
		redirect(base_url('rosecuritypayroll/list'));
	}

	private function checkAttendance($params = array()){
		$not_present	= 0;
		$present		= 0;
		$late			= 0;
		$overtime		= 0;
		$date_start 	= $params['date_start'];
		$date_end 		= $params['date_end'];
		if($params['join_date'] != '' ){
			$date_start = $params['join_date'];
		}

		if($params['resign_submit'] != ''){
			$date_end 		= $params['resign_submit'];
		}
		
		$list_attendance = $this->attendance_model->gets(array('employee_id' => $params['employee_id'], 'date_start' => $date_start, 'date_end' => $date_end, 'columns' => 'A.date, A.schedule_start, A.schedule_end, A.attendance_start, A.attendance_end'));
		
		foreach($list_attendance AS $item){
			if($item->schedule_start == '' && $item->schedule_end == ''){
				$present +1;
				continue;
			}

			if($item->schedule_start == '00:00' && $item->schedule_end == '00:00'){
				continue;
			}
			if($item->attendance_start == '00:00' && $item->attendance_end == '00:00'){
				$not_present= $not_present + 1;
			}else{
				$present 		= $present + 1;
				$start_come 	= strtotime($item->date." ".$item->schedule_start);
				$end_come 		= strtotime($item->date." ".$item->attendance_start);
				$diff_come 		= ceil(($end_come - $start_come) / (60 * 60));
				if($diff_come > 0){
					$late = $late + $diff_come;
				}

				$start_return 		= strtotime($item->date." ".$item->schedule_end_);
				$end_return 		= strtotime($item->date." ".$item->attendance_end);
				$diff_return 		= ceil(($end_return - $start_return) / (60 * 60));
				if($diff_return > 0){
					$overtime = $overtime + $diff_return;
				}
			}
		}
		return array('not_present' => $not_present, 'present' => $present, 'late' => $late, 'overtime' => $overtime);
	}
	private function getTax($params = array()){
		$result = 0;
		$taxYear = $params['grossYear'] - $params['ptkp'];
		if($taxYear > 0){
			if($taxYear > 500000000){
				$result = ($taxYear*30)/100;
			}else if($taxYear > 250000000){
				$result = ($taxYear*25)/100;
			}else if($taxYear > 50000000){
				$result = ($taxYear*15)/100;
			}else{
				$result = ($taxYear*5)/100;
			}
			$result = $result/12;
		}else{
			$result = 0;
		}
		return $result;
	}
	private function feePosition($gross = 0){
		$result = (5*$gross)/100;
		if($result > 500000){
			$result = 500000;
		}
		return $result;
	}
	private function yearService($month = 0){
		$result = 0;
		if($month >= 216){
			$result = 350000;
		}else if($month >= 180){
			$result = 300000;
		}else if($month >= 144){
			$result = 250000;
		}else if($month >= 108){
			$result = 200000;
		}else if($month >= 72){
			$result = 150000;
		}else if($month >= 36){
			$result = 100000;
		}else if($month >= 12){
			$result = 50000;
		}else{
			$result = 0;
		}
		return $result;
	}

	private function validate($value){
		$result = false;
		if(substr($value, 0, 1) == "="){
			$result = true;
		}
		return $result;
	}

    public function generate_payroll(){
		$this->load->model(array('payroll_model', 'employee_model'));

		$list_payroll	= $this->payroll_model->generate_payroll(array('submission' => array('Draft', 'Menunggu'), 
		// 'site_id' => $this->session->userdata('site'), 
		'not_preview' => true,
		'columns' => 'A.id, A.employee_id, A.attendance, A.overtime_hour, A.basic_salary, A.overtime_calculation, A.bpjs_ks, A.bpjs_jht, A.tax_calculation, A.salary, A.periode_start, A.periode_end, A.salary_note, B.full_name, B.employee_number, B.bank_account, C.code AS company_code, D.name AS site_name, E.name AS position'));

		foreach ($list_payroll as $payroll) {	
			$payroll_id 	= $payroll->id;
			$income			= $this->payroll_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'pendapatan', 'columns' => 'A.name, A.value'));
			$outcome		= $this->payroll_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'potongan', 'columns' => 'A.name, A.value'));

			$start_date = substr($payroll->periode_start, 8, 2).' '.get_month(substr($payroll->periode_start, 5, 2));
			$end_date 	= substr($payroll->periode_end, 8, 2).' '.get_month(substr($payroll->periode_end, 5, 2)).' '.substr($payroll->periode_end, 0, 4);

			$kehadiran 		  = "";
			$jumlah_kehadiran = "";
			if($payroll->attendance){
				$kehadiran 			= 'JUMLAH KEHADIRAN <br>';
				$jumlah_kehadiran 	= ': '.$payroll->attendance;
			}
			$lembur 		= "";
			$jumlah_lembur 	= "";
			if($payroll->overtime_hour){
				$lembur 		= 'JUMLAH JAM LEMBUR <br>';
				$jumlah_lembur 	= $payroll->overtime_hour;
			}
			$gaji_pokok 	= "0";
			if($payroll->basic_salary){
				$gaji_pokok = $payroll->basic_salary;
			}
			$pendapatan = "";
			if($payroll->overtime_calculation){
				$pendapatan .= '<tr><td>Lembur</td><td>: Rp.</td><td style="float:right;">'.rupiah($payroll->overtime_calculation).'</td></tr>';
			}
			foreach( $income AS $item){
				if($item->name != ''){
				    $pendapatan .= "<tr><td >".$item->name." &nbsp;&nbsp;</td><td>: Rp.</td><td style='float:right;'>".rupiah($item->value)."</td></tr>";
				}
			}
			$potongan 	= "";
			$potongan .= '<tr><td>BPJS Kesehatan</td><td>: Rp.</td><td style="float:right;">'.rupiah($payroll->bpjs_ks).'</td></tr>';
			$potongan .= '<tr><td>BPJS Ketenagakerjaan</td><td>: Rp.</td><td style="float:right;">'.rupiah($payroll->bpjs_jht).'</td></tr>';
			$potongan .= '<tr><td>PPH21</td><td>: Rp.</td><td style="float:right;">'.rupiah($payroll->tax_calculation).'</td></tr>';
			
			foreach( $outcome AS $item){
				if($item->name != ''){
				    $potongan .= "<tr><td >".$item->name." &nbsp;&nbsp;</td><td>: Rp.</td><td style='float:right;'>".rupiah($item->value)."</td></tr>";
				}
			}

			$image_url = 'files/'.strtolower($payroll->company_code).'.jpg';
			$preview = '<section class="invoice">
				<div class="row">
					<div class="col-12">
						<h2 class="page-header">
							<img width="100%" src="'.base_url($image_url).'"/>
						</h2>
					</div>
				</div>
				<div class="row invoice-info m-4">
					<div class="col-sm-3 invoice-col">
						<address>
							PERIODE BULAN <br>
							NO REKENING <br>
							NAMA <br>
							NOMOR KARYAWAN <br>
							JABATAN <br>
							SITE <br>
							'.$kehadiran.'
							'.$lembur.'
						</address>
					</div>
					<div class="col-sm-6 invoice-col">
						<?php 
							
						?>
						<address>
							: '.$start_date.' - '.$end_date.'<br>
							: '.$payroll->bank_account.'<br>
							: '.$payroll->full_name.'<br>
							: '.$payroll->employee_number.'<br>
							: '.$payroll->position.'<br>
							: '.$payroll->site_name.'<br>
							  '.$jumlah_kehadiran.'<br>
							  '.$jumlah_lembur.'<br>
						</address>
					</div>
				</div>
				<div class="row invoice-info">
					<div class="col-sm-6 invoice-col">
					&nbsp;
					</div>
					<div class="col-sm-6 invoice-col">
						<address >
							<strong>Gaji bersih diterima : '.format_rupiah($payroll->salary).'</strong>
						</address>
					</div>
				</div>
				<div class="row invoice-info m-4">
					<div class="col-sm-6 invoice-col">
						<address>
							<table style="border:none">
								<tr><td><strong>Pendapatan</strong></td><td></td><td></td></tr>
								<tr><td>Gaji Pokok</td><td>: Rp.</td><td style="float:right;">'.rupiah($gaji_pokok).'</td></tr>
								'.$pendapatan.'
        					</table>
        				</address>
      				</div>
      				<div class="col-sm-6 invoice-col">
      					<address>
      						<table style="border:none">
      							<tr><td><strong>Potongan</strong></td><td></td><td></td></tr>
								'.$potongan.'
							</table>
						</address>
					</div>
				</div>
				<div class="row invoice-info m-4" style="font-size: 12px;">
					<address >
						<strong>Catatan : </strong>'.$payroll->salary_note.'
					</address>
				</div>
			</section>';

			$pdf  = '<table width="100%">';
			$pdf .= '<tr><td width="25%">PERIODE BULAN</td><td> : '.$start_date.' - '.$end_date.'</td></tr>
				<tr><td>NO REKENING</td><td> : '.$payroll->bank_account.'</td></tr>
				<tr><td>NAMA</td><td> : '.$payroll->full_name.'</td></tr>
				<tr><td>NOMOR KARYAWAN</td><td> : '.$payroll->employee_number.'</td></tr>
				<tr><td>JABATAN</td><td> : '.$payroll->position.'</td></tr>
				<tr><td>SITE</td><td> : '.$payroll->site_name.'</td></tr>';
			if($payroll->attendance){
				$pdf .= '<tr><td>JUMLAH KEHADIRAN</td><td> : '.$payroll->attendance.'</td></tr>';
			}
			if($payroll->overtime_hour){
				$pdf .= '<tr><td>JUMLAH JAM LEMBUR</td><td> : '.$payroll->overtime_hour.'</td></tr>';
			}

			$pdf .= '</table><table width="100%">';
			$pdf .= '<tr><td width="60%">&nbsp;</td><td><strong>Gaji Bersih diterima : '.format_rupiah($payroll->salary).'</strong></td></tr>';
			$pdf .= '</table><br><br><table width="100%">';
			$pdf .= '<tr><td width="50%"><table>';
			$pdf .= '<tr><td><strong>Pendapatan</strong></td><td></td><td></td></tr>';
			if($payroll->basic_salary){
				$pdf .= "<tr><td>Gaji Pokok</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->basic_salary)."</td></tr>";
			}else{
				$pdf .= "<tr><td>Gaji Pokok</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
			}
			if($payroll->overtime_calculation){
				$pdf .= "<tr><td>Lembur</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->overtime_calculation)."</td></tr>";
			}
			foreach( $income AS $item):
				if($item->name != ''){
				    $pdf .= "<tr><td >".$item->name."</td><td>: Rp.</td><td style='float:right'>".rupiah($item->value)."</td></tr>";
				}
			endforeach;
			$pdf .= '</table></td>';
			$pdf .= '<td width="50%"><table>';
			$pdf .= '<tr><td><strong>Potongan</strong></td><td></td><td></td></tr>';
			if($payroll->bpjs_ks){
				$pdf .= "<tr><td>BPJS Kesehatan</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->bpjs_ks)."</td></tr>";
			}else{
				$pdf .= "<tr><td>BPJS Kesehatan</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
			}
			if($payroll->bpjs_jht){
				$pdf .= "<tr><td>BPJS Ketenagakerjaan</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->bpjs_jht)."</td></tr>";
			}else{
				$pdf .= "<tr><td>BPJS Ketenagakerjaan</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
			}
			if($payroll->tax_calculation){
				$pdf .= "<tr><td>PPH21</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->tax_calculation)."</td></tr>";
			}else{
				$pdf .= "<tr><td>PPH21</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
				
			}
			foreach($outcome AS $item):
				if($item->name != ''){
				    $pdf .= "<tr><td >".$item->name."</td><td>: Rp.</td><td style='float:right'>".rupiah($item->value)."</td></tr>";
				}
			endforeach;
			$pdf .= '</table></td></tr>';
			$pdf .= '</table>';
			$pdf .= '</table></td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr></table>';
			$pdf .= '<table width="100%"><tr><td><strong>Catatan : </strong>'.$payroll->salary_note.'</td></tr></table><br>';
			$pdf .= '<div syle="width=100%"></hr></div><br pagebreak="true"/>';

			$this->payroll_model->save(array('id' => $payroll->id, 'preview' => $preview, 'pdf' => $pdf, 'company_code' => $payroll->company_code));
			// print_prev($preview);
		}
	}
}
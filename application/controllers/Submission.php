<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Submission extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('recruitment'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function approval($page = FALSE){

		$this->load->model(array('candidate_model', 'approval_model'));
		$action = $this->input->post('action');
		$note = $this->input->post('note');

		foreach ($this->input->post('id') as $candidate_id)
		{
			$data = (array) $this->candidate_model->get(array('id' => $candidate_id, 'columns' => 'A.id, A.site_id, A.company_id, A.position_id, A.status_nonjob'));
			if (empty($data)){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('submission/'.$page));
			}else{

				$insert = array('id' => $candidate_id, 'site_id' => $data['site_id']);
				$insert['status_approval'] = 0;
				$status_approval = 'Ditolak';
				if($data['status_nonjob'] == 1){
					$insert['status_approval'] = 3;
					$status_approval = 'Diterima';
				}
				if($action == 'approve'){
					$insert['status_approval'] 	= 2;
					$insert['status_nonjob'] 	= 0;
					$status_approval = 'Pengajuan ke Personalia';
				}
				$result = $this->candidate_model->save($insert);

				$approval = array('id' => '', 'employee_id' => $data['id'] , 'site_id' => $data['site_id'], 'company_id' => $data['company_id'], 'position_id' => $data['position_id'], 'status_approval' => $status_approval, 'note' => $note);
				$this->approval_model->save($approval);

				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pembatalan gagal.','danger'));
					redirect(base_url('submission/'.$page));
				}
			}
		}
		
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Pembatalan Sukses.','success'));
		redirect(base_url('submission/'.$page));
	}

	public function personalia(){
		$this->load->model(array('site_model', 'company_model'));
		$data['_TITLE_'] 		= 'Pengajuan ke Personalia';
		$data['_PAGE_'] 		= 'submission/personalia';
		$data['_MENU_PARENT_'] 	= 'recruitment_direct';
		$data['_MENU_'] 		= 'personalia';
		$this->view($data);
	}

	public function personalia_ajax(){
		$this->load->model(array('candidate_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.registration_number, A.full_name, A.id_card, A.phone_number, FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25) AS age, A.education_level, A.education_majors, I.name AS site_name, J.name AS company_name, K.name AS position, DATE_FORMAT(A.placement_date, '%d/%m/%Y') AS placement_date, A.status_completeness, A.document_family_card, A.document_photo, A.document_id_card, A.document_certificate_education";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		// $params['status']		= 0;
		$params['not_site_id']		= 2;
		$params['status_approval'] 	= 2;

		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['id_card']			= $_POST['columns'][3]['search']['value'];
		$params['phone_number']		= $_POST['columns'][4]['search']['value'];
		$params['age']				= $_POST['columns'][5]['search']['value'];
		$params['education_level']	= $_POST['columns'][6]['search']['value'];
		$params['education_majors']	= $_POST['columns'][7]['search']['value'];
		$params['site_name']		= $_POST['columns'][8]['search']['value'];
		$params['company_name']		= $_POST['columns'][9]['search']['value'];
		$params['position']			= $_POST['columns'][10]['search']['value'];
		$params['placement_date']	= $_POST['columns'][11]['search']['value'];

		if($this->session->userdata('branch') > 1){
			$params['branch_id']	= $this->session->userdata('branch');
		}

		$list 	= $this->candidate_model->gets($params);
		$total 	= $this->candidate_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['id']					= '';

			$result['no'] 					= $i;
			$result['full_name']			= $item->full_name;
			$result['id_card']				= $item->id_card;
			$list_number 					= $item->phone_number;
			$phone_number					= "";
			if($list_number != ""){
				$phone_number = explode ("/", $list_number);
				$phone_number = preg_replace("/[^0-9]/", "", $phone_number[0]);
				$start = substr($phone_number, 0, 1 );
				if($start == '0'){
					$phone_number = '62'.substr($phone_number, 1,strlen($phone_number));
				}else if($start == '+'){
					$phone_number = substr($phone_number, 1,strlen($phone_number));
				}
				$phone_number = '<a href="http://wa.me/'.$phone_number.'" target="_blank">'.$item->phone_number.'</a>';
			}
			$result['phone_number']			= $phone_number;
			$result['age']					= $item->age;
			$result['education_level']		= $item->education_level;
			$result['education_majors']		= $item->education_majors;
			$result['site_name']			= $item->site_name;
			$result['company_name']			= $item->company_name;
			$result['position']				= $item->position;
			$result['placement_date']		= $item->placement_date;
			$result['document']				= '';
			if($item->document_family_card == '' || $item->document_photo == ''|| $item->document_id_card == '' || $item->document_certificate_education == ''){
				$result['document'] = '<span class="text-danger"> Kurang : ';
				if($item->document_family_card == ''){
					$result['document'] .= ' Kartu Keluarga,';
				}
				if($item->document_photo == ''){
					$result['document'] .= ' Foto Diri,';
				}
				if($item->document_id_card == ''){
					$result['document'] .= ' KTP,';
				} 
				if($item->document_certificate_education == ''){
					$result['document'] .= ' Ijazah,';
				}
				$result['document'] .= '</span>';
			}else{
				$result['document'] = '<span class="text-success"> Lengkap </span>';
				$result['id']		= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			}

			$result['action'] 				=
				'<a class="btn-sm btn-warning btn-action btn-block" style="cursor:pointer;" href="'. base_url("submission/edit/".$item->id).'">Ubah</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("candidate/preview/".$item->id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("candidate/curriculum_vitae/".$item->id).'">CV</a>
				<a onclick="preview(this)" class="btn-sm btn-success btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("approval/approval_history/".$item->id).'">Riwayat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function client(){
		$this->load->model(array('site_model', 'company_model'));
		$data['_TITLE_'] 		= 'Pengajuan Client';
		$data['_PAGE_'] 		= 'submission/client';
		$data['_MENU_PARENT_'] 	= 'recruitment_direct';
		$data['_MENU_'] 		= 'client';
		$this->view($data);
	}

	public function client_ajax(){
		$this->load->model(array('candidate_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.registration_number, A.full_name, A.id_card, FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25) AS age, A.education_level, A.education_majors, B.name AS city_domisili, GROUP_CONCAT(C.certificate_name) AS certificate, I.name AS site_name, J.code AS company_code, K.name AS position, DATE_FORMAT(A.placement_date, '%d/%m/%Y') AS placement_date, A.status_completeness, A.document_family_card, A.document_photo, A.document_id_card, A.document_certificate_education";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['not_site_id']		= 2;
		$params['status_approval'] 	= 1;

		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['id_card']			= $_POST['columns'][3]['search']['value'];
		$params['age']				= $_POST['columns'][4]['search']['value'];
		$params['education_level']	= $_POST['columns'][5]['search']['value'];
		$params['education_majors']	= $_POST['columns'][6]['search']['value'];
		$params['city_domisili']	= $_POST['columns'][7]['search']['value'];
		$params['certificate']		= $_POST['columns'][8]['search']['value'];
		$params['site_name']		= $_POST['columns'][9]['search']['value'];
		$params['company_code']		= $_POST['columns'][10]['search']['value'];
		$params['position']			= $_POST['columns'][11]['search']['value'];
		$params['placement_date']	= $_POST['columns'][12]['search']['value'];

		if($this->session->userdata('branch') > 1){
			$params['branch_id']	= $this->session->userdata('branch');
		}

		$list 	= $this->candidate_model->gets($params);
		$total 	= $this->candidate_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			
			$result['no'] 					= $i;
			$result['full_name']			= $item->full_name;
			$result['id_card']				= $item->id_card;
			$result['age']					= $item->age;
			$result['education_level']		= $item->education_level;
			$result['education_majors']		= $item->education_majors;
			$result['city_domisili']		= $item->city_domisili;
			$result['certificate']			= $item->certificate;
			$result['site_name']			= $item->site_name;
			$result['company_code']			= $item->company_code;
			$result['position']				= $item->position;
			$result['placement_date']		= $item->placement_date;
			$result['document']				= '';
			$result['id']					= '';
			if($item->document_family_card == '' || $item->document_photo == '' || $item->document_id_card == '' || $item->document_certificate_education == ''){
				$result['document'] = '<span class="text-danger"> Kurang : ';
				if($item->document_family_card == ''){
					$result['document'] .= ' Kartu Keluarga,';
				}
				if($item->document_photo == ''){
					$result['document'] .= ' Foto Diri,';
				}
				if($item->document_id_card == ''){
					$result['document'] .= ' KTP,';
				} 
				if($item->document_certificate_education == ''){
					$result['document'] .= ' Ijazah,';
				}
				$result['document'] .= '</span>';
			}else{
				$result['document'] = '<span class="text-success"> Lengkap </span>';
				$result['id']		= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			
			}
			
			$result['action'] 				=
				'<a class="btn-sm btn-warning btn-action btn-block" style="cursor:pointer;" href="'. base_url("submission/edit/".$item->id).'">Ubah</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("candidate/preview/".$item->id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("candidate/curriculum_vitae/".$item->id).'">CV</a>
				<a onclick="preview(this)" class="btn-sm btn-success btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("approval/approval_history/".$item->id).'">Riwayat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function indirect(){
		$this->load->model(array('site_model', 'company_model'));
		$data['_TITLE_'] 		= 'Pengajuan Indirect';
		$data['_PAGE_'] 		= 'submission/indirect';
		$data['_MENU_PARENT_'] 	= 'recruitment_indirect';
		$data['_MENU_'] 		= 'indirect';
		$this->view($data);
	}

	public function indirect_ajax(){
		$this->load->model(array('candidate_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.registration_number, A.full_name, A.id_card, FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25) AS age, A.education_level, A.education_majors, B.name AS city_domisili, GROUP_CONCAT(C.certificate_name) AS certificate, J.name AS company_name, K.name AS position, DATE_FORMAT(A.placement_date, '%d/%m/%Y') AS placement_date";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		// $params['status']		= 0;
		$params['site_id']		= 2;
		$params['status_approval'] 	= 2;

		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['id_card']			= $_POST['columns'][3]['search']['value'];
		$params['age']				= $_POST['columns'][4]['search']['value'];
		$params['education_level']	= $_POST['columns'][5]['search']['value'];
		$params['education_majors']	= $_POST['columns'][6]['search']['value'];
		$params['city_domisili']	= $_POST['columns'][7]['search']['value'];
		$params['certificate']		= $_POST['columns'][8]['search']['value'];
		$params['company_name']		= $_POST['columns'][9]['search']['value'];
		$params['position']			= $_POST['columns'][10]['search']['value'];
		$params['placement_date']	= $_POST['columns'][11]['search']['value'];

		if($this->session->userdata('branch') > 1){
			$params['session_branch']	= $this->session->userdata('branch');
		}
		
		$list 	= $this->candidate_model->gets($params);
		$total 	= $this->candidate_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['full_name']			= $item->full_name;
			$result['id_card']				= $item->id_card;
			$result['age']					= $item->age;
			$result['education_level']		= $item->education_level;
			$result['education_majors']		= $item->education_majors;
			$result['city_domisili']		= $item->city_domisili;
			$result['certificate']			= $item->certificate;
			$result['company_name']			= $item->company_name;
			$result['position']				= $item->position;
			$result['placement_date']		= $item->placement_date;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("candidate/preview/".$item->id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("candidate/curriculum_vitae/".$item->id).'">CV</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function export()
	{
		$this->load->model(array('candidate_model', 'city_model', 'site_model','employee_education_model', 'employee_emergency_model', 'employee_history_model', 'employee_language_model', 'employee_reference_model', 'employee_skill_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = "Data Calon Karyawan";
		$params = $this->input->get();
		
		if($params['status'] == 'pengajuan_ke_client'){
			$params['status_approval'] = 1;
			$params['not_site_id']		= 2;

			$title = 'pengajuan_ke_client';
			$files = glob(FCPATH."files/".$title."/*");
		}else if($params['status'] == 'pengajuan_ke_personalia'){
			$params['status_approval'] = 2;
			$params['not_site_id']		= 2;

			$title = 'pengajuan_ke_personalia';
			$files = glob(FCPATH."files/".$title."/*");
		}else if($params['status'] == 'pengajuan_indirect'){
			$params['status_approval'] 	= 2;
			$params['site_id'] 			= 2;

			$title = 'pengajuan_indirect';
			$files = glob(FCPATH."files/".$title."/*");
		}

		$params['status'] = 0;
		
		unset($params['type']);
		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Nomor Pendaftaran");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("C".$i, "Nomor Identitas (KTP/SIM)");
		$excel->getActiveSheet()->setCellValue("D".$i, "Masa Berlaku (KTP/SIM)");
		$excel->getActiveSheet()->setCellValue("E".$i, "Tempat Lahir");
		$excel->getActiveSheet()->setCellValue("F".$i, "Tanggal Lahir");
		$excel->getActiveSheet()->setCellValue("G".$i, "Kota Sesuai KTP");
		$excel->getActiveSheet()->setCellValue("H".$i, "Alamat Sesuai KTP");
		$excel->getActiveSheet()->setCellValue("I".$i, "Kota Domisili");
		$excel->getActiveSheet()->setCellValue("J".$i, "Alamat Domisili");
		$excel->getActiveSheet()->setCellValue("K".$i, "Status Rumah");
		$excel->getActiveSheet()->setCellValue("L".$i, "No. Telephon/HP");
		$excel->getActiveSheet()->setCellValue("M".$i, "Email");
		$excel->getActiveSheet()->setCellValue("N".$i, "Facebook");
		$excel->getActiveSheet()->setCellValue("O".$i, "Instagram");
		$excel->getActiveSheet()->setCellValue("P".$i, "Twitter");
		$excel->getActiveSheet()->setCellValue("Q".$i, "Agama");
		$excel->getActiveSheet()->setCellValue("R".$i, "Status Pernikahan");
		$excel->getActiveSheet()->setCellValue("S".$i, "Jenis Kelamin");
		$excel->getActiveSheet()->setCellValue("T".$i, "Tinggi");
		$excel->getActiveSheet()->setCellValue("U".$i, "Berat");
		$excel->getActiveSheet()->setCellValue("V".$i, "Pendidikan Terakhir");
		$excel->getActiveSheet()->setCellValue("W".$i, "Jurusan");
		$excel->getActiveSheet()->setCellValue("X".$i, "Sakit Ringan");
		$excel->getActiveSheet()->setCellValue("Y".$i, "Sakit Serius");
		$excel->getActiveSheet()->setCellValue("Z".$i, "Rawat RS");
		$excel->getActiveSheet()->setCellValue("AA".$i, "Tindak Kriminal");
		$excel->getActiveSheet()->setCellValue("AB".$i, "No Rekening");
		$excel->getActiveSheet()->setCellValue("AC".$i, "Atas Nama");
		$excel->getActiveSheet()->setCellValue("AD".$i, "Bersedia Membuka Rekening");
		$excel->getActiveSheet()->setCellValue("AE".$i, "NPWP");
		$excel->getActiveSheet()->setCellValue("AF".$i, "Memiliki BPJS Kesehatan?");
		$excel->getActiveSheet()->setCellValue("AG".$i, "Keterangan BPJSKS");
		$excel->getActiveSheet()->setCellValue("AH".$i, "Bersedia Mengikuti BPJSKS Perusahaan");
		$excel->getActiveSheet()->setCellValue("AI".$i, "Surat Lamaran Pekerjaan");
		$excel->getActiveSheet()->setCellValue("AJ".$i, "Lampiran Riwayat Hidup");
		$excel->getActiveSheet()->setCellValue("AK".$i, "Foto KTP");
		$excel->getActiveSheet()->setCellValue("AL".$i, "Foto KK");
		$excel->getActiveSheet()->setCellValue("AM".$i, "Foto");
		$excel->getActiveSheet()->setCellValue("AN".$i, "Surat Sehat");
		$excel->getActiveSheet()->setCellValue("AO".$i, "Foto Buku Nikah");
		$excel->getActiveSheet()->setCellValue("AP".$i, "SKCK");
		$excel->getActiveSheet()->setCellValue("AQ".$i, "FOTO NPWP");
		$excel->getActiveSheet()->setCellValue("AR".$i, "Rekomendasi");
		$excel->getActiveSheet()->setCellValue("AS".$i, "Pilihan Kerja");
		$excel->getActiveSheet()->setCellValue("AT".$i, "Keahlian/sertifikat:nomor:url Dokumen,");
		$excel->getActiveSheet()->setCellValue("AU".$i, "Pendidikan:mulai:Selesai,");
		$excel->getActiveSheet()->setCellValue("AV".$i, "Bahasa:Lisan:Tulisan,");
		$excel->getActiveSheet()->setCellValue("AW".$i, "Riwayat Pekerjaan:Posisi:Periode:Gaji:Alamat:Alasan Resign,");
		$excel->getActiveSheet()->setCellValue("AX".$i, "Kontak Referensi:Nama Perusahaan:Posisi:Nomor HP,");
		$excel->getActiveSheet()->setCellValue("AY".$i, "Kontak Darurat:hubungan:Nomor Telepon:Alamat,");

		$params['columns'] 		= "A.*, B.name AS city_domisili, G.name AS city_birth, H.name AS city_card, CONCAT_WS(', ', D.name, E.name, F.name) AS preference_job";

		$list_candidate = $this->candidate_model->gets($params);
		$i=2;
		foreach ($list_candidate as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->registration_number);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->id_card_period);
			$excel->getActiveSheet()->setCellValue("E".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_birth));

			$date = new DateTime($item->date_birth);
			$excel->getActiveSheet()->setCellValue("F".$i, PHPExcel_Shared_Date::PHPToExcel($date));
			$excel->getActiveSheet()->getStyle("F".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			
			// $excel->getActiveSheet()->setCellValue("F".$i, format_date_ina($item->date_birth));
			$excel->getActiveSheet()->setCellValue("G".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_card));
			$excel->getActiveSheet()->setCellValue("H".$i, $item->address_card);
			$excel->getActiveSheet()->setCellValue("I".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_domisili));
			$excel->getActiveSheet()->setCellValue("J".$i, $item->address_domisili);
			$excel->getActiveSheet()->setCellValue("K".$i, $item->status_residance);
			$excel->getActiveSheet()->setCellValueExplicit("L".$i, $item->phone_number);
			$excel->getActiveSheet()->setCellValue("M".$i, $item->email);
			$excel->getActiveSheet()->setCellValue("N".$i, $item->socmed_fb);
			$excel->getActiveSheet()->setCellValue("O".$i, $item->socmed_ig);
			$excel->getActiveSheet()->setCellValue("P".$i, $item->socmed_tw);
			$excel->getActiveSheet()->setCellValue("Q".$i, $item->religion);
			$excel->getActiveSheet()->setCellValue("R".$i, $item->marital_status);
			$excel->getActiveSheet()->setCellValue("S".$i, $item->gender);
			$excel->getActiveSheet()->setCellValue("T".$i, $item->heigh);
			$excel->getActiveSheet()->setCellValue("U".$i, $item->weigh);
			$excel->getActiveSheet()->setCellValue("V".$i, $item->education_level);
			$excel->getActiveSheet()->setCellValue("W".$i, $item->education_majors);
			$excel->getActiveSheet()->setCellValue("X".$i, $item->illness_minor);
			$excel->getActiveSheet()->setCellValue("Y".$i, $item->illness_serious);
			$excel->getActiveSheet()->setCellValue("Z".$i, $item->illness_treated);
			$excel->getActiveSheet()->setCellValue("AA".$i, $item->crime_notes);
			$excel->getActiveSheet()->setCellValue("AB".$i, $item->bank_account);
			$excel->getActiveSheet()->setCellValue("AC".$i, $item->bank_account_name);
			$excel->getActiveSheet()->setCellValue("AD".$i, $item->bank_account_create);
			$excel->getActiveSheet()->setCellValue("AE".$i, $item->tax_number);
			$excel->getActiveSheet()->setCellValue("AF".$i, $item->health_insurance);
			$excel->getActiveSheet()->setCellValue("AG".$i, $item->health_insurance_note);
			$excel->getActiveSheet()->setCellValue("AH".$i, $item->health_insurance_company);
			$excel->getActiveSheet()->setCellValue("AK".$i, $item->document_id_card);
			$excel->getActiveSheet()->setCellValue("AL".$i, $item->document_family_card);
			$excel->getActiveSheet()->setCellValue("AM".$i, $item->document_photo);
			$excel->getActiveSheet()->setCellValue("AN".$i, $item->document_doctor_note);
			$excel->getActiveSheet()->setCellValue("AO".$i, $item->document_marriage_certificate);
			$excel->getActiveSheet()->setCellValue("AP".$i, $item->document_police_certificate);
			$excel->getActiveSheet()->setCellValue("AQ".$i, $item->document_tax_number);
			$excel->getActiveSheet()->setCellValue("AR".$i, $item->recommendation);
			$excel->getActiveSheet()->setCellValue("AS".$i, $item->preference_job);

			$list_skill 	= $this->employee_skill_model->gets(array('employee_id' => $item->id, 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));
			$skill = "";
			foreach ($list_skill as $item_skill) {
				$skill .=" ".$item_skill->certificate_name.":".$item_skill->certificate_number.":".$item_skill->certificate_document.",";
			}
			
			$list_education = $this->employee_education_model->gets(array('employee_id' => $item->id, 'columns' => 'A.id, A.institute, A.start, A.end'));
			$education = "";
			foreach ($list_education as $item_education) {
				$education .=" ".$item_education->institute.":".$item_education->start.":".$item_education->end.",";
			}

			$list_language 	= $this->employee_language_model->gets(array('employee_id' => $item->id, 'columns' => 'A.id, A.language, A.verbal, A.write'));
			$language = "";
			foreach ($list_language as $item_language) {
				$language .=" ".$item_language->language.":".$item_language->verbal.":".$item_language->write.",";
			}

			$list_history 	= $this->employee_history_model->gets(array('employee_id' => $item->id, 'columns' => 'A.id, A.company_name, A.position, A.period, A.salary, A.company_address, A.resign_note'));
			$history = "";
			foreach ($list_history as $item_history) {
				$history .=" ".$item_history->company_name.":".$item_history->position.":".$item_history->period.":".$item_history->salary.":".$item_history->company_address.":".$item_history->resign_note.",";
			}

			$list_reference = $this->employee_reference_model->gets(array('employee_id' => $item->id, 'columns' => 'A.id, A.name, A.company_name, A.position, A.phone'));
			
			$reference  = "";
			foreach ($list_reference as $item_reference) {
				$reference  .=" ".$item_reference->name.":".$item_reference->company_name.":".$item_reference->position.":".$item_reference->phone.",";
			}

			$list_emergency = $this->employee_emergency_model->gets(array('employee_id' => $item->id, 'columns' => 'A.id, A.name, A.relation, A.phone, A.address'));
			
			$emergency = "";
			foreach ($list_emergency as $item_emergency) {
				$emergency .=" ".$item_emergency->name.":".$item_emergency->relation.":".$item_emergency->phone.":".$item_emergency->address.",";
			}

			$excel->getActiveSheet()->setCellValue("AT".$i, $skill);
			$excel->getActiveSheet()->setCellValue("AU".$i, $education);
			$excel->getActiveSheet()->setCellValue("AV".$i, $language);
			$excel->getActiveSheet()->setCellValue("AW".$i, $history);
			$excel->getActiveSheet()->setCellValue("AX".$i, $reference);
			$excel->getActiveSheet()->setCellValue("AX".$i, $emergency);

			$i++;
		}
		$excel->getActiveSheet()->setTitle($title);
		
		$excel->createSheet();
		$excel->setActiveSheetIndex(1);
		$excel->getActiveSheet()->setCellValue('A1', 'ID Kota');
		$excel->getActiveSheet()->setCellValue('B1', 'Nama Kota');

		$list_city	= $this->city_model->gets(array('orderby' => 'A.name', 'order' => 'ASC'));
		$j=2;
		foreach ($list_city as $item) {
			$excel->getActiveSheet()->setCellValue("A".$j,$item->id);
			$excel->getActiveSheet()->setCellValue("B".$j,$item->name);
			$j++;
		}
		$excel->getActiveSheet()->setTitle('Daftar Kota');

		$excel->createSheet();
		$excel->setActiveSheetIndex(2);
		$excel->getActiveSheet()->setCellValue('A1', 'ID Site');
		$excel->getActiveSheet()->setCellValue('B1', 'Nama Site');

		$list_site 	= $this->site_model->gets(array('bank'=>TRUE, 'orderby' => 'A.name', 'order' => 'ASC'));
		$k=2;
		foreach ($list_site as $item) {
			$excel->getActiveSheet()->setCellValue("A".$k,$item->id);
			$excel->getActiveSheet()->setCellValue("B".$k,$item->name);
			$k++;
		}
		$excel->getActiveSheet()->setTitle('Daftar Site');
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function import($type = FALSE){
		if(!$_FILES['file']['name'] == ""){
			$this->load->model(array('employee_model', 'site_model', 'city_model', 'branch_model', 'position_model', 'company_model', 'approval_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {
				$data['id'] 					= '';
				$data['registration_number']	= '';
				$data['status']					= 0;
				$data['status_approval']		= 2;
				$data['status_nonjob']			= 0;
				$data['approved_at']			= date('Y-m-d H:i:s');
				$data['resign_submit'] 			= null;
				
				$data['id_card'] 				= replace_null($sheet->getCell('C'.$row)->getValue());
				if($data['id_card'] == ''){
					continue;
				}

				$candidate = $this->employee_model->get(array('id_card' => $data['id_card'], 'columns' => 'A.id, A.registration_number, A.status'));
				if($candidate){
					if($candidate->status <= 2){
						$data['id']					= $candidate->id;
						$data['registration_number']= $candidate->registration_number;
					}
				}

				$data['full_name'] 				= replace_null($sheet->getCell('B'.$row)->getValue());
				$city_birth 					= $this->city_model->get(array('name' => replace_null($sheet->getCell('D'.$row)->getValue()), 'columns' => 'A.id'));
				if($city_birth){
					$data['city_birth_id'] 	= $data['city_card_id'] = $data['city_domisili_id'] = $city_birth->id;
				}

				$excel_date = $sheet->getCell('E'.$row)->getValue();
				$unix_date = ($excel_date - 25569) * 86400;
				$excel_date = 25569 + ($unix_date / 86400);
				$unix_date = ($excel_date - 25569) * 86400;
				$data['date_birth']		= gmdate("Y-m-d", $unix_date);
				$data['address_card']	= $data['address_domisili'] = replace_null($sheet->getCell('F'.$row)->getValue());
				
				$data['site_id'] = 1;
				$site 	= $this->site_model->get(array('name' => replace_null($sheet->getCell('G'.$row)->getValue()), 'columns' => 'A.id'));
				if($site){
					$data['site_id']			= $site->id;
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Site '.replace_null($sheet->getCell('G'.$row)->getValue())." belum terdaftar.",'danger'));
					redirect(base_url('submission/personalia'));
				}
				
				$data['position_id'] = 0;
				$position 	= $this->position_model->get(array('name' => replace_null($sheet->getCell('H'.$row)->getValue()), 'columns' => 'A.id'));
				if($position){
					$data['position_id'] 			= $position->id;
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Posisi/Jabatan '.replace_null($sheet->getCell('H'.$row)->getValue())." belum terdaftar.",'danger'));
					redirect(base_url('submission/personalia'));
				}
				
				$data['company_id'] = 1;
				$company 	= $this->company_model->get(array('code' => replace_null(strtoupper($sheet->getCell('I'.$row)->getValue())), 'columns' => 'A.id'));
				if($company){
					$data['company_id'] 			= $company->id;
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Unit Kontrak '.replace_null($sheet->getCell('I'.$row)->getValue())." belum terdaftar.",'danger'));
					redirect(base_url('submission/personalia'));
				}

				$branch 	= $this->branch_model->get(array('name' => replace_null($sheet->getCell('J'.$row)->getValue()), 'columns' => 'A.id'));
				if($branch){
					$data['branch_id'] 			= $branch->id;
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Branch '.replace_null($sheet->getCell('J'.$row)->getValue())." belum terdaftar.",'danger'));
					redirect(base_url('submission/personalia'));
				}
				$data['employee_number'] 		= replace_null($sheet->getCell('K'.$row)->getValue());
				if($data['employee_number'] == ''){
					$employee_number = $this->employee_number($data['company_id']);
				}

				$excel_date = $sheet->getCell('L'.$row)->getValue();
				$unix_date = ($excel_date - 25569) * 86400;
				$excel_date = 25569 + ($unix_date / 86400);
				$unix_date = ($excel_date - 25569) * 86400;
				$data['approved_at'] = gmdate("Y-m-d", $unix_date)." 00:00:00";

				$save_id = $this->employee_model->save($data);
				if ($save_id) {
					if($data['registration_number'] == ''){
						$registration_number 		= 10000+$save_id;
						$this->employee_model->save(array('id' => $save_id, 'registration_number' => $registration_number));
					}

					$approval = array('id' => '', 'employee_id' => $save_id , 'site_id' => $data['site_id'] , 'company_id' => $data['company_id'], 'position_id' => $data['position_id'], 'note' => 'import karyawan', 'status_approval' => 'Pengajuan ke Personalia');
					$this->approval_model->save($approval);
				}
			}

			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			redirect(base_url('submission/personalia'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
			redirect(base_url('submission/personalia'));
		}
	}

	private function employee_number($site_id){
		$employee_number = "";
		$this->load->model('employee_model');

		$employee_number = $site_id.date('ym');

		$site = $this->employee_model->last_employee_number(array('prefix' => $employee_number));

		$inc = 1;
		if($site){
			// $inc = intval(substr($site->employee_number, 13, 5))+1;
			$inc = intval(substr($site->employee_number, 5, 5))+1;
		}		
		$space = "";
		for ($i=strlen($inc); $i <5; $i++) {
			$space .= "0";
		}

		$employee_number = $employee_number.$space.$inc;		
		return $employee_number;
	}

	public function edit($employee_id = FALSE)
	{
		$this->load->model(array('city_model', 'province_model', 'preference_model', 'candidate_model','employee_education_model', 'employee_emergency_model', 'employee_history_model', 'employee_language_model', 'employee_reference_model', 'employee_skill_model', 'auth_model', 'email_model'));
		
		$data['id']								= $employee_id;
		$data['registration_number']			= '';
		$data['full_name']						= '';
		$data['id_card']						= '';
		$data['id_card_period']					= 'SEUMUR HIDUP';
		$data['city_birth_id']					= '';
		$data['date_birth']						= '';
		$data['city_card_id']					= '';
		$data['address_card']					= '';
		$data['city_domisili_id'] 				= '';
		$data['address_domisili'] 				= '';
		$data['status_residance'] 				= '';
		$data['phone_number'] 					= '';
		$data['email'] 							= '';
		$data['socmed_fb'] 						= '';
		$data['socmed_ig'] 						= '';
		$data['socmed_tw'] 						= '';
		$data['religion'] 						= '';
		$data['marital_status']					= '';
		$data['gender'] 						= '';
		$data['heigh'] 							= '';
		$data['weigh'] 							= '';
		$data['clothing_size'] 					= '';
		$data['shoe_size'] 						= '';
		$data['education_level']	 			= '';
		$data['education_majors'] 				= '';
		$data['illness_minor'] 					= '';
		$data['illness_serious'] 				= '';
		$data['illness_treated'] 				= '';
		$data['crime_notes'] 					= '';
		$data['bank_account'] 					= '';
		$data['bank_account_name'] 				= '';
		$data['bank_account_create'] 			= '';
		$data['tax_number'] 					= '';
		$data['health_insurance'] 				= '';
		$data['health_insurance_note'] 			= '';
		$data['health_insurance_company'] 		= '';
		$data['preference_job1'] 				= '';
		$data['preference_job2'] 				= '';
		$data['preference_job3'] 				= '';
		$data['preference_placement']			= '';
		$data['recommendation'] 				= '';
		$data['blood_group'] 					= '';
		$data['covid_vaccine'] 					= '';
		$data['family_mother'] 					= '';

		$data['document_photo']					= '';
		$data['document_id_card']				= '';
		$data['document_marriage_certificate']	= '';
		$data['document_tax_number']			= '';
		$data['document_family_card']			= '';
		$data['document_doctor_note']			= '';
		$data['document_police_certificate']	= '';
		$data['document_certificate_education']	= '';
		$data['document_cv']					= '';

		$data['period_doctor_note']				= '';

		$data['status']							= 0;
		$data['status_approval']				= 0;
		$data['status_completeness']			= 0;

		if($this->input->post()){
			$this->form_validation->set_rules('full_name', 'Nama Lengkap', 'required');
			$this->form_validation->set_rules('id_card', 'Nomor KTP', 'required|numeric|min_length[16]|max_length[18]');

			$data['id_card']			= $params['id_card']			= $this->input->post('id_card');
			$result = (array) $this->candidate_model->get(array('id_card' => $data['id_card']));
			if(!empty($result)){
				$data = $result;
			}
			
			if ($this->form_validation->run() == FALSE){
				$data['message']= message_box('<strong>Gagal!</strong> Silahkan lengkapi data anda sesuai dengan ketentuan.','danger');
			}else{
				$data['full_name']			= $params['full_name']			= $this->input->post('full_name');
				$data['id_card']			= $params['id_card']			= $this->input->post('id_card');
				$data['city_birth_id']		= $params['city_birth_id'] 		= $this->input->post('city_birth_id');
				$data['date_birth']			= $params['date_birth'] 		= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_birth'))));
				$data['city_card_id']		= $params['city_card_id'] 		= $this->input->post('city_card_id');
				$data['address_card']		= $params['address_card'] 		= $this->input->post('address_card');
				$data['city_domisili_id']	= $params['city_domisili_id'] 	= $this->input->post('city_domisili_id');
				$data['address_domisili']	= $params['address_domisili'] 	= $this->input->post('address_domisili');
				if($data['city_domisili_id'] == ''){
					$data['city_domisili_id']	= $params['city_domisili_id'] = $data['city_card_id'];
				}
				if($data['address_domisili'] == ''){
					$data['address_domisili']	= $params['address_domisili'] = $data['address_card'];
				}

				$data['status_residance']	= $params['status_residance'] 	= strtolower($this->input->post('status_residance'));
				$data['phone_number']		= $params['phone_number'] 		= $this->input->post('phone_number');
				$data['email']				= $params['email'] 				= $this->input->post('email');
				$data['socmed_fb']			= $params['socmed_fb'] 			= $this->input->post('socmed_fb');
				$data['socmed_ig']			= $params['socmed_ig'] 			= $this->input->post('socmed_ig');
				$data['socmed_tw']			= $params['socmed_tw'] 			= $this->input->post('socmed_tw');
				$data['religion']			= $params['religion'] 			= $this->input->post('religion');
				$data['marital_status']		= $params['marital_status'] 	= $this->input->post('marital_status');
				$data['gender']				= $params['gender'] 			= $this->input->post('gender');
				$data['heigh']				= $params['heigh'] 				= $this->input->post('heigh');
				$data['weigh']				= $params['weigh'] 				= $this->input->post('weigh');
				$data['clothing_size']		= $params['clothing_size'] 		= $this->input->post('clothing_size');
				$data['shoe_size']			= $params['shoe_size'] 			= $this->input->post('shoe_size');
				$data['education_level']	= $params['education_level'] 	= $this->input->post('education_level');
				$data['education_majors']	= $params['education_majors'] 	= strtoupper($this->input->post('education_majors'));

				$data['illness_minor']		= $params['illness_minor'] 		= $this->input->post('illness_minor');
				$data['illness_serious']	= $params['illness_serious'] 	= $this->input->post('illness_serious');
				$data['illness_treated']	= $params['illness_treated'] 	= $this->input->post('illness_treated');

				$data['crime_notes']				= $params['crime_notes'] 				= $this->input->post('crime_notes');
				$data['bank_account_create']		= $params['bank_account_create']		= $this->input->post('bank_account_create');
				$data['bank_account']				= $params['bank_account'] 				= $this->input->post('bank_account');
				$data['bank_account_name']			= $params['bank_account_name'] 			= $this->input->post('bank_account_name');
				$data['tax_number']					= $params['tax_number'] 				= $this->input->post('tax_number');
				$data['health_insurance']			= $params['health_insurance'] 			= $this->input->post('health_insurance');
				$data['health_insurance_note']		= $params['health_insurance_note'] 		= $this->input->post('health_insurance_note');
				$data['health_insurance_company']	= $params['health_insurance_company'] 	= $this->input->post('health_insurance_company');
				$data['recommendation']				= $params['recommendation'] 			= $this->input->post('recommendation');
				$data['blood_group']				= $params['blood_group'] 				= $this->input->post('blood_group');
				$data['covid_vaccine']				= $params['covid_vaccine'] 				= $this->input->post('covid_vaccine');
				$data['family_mother']				= $params['family_mother'] 				= $this->input->post('family_mother');
				
				$data['preference_placement']		= $params['preference_placement'] 		= $this->input->post('preference_placement');
				if($data['preference_placement'] == ''){
					$data['preference_placement']	= $params['preference_placement'] 		= $data['city_domisili_id'];
				}
				

				$data['period_doctor_note']			= $params['period_doctor_note'] 	= $this->input->post('period_doctor_note');

				$preference_job 					= $this->input->post('preference_job');
				if(isset($preference_job[0])){
					$data['preference_job1'] = $params['preference_job1'] = $preference_job[0];
				}
				if(isset($preference_job[1])){
					$data['preference_job2'] = $params['preference_job2'] = $preference_job[1];
				}
				if(isset($preference_job[2])){
					$data['preference_job3'] = $params['preference_job3'] = $preference_job[2];
				}

				$params['id_card_period']			= $data['id_card_period'];
				$params['status']					= $data['status'];
				$params['status_approval']			= $data['status_approval'];
				$params['status_completeness']		= 0;

				$url_document = 'files/document/'.$data['id_card'];
				$document_path = FCPATH.$url_document;
				if(!is_dir($document_path)){
					mkdir($document_path, 0755, TRUE);
				}

				$config['upload_path'] 	= $document_path;
				$config['overwrite']  	= TRUE;
				$config['allowed_types']= 'jpg|jpeg|png';
				
				if($_FILES['document_photo']['name'] != ""){
					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'foto';
					$this->load->library('upload', $config, 'document_photo');

					if (!$this->document_photo->do_upload('document_photo')) {
						$data['message']= message_box('<strong>Gagal!</strong> '.$this->document_photo->display_errors().'.','danger');
					} else {
						$document 	= $this->document_photo->data();
						$params['document_photo'] = $data['document_photo'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_id_card']['name'] != ""){

					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'ktp';
					$this->load->library('upload', $config, 'document_id_card');

					if (!$this->document_id_card->do_upload('document_id_card')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_id_card->display_errors().'.','danger');
					} else {
						$document = $this->document_id_card->data();
						$params['document_id_card'] =	$data['document_id_card'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_marriage_certificate']['name'] != ""){

					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'buku-nikah';
					$this->load->library('upload', $config, 'document_marriage_certificate');

					if (!$this->document_marriage_certificate->do_upload('document_marriage_certificate')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_marriage_certificate->display_errors().'.','danger');
					} else {
						$document = $this->document_marriage_certificate->data();
						$params['document_marriage_certificate'] =	$data['document_marriage_certificate'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_tax_number']['name'] != ""){

					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'npwp';
					$this->load->library('upload', $config, 'document_tax_number');

					if (!$this->document_tax_number->do_upload('document_tax_number')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_tax_number->display_errors().'.','danger');
					} else {
						$document = $this->document_tax_number->data();
						$params['document_tax_number'] =	$data['document_tax_number'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_family_card']['name'] != ""){

					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'kartu-keluarga';
					$this->load->library('upload', $config, 'document_family_card');

					if (!$this->document_family_card->do_upload('document_family_card')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_family_card->display_errors().'.','danger');
					} else {
						$document = $this->document_family_card->data();
						$params['document_family_card'] =	$data['document_family_card'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_doctor_note']['name'] != ""){

					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'surat-sehat';
					$this->load->library('upload', $config, 'document_doctor_note');

					if (!$this->document_doctor_note->do_upload('document_doctor_note')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_doctor_note->display_errors().'.','danger');
					} else {
						$document = $this->document_doctor_note->data();
						$params['document_doctor_note'] =	$data['document_doctor_note'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_police_certificate']['name'] != ""){

					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'skck';
					$this->load->library('upload', $config, 'document_police_certificate');

					if (!$this->document_police_certificate->do_upload('document_police_certificate')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_police_certificate->display_errors().'.','danger');
					} else {
						$document = $this->document_police_certificate->data();
						$params['document_police_certificate'] =	$data['document_police_certificate'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_certificate_education']['name'] != ""){

					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'skck';
					$this->load->library('upload', $config, 'document_certificate_education');

					if (!$this->document_certificate_education->do_upload('document_certificate_education')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_certificate_education->display_errors().'.','danger');
					} else {
						$document = $this->document_certificate_education->data();
						$params['document_certificate_education'] =	$data['document_certificate_education'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_cv']['name'] != ""){

					$config['allowed_types']= 'pdf';
					$config['file_name'] 	= 'curriculum_vitae';
					$this->load->library('upload', $config, 'document_cv');

					if (!$this->document_cv->do_upload('document_cv')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_cv->display_errors().'.','danger');
					} else {
						$document = $this->document_cv->data();
						$params['document_cv'] =	$data['document_cv'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($data['id'] == ''){
					$params['id'] 		= '';
					$params['site_id'] 	= 1;
					$city 				= $this->city_model->get(array('id' => $data['city_card_id'], 'columns' => 'A.province_id'));
					$province 			= $this->province_model->get(array('id' => $city->province_id, 'columns' => 'A.branch_id'));
					$params['branch_id']= $province->branch_id;
				}else{
					$params['id'] 		= $data['id'];
				}

				if($this->session->userdata('is_employee')){
					unset($params['full_name']);
					unset($params['id_card']);
					unset($params['bank_account']);
				}
				$save_id = $this->candidate_model->save($params);
				if ($save_id) {
					$this->session->set_userdata('name',$params['full_name']);
					$this->auth_model->save(array('id' => $employee_id, 'username' => $params['id_card'], 'full_name' => $params['full_name'], 'email' => $params['email']));
					if($data['registration_number'] == ''){
						$registration_number 		= 10000+$save_id;
						$this->candidate_model->save(array('id' => $save_id, 'registration_number' => $registration_number));
					}

					$this->session->set_userdata('employee_id',$save_id);
					$this->auth_model->save(array('id' => $employee_id, 'employee_id' => $save_id));

					$data['id'] = $save_id;

					$emergency_contact_id		= $this->input->post('emergency_contact_id');
					$emergency_contact_name		= $this->input->post('emergency_contact_name');
					$emergency_contact_relation	= $this->input->post('emergency_contact_relation');
					$emergency_contact_phone	= $this->input->post('emergency_contact_phone');
					$emergency_contact_address	= $this->input->post('emergency_contact_address');

					foreach ($emergency_contact_id as $key => $value ) {
						if($emergency_contact_name[$key] == ''){
							continue;
						}
						$params_emergency['employee_id']= $data['id'];
						$params_emergency['id']			= $emergency_contact_id[$key];
						$params_emergency['name']		= $emergency_contact_name[$key];
						$params_emergency['relation']	= $emergency_contact_relation[$key];
						$params_emergency['phone']		= $emergency_contact_phone[$key];
						$params_emergency['address']	= $emergency_contact_address[$key];
						$this->employee_emergency_model->save($params_emergency);
					}

					$education_id				= $this->input->post('education_id');
					$education					= $this->input->post('education');
					$education_start			= $this->input->post('education_start');
					$education_end				= $this->input->post('education_end');

					foreach ($education_id as $key => $value ) {
						if($education[$key] == ''){
							continue;
						}
						$params_education['employee_id']= $data['id'];
						$params_education['id']			= $education_id[$key];
						$params_education['institute']	= $education[$key];
						$params_education['start']		= $education_start[$key];
						$params_education['end']		= $education_end[$key];
						$this->employee_education_model->save($params_education);
					}

					$languange_id				= $this->input->post('languange_id');
					$languange					= $this->input->post('languange');
					$languange_verbal			= $this->input->post('languange_verbal');
					$languange_write			= $this->input->post('languange_write');

					foreach ($languange_id as $key => $value ) {
						if($languange[$key] == ''){
							continue;
						}
						$params_languange['employee_id']= $data['id'];
						$params_languange['id']			= $languange_id[$key];
						$params_languange['language']	= $languange[$key];
						$params_languange['verbal']		= $languange_verbal[$key];
						$params_languange['write']		= $languange_write[$key];
						$this->employee_language_model->save($params_languange);
					}

					$skill_id				= $this->input->post('skill_id');
					$skill					= $this->input->post('skill');
					$certificate_number		= $this->input->post('certificate_number');
					$skill_certificate		= $this->input->post('skill_certificate');

					$files = $_FILES;
					foreach ($skill_id as $key => $value ) {
						if($skill[$key] == ''){
							continue;
						}
						$params_skill['employee_id']		= $data['id'];
						$params_skill['id']					= $skill_id[$key];
						$params_skill['certificate_name']	= $skill[$key];
						$params_skill['certificate_number']	= $certificate_number[$key];

						$temp_skill_id	=	$this->employee_skill_model->save($params_skill);
						
						if($files['skill_certificate']['name'][$key] == ''){
							continue;
						}
						
						$_FILES['skill_certificate']['name']	= $files['skill_certificate']['name'][$key];
				        $_FILES['skill_certificate']['type']	= $files['skill_certificate']['type'][$key];
				        $_FILES['skill_certificate']['tmp_name']= $files['skill_certificate']['tmp_name'][$key];
				        $_FILES['skill_certificate']['error']	= $files['skill_certificate']['error'][$key];
				        $_FILES['skill_certificate']['size']	= $files['skill_certificate']['size'][$key];  

						$this->load->library('upload');
						$config['upload_path'] 	= $document_path;
						$config['overwrite']  	= TRUE;
						$config['allowed_types']= 'jpg|jpeg|png';
						$config['file_name'] 	= 'certificate-'.$temp_skill_id;
						$this->upload->initialize($config);

						if($this->upload->do_upload('skill_certificate')){
							$document = $this->upload->data();	
							$params_skill['certificate_document'] = base_url($url_document.'/'.$document['file_name']);
							$params_skill['id']	= $temp_skill_id;
							$this->employee_skill_model->save($params_skill);
						}
					}

					$history_id					= $this->input->post('history_id');
					$history_company			= $this->input->post('history_company');
					$history_company_position	= $this->input->post('history_company_position');
					$history_company_period		= $this->input->post('history_company_period');
					$history_company_salary		= $this->input->post('history_company_salary');
					$history_company_address	= $this->input->post('history_company_address');
					$history_company_resign_note= $this->input->post('history_company_resign_note');

					foreach ($history_id as $key => $value ) {
						if($history_company[$key] == ''){
							continue;
						}
						$params_history['employee_id']	= $data['id'];
						$params_history['id']			= $history_id[$key];
						$params_history['company_name']	= $history_company[$key];
						$params_history['position']		= $history_company_position[$key];
						$params_history['period']		= $history_company_period[$key];
						$params_history['salary']		= $history_company_salary[$key];
						$params_history['company_address']	= $history_company_address[$key];
						$params_history['resign_note']	= $history_company_resign_note[$key];
						$this->employee_history_model->save($params_history);
					}

					$reference_id		= $this->input->post('reference_id');
					$reference_name		= $this->input->post('reference_name');
					$reference_company	= $this->input->post('reference_company');
					$reference_position	= $this->input->post('reference_position');
					$reference_phone	= $this->input->post('reference_phone');

					foreach ($reference_id as $key => $value ) {
						if($reference_name[$key] == ''){
							continue;
						}
						$params_reference['employee_id']	= $data['id'];
						$params_reference['id']				= $reference_id[$key];
						$params_reference['name']			= $reference_name[$key];
						$params_reference['company_name']	= $reference_company[$key];
						$params_reference['position']		= $reference_position[$key];
						$params_reference['phone']			= $reference_phone[$key];
						$this->employee_reference_model->save($params_reference);
					}

					$data_candidate =  $this->candidate_model->get(array('id' => $data['id'], 'columns' => 'A.document_family_card, A.document_photo, A.document_id_card, A.document_certificate_education'));
					if($data_candidate->document_family_card != '' && $data_candidate->document_photo != ''&& $data_candidate->document_id_card != '' && $data_candidate->document_certificate_education != ''){
						$this->candidate_model->save(array('id' => $data['id'], 'status_completeness' => 1));
					}
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
					redirect(base_url('submission/edit/'.$data['id']));
				}else{
					$data['message'] = message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger');
				}
			}
		}else{
			$result = (array) $this->candidate_model->get(array('id' => $data['id']));
			if(!empty($result)){
				$data = $result;
			}else{
				$data['email'] = $this->session->userdata('email');
			}
		}

		$data['date_birth'] = date("d/m/Y", strtotime($data['date_birth']));
		$data['list_education'] = $this->employee_education_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.institute, A.start, A.end'));
		$data['list_emergency'] = $this->employee_emergency_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.relation, A.phone, A.address'));
		$data['list_history'] 	= $this->employee_history_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.company_name, A.position, A.period,  A.salary, A.company_address, A.resign_note'));
		$data['list_language'] 	= $this->employee_language_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.language, A.verbal, A.write'));
		$data['list_reference'] = $this->employee_reference_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.company_name, A.position, A.phone'));
		$data['list_skill'] 	= $this->employee_skill_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));

		$data['_TITLE_']		= 'Form Pelamar';
		$data['_PAGE_'] 		= 'submission/form';
		$data['_MENU_PARENT_'] 	= 'recruitment_direct';
		$data['_MENU_'] 		= 'personalia';

		$data['list_city'] 		= $this->city_model->gets(array('columns' => 'A.id, A.name, B.name AS province_name'));
		$data['list_job'] 		= $this->preference_model->gets(array('columns' => 'A.id, A.name'));
		$this->view($data);
	}
}
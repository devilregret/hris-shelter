<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Vacation extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function site(){
		$data['_TITLE_'] 		= 'Daftar Site Bisnis';
		$data['_PAGE_'] 		= 'vacation/site';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'vacation';
		$this->view($data);
	}

	public function site_ajax(){

		$this->load->model(array('site_model'));

		$column_index = $_POST['order'][0]['column']; 
		$params['columns'] 		= "A.id AS site_id, A.code AS site_code, A.name AS site_name, A.vacation, B.code AS company_code, C.name AS branch_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];

		$params['company_code']		= $_POST['columns'][1]['search']['value'];
		$params['site_code']		= $_POST['columns'][2]['search']['value'];
		$params['site_name']		= $_POST['columns'][3]['search']['value'];
		$params['branch_name']		= $_POST['columns'][4]['search']['value'];
		$params['vacation']			= $_POST['columns'][5]['search']['value'];

		if(in_array($this->session->userdata('role'), array(16,17,18,19,20,21))){
			$params['indirect'] = TRUE;
		}

		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		
		$list 	= $this->site_model->gets_ro_site($params);
		$total 	= $this->site_model->gets_ro_site($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['no'] 				= $i;
			$result['company_code']		= $item->company_code;
			$result['site_code']		= $item->site_code;
			$result['site_name']		= $item->site_name;
			$result['branch_name']		= $item->branch_name;
			$result['vacation']			= $item->vacation;
			$result['action'] 			= 
				'<a class="btn-sm btn-success btn-block" href="'.base_url("vacation/site_form/".$item->site_id).'">Ubah</a>
				<a class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" href="'. base_url("vacation/list/".$item->site_id).'">Detail</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function site_form($site_id = FALSE)
	{
		$this->load->model(array('site_model'));
		if($this->input->post()){
			$insert['id'] 			= $this->input->post('id');
			$insert['vacation']		= $this->input->post('vacation');
			
			$save_id	 	= $this->site_model->save($insert);			
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			redirect(base_url('vacation/site'));
		}
		
		$site 					= $this->site_model->gets(array('id' => $site_id, 'columns' => 'A.id, A.vacation, A.code AS site_code, A.name AS site_name, A.address AS site_address, C.code AS company_code', 'indirect' => true));
		if($site){
			$data = (array) $site[0];
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> site bisnis tidak ditemukan.','danger'));
			redirect(base_url('vacation/site'));
		}
		$data['_TITLE_'] 		= 'Form Cuti Site';
		$data['_PAGE_'] 		= 'vacation/site_form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'vacation';
		return $this->view($data);
	}

	public function list($site_id = FALSE){
		$this->load->model(array('site_model'));
		$data['site'] 			= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.id, A.name, A.address'));
		$data['_TITLE_'] 		= 'Daftar Karyawan';
		$data['_PAGE_'] 		= 'vacation/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'vacation';
		$this->view($data);
	}

	public function list_ajax($site_id = FALSE){
		$this->load->model(array('vacation_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.full_name, C.vacation";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];

		$params['employee_number']	= $_POST['columns'][1]['search']['value'];
		$params['id_card']			= $_POST['columns'][2]['search']['value'];
		$params['full_name']		= $_POST['columns'][3]['search']['value'];
		$params['vacation']			= $_POST['columns'][4]['search']['value'];
		$params['site_id']			= $site_id;

		$list 	= $this->vacation_model->gets($params);
		$total 	= $this->vacation_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['no'] 				= $i;
			$result['employee_number']	= $item->employee_number;
			$result['id_card']			= $item->id_card;
			$result['full_name']		= $item->full_name;
			$result['vacation']			= (int) $item->vacation;
			$result['action'] 			= 
				'<a class="btn-sm btn-success btn-action btn-block"  style="cursor:pointer;" href="'. base_url("vacation/detail_form/".$item->id).'">Ubah</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function detail_form($employee_id = FALSE)
	{
		$this->load->model(array('vacation_model'));
		if($this->input->post()){
			$insert['employee_id'] 	= $this->input->post('employee_id');
			$insert['vacation']		= $this->input->post('vacation');
			$insert['is_active']	= 1;
			$site_id 				= $this->input->post('site_id');
			
			$save_id	 	= $this->vacation_model->save($insert);			
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			redirect(base_url('vacation/list/'.$site_id));
		}
		
		$vacation 					= $this->vacation_model->get(array('id' => $employee_id, 'columns' => 'A.id, A.employee_number, A.site_id, A.id_card, A.full_name, B.vacation'));
		if($vacation){
			$data = (array) $vacation;
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> site bisnis tidak ditemukan.','danger'));
			redirect(base_url('vacation/site'));
		}
		$data['_TITLE_'] 		= 'Form Cuti Karyawan';
		$data['_PAGE_'] 		= 'vacation/detail_form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'vacation';
		return $this->view($data);
	}

	public function generate($site_id = FALSE)
	{
		if($site_id){
			$this->load->model(array('vacation_model', 'site_model'));
			$params['columns'] 		= "A.id, C.vacation";
			$params['site_id']		= $site_id;
			$vacation 	= $this->vacation_model->gets($params);
			$site 		= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.vacation'));
			foreach($vacation as $item)
			{			
				$this->vacation_model->save(array('employee_id' => $item->id, 'vacation' => $site->vacation, 'is_active' => 1));		
			}
			$this->session->set_flashdata('message', message_box('<strong>Berhasil!</strong> data berhasil digenerate.','success'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> site bisnis tidak ditemukan.','danger'));
		}
		redirect(base_url('vacation/list/'.$site_id));
		
	}

}
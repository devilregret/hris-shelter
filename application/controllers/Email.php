<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('recruitment'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		$role 	= array_merge($role, $this->config->item('benefit'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,4,7,5,6,11,12,13,14))){
			redirect(base_url());
		}
	}

	public function delete($email_id = false)
	{
		$this->load->model('email_model');
		if ($email_id)
		{
			$data =  $this->email_model->get(array('id' => $email_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $email_id, 'is_active' => 0);
				$result = $this->email_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('email/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('email/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('email/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('email/list'));
		}
	}

	public function preview($email_id = FALSE)
	{
		$this->load->model('email_model');
		$data['_TITLE_'] 		= 'Preview Template Email';
		$data['_PAGE_']	 		= 'email/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'email';

		$data['id'] = $email_id;

		
		if (!$email_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('email/list'));
		}

		$data['preview'] = $this->email_model->preview(array('id' => $email_id));
		$this->load->view('email/preview', $data);
	}

	public function form($email_id = FALSE)
	{
		$this->load->model(array('email_model'));

		$data['id'] 			= '';
		$data['email_sender']	= '';
		$data['name_sender']	= '';
		$data['carbon_copy']	= '';
		$data['password']		= '';
		$data['subject']		= '';
		$data['content']		= '';

		if($this->input->post()){
			$data['id'] 			= $this->input->post('id');
			$data['email_sender'] 	= $this->input->post('email_sender');
			$data['name_sender']	= $this->input->post('name_sender');
			$data['carbon_copy']	= $this->input->post('carbon_copy');
			$data['password'] 		= $this->input->post('password');
			$data['subject']		= $this->input->post('subject');
			$data['content']		= $this->input->post('content');
			
			$this->form_validation->set_rules('email_sender', '', 'required');
			$this->form_validation->set_rules('name_sender', '', 'required');
			$this->form_validation->set_rules('subject', '', 'required');
			$this->form_validation->set_rules('content', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$insert = array('id' => $data['id'], 'email_sender' => $data['email_sender'], 'name_sender' => $data['name_sender'], 'password' => $data['password'], 'carbon_copy' => $data['carbon_copy'], 'subject' => $data['subject'],'content' => $data['content']);
				
				$save_id	 	= $this->email_model->save($insert);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('email/list'));
		}

		if ($email_id)
		{
			$data = (array) $this->email_model->get(array('id' => $email_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('email/list'));
			}
		}
		$data['_TITLE_'] 		= 'Template Email';
		$data['_PAGE_'] 		= 'email/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'email';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Template Email';
		$data['_PAGE_'] 		= 'email/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'email';
		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('email_model'));

		$column_index = $_POST['order'][0]['column']; 
		if(in_array($this->session->userdata('role'), $this->config->item('recruitment'))){
			$params['type']		= 'recruitment';
		}
		if(in_array($this->session->userdata('role'), $this->config->item('personalia'))){
			$params['type']		= 'personalia';
		}
		if(in_array($this->session->userdata('role'), $this->config->item('payroll'))){
			$params['type']		= 'payroll';
		}
		if(in_array($this->session->userdata('role'), $this->config->item('benefit'))){
			$params['type']		= 'benefit';
		}
		$params['columns'] 		= 'A.id, A.email_sender, A.name_sender, A.subject';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['email_sender']	= $_POST['columns'][1]['search']['value'];
		$params['name_sender']	= $_POST['columns'][2]['search']['value'];
		$params['subject']		= $_POST['columns'][3]['search']['value'];

		$list 	= $this->email_model->gets($params);
		$total 	= $this->email_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['email_sender'] = $item->email_sender;
			$result['name_sender'] 	= $item->name_sender;
			$result['subject'] 		= $item->subject;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("email/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("email/form/".$item->id).'">Ubah</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}

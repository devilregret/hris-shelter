<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Benefit_employee extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('benefit'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function form($employee_id = FALSE)
	{
		$this->load->model(array('employee_model'));
		
		if($this->input->post()){
			$params['id'] 						= $this->input->post('id');
			$benefit_labor_note					= "";
			$labor = 0;
			foreach($this->input->post('benefit_labor_choice') AS $item){
				if($labor > 0){
					$benefit_labor_note .= ",".$item;
				}else{

					$benefit_labor_note .= $item;
				}
				$labor++;
			}
			
			$params['benefit_labor_note'] 		= $benefit_labor_note;
			$params['benefit_labor'] 			= $this->input->post('benefit_labor');
			$params['benefit_labor_company'] 	= $this->input->post('benefit_labor_company');
			$params['benefit_health_note'] 		= $this->input->post('benefit_health_note');
			$params['benefit_health'] 			= $this->input->post('benefit_health');
			$params['benefit_health_company'] 	= $this->input->post('benefit_health_company');
			if($params['benefit_labor'] 		!= '' && $params['benefit_health'] != ''){
				$params['status_benefit']		= 0;
			}
			$save_id = $this->employee_model->save($params);
			if ($save_id) {
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong>  data gagal disimpan.','danger'));
			}
			redirect(base_url('benefit_employee/list?filter=site&site='.$this->input->post('site_id')));
		}

		if ($employee_id)
		{
			$data = (array) $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.*'));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('benefit_employee/list'));
			}
		}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('benefit_employee/list'));
			
		}

		$data['_TITLE_']			= 'Karyawan';
		$data['_PAGE_']				= 'benefit_employee/form';
		$data['_MENU_PARENT_']		= 'benefit_employee';
		$data['_MENU_']				= 'benefit_employee';
		$this->view($data);
	}

	public function list(){
		$this->load->model(array('site_model', 'employee_model'));
		$data['_TITLE_'] 		= 'Karyawan - Data Lengkap';
		$data['_PAGE_'] 		= 'benefit_employee/list';
		$data['_MENU_PARENT_'] 	= 'benefit_data_employee';
		$data['_MENU_'] 		= 'benefit_employee';
		$branch_id				= '';
		if($this->session->userdata('branch') > 1){
			$branch_id 	= $this->session->userdata('branch');
		}
		
		$data['list_site']		= $this->site_model->gets(array('columns' => 'A.id, A.name, C.code AS company_code', 'indirect' => true, 'branch_id' => $branch_id));
		$data['list_npptk']		= $this->employee_model->gets_benefit_company(array('columns' => 'A.benefit_labor_company', 'groupby' => 'benefit_labor_company', 'orderby' => 'A.benefit_labor_company', 'order' => 'ASC', 'branch_id' => $branch_id));
		$data['list_nppks']		= $this->employee_model->gets_benefit_company(array('columns' => 'A.benefit_health_company', 'groupby' => 'benefit_health_company', 'orderby' => 'A.benefit_health_company', 'order' => 'ASC', 'branch_id' => $branch_id));
		
		$data['filter']			= $this->input->get('filter');
		$data['site_id']		= $this->input->get('site');
		$data['nppks']			= $this->input->get('nppks');
		$data['npptk']			= $this->input->get('npptk');
		
		$data['site']			= $this->site_model->get(array('columns' => 'A.id, A.name, A.address', 'site_id' => $data['site_id'], 'branch_id' => $branch_id));
		$data['employee']		= $this->employee_model->gets(array('site_id' => $data['site_id'] , 'status_approval' => 3, 'branch_id' => $branch_id), TRUE);
		$this->view($data);
	}

	public function list_ajax(){
		$this->load->model(array('employee_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.full_name, A.benefit_labor_note, A.benefit_labor, A.benefit_labor_company, A.benefit_health_note, A.benefit_health, A.benefit_health_company, B.name AS site_name, C.name AS company_name, A.benefit_labor_card, A.benefit_health_card";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']		= 1;
		$params['status_approval'] 		= 3;
		$params['benefit_complete'] 	= true;
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		$params['company_name']				= $_POST['columns'][1]['search']['value'];
		$params['site_name']				= $_POST['columns'][2]['search']['value'];
		$params['employee_number']			= $_POST['columns'][3]['search']['value'];
		$params['id_card']					= $_POST['columns'][4]['search']['value'];
		$params['full_name']				= $_POST['columns'][5]['search']['value'];
		$params['benefit_labor_note']		= $_POST['columns'][6]['search']['value'];
		$params['benefit_labor_company']	= $_POST['columns'][7]['search']['value'];
		$params['benefit_labor']			= $_POST['columns'][8]['search']['value'];
		$params['benefit_health_note']		= $_POST['columns'][9]['search']['value'];
		$params['benefit_health_company']	= $_POST['columns'][10]['search']['value'];
		$params['benefit_health']			= $_POST['columns'][11]['search']['value'];
		$params['site_id'] 					= $this->input->get('site');
		$params['is_labor_company']		 	= $this->input->get('npptk');
		$params['is_health_company'] 		= $this->input->get('nppks');
		$list 	= $this->employee_model->gets($params);
		$total 	= $this->employee_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 						= $i;
			$result['site_name']				= $item->site_name;
			$result['company_name']				= $item->company_name;
			$result['employee_number']			= $item->employee_number;
			$result['id_card']					= $item->id_card;
			$result['full_name']				= $item->full_name;
			$result['benefit_labor_note']		= $item->benefit_labor_note;
			$result['benefit_labor']			= $item->benefit_labor;
			$result['benefit_labor_company']	= $item->benefit_labor_company;
			$result['benefit_health_note']		= $item->benefit_health_note;
			$result['benefit_health_company']	= $item->benefit_health_company;
			$result['benefit_health']			= $item->benefit_health;
			$result['action'] 					=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("benefit_employee/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("benefit_employee/form/".$item->id).'">Ubah</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("benefit_employee/accident/".$item->id).'">Surat Kecelakaan</a>';
			if($item->benefit_labor_card){
				$result['action'] .= '<a class="btn-sm btn-warning btn-block" target="_blank" href="'.base_url($item->benefit_labor_card).'">KPJ</a> ';
			}
			if($item->benefit_health_card){
				$result['action'] .= '<a class="btn-sm btn-success btn-block" target="_blank" href="'.base_url($item->benefit_health_card).'">KIS</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function notcomplete(){
		$this->load->model(array('site_model', 'employee_model'));
		$data['_TITLE_'] 		= 'Karyawan - Data Belum Lengkap';
		$data['_PAGE_'] 		= 'benefit_employee/notcomplete';
		$data['_MENU_PARENT_'] 	= 'benefit_data_employee';
		$data['_MENU_'] 		= 'benefit_notcomplete';
		$branch_id				= '';
		if($this->session->userdata('branch') > 1){
			$branch_id 	= $this->session->userdata('branch');
		}
		
		$data['filter']			= $this->input->get('filter');
		$data['site_id']		= $this->input->get('site');
		$data['nppks']			= $this->input->get('nppks');
		$data['npptk']			= $this->input->get('npptk');
		
		$data['site']			= $this->site_model->get(array('columns' => 'A.id, A.name, A.address', 'site_id' => $data['site_id'], 'branch_id' => $branch_id));
		$data['employee']		= $this->employee_model->gets(array('site_id' => $data['site_id'] , 'status_approval' => 3, 'branch_id' => $branch_id), TRUE);
		$this->view($data);
	}

	public function notcomplete_ajax(){
		$this->load->model(array('employee_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.full_name, A.benefit_labor_note, A.benefit_labor, A.benefit_labor_company, A.benefit_health_note, A.benefit_health, A.benefit_health_company, B.name AS site_name, C.name AS company_name, A.benefit_labor_card, A.benefit_health_card";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']		= 1;
		$params['status_approval'] 		= 3;
		$params['benefit_not_complete'] = true;
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		$params['company_name']				= $_POST['columns'][1]['search']['value'];
		$params['site_name']				= $_POST['columns'][2]['search']['value'];
		$params['employee_number']			= $_POST['columns'][3]['search']['value'];
		$params['id_card']					= $_POST['columns'][4]['search']['value'];
		$params['full_name']				= $_POST['columns'][5]['search']['value'];
		$params['benefit_labor_note']		= $_POST['columns'][6]['search']['value'];
		$params['benefit_labor_company']	= $_POST['columns'][7]['search']['value'];
		$params['benefit_labor']			= $_POST['columns'][8]['search']['value'];
		$params['benefit_health_note']		= $_POST['columns'][9]['search']['value'];
		$params['benefit_health_company']	= $_POST['columns'][10]['search']['value'];
		$params['benefit_health']			= $_POST['columns'][11]['search']['value'];
		$params['site_id'] 					= $this->input->get('site');
		$params['is_labor_company']		 	= $this->input->get('npptk');
		$params['is_health_company'] 		= $this->input->get('nppks');
		$list 	= $this->employee_model->gets($params);
		$total 	= $this->employee_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 						= $i;
			$result['site_name']				= $item->site_name;
			$result['company_name']				= $item->company_name;
			$result['employee_number']			= $item->employee_number;
			$result['id_card']					= $item->id_card;
			$result['full_name']				= $item->full_name;
			$result['benefit_labor_note']		= $item->benefit_labor_note;
			$result['benefit_labor']			= $item->benefit_labor;
			$result['benefit_labor_company']	= $item->benefit_labor_company;
			$result['benefit_health_note']		= $item->benefit_health_note;
			$result['benefit_health_company']	= $item->benefit_health_company;
			$result['benefit_health']			= $item->benefit_health;
			$result['action'] 					=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("benefit_employee/preview/".$item->id).'">Preview</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("benefit_employee/form/".$item->id).'">Ubah</a>';
			if($item->benefit_labor_card){
				$result['action'] .= '<a class="btn-sm btn-warning btn-block" target="_blank" href="'.base_url($item->benefit_labor_card).'">KIS</a> ';
			}
			if($item->benefit_health_card){
				$result['action'] .= '<a class="btn-sm btn-success btn-block" target="_blank" href="'.base_url($item->benefit_health_card).'">KJP</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function approval(){
		$this->load->model(array('site_model', 'employee_model'));
		$data['_TITLE_'] 		= 'Karyawan - Pengajuan Pendaftaran';
		$data['_PAGE_'] 		= 'benefit_employee/approval';
		$data['_MENU_PARENT_'] 	= 'benefit_data_employee';
		$data['_MENU_'] 		= 'benefit_approval';
		$branch_id				= '';
		if($this->session->userdata('branch') > 1){
			$branch_id 	= $this->session->userdata('branch');
		}
		
		$data['filter']			= $this->input->get('filter');
		$data['site_id']		= $this->input->get('site');
		$data['nppks']			= $this->input->get('nppks');
		$data['npptk']			= $this->input->get('npptk');
		
		$data['site']			= $this->site_model->get(array('columns' => 'A.id, A.name, A.address', 'site_id' => $data['site_id'], 'branch_id' => $branch_id));
		$data['employee']		= $this->employee_model->gets(array('site_id' => $data['site_id'] , 'status_approval' => 3, 'branch_id' => $branch_id), TRUE);
		$this->view($data);
	}

	public function approval_ajax(){
		$this->load->model(array('employee_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.full_name, A.benefit_labor_note, A.benefit_labor, A.benefit_labor_company, A.benefit_health_note, A.benefit_health, A.benefit_health_company, B.name AS site_name, C.name AS company_name, A.benefit_labor_card, A.benefit_health_card";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']		= 1;
		$params['status_approval']= 3;
		$params['status_benefit'] = 1;
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		$params['company_name']				= $_POST['columns'][1]['search']['value'];
		$params['site_name']				= $_POST['columns'][2]['search']['value'];
		$params['employee_number']			= $_POST['columns'][3]['search']['value'];
		$params['id_card']					= $_POST['columns'][4]['search']['value'];
		$params['full_name']				= $_POST['columns'][5]['search']['value'];
		$params['benefit_labor_note']		= $_POST['columns'][6]['search']['value'];
		$params['benefit_labor_company']	= $_POST['columns'][7]['search']['value'];
		$params['benefit_labor']			= $_POST['columns'][8]['search']['value'];
		$params['benefit_health_note']		= $_POST['columns'][9]['search']['value'];
		$params['benefit_health_company']	= $_POST['columns'][10]['search']['value'];
		$params['benefit_health']			= $_POST['columns'][11]['search']['value'];
		$params['site_id'] 					= $this->input->get('site');
		$params['is_labor_company']		 	= $this->input->get('npptk');
		$params['is_health_company'] 		= $this->input->get('nppks');
		$list 	= $this->employee_model->gets($params);
		$total 	= $this->employee_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['id']						= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 						= $i;
			$result['site_name']				= $item->site_name;
			$result['company_name']				= $item->company_name;
			$result['employee_number']			= $item->employee_number;
			$result['id_card']					= $item->id_card;
			$result['full_name']				= $item->full_name;
			$result['benefit_labor_note']		= $item->benefit_labor_note;
			$result['benefit_labor']			= $item->benefit_labor;
			$result['benefit_labor_company']	= $item->benefit_labor_company;
			$result['benefit_health_note']		= $item->benefit_health_note;
			$result['benefit_health_company']	= $item->benefit_health_company;
			$result['benefit_health']			= $item->benefit_health;
			$result['action'] 					=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("benefit_employee/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("benefit_employee/form/".$item->id).'">Ubah</a>';
			if($item->benefit_labor_card){
				$result['action'] .= '<a class="btn-sm btn-warning btn-block" target="_blank" href="'.base_url($item->benefit_labor_card).'">KPJ</a> ';
			}
			if($item->benefit_health_card){
				$result['action'] .= '<a class="btn-sm btn-success btn-block" target="_blank" href="'.base_url($item->benefit_health_card).'">KIS</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function preview($employee_id=FALSE)
	{
		$this->load->model(array('employee_model','employee_education_model', 'employee_emergency_model', 'employee_history_model', 'employee_language_model', 'employee_reference_model', 'employee_skill_model'));
		$data['_TITLE_'] 		= 'Preview Karyawan';
		$data['_PAGE_']	 		= 'employee/preview';
		$data['_MENU_PARENT_'] 	= 'personalia';
		$data['_MENU_'] 		= 'employee';

		$data['id'] = $employee_id;

		if (!$employee_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('benefit_employee/list'));
		}

		$data['list_education'] = $this->employee_education_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.institute, A.start, A.end'));
		$data['list_emergency'] = $this->employee_emergency_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.relation, A.phone, A.address'));
		$data['list_history'] 	= $this->employee_history_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.company_name, A.position, A.period,  A.salary, A.company_address, A.resign_note'));
		$data['list_language'] 	= $this->employee_language_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.language, A.verbal, A.write'));
		$data['list_reference'] = $this->employee_reference_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.company_name, A.position, A.phone'));
		$data['list_skill'] 	= $this->employee_skill_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));

		$data['preview'] = $this->employee_model->preview(array('id' => $employee_id));
		$data['preview']->date_birth = format_date_ina($data['preview']->date_birth);
		$data['preview']->city_birth = str_replace(array('Kota', 'Kabupaten'), '', $data['preview']->city_birth);
		$this->load->view('roemployee/preview', $data);
	}

	public function export($status = 'complete')
	{
		$this->load->model(array('employee_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = "Data Karyawan";
		$title = 'benefit_karyawan';
		$files = glob(FCPATH."files/".$title."/*");
		$params = $this->input->get();
		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.full_name, A.health_insurance, A.health_insurance_note, A.benefit_labor_note, A.benefit_labor, A.benefit_labor_company, A.benefit_health_note, A.benefit_health, A.benefit_health_company, B.name AS site_name, C.name AS company_name";

		$params['orderby']		= "A.full_name";
		$params['order']		= "ASC";
		$params['status']		= 1;
		$params['status_approval'] 	= 3;
		if($status == 'notcomplete'){
			$params['benefit_not_complete'] = true;
		}else if($status == 'approval'){
			$params['status_benefit'] = 1;
		}else{
			$params['benefit_complete'] = true;
		}
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "No");
		$excel->getActiveSheet()->setCellValue("B".$i, "Unit Kontrak");
		$excel->getActiveSheet()->setCellValue("C".$i, "Site Bisnis");
		$excel->getActiveSheet()->setCellValue("D".$i, "ID Karyawan");
		$excel->getActiveSheet()->setCellValue("E".$i, "NIK KTP");
		$excel->getActiveSheet()->setCellValue("F".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("G".$i, "Program BPJS Ketenagakerjaan");
		$excel->getActiveSheet()->setCellValue("H".$i, "NPP Ketenagakerjaan");
		$excel->getActiveSheet()->setCellValue("I".$i, "Nomor KPJ");
		$excel->getActiveSheet()->setCellValue("J".$i, "Nama Asuransi Kesehatan");
		$excel->getActiveSheet()->setCellValue("K".$i, "NPP Asuransi Kesehatan");
		$excel->getActiveSheet()->setCellValue("L".$i, "Nomor KIS");

		$list_candidate = $this->employee_model->gets($params);
		$i=2;
		$no = 1;
		foreach ($list_candidate as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $no);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->company_name);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->site_name);
			$excel->getActiveSheet()->setCellValueExplicit("D".$i, $item->employee_number);
			$excel->getActiveSheet()->setCellValueExplicit("E".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValueExplicit("F".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("G".$i, $item->benefit_labor_note);
			$excel->getActiveSheet()->setCellValueExplicit("H".$i, $item->benefit_labor_company);
			$excel->getActiveSheet()->setCellValueExplicit("I".$i, $item->benefit_labor);
			$excel->getActiveSheet()->setCellValueExplicit("J".$i, $item->benefit_health_note);
			$excel->getActiveSheet()->setCellValueExplicit("K".$i, $item->benefit_health);
			$excel->getActiveSheet()->setCellValueExplicit("L".$i, $item->benefit_health_company);
			$i++;
			$no++;
		}
		$excel->getActiveSheet()->setTitle('Daftar Karyawan');
		
		$excel->createSheet();
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function import(){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('employee_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {
				$id_card			= replace_null($sheet->getCell('E'.$row)->getValue());
				$site_id			= $this->session->userdata('site');
				$candidate 			= $this->employee_model->get(array('id_card' => $id_card, 'columns' => 'A.id'));
				if(!$candidate){
					continue;
				}
				$data['id'] 					= $candidate->id;
				$data['benefit_labor_note']		= replace_null($sheet->getCell('G'.$row)->getValue());
				$data['benefit_labor_company']	= replace_null($sheet->getCell('H'.$row)->getValue());
				$data['benefit_labor']			= replace_null($sheet->getCell('I'.$row)->getValue());
				$data['benefit_health_note']	= replace_null($sheet->getCell('J'.$row)->getValue());
				$data['benefit_health_company']	= replace_null($sheet->getCell('K'.$row)->getValue());
				$data['benefit_health']			= replace_null($sheet->getCell('L'.$row)->getValue());
				if($data['benefit_labor'] 		!= '' && $data['benefit_health'] != ''){
					$data['status_benefit']		= 0;
				}
				$save_id = $this->employee_model->save($data);
			}

			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
					redirect(base_url('benefit_employee/list'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
					redirect(base_url('benefit_employee/list'));
		}
	}

	public function accident($employee_id = FALSE){
		$this->load->model(array('accident_model', 'employee_model', 'city_model'));
		$employee = $this->employee_model->preview(array('id' => $employee_id));
		$template = $this->accident_model->get(array('company_id' => $employee->company_id, 'columns' => 'A.content'));
		$data['content'] = "";
		if($template){
			$data['content']= $template->content;
		}
		$data['content']	= str_replace("{NAMA_LENGKAP}", $employee->full_name, $data['content']);
		$data['content']	= str_replace("{NOMOR_KARYAWAN}", $employee->employee_number, $data['content']);
		$data['content']	= str_replace("{NOMOR_KESEHATAN}", $employee->benefit_health, $data['content']);
		$data['content']	= str_replace("{NOMOR_KETENAGAKERJAAN}", $employee->benefit_labor, $data['content']);
		$data['content']	= str_replace("{NIK}", $employee->id_card, $data['content']);
		$birth_date			= substr($employee->date_birth, 8, 2)." ".get_month(substr($employee->date_birth, 5, 2))." ".substr($employee->date_birth, 0, 4);
		$data['content'] 	= str_replace("{TANGGAL_LAHIR}", $birth_date, $data['content'] );
		$birth_place 		= str_replace("KOTA", "", strtoupper($employee->city_birth));
		$birth_place 		= str_replace("KABUPATEN", "", strtoupper($employee->city_birth));
		$data['content'] 	= str_replace("{TEMPAT_LAHIR}",$birth_place, $data['content'] );
		$city 				= $this->city_model->preview(array('columns' => 'A.name, B.name AS province_name', 'id' => $employee->city_card_id));
		$address_card		= $employee->address_card;
		if($city){
				$address_card = $address_card.", ".$city->name.", ".$city->province_name;
		}
		$data['content'] 	= str_replace("{ALAMAT_KTP}",$address_card,$data['content']);
		$data['content']	= str_replace("{JABATAN}", $employee->position, $data['content']);
		$data['content']	= str_replace("{NAMA_SITE}", $employee->site_name, $data['content']);
		$data['content']	= str_replace("{ALAMAT_SITE}", $employee->site_address, $data['content']);
		$data['content']	= str_replace("{NAMA_BISNIS_UNIT}", $employee->company_name, $data['content']);
		$data['content']	= str_replace("{ALAMAT_BISNIS_UNIT}", $employee->company_address, $data['content']);
		$data['content']	= str_replace("{TANGGAL_SEKARANG}", format_date_ina(date('Y-m-d')), $data['content']);

		$letter_number = $employee->employee_number.'/'.$employee->site_code.'/'.$employee->company_code.'/'.get_roman_month(date('m')).'/'.date('Y');
		$data['content']	= str_replace("{NOMOR_SURAT}", $letter_number, $data['content']);

		$data['_TITLE_'] 		= 'Surat Keterangan Kerja';
		$this->load->view('monitoring/work', $data);
	}

	public function register()
	{
		if($this->input->post()){
			$this->load->model(array('employee_model', 'contract_model', 'employee_contract_model', 'employee_salary_model', 'city_model'));
		
			$id 							= $this->input->post('id');
			$data['benefit_labor_note']		= $this->input->post('benefit_labor_note');
			$data['benefit_labor'] 			= $this->input->post('benefit_labor');
			$data['benefit_labor_company'] 	= $this->input->post('benefit_labor_company');
			$data['benefit_health_note'] 	= $this->input->post('benefit_health_note');
			$data['benefit_health'] 		= $this->input->post('benefit_health');
			$data['benefit_health_company'] = $this->input->post('benefit_health_company');
			foreach ($id as $employee_id) {
				$data['id'] 		= $employee_id;
				if($data['benefit_labor'] 		!= '' && $data['benefit_health'] != ''){
					$data['status_benefit']		= 0;
				}
				$result = $this->employee_model->save($data);
				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger'));
					redirect(base_url('benefit_employee/list'));
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			redirect(base_url('benefit_employee/list'));
		}
	}

	public function submission(){

		$data['_TITLE_'] 		= 'Karyawan - Pengajuan Pendaftaran';
		$data['_PAGE_'] 		= 'benefit_employee/submission';
		$data['_MENU_PARENT_'] 	= 'benefit_data_employee';
		$data['_MENU_'] 		= 'benefit_submission';
		$this->view($data);
	}

	public function submission_ajax(){
		$this->load->model(array('employee_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.full_name, A.benefit_labor_note, A.benefit_labor, A.benefit_labor_company, A.benefit_health_note, A.benefit_health, A.benefit_health_company, B.name AS site_name, C.name AS company_name, A.benefit_labor_card, A.benefit_health_card";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']		= 1;
		$params['status_approval']= 3;
		$params['status_benefit'] = 1;
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		$params['company_name']				= $_POST['columns'][2]['search']['value'];
		$params['site_name']				= $_POST['columns'][3]['search']['value'];
		$params['employee_number']			= $_POST['columns'][4]['search']['value'];
		$params['id_card']					= $_POST['columns'][5]['search']['value'];
		$params['full_name']				= $_POST['columns'][6]['search']['value'];
		$params['benefit_labor_note']		= $_POST['columns'][7]['search']['value'];
		$params['benefit_labor_company']	= $_POST['columns'][8]['search']['value'];
		$params['benefit_labor']			= $_POST['columns'][9]['search']['value'];
		$params['benefit_health_note']		= $_POST['columns'][10]['search']['value'];
		$params['benefit_health_company']	= $_POST['columns'][11]['search']['value'];
		$params['benefit_health']			= $_POST['columns'][12]['search']['value'];
		$list 	= $this->employee_model->gets($params);
		$total 	= $this->employee_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['id']						= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 						= $i;
			$result['site_name']				= $item->site_name;
			$result['company_name']				= $item->company_name;
			$result['employee_number']			= $item->employee_number;
			$result['id_card']					= $item->id_card;
			$result['full_name']				= $item->full_name;
			$result['benefit_labor_note']		= $item->benefit_labor_note;
			$result['benefit_labor']			= $item->benefit_labor;
			$result['benefit_labor_company']	= $item->benefit_labor_company;
			$result['benefit_health_note']		= $item->benefit_health_note;
			$result['benefit_health_company']	= $item->benefit_health_company;
			$result['benefit_health']			= $item->benefit_health;
			$result['action'] 					=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("benefit_employee/preview/".$item->id).'">Lihat</a>
				<a onclick="confirm_cancel(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("benefit_employee/cancel_submit/".$item->id).'">Batalkan</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function employee_ajax(){
		$this->load->model(array('employee_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, DATE_FORMAT(A.approved_at, '%d/%m/%Y') AS approved_at, A.employee_number, A.id_card, A.full_name, A.benefit_labor_note, A.benefit_labor, A.benefit_labor_company, A.benefit_health_note, A.benefit_health, A.benefit_health_company, B.name AS site_name, C.name AS company_name, A.benefit_labor_card, A.benefit_health_card";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status_approval']= 3;
		$params['status_benefit'] = 0;
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		$params['approved_at']				= $_POST['columns'][1]['search']['value'];
		$params['company_name']				= $_POST['columns'][2]['search']['value'];
		$params['site_name']				= $_POST['columns'][3]['search']['value'];
		$params['employee_number']			= $_POST['columns'][4]['search']['value'];
		$params['id_card']					= $_POST['columns'][5]['search']['value'];
		$params['full_name']				= $_POST['columns'][6]['search']['value'];
		$params['benefit_labor_note']		= $_POST['columns'][7]['search']['value'];
		$params['benefit_labor_company']	= $_POST['columns'][8]['search']['value'];
		$params['benefit_labor']			= $_POST['columns'][9]['search']['value'];
		$params['benefit_health_note']		= $_POST['columns'][10]['search']['value'];
		$params['benefit_health_company']	= $_POST['columns'][11]['search']['value'];
		$params['benefit_health']			= $_POST['columns'][12]['search']['value'];
		$list 	= $this->employee_model->gets($params);
		$total 	= $this->employee_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['id']						= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 						= $i;
			$result['approved_at']				= $item->approved_at;
			$result['site_name']				= $item->site_name;
			$result['company_name']				= $item->company_name;
			$result['employee_number']			= $item->employee_number;
			$result['id_card']					= $item->id_card;
			$result['full_name']				= $item->full_name;
			$result['benefit_labor_note']		= $item->benefit_labor_note;
			$result['benefit_labor']			= $item->benefit_labor;
			$result['benefit_labor_company']	= $item->benefit_labor_company;
			$result['benefit_health_note']		= $item->benefit_health_note;
			$result['benefit_health_company']	= $item->benefit_health_company;
			$result['benefit_health']			= $item->benefit_health;
			$result['action'] 					=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("benefit_employee/preview/".$item->id).'">Lihat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function submit()
	{
		if($this->input->post()){
			$this->load->model(array('employee_model'));
		
			$id 							= $this->input->post('id');
			foreach ($id as $employee_id) {
				$data['id'] 			= $employee_id;
				$data['status_benefit']	= 1;
				$result = $this->employee_model->save($data);
				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger'));
					redirect(base_url('benefit_employee/submission'));
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			redirect(base_url('benefit_employee/submission'));
		}
	}

	public function cancel_submit($employee_id = FALSE)
	{
		$this->load->model(array('employee_model'));
		if($employee_id){
			$data['id'] 			= $employee_id;
			$data['status_benefit']	= 0;
			$result = $this->employee_model->save($data);
			if ($result) {
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
				redirect(base_url('benefit_employee/submission'));
			}
		}

		$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger'));
		redirect(base_url('benefit_employee/submission'));
	}
}
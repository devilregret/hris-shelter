<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('sales'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}
	
	public function list(){
		$data['_TITLE_'] 		= 'Daftar Perusahaan';
		$data['_PAGE_'] 		= 'client/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'client';

		$this->view($data);
	}

	public function list_ajax(){
		$this->load->model(array('client_model'));

		$column_index 			= $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.name, A.address";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['all']			= TRUE;

		$params['name']			= $_POST['columns'][1]['search']['value'];
		$params['address']		= $_POST['columns'][2]['search']['value'];

		$list 	= $this->client_model->gets($params);
		$total 	= $this->client_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['name'] 		= $item->name;
			$result['address']		= $item->address;

			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("client/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("client/form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("client/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function form($client_id = FALSE)
	{
		$this->load->model(array('client_model'));

		$data['id'] 			= '';
		$data['name']			= '';
		$data['address']		= '';

		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['name'] 		= $this->input->post('name');
			$data['address']	= $this->input->post('address');
			
			$this->form_validation->set_rules('name', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$insert 	= array('id' => $data['id'], 'name' => $data['name'], 'address' => $data['address']);
				$save_id	= $this->client_model->save($insert);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('client/list'));
		}

		if ($client_id)
		{
			$data = (array) $this->client_model->get(array('id' => $client_id, 'A.id, A.name, A.address'));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('client/list'));
			}
		}

		$data['_TITLE_'] 		= 'Daftar Perusahaan';
		$data['_PAGE_'] 		= 'client/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'client';
		return $this->view($data);
	}

	public function preview($client_id=FALSE)
	{
		$this->load->model('client_model');
		$data['_TITLE_'] 		= 'Preview Perusahaan';
		$data['_PAGE_']	 		= 'client/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'client';

		$data['id'] = $client_id;
		
		if (!$client_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('client/list'));
		}

		$data['preview'] = $this->client_model->preview(array('id' => $client_id));
		$this->load->view('client/preview', $data);
	}

	public function delete($client_id = false)
	{
		$this->load->model('client_model');
		if ($client_id)
		{
			$data =  $this->client_model->get(array('id' => $client_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $client_id, 'is_active' => 0);
				$result = $this->client_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('client/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('client/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('client/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('client/list'));
		}
	}	

	public function syncronize_customer(){

		$this->load->model(array('accurate_model', 'proyek_accurate_model', 'site_model', 'client_model'));
		$list_proyek = $this->proyek_accurate_model->gets(array('columns' => 'A.id, A.db_id, A.customer_id', 'kontrak_selesai' => '2021-01-01', 'orderby' => 'A.db_id', 'order' => 'ASC'));

		$accurate 		= $this->accurate_model->get(array('id' => 1, 'columns' => 'A.id, A.access_token'));
		$authorization = "Authorization: Bearer ".$accurate->access_token;
		$success 	= 0;
		$notfound 	= 0;
		
		foreach($list_proyek AS $item){	
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
			curl_setopt($ch, CURLOPT_URL, "https://account.accurate.id/api/open-db.do?id=".$item->db_id);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			$response = curl_exec($ch); 
			$resultSession = json_decode($response, true);
			curl_close($ch);

			$header = array('Content-Type: application/json' , $authorization, "X-Session-ID: ".$resultSession['session']);
			$customerURL = $resultSession['host']."/accurate/api/customer/detail.do?id=".$item->customer_id;

			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_URL, $customerURL);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			$response = curl_exec($ch); 
			$resultCustomer = json_decode($response, true);
			curl_close($ch);

			if(isset($resultCustomer['d']['id'])){
				$client = $this->client_model->get(array('name' => $resultCustomer['d']['name'], 'columns' => 'A.id, A.name, A.address'));
				if($client){
					$data_client['id']						= $client->id;
					$data_client['accurate_customer_id']	= $resultCustomer['d']['id'];

					if($client->address == ''){
						$data_client['address']	= $resultCustomer['d']['billStreet'];
					}
					$this->client_model->save($data_client);
					$success++;
				}else{
					$notfound++;
				}
			}				
		}

		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  '.$success.' Customer Berhasil dihubungkan, '.$notfound.' Customer Belum ditemukan.','success'));
		redirect(base_url('client/list'));
	}
}
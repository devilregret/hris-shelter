<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mutation extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		$role 	= array_merge($role, $this->config->item('relation_officer_security'));
		$role 	= array_merge($role, $this->config->item('benefit'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function submit(){
		$type 			=$this->input->post('type');
		foreach ($this->input->post('id') as $employee_id)
		{
			$this->load->model(array('employee_model', 'approval_model'));
			$data = $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.company_id, A.site_id, A.position_id, A.resign_note, A.new_site_id'));
			if (!$data){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('mutation/approval/'.$type));
			}else{
				if($this->input->post('action') == 'reject'){
					$insert = array('id' => $employee_id, 'status_approval' => 3);
					$result = $this->employee_model->save($insert);

					$approval = array('id' => '', 'employee_id' => $employee_id, 'site_id' => $data->site_id, 'company_id' => $data->company_id, 'position_id' => $data->position_id, 'status_approval' => 'Mutasi Ditolak', 'note' => $data->resign_note);
						$this->approval_model->save($approval);
				}else{
					$approval = array('id' => '', 'employee_id' => $employee_id, 'site_id' => $data->site_id, 'company_id' => $data->company_id, 'position_id' => $data->position_id, 'status_approval' => 'Mutasi', 'note' => $data->resign_note);
					$this->approval_model->save($approval);
					$this->employee_model->save(array('id' => $employee_id, 'status' => 1, 'status_approval' => 3, 'site_id' => $data->new_site_id, 'new_site_id' => 0));
				}
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diupdate.','success'));
		redirect(base_url('mutation/approval/'.$type));

	}

	public function approval($type = FALSE){
		$data['_TITLE_'] 		= 'Pengajuan Mutasi';
		$data['_PAGE_'] 		= 'mutation/approval';
		$data['_MENU_PARENT_'] 	= 'other';
		$data['_MENU_'] 		= 'mutation_approval';
		$this->view($data);
	}

	public function approval_ajax(){
		$this->load->model(array('roresign_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.full_name, DATE_FORMAT(A.resign_submit, '%d/%m/%Y') AS resign_submit, A.resign_note, A.resign_burden, B.name AS site_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['status_approval'] 		= 6;
		
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		
		$params['employee_number']		= $_POST['columns'][1]['search']['value'];
		$params['id_card']				= $_POST['columns'][2]['search']['value'];
		$params['full_name']			= $_POST['columns'][3]['search']['value'];
		$params['site_name']			= $_POST['columns'][4]['search']['value'];
		$params['resign_note']			= $_POST['columns'][5]['search']['value'];
		$params['resign_burden']		= $_POST['columns'][6]['search']['value'];
		$params['resign_submit']		= $_POST['columns'][7]['search']['value'];

		$list 	= $this->roresign_model->gets($params);
		$total 	= $this->roresign_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['employee_number']		= $item->employee_number;
			$result['id_card']				= $item->id_card;
			$result['full_name']			= $item->full_name;
			$result['site_name']			= $item->site_name;
			$result['resign_note']			= $item->resign_note;
			$result['resign_burden']		= $item->resign_burden;
			$result['resign_submit']		= $item->resign_submit;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("employee/preview/".$item->id).'">Lihat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}
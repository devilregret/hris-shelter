<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function delete($user_id = false)
	{
		$this->load->model('user_model');
		if ($user_id)
		{
			$data =  $this->user_model->get(array('id' => $user_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $user_id, 'is_active' => 0);
				$result = $this->user_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dinonaktifkan.','success'));
					redirect(base_url('user/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal dinonaktifkan.','danger'));
					redirect(base_url('user/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('user/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('user/list'));
		}
	}

	public function active($user_id = false)
	{
		$this->load->model('user_model');
		if ($user_id)
		{
			$data =  $this->user_model->get(array('id' => $user_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $user_id, 'is_active' => 1);
				$result = $this->user_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil diaktifkan.','success'));
					redirect(base_url('user/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal diaktifkan.','danger'));
					redirect(base_url('user/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('user/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('user/list'));
		}
	}

	public function preview($user_id=FALSE)
	{
		$this->load->model('user_model');
		$data['_TITLE_'] 		= 'Preview Akun Pengguna';
		$data['_PAGE_']	 		= 'user/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'user';

		$data['id'] = $user_id;

		
		if (!$user_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('user/list'));
		}

		$data['preview'] = $this->user_model->preview(array('id' => $user_id));
		$this->load->view('user/preview', $data);
	}

	public function form($user_id = FALSE)
	{
		$this->load->model(array('user_model', 'branch_model','role_model', 'auth_model', 'company_model'));

		$data['id']		 	= $user_id;
		$data['full_name'] 	= '';
		$data['username'] 	= '';
		$data['email'] 		= '';
		$data['company_id']	= '';
		$data['branch_id']	= '';
		$data['role_id']	= '';
		$data['password'] 	= '';
		$data['repassword'] = '';
		$data['tab']		= 'profile';

		if($this->input->post('full_name')){

			$params['id']			= $data['id'] 			= $this->input->post('id');
			$params['full_name'] 	= $data['full_name'] 	= $this->input->post('full_name');
			$params['username'] 	= $data['username'] 	= $this->input->post('username');
			$params['email']		= $data['email']		= $this->input->post('email');
			$params['company_id'] 	= $data['company_id'] 	= $this->input->post('company_id');
			$params['branch_id'] 	= $data['branch_id'] 	= $this->input->post('branch_id');
			$params['role_id']		= $data['role_id']		= $this->input->post('role_id');

			// $this->form_validation->set_rules('username', '', 'required');
			// $this->form_validation->set_rules('full_name', '', 'required');
			$this->form_validation->set_rules('email', '', 'required|valid_email');
			$this->form_validation->set_rules('company_id', '', 'required');
			$this->form_validation->set_rules('branch_id', '', 'required');
			$this->form_validation->set_rules('role_id', '', 'required');

			if($params['id'] == ''){
				$data['password'] 	= $this->input->post('password');
				$data['repassword'] = $this->input->post('repassword');
				
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('repassword', 'Ulangi Password', 'required|matches[password]');
				$params['password']	= generate_password($data['password']);				
			}


			$exist = $this->auth_model->get(array('username' => $data['username'], 'email' => $data['email']));
			if($exist){
				if($exist->id != $params['id']){
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> username atau email yang anda masukkan sudah terdaftar.','danger'));
					redirect(base_url('user/list'));
				}
			}

			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
				redirect(base_url('user/list'));
			}else{
				$result	 		= $this->user_model->save($params);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
					redirect(base_url('user/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
					redirect(base_url('user/list'));
				}		
			}
		}else{
			if($this->input->post('password')){
				$data['tab']		= 'password';
				$data['password'] 	= $this->input->post('password');
				$data['repassword'] = $this->input->post('repassword');
				
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('repassword', 'Ulangi Password', 'required|matches[password]');
				if ($this->form_validation->run() == FALSE)
				{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
					redirect(base_url('user/list'));
				}else{
					$params['id']		= $data['id'] 			= $this->input->post('id');
					$params['password']	= generate_password($data['password']);
					$result	 			= $this->user_model->save($params);
					if ($result) {
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
						redirect(base_url('user/list'));
					}else{
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
						redirect(base_url('user/list'));
					}	
				}
			}

			$account = $this->user_model->get(array('id' => $user_id, 'columns' => 'A.full_name, A.username, A.email, A.branch_id, A.role_id, A.company_id'));

			if($account){
				$data['full_name'] 	= $account->full_name;
				$data['username'] 	= $account->username;
				$data['email'] 		= $account->email;
				$data['branch_id'] 	= $account->branch_id;
				$data['role_id'] 	= $account->role_id;
				$data['company_id'] = $account->company_id;
			}
		}

		$data['list_company'] 	= $this->company_model->gets(array());
		$data['list_branch'] 	= $this->branch_model->gets(array());
		$data['list_role'] 		= $this->role_model->gets(array());

		$data['_TITLE_'] 		= 'Akun Pengguna';
		$data['_PAGE_'] 		= 'user/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'user';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Daftar Akun Pengguna';
		$data['_PAGE_'] 		= 'user/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'user';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('user_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.username, H.phone_number, A.full_name, B.name AS role_name, C.name AS branch_name, D.name AS company_name, A.is_active';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['not_applicant']= true;
		$params['not_employee'] = true;
		$params['username']		= $_POST['columns'][1]['search']['value'];
		$params['phone_number']	= $_POST['columns'][2]['search']['value'];
		$params['full_name']	= $_POST['columns'][3]['search']['value'];
		$params['role_name']	= $_POST['columns'][4]['search']['value'];
		$params['branch_name']	= $_POST['columns'][5]['search']['value'];
		$params['company_name']	= $_POST['columns'][6]['search']['value'];
		$params['all']			= true;
		$list 	= $this->user_model->gets($params);
		$total 	= $this->user_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		
		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['username'] 	= $item->username;
			$result['phone_number'] = $item->phone_number;
			$result['full_name']	= $item->full_name;
			$result['role_name']	= $item->role_name;
			$result['branch_name']	= $item->branch_name;
			$result['company_name']	= $item->company_name;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("user/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("user/form/".$item->id).'">Ubah</a>';
			if($item->is_active == 1){
				$result['action'] .= '<a onclick="confirm_nonactive(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("user/delete/".$item->id).'">Non Aktifkan</a>';
			}else{
				$result['action'] .= '<a onclick="confirm_active(this)" class="btn-sm btn-warning btn-block" style="cursor:pointer;" data-href="'.base_url("user/active/".$item->id).'">Aktifkan</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function delete_indirect($user_id = false)
	{
		$this->load->model('user_model');
		if ($user_id)
		{
			$data =  $this->user_model->get(array('id' => $user_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $user_id, 'is_active' => 0, 'role_id' => 1);
				$result = $this->user_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dinonaktifkan.','success'));
					redirect(base_url('user/indirect'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dinonaktifkan.','danger'));
					redirect(base_url('user/indirect'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('user/indirect'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('user/indirect'));
		}
	}

	public function active_indirect($user_id = false)
	{
		$this->load->model('user_model');
		if ($user_id)
		{
			$data =  $this->user_model->get(array('id' => $user_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $user_id, 'is_active' => 1, 'role_id' => 1);
				$result = $this->user_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil diaktifkan.','success'));
					redirect(base_url('user/indirect'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal diaktifkan.','danger'));
					redirect(base_url('user/indirect'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('user/indirect'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('user/indirect'));
		}
	}

	public function form_indirect($user_id = FALSE)
	{
		$this->load->model(array('user_model', 'branch_model','role_model', 'auth_model', 'company_model', 'struktur_jabatan_model'));

		$data['id']		 	= $user_id;
		$data['full_name'] 	= '';
		$data['username'] 	= '';
		$data['email'] 		= '';
		$data['company_id']	= '';
		$data['branch_id']	= '';
		$data['role_id']	= '';
		$data['password'] 	= '';
		$data['repassword'] = '';
		$data['tab']		= 'profile';

		if($this->input->post('full_name')){

			$params['id']			= $data['id'] 			= $this->input->post('id');
			$params['full_name'] 	= $data['full_name'] 	= $this->input->post('full_name');
			$params['username'] 	= $data['username'] 	= $this->input->post('username');
			$params['email']		= $data['email']		= $this->input->post('email');
			$params['company_id'] 	= $data['company_id'] 	= $this->input->post('company_id');
			$params['branch_id'] 	= $data['branch_id'] 	= $this->input->post('branch_id');
			$params['role_id']		= $data['role_id']		= $this->input->post('role_id');

			$this->form_validation->set_rules('username', '', 'required');
			$this->form_validation->set_rules('full_name', '', 'required');
			// $this->form_validation->set_rules('email', '', 'required|valid_email');
			$this->form_validation->set_rules('company_id', '', 'required');
			$this->form_validation->set_rules('branch_id', '', 'required');
			$this->form_validation->set_rules('role_id', '', 'required');
			// $this->form_validation->set_rules('struktur_id', '', 'required');

			if($params['id'] == ''){
				$data['password'] 	= $this->input->post('password');
				$data['repassword'] = $this->input->post('repassword');
				
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('repassword', 'Ulangi Password', 'required|matches[password]');
				$params['password']	= generate_password($data['password']);				
			}


			$exist = $this->auth_model->get(array('username' => $data['username']));
			if($exist){
				if($exist->id != $params['id']){
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> username yang anda masukkan sudah terdaftar.','danger'));
					redirect(base_url('user/indirect'));
				}
			}

			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
				redirect(base_url('user/indirect'));
			}else{
				$account = $this->user_model->get(array('id' => $params['id'], 'columns' => 'A.employee_id'));
				$result	 		= $this->user_model->save($params, $account);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
					redirect(base_url('user/indirect'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
					redirect(base_url('user/indirect'));
				}		
			}
		}else{
			if($this->input->post('password')){
				$data['tab']		= 'password';
				$data['password'] 	= $this->input->post('password');
				$data['repassword'] = $this->input->post('repassword');
				
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('repassword', 'Ulangi Password', 'required|matches[password]');
				if ($this->form_validation->run() == FALSE)
				{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
					redirect(base_url('user/indirect'));
				}else{
					$params['id']		= $data['id'] 			= $this->input->post('id');
					$params['password']	= generate_password($data['password']);
					$result	 			= $this->user_model->save($params);
					if ($result) {
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
						redirect(base_url('user/indirect'));
					}else{
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
						redirect(base_url('user/indirect'));
					}	
				}
			}

			$account = $this->user_model->get(array('id' => $user_id, 'columns' => 'A.full_name, A.username, A.email, A.branch_id, A.role_id, A.company_id, A.employee_id'));

			if($account){
				$data['full_name'] 	= $account->full_name;
				$data['username'] 	= $account->username;
				$data['email'] 		= $account->email;
				$data['branch_id'] 	= $account->branch_id;
				$data['role_id'] 	= $account->role_id;
				$data['company_id'] = $account->company_id;
				if($data['full_name']  == '' || $data['full_name']  == ''){
					$employee = $this->user_model->employee_get(array('columns' => 'A.full_name, A.id_card, A.branch_id, A.company_id, A.email', 'id' => $account->employee_id));
					if($employee){				
						$data['full_name'] 	= $employee->full_name;
						$data['username'] 	= $employee->id_card;
						$data['email'] 		= $employee->email;
						$data['branch_id'] 	= $employee->branch_id;
						$data['company_id'] = $employee->company_id;
						$this->user_model->save(array('id' => $user_id, 'full_name' => $employee->full_name, 'username' => $employee->id_card, 'email' => $employee->email, 'branch_id' => $employee->branch_id, 'company_id' => $employee->company_id));
					}
				}
			}
		}

		$struktur['columns'] = 'A.id, B.level_name, F.name ';

		$data['list_company'] 	= $this->company_model->gets(array());
		$data['list_branch'] 	= $this->branch_model->gets(array());
		$data['list_role'] 		= $this->role_model->gets(array());
		// $data['list_approval'] 	= $this->struktur_jabatan_model->getStruktur($struktur);

		// print_r($data['list_approval']);
		// die();
		$data['_TITLE_'] 		= 'Akun Indirect';
		$data['_PAGE_'] 		= 'user/form_indirect';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'indirect';
		return $this->view($data);
	}

	public function indirect(){
		$data['_TITLE_'] 		= 'Karyawan Indirect';
		$data['_PAGE_'] 		= 'user/indirect';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'indirect';

		$this->view($data);

	}

	public function indirect_ajax(){
		$this->load->model(array('user_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, H.id_card, H.phone_number, H.full_name, B.name AS role_name, C.name AS branch_name, D.name AS company_name, A.is_active';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['site_id'] 		= 2;

		$params['id_card']		= $_POST['columns'][1]['search']['value'];
		$params['phone_number']	= $_POST['columns'][2]['search']['value'];
		$params['employee_name']= $_POST['columns'][3]['search']['value'];
		$params['role_name']	= $_POST['columns'][4]['search']['value'];
		$params['branch_name']	= $_POST['columns'][5]['search']['value'];
		$params['company_name']	= $_POST['columns'][6]['search']['value'];
		// $params['struktur_id']	= $_POST['columns'][7]['search']['value'];
		$params['all']			= true;

		$list 	= $this->user_model->gets($params);
		$total 	= $this->user_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		
		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['username'] 	= $item->id_card;
			$result['phone_number'] = $item->phone_number;
			$result['full_name']	= $item->full_name;
			$result['role_name']	= $item->role_name;
			$result['branch_name']	= $item->branch_name;
			$result['company_name']	= $item->company_name;
			// $result['struktur_approval']	= $item->struktur_name;
			$result['action'] 	=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("user/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("user/form_indirect/".$item->id).'">Ubah</a>';
			if($item->is_active == 1){
				$result['action'] .= '<a onclick="confirm_nonactive(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("user/delete_indirect/".$item->id).'">Non Aktifkan</a>';
			}else{
				$result['action'] .= '<a onclick="confirm_active(this)" class="btn-sm btn-warning btn-block" style="cursor:pointer;" data-href="'.base_url("user/active_indirect/".$item->id).'">Aktifkan</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function client(){
		$data['_TITLE_'] 		= 'Daftar Client';
		$data['_PAGE_'] 		= 'user/client';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'client';

		$this->view($data);

	}

	public function client_ajax(){
		$this->load->model(array('user_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.username, A.email, A.full_name, C.code AS company_code, B.name AS site_name, D.name AS branch_name';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['role_id']		= 100;

		$params['username']		= $_POST['columns'][1]['search']['value'];
		$params['email']		= $_POST['columns'][2]['search']['value'];
		$params['full_name']	= $_POST['columns'][3]['search']['value'];
		$params['company_code']	= $_POST['columns'][4]['search']['value'];
		$params['site_name']	= $_POST['columns'][5]['search']['value'];
		$params['branch_name']	= $_POST['columns'][6]['search']['value'];
		
		$list 	= $this->user_model->gets_client($params);
		$total 	= $this->user_model->gets_client($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		
		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['username'] 	= $item->username;
			$result['email'] 		= $item->email;
			$result['full_name']	= $item->full_name;
			$result['company_code']	= $item->company_code;
			$result['site_name']	= $item->site_name;
			$result['branch_name']	= $item->branch_name;
			$result['action'] 	=
				'<a class="btn-sm btn-success btn-block" href="'.base_url("user/form_client/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("user/delete_client/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function delete_client($user_id = false)
	{
		$this->load->model('user_model');
		if ($user_id)
		{
			$data =  $this->user_model->get(array('id' => $user_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $user_id, 'is_active' => 0, 'role_id' => 1);
				$result = $this->user_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('user/client'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('user/client'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('user/client'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('user/indirect'));
		}
	}

	public function form_client($user_id = FALSE)
	{
		$this->load->model(array('user_model', 'site_model', 'auth_model'));

		$data['id']		 	= $user_id;
		$data['full_name'] 	= '';
		$data['username'] 	= '';
		$data['email'] 		= '';
		$data['site_id']	= '';
		$data['password'] 	= '';
		$data['repassword'] = '';
		$data['tab']		= 'profile';

		if($this->input->post('full_name')){

			$params['id']			= $data['id'] 			= $this->input->post('id');
			$params['full_name'] 	= $data['full_name'] 	= $this->input->post('full_name');
			$params['username'] 	= $data['username'] 	= $this->input->post('username');
			$params['email']		= $data['email']		= $this->input->post('email');
			$params['site_id'] 		= $data['site_id'] 		= $this->input->post('site_id');
			$params['role_id'] 		= 100;

			$this->form_validation->set_rules('username', '', 'required');
			$this->form_validation->set_rules('full_name', '', 'required');
			$this->form_validation->set_rules('site_id', '', 'required');

			if($params['id'] == ''){
				$data['password'] 	= $this->input->post('password');
				$data['repassword'] = $this->input->post('repassword');
				
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('repassword', 'Ulangi Password', 'required|matches[password]');
				$params['password']	= generate_password($data['password']);				
			}


			$exist = $this->auth_model->get(array('username' => $data['username']));
			if($exist){
				if($exist->id != $params['id']){
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> username yang anda masukkan sudah terdaftar.','danger'));
					redirect(base_url('user/client'));
				}
			}

			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
				redirect(base_url('user/client'));
			}else{
				$result	 		= $this->user_model->save($params);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
					redirect(base_url('user/client'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
					redirect(base_url('user/client'));
				}		
			}
		}else{
			if($this->input->post('password')){
				$data['tab']		= 'password';
				$data['password'] 	= $this->input->post('password');
				$data['repassword'] = $this->input->post('repassword');
				
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('repassword', 'Ulangi Password', 'required|matches[password]');
				if ($this->form_validation->run() == FALSE)
				{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
					redirect(base_url('user/client'));
				}else{
					$params['id']		= $data['id'] 			= $this->input->post('id');
					$params['password']	= generate_password($data['password']);
					$params['role_id'] 	= 100;
					$result	 			= $this->user_model->save($params);
					if ($result) {
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
						redirect(base_url('user/client'));
					}else{
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
						redirect(base_url('user/client'));
					}	
				}
			}

			$account = $this->user_model->get(array('id' => $user_id, 'columns' => 'A.full_name, A.username, A.email, A.site_id'));

			if($account){
				$data['full_name'] 	= $account->full_name;
				$data['username'] 	= $account->username;
				$data['email'] 		= $account->email;
				$data['site_id'] 	= $account->site_id;
			}
		}

		$data['list_site'] 	= $this->site_model->gets(array('columns' => 'A.id, A.name, C.code AS company_code'));

		$data['_TITLE_'] 		= 'Akun Client';
		$data['_PAGE_'] 		= 'user/form_client';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'client';
		return $this->view($data);
	}

	public function developer(){
		$data['_TITLE_'] 		= 'Daftar Developer';
		$data['_PAGE_'] 		= 'user/developer';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'developer';

		$this->view($data);

	}

	public function developer_ajax(){
		$this->load->model(array('user_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.username, A.email, A.full_name, B.code AS company_code';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['role_id']		= 101;

		$params['username']		= $_POST['columns'][1]['search']['value'];
		$params['email']		= $_POST['columns'][2]['search']['value'];
		$params['full_name']	= $_POST['columns'][3]['search']['value'];
		$params['company_code']	= $_POST['columns'][4]['search']['value'];
		
		$list 	= $this->user_model->gets_developer($params);
		$total 	= $this->user_model->gets_developer($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		
		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['username'] 	= $item->username;
			$result['email'] 		= $item->email;
			$result['full_name']	= $item->full_name;
			$result['company_code']	= $item->company_code;
			$result['action'] 	=
				'<a class="btn-sm btn-success btn-block" href="'.base_url("user/form_developer/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("user/delete_developer/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function delete_developer($user_id = false)
	{
		$this->load->model('user_model');
		if ($user_id)
		{
			$data =  $this->user_model->get(array('id' => $user_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $user_id, 'is_active' => 0, 'role_id' => 101);
				$result = $this->user_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('user/developer'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('user/developer'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('user/developer'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('user/developer'));
		}
	}

	public function form_developer($user_id = FALSE)
	{
		$this->load->model(array('user_model', 'company_model', 'auth_model'));

		$data['id']		 	= $user_id;
		$data['full_name'] 	= '';
		$data['username'] 	= '';
		$data['email'] 		= '';
		$data['company_id']	= '';
		$data['password'] 	= '';
		$data['repassword'] = '';
		$data['tab']		= 'profile';

		if($this->input->post('full_name')){

			$params['id']			= $data['id'] 			= $this->input->post('id');
			$params['full_name'] 	= $data['full_name'] 	= $this->input->post('full_name');
			$params['username'] 	= $data['username'] 	= $this->input->post('username');
			$params['email']		= $data['email']		= $this->input->post('email');
			$params['company_id'] 	= $data['company_id'] 	= $this->input->post('company_id');
			$params['role_id'] 		= 101;

			$this->form_validation->set_rules('username', '', 'required');
			$this->form_validation->set_rules('full_name', '', 'required');
			$this->form_validation->set_rules('company_id', '', 'required');

			if($params['id'] == ''){
				$data['password'] 	= $this->input->post('password');
				$data['repassword'] = $this->input->post('repassword');
				
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('repassword', 'Ulangi Password', 'required|matches[password]');
				$params['password']	= generate_password($data['password']);				
			}


			$exist = $this->auth_model->get(array('username' => $data['username']));
			if($exist){
				if($exist->id != $params['id']){
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> username yang anda masukkan sudah terdaftar.','danger'));
					redirect(base_url('user/developer'));
				}
			}

			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
				redirect(base_url('user/developer'));
			}else{
				$result	 		= $this->user_model->save($params);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
					redirect(base_url('user/developer'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
					redirect(base_url('user/developer'));
				}		
			}
		}else{
			if($this->input->post('password')){
				$data['tab']		= 'password';
				$data['password'] 	= $this->input->post('password');
				$data['repassword'] = $this->input->post('repassword');
				
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('repassword', 'Ulangi Password', 'required|matches[password]');
				if ($this->form_validation->run() == FALSE)
				{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
					redirect(base_url('user/developer'));
				}else{
					$params['id']		= $data['id'] 			= $this->input->post('id');
					$params['password']	= generate_password($data['password']);
					$params['role_id'] 	= 101;
					$result	 			= $this->user_model->save($params);
					if ($result) {
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
						redirect(base_url('user/developer'));
					}else{
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
						redirect(base_url('user/developer'));
					}	
				}
			}

			$account = $this->user_model->get(array('id' => $user_id, 'columns' => 'A.full_name, A.username, A.email, A.company_id'));

			if($account){
				$data['full_name'] 	= $account->full_name;
				$data['username'] 	= $account->username;
				$data['email'] 		= $account->email;
				$data['company_id'] 	= $account->company_id;
			}
		}

		$data['list_company'] 	= $this->company_model->gets(array('columns' => 'A.id, A.code, A.name'));

		$data['_TITLE_'] 		= 'Akun Developer';
		$data['_PAGE_'] 		= 'user/form_developer';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'developer';
		return $this->view($data);
	}

	public function generate_employee(){
		$this->load->model(array('user_model','candidate_model'));
		$list_user = $this->user_model->gets(array('applicant' => 1));
		foreach($list_user AS $item){
			if($item->employee_id != ""){
				continue;
			}

			$data['full_name'] 			= $item->full_name;
			$data['phone_number']		= $item->phone_number;
			$data['gender']				= $item->gender;
			$data['date_birth']			= $item->date_birth;
			$data['education_level']	= $item->education_level;
			$data['education_majors']	= $item->education_majors;
			$data['experience']			= $item->experience;
			$data['branch_id']			= $item->branch_id;
			$data['id']					= '';
			$data['status']				= 0;
			$data['status_approval']	= 0;
			$data['status_completeness']= 0;

			$employee_id = $this->candidate_model->save($data);
			$this->user_model->save(array('id' => $item->id, 'employee_id' => $employee_id));
			print_r($item);
			echo "<br>".$item->employee_id."<br>";
			print_r($data);
			echo "<hr>";
		}
	}

	public function reset_password_site($site_id = FALSE){
		if($site_id){
			$this->load->model('user_model');
			$employee = $this->user_model->employee_gets(array('columns' => 'A.id AS employee_id, A.id_card, B.id', 'site_id' => $site_id));
			foreach($employee as $item)
			{
				if($item->id_card == ''){
					continue;
				}

				$user['username'] 			= $item->id_card;
				$user['password']			= generate_password($item->id_card);
				$user['employee_id']		= $item->employee_id;
				$user['id']					= $item->id;
				$result = $this->user_model->save($user);
				if($result){
					$count++;
					$username .= $item->id_card.', ';
				}
			}
		}
		redirect(base_url('user/indirect'));
	}
}
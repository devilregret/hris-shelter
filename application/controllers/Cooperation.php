<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cooperation extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('sales'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,9,10))){
			redirect(base_url());
		}
	}
	
	public function approval(){
		$data['_TITLE_'] 		= 'Pengajuan Penawaran';
		$data['_PAGE_'] 		= 'cooperation/approval';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= 'approval';

		$this->view($data);
	}

	public function approval_ajax(){
		$this->load->model(array('cooperation_model', 'position_model'));

		$column_index 			= $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.status, A.position_id, B.name AS company_name, C.name AS branch_name, D.name AS site_name, E.full_name AS sales_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['status']		= 'Pengajuan MOU';

		$params['company_name']		= $_POST['columns'][1]['search']['value'];
		$params['branch_name']		= $_POST['columns'][2]['search']['value'];
		$params['site_name']		= $_POST['columns'][3]['search']['value'];
		$params['sales_name']		= $_POST['columns'][4]['search']['value'];

		if($this->session->userdata('role') == 29){
			$params['sales_id']		=  $this->session->userdata('user_id');
		}

		$list 	= $this->cooperation_model->gets($params);
		$total 	= $this->cooperation_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$position 		= "";
			$list_job 		= $this->position_model->gets(array('columns' => 'A.name','list_id' => explode(",",$item->position_id)));
			$first 			= true;
			foreach($list_job as $job){
				if($first){
					$position .= $job->name;
					$first = false;
				}else{
					$position .= ', '.$job->name;
				}
			}

			$result['id']				= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 				= $i;
			$result['company_name']		= $item->company_name;
			$result['branch_name']		= $item->branch_name;
			$result['site_name'] 		= $item->site_name;
			$result['sales_name'] 		= $item->sales_name;
			$result['position']			= $position;
			$result['status']			= $item->status;

			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("cooperation/preview_contract/".$item->id).'">Lihat</a>';
	
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Daftar Penawaran';
		$data['_PAGE_'] 		= 'cooperation/list';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= 'cooperation';

		$this->view($data);
	}

	public function list_ajax(){
		$this->load->model(array('cooperation_model', 'position_model'));

		$column_index 			= $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.status, A.position_id, B.name AS company_name, C.name AS branch_name, D.name AS site_name, E.full_name AS sales_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['status']		= 'Penawaran';

		$params['company_name']		= $_POST['columns'][1]['search']['value'];
		$params['branch_name']		= $_POST['columns'][2]['search']['value'];
		$params['site_name']		= $_POST['columns'][3]['search']['value'];
		$params['sales_name']		= $_POST['columns'][4]['search']['value'];

		if($this->session->userdata('role') == 29){
			$params['sales_id']		=  $this->session->userdata('user_id');
		}

		$list 	= $this->cooperation_model->gets($params);
		$total 	= $this->cooperation_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$position 		= "";
			$list_job 		= $this->position_model->gets(array('columns' => 'A.name','list_id' => explode(",",$item->position_id)));
			$first 			= true;
			foreach($list_job as $job){
				if($first){
					$position .= $job->name;
					$first = false;
				}else{
					$position .= ', '.$job->name;
				}
			}

			$result['id']				= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 				= $i;
			$result['company_name']		= $item->company_name;
			$result['branch_name']		= $item->branch_name;
			$result['site_name'] 		= $item->site_name;
			$result['sales_name'] 		= $item->sales_name;
			$result['position']			= $position;
			$result['status']			= $item->status;

			$result['action'] 		=
				'<a class="btn-sm btn-success btn-block" href="'.base_url("cooperation/form/".$item->id).'">Ubah</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("cooperation/detail/".$item->id).'">Detail</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("cooperation/delete/".$item->id).'">Hapus</a>';
	
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function form($cooperation_id = FALSE)
	{
		$this->load->model(array('cooperation_model', 'company_model', 'branch_model', 'site_model', 'position_model'));

		$data['id'] 				= '';
		$data['company_id']			= '';
		$data['branch_id']			= '';
		$data['site_id']			= '';
		$data['position_id']		= '';
		$data['minimum_offer']		= 10;
		$data['management_fee']		= 10;
		$data['ppn']				= 10;
		$data['management_fee_ppn']	= 2;
		$data['sales_template_id']	= 1;
		$data['status']				= '';

		if($this->input->post()){
			$data['id'] 				= $this->input->post('id');
			$data['company_id'] 		= $this->input->post('company_id');
			$data['branch_id']			= $this->input->post('branch_id');
			$data['site_id']			= $this->input->post('site_id');
			$position					= $this->input->post('position_id');
			$data['minimum_offer']		= $this->input->post('minimum_offer');
			$data['management_fee']		= $this->input->post('management_fee');
			$data['ppn']				= $this->input->post('ppn');
			$data['management_fee_ppn']	= $this->input->post('management_fee_ppn');
			$data['status']				= $this->input->post('status');
			
			$this->form_validation->set_rules('company_id', '', 'required');
			$this->form_validation->set_rules('branch_id', '', 'required');
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				if(!$data['minimum_offer']){
					unset($data['minimum_offer']);
				}

				$data['position_id'] = '';
				if($position){
					$first = TRUE;
					foreach($position AS $item){
						if($first){
							$data['position_id'] .= $item;
							$first = FALSE;
						}else{
							$data['position_id'] .= ','.$item;
						}
					}
				}

				$save_id	= $this->cooperation_model->save($data);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('cooperation/list'));
		}

		if ($cooperation_id)
		{
			$data = (array) $this->cooperation_model->get(array('id' => $cooperation_id, 'A.id, A.company_id, A.branch_id, A.site_id, A.position_id, A.minimum_offer, A.status'));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('cooperation/list'));
			}
		}

		$data['list_company'] 	= $this->company_model->gets(array('columns' => 'A.id, A.name', 'orderby' => 'A.name', 'order' => 'ASC'));
		$data['list_branch'] 	= $this->branch_model->gets(array('columns' => 'A.id, A.name', 'orderby' => 'A.name', 'order' => 'ASC'));
		$data['list_site'] 		= $this->site_model->gets(array('columns' => 'A.id, A.name', 'orderby' => 'A.name', 'order' => 'ASC'));
		$data['list_job']		= $this->position_model->gets(array('columns' => 'A.id, A.name'));
		$data['_TITLE_'] 		= 'Form Kerjasama';
		$data['_PAGE_'] 		= 'cooperation/form';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= 'cooperation';
		return $this->view($data);
	}

	public function delete($cooperation_id = false)
	{
		$this->load->model('cooperation_model');
		if ($cooperation_id)
		{
			$data =  $this->cooperation_model->get(array('id' => $cooperation_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $cooperation_id, 'is_active' => 0);
				$result = $this->cooperation_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('cooperation/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('cooperation/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('cooperation/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('cooperation/list'));
		}
	}	

	public function detail($cooperation_id = FALSE)
	{
		$this->load->model(array('cooperation_model', 'site_model'));

		$cooperation	= $this->cooperation_model->gets(array('id' => $cooperation_id, 'columns' => 'B.name AS company_name, C.name AS branch_name, D.name AS site_name, D.address AS site_address'));
	 	if($cooperation){
			$data['cooperation'] =  $cooperation[0];
		}
		$data['cooperation_id'] = $cooperation_id;
		$data['_TITLE_'] 		= 'Detail Penawaran';
		$data['_PAGE_'] 		= 'cooperation/detail';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= 'cooperation';
		return $this->view($data);
	}

	public function detail_ajax($cooperation_id = FALSE){
		$this->load->model(array('cooperation_model'));

		$column_index 			= $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.name AS position_name, B.headcount, B.salary";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];

		$params['cooperation_id']	= $cooperation_id;
		$params['list_position']	= array();
		$cooperation 			= $this->cooperation_model->get(array('id' => $cooperation_id, 'columns' => 'A.position_id'));
		if($cooperation){
			$params['list_position'] = explode(',', $cooperation->position_id);
		}

		$list 	= $this->cooperation_model->gets_detail($params);
		$total 	= $this->cooperation_model->gets_detail($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['position_name']= $item->position_name;
			$result['headcount']	= $item->headcount;
			$result['salary'] 		= format_rupiah((int) $item->salary);

			$result['action'] 		= 
				'<a class="btn-sm btn-success btn-block" href="'.base_url("cooperation/form_detail/".$cooperation_id."/".$item->id).'">Ubah</a>';
	
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function form_detail($cooperation_id = FALSE, $position_id = FALSE)
	{
		$this->load->model(array('cooperation_model', 'position_model', 'city_model', 'selling_price_model'));

		$data['id'] 				= '';
		$data['cooperation_id'] 	= $cooperation_id;
		$data['position_id']		= $position_id;
		$data['headcount']			= '';
		$data['city_id']			= 0;
		$data['salary']				= 0;
		$data['salary_standard']	= 0;
		$data['salary_percentage']	= 0;

		if($this->input->post()){
			$data['id'] 				= $this->input->post('id');
			$data['cooperation_id'] 	= $this->input->post('cooperation_id');
			$data['position_id']		= $this->input->post('position_id');
			$data['headcount']			= $this->input->post('headcount');
			$data['city_id']			= $this->input->post('city_id');
			$data['salary_standard']	= $this->input->post('salary_standard');
			$data['salary_percentage']	= $this->input->post('salary_percentage');
			$data['salary']				= str_replace(".", "", $this->input->post('salary'));
			$this->form_validation->set_rules('cooperation_id', '', 'required');
			$this->form_validation->set_rules('position_id', '', 'required');
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{

				$component_id		= $this->input->post('component_id');
				$selling_price_id	= $this->input->post('selling_price_id');
				$offering_price		= $this->input->post('offering_price');
				$amortization		= $this->input->post('amortization');
				$standard_price		= $this->input->post('standard_price');
				$percentage_price	= $this->input->post('percentage_price');

				foreach ($component_id as $key => $value ) {
					if($offering_price[$key] == ''){
						continue;
					}
					$component['cooperation_id']	= $data['cooperation_id'];
					$component['position_id']		= $data['position_id'];
					$component['id']				= $component_id[$key];
					$component['selling_price_id']	= $selling_price_id[$key];
					$component['amortization']		= $amortization[$key];
					$component['standard_price']	= str_replace(".", "", $standard_price[$key]);
					$component['offering_price']	= str_replace(".", "", $offering_price[$key]);
					$component['percentage_price']	= $percentage_price[$key];
					$this->cooperation_model->save_component($component);
				}
				
				$save_id = $this->cooperation_model->save_detail($data);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('cooperation/form_detail/'.$cooperation_id.'/'.$position_id));
		}

		$cooperation_detail = (array) $this->cooperation_model->get_detail(array('cooperation_id' => $cooperation_id, 'position_id' => $position_id, 'A.id, A.cooperation_id, A.position_id, A.headcount, A.city_id, A.salary, A.salary_standard, A.salary_percentage'));
		if(!empty($cooperation_detail)){
			$data = $cooperation_detail;
		}

		$cooperation	= $this->cooperation_model->gets(array('id' => $cooperation_id, 'columns' => 'B.name AS company_name, C.name AS branch_name, D.name AS site_name, D.address AS site_address, A.minimum_offer'));
	 	if($cooperation){
			$data['cooperation'] =  $cooperation[0];
		}
		
		$data['position']		= $this->position_model->get(array('id' => $position_id, 'columns' => 'A.id, A.name'));
		$data['list_city']		= $this->city_model->gets(array('columns' => 'A.id, A.name, ROUND(A.minimum_salary, 0) AS minimum_salary'));
		$data['list_price']		= $this->selling_price_model->gets(array('columns' => 'A.id, A.name, A.price, A.amortization, A.amortization_price'));
		$data['list_component']		= $this->cooperation_model->gets_component(array('columns' => 'A.id, A.cooperation_id, A.selling_price_id, A.amortization, A.standard_price, A.percentage_price, A.offering_price', 'cooperation_id' => $cooperation_id, 'position_id' => $position_id));
		$data['_TITLE_'] 		= 'Form Penawaran';
		$data['_PAGE_'] 		= 'cooperation/form_detail';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= 'cooperation';
		return $this->view($data);
	}

	public function delete_component(){
		$this->load->model(array('cooperation_model'));
		$response['code'] 		= 500;
		$response['description']= "data gagal dihapus";

		$result	 = $this->cooperation_model->save_component(array('id' => $this->input->post('id'), 'is_active' => 0));
		if ($result) {
			$response['code'] 		= 200;
			$response['description']= "data berhasil dihapus.";
		}

		echo json_encode($response);
	}

	public function preview_contract($cooperation_id = FALSE)
	{
		$this->load->model(array('cooperation_model', 'sales_template_model', 'position_model'));
		$data['_TITLE_'] 		= 'Preview Penawaran Kontrak';
		$data['_PAGE_']	 		= 'employee/preview';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= 'cooperation';
		$data['content']		= "";
		$total_personel			= array();
		$total_price			= array();

		$template = $this->sales_template_model->get(array('id' => 1, 'columns' => 'A.content'));
		if($template){
			$data['content']	= $template->content;
		}
		$cooperation	= $this->cooperation_model->gets(array('id' => $cooperation_id, 'columns' => 'A.position_id, A.management_fee, A.ppn, A.management_fee_ppn, B.name AS company_name, B.address AS company_address, D.name AS site_name, D.address AS site_address'));
	 	if($cooperation){
			$data['cooperation'] 	=  $cooperation[0];
			if($template){
				$data['content'] 		= str_replace("{BISNIS_UNIT}", $data['cooperation']->company_name, $data['content']);
				$data['content'] 		= str_replace("{ALAMAT_BISNIS_UNIT}", $data['cooperation']->company_address, $data['content']);
				$data['content'] 		= str_replace("{SITE_BISNIS}", $data['cooperation']->site_name, $data['content']);
				$data['content'] 		= str_replace("{ALAMAT_SITE_BISNIS}", $data['cooperation']->site_address, $data['content']);
			}else{
				$data['content'] 		= str_replace("{BISNIS_UNIT}", '', $data['content']);
				$data['content'] 		= str_replace("{ALAMAT_BISNIS_UNIT}", '', $data['content']);
				$data['content'] 		= str_replace("{SITE_BISNIS}", '', $data['content']);
				$data['content'] 		= str_replace("{ALAMAT_SITE_BISNIS}", '', $data['content']);
			}
			$list_position 			= explode(",",$data['cooperation']->position_id);
			$data['content']		.= "<p>&nbsp;</p>";
			$data['content']		.= "<table width='100%' border='1' style='margin:10px'>";
			$data['content']		.= "<tr style='background-color:yellow;'>";
			$data['content']		.= "<th style='padding: 0 15px 0 15px;'>".$data['cooperation']->site_name."</th>";
			foreach($list_position as $item){
				$position = $this->position_model->get(array('id' => $item, 'columns' => 'A.name'));
				$data['content']	.= "<th style='padding: 0 15px 0 15px;'>".$position->name."</th>";
			}
			$data['content']		.= "</tr>";
			$data['content']		.= "<tr>";
			$data['content']		.= "<td style='padding: 0 15px 0 15px;'>HEADCOUNT</td>";
			foreach($list_position as $item){
				$detail = $this->cooperation_model->get_detail(array('cooperation_id' => $cooperation_id, 'position_id' => $item, 'columns' => 'A.headcount'));
				if($detail){
					$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'>".$detail->headcount."</td>";
					$total_personel[$item] = $detail->headcount;
				}else{
					$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'>0</td>";
					$total_personel[$item] = 0;
				}
			}
			$data['content']		.= "</tr>";
			$data['content']		.= "<tr>";
			$data['content']		.= "<td style='padding: 0 15px 0 15px;'>Gaji Pokok</td>";
			foreach($list_position as $item){
				$detail = $this->cooperation_model->get_detail(array('cooperation_id' => $cooperation_id, 'position_id' => $item, 'columns' => 'A.salary'));
				if($detail){
					$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'>".nominal_rupiah($detail->salary)."</td>";
					$total_price[$item] = $detail->salary;
				}else{
					$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'>Rp. 0</td>";
					$total_price[$item] = 0;
				}
			}
			$data['content']		.= "</tr>";
			$list_price = $this->cooperation_model->gets_selling_price(array('cooperation_id' => $cooperation_id, 'list_position' => $list_position, 'columns' => 'A.selling_price_id, B.name', 'groupby' => 'A.selling_price_id'));
			foreach($list_price as $price){
				$data['content']		.= "<tr>";
				$data['content']		.= "<td style='padding: 0 15px 0 15px;'>".$price->name."</td>";
				foreach($list_position as $item){
					$detail = $this->cooperation_model->get_component(array('cooperation_id' => $cooperation_id, 'position_id' => $item, 'selling_price_id' => $price->selling_price_id, 'columns' => 'A.offering_price'));
					if($detail){
						$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'>".nominal_rupiah($detail->offering_price)."</td>";
						$total_price[$item] = $total_price[$item] + $detail->offering_price;
					}else{
						$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'> Rp. 0</td>";
						$total_price[$item] = $total_price[$item] + 0;
					}
				}
				$data['content']		.= "</tr>";				
			}

			$data['content']		.= "<tr style='background-color:#ff6666;'>";
			$data['content']		.= "<td style='padding: 0 15px 0 15px;'><strong>Harga Perpersonil</strong></td>";
			foreach($list_position as $item){
				$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($total_price[$item])."</strong></td>";
			}
			$data['content']		.= "</tr>";

			$total 	= 0;
			$data['content']		.= "<tr style='background-color:#999966;'>";
			$data['content']		.= "<td style='padding: 0 15px 0 15px;'><strong>Harga Perpersonil X Jumlah Personil</strong></th>";
			foreach($list_position as $item){
				$total = $total+$total_price[$item] * $total_personel[$item];
				$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($total_price[$item] * $total_personel[$item])." </strong></th>";
			}
			$data['content']		.= "</tr>";
			$data['content']		.= "<tr style='background-color:#ff9900;'>";
			$data['content']		.= "<td style='padding: 0 15px 0 15px;'><strong>TOTAL HARGA SEBELUM MANAGEMENT FEE</strong></th>";
			for($i=2; $i<=count($list_position); $i++){
				$data['content']	.= "<td style='padding: 0 15px 0 15px;'>&nbsp;</td>";
			}
			$data['content']		.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($total)."</strong></td>";
			$data['content']		.= "</tr>";

			$mf = ($total * $data['cooperation']->management_fee)/100;
			$data['content']		.= "<tr>";
			$data['content']		.= "<th style='padding: 0 15px 0 15px;'>MANAGEMENT FEE ".$data['cooperation']->management_fee."%</th>";
			for($i=2; $i<=count($list_position); $i++){
				$data['content']	.= "<th style='padding: 0 15px 0 15px;'>&nbsp;</th>";
			}
			$data['content']		.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($mf)."</strong></td>";
			$data['content']		.= "</tr>";
			$before_tax				= $total+$mf;
			$data['content']		.= "<tr>";
			$data['content']		.= "<th style='padding: 0 15px 0 15px;'>TOTAL HARGA SEBELUM PAJAK</th>";
			for($i=2; $i<=count($list_position); $i++){
				$data['content']	.= "<th style='padding: 0 15px 0 15px;'>&nbsp;</th>";
			}
			$data['content']		.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($before_tax)."</strong></td>";
			$data['content']		.= "</tr>";
			$ppn					= (($before_tax)*$data['cooperation']->ppn)/100;
			$data['content']		.= "<tr>";
			$data['content']		.= "<th style='padding: 0 15px 0 15px;'>PPN ".$data['cooperation']->ppn."%</th>";
			for($i=2; $i<=count($list_position); $i++){
				$data['content']	.= "<th style='padding: 0 15px 0 15px;'>&nbsp;</th>";
			}
			$data['content']		.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($ppn)."</strong></td>";
			$data['content']		.= "</tr>";
			$ppn_mf					= (($ppn)*$data['cooperation']->management_fee_ppn)/100;
			$data['content']		.= "<tr>";
			$data['content']		.= "<th style='padding: 0 15px 0 15px;'>PEMOTONGAN PPN (DARI MF) ".$data['cooperation']->management_fee_ppn."%</th>";
			for($i=2; $i<=count($list_position); $i++){
				$data['content']	.= "<th style='padding: 0 15px 0 15px;'>&nbsp;</th>";
			}
			$data['content']		.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>-".nominal_rupiah($ppn_mf)."</strong></td>";
			$data['content']		.= "</tr>";
			$total_invoice = $before_tax+$ppn-$ppn_mf;
			$data['content']		.= "<tr style='background-color:yellow;'>";
			$data['content']		.= "<td style='padding: 0 15px 0 15px;'><strong>TOTAL INVOICE PERBULAN</strong></th>";
			for($i=2; $i<=count($list_position); $i++){
				$data['content']	.= "<td style='padding: 0 15px 0 15px;'>&nbsp;</td>";
			}
			$data['content']		.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($total_invoice)."</strong></td>";
			$data['content']		.= "</tr>";
			$rounding_invoice 		= 0;
			$lenght = strlen((int)$total_invoice);
			if($lenght > 2){
				$rounding_invoice = substr((int)$total_invoice,0,$lenght-2);
				$rounding_invoice = $rounding_invoice+1;
				$rounding_invoice = $rounding_invoice."00";
			}
			$data['content']		.= "<tr style='background-color:yellow;'>";
			$data['content']		.= "<td style='padding: 0 15px 0 15px;'><strong>PEMBULATAN</strong></th>";
			for($i=2; $i<=count($list_position); $i++){
				$data['content']	.= "<td style='padding: 0 15px 0 15px;'>&nbsp;</td>";
			}
			$data['content']		.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($rounding_invoice)."</strong></td>";
			$data['content']		.= "</tr>";
			$data['content']		.= "</table>";
		}

		$this->load->view('cooperation/preview_contract', $data);
	}

	public function create($file_type = FALSE, $cooperation_id = FALSE){
		if($file_type == 'pdf'){
			$files = glob(FCPATH."files/cooperation_pdf/*");
			foreach($files as $file){
				gc_collect_cycles();
				if(is_file($file)) {
					unlink($file);
				}
			}
			$data['basepath'] 	= FCPATH."files/cooperation_pdf/";
		}else if($file_type == 'word'){
			$files = glob(FCPATH."files/cooperation_word/*");
			foreach($files as $file){
				gc_collect_cycles();
				if(is_file($file)) {
					unlink($file);
				}
			}
			$data['basepath'] 	= FCPATH."files/cooperation_word/";
		}else{
			redirect(base_url('cooperation/list'));
		}

		$this->load->model(array('cooperation_model', 'sales_template_model', 'position_model'));
		$total_personel			= array();
		$total_price			= array();

		$template = $this->sales_template_model->get(array('id' => 1, 'columns' => 'A.content'));
		if($template){
			$data['content']	= $template->content;
		}
		$cooperation	= $this->cooperation_model->gets(array('id' => $cooperation_id, 'columns' => 'A.position_id, A.management_fee, A.ppn, A.management_fee_ppn, B.name AS company_name, B.address AS company_address, D.name AS site_name, D.address AS site_address'));
	 	if($cooperation){
			$data['cooperation'] 	=  $cooperation[0];
			if($template){
				$data['content'] 		= str_replace("{BISNIS_UNIT}", $data['cooperation']->company_name, $data['content']);
				$data['content'] 		= str_replace("{ALAMAT_BISNIS_UNIT}", $data['cooperation']->company_address, $data['content']);
				$data['content'] 		= str_replace("{SITE_BISNIS}", $data['cooperation']->site_name, $data['content']);
				$data['content'] 		= str_replace("{ALAMAT_SITE_BISNIS}", $data['cooperation']->site_address, $data['content']);
			}else{
				$data['content'] 		= str_replace("{BISNIS_UNIT}", '', $data['content']);
				$data['content'] 		= str_replace("{ALAMAT_BISNIS_UNIT}", '', $data['content']);
				$data['content'] 		= str_replace("{SITE_BISNIS}", '', $data['content']);
				$data['content'] 		= str_replace("{ALAMAT_SITE_BISNIS}", '', $data['content']);
			}
			$list_position 			= explode(",",$data['cooperation']->position_id);
			$data['content']		.= "<p>&nbsp;</p>";
			$data['content']		.= "<table width='100%' border='1' style='margin:10px'>";
			$data['content']		.= "<tr style='background-color:yellow;'>";
			$data['content']		.= "<th style='padding: 0 15px 0 15px;'>".$data['cooperation']->site_name."</th>";
			foreach($list_position as $item){
				$position = $this->position_model->get(array('id' => $item, 'columns' => 'A.name'));
				$data['content']	.= "<th style='padding: 0 15px 0 15px;'>".$position->name."</th>";
			}
			$data['content']		.= "</tr>";
			$data['content']		.= "<tr>";
			$data['content']		.= "<td style='padding: 0 15px 0 15px;'>HEADCOUNT</td>";
			foreach($list_position as $item){
				$detail = $this->cooperation_model->get_detail(array('cooperation_id' => $cooperation_id, 'position_id' => $item, 'columns' => 'A.headcount'));
				if($detail){
					$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'>".$detail->headcount."</td>";
					$total_personel[$item] = $detail->headcount;
				}else{
					$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'>0</td>";
					$total_personel[$item] = 0;
				}
			}
			$data['content']		.= "</tr>";
			$data['content']		.= "<tr>";
			$data['content']		.= "<td style='padding: 0 15px 0 15px;'>Gaji Pokok</td>";
			foreach($list_position as $item){
				$detail = $this->cooperation_model->get_detail(array('cooperation_id' => $cooperation_id, 'position_id' => $item, 'columns' => 'A.salary'));
				if($detail){
					$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'>".nominal_rupiah($detail->salary)."</td>";
					$total_price[$item] = $detail->salary;
				}else{
					$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'>Rp. 0</td>";
					$total_price[$item] = 0;
				}
			}
			$data['content']		.= "</tr>";
			$list_price = $this->cooperation_model->gets_selling_price(array('cooperation_id' => $cooperation_id, 'list_position' => $list_position, 'columns' => 'A.selling_price_id, B.name', 'groupby' => 'A.selling_price_id'));
			foreach($list_price as $price){
				$data['content']		.= "<tr>";
				$data['content']		.= "<td style='padding: 0 15px 0 15px;'>".$price->name."</td>";
				foreach($list_position as $item){
					$detail = $this->cooperation_model->get_component(array('cooperation_id' => $cooperation_id, 'position_id' => $item, 'selling_price_id' => $price->selling_price_id, 'columns' => 'A.offering_price'));
					if($detail){
						$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'>".nominal_rupiah($detail->offering_price)."</td>";
						$total_price[$item] = $total_price[$item] + $detail->offering_price;
					}else{
						$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'> Rp. 0</td>";
						$total_price[$item] = $total_price[$item] + 0;
					}
				}
				$data['content']		.= "</tr>";				
			}

			$data['content']		.= "<tr style='background-color:#ff6666;'>";
			$data['content']		.= "<td style='padding: 0 15px 0 15px;'><strong>Harga Perpersonil</strong></td>";
			foreach($list_position as $item){
				$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($total_price[$item])."</strong></td>";
			}
			$data['content']		.= "</tr>";

			$total 	= 0;
			$data['content']		.= "<tr style='background-color:#999966;'>";
			$data['content']		.= "<td style='padding: 0 15px 0 15px;'><strong>Harga Perpersonil X Jumlah Personil</strong></th>";
			foreach($list_position as $item){
				$total = $total+$total_price[$item] * $total_personel[$item];
				$data['content']	.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($total_price[$item] * $total_personel[$item])." </strong></th>";
			}
			$data['content']		.= "</tr>";
			$data['content']		.= "<tr style='background-color:#ff9900;'>";
			$data['content']		.= "<td style='padding: 0 15px 0 15px;'><strong>TOTAL HARGA SEBELUM MANAGEMENT FEE</strong></th>";
			for($i=2; $i<=count($list_position); $i++){
				$data['content']	.= "<td style='padding: 0 15px 0 15px;'>&nbsp;</td>";
			}
			$data['content']		.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($total)."</strong></td>";
			$data['content']		.= "</tr>";

			$mf = ($total * $data['cooperation']->management_fee)/100;
			$data['content']		.= "<tr>";
			$data['content']		.= "<th style='padding: 0 15px 0 15px;'>MANAGEMENT FEE ".$data['cooperation']->management_fee."%</th>";
			for($i=2; $i<=count($list_position); $i++){
				$data['content']	.= "<th style='padding: 0 15px 0 15px;'>&nbsp;</th>";
			}
			$data['content']		.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($mf)."</strong></td>";
			$data['content']		.= "</tr>";
			$before_tax				= $total+$mf;
			$data['content']		.= "<tr>";
			$data['content']		.= "<th style='padding: 0 15px 0 15px;'>TOTAL HARGA SEBELUM PAJAK</th>";
			for($i=2; $i<=count($list_position); $i++){
				$data['content']	.= "<th style='padding: 0 15px 0 15px;'>&nbsp;</th>";
			}
			$data['content']		.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($before_tax)."</strong></td>";
			$data['content']		.= "</tr>";
			$ppn					= (($before_tax)*$data['cooperation']->ppn)/100;
			$data['content']		.= "<tr>";
			$data['content']		.= "<th style='padding: 0 15px 0 15px;'>PPN ".$data['cooperation']->ppn."%</th>";
			for($i=2; $i<=count($list_position); $i++){
				$data['content']	.= "<th style='padding: 0 15px 0 15px;'>&nbsp;</th>";
			}
			$data['content']		.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($ppn)."</strong></td>";
			$data['content']		.= "</tr>";
			$ppn_mf					= (($ppn)*$data['cooperation']->management_fee_ppn)/100;
			$data['content']		.= "<tr>";
			$data['content']		.= "<th style='padding: 0 15px 0 15px;'>PEMOTONGAN PPN (DARI MF) ".$data['cooperation']->management_fee_ppn."%</th>";
			for($i=2; $i<=count($list_position); $i++){
				$data['content']	.= "<th style='padding: 0 15px 0 15px;'>&nbsp;</th>";
			}
			$data['content']		.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>-".nominal_rupiah($ppn_mf)."</strong></td>";
			$data['content']		.= "</tr>";
			$total_invoice = $before_tax+$ppn-$ppn_mf;
			$data['content']		.= "<tr style='background-color:yellow;'>";
			$data['content']		.= "<td style='padding: 0 15px 0 15px;'><strong>TOTAL INVOICE PERBULAN</strong></th>";
			for($i=2; $i<=count($list_position); $i++){
				$data['content']	.= "<td style='padding: 0 15px 0 15px;'>&nbsp;</td>";
			}
			$data['content']		.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($total_invoice)."</strong></td>";
			$data['content']		.= "</tr>";
			$rounding_invoice 		= 0;
			$lenght = strlen((int)$total_invoice);
			if($lenght > 2){
				$rounding_invoice = substr((int)$total_invoice,0,$lenght-2);
				$rounding_invoice = $rounding_invoice+1;
				$rounding_invoice = $rounding_invoice."00";
			}
			$data['content']		.= "<tr style='background-color:yellow;'>";
			$data['content']		.= "<td style='padding: 0 15px 0 15px;'><strong>PEMBULATAN</strong></th>";
			for($i=2; $i<=count($list_position); $i++){
				$data['content']	.= "<td style='padding: 0 15px 0 15px;'>&nbsp;</td>";
			}
			$data['content']		.= "<td align='right' style='padding: 0 15px 0 15px;'><strong>".nominal_rupiah($rounding_invoice)."</strong></td>";
			$data['content']		.= "</tr>";
			$data['content']		.= "</table>";

			$data['title'] =  preg_replace("/[^A-Za-z0-9 -]/", "", strtolower($data['cooperation']->site_name));
			if($file_type == 'pdf'){
				$this->generate_PDF($data);
			}else if($file_type == 'word'){
				$this->generate_Word($data);
			}
		}
		redirect(base_url('cooperation/list'));
	}

	private function generate_Word($data = array()){
		$this->load->library('Word');
		$filename = strtolower(str_replace(" ", "_",$data['title']));
		$phpWord = new PHPWord();
		$phpWord->createDoc($data['content'],$filename,1);
		die();
	}

	public function save_offer()
	{
		if($this->input->post()){
			$this->load->model(array('cooperation_model', 'cooperation_history_model'));
		
			$id 		= $this->input->post('id');
			foreach ($id as $cooperation_id) {
				$cooperation = (array) $this->cooperation_model->get(array('id' => $cooperation_id));
				$this->cooperation_history_model->save($cooperation);

				$cooperation_detail = $this->cooperation_model->gets_detail_item(array('cooperation_id' => $cooperation_id));
				foreach ($cooperation_detail as $detail) {
					$this->cooperation_history_model->save_detail((array)$detail);
				}
				
				$cooperation_component = $this->cooperation_model->gets_component(array('cooperation_id' => $cooperation_id));
				foreach ($cooperation_component as $component) {
					$this->cooperation_history_model->save_component((array)$component);
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>History Penawaran berhasil disimpan.','success'));
		}
		redirect(base_url('cooperation/list'));
	}

	public function save_mou()
	{
		if($this->input->post()){
			$this->load->model(array('cooperation_model', 'cooperation_history_model'));
		
			$id 		= $this->input->post('id');
			foreach ($id as $cooperation_id) {
				$this->cooperation_model->save(array('id' => $cooperation_id, 'status' => 'Pengajuan MOU'));
				$this->cooperation_history_model->save_approval(array('cooperation_id' => $cooperation_id, 'status' => 'Pengajuan MOU'));
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>Pengajuan MOU berhasil.','success'));
		}
		redirect(base_url('cooperation/list'));
	}

	public function process($action = FALSE)
	{
		if($this->input->post()){
			$this->load->model(array('cooperation_model', 'cooperation_history_model'));
		
			$id 		= $this->input->post('id');
			foreach ($id as $cooperation_id) {
				if($action == 'cancel'){
					$this->cooperation_model->save(array('id' => $cooperation_id, 'status' => 'Penawaran'));
					$this->cooperation_history_model->save_approval(array('cooperation_id' => $cooperation_id, 'status' => 'Dibatalkan'));
				}else{
					continue;
				}

			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>Pengajuan MOU berhasil.','success'));
		}
		redirect(base_url('cooperation/approval'));
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ketelitian extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role   = array_merge($role, $this->config->item('employee'));
		$role   = array_merge($role, $this->config->item('recruitment'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function form() {
		$this->load->model(array('ketelitian_model','psikotes_model','vacancy_model'));
	
		$employeeId = $this->session->userdata('employee_id');
		$data['data_employee'] 			= $this->psikotes_model->getEmployeeInfo($employeeId);
		$data['list_nomor']				= $this->ketelitian_model->getSoal();
		$data['vacancy_id'] 			= $_GET['vacancy'];
		$data['data_vacancy']			= $this->vacancy_model->getDataForTes($_GET['vacancy']);

		$data['_TITLE_'] 		= 'Ketelitian';
		$data['_PAGE_'] 		= 'ketelitian/form';
		$data['_MENU_PARENT_'] 	= 'tes_online';
		$data['_MENU_'] 		= 'ketelitian';
		return $this->view($data);
	}

	public function saveform(){
		$this->load->model(array('ketelitian_model','vacancy_model','history_tes_model'));
		if($this->input->post()){
			$dataVacancy 						= $this->vacancy_model->getDataForTes($this->input->post('vacancy_id'));

			// Data Master
			$data['m_employee_id'] 	= $this->session->userdata('employee_id');
			$data['company_id'] 	= $dataVacancy[0]->company_id;
			$data['branch_id'] 		= $dataVacancy[0]->branch_id;
			$data['site_id'] 		= $dataVacancy[0]->site_id;
			$data['province_id'] 	= $dataVacancy[0]->province_id;
			$data['position_id'] 	= $dataVacancy[0]->position_id;
			$data['nama'] 			= $this->input->post('full_name');
			$data['usia']			= $this->input->post('umur');
			$data['jk']				= $this->input->post('jk');
			$data['tgl_tes']		= $this->input->post('tgl_tes');
			$data['tes_dimulai']	= $this->input->post('tes_mulai');
			$data['m_vacancy_id']	= $this->input->post('vacancy_id');
			$data['tes_selesai']	= date('Y-m-d H:i:s');
			$data['id']				= '';
			$save_id				= $this->ketelitian_model->save($data);

			//save detail
			$dataNomor 		= $this->ketelitian_model->getSoal();
			$jawabanBenar 	= 0;
			$jawabanSalah 	= 0;
			$tidakDiisi 	= 0;
			foreach ($dataNomor as $key => $element) {
				$datad['id']						='';
				$datad['t_ketelitian_id']			= $save_id;
				$datad['m_ketelitian_id']			= $element->id;

				$nval 		= 'soal_'.$element->id;
				$jawaban 	= null;
				$isBenar 	= 0;
				if ($this->input->post($nval)!=null) {
					$jawaban = $this->input->post($nval);

					if($jawaban == $element->jawaban){
						$isBenar=1;
						$jawabanBenar++;
					}else{
						$jawabanSalah++;
					}
				}else{
					$tidakDiisi++;
				}

				$datad['jawaban']					= $jawaban;
				$datad['is_benar']					= $isBenar;
				$save_d_id							= $this->ketelitian_model->saveD($datad);
			}

			//update main
			$dataUpdate['jawaban_benar']		= $jawabanBenar;
			$dataUpdate['jawaban_salah']		= $jawabanSalah;
			$dataUpdate['tidak_diisi']			= $tidakDiisi;
			$persentase 						= $jawabanBenar/count($dataNomor)*100;
			$dataUpdate['persentase']			= $persentase;
			$dataUpdate['id']					= $save_id;

			if($persentase >= $dataVacancy[0]->min_lolos_ketelitian){
				$dataUpdate['kategori']			= "LOLOS";
			}else{
				$dataUpdate['kategori']			= "TIDAK LOLOS";
			}

			$updateId	= $this->ketelitian_model->updateHasil($dataUpdate);

			//save history
			$history['tes']					='ketelitian';
			$history['doc_id']				=$save_id;
			$history['company_id']			=$data['company_id'] ;
			$history['branch_id']			=$data['branch_id'] ;
			$history['site_id']				=$data['site_id'] ;
			$history['m_employee_id']		=$data['m_employee_id'] ;
			$history['province_id']			=$data['province_id'] ;
			$history['position_id']			=$data['position_id'] ;
			$history['m_vacancy_id']		=$data['m_vacancy_id'] ;
			$history['nama']				=$data['nama'] ;
			$history['usia']				=$data['usia'] ;
			$history['jk']					=$data['jk'] ;
			$history['tgl_tes']				=$data['tgl_tes'] ;
			$history['tes_dimulai']			=$data['tes_dimulai'] ;
			$history['tes_selesai']			=$data['tes_selesai'] ;
			$history['jawaban_benar']		=$dataUpdate['jawaban_benar'] ;
			$history['jawaban_salah']		=$dataUpdate['jawaban_salah'] ;
			$history['tidak_diisi']			=$dataUpdate['tidak_diisi'] ;
			$history['persentase_minimal']	=$dataVacancy[0]->min_lolos_deret_angka ;
			$history['persentase']			=$dataUpdate['persentase'];
			$history['kategori']			=$dataUpdate['kategori'];
			$history_id						= $this->history_tes_model->save($history);
			
			if (!$save_id) {
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));		
				redirect(base_url('appeal/applied'));
				die();
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));		
			redirect(base_url('appeal/applied'));

		}
	}

	public function result ($ketelitian_id){
		$this->load->model(array('psikotes_model','ketelitian_model'));
		$data['data_ketelitian']		= $this->ketelitian_model->get_data_ketelitian($ketelitian_id);
		
		if (empty($data['data_ketelitian'])){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data Ketelitian tidak ditemukan.','danger'));
			redirect(base_url('vacancy/applicant'));
		}

		$data['data_ketelitian_d']		= $this->ketelitian_model->get_data_ketelitian_d($ketelitian_id);	
		$data['data_employee'] 			= $this->psikotes_model->getEmployeeInfo($data['data_ketelitian'][0]->m_employee_id);
		$data['is_hanya_hasil']			= $this->input->get('is_hanya_hasil');

		//update petugas
		$data['petugas']				= $this->session->userdata('name');

		$data['_TITLE_'] 		= 'Hasil Ketelitian '.$data['data_ketelitian'][0]->nama;
		$data['_PAGE_'] 		= 'ketelitian/result';
		$data['_MENU_PARENT_'] 	= 'tes_online';
		$data['_MENU_'] 		= 'ketelitian';
		return $this->view($data);
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_template extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('recruitment'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		$role 	= array_merge($role, $this->config->item('benefit'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,4,7,5,6,11,12,13,14))){
			redirect(base_url());
		}
	}

	public function delete($template_id = false)
	{
		$this->load->model('report_template_model');
		if ($template_id)
		{
			$data =  $this->report_template_model->get(array('id' => $template_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $template_id, 'is_active' => 0);
				$result = $this->report_template_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('report_template/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('report_template/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('report_template/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('report_template/list'));
		}
	}

	public function preview($template_id = FALSE)
	{
		$this->load->model('report_template_model');
		$data['_TITLE_'] 		= 'Preview Template Laporan';
		$data['_PAGE_']	 		= 'report_template/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'report_template';

		$data['id'] = $template_id;

		
		if (!$template_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('report_template/list'));
		}

		$data['preview'] = $this->report_template_model->preview(array('id' => $template_id));
		$this->load->view('report_template/preview', $data);
	}

	public function form($template_id = FALSE)
	{
		$this->load->model(array('report_template_model'));

		$data['id'] 			= '';
		$data['title']			= '';
		$data['content']		= '';

		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['title'] 		= $this->input->post('title');
			$data['content']	= $this->input->post('content');
			
			$this->form_validation->set_rules('title', '', 'required');
			$this->form_validation->set_rules('content', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{				
				$save_id	 	= $this->report_template_model->save($data);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('report_template/list'));
		}

		if ($template_id)
		{
			$data = (array) $this->report_template_model->get(array('id' => $template_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('report_template/list'));
			}
		}
		$data['_TITLE_'] 		= 'Form Template Laporan';
		$data['_PAGE_'] 		= 'report_template/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'report_template';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Template Laporan';
		$data['_PAGE_'] 		= 'report_template/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'report_template';
		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('report_template_model'));

		$column_index = $_POST['order'][0]['column']; 
		if(in_array($this->session->userdata('role'), $this->config->item('recruitment'))){
			$params['type']		= 'recruitment';
		}
		if(in_array($this->session->userdata('role'), $this->config->item('personalia'))){
			$params['type']		= 'personalia';
		}
		if(in_array($this->session->userdata('role'), $this->config->item('payroll'))){
			$params['type']		= 'payroll';
		}
		if(in_array($this->session->userdata('role'), $this->config->item('benefit'))){
			$params['type']		= 'benefit';
		}
		$params['columns'] 		= 'A.id, A.title';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['title']	= $_POST['columns'][1]['search']['value'];

		$list 	= $this->report_template_model->gets($params);
		$total 	= $this->report_template_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['title'] 		= $item->title;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("report_template/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("report_template/form/".$item->id).'">Ubah</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}

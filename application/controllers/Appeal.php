<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appeal extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
	}
	public function apply($vacancy_id = FALSE){
		if($vacancy_id){
			if($this->session->userdata('employee_id')){
				$this->load->model(array('applicant_model'));
				$application = $this->applicant_model->get(array('employee_id' => $this->session->userdata('employee_id'), 'vacancy_id' => $vacancy_id));
				if($application){
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Lowongan sudah dilamar.','danger'));
				}else{
					$this->applicant_model->save(array('employee_id' => $this->session->userdata('employee_id'), 'vacancy_id' => $vacancy_id, 'question' => ''));
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  Lamaran berhasil disimpan.','success'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Id Karyawan Tidak ditemukan.','danger'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Lowongan tidak ditemukan.','danger'));
		}
		redirect(base_url('appeal/vacancy'));
	}

	public function apply_form($vacancy_id = FALSE){
		$this->load->model(array('vacancy_model', 'applicant_model'));

		$data['vacancy_id']		= $vacancy_id;

		if($this->input->post()){
			$data['vacancy_id'] = $this->input->post('vacancy_id');
			$list_question 			= $this->input->post('question');
			$list_answer 			= $this->input->post('answer');
			
			$this->form_validation->set_rules('vacancy_id', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$questioner = '';
				foreach ($list_question AS $key => $value ) {
					$no = ($key+1);
					$quest = '<p><strong>Pertanyaan '.$no.': </strong>'.$list_question[$key].'<br><strong>Jawaban '.$no.' : </strong>'.$list_answer[$key].'</p><hr>';
					$questioner .= $quest; 
				}

				$this->applicant_model->save(array('employee_id' => $this->session->userdata('employee_id'), 'vacancy_id' => $vacancy_id, 'question' => $questioner));
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  Lamaran berhasil disimpan.','success'));
			}
			redirect(base_url('appeal/vacancy'));
		}

		if (!$vacancy_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('appeal/vacancy'));
		}
		$data['vacancy']		= $this->vacancy_model->get(array('id' => $vacancy_id, 'columns' => 'A.id, A.title'));
		$data['list_question']	= $this->vacancy_model->question_gets(array('vacancy_id' => $vacancy_id, 'columns' => 'A.id, A.question', 'orderby' => 'A.id', 'order' => 'DESC'));
		$data['_TITLE_'] 		= 'Pertanyaan';
		$data['_PAGE_'] 		= 'appeal/apply_form';
		$data['_MENU_PARENT_'] 	= 'appeal';
		$data['_MENU_'] 		= 'vacancy';
		return $this->view($data);
	}

	public function applied(){
		$data['_TITLE_'] 		= 'Lamaran Aktif';
		$data['_PAGE_'] 		= 'appeal/applied';
		$data['_MENU_PARENT_'] 	= 'appeal';
		$data['_MENU_'] 		= 'applied';
		
		$this->load->model(array('vacancy_model', 'applicant_model','psikotes_model','deret_angka_model','hitung_cepat_model','ketelitian_model','kraepelin_model'));

		$vacancy['active'] 		= true;
				$vacancy['columns'] 	= 'B.id, B.title, B.site_id, C.name AS province_name, DATE_FORMAT(A.created_at, "%d/%m/%Y") AS apply_date,B.is_tes_psikotes,B.is_tes_deret_angka,B.is_tes_hitung_cepat,B.durasi_psikotes,B.durasi_hitung_cepat,B.durasi_deret_angka,B.is_ketelitian,B.durasi_ketelitian,B.is_kraepelin,B.durasi_kolom_kraepelin,if(B.end_date<=date(now()),1,0) as tes_berakhir';
		$vacancy['employee_id'] = $this->session->userdata('employee_id');
		
	
		$list					= $this->vacancy_model->apply_gets($vacancy);
		$data['list_belum_tes']	= $this->applicant_model->getDataBelumTes($this->session->userdata('employee_id'));

		$list_vacancy 	= array();
		$i = 1;
		foreach($list as $item)
		{	
			$result['no'] 					= $i;
			$result['id']					= $item->id;
			$result['title']				= $item->title;
			$result['province_name']		= $item->province_name;
			$result['apply_date']			= $item->apply_date;

			$result['is_tes_psikotes']		= $item->is_tes_psikotes;
			$result['is_done_psikotes']		= 0;
			$result['durasi_psikotes']		= $item->durasi_psikotes;
			$result['is_tes_deret_angka']	= $item->is_tes_deret_angka;
			$result['is_done_deret_angka']	= 0;
			$result['durasi_deret_angka']	= $item->durasi_deret_angka;
			$result['is_tes_hitung_cepat']	= $item->is_tes_hitung_cepat;
			$result['is_done_hitung_cepat']	= 0;
			$result['durasi_hitung_cepat']	= $item->durasi_hitung_cepat;
			$result['is_ketelitian']		= $item->is_ketelitian;
			$result['is_done_ketelitian']	= 0;
			$result['durasi_ketelitian']	= $item->durasi_ketelitian;
			$result['is_kraepelin']			= $item->is_kraepelin;
			$result['is_done_kraepelin']	= 0;
			$result['durasi_kolom_kraepelin']	= $item->durasi_kolom_kraepelin;

			$result['followup_status']		= "";
			$result['followup_description']	= "";

			$result['intruksi_psikotes']	= $this->applicant_model->getInstruksiTes("psikotes");
			$result['intruksi_deret_angka'] = $this->applicant_model->getInstruksiTes("deret_angka");
			$result['intruksi_hitung_cepat'] = $this->applicant_model->getInstruksiTes("hitung_cepat");
			$result['intruksi_ketelitian']	= $this->applicant_model->getInstruksiTes("ketelitian");
			$result['intruksi_kraepelin']	= $this->applicant_model->getInstruksiTes("kraepelin");

			$followup = $this->vacancy_model->last_status(array('employee_id' => $item->id, 'site_id' => $item->site_id, 'columns' => 'A.status_approval, A.note'));
			if($followup){	
				$result['followup_status']		= $followup->status_approval;
				$result['followup_description']	= $followup->note;
		}

			//cek apakah sudah dikerjakan
			if($item->is_tes_psikotes==1){
				$psikotesData = $this->psikotes_model->count_psikotes_vacancy($vacancy['employee_id'],$item->id);
				if(!empty($psikotesData)){
					$result['is_done_psikotes']		= 1;
				}
			}

			if($item->is_tes_deret_angka==1){
				$psikotesData = $this->deret_angka_model->count_deret_angka_vacancy($vacancy['employee_id'],$item->id);
				if(!empty($psikotesData)){
					$result['is_done_deret_angka']	= 1;
				}
			}

			if($item->is_tes_hitung_cepat==1){
				$psikotesData = $this->hitung_cepat_model->count_hitung_cepat_vacancy($vacancy['employee_id'],$item->id);
				if(!empty($psikotesData)){
					$result['is_done_hitung_cepat']	= 1;
				}
			}

			if($item->is_ketelitian==1){
				$datas = $this->ketelitian_model->count_ketelitian_vacancy($vacancy['employee_id'],$item->id);
				if(!empty($datas)){
					$result['is_done_ketelitian']	= 1;
				}
			}
			
			if($item->is_kraepelin==1){
				$datas = $this->kraepelin_model->count_kraepelin_vacancy($vacancy['employee_id'],$item->id);
				if(!empty($datas)){
					$result['is_done_kraepelin']	= 1;
				}
			}

			if($item->tes_berakhir==1){
				$result['is_tes_psikotes']		= 0;
				$result['is_tes_deret_angka']	= 0;
				$result['is_tes_hitung_cepat']	= 0;
				$result['is_ketelitian']		= 0;
				$result['is_kraepelin']			= 0;
			}

			array_push($list_vacancy, $result);
			$i++;
		}

		$data['vacancy'] 		= $list_vacancy;
		$this->view($data);
	}

	public function vacancy(){
		$data['_TITLE_'] 		= 'Daftar Lowongan';
		$data['_PAGE_'] 		= 'appeal/vacancy';
		$data['_MENU_PARENT_'] 	= 'appeal';
		$data['_MENU_'] 		= 'vacancy';
		
		$this->load->model(array('vacancy_model', 'province_model', 'applicant_model', 'employee_model'));
		
		$candidate 		= $this->employee_model->get(array('id' => $this->session->userdata('employee_id'), 'columns' => 'A.followup_status'));
		$data['blokir'] = "";
		if($candidate){
			if($candidate->followup_status === 'Blokir'){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Akun Anda Diblokir.','danger'));
				redirect(base_url('appeal/applied'));
			}
		}

		$data['list_province']	= $this->province_model->gets(array('columns' => 'A.id, A.name, B.name AS province_name'));
		$data['list_belum_tes']	= $this->applicant_model->getDataBelumTes($this->session->userdata('employee_id'));

		$data['province_id']	= '';
		$data['search']			= '';

		if($this->input->get()){
			$vacancy['province_id'] = $data['province_id']	= $this->input->get('province');
			$vacancy['search'] 		= $data['search']		= $this->input->get('search');
		}
		$vacancy['active'] 		 = true;
		$vacancy['columns'] 	 = 'A.id, A.title, A.content, B.name AS province_name, C.name AS branch_name, DATE_FORMAT(A.end_date, "%W") AS day, DATE_FORMAT(A.end_date, "%d %m %Y") AS date, A.flyer,A.is_tes_psikotes,A.is_tes_deret_angka,A.is_tes_hitung_cepat,A.is_ketelitian,A.is_kraepelin';
		$this->load->library('pagination');
		$config['base_url'] 	= base_url().'appeal/vacancy/';
		$config['total_rows'] 	= $this->vacancy_model->gets($vacancy, TRUE);
		$config['per_page'] 	= 5;

		$config['full_tag_open']    = '<ul class="pagination pagination-sm">';
        $config['full_tag_close']   = '</ul>';
        $config['num_tag_open']     = '<li class="page-item">';
        $config['num_tag_close']    = '</li>';
        $config['cur_tag_open']     = '<li class="page-item active"><a href="#" class="page-link">';
        $config['cur_tag_close']    = "</a></li>";
        $config['attributes'] 		= array('class' => 'page-link');
		if ($this->input->get()){
			$config['suffix'] = '?province=' . $this->input->get('province').'&search=' . $this->input->get('search');	
		}
        $this->pagination->initialize($config); 

		$vacancy['page'] 		= $this->uri->segment(3);
		$vacancy['limit']		= $config['per_page'];
		$vacancy		= $this->vacancy_model->gets($vacancy);

		$result 	= array();
		foreach($vacancy as $item)
		{

			$item->apply = false;
			$item->question = false;
			
			$application = $this->applicant_model->get(array('employee_id' => $this->session->userdata('employee_id'), 'vacancy_id' => $item->id));
			if($application){
				$item->apply = true;
			}

			$question = $this->vacancy_model->question_get(array('vacancy_id' => $item->id));
			if($question){
				$item->question = true;
			}

			// text tes online
			$txt ="";
			if($item->is_tes_psikotes == 1 || $item->is_tes_deret_angka == 1 || $item->is_tes_hitung_cepat== 1 || $item->is_ketelitian== 1|| $item->is_kraepelin== 1) {
				$txt = "( Tes Online : <strong>";
				if ($item->is_tes_psikotes == 1) {
					$txt = $txt." Disc ,";
				}
				if ($item->is_tes_deret_angka == 1) {
					$txt = $txt." Deret Angka ,";
				}

				if ($item->is_tes_hitung_cepat == 1) {
					$txt = $txt." Hitung Cepat ,";
				}

				if ($item->is_ketelitian == 1) {
					$txt = $txt." Ketelitian ,";
				}
				if ($item->is_kraepelin == 1) {
					$txt = $txt." Kraepelin ,";
				}
				$txt = substr($txt, 0, -1);
				$txt = $txt." </strong> )";
			};
			
			$item->txt_tes_online = $txt;
			array_push($result, $item);
		}
		$data['vacancy'] = $result;
		$this->view($data);
	}
}
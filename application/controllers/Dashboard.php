<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function contract(){
		$this->load->model(array('employee_model', 'roresign_model', 'employee_contract_model'));
		$branch_id = '';
		if($this->session->userdata('branch') > 1){
			$branch_id = $this->session->userdata('branch');
		}
		$contract['Kontrak Habis'] 	= array('day' => 0, 'summary' => $this->employee_contract_model->gets_expired(array('expired' => 0, 'columns' => 'COUNT(A.employee_id) AS summary, C.name AS site_name, D.code AS company_code, E.name AS branch_name')));

		for($i=1; $i <= 30;$i++){
			$expired = $this->employee_contract_model->gets_expired(array('expired' => $i, 'columns' => 'COUNT(A.employee_id) AS summary, C.name AS site_name, D.code AS company_code, E.name AS branch_name'));
			if(!empty($expired)){
				$detail['day']		= $i;
				$detail['summary']	= $expired; 
				$contract['Kontrak Kurang '.$i.' Hari'] 	= $detail;
			}
		}

		$data['list_expired']		= $contract;
		$data['_PAGE_'] 			= 'dashboard/contract';;
		$data['_TITLE_'] 			= 'Dashboard Kontrak';
		$data['_MENU_PARENT_'] 		= 'dashboard_contract';
		$data['_MENU_'] 			= 'dashboard_contract';

		$this->view($data);
	}

	public function progress(){
		$data['_PAGE_'] 			= 'dashboard/chart_progress';
		$data['_TITLE_'] 			= 'Dashboard Progress';
		$data['_MENU_PARENT_'] 		= 'dashboard_progress';
		$data['_MENU_'] 			= 'dashboard_progress';
		$this->load->model(array('site_model', 'employee_model'));
		$data['site_east'] 		= $this->site_model->gets(array('branch_id' => 2), TRUE);
		$data['site_west'] 		= $this->site_model->gets(array('branch_id' => 3), TRUE);
		$data['site_central'] 	= $this->site_model->gets(array('branch_id' => 4), TRUE);
		$data['site_makasar'] 	= $this->site_model->gets(array('branch_id' => 5), TRUE);
		$data['site_bali'] 		= $this->site_model->gets(array('branch_id' => 8), TRUE);

		$data['employee_east'] 		= $this->employee_model->count(array('branch_id' => 2, 'status_approval' => 3, 'status_nonjob' => 0 , 'not_site_id' => 2), TRUE);
		$data['employee_west'] 		= $this->employee_model->count(array('branch_id' => 3, 'status_approval' => 3, 'status_nonjob' => 0 , 'not_site_id' => 2), TRUE);
		$data['employee_central'] 	= $this->employee_model->count(array('branch_id' => 4, 'status_approval' => 3, 'status_nonjob' => 0 , 'not_site_id' => 2), TRUE);
		$data['employee_makasar'] 	= $this->employee_model->count(array('branch_id' => 5, 'status_approval' => 3, 'status_nonjob' => 0 , 'not_site_id' => 2), TRUE);
		$data['employee_bali'] 		= $this->employee_model->count(array('branch_id' => 8, 'status_approval' => 3, 'status_nonjob' => 0 , 'not_site_id' => 2), TRUE);

		$data['security_east'] 		= $this->employee_model->count(array('branch_id' => 2, 'status_approval' => 3, 'status_nonjob' => 0 , 'position_type' => 'Security', 'not_site_id' => 2), TRUE);
		$data['security_west'] 		= $this->employee_model->count(array('branch_id' => 3, 'status_approval' => 3, 'status_nonjob' => 0 , 'position_type' => 'Security', 'not_site_id' => 2), TRUE);
		$data['security_central'] 	= $this->employee_model->count(array('branch_id' => 4, 'status_approval' => 3, 'status_nonjob' => 0 , 'position_type' => 'Security', 'not_site_id' => 2), TRUE);
		$data['security_makasar'] 	= $this->employee_model->count(array('branch_id' => 5, 'status_approval' => 3, 'status_nonjob' => 0 , 'position_type' => 'Security', 'not_site_id' => 2), TRUE);
		$data['security_bali'] 		= $this->employee_model->count(array('branch_id' => 8, 'status_approval' => 3, 'status_nonjob' => 0 , 'position_type' => 'Security', 'not_site_id' => 2), TRUE);

		$data['indirect_east'] 		= $this->employee_model->count(array('employee_branch' => 2, 'status_approval' => 3, 'status_nonjob' => 0 , 'site_id' => 2), TRUE);
		$data['indirect_west'] 		= $this->employee_model->count(array('employee_branch' => 3, 'status_approval' => 3, 'status_nonjob' => 0 , 'site_id' => 2), TRUE);
		$data['indirect_central'] 	= $this->employee_model->count(array('employee_branch' => 4, 'status_approval' => 3, 'status_nonjob' => 0 , 'site_id' => 2), TRUE);
		$data['indirect_makasar'] 	= $this->employee_model->count(array('employee_branch' => 5, 'status_approval' => 3, 'status_nonjob' => 0 , 'site_id' => 2), TRUE);
		$data['indirect_bali'] 		= $this->employee_model->count(array('employee_branch' => 8, 'status_approval' => 3, 'status_nonjob' => 0 , 'site_id' => 2), TRUE);

		$data['nonsecurity_east'] 	= $data['employee_east']  - $data['security_east'];
		$data['nonsecurity_west'] 	= $data['employee_west']  - $data['security_west'];
		$data['nonsecurity_central']= $data['employee_central'] - $data['security_central'];
		$data['nonsecurity_makasar']= $data['employee_makasar'] - $data['security_makasar'];
		$data['nonsecurity_bali'] 	= $data['employee_bali']  - $data['security_bali'];

		$data['total_east'] 	= $data['employee_east']  + $data['indirect_east'];
		$data['total_west'] 	= $data['employee_west']  + $data['indirect_west'];
		$data['total_central'] 	= $data['employee_central'] + $data['indirect_central'];
		$data['total_makasar'] 	= $data['employee_makasar'] + $data['indirect_makasar'];
		$data['total_bali'] 	= $data['employee_bali']  + $data['indirect_bali'];

		$this->view($data);		
	}

	public function personalia(){
		$data['_PAGE_'] 			= 'dashboard/chart_personalia';
		$data['_TITLE_'] 			= 'Dashboard Personalia';
		$data['_MENU_PARENT_'] 		= 'dashboard_personalia';
		$data['_MENU_'] 			= 'dashboard_personalia';
		$params = array();
		$data['period']			= $this->input->get('period');
		if($data['period'] != ''){
			$params['period']	= $data['period'];
		}

		$data['branch_id']		= $this->input->get('branch_id');
		if($data['branch_id'] != '' && $data['branch_id'] > 1){
			$params['branch_id'] = $data['branch_id']; 
		}else{
			if($this->session->userdata('branch') > 1){
				$params['branch_id'] = $data['branch_id']= $this->session->userdata('branch');
			}	
		}

		$data['business_id']	= $this->input->get('business_id');
		if($data['business_id'] != ''){
			$params['company_id']	= $data['business_id']; 
		}
		
		$this->load->model(array('branch_model', 'site_model', 'dashboard_model', 'company_model', 'employee_model', 'legality_model'));

		$data['list_branch'] 	= $this->branch_model->gets(array('columns' => 'A.id, A.name'));
		$data['list_company'] 	= $this->company_model->gets(array('columns' => 'A.id, A.name'));
		$data['total_site'] 	= $this->site_model->gets($params, TRUE);
		$params['status_approval']= 3;
		$params['status_nonjob']= 0;
		$data['total_employee']	= $this->employee_model->count($params, TRUE);
		$params['status_nonjob']= 1;
		$data['total_nonjob']	= $this->employee_model->count($params, TRUE);
		$params_job['columns'] = 'COUNT(A.position_id)';
		$params_job['groupby'] = 'A.position_id';
		$data['total_position']		= $this->employee_model->count($params_job , TRUE);
		
		$data['resume_employee'] = array();
		foreach($data['list_branch'] AS $item){
			if($item->id < 2){
				continue;
			}
			$resume['branch'] 		= $item->name;
			$resume['branch_id'] 	= $item->id;
			$resume['indirect_sn'] = $this->employee_model->count(array('site_id' => 2, 'status_approval' => 3, 'status_nonjob' => 0 ,'employee_branch' => $item->id, 'bussiness_company_id' => 2), TRUE);
			$resume['sn'] = $this->employee_model->count(array('not_site_id' => 2, 'status_approval' => 3, 'status_nonjob' => 0, 'branch_id' => $item->id, 'bussiness_company_id' => 2), TRUE);
			$resume['indirect_sni'] = $this->employee_model->count(array('site_id' => 2, 'status_approval' => 3, 'status_nonjob' => 0, 'employee_branch' => $item->id, 'bussiness_company_id' => 3), TRUE);
			$resume['sni'] = $this->employee_model->count(array('not_site_id' => 2, 'status_approval' => 3, 'status_nonjob' => 0, 'branch_id' => $item->id, 'bussiness_company_id' => 3), TRUE);
			$resume['indirect_ion'] = $this->employee_model->count(array('site_id' => 2, 'status_approval' => 3, 'status_nonjob' => 0, 'employee_branch' => $item->id, 'bussiness_company_id' => 4), TRUE);
			$resume['ion'] = $this->employee_model->count(array('not_site_id' => 2, 'status_approval' => 3, 'status_nonjob' => 0, 'branch_id' => $item->id, 'bussiness_company_id' => 4), TRUE);
			$resume['indirect_sapta'] = $this->employee_model->count(array('site_id' => 2, 'status_approval' => 3, 'status_nonjob' => 0, 'employee_branch' => $item->id, 'bussiness_company_id' => 5), TRUE);
			$resume['sapta'] = $this->employee_model->count(array('not_site_id' => 2, 'status_approval' => 3, 'status_nonjob' => 0, 'branch_id' => $item->id, 'bussiness_company_id' => 5), TRUE);
			array_push($data['resume_employee'], $resume);
		}

		$this->view($data);
	}
	

	public function sales(){
		$this->load->model(array('branch_model', 'site_model', 'dashboard_model', 'candidate_model', 'legality_model'));
		$data['list_branch'] 	= $this->branch_model->gets(array('columns' => 'A.id, A.name'));

		$params = array();
		$data['period']			= $this->input->get('period');
		if($data['period'] != ''){
			$params['period']	= $data['period'];
		}
		
		$data['branch_id']		= $this->input->get('branch_id');
		if($data['branch_id'] != '' && $data['branch_id'] > 1){
			$data['branch_id'] = $params['branch_id'] = $params['session_branch']	= $data['branch_id']; 
		}else{
			if($this->session->userdata('branch') > 1){
				$data['branch_id']= $this->session->userdata('branch');
			}	
		}
		$data['total_resume_job']	= $this->dashboard_model->total_resume_job($params);
		$data['_PAGE_'] 			= 'dashboard/sales';
		$data['_TITLE_'] 			= 'Dashboard Sales';
		$data['_MENU_PARENT_'] 		= 'dashboard_sales';
		$data['_MENU_'] 			= 'dashboard_sales';
		$this->view($data);
	}

	public function recruitment(){
		$this->load->model(array('branch_model', 'site_model', 'dashboard_model', 'candidate_model', 'legality_model'));
		$data['list_branch'] 	= $this->branch_model->gets(array('columns' => 'A.id, A.name'));

		$params = array();
		$data['period']			= $this->input->get('period');
		if($data['period'] != ''){
			$params['period']	= $data['period'];
		}
		
		$data['branch_id']		= $this->input->get('branch_id');
		if($data['branch_id'] != '' && $data['branch_id'] > 1){
			$data['branch_id'] = $params['branch_id'] = $params['session_branch']	= $data['branch_id']; 
		}else{
			if($this->session->userdata('branch') > 1){
				$params['branch_id'] = $params['session_branch'] = $data['branch_id']= $this->session->userdata('branch');
			}	
		}

		$data['candidate_total']	= $this->candidate_model->gets(array('session_branch' => $data['branch_id'], 'status_approval' => 0), TRUE);
		$data['candidate_active']	= $this->candidate_model->gets(array('session_branch' => $data['branch_id'], 'status_approval' => 0, 'active' => TRUE), TRUE);
		$data['candidate_nonjob']	= $this->candidate_model->gets(array('session_branch' => $data['branch_id'], 'status_nonjob' => 1), TRUE);
		$data['candidate_notcomplete']	= $this->candidate_model->gets(array('session_branch' => $data['branch_id'], 'status_nonjob' => 0, 'status_completeness' => 0, 'status_approval' => 0), TRUE);
		
		$data['total_client']		= $this->dashboard_model->count_client($params);
		$data['total_personalia']	= $this->dashboard_model->count_personalia($params);
		$data['total_resume_job']	= $this->dashboard_model->total_resume_job($params);
		
		$data['register_today'] 	= $this->candidate_model->gets_register(array('columns' => 'A.id', 'status_approval' => 1, 'last_date' => date("Y-m-d")), TRUE);
		
		$data['_PAGE_'] 			= 'dashboard/chart_recruitment';
		$data['_TITLE_'] 			= 'Dashboard Rekrutmen';
		$data['_MENU_PARENT_'] 		= 'dashboard_recruitment';
		$data['_MENU_'] 			= 'dashboard_recruitment';
		$this->view($data);
	}

	public function payroll(){
		if(!in_array($this->session->userdata('role'), array(3,17,18,19,20,21,98,99))){
			redirect(base_url());
		}

		$this->load->model(array('branch_model', 'company_model', 'site_model', 'dashboard_model', 'employee_model', 'legality_model'));
		$data['list_branch'] 	= $this->branch_model->gets(array('columns' => 'A.id, A.name'));
		$data['list_company'] 	= $this->company_model->gets(array('columns' => 'A.id, A.name'));
		$period = strtotime("-1 month");

		$params = array();
		$data['period']			= $this->input->get('period');
		if($data['period'] != ''){
			$params['period']	= $data['period'];
			$period = strtotime($data['period']);
		}else{
			$params['period']	= $data['period'] 	= date('Y-m', $period);
		}

		$data['branch_id']		= $this->input->get('branch_id');
		if($data['branch_id'] != '' && $data['branch_id'] > 1){
			$params['branch_id'] = $data['branch_id']; 
		}else{
			if($this->session->userdata('branch') > 1){
				$params['branch_id'] = $data['branch_id']= $this->session->userdata('branch');
			}	
		}

		$data['business_id']	= $this->input->get('business_id');
		if($data['business_id'] != ''){
			$params['company_id']	= $data['business_id']; 
		}

		$params_employee['period'] 	= $params['period'].'-31';
		$data['total_employee'] 	= $this->dashboard_model->history_employee($params_employee);
		$data['resume_payroll'] = array();
		foreach($data['list_branch'] AS $item){
			if($item->id < 2){
				continue;
			}
			$resume['branch'] 	= $item->name;

			$resume['indirect_sn'] = $this->dashboard_model->gets_payroll(array('site_id' => 2, 'employee_branch' => $item->id, 'company_id' => 2, 'period' => date('Y-m', $period)));
			$resume['sn'] = $this->dashboard_model->gets_payroll(array('not_site_id' => 2, 'branch_id' => $item->id, 'company_id' => 2, 'period' => date('Y-m', $period)));
			$resume['indirect_sni'] = $this->dashboard_model->gets_payroll(array('site_id' => 2, 'employee_branch' => $item->id, 'company_id' => 3, 'period' => date('Y-m', $period)));
			$resume['sni'] = $this->dashboard_model->gets_payroll(array('not_site_id' => 2, 'branch_id' => $item->id, 'company_id' => 3, 'period' =>date('Y-m', $period)));
			$resume['indirect_ion'] = $this->dashboard_model->gets_payroll(array('site_id' => 2, 'employee_branch' => $item->id, 'company_id' => 4, 'period' => date('Y-m', $period)));
			$resume['ion'] = $this->dashboard_model->gets_payroll(array('not_site_id' => 2, 'branch_id' => $item->id, 'company_id' => 4, 'period' => date('Y-m', $period)));
			$resume['indirect_sapta'] = $this->dashboard_model->gets_payroll(array('site_id' => 2, 'employee_branch' => $item->id, 'company_id' => 5, 'period' => date('Y-m', $period)));
			$resume['sapta'] = $this->dashboard_model->gets_payroll(array('not_site_id' => 2, 'branch_id' => $item->id, 'company_id' => 5, 'period' => date('Y-m', $period)));
			array_push($data['resume_payroll'], $resume);
		}

		$data['_PAGE_'] 			= 'dashboard/chart_payroll';
		$data['_TITLE_'] 			= 'Dashboard Payroll';
		$data['_MENU_PARENT_'] 		= 'dashboard_payroll';
		$data['_MENU_'] 			= 'dashboard_payroll';
		$this->view($data);
	}

	public function benefit(){

		$this->load->model(array('branch_model', 'company_model', 'site_model', 'dashboard_model', 'employee_model', 'legality_model'));
		$data['list_branch'] 	= $this->branch_model->gets(array('columns' => 'A.id, A.name'));
		$data['list_company'] 	= $this->company_model->gets(array('columns' => 'A.id, A.name'));
		$period = strtotime("-1 month");

		$params = array();
		$data['period']			= $this->input->get('period');
		if($data['period'] != ''){
			$params['period']	= $data['period'];
			$period = strtotime($data['period']);
		}else{
			$params['period']	= $data['period'] 	= date('Y-m', $period);
		}

		$data['branch_id']		= $this->input->get('branch_id');
		if($data['branch_id'] != '' && $data['branch_id'] > 1){
			$params['branch_id'] = $data['branch_id']; 
		}else{
			if($this->session->userdata('branch') > 1){
				$params['branch_id'] = $data['branch_id']= $this->session->userdata('branch');
			}	
		}

		$data['business_id']	= $this->input->get('business_id');
		if($data['business_id'] != ''){
			$params['company_id']	= $data['business_id']; 
		}

		$data['resume_benefit'] = array();
		foreach($data['list_branch'] AS $item){
			if($item->id < 2){
				continue;
			}
			$resume['branch'] 	= $item->name;

			$resume['indirect_sn'] = $this->dashboard_model->count_benefit(array('site_id' => 2, 'company_branch' => $item->id, 'company_id' => 2, 'period' => date('Y-m', $period)));
			$resume['sn'] = $this->dashboard_model->count_benefit(array('not_site_id' => 2, 'employee_branch' => $item->id, 'company_id' => 2, 'period' => date('Y-m', $period)));
			$resume['indirect_sni'] = $this->dashboard_model->count_benefit(array('site_id' => 2, 'company_branch' => $item->id, 'company_id' => 3, 'period' => date('Y-m', $period)));
			$resume['sni'] = $this->dashboard_model->count_benefit(array('not_site_id' => 2, 'employee_branch' => $item->id, 'company_id' => 3, 'period' =>date('Y-m', $period)));
			$resume['indirect_ion'] = $this->dashboard_model->count_benefit(array('site_id' => 2, 'company_branch' => $item->id, 'company_id' => 4, 'period' => date('Y-m', $period)));
			$resume['ion'] = $this->dashboard_model->count_benefit(array('not_site_id' => 2, 'employee_branch' => $item->id, 'company_id' => 4, 'period' => date('Y-m', $period)));
			$resume['indirect_sapta'] = $this->dashboard_model->count_benefit(array('site_id' => 2, 'company_branch' => $item->id, 'company_id' => 5, 'period' => date('Y-m', $period)));
			$resume['sapta'] = $this->dashboard_model->count_benefit(array('not_site_id' => 2, 'employee_branch' => $item->id, 'company_id' => 5, 'period' => date('Y-m', $period)));

			$resume['indirect_sn_hc'] = $this->dashboard_model->employee_benefit(array('site_id' => 2, 'company_branch' => $item->id, 'company_id' => 2, 'period' => date('Y-m', $period)));
			$resume['sn_hc'] = $this->dashboard_model->employee_benefit(array('not_site_id' => 2, 'employee_branch' => $item->id, 'company_id' => 2, 'period' => date('Y-m', $period)));
			$resume['indirect_sni_hc'] = $this->dashboard_model->employee_benefit(array('site_id' => 2, 'company_branch' => $item->id, 'company_id' => 3, 'period' => date('Y-m', $period)));
			$resume['sni_hc'] = $this->dashboard_model->employee_benefit(array('not_site_id' => 2, 'employee_branch' => $item->id, 'company_id' => 3, 'period' =>date('Y-m', $period)));
			$resume['indirect_ion_hc'] = $this->dashboard_model->employee_benefit(array('site_id' => 2, 'company_branch' => $item->id, 'company_id' => 4, 'period' => date('Y-m', $period)));
			$resume['ion_hc'] = $this->dashboard_model->employee_benefit(array('not_site_id' => 2, 'employee_branch' => $item->id, 'company_id' => 4, 'period' => date('Y-m', $period)));
			$resume['indirect_sapta_hc'] = $this->dashboard_model->employee_benefit(array('site_id' => 2, 'company_branch' => $item->id, 'company_id' => 5, 'period' => date('Y-m', $period)));
			$resume['sapta_hc'] = $this->dashboard_model->employee_benefit(array('not_site_id' => 2, 'employee_branch' => $item->id, 'company_id' => 5, 'period' => date('Y-m', $period)));
			
			array_push($data['resume_benefit'], $resume);
		}

		$data['_PAGE_'] 			= 'dashboard/chart_benefit';
		$data['_TITLE_'] 			= 'Dashboard Benefit';
		$data['_MENU_PARENT_'] 		= 'dashboard_benefit';
		$data['_MENU_'] 			= 'dashboard_benefit';

		$this->view($data);
	}

	public function development($view = 'all'){
		$this->load->model(array('development_model'));
		$data['_TITLE_'] 		= 'Dashboard Development';
		$data['_PAGE_'] 		= 'dashboard/chart_development';
		$data['_MENU_PARENT_'] 	= 'dashboard_development';
		$data['_MENU_'] 		= 'dashboard_development';
		$data['view']			= $view;
		$data['development_new'] 		= $this->development_model->gets(array('status' => 'new'), TRUE);
		$data['development_processed'] 	= $this->development_model->gets(array('status' => 'processed'), TRUE);
		$data['development_done'] 		= $this->development_model->gets(array('status' => 'done'), TRUE);
		$data['development_closed'] 	= $this->development_model->gets(array('status' => 'closed'), TRUE);
		$this->view($data);
	}

	public function development_ajax($view = 'all'){
		$this->load->model(array('development_model', 'user_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.title, A.description, UPPER(A.status) AS status, A.target, A.cost, A.priority, DATE_FORMAT(A.created_at, "%Y-%m-%d") AS created_at, DATE_FORMAT(A.updated_at, "%Y-%m-%d") AS updated_at, B.name AS position, C.name AS city, A.developer_id, A.created_by, A.updated_by';

		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		// $params['employee_id']	= $this->session->userdata('user_id');
		// $params['position_id']	= $this->session->userdata('position_id');

		$params['title']		= $_POST['columns'][0]['search']['value'];
		$params['description']	= $_POST['columns'][1]['search']['value'];
		$params['status']		= $_POST['columns'][2]['search']['value'];
		$params['target']		= $_POST['columns'][3]['search']['value'];
		$params['cost']			= $_POST['columns'][4]['search']['value'];
		$params['developer']	= $_POST['columns'][5]['search']['value'];
		$params['priority']		= $_POST['columns'][6]['search']['value'];
		$params['position']		= $_POST['columns'][7]['search']['value'];
		$params['city']			= $_POST['columns'][8]['search']['value'];

		if($view != 'all'){
			$params['status'] = $view;
		}
		$list 	= $this->development_model->gets($params);
		$total 	= $this->development_model->gets($params, TRUE);
		
		$data 	= array();
		foreach($list as $item)
		{
			$result['title']		= $item->title;
			$result['description']	= $item->description;
			$result['target'] 		= $item->target;
			$result['cost'] 		= rupiah_round($item->cost);
			$result['priority']		= $item->priority;
			$result['position']		= $item->position;
			$result['city']			= $item->city;
			$result['developer'] 	= '';
			$result['created_at']	= $item->created_at;
			$result['created_by']	= '';
			$result['updated_at']	= $item->updated_at;
			$result['updated_by']	= '';

			$developer = $this->user_model->get(array('columns' => 'A.full_name', 'id' => $item->developer_id));
			if($developer){
				$result['developer'] = $developer->full_name;
			}
			$create = $this->user_model->get(array('columns' => 'A.full_name', 'id' => $item->created_by));
			if($create){
				$result['created_by'] = $create->full_name;
			}
			$update = $this->user_model->get(array('columns' => 'A.full_name', 'id' => $item->updated_by));
			if($update){
				$result['updated_by'] = $update->full_name;
			}
			
			if($item->status == 'NEW'){
				$result['status'] = '<span class="text-primary">'.$item->status.'</span>';
			}else if($item->status == 'PROCESSED'){
				$result['status'] = '<span class="text-warning">'.$item->status.'</span>';
			}else if($item->status == 'CLOSED'){
				$result['status'] = '<span class="text-danger">'.$item->status.'</span>';
			}else{
				$result['status'] = '<span class="text-success">'.$item->status.'</span>';
			}
			$result['action'] 		=
				'<a class="btn-sm btn-info btn-action btn-block" target="_blank" href="'. base_url("development/preview/".$item->id).'">Lihat</a>';
			array_push($data, $result);
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function index()
	{
		$this->load->model(array('dashboard_model', 'site_model', 'candidate_model', 'position_model','applicant_model'));
		$data['_TITLE_'] 			= 'Dashboard';
		$data['_MENU_PARENT_'] 		= 'dashboard';
		$data['_MENU_'] 			= 'dashboard';
		$data['_PAGE_'] 			= "dashboard";
		switch ($this->session->userdata('role')){
			case 3:
			case 98:
			case 99:
				redirect(base_url('dashboard/progress'));
				break;
			case 1: 
				$employee_id = $this->session->userdata('employee_id');
				$data['candidate'] = $this->candidate_model->get(array('id' => $employee_id));
				if($data['candidate']){
					$this->load->model(array('employee_education_model', 'employee_emergency_model', 'employee_history_model', 'employee_language_model', 'employee_reference_model', 'employee_skill_model'));

					$data['list_education'] = $this->employee_education_model->gets(array('employee_id' => $employee_id, 'columns' => 'A.id, A.institute, A.start, A.end'));
					$data['list_emergency'] = $this->employee_emergency_model->gets(array('employee_id' => $employee_id, 'columns' => 'A.id, A.name, A.relation, A.phone, A.address'));
					$data['list_history'] 	= $this->employee_history_model->gets(array('employee_id' => $employee_id, 'columns' => 'A.id, A.company_name, A.position, A.period,  A.salary, A.company_address, A.resign_note'));
					$data['list_language'] 	= $this->employee_language_model->gets(array('employee_id' => $employee_id, 'columns' => 'A.id, A.language, A.verbal, A.write'));
					$data['list_reference'] = $this->employee_reference_model->gets(array('employee_id' => $employee_id, 'columns' => 'A.id, A.name, A.company_name, A.position, A.phone'));
					$data['list_skill'] 	= $this->employee_skill_model->gets(array('employee_id' => $employee_id, 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));
					$data['list_belum_tes']	= $this->applicant_model->getDataBelumTes($employee_id);
				}
				$data['_PAGE_'] 			= 'account/dashboard';
				break;
			case 2: 
				redirect(base_url('dashboard/recruitment'));
				break;
			case 4:
			case 6:
			case 8:
			case 9:
			case 10:
				$this->load->model(array('site_model', 'auth_model'));
				$data['site_id']			= '';
				$data['site_name']			= '';
				$data['message']			= '';
				$data['list_site']			= $this->site_model->gets_ro_site(array('columns' => 'A.id, A.name, B.name AS company_name, B.code AS company_code, C.name AS branch_name'));
				$data['count_employee']		= $this->site_model->count_employee();
				
				if($this->input->post()){
					$valid 			= $this->auth_model->get(array('username' => $this->input->post('username'), 'password' => generate_password($this->input->post('password'))));
					if($valid){
						if($valid->id != $this->session->userdata('user_id')){
							$data['message']	= message_box('<strong>Gagal!</strong> Maaf, username atau password yang anda masukkan salah.','danger');
						}else{
							$this->session->set_userdata('site',$this->input->post('site_id'));
							$data['message']	= message_box('<strong>Sukses!</strong> Anda berhasil memilih site bisnis.','success');
						}
					}else{
						$data['message']	= message_box('<strong>Gagal!</strong> Maaf, username atau password yang anda masukkan salah.','danger');
					}
				}

				if($this->session->userdata('site') != ''){
					$data['site_id']		= $this->session->userdata('site');
				}
				$site 						= $this->site_model->get_site(array('columns' => 'A.id, A.name', 'id' => $data['site_id']));
				if($site){
					$data['site_id']		= $site->id;
					$data['site_name']		= $site->name;
				}

				$data['_PAGE_'] 			= 'dashboard/ro';
				break;
			case 5:
			case 7:
				$this->load->model(array('site_model'));
				$data['count_employee']		= $this->site_model->gets_security(array('columns' => 'A.name, A.address, COUNT(B.id) AS employee', 'job_type' => 'Security', 'status_approval' => 3, 'group_by' => 'A.id'));
				$data['_PAGE_'] 			= 'dashboard/ro_security';
				break;
			case 22:
			case 23:
			case 24:
			case 25:
			case 26:
			case 27:
			case 28: 
				$this->load->model(array('branch_model'));
				$data['list_branch'] 	= $this->branch_model->gets(array('columns' => 'A.id, A.name'));

				$params = array();
				$data['period']			= $this->input->get('period');
				if($data['period'] != ''){
					$params['period']	= $data['period'];
				}

				$data['branch_id']		= $this->input->get('branch_id');
				if($data['branch_id'] != '' && $data['branch_id'] > 1){
					$data['branch_id'] = $params['branch_id'] = $params['session_branch']	= $data['branch_id']; 
				}else{
					if($this->session->userdata('branch') > 1){
						$params['branch_id'] = $params['session_branch'] = $data['branch_id'] = $this->session->userdata('branch');
					}	
				}

				$data['candidate_active']	= $this->candidate_model->gets(array('session_branch' => $data['branch_id'], 'status_approval' => 0, 'active' => TRUE), TRUE);
				$data['candidate_total']	= $this->candidate_model->gets(array('session_branch' => $data['branch_id'], 'status_approval' => 0), TRUE);
				$data['candidate_nonjob']	= $this->candidate_model->gets(array('session_branch' => $data['branch_id'], 'status_nonjob' => 1, 'status_approval' => 3), TRUE);
				$data['candidate_notcomplete']	= $this->candidate_model->gets(array('session_branch' => $data['branch_id'], 'status_nonjob' => 0, 'status_completeness' => 0, 'status_approval' => 0), TRUE);

				$data['total_client']		= $this->dashboard_model->count_client($params);
				$data['total_personalia']	= $this->dashboard_model->count_personalia($params);
				$data['total_resume_job']	= $this->dashboard_model->total_resume_job($params);

				$data['register_today'] 	= $this->candidate_model->gets_register(array('columns' => 'A.id', 'status_approval' => 1, 'last_date' => date("Y-m-d")), TRUE);
				$data['_PAGE_'] 			= 'dashboard/recruitment';
				break;
			case 15:
			case 16:
			case 17:
			case 18:
			case 19:
			case 20:
			case 21: 
				$this->load->model(array('employee_model', 'roresign_model', 'employee_contract_model'));
				$branch_id = '';
				if($this->session->userdata('branch') > 1){
					$branch_id = $this->session->userdata('branch');
				}

				$data['total_indirect'] 	= $this->employee_model->count(array('site_id' => 2, 'status_approval' => 3, 'branch_id' => $branch_id), TRUE);
				$data['total_direct'] 		= $this->employee_model->count(array('not_site_id' => 2, 'status_approval' => 3, 'branch_id' => $branch_id), TRUE);
				$data['approval_indirect'] 	= $this->employee_model->count(array('site_id' => 2, 'status_approval' => 2, 'branch_id' => $branch_id), TRUE);
				$data['approval_direct'] 	= $this->employee_model->count(array('not_site_id' => 2, 'status_approval' => 2, 'branch_id' => $branch_id), TRUE);
				$data['total_nonjob'] 		= $this->employee_model->count(array('status_nonjob' => 1, 'status_approval' => 3, 'branch_id' => $branch_id), TRUE);
				$data['total_resign'] 		= $this->roresign_model->gets(array('status_approval' => 4, 'status' => 1, 'branch_id' => $branch_id), TRUE);
				$contract['Kontrak Habis'] 	= array('day' => 0, 'summary' => $this->employee_contract_model->gets_expired(array('expired' => 0, 'branch_id' => $branch_id, 'columns' => 'COUNT(A.employee_id) AS summary, C.name AS site_name, D.code AS company_code, E.name AS branch_name')));
				for($i=1; $i <= 30;$i++){
					$expired = $this->employee_contract_model->gets_expired(array('expired' => $i, 'branch_id' => $branch_id, 'columns' => 'COUNT(A.employee_id) AS summary, C.name AS site_name, D.code AS company_code, E.name AS branch_name'));
					if(!empty($expired)){
						$detail['day']		= $i;
						$detail['summary']	= $expired; 
						$contract['Kontrak Kurang '.$i.' Hari'] 	= $detail;
					}
				}

				$data['list_expired']		= $contract;
				$data['_PAGE_'] 			= 'dashboard/personalia';
				break;
			case 29:
			case 30:
			case 31:
			case 32:
			case 33:
			case 34:
			case 35:
				/**
				$this->load->model(array('sales_model'));
				$data['total_site'] 	= $this->site_model->gets(array(), TRUE);
				$data['total_position'] = $this->position_model->gets(array(), TRUE);
				$data['candidate']		= $this->dashboard_model->count_candidate(array());
				$data['total_candidate']= 0;
				$data['total_approved'] = 0;
				$data['total_needs'] 	= 0;
				$data['_PAGE_'] 		= 'dashboard/sales';
				*/
				redirect(base_url('dashboard/sales'));
				break;
			case 11:
			case 12:
				$this->load->model(array('employee_model', 'payroll_model'));
				$data['total_indirect'] = $this->employee_model->count(array('site_id' => 2, 'status_approval' => 3), TRUE);
				$data['total_direct'] 	= $this->employee_model->count(array('not_site_id' => 2, 'status_approval' => 3), TRUE);
				// $data['waiting_send']	= $this->payroll_model->gets(array('email' => 'Menunggu', 'direct' => TRUE), TRUE);
				$data['waiting_security']	= $this->payroll_model->gets(array('payment' => 'Menunggu', 'direct' => TRUE, 'position_type' => 'Security'), TRUE);
				$data['waiting_non_security']= $this->payroll_model->gets(array('payment' => 'Menunggu', 'direct' => TRUE), TRUE);
				$data['list_submission']= $this->payroll_model->resume_submission();

				$data['_PAGE_'] 		= 'dashboard/payroll';
				break;

			case 13:
			case 14:
				$this->load->model(array('employee_model', 'benefit_model', 'roresign_model', 'site_model'));
				$data['total_indirect'] = $this->employee_model->count(array('site_id' => 2, 'status_approval' => 3), TRUE);
				$data['total_direct'] 	= $this->employee_model->count(array('not_site_id' => 2, 'status_approval' => 3), TRUE);
				$data['benefit_labor']	= $this->benefit_model->count(array('status_approval' => 3, 'benefit_labor' => ''), TRUE);
				$data['benefit_health']	= $this->benefit_model->count(array('status_approval' => 3, 'benefit_health' => ''), TRUE);
				$data['break_site'] 	= $this->site_model->gets_site(array('non_active' => TRUE, 'assurance' => TRUE), TRUE);
				$data['employee_resign']= $this->employee_model->gets(array('assurance_active' => TRUE), TRUE);
				
				$params['columns'] 		= "A.id, A.id_card, A.full_name, A.resign_note, B.name AS site_name, DATE_FORMAT(A.resign_submit, '%d/%m/%Y') AS resign_submit";
				$params['status_approval'] 		= 4;
				$data['list_submission']=  $this->roresign_model->gets($params);
				$data['_PAGE_'] 		= 'dashboard/benefit';
				break;
			case 36:
			case 37:
			case 38:
			case 39:
			case 40:
			case 41:
			case 42:
				$data['_PAGE_'] 		= 'dashboard/accounting';
				break;
			case 43:
			case 44:
			case 45:
			case 46:
				$this->load->model(array('mou_model', 'legality_model'));
				$data['list_mou']=  $this->mou_model->gets(array('onprocess' => 'Done', 'columns' => 'A.company_name, C.name AS company_now, D.name AS province_name, E.name AS city_name, DATE_FORMAT(A.contract_start, "%d/%m/%Y") AS contract_start, DATE_FORMAT(A.contract_end, "%d/%m/%Y") AS contract_end, A.crm, A.status_registration'));
				$data['_PAGE_'] 		= 'dashboard/legal';
				break;
			case 100:
				redirect(base_url("client_report/payroll_regular"));
				break;
			case 101:
				redirect(base_url("development/list"));
				break;
		}

		$this->view($data);
	}

	public function list_job($branch = "", $period = ""){
		$this->load->model(array('dashboard_model'));
		$labels 		= array();
		$total 			= array();
		$params 		= array();
		if($period != ""){
			$params['period']			= $period;	
		}
		if($branch != "" && $branch > 1){
			$params['session_branch']	= $branch;
		}

		$list_job 		= $this->dashboard_model->chart_job($params);
		$response = array(
			"iTotalRecords" 		=> count($list_job),
			"iTotalDisplayRecords" 	=> count($list_job),
			"aaData" 				=> $list_job
		);
		echo json_encode($response);
	}
	
	public function chart_job($branch = "", $period = ""){
		$this->load->model(array('dashboard_model'));
		$labels 		= array();
		$total 			= array();
		$params 		= array();
		if($period != ""){
			$params['period']			= $period;	
		}
		if($branch != "" && $branch > 1){
			$params['session_branch']	= $branch;
		}

		$list_job 		= $this->dashboard_model->chart_job($params);
		foreach ($list_job as $item) {
			array_push($labels, $item->name);
			array_push($total, $item->total);

		}
		$result = array('labels' => $labels, 'total' => $total);
		echo json_encode($result);
	}

	public function chart_education($branch = "", $period = ""){
		$this->load->model(array('dashboard_model'));
		$labels 		= array();
		$total 			= array();
		$params 		= array();
		if($period != ""){
			$params['period']			= $period;	
		}
		if($branch != "" && $branch > 1){
			$params['session_branch']	= $branch;
		}
		$list_edu 		= $this->dashboard_model->chart_education($params);
		foreach ($list_edu as $item) {
			array_push($labels, $item->name);
			array_push($total, $item->total);
		}
		$result = array('labels' => $labels, 'total' => $total);
		echo json_encode($result);

	}

	public function chart_recommendation($branch = "", $period = ""){
		$this->load->model(array('dashboard_model'));
		$labels 		= array();
		$total 			= array();
		$params 		= array();
		if($period != ""){
			$params['period']			= $period;	
		}
		if($branch != "" && $branch > 1){
			$params['session_branch']	= $branch;
		}

		$list_recom		= $this->dashboard_model->chart_recommendation($params);
		foreach ($list_recom as $item) {
			$name = strtoupper($item->name);
			if($item->name == ''){
				continue;
			}

			array_push($labels, $name);
			array_push($total, $item->total);

		}
		$result = array('labels' => $labels, 'total' => $total);
		echo json_encode($result);
	}

	public function chart_age($branch = "", $period = ""){
		$this->load->model(array('dashboard_model'));
		$labels 		= array();
		$total 			= array();
		$params 		= array();
		if($period != ""){
			$params['period']			= $period;	
		}
		if($branch != "" && $branch > 1){
			$params['session_branch']	= $branch;
		}

		$params['start']			= 0;
		$params['end']				= 24;
		array_push($labels, '18 - 24 Tahun');
		array_push($total, $this->dashboard_model->count_range_age($params));

		$params['start']			= 25;
		$params['end']				= 34;
		array_push($labels, '25 - 34 Tahun');
		array_push($total, $this->dashboard_model->count_range_age($params));

		$params['start']			= 35;
		$params['end']				= 39;
		array_push($labels, '35 - 39 Tahun');
		array_push($total, $this->dashboard_model->count_range_age($params));

		$params['start']			= 40;
		$params['end']				= 1000;
		array_push($labels, '40 Tahun Lebih');
		array_push($total, $this->dashboard_model->count_range_age($params));
		
		$result = array('labels' => $labels, 'total' => $total);
		echo json_encode($result);
	}

	public function chart_applicant($branch = ""){
		$this->load->model(array('dashboard_model'));
		$labels 		= array();
		$total 			= array();
		$params 		= array();
		if($branch != "" && $branch > 1){
			$params['session_branch']	= $branch;
		}
		
		$list_applicant	= (array) $this->dashboard_model->chart_applicant($params);
		$list_applicant	= array_reverse($list_applicant);

		foreach ($list_applicant as $item) {
			array_push($labels, $item->month);
			array_push($total, (int) $item->total);
		}	

		$result = array('labels' => $labels, 'total' => $total);
		echo json_encode($result);
	}

	public function resume_salary($params = FALSE){
		$this->load->model(array('site_model', 'payroll_model', 'benefit_labor_model', 'benefit_health_model'));
		$column_index = $_POST['order'][0]['column'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['branch_id'] 	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		$params['company_code']		= $_POST['columns'][1]['search']['value'];
		$params['site_name']		= $_POST['columns'][2]['search']['value'];
		$params['columns']			= 'SUM(A.salary) AS payroll, A.periode_end, COUNT(A.employee_id) AS employee,
		SUM(A.bpjs_ks) AS health_employee, SUM(A.bpjs_ks_company) health_company, SUM(A.bpjs_jht + A.bpjs_jp) AS labor_employee, SUM(A.bpjs_jht_company + A.bpjs_jp_company) AS labor_company, C.id AS site_id, C.name AS site_name, D.code AS company_code';
		$list 	= $this->payroll_model->resume_payroll($params);
		$total 	= count($this->payroll_model->resume_payroll(array('columns' => 'COUNT(*)')));
		$i 		= $_POST['start']+1;
		$data 	= array();
		
		foreach($list as $item)
		{
			$end_date 		= substr($item->periode_end, 8, 2).' '.get_month(substr($item->periode_end, 5, 2)).' '.substr($item->periode_end, 0, 4);
			$benefit_labor = 'Karyawan : '.rupiah($item->labor_employee).' <br>Perusahaan : '.rupiah($item->labor_company);
			$benefit_health = 'Karyawan : '.rupiah($item->health_employee).' <br>Perusahaan : '.rupiah($item->health_company);
			$result['no'] 				= $i;
			$result['company_code'] 	= $item->company_code;
			$result['site_name'] 		= $item->site_name;
			$result['periode']			= $end_date;
			$result['employee']			= $item->employee;
			$result['payroll']			= rupiah($item->payroll);
			$result['benefit_labor']	= $benefit_labor;
			$result['benefit_health']	= $benefit_health;
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function chart_last_contract($branch_id = FALSE, $business_id = FALSE){
		$this->load->model(array('dashboard_model'));
		$labels 		= array();
		$total 			= array();
		$params 		= array();

		if($branch_id != "" && $branch_id > 1){
			$params['branch_id']	= $branch_id;
		}

		if($business_id != ""){
			$params['company_id']	= $business_id;
		}

		$list_applicant	= (array) $this->dashboard_model->chart_employee($params);
		$list_applicant	= array_reverse($list_applicant);

		foreach ($list_applicant as $item) {
			array_push($labels, $item->date);
			array_push($total, (int) $item->total);
		}	

		$result = array('labels' => $labels, 'total' => $total);
		echo json_encode($result);
	}

	public function chart_employee($branch_id = FALSE, $business_id = FALSE){
		$this->load->model(array('dashboard_model'));
		$labels 		= array();
		$total 			= array();
		$params 		= array();

		if($branch_id != "" && $branch_id > 1){
			$params['branch_id']	= $branch_id;
		}

		if($business_id != ""){
			$params['company_id']	= $business_id;
		}

		for ($i = 11; $i >= 0; $i--) {
			$months = date("Y-m-31", strtotime( date( 'Y-m-01' )." -$i months"));
			$params['period'] = $months;
			array_push($labels, get_month(substr($months, 5, 2))." ".substr($months, 0, 4));
			array_push($total, $this->dashboard_model->history_employee($params));
		}

		$result = array('labels' => $labels, 'total' => $total);
		echo json_encode($result);
	}

	public function chart_cuti(){
		$this->load->model(array('dashboard_model'));
		$labels 		= array();
		$value1 		= array();
		$value2 		= array();
		$value3 		= array();
		$params 		= array();
		$time = date('Y-m-').'1';
		
		// $final = date("Y-m-d", strtotime("+1 month", $time));


		for ($i = 6; $i >= 0; $i--) {
			$months = date("Y-m-d", strtotime( date( 'Y-m-01' )." -$i months"));
			$params['period'] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));

			// print_r($params['period']);
			array_push($labels, get_month(substr($months, 5, 2))." ".substr($months, 0, 4));
			// $value = ($i * intval(10));
			array_push($value1, $this->dashboard_model->chart_cuti_req($params));
			array_push($value2, $this->dashboard_model->chart_cuti_aprove($params));
			array_push($value3, $this->dashboard_model->chart_cuti_reject($params));
		}

		
		for ($i = 1; $i <= 6; $i++) {
			$months = date("Y-m-d", strtotime( date( 'Y-m-01' )." +$i months"));
			$params['period'] = date("Y-m", strtotime( date( 'Y-m-01' )." +$i months"));
			// print_r(date("Y-m", strtotime( date( 'Y-m-01' )." +$i months")));
			array_push($labels, get_month(substr($months, 5, 2))." ".substr($months, 0, 4));
			array_push($value1, $this->dashboard_model->chart_cuti_req($params));
			array_push($value2, $this->dashboard_model->chart_cuti_aprove($params));
			array_push($value3, $this->dashboard_model->chart_cuti_reject($params));
		}

		$result = array('labels' => $labels, 'value1' => $value1, 'value2' => $value2, 'value3' => $value3);
		echo json_encode($result);
	}
	
	public function chart_position($branch_id = FALSE, $business_id = FALSE){
		$this->load->model(array('dashboard_model'));
		$labels 		= array();
		$total 			= array();
		$params 		= array();

		if($branch_id != "" && $branch_id > 1){
			$params['branch_id']	= $branch_id;
		}

		if($business_id != ""){
			$params['company_id']	= $business_id;
		}
		$list_applicant	= (array) $this->dashboard_model->chart_position($params);
		$list_applicant	= array_reverse($list_applicant);

		foreach ($list_applicant as $item) {
			if($item->total < 1){
				continue;
			}
			array_push($labels, $item->position);
			array_push($total, (int) $item->total);
		}	

		$result = array('labels' => $labels, 'total' => $total);
		echo json_encode($result);
	}

	public function list_site($branch_id = FALSE, $business_id = FALSE){
		$this->load->model(array('dashboard_model'));
		$labels 		= array();
		$total 			= array();
		$params 		= array();
		if($branch_id != "" && $branch_id > 1){
			$params['branch_id']	= $branch_id;
		}

		if($business_id != ""){
			$params['company_id']	= $business_id;
		}
		$list_job 		= $this->dashboard_model->chart_site($params);
		$response = array(
			"iTotalRecords" 		=> count($list_job),
			"iTotalDisplayRecords" 	=> count($list_job),
			"aaData" 				=> $list_job
		);
		echo json_encode($response);
	}

	public function chart_salary($branch_id = FALSE, $business_id = FALSE){
		$this->load->model(array('dashboard_model'));
		$labels 		= array();
		$total 			= array();
		$params 		= array();
		if($branch_id != "" && $branch_id > 1){
			$params['branch_id']	= $branch_id;
		}

		if($business_id != ""){
			$params['company_id']	= $business_id;
		}

		$params['low']			= 0;
		$params['high']			= 2000000;
		array_push($labels, 'Kurang dari 2 Juta ');
		array_push($total, $this->dashboard_model->count_range_salary($params));

		$params['low']			= 2000000;
		$params['high']			= 4000000;
		array_push($labels, '2 - 4 Juta');
		array_push($total, $this->dashboard_model->count_range_salary($params));

		$params['low']			= 4000000;
		$params['high']			= 5000000;
		array_push($labels, '4 - 5 Juta');
		array_push($total, $this->dashboard_model->count_range_salary($params));

		$params['low']			= 40;
		$params['high']			= 1000;
		array_push($labels, '5 - 10 Juta');
		array_push($total, $this->dashboard_model->count_range_salary($params));

		$params['low']			= 40;
		$params['high']			= 1000;
		array_push($labels, 'Lebih dari 10 Juta');
		array_push($total, $this->dashboard_model->count_range_salary($params));
		$result = array('labels' => $labels, 'total' => $total);
		echo json_encode($result);
	}

	public function payroll_chart($period = FALSE, $branch_id = FALSE, $business_id = FALSE){
		$this->load->model(array('dashboard_model'));
		$labels 		= array();
		$total 			= array();
		$params 		= array();

		if($branch_id != "" && $branch_id > 1){
			$params['branch_id']	= $branch_id;
		}

		if($business_id != ""){
			$params['company_id']	= $business_id;
		}

		for ($i = 12; $i >= 1; $i--) {
			$months = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
			$params['period'] = $months;
			$payroll = round((double)$this->dashboard_model->count_payroll($params)/1000000000, 2);
			array_push($labels, get_month(substr($months, 5, 2))." ".substr($months, 0, 4));
			array_push($total, $payroll);
		}

		$result = array('labels' => $labels, 'total' => $total);
		echo json_encode($result);
	}


	public function chart_resume_payroll($period = FALSE, $branch_id = FALSE, $business_id = FALSE){
		$this->load->model(array('dashboard_model'));
		$labels 		= array('INDIRECT SN', 'DIRECT SN','INDIRECT SNI','DIRECT SNI','INDIRECT ION','DIRECT ION','INDIRECT SAPTA','DIRECT SAPTA');
		$total 			= array();
		$color 			= array('#ff3300', '#ff9900', '#000066', '#3333ff', '#006600', '#00ff00', '#cc0066', '#ff6699');
		if($branch_id != "" && $branch_id > 1){
			$params_indirect['branch_id']	= $branch_id;
			$params_direct['branch_id']	= $branch_id;
		}
		$params_indirect['site_id']		= 2;
		$params_indirect['company_id']	= 2;
		$params_indirect['period']		= $period;
		$indirect_sn = (int) $this->dashboard_model->count_payroll($params_indirect);
		array_push($total, $indirect_sn);
		
		$params_direct['not_site_id']	= 2;
		$params_direct['company_id']	= 2;
		$params_direct['period']		= $period;
		$sn = (int) $this->dashboard_model->count_payroll($params_direct);
		array_push($total, $sn);

		$params_indirect['site_id']		= 2;
		$params_indirect['company_id']	= 3;
		$params_indirect['period']		= $period;
		$indirect_sni = (int) $this->dashboard_model->count_payroll($params_indirect);
		array_push($total, $indirect_sni);
		
		$params_direct['not_site_id']	= 2;
		$params_direct['company_id']	= 3;
		$params_direct['period']		= $period;
		$sni = (int) $this->dashboard_model->count_payroll($params_direct);
		array_push($total, $sni);

		$params_indirect['site_id']		= 2;
		$params_indirect['company_id']	= 4;
		$params_indirect['period']		= $period;
		$indirect_ion = (int) $this->dashboard_model->count_payroll($params_indirect);
		array_push($total, $indirect_ion);
		
		$params_direct['not_site_id']	= 2;
		$params_direct['company_id']	= 4;
		$params_direct['period']		= $period;
		$ion = (int) $this->dashboard_model->count_payroll($params_direct);
		array_push($total, $ion);

		$params_indirect['site_id']		= 2;
		$params_indirect['company_id']	= 5;
		$params_indirect['period']		= $period;
		$indirect_sapta = (int) $this->dashboard_model->count_payroll($params_indirect);
		array_push($total, $indirect_sapta);
		
		$params_direct['not_site_id']	= 2;
		$params_direct['company_id']	= 5;
		$params_direct['period']		= $period;
		$sapta = (int) $this->dashboard_model->count_payroll($params_direct);
		array_push($total, $sapta);

		$result = array('labels' => $labels, 'total' => $total, 'color' => $color);
		echo json_encode($result);
	}

	public function benefit_chart($period = FALSE, $branch_id = FALSE, $business_id = FALSE){
		$this->load->model(array('dashboard_model'));
		$labels 		= array();
		$total 			= array();
		$params 		= array();

		if($branch_id != "" && $branch_id > 1){
			$params['branch_id']	= $branch_id;
		}

		if($business_id != ""){
			$params['company_id']	= $business_id;
		}

		for ($i = 11; $i >= 0; $i--) {
			$months = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
			$params['period'] = $months;
			$payroll = round((double)$this->dashboard_model->count_benefit($params)/1000000, 2);
			array_push($labels, get_month(substr($months, 5, 2))." ".substr($months, 0, 4));
			array_push($total, $payroll);
		}

		$result = array('labels' => $labels, 'total' => $total);
		echo json_encode($result);
	}

	public function chart_resume_benefit($period = FALSE, $branch_id = FALSE, $business_id = FALSE){
		$this->load->model(array('dashboard_model'));
		$labels 		= array('INDIRECT SN', 'DIRECT SN','INDIRECT SNI','DIRECT SNI','INDIRECT ION','DIRECT ION','INDIRECT SAPTA','DIRECT SAPTA');
		$total 			= array();
		$color 			= array('#ff3300', '#ff9900', '#000066', '#3333ff', '#006600', '#00ff00', '#cc0066', '#ff6699');
		if($branch_id != "" && $branch_id > 1){
			$params_indirect['branch_id']	= $branch_id;
			$params_direct['branch_id']	= $branch_id;
		}
		$params_indirect['site_id']		= 2;
		$params_indirect['company_id']	= 2;
		$params_indirect['period']		= $period;
		$indirect_sn = (int) $this->dashboard_model->count_benefit($params_indirect);
		array_push($total, $indirect_sn);
		
		$params_direct['not_site_id']	= 2;
		$params_direct['company_id']	= 2;
		$params_direct['period']		= $period;
		$sn = (int) $this->dashboard_model->count_benefit($params_direct);
		array_push($total, $sn);

		$params_indirect['site_id']		= 2;
		$params_indirect['company_id']	= 3;
		$params_indirect['period']		= $period;
		$indirect_sni = (int) $this->dashboard_model->count_benefit($params_indirect);
		array_push($total, $indirect_sni);
		
		$params_direct['not_site_id']	= 2;
		$params_direct['company_id']	= 3;
		$params_direct['period']		= $period;
		$sni = (int) $this->dashboard_model->count_benefit($params_direct);
		array_push($total, $sni);

		$params_indirect['site_id']		= 2;
		$params_indirect['company_id']	= 4;
		$params_indirect['period']		= $period;
		$indirect_ion = (int) $this->dashboard_model->count_benefit($params_indirect);
		array_push($total, $indirect_ion);
		
		$params_direct['not_site_id']	= 2;
		$params_direct['company_id']	= 4;
		$params_direct['period']		= $period;
		$ion = (int) $this->dashboard_model->count_benefit($params_direct);
		array_push($total, $ion);

		$params_indirect['site_id']		= 2;
		$params_indirect['company_id']	= 5;
		$params_indirect['period']		= $period;
		$indirect_sapta = (int) $this->dashboard_model->count_benefit($params_indirect);
		array_push($total, $indirect_sapta);
		
		$params_direct['not_site_id']	= 2;
		$params_direct['company_id']	= 5;
		$params_direct['period']		= $period;
		$sapta = (int) $this->dashboard_model->count_benefit($params_direct);
		array_push($total, $sapta);

		$result = array('labels' => $labels, 'total' => $total, 'color' => $color);
		echo json_encode($result);
	}

	public function chart_benefit_payment($period = FALSE, $branch_id = FALSE, $business_id = FALSE){
		$this->load->model(array('dashboard_model'));
		$labels 		= array();
		$total 			= array();
		$params 		= array();
		if($branch_id != "" && $branch_id > 1){
			$params['branch_id']	= $branch_id;
		}

		if($business_id != ""){
			$params['company_id']	= $business_id;
		}

		$params['period']		= $period;
		$health = $this->dashboard_model->count_health($params);
		array_push($labels, 'Kesehatan dari Potongan ');
		array_push($total, $health->employee_payment);
		array_push($labels, 'Kesehatan dari Perusahaan ');
		array_push($total, $health->company_payment);

		$labor = $this->dashboard_model->count_labor($params);
		array_push($labels, 'Ketenagakerjaan dari Potongan ');
		array_push($total, $labor->employee_payment);
		array_push($labels, 'Ketenagakerjaan dari Perusahaan ');
		array_push($total, $labor->company_payment);

		$result = array('labels' => $labels, 'total' => $total);
		echo json_encode($result);
	}

	public function list_employee_today(){
		$this->load->model(array('candidate_model'));
		$labels 		= array();
		$total 			= array();
		$params 		= array();

		$params['columns'] 			= "A.full_name, A.id_card, A.phone_number, A.gender, DATE_FORMAT(A.date_birth, '%d/%m/%Y') AS date_birth, A.education_level, A.education_majors, A.experience, C.name AS preference_job1, A.site_preference, B.name AS address";
		$params['status_approval'] 	= 1;
		$params['last_date'] 		= date("Y-m-d");
		$list_employee 				= $this->candidate_model->gets_register($params);
		$data 						= array();
		foreach($list_employee as $item)
		{
			$result['full_name'] 	= $item->full_name;
			$result['id_card'] 	= $item->id_card;
			
			$list_number 			= $item->phone_number;
			$phone_number			= "";
			if($list_number != ""){
				$phone_number = explode ("/", $list_number);
				$phone_number = preg_replace("/[^0-9]/", "", $phone_number[0]);
				$start = substr($phone_number, 0, 1 );
				if($start == '0'){
					$phone_number = '62'.substr($phone_number, 1,strlen($phone_number));
				}else if($start == '+'){
					$phone_number = substr($phone_number, 1,strlen($phone_number));
				}
				$phone_number = '<a href="http://wa.me/'.$phone_number.'" target="_blank">'.$item->phone_number.'</a>';
			}
			$result['phone_number'] 		= $phone_number;
			$result['gender'] 			= $item->gender;
			$result['date_birth'] 		= $item->date_birth;
			$result['education_level']	= $item->education_level;
			$result['education_majors']	= $item->education_majors;
			$result['experience']		= $item->experience;
			$result['preference_job1']	= $item->preference_job1;
			$result['site_preference']	= $item->site_preference;
			$result['address']			= $item->address;
			array_push($data, $result);
			
		}
		$response = array(
			"iTotalRecords" 		=> count($data),
			"iTotalDisplayRecords" 	=> count($data),
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}
	
	public function candidate_ajax($branch = 1){

		$this->load->model(array('candidate_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.registration_number, DATE_FORMAT(A.created_at, '%d/%m/%Y') AS date_registration, DATE_FORMAT(A.updated_at, '%d/%m/%Y') AS last_update, A.full_name, A.id_card, FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25) AS age, A.gender, A.education_level, A.education_majors, A.phone_number, B.name AS city_domisili, CONCAT_WS(', ', D.name, E.name, F.name) AS preference_job, GROUP_CONCAT(C.certificate_name) AS certificate, L.name AS province_domisili, A.site_preference, A.status_completeness, A.status_nonjob, DATE_FORMAT(A.updated_at, '%Y-%m-%d') AS updated_at, CASE WHEN A.updated_at < NOW() - INTERVAL 3 MONTH THEN 'non aktif' ELSE 'aktif' END AS activated ";
		$params['orderby'] 			= $_POST['columns'][$column_index]['data'];
		$params['order']			= $_POST['order'][0]['dir'];
		$params['limit']			= $_POST['length'];
		$params['page']				=  $_POST['start'];
		
		$params['status_approval'] 		= 0;
		
		$params['full_name']		= $_POST['columns'][0]['search']['value'];
		$params['id_card']			= $_POST['columns'][1]['search']['value'];
		$params['age']				= $_POST['columns'][2]['search']['value'];
		$params['gender']			= $_POST['columns'][3]['search']['value'];
		$params['education_level']	= $_POST['columns'][4]['search']['value'];
		$params['education_majors']	= $_POST['columns'][5]['search']['value'];
		$params['phone_number']		= $_POST['columns'][6]['search']['value'];
		$params['city_domisili']	= $_POST['columns'][7]['search']['value'];
		$params['province_domisili']= $_POST['columns'][8]['search']['value'];
		$params['preference_job']	= $_POST['columns'][9]['search']['value'];
		$params['certificate']		= $_POST['columns'][10]['search']['value'];

		if($branch > 1){
			$params['session_branch']	= $branch;
		}
		
		$list 	= $this->candidate_model->gets($params);
		$total 	= $this->candidate_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{				
			$result['full_name']			= $item->full_name;
			$result['id_card']				= $item->id_card;
			$result['age']					= $item->age;
			$result['gender']				= $item->gender;
			$result['education_level']		= $item->education_level;
			$result['education_majors']		= $item->education_majors;
			$list_number 					= $item->phone_number;
			$phone_number					= "";
			if($list_number != ""){
				$phone_number = explode ("/", $list_number);
				$phone_number = preg_replace("/[^0-9]/", "", $phone_number[0]);
				$start = substr($phone_number, 0, 1 );
				if($start == '0'){
					$phone_number = '62'.substr($phone_number, 1,strlen($phone_number));
				}else if($start == '+'){
					$phone_number = substr($phone_number, 1,strlen($phone_number));
				}
				$phone_number = '<a href="http://wa.me/'.$phone_number.'" target="_blank">'.$item->phone_number.'</a>';
			}
			$result['phone_number']			= $phone_number;
			$result['city_domisili']		= $item->city_domisili; 
			$result['province_domisili']	= $item->province_domisili; 
			$result['preference_job']		= $item->preference_job;
			$result['certificate']			= $item->certificate;
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function employee_site_export()
	{
		$this->load->model(array('dashboard_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = 'headcount_site';
		$files = glob(FCPATH."files/".$title."/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "No");
		$excel->getActiveSheet()->setCellValue("B".$i, "ID Karyawan");
		$excel->getActiveSheet()->setCellValue("C".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("D".$i, "Branch");
		$excel->getActiveSheet()->setCellValue("E".$i, "Unit Kontrak");
		$excel->getActiveSheet()->setCellValue("F".$i, "Site Bisnis");
		$excel->getActiveSheet()->setCellValue("G".$i, "Unit Bisnis");
		$excel->getActiveSheet()->setCellValue("H".$i, "Jabatan");

		$params['columns'] 		= "A.id, A.employee_number, A.full_name, F.name AS branch_name, C.code AS company_code, B.name AS site_name, E.name AS position, J.code AS business_code";
		$params['orderby'] 		= 'B.name, A.full_name';
		$params['order']		= 'ASC';
		$params['status']		= 1;
		$params['status_approval'] 	= 3;
		$params['not_site_id']		= 2;

		$list_site = $this->dashboard_model->gets_all_employee($params);

		$i=2;
		$no = 1;
		foreach ($list_site as $item) {
			$excel->getActiveSheet()->setCellValue("A".$i, $no);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->employee_number);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("D".$i, $item->branch_name);
			$excel->getActiveSheet()->setCellValueExplicit("E".$i, $item->business_code);
			$excel->getActiveSheet()->setCellValueExplicit("F".$i, $item->site_name);
			$excel->getActiveSheet()->setCellValueExplicit("G".$i, $item->company_code);
			$excel->getActiveSheet()->setCellValueExplicit("H".$i, $item->position);
			$no++;
			$i++;
		}
		$excel->getActiveSheet()->setTitle('Headcount Karyawan');
		$sheet = $excel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells( true );
		foreach( $cellIterator as $cell ) {
			$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		}
		$excel->createSheet();
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Resign extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		$role 	= array_merge($role, $this->config->item('relation_officer_security'));
		$role 	= array_merge($role, $this->config->item('benefit'));
		$role 	= array_merge($role, $this->config->item('recruitment'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function work($employee_id = FALSE){
		$this->load->model(array('work_model', 'monitoring_model', 'employee_model', 'resign_model', 'city_model'));
		$employee = $this->employee_model->preview(array('id' => $employee_id));
		$template = $this->work_model->get(array('company_id' => $employee->company_id, 'columns' => 'A.content'));
		$period = $this->monitoring_model->get_work_period(array('id' => $employee_id));
		$resign = $this->resign_model->get(array('employee_id' => $employee_id));
		$data['content'] = "";
		if($template){
			$data['content']= $template->content;
		}
		$data['content']	= str_replace("{NAMA_LENGKAP}", $employee->full_name, $data['content']);
		$data['content']	= str_replace("{NOMOR_KARYAWAN}", $employee->employee_number, $data['content']);
		$data['content']	= str_replace("{NOMOR_KESEHATAN}", $employee->benefit_health, $data['content']);
		$data['content']	= str_replace("{NOMOR_KETENAGAKERJAAN}", $employee->benefit_labor, $data['content']);
		$data['content']	= str_replace("{NIK}", $employee->id_card, $data['content']);
		$birth_date			= substr($employee->date_birth, 8, 2)." ".get_month(substr($employee->date_birth, 5, 2))." ".substr($employee->date_birth, 0, 4);
		$data['content'] 	= str_replace("{TANGGAL_LAHIR}", $birth_date, $data['content'] );
		$birth_place 		= str_replace("KOTA", "", strtoupper($employee->city_birth));
		$birth_place 		= str_replace("KABUPATEN", "", strtoupper($employee->city_birth));
		$data['content'] 	= str_replace("{TEMPAT_LAHIR}",$birth_place, $data['content'] );
		$city 				= $this->city_model->preview(array('columns' => 'A.name, B.name AS province_name', 'id' => $employee->city_card_id));
		$address_card		= $employee->address_card;
		if($city){
				$address_card = $address_card.", ".$city->name.", ".$city->province_name;
		}
		$data['content'] 	= str_replace("{ALAMAT_KTP}",$address_card,$data['content']);
		$data['content']	= str_replace("{JABATAN}", $employee->position, $data['content']);
		$data['content']	= str_replace("{NAMA_SITE}", $employee->site_name, $data['content']);
		$data['content']	= str_replace("{ALAMAT_SITE}", $employee->site_address, $data['content']);
		$data['content']	= str_replace("{NAMA_BISNIS_UNIT}", $employee->company_name, $data['content']);
		$data['content']	= str_replace("{ALAMAT_BISNIS_UNIT}", $employee->company_address, $data['content']);
		$data['content']	= str_replace("{TANGGAL_SEKARANG}", format_date_ina(date('Y-m-d')), $data['content']);
		$data['content']	= str_replace("{MULAI_KERJA}", format_date_ina($period->contract_start), $data['content']);
		$data['content']	= str_replace("{AKHIR_KERJA}", format_date_ina($employee->resign_submit), $data['content']);

		$letter_number = $employee->employee_number.'/'.$employee->site_code.'/'.$employee->company_code.'/'.get_roman_month(date('m')).'/'.date('Y');
		$data['content']	= str_replace("{NOMOR_SURAT}", $letter_number, $data['content']);

		$data['_TITLE_'] 		= 'Surat Keterangan Kerja';
		$this->load->view('monitoring/work', $data);
	}

	public function export($type = FALSE)
	{
		$this->load->model(array('resign_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = "Daftar Karyawan Resign";
		$params = $this->input->get();
		$params['columns'] 		= "B.id, B.employee_number, B.full_name, B.site_id, C.name AS company_name, D.name AS site_name, E.name AS position, DATE_FORMAT(A.updated_at, '%d/%m/%Y') AS contract_end, CONCAT_WS(', ', F.full_name, G.full_name, H.full_name) AS ro, A.note";
		$params['status_approval']	= 'Resign';
		
		$list 	= $this->resign_model->gets($params);
		$total 	= $this->resign_model->gets($params, TRUE);

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "No");
		$excel->getActiveSheet()->setCellValue("B".$i, "ID Karyawan");
		$excel->getActiveSheet()->setCellValue("C".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("D".$i, "Unit Kontrak");
		$excel->getActiveSheet()->setCellValue("E".$i, "Site Bisnis");
		$excel->getActiveSheet()->setCellValue("F".$i, "Jabatan");
		$excel->getActiveSheet()->setCellValue("G".$i, "Akhir Kontrak");
		$excel->getActiveSheet()->setCellValue("H".$i, "RO");
		$excel->getActiveSheet()->setCellValue("I".$i, "Catatan Resign");
		
		$list_monitoring 	= $this->resign_model->gets($params);
		$no = 1;		
		$i=2;
		foreach ($list_monitoring as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $no);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->employee_number);
			$excel->getActiveSheet()->setCellValue("C".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->company_name);
			$excel->getActiveSheet()->setCellValue("E".$i, $item->site_name);
			$excel->getActiveSheet()->setCellValue("F".$i, $item->position);
			$excel->getActiveSheet()->setCellValue("G".$i, $item->contract_end);
			$excel->getActiveSheet()->setCellValue("H".$i, $item->ro);
			$excel->getActiveSheet()->setCellValue("I".$i, $item->note);

			$i++;
			$no++;
		}

		$sheet = $excel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells( true );
		foreach( $cellIterator as $cell ) {
			$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		}

		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title));
		$objWriter->save(FCPATH."files/".$filename.".xlsx");
		redirect(base_url("files/".$filename.".xlsx"));
		exit;		
	}
	
	public function submit(){
		$type 			=$this->input->post('type');
		$resign_status 	=$this->input->post('resign_status');
		foreach ($this->input->post('id') as $employee_id)
		{
			$this->load->model(array('employee_model', 'approval_model'));
			$data = $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.company_id, A.site_id, A.position_id, A.resign_note'));
			if (!$data){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('resign/approval/'.$type));
			}else{
				if($this->input->post('action') == 'reject'){
					$insert = array('id' => $employee_id, 'status_approval' => 3);
					$result = $this->employee_model->save($insert);

					$approval = array('id' => '', 'employee_id' => $employee_id, 'site_id' => $data->site_id, 'company_id' => $data->company_id, 'position_id' => $data->position_id, 'status_approval' => 'Resign Ditolak', 'note' => $data->resign_note);
						$this->approval_model->save($approval);
				}else{
					if($resign_status == 'Proses'){
						$this->employee_model->save(array('id' => $employee_id, 'resign_status' => $resign_status));
					}else{
						// print_prev($data); die();
						$approval = array('id' => '', 'employee_id' => $employee_id, 'site_id' => $data->site_id, 'company_id' => $data->company_id, 'position_id' => $data->position_id, 'status_approval' => 'Resign', 'note' => $data->resign_note);
						$this->approval_model->save($approval);
						$this->employee_model->save(array('id' => $employee_id, 'status' => 0, 'status_approval' => 0, 'site_id' => 1, 'new_site_id' => 0, 'resign_status' => $resign_status));
					}
				}
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diupdate.','success'));
		redirect(base_url('resign/approval/'.$type));

	}

	public function approval_export($type = FALSE)
	{
		$this->load->model(array('roresign_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = "Daftar Pengajuan Resign";
		$params = $this->input->get();
		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.full_name, A.document_resign, A.resign_submit, A.resign_note, A.resign_burden, A.resign_status, B.name AS site_name";

		$params['status_approval']	= 4;
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		$list 	= $this->roresign_model->gets($params);
		$total 	= $this->roresign_model->gets($params, TRUE);

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "No");
		$excel->getActiveSheet()->setCellValue("B".$i, "ID Karyawan");
		$excel->getActiveSheet()->setCellValue("C".$i, "NIK KTP");
		$excel->getActiveSheet()->setCellValue("D".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("E".$i, "Site Bisnis");
		$excel->getActiveSheet()->setCellValue("F".$i, "Alasan Resign");
		$excel->getActiveSheet()->setCellValue("G".$i, "Tanggungan");
		$excel->getActiveSheet()->setCellValue("H".$i, "Tanggal Resign");
		$excel->getActiveSheet()->setCellValue("I".$i, "Status Pengajuan");
		
		$list_monitoring 	= $this->roresign_model->gets($params);
		$no = 1;		
		$i=2;
		foreach ($list_monitoring as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $no);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->employee_number);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValue("E".$i, $item->site_name);
			$excel->getActiveSheet()->setCellValue("F".$i, $item->resign_burden);
			$excel->getActiveSheet()->setCellValue("G".$i, $item->resign_note);
			$date = new DateTime($item->resign_submit);
			$excel->getActiveSheet()->setCellValue("H".$i, PHPExcel_Shared_Date::PHPToExcel($date));
			$excel->getActiveSheet()->getStyle("H".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			$excel->getActiveSheet()->setCellValue("I".$i, $item->resign_status);

			$i++;
			$no++;
		}
		$sheet = $excel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells( true );
		foreach( $cellIterator as $cell ) {
			$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		}
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title));
		$objWriter->save(FCPATH."files/".$filename.".xlsx");
		redirect(base_url("files/".$filename.".xlsx"));
		exit;		
	}

	public function approval($type = FALSE){
		$data['_TITLE_'] 		= 'Pengajuan Resign';
		$data['_PAGE_'] 		= 'resign/approval';
		$data['_MENU_PARENT_'] 	= 'other';
		$data['_MENU_'] 		= 'resign_approval';
		$this->view($data);
	}

	public function list_approval(){
		$this->load->model(array('roresign_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.full_name, A.document_resign, DATE_FORMAT(A.resign_submit, '%d/%m/%Y') AS resign_submit, A.resign_note, A.resign_burden, A.resign_status, B.name AS site_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		// $params['status']		= 1;
		
		$params['status_approval'] 		= 4;
		
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		
		$params['employee_number']		= $_POST['columns'][1]['search']['value'];
		$params['id_card']				= $_POST['columns'][2]['search']['value'];
		$params['full_name']			= $_POST['columns'][3]['search']['value'];
		$params['site_name']			= $_POST['columns'][4]['search']['value'];
		$params['resign_note']			= $_POST['columns'][5]['search']['value'];
		$params['resign_burden']		= $_POST['columns'][6]['search']['value'];
		$params['resign_submit']		= $_POST['columns'][7]['search']['value'];
		$params['resign_status']		= $_POST['columns'][8]['search']['value'];

		$list 	= $this->roresign_model->gets($params);
		$total 	= $this->roresign_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$document_resign				= '';
			if($item->document_resign != ''){
				$document_resign = '<a href="'.$item->document_resign.'" target="_blank">Dokumen</a>';
			}

			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['employee_number']		= $item->employee_number;
			$result['id_card']				= $item->id_card;
			$result['full_name']			= $item->full_name;
			$result['site_name']			= $item->site_name;
			$result['resign_note']			= $item->resign_note;
			$result['resign_burden']		= $item->resign_burden;
			$result['resign_submit']		= $item->resign_submit;
			$result['resign_status']		= $item->resign_status;
			$result['document_resign']		= $document_resign;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("employee/preview/".$item->id).'">Lihat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Karyawan Resign';
		$data['_PAGE_'] 		= 'resign/list';
		$data['_MENU_PARENT_'] 	= 'other';
		$data['_MENU_'] 		= 'resign';

		$this->view($data);
	}

	public function list_ajax($type = FALSE){

		$this->load->model(array('resign_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		
		$params['id_card']			= $_POST['columns'][0]['search']['value'];
		$params['employee_number']	= $_POST['columns'][1]['search']['value'];
		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['company_name']		= $_POST['columns'][3]['search']['value'];
		$params['site_name']		= $_POST['columns'][4]['search']['value'];
		$params['position']			= $_POST['columns'][5]['search']['value'];
		$params['resign_submit']	= $_POST['columns'][6]['search']['value'];
		$params['ro']				= $_POST['columns'][7]['search']['value'];
		$params['note']				= $_POST['columns'][8]['search']['value'];
		$params['status_approval']	= 'Resign';
		
		$params['columns']			= "B.id";
		$total 	= count($this->resign_model->gets($params));

		$params['columns'] 		= "B.id, B.employee_number, B.id_card, B.full_name, B.site_id, C.name AS company_name, D.name AS site_name, E.name AS position, DATE_FORMAT(B.resign_submit, '%d/%m/%Y') AS resign_submit, CONCAT_WS(', ', F.full_name, G.full_name, H.full_name) AS ro, B.resign_note AS note";
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$list 	= $this->resign_model->gets($params);

		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{			
			$result['no'] 					= $i;
			$result['id_card']				= $item->id_card;
			$result['employee_number']		= $item->employee_number;
			$result['full_name']			= $item->full_name;
			$result['company_name']			= $item->company_name;
			$result['site_name']			= $item->site_name;
			$result['position']				= $item->position;
			$result['resign_submit']		= $item->resign_submit;
			$result['ro']					= $item->ro;
			$result['note']					= $item->note;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("resign/work/".$item->id).'">Paklaring</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("employee/preview/".$item->id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("approval/approval_history/".$item->id).'">Riwayat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function note(){
		$data['_TITLE_'] 		= 'Daftar Alasan Resign';
		$data['_PAGE_'] 		= 'resign/note';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'resign_note';

		$this->view($data);
	}

	public function note_ajax($type = FALSE){

		$this->load->model(array('resign_note_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['columns'] 		= "A.id, A.name";
				
		$params['name']			= $_POST['columns'][1]['search']['value'];

		$total 	= $this->resign_note_model->gets($params, TRUE);
		$list 	= $this->resign_note_model->gets($params);

		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{			
			$result['no'] 		= $i;
			$result['name']		= $item->name;
			$result['action'] 	= '';	
			if($item->id > 1){
				$result['action'] 				=
					'<a class="btn-sm btn-success btn-block" href="'.base_url("resign/note_form/".$item->id).'">Ubah</a>
					<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("resign/note_delete/".$item->id).'">Hapus</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function note_form($resign_note_id = FALSE)
	{
		$this->load->model(array('resign_note_model'));

		$data['id'] 		= '';
		$data['name']		= '';
		
		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['name'] 		= $this->input->post('name');
			
			$this->form_validation->set_rules('name', '', 'required');
		
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$save_id	 	= $this->resign_note_model->save($data);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('resign/note'));
		}

		if ($resign_note_id)
		{
			$data = (array) $this->resign_note_model->get(array('id' => $resign_note_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('resign/note'));
			}
		}

		$data['_TITLE_'] 		= 'Form Alasan Resign';
		$data['_PAGE_'] 		= 'resign/note_form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'resign_note';
		return $this->view($data);
	}

	public function note_delete($resign_note_id = false)
	{
		$this->load->model('resign_note_model');
		if ($resign_note_id)
		{
			$data =  $this->resign_note_model->get(array('id' => $resign_note_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $resign_note_id, 'is_active' => 0);
				$result = $this->resign_note_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('resign/note'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal dihapus.','danger'));
					redirect(base_url('resign/note'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('resign/note'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('resign/note'));
		}
	}

	public function ro(){
		if($this->session->userdata('site') == ''){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan pilih site bisnis.','danger'));
			redirect(base_url());	
		}

		$data['_TITLE_'] 		= 'Karyawan Resign';
		$data['_PAGE_'] 		= 'resign/ro';
		$data['_MENU_PARENT_'] 	= 'other';
		$data['_MENU_'] 		= 'resign';

		$this->view($data);
	}

	public function ro_ajax($type = FALSE){

		$this->load->model(array('resign_model'));
		$column_index = $_POST['order'][0]['column']; 

	
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		
		$params['employee_number']	= $_POST['columns'][1]['search']['value'];
		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['company_name']		= $_POST['columns'][3]['search']['value'];
		$params['site_name']		= $_POST['columns'][4]['search']['value'];
		$params['position']			= $_POST['columns'][5]['search']['value'];
		$params['resign_submit']	= $_POST['columns'][6]['search']['value'];
		$params['ro']				= $_POST['columns'][7]['search']['value'];
		$params['note']				= $_POST['columns'][8]['search']['value'];
		$params['status_approval']	= 'Resign';
		$params['site_id']			= $this->session->userdata('site');

		$params['columns']			= "B.id";
		$total 	= count($this->resign_model->gets($params));

		$params['columns'] 		= "B.id, B.employee_number, B.full_name, B.site_id, C.name AS company_name, D.name AS site_name, E.name AS position, DATE_FORMAT(B.resign_submit, '%d/%m/%Y') AS resign_submit, CONCAT_WS(', ', F.full_name, G.full_name, H.full_name) AS ro, A.note";
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$list 	= $this->resign_model->gets($params);


		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{			
			$result['no'] 					= $i;
			$result['employee_number']		= $item->employee_number;
			$result['full_name']			= $item->full_name;
			$result['company_name']			= $item->company_name;
			$result['site_name']			= $item->site_name;
			$result['position']				= $item->position;
			$result['resign_submit']		= $item->resign_submit;
			$result['ro']					= $item->ro;
			$result['note']					= $item->note;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("resign/work/".$item->id).'">Paklaring</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("roemployee/preview/".$item->id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("approval/approval_history/".$item->id).'">Riwayat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}
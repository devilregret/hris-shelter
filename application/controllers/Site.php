<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('accounting'));
		$role 	= array_merge($role, $this->config->item('sales'));
		$role 	= array_merge($role, $this->config->item('directur'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function accounting(){
		$data['_TITLE_'] 		= 'Site Bisnis';
		$data['_PAGE_'] 		= 'site/accounting';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'site';

		$this->view($data);

	}

	public function accounting_ajax(){
		$this->load->model(array('site_model', 'employee_model', 'proyek_accurate_model'));

		$column_index = $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.code, A.name, A.address, A.contract_status, A.proyek_id, B.name AS city_name, C.code AS company_code, F.name AS branch, CONCAT_WS(', ', G.full_name, H.full_name, I.full_name) AS ro, J.full_name AS supervisor, K.name AS client_name";
		$params['orderby'] 			= $_POST['columns'][$column_index]['data'];
		$params['order']			= $_POST['order'][0]['dir'];
		$params['limit']			= $_POST['length'];
		$params['page']				= $_POST['start'];
		$params['all']				= TRUE;
		
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
			$params['code']				= $_POST['columns'][1]['search']['value'];
			$params['name']				= $_POST['columns'][2]['search']['value'];
			$params['client_name']		= $_POST['columns'][3]['search']['value'];
			$params['company_code']		= $_POST['columns'][4]['search']['value'];
			$params['city_name']		= $_POST['columns'][5]['search']['value'];
			$params['address']			= $_POST['columns'][6]['search']['value'];
			$params['ro']				= $_POST['columns'][7]['search']['value'];
			$params['supervisor']		= $_POST['columns'][8]['search']['value'];
			$params['branch']			= $_POST['columns'][9]['search']['value'];
			$params['contract_status']	= $_POST['columns'][10]['search']['value'];

		$list 	= $this->site_model->gets($params);
		$total 	= $this->site_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$total_employee = $this->employee_model->count(array('site_id' => $item->id, 'status_approval' => 3), TRUE);
			$total_security = $this->employee_model->count(array('site_id' => $item->id, 'status_approval' => 3, 'position_type' => 'Security'), TRUE);

			$result['no'] 				= $i;
			$result['code'] 			= $item->code;
			$result['nomor_proyek'] 	= '';
			$result['name'] 			= $item->name;
			$result['client_name']		= $item->client_name;
			$result['address']			= $item->address;
			$result['city_name']		= $item->city_name;
			$result['company_code']		= $item->company_code;
			$result['ro']				= $item->ro;
			$result['supervisor']		= $item->supervisor;
			$result['branch']			= $item->branch;
			$result['count_employee']	= 'Security:'.$total_security.'<br>NonSecurity:'.($total_employee-$total_security).'<br><strong>Total:'.$total_employee.'</strong>';
			if($item->contract_status == 'Aktif'){
				$result['contract_status'] = '<span class="text-success">'.$item->contract_status.'</span>';
			}else{
				$result['contract_status'] = '<span class="text-danger">'.$item->contract_status.'</span>';
			}


			$proyek_id		= explode(",", $item->proyek_id);
			$proyek 		= $this->proyek_accurate_model->get(array('columns' => 'GROUP_CONCAT(A.nomor_proyek SEPARATOR ", ") AS nomor_proyek', 'in_id' => $proyek_id));
			if($proyek){
				$result['nomor_proyek'] 	= $proyek->nomor_proyek;
			}
			$result['action'] 		= '';
				// '<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("site/preview/".$item->id).'">Lihat</a>
				// <a class="btn-sm btn-success btn-block" href="'.base_url("site/form/".$item->id).'">Ubah</a>
				// <a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("site/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function generate(){
		$this->load->model('site_model');
		$data =  $this->site_model->gets(array('columns' => 'A.id, A.name'));
		foreach ($data as $item) {
			$this->site_model->save(array('id' => $item->id, 'code' => $this->generate_code($item->name)));
		}
	}

	public function delete($site_id = false)
	{
		$this->load->model('site_model');
		if ($site_id)
		{
			$data =  $this->site_model->get(array('id' => $site_id, 'columns' => 'A.id'));
			if ($data)
			{
				$insert = array('id' => $site_id, 'is_active' => 0);
				$result = $this->site_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('site/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('site/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('site/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('site/list'));
		}
	}

	public function preview($site_id=FALSE)
	{
		$this->load->model(array('site_model', 'proyek_accurate_model'));
		$data['_TITLE_'] 		= 'Preview Site Bisnis';
		$data['_PAGE_']	 		= 'site/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'site';

		$data['id'] = $site_id;

		
		if (!$site_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('site/list'));
		}

		$data['preview'] = $this->site_model->preview(array('id' => $site_id));
		$proyek_id		= explode(",", $data['preview']->proyek_id);
		$proyek 		= $this->proyek_accurate_model->get(array('columns' => 'GROUP_CONCAT(A.nomor_proyek SEPARATOR "<br>") AS nomor_proyek', 'in_id' => $proyek_id));
		$data['nomor_proyek']	= '';
		if($proyek){
			$data['nomor_proyek'] 	= $proyek->nomor_proyek;
		}

		$this->load->view('site/preview', $data);
	}

	public function form($site_id = FALSE)
	{
		$this->load->model(array('site_model', 'company_model', 'city_model', 'user_model', 'branch_model', 'client_model', 'employee_model', 'proyek_accurate_model'));

		$data['id'] 					= '';
		$data['branch_id']				= $params['session_branch']	= $this->session->userdata('branch');
		$data['proyek_id']				= '';
		$data['name']					= '';
		$data['address'] 				= '';
		$data['client_id'] 				= '';
		$data['city_id'] 				= '';
		$data['company_id'] 			= '';
		$data['supervisor_id'] 			= '';
		$data['contract_start'] 		= '';
		$data['contract_end'] 			= '';
		$data['contract_terminated']	= '';
		$data['note_terminated'] 		= '';
		$data['contract_status'] 		= '';
		$data['pic_id'] 				= '';
		$data['pic_id_1'] 				= '';
		$data['pic_id_2'] 				= '';
		$data['pic_id_3'] 				= '';

		if($this->input->post()){
			$insert['id']					= $data['id'] 					= $this->input->post('id');
			$insert['branch_id']			= $data['branch_id'] 			= $this->input->post('branch_id');
			$insert['name']					= $data['name'] 				= $this->input->post('name');
			$insert['address']				= $data['address'] 				= $this->input->post('address');
			$insert['client_id']			= $data['client_id'] 			= $this->input->post('client_id');
			$insert['city_id']				= $data['city_id'] 				= $this->input->post('city_id');
			$insert['company_id']			= $data['company_id'] 			= $this->input->post('company_id');
			$insert['supervisor_id']		= $data['supervisor_id'] 		= $this->input->post('supervisor_id');
			$insert['note_terminated']		= $data['note_terminated'] 		= $this->input->post('note_terminated');
			$insert['contract_status']		= $data['contract_status'] 		= $this->input->post('contract_status');

			$data['pic_id'] 				= $this->input->post('pic_id');
			$data['contract_start'] 		= $this->input->post('contract_start');
			$data['contract_end'] 			= $this->input->post('contract_end');
			$data['contract_terminated']	= $this->input->post('contract_terminated');
			if($data['contract_start']){
				$insert['contract_start']	= $data['contract_start'];
			}
			if($data['contract_end']){
				$insert['contract_end']		= $data['contract_end'];
			}
			if($data['contract_terminated']){
				$insert['contract_terminated']	= $data['contract_terminated'];
			}

			$list_proyek					= $this->input->post('list_proyek');

			$this->form_validation->set_rules('name', '', 'required');
			$this->form_validation->set_rules('branch_id', '', 'required');
			$this->form_validation->set_rules('city_id', '', 'required');
			$this->form_validation->set_rules('company_id', '', 'required');
			$this->form_validation->set_rules('supervisor_id', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$site =  $this->site_model->get(array('site_name' => $data['name'], 'columns' => 'A.id'));
				if($site){
					if($data['id'] != $site->id){
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Nama Site sudah terdaftar.','danger'));
						redirect(base_url('site/list'));
					}
				}else{
					$insert['code'] 		= $this->generate_code($data['name']);
				}

				$pic_id_1 = 0;
				$pic_id_2 = 0;
				$pic_id_3 = 0;
				
				if(isset($data['pic_id'][0])){
					$insert['pic_id_1'] = $data['pic_id'][0];
				}
				if(isset($data['pic_id'][1])){
					$insert['pic_id_2'] = $data['pic_id'][1];
				}
				if(isset($data['pic_id'][2])){
					$insert['pic_id_3'] = $data['pic_id'][2];
				}

				$insert['proyek_id']	= '';
				for($i=0; $i<count($list_proyek); $i++) {
					if($i==0){
						$insert['proyek_id']	= $list_proyek[$i];
						continue;
					}

					$insert['proyek_id']	.= ','.$list_proyek[$i];
				}

				if($list_proyek){
					$date 	= $this->proyek_accurate_model->get(array('columns' => 'MIN(A.kontrak_mulai) AS mulai, MIN(A.kontrak_selesai ) AS selesai', 'in_id' => $insert['proyek_id']));
					if($date){
						$insert['contract_start']	= $date->mulai;
						$insert['contract_end']		= $date->selesai;
					}
				}
				$save_id	 	= $this->site_model->save($insert);
				
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
				
				if($insert['contract_status'] != 'Aktif'){
					$this->employee_model->set_nonjob(array('site_id' => $insert['id']));
				}
			}
			redirect(base_url('site/list'));
		}

		if ($site_id)
		{
			$data = (array) $this->site_model->get(array('id' => $site_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('site/list'));
			}
		}
		$params_proyek['columns']			= 'A.id, A.db_id, A.branch_id, A.nomor_proyek, A.nama_proyek';
		$params_proyek['kontrak_selesai']	= '2021-01-01';
		if($data['client_id'] > 0){
			$client = $this->client_model->get(array('columns' => 'A.accurate_customer_id', 'id' => $data['client_id']));
			if($client->accurate_customer_id > 0){
				$params_proyek['customer_id'] 	= $client->accurate_customer_id;
			}
		}

		$data['list_branch'] 		= $this->branch_model->gets(array());
		$data['list_client'] 		= $this->client_model->gets(array());
		$data['list_city'] 			= $this->city_model->list(array());
		$data['list_proyek'] 		= $this->proyek_accurate_model->gets($params_proyek);
		$data['list_company'] 		= $this->company_model->gets(array('columns' => 'A.id, A.name'));
		$data['list_user'] 			= $this->user_model->gets(array('role_id'=>4, 'columns' => 'A.id, A.full_name'));
		$data['list_supervisor'] 	= $this->user_model->gets(array('supervisor_id'=>6, 'columns' => 'A.id, A.full_name'));
		$data['proyek']				= explode(",", $data['proyek_id']);
		
		$data['_TITLE_'] 		= 'Site Bisnis';
		$data['_PAGE_'] 		= 'site/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'site';
		return $this->view($data);
	}

	
	public function list(){
		$data['_TITLE_'] 		= 'Site Bisnis';
		$data['_PAGE_'] 		= 'site/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'site';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('site_model', 'employee_model', 'proyek_accurate_model'));

		$column_index = $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.code, A.name, A.address, A.contract_status, A.proyek_id, B.name AS city_name, C.code AS company_code, F.name AS branch, CONCAT_WS(', ', G.full_name, H.full_name, I.full_name) AS ro, J.full_name AS supervisor, K.name AS client_name";
		$params['orderby'] 			= $_POST['columns'][$column_index]['data'];
		$params['order']			= $_POST['order'][0]['dir'];
		$params['limit']			= $_POST['length'];
		$params['page']				= $_POST['start'];
		$params['all']				= TRUE;
		
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
			$params['code']				= $_POST['columns'][1]['search']['value'];
			$params['name']				= $_POST['columns'][2]['search']['value'];
			$params['client_name']		= $_POST['columns'][3]['search']['value'];
			$params['company_code']		= $_POST['columns'][4]['search']['value'];
			$params['city_name']		= $_POST['columns'][5]['search']['value'];
			$params['address']			= $_POST['columns'][6]['search']['value'];
			$params['ro']				= $_POST['columns'][7]['search']['value'];
			$params['supervisor']		= $_POST['columns'][8]['search']['value'];
			$params['branch']			= $_POST['columns'][9]['search']['value'];
			$params['contract_status']	= $_POST['columns'][10]['search']['value'];

		$list 	= $this->site_model->gets($params);
		$total 	= $this->site_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$total_employee = $this->employee_model->count(array('site_id' => $item->id, 'status_approval' => 3), TRUE);
			$total_security = $this->employee_model->count(array('site_id' => $item->id, 'status_approval' => 3, 'position_type' => 'Security'), TRUE);

			$result['no'] 				= $i;
			$result['code'] 			= $item->code;
			$result['nomor_proyek'] 	= '';
			$result['name'] 			= $item->name;
			$result['client_name']		= $item->client_name;
			$result['address']			= $item->address;
			$result['city_name']		= $item->city_name;
			$result['company_code']		= $item->company_code;
			$result['ro']				= $item->ro;
			$result['supervisor']		= $item->supervisor;
			$result['branch']			= $item->branch;
			$result['count_employee']	= 'Security:'.$total_security.'<br>NonSecurity:'.($total_employee-$total_security).'<br><strong>Total:'.$total_employee.'</strong>';
			if($item->contract_status == 'Aktif'){
				$result['contract_status'] = '<span class="text-success">'.$item->contract_status.'</span>';
			}else{
				$result['contract_status'] = '<span class="text-danger">'.$item->contract_status.'</span>';
			}


			$proyek_id		= explode(",", $item->proyek_id);
			$proyek 		= $this->proyek_accurate_model->get(array('columns' => 'GROUP_CONCAT(A.nomor_proyek SEPARATOR ", ") AS nomor_proyek', 'in_id' => $proyek_id));
			if($proyek){
				$result['nomor_proyek'] 	= $proyek->nomor_proyek;
			}
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("site/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("site/form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("site/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function export()
	{
		$this->load->model(array('site_model', 'employee_model', 'proyek_accurate_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title 				= "Daftar Site";
		$params 			= $this->input->get();
		$title = 'site';
		$files = glob(FCPATH."files/".$title."/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Nomor Proyek");
		$excel->getActiveSheet()->setCellValue("B".$i, "Kode Site");
		$excel->getActiveSheet()->setCellValue("C".$i, "Nama Site");
		$excel->getActiveSheet()->setCellValue("D".$i, "Nama Perusahaan");
		$excel->getActiveSheet()->setCellValue("E".$i, "Nama Bisnis Unit");
		$excel->getActiveSheet()->setCellValue("F".$i, "Kota Site Bisnis");
		$excel->getActiveSheet()->setCellValue("G".$i, "Alamat Site Bisnis");
		$excel->getActiveSheet()->setCellValue("H".$i, "Nama RO");
		$excel->getActiveSheet()->setCellValue("I".$i, "Supervisor RO");
		$excel->getActiveSheet()->setCellValue("J".$i, "Nama Branch");
		$excel->getActiveSheet()->setCellValue("K".$i, "Status Kontrak");
		$excel->getActiveSheet()->setCellValue("L".$i, "Jumlah Karyawan");

		$params['columns'] 		= "A.id, A.proyek_id, A.code, A.name, A.address, A.contract_status, B.name AS city_name, C.code AS company_code, F.name AS branch, CONCAT_WS(', ', G.full_name, H.full_name, I.full_name) AS ro, J.full_name AS supervisor, K.name AS client_name";
		$params['orderby']		= "A.name";
		$params['order']		= "ASC";
		$params['all']				= TRUE;
		$list_site = $this->site_model->gets($params);
		$i=2;
		foreach ($list_site as $item) {
			$count_employee	= $this->employee_model->count(array('site_id' => $item->id, 'status_approval' => 3), TRUE);
			$nomor_proyek	= '';
			$proyek_id		= explode(",", $item->proyek_id);
			$proyek 		= $this->proyek_accurate_model->get(array('columns' => 'GROUP_CONCAT(A.nomor_proyek SEPARATOR ", ") AS nomor_proyek', 'in_id' => $proyek_id));
			if($proyek){
				$nomor_proyek 	= $proyek->nomor_proyek;
			}

			$excel->getActiveSheet()->setCellValue("A".$i, $nomor_proyek);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->code);
			$excel->getActiveSheet()->setCellValue("C".$i, $item->name);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->client_name);
			$excel->getActiveSheet()->setCellValue("E".$i, $item->company_code);
			$excel->getActiveSheet()->setCellValue("F".$i, $item->city_name);
			$excel->getActiveSheet()->setCellValue("G".$i, $item->address);
			$excel->getActiveSheet()->setCellValue("H".$i, $item->ro);
			$excel->getActiveSheet()->setCellValue("I".$i, $item->supervisor);
			$excel->getActiveSheet()->setCellValue("J".$i, $item->branch);
			$excel->getActiveSheet()->setCellValue("K".$i, $item->contract_status);
			$excel->getActiveSheet()->setCellValueExplicit("L".$i, $count_employee);
			$i++;
		}
		$excel->getActiveSheet()->setTitle($title);
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function create_code(){
		$this->load->model('site_model');
		$list_site = $this->site_model->gets(array('columns' => 'A.id, A.code, A.name', 'code' => ''));
		foreach($list_site AS $item){
			if($item->code != ''){
				continue;
			}
			$code = $this->generate_code($item->name);
			$this->site_model->save(array('id' => $item->id, 'code' => $code));
		}
	}

	public function syncronize_site(){
		$this->load->model(array('accurate_model', 'proyek_accurate_model'));
		$accurate 		= $this->accurate_model->get(array('id' => 1, 'columns' => 'A.id, A.access_token'));
		$authorization = "Authorization: Bearer ".$accurate->access_token;
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
		curl_setopt($ch, CURLOPT_URL, "https://account.accurate.id/api/db-list.do");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$response = curl_exec($ch); 
		$resultBU = json_decode($response, true);
		curl_close($ch);

		if(isset($resultBU['d'])){
			// $this->proyek_accurate_model->nonactive_all();
			foreach($resultBU['d'] AS $item){
				$BUId = $item['id'];
				if($BUId == 423160){
					$BUName = 'SN'; 
				}else if($BUId == 433997){
					$BUName = 'SNI';
				}else if($BUId == 491420){
					$BUName = 'ION';
				}

				$ch = curl_init(); 
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
				curl_setopt($ch, CURLOPT_URL, "https://account.accurate.id/api/open-db.do?id=".$BUId);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
				$response = curl_exec($ch); 
				$resultSession = json_decode($response, true);
				curl_close($ch);

				$header = array('Content-Type: application/json' , $authorization, "X-Session-ID: ".$resultSession['session']);
				$projectURL = $resultSession['host']."/accurate/api/project/list.do?sp.pageSize=100&sp.sort=name|asc";
				$ch = curl_init(); 
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
				curl_setopt($ch, CURLOPT_URL, $projectURL);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
				$response = curl_exec($ch); 
				$resultProject = json_decode($response, true);
				curl_close($ch);

				if(isset($resultProject['d'])){
					for($page=1; $page <= $resultProject['sp']['pageCount']; $page++){

						$header = array('Content-Type: application/json' , $authorization, "X-Session-ID: ".$resultSession['session']);
						$projectURL = $resultSession['host']."/accurate/api/project/list.do?sp.pageSize=100&sp.sort=name|asc&fields=id,name,no,address&suspendedFilter=false&sp.page=".$page;

						$ch = curl_init(); 
						curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
						curl_setopt($ch, CURLOPT_URL, $projectURL);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
						$response = curl_exec($ch); 
						$resultPrpject = json_decode($response, true);
						curl_close($ch);

						foreach($resultProject['d'] AS $item){
							$projectName = $item['name'];
							$projectNo = $item['no'];
							$projectId = $item['id'];
								$projectBranch = '';
							if($item['branchId'] == 50){
								$projectBranch = 'Head Office';
							}else if($item['branchId'] == 101 ||  $item['branchId'] == 1801){
								$projectBranch = 'Central';
							}else if($item['branchId'] == 150 ||  $item['branchId'] == 1751 ){
								$projectBranch = 'East';
							}else if($item['branchId'] == 250){
								$projectBranch = 'Makasar';
							}else if($item['branchId'] == 52 ||  $item['branchId'] == 1750){
								$projectBranch = 'West';
							}

							$data['id']				= $item['id'];
							$data['db_id']			= $BUId;
							$data['customer_id']	= 0;
							$data['branch_id']		= 0;
							$data['nomor_proyek']	= $item['no'];
							$data['nama_proyek']	= $item['name'];
							$data['kontrak_mulai']	= '';
							$data['kontrak_selesai']= '';
							$data['is_active']		= 1;
							if($item['customerId']){
								$data['customer_id'] = $item['customerId'];
							}
							if($item['branchId']){
								$data['branch_id'] = $item['branchId'];
							}
							if($item['startDate'] != ''){
								$data['kontrak_mulai']	= substr($item['startDate'], 6, 4).'-'.substr($item['startDate'], 3, 2).'-'.substr($item['startDate'], 0, 2);
							}
							if($item['finishDate'] != ''){
								$data['kontrak_selesai']	= substr($item['finishDate'], 6, 4).'-'.substr($item['finishDate'], 3, 2).'-'.substr($item['finishDate'], 0, 2);
							}
							$this->proyek_accurate_model->replace($data);
						}					
					}
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  Sinkronisasi Berhasil.','success'));
			redirect(base_url('site/list'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Sinkronisasi Gagal.','danger'));
			redirect(base_url('site/list'));
		}
	}

	private function generate_code($site_name = ''){
		
		$this->load->model('site_model');

		$site_name 	= preg_replace("/[^A-Za-z0-9 ]/", "", $site_name);
		$site_name 	= str_replace("  ", " ", strtoupper($site_name));
		$last_char 	= substr($site_name, -1);

		if($last_char == ' '){
			$site_name = substr($site_name, 0, -1);
		}

		$word 		= explode(" ", $site_name);
		$count 		= count($word);
		$code 		= "";
		
		if($count > 2){
			$code = substr($word[0], 0, 1).substr($word[1], 0, 1).substr($word[2], 0, 1);
		}else if($count == 2){
			if(strlen($word[1]) < 2){
				$code = substr($word[0], 0, 2).substr($word[1], 0, 1);
			}else{
				$code = substr($word[0], 0, 1).substr($word[1], 0, 2);
			}
		}else if($count == 1){
			$code = substr($word[0], 0, 3);
		}

		$exist  = $this->site_model->get(array('columns' => 'A.code', 'count_code' => $code, 'order' => 'DESC', 'orderby', 'A.code'));
		$inc = '00';
		if($exist){
			$inc = intval(substr($exist->code, 3, 5))+1;	
			// print_prev(substr($exist->code, 3, 5));
			if(strlen($inc) == 1){
				$inc = '0'.$inc;
			}
		}

		return $code.$inc;
	}

	public function import(){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('site_model', 'proyek_accurate_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			$succes 		= 0;
			for ($row = 2; $row <= $rend; $row++) {
				$data['id'] 		= '';
				$data['proyek_id']	= '0';
				$nomor_proyek		= replace_null($sheet->getCell('A'.$row)->getValue());
				$code				= replace_null($sheet->getCell('B'.$row)->getValue());
				$name 				= replace_null($sheet->getCell('C'.$row)->getValue());

				$site = $this->site_model->get(array('site_name' => $name, 'code' => $code, 'columns' => 'A.id'));
				if(!$site){
					continue;
				}

				$proyek 	= $this->proyek_accurate_model->get(array('nomor_proyek' => $nomor_proyek, 'columns' => 'A.id, A.kontrak_mulai, A.kontrak_selesai'));
				if(!$proyek){
					continue;
				}
				
				$data['id']	= $site->id;
				$data['proyek_id']	= $proyek->id;
				$data['contract_start']	= $proyek->kontrak_mulai;
				$data['contract_end']	= $proyek->kontrak_selesai;
				$save_id = $this->site_model->save($data);
				$succes++;
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> '.$succes.' data berhasil diupdate.','success'));
			redirect(base_url('site/list'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
			redirect(base_url('site/list'));
		}
	}

	public function import_proyek(){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('site_model', 'proyek_accurate_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			$succes 		= 0;
			for ($row = 2; $row <= $rend; $row++) {
				$data['id'] 		= replace_null($sheet->getCell('A'.$row)->getValue());
				$data['proyek_id']	= '0';
				$nomor_proyek		= replace_null($sheet->getCell('F'.$row)->getValue());
				// $code				= replace_null($sheet->getCell('B'.$row)->getValue());
				// $name 				= replace_null($sheet->getCell('C'.$row)->getValue());

				// $site = $this->site_model->get(array('id' => $name, 'code' => $code, 'columns' => 'A.id'));
				// if(!$site){
				// 	continue;
				// }

				$proyek 	= $this->proyek_accurate_model->get(array('nomor_proyek' => $nomor_proyek, 'columns' => 'A.id, A.kontrak_mulai, A.kontrak_selesai'));
				if(!$proyek){
					continue;
				}
				
				// $data['id']	= $site->id;
				$data['proyek_id']	= $proyek->id;
				$data['contract_start']	= $proyek->kontrak_mulai;
				$data['contract_end']	= $proyek->kontrak_selesai;
				// print_prev($data);
				$save_id = $this->site_model->save($data);
				$succes++;
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> '.$succes.' data berhasil diupdate.','success'));
			redirect(base_url('site/list'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
			redirect(base_url('site/list'));
		}
	}

	public function duplicate(){

		$this->load->model(array('site_model'));
		
		$result = $this->site_model->check_duplicate();

		foreach($result AS $item){
			// print_prev($item);
			$data['id'] 	= $item->id;
			$data['code'] 	= $this->generate_code($item->name);
			$this->site_model->save($data);
			print_prev($data);
			// $code = substr($item->code, 0, 3);
			// $list_site = $this->site_model->check_code($item->code);

			// $i = 5;
			// foreach($list_site AS $site){
			// 	$inc = $i;
			// 	if(strlen($inc) == 1){
			// 		$inc = '0'.$i;
			// 	}
			// 	$data['id'] 	= $site->id;
			// 	$data['code'] 	= $code.$inc;
			// 	// $this->site_model->save($data);
			// 	print_prev($data);
			// 	$i++;
			// }
			// print_prev($list_site);
		}
	}
}
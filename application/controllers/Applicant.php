<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Applicant extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('recruitment'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function generate(){
		$this->load->model('user_model');
		$employee = $this->user_model->employee_gets(array('not_user' => TRUE, 'columns' => 'A.id, A.id_card, A.email, A.full_name, A.company_id, A.branch_id'));
		$count 		= 0;
		$username 	= "";
		foreach($employee as $item)
		{
			if($item->id_card == ''){
				continue;
			}

			$user['username'] 			= $item->id_card;
			$user['email'] 				= $item->email;
			$user['full_name'] 			= $item->full_name;
			$user['password']			= generate_password($item->id_card);
			// $user['role_id']			= 1;
			$user['company_id']			= $item->company_id;
			$user['branch_id']			= $item->branch_id;
			$user['employee_id']		= $item->id;
			$user['id']					= '';
			$result = $this->user_model->save($user);
			if($result){
				$count++;
				$username .= $item->id_card.', ';
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> '.$count.' user berhasil dibuat : '.$username,'success'));
		redirect(base_url('applicant/list'));
	}

	public function delete($user_id = false)
	{
		$this->load->model('user_model');
		if ($user_id)
		{
			$data =  $this->user_model->get(array('id' => $user_id, 'columns' => 'A.id'));
			if ($data)
			{
				$insert = array('id' => $user_id, 'is_active' => 0);
				$result = $this->user_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dinonaktifkan.','success'));
					redirect(base_url('applicant/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal dinonaktifkan.','danger'));
					redirect(base_url('applicant/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('applicant/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('applicant/list'));
		}
	}

	public function active($user_id = false)
	{
		$this->load->model('user_model');
		if ($user_id)
		{
			$data =  $this->user_model->get(array('id' => $user_id, 'columns' => 'A.id'));
			if ($data)
			{
				$insert = array('id' => $user_id, 'is_active' => 1);
				$result = $this->user_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil diaktifkan.','success'));
					redirect(base_url('applicant/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal diaktifkan.','danger'));
					redirect(base_url('applicant/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('applicant/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('applicant/list'));
		}
	}

	public function preview($user_id=FALSE)
	{
		$this->load->model('user_model');
		$data['_TITLE_'] 		= 'Preview Pengguna';
		$data['_PAGE_']	 		= 'applicant/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'applicant';

		$data['id'] = $user_id;

		
		if (!$user_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('applicant/list'));
		}

		$data['preview'] = $this->user_model->preview(array('id' => $user_id));
		$this->load->view('applicant/preview', $data);
	}

	public function form($user_id = FALSE)
	{
		$this->load->model(array('user_model', 'branch_model','role_model', 'auth_model', 'company_model','account_model'));
		$data['id']		 	= $user_id;
		$data['username'] 	= '';
		$data['password'] 	= '';
		$data['repassword'] = '';
		if($this->input->post('password')){
			$data['username'] 	= $this->input->post('username');
			$data['password'] 	= $this->input->post('password');
			$data['repassword'] = $this->input->post('repassword');
				
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('repassword', 'Ulangi Password', 'required|matches[password]');
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{

				// print_prev($data);
				// die();
				$params['id']		= $this->input->post('id');
				$params['password']	= generate_password($data['password']);
				$params['username'] = $data['username'];
				$params['role_id'] 	= 1; 
				$params['is_active']= 1; 
				// print_prev($params);
				// die();
				$result	 			= $this->account_model->save($params);
					
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
				redirect(base_url('applicant/list'));
			}
		}

		$data['account'] = $this->user_model->get_user(array('id' => $user_id));
		$data['list_company'] 	= $this->company_model->gets(array());
		$data['list_branch'] 	= $this->branch_model->gets(array());
		$data['list_role'] 		= $this->role_model->gets(array());

		$data['_TITLE_'] 		= 'Pengguna';
		$data['_PAGE_'] 		= 'applicant/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'applicant';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Pengguna';
		$data['_PAGE_'] 		= 'applicant/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'applicant';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('user_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.username, H.email, H.full_name, H.phone_number, B.name AS role_name, E.name AS city_domisili, G.name AS province_name, H.id_card, D.code AS company_code, I.name AS site_name, A.is_active";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['id_card']		= $_POST['columns'][1]['search']['value'];
		$params['email']		= $_POST['columns'][2]['search']['value'];
		$params['employee_name']= $_POST['columns'][3]['search']['value'];
		$params['phone_number']	= $_POST['columns'][4]['search']['value'];
		$params['city_domisili']= $_POST['columns'][5]['search']['value'];
		$params['company_code']	= $_POST['columns'][6]['search']['value'];
		$params['site_name']	= $_POST['columns'][7]['search']['value'];
		$params['applicant']	= TRUE;
		$params['all']			= true;
		
		if($this->session->userdata('branch') > 1){
			$params['session_branch']	= $this->session->userdata('branch');
		}

		$list 	= $this->user_model->gets($params);
		$total 	= $this->user_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 				= $i;
			$result['id_card'] 			= $item->id_card;
			$result['email'] 			= $item->email;
			$result['full_name']		= $item->full_name;
			$result['phone_number']		= $item->phone_number;
			$result['city_domisili']	= $item->city_domisili;
			$result['company_code']		= $item->company_code;
			$result['site_name']		= $item->site_name;
			$result['action'] 	=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("applicant/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("applicant/form/".$item->id).'">Ubah</a>';
			if($item->is_active == 1){
				$result['action'] .= '<a onclick="confirm_nonactive(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("applicant/delete/".$item->id).'">Non Aktifkan</a>';
			}else{
				$result['action'] .= '<a onclick="confirm_active(this)" class="btn-sm btn-warning btn-block" style="cursor:pointer;" data-href="'.base_url("applicant/active/".$item->id).'">Aktifkan</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}
	
	public function export($type = FALSE)
	{
		$this->load->model(array('user_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = "data_akun_pelamar";
		if($type == 'notcomplete'){
			$title = "data_pelamar_tidak_lengkap";	
		}

		$params = $this->input->get();

		if($type == 'all'){
			$files = glob(FCPATH."files/data_akun_pelamar/*");
		}else if($type == 'notcomplete'){
			$params['notcomplete']	= TRUE;
			$files = glob(FCPATH."files/data_pelamar_tidak_lengkap/*");
		}

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}
		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);
		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Tanggal Daftar");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("C".$i, "Username");
		$excel->getActiveSheet()->setCellValue("D".$i, "Email");
		$excel->getActiveSheet()->setCellValue("E".$i, "No. Telephon/HP");
		$excel->getActiveSheet()->setCellValue("F".$i, "Kota Domisili");
		$excel->getActiveSheet()->setCellValue("G".$i, "Provinsi");

		$params['columns'] 		= "A.id, A.created_at, A.username, H.email, A.full_name, H.phone_number, B.name AS role_name, E.name AS city_domisili, G.name AS province_name";

		if($this->session->userdata('branch') > 1){
			$params['session_branch']	= $this->session->userdata('branch');
		}

		$params['applicant']	= TRUE;
		$list_candidate 		= $this->user_model->gets($params);

		$i=2;
		foreach ($list_candidate as $item) {
			$date = new DateTime($item->created_at);
			$excel->getActiveSheet()->setCellValue("A".$i, PHPExcel_Shared_Date::PHPToExcel($date));
			$excel->getActiveSheet()->getStyle("A".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->username);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->email);
			$excel->getActiveSheet()->setCellValueExplicit("E".$i, $item->phone_number);
			$excel->getActiveSheet()->setCellValue("F".$i, $item->city_domisili);
			$excel->getActiveSheet()->setCellValue("G".$i, $item->province_name);
			$i++;
		}
		$excel->getActiveSheet()->setTitle($title);
		

		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		print_r(FCPATH."files/".$title."/".$filename.".xlsx");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function export_employee()
	{
		$this->load->model(array('user_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = "data_akun_pelamar";
		$params = $this->input->get();
		$files = glob(FCPATH."files/data_akun_pelamar/*");
		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$params['columns'] 		= "A.id, A.created_at, H.id_card, A.username, H.email, H.full_name, H.phone_number, E.name AS city_domisili, D.code AS company_code, I.name AS site_name";

		$params['role']			= 1;
		$list_candidate 		= $this->user_model->gets($params);

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);
		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Tanggal Daftar");
		$excel->getActiveSheet()->setCellValue("B".$i, "NIK KTP");
		$excel->getActiveSheet()->setCellValue("C".$i, "Username");
		$excel->getActiveSheet()->setCellValue("D".$i, "Email");
		$excel->getActiveSheet()->setCellValue("E".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("F".$i, "No. Telephon/HP");
		$excel->getActiveSheet()->setCellValue("G".$i, "Kota Domisili");
		$excel->getActiveSheet()->setCellValue("H".$i, "Bisnis Unit");
		$excel->getActiveSheet()->setCellValue("I".$i, "Penempatan");


		$i=2;
		foreach ($list_candidate as $item) {
			$date = new DateTime($item->created_at);
			$excel->getActiveSheet()->setCellValue("A".$i, PHPExcel_Shared_Date::PHPToExcel($date));
			$excel->getActiveSheet()->getStyle("A".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->username);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->email);
			$excel->getActiveSheet()->setCellValue("E".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("F".$i, $item->phone_number);
			$excel->getActiveSheet()->setCellValue("G".$i, $item->city_domisili);
			$excel->getActiveSheet()->setCellValue("H".$i, $item->company_code);
			$excel->getActiveSheet()->setCellValue("I".$i, $item->site_name);
			$i++;
		}
		$excel->getActiveSheet()->setTitle($title);
		$sheet = $excel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells( true );
		foreach( $cellIterator as $cell ) {
			$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		}
		$excel->createSheet();
		$excel->setActiveSheetIndex(0);

		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		print_r(FCPATH."files/".$title."/".$filename.".xlsx");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function check()
	{
		$this->load->model(array('employee_model', 'user_model'));
		$data['id_card'] 	= $this->input->post('id_card');
		$data['employee'] 	= '';
		$data['user'] 		= '';

		if($data['id_card']){
			$data['employee'] 	= $this->employee_model->preview(array('id_card' => $data['id_card']));
			if($data['employee']){
				$data['user'] 	= $this->user_model->get(array('employee_id' => $data['employee']->id, 'columns' => 'A.id, A.full_name, A.username'));
			}
		}

		$data['_TITLE_']		= 'Cek NIK karyawan';
		$data['_PAGE_']			= 'applicant/check';
		$data['_MENU_PARENT_']	= 'setting';
		$data['_MENU_']			= 'check';
		$this->view($data);
	}
}
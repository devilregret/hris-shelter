<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vacancy extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('recruitment'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function delete($vacancy_id = false)
	{
		$this->load->model('vacancy_model');
		if ($vacancy_id)
		{
			$data =  $this->vacancy_model->get(array('id' => $vacancy_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $vacancy_id, 'is_active' => 0);
				$result = $this->vacancy_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('vacancy/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('vacancy/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('vacancy/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('vacancy/list'));
		}
	}

	public function preview($vacancy_id=FALSE)
	{
		$this->load->model('vacancy_model');
		$data['_TITLE_'] 		= 'Preview Lowongan Pekerjaan';
		$data['_PAGE_']	 		= 'vacancy/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'vacancy';

		$data['id'] = $vacancy_id;

		
		if (!$vacancy_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('vacancy/list'));
		}

		$data['preview'] = $this->vacancy_model->preview(array('id' => $vacancy_id));
		$this->load->view('vacancy/preview', $data);
	}

	public function form($vacancy_id = FALSE)
	{
		$this->load->model(array('vacancy_model', 'province_model', 'site_model', 'position_model', 'company_model'));

		$data['id'] 					= '';
		$data['company_id']				= '';
		$data['site_id']				= '';
		$data['position_id']			= '';
		$data['province_id']			= '';
		$data['title']					= '';
		$data['content'] 				= '';
		$data['needs'] 					= '';
		$data['start_date'] 			= '';
		$data['end_date'] 				= '';
		$data['is_tes_psikotes'] 		= '0';
		$data['is_tes_deret_angka'] 	= '0';
		$data['is_tes_hitung_cepat'] 	= '0';
		$data['is_ketelitian']	 		= '0';
		$data['is_kraepelin']	 		= '0';
		$data['durasi_psikotes'] 		= '20';
		$data['durasi_deret_angka'] 	= '10';
		$data['durasi_hitung_cepat'] 	= '10';
		$data['durasi_ketelitian'] 		= '10';
		$data['durasi_kolom_kraepelin'] = '10';
		$data['min_lolos_deret_angka'] 	= '70';
		$data['min_lolos_hitung_cepat'] = '70';
		$data['min_lolos_ketelitian'] 	= '70';
		$data['jumlah_kolom_kraepelin'] = '10';
		$data['jumlah_baris_kraepelin'] = '10';
		if($this->input->post()){
			$data['id'] 					= $this->input->post('id');
			$data['company_id']				= $this->input->post('company_id');
			$data['site_id']				= $this->input->post('site_id');
			$data['position_id']			= $this->input->post('position_id');
			$data['province_id']			= $this->input->post('province_id');
			$data['title'] 					= $this->input->post('title');
			$data['content'] 				= $this->input->post('content');
			$data['needs']					= $this->input->post('needs');
			$data['start_date'] 			= $this->input->post('start_date');
			$data['end_date']				= $this->input->post('end_date');
			$data['is_tes_psikotes'] 		= $this->input->post('is_tes_psikotes');
			$data['is_tes_deret_angka'] 	= $this->input->post('is_tes_deret_angka');
			$data['is_tes_hitung_cepat'] 	= $this->input->post('is_tes_hitung_cepat');
			$data['is_ketelitian'] 			= $this->input->post('is_ketelitian');
			$data['is_kraepelin'] 			= $this->input->post('is_kraepelin');
			$data['durasi_psikotes'] 		= $this->input->post('durasi_psikotes');
			$data['durasi_deret_angka'] 	= $this->input->post('durasi_deret_angka');
			$data['durasi_hitung_cepat'] 	= $this->input->post('durasi_hitung_cepat');
			$data['durasi_ketelitian']	 	= $this->input->post('durasi_ketelitian');
			$data['durasi_kolom_kraepelin'] = $this->input->post('durasi_kolom_kraepelin');
			$data['min_lolos_deret_angka'] 	= $this->input->post('min_lolos_deret_angka');
			$data['min_lolos_hitung_cepat'] = $this->input->post('min_lolos_hitung_cepat');
			$data['min_lolos_ketelitian'] 	= $this->input->post('min_lolos_ketelitian');
			$data['jumlah_kolom_kraepelin'] = $this->input->post('jumlah_kolom_kraepelin');
			$data['jumlah_baris_kraepelin'] = $this->input->post('jumlah_baris_kraepelin');

			$this->form_validation->set_rules('title', '', 'required');
			$this->form_validation->set_rules('start_date', '', 'required');
			$this->form_validation->set_rules('end_date', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{

				$save_id	= $this->vacancy_model->save($data);
				if (!$save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));		
					redirect(base_url('vacancy/list'));
					die();
				}

				$url_file = 'files/vacancy/'.$save_id;
				$file_path = FCPATH.$url_file;
				if(!is_dir($file_path)){
					mkdir($file_path, 0755, TRUE);
				}

				$config['upload_path'] 	= $file_path;
				$config['overwrite']  	= TRUE;
				$config['allowed_types']= 'jpg|jpeg|png';
				if($_FILES['flyer']['name'] != ""){
					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'lowongan';
					$this->load->library('upload', $config, 'flyer');

					if (!$this->flyer->do_upload('flyer')) {
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> '.$this->file->display_errors().'.','danger'));
					} else {
						$document 	= $this->flyer->data();
						$save_id = $this->vacancy_model->save(array('id' => $save_id, 'flyer' => base_url($url_file.'/'.$document['file_name'])));
					}
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));		
			redirect(base_url('vacancy/list'));
		}

		if ($vacancy_id)
		{
			$data = (array) $this->vacancy_model->get(array('id' => $vacancy_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('vacancy/list'));
			}
		}else{
			$data['flyer']					= '';
		}

		if($this->session->userdata('branch') > 1){
			$data['list_province'] 	= $this->province_model->gets(array('branch_id' => $this->session->userdata('branch'), 'columns' => 'A.id, A.name'));
			$data['list_site'] 		= $this->site_model->gets(array('branch_id' => $this->session->userdata('branch'), 'columns' => 'A.id, A.name'));
		}else{
			$data['list_province'] 	= $this->province_model->gets(array('columns' => 'A.id, A.name'));
			$data['list_site'] 		= $this->site_model->gets(array('columns' => 'A.id, A.name'));
		}
		$data['list_position'] 		= $this->position_model->gets(array('columns' => 'A.id, A.name'));
		$data['list_company'] 		= $this->company_model->gets(array('columns' => 'A.id, A.name'));
		$data['_TITLE_'] 		= 'Informasi Lowongan';
		$data['_PAGE_'] 		= 'vacancy/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'vacancy';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Informasi Lowongan';
		$data['_PAGE_'] 		= 'vacancy/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'vacancy';
		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('vacancy_model', 'applicant_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.title, DATE_FORMAT(A.start_date, '%d/%m/%Y') AS start_date, DATE_FORMAT(A.end_date , '%d/%m/%Y') AS end_date, A.needs, B.name AS province_name, C.name AS branch_name, D.name AS company_name, E.name AS site_name, F.name AS position_name,A.is_tes_psikotes,A.is_tes_deret_angka,A.is_tes_hitung_cepat,A.is_ketelitian,A.is_kraepelin ";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['title']		= $_POST['columns'][0]['search']['value'];
		$params['start_date']	= $_POST['columns'][1]['search']['value'];
		$params['end_date']		= $_POST['columns'][2]['search']['value'];
		$params['province_name']= $_POST['columns'][3]['search']['value'];
		$params['branch_name']	= $_POST['columns'][4]['search']['value'];
		$params['company_name']	= $_POST['columns'][5]['search']['value'];
		$params['site_name']	= $_POST['columns'][6]['search']['value'];
		$params['position_name']= $_POST['columns'][7]['search']['value'];

		if($this->session->userdata('branch') > 1){
			$params['session_branch']	= $this->session->userdata('branch');
		}

		$list 	= $this->vacancy_model->gets($params);
		$total 	= $this->vacancy_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$daftarTes = "";
			if($item->is_tes_psikotes==1) { $daftarTes.='<span class="badge badge-pill badge-success" style="padding:10px 10px 10px 10px;margin-bottom:5px;width:100%">DISC</span>';};
			if($item->is_tes_deret_angka==1) {  $daftarTes.='<span class="badge badge-pill badge-success" style="padding:10px 10px 10px 10px;margin-bottom:5px;width:100%">Deret Angka</span>';};
			if($item->is_tes_hitung_cepat==1) { $daftarTes.='<span class="badge badge-pill badge-success" style="padding:10px 10px 10px 10px;margin-bottom:5px;width:100%">Hitung Cepat</span>';};
			if($item->is_ketelitian==1) { $daftarTes.='<span class="badge badge-pill badge-success" style="padding:10px 10px 10px 10px;margin-bottom:5px;width:100%">Ketelitian</span>';};
			if($item->is_kraepelin==1) { $daftarTes.='<span class="badge badge-pill badge-success" style="padding:10px 10px 10px 10px;margin-bottom:5px;width:100%">Kraepelin</span>';};

			$result['daftar_tes'] 			= $daftarTes;
			$result['no'] 					= $i;
			$result['title'] 				= $item->title;
			$result['start_date'] 			= $item->start_date;
			$result['end_date']				= $item->end_date;
			$result['branch_name']			= $item->branch_name;
			$result['province_name']		= $item->province_name;
			$result['company_name']			= $item->company_name;
			$result['site_name']			= $item->site_name;
			$result['position_name']		= $item->position_name;
			$result['needs'] 				= $item->needs;
			$result['applicant'] 			= '<span class="badge badge-pill badge-info" style="padding:10px 10px 10px 10px;margin-bottom:5px;width:100%"><a style="color:white" href="'.base_url("vacancy/applicant/").$item->id.'">'.$this->applicant_model->gets(array('vacancy_id' => $item->id), TRUE).' Pelamar</a></span>';
			$result['submission'] 			= '<span class="badge badge-pill badge-warning" style="padding:10px 10px 10px 10px;margin-bottom:5px;width:100%"><a style="color:white" href="'.base_url("vacancy/submission/").$item->id.'">'.$this->applicant_model->submission_gets(array('vacancy_id' => $item->id), TRUE).' Pelamar</a></span>';
			$result['accepted'] 			= '<span class="badge badge-pill badge-success" style="padding:10px 10px 10px 10px;margin-bottom:5px;width:100%"><a style="color:white" href="'.base_url("vacancy/employee/").$item->id.'">'.$this->applicant_model->approved_gets(array('vacancy_id' => $item->id), TRUE).' Pelamar</a></span>';
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("vacancy/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-secondary btn-block" href="'.base_url("vacancy/question/".$item->id).'">Pertanyaan</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("vacancy/form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("vacancy/delete/".$item->id).'">Hapus</a>
				<a onclick="preview_chart(this)" class="btn-sm btn-warning btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("vacancy/preview_chart/".$item->id).'">Chart</a>';
			if($daftarTes!=""){
				$result['action'] = $result['action'].'<a onclick="rekap_hasil(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-id="'.$item->id.'">Hasil</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}

	public function question_delete($vacancy_id = FALSE, $question_id = false)
	{
		$this->load->model('vacancy_model');
		if ($question_id)
		{
			$data =  $this->vacancy_model->question_get(array('id' => $question_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $question_id, 'is_active' => 0);
				$result = $this->vacancy_model->question_save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
				redirect(base_url('vacancy/question/'.$vacancy_id));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
				redirect(base_url('vacancy/question/'.$vacancy_id));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('vacancy/question/'.$vacancy_id));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('vacancy/question/'.$vacancy_id));
		}
	}

	public function question_form($vacancy_id = FALSE, $question_id = FALSE)
	{
		$this->load->model(array('vacancy_model'));

		$data['id'] 		= $question_id;
		$data['vacancy_id']	= $vacancy_id;
		$data['question']	= '';

		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['vacancy_id']	= $this->input->post('vacancy_id');
			$data['question']	= $this->input->post('question');
			
			$this->form_validation->set_rules('vacancy_id', '', 'required');
			$this->form_validation->set_rules('question', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
				redirect(base_url('vacancy/question/'.$data['vacancy_id']));
				die();
			}else{
				$save_id	= $this->vacancy_model->question_save($data);
				if (!$save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
					redirect(base_url('vacancy/question/'.$data['vacancy_id']));
					die();
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));		
			redirect(base_url('vacancy/question/'.$data['vacancy_id']));
		}

		if ($question_id)
		{
			$data = (array) $this->vacancy_model->question_get(array('id' => $question_id, 'columns' => 'A.id, A.vacancy_id, A.question'));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('vacancy/question/'.$vacancy_id));
			}
		}

		$data['vacancy'] 		= $this->vacancy_model->get(array('columns' => 'A.id, A.title', 'id' => $vacancy_id));
		$data['_TITLE_'] 		= 'Form Pertanyaan';
		$data['_PAGE_'] 		= 'vacancy/question_form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'vacancy';
		return $this->view($data);
	}

	public function question($vacancy_id = FALSE){
		$data['_TITLE_'] 		= 'Daftar Pertanyaan';
		$data['_PAGE_'] 		= 'vacancy/question';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'vacancy';
		if(!$vacancy_id){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('vacancy/list'));
		}

		$this->load->model(array('vacancy_model'));
		$data['vacancy'] = $this->vacancy_model->get(array('columns' => 'A.id, A.title', 'id' => $vacancy_id));
		$this->view($data);

	}

	public function question_ajax($vacancy_id = FALSE){
		$this->load->model(array('vacancy_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.question";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['vacancy_id']	= $vacancy_id;
		$params['question']		= $_POST['columns'][0]['search']['value'];
		
		$list 	= $this->vacancy_model->question_gets($params);
		$total 	= $this->vacancy_model->question_gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['question'] 	= $item->question;
			$result['action'] 		=
				'<a class="btn-sm btn-success btn-block" href="'.base_url("vacancy/question_form/".$vacancy_id.'/'.$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("vacancy/question_delete/".$vacancy_id.'/'.$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}

	public function submission($vacancy_id = FALSE){
		$data['_TITLE_'] 		= 'Pelamar Diajukan';
		$data['_PAGE_'] 		= 'vacancy/submission';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'vacancy';
		$data['vacancy_id'] 	= $vacancy_id;
		$this->load->model(array('vacancy_model'));
		$data['vacancy']		= $this->vacancy_model->get(array('id' => $vacancy_id, 'columns' => 'A.title'));
		$this->view($data);
	}

	public function submission_ajax($vacancy_id = FALSE){
		$this->load->model(array('vacancy_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, DATE_FORMAT(A.submitted_at, '%d/%m/%Y') AS submitted_at, A.registration_number, A.full_name, A.id_card, FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25) AS age, A.education_level, A.education_majors, B.name AS city_domisili, C.name AS site_name, E.name AS position"; 
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status_approval']	= 2;
		
		// $params['submitted_at']		= $_POST['columns'][0]['search']['value'];
		$params['full_name']		= $_POST['columns'][1]['search']['value'];
		$params['id_card']			= $_POST['columns'][2]['search']['value'];
		$params['age']				= $_POST['columns'][3]['search']['value'];
		$params['education_level']	= $_POST['columns'][4]['search']['value'];
		$params['education_majors']	= $_POST['columns'][5]['search']['value'];
		$params['city_domisili']	= $_POST['columns'][6]['search']['value'];
		$params['site_name']		= $_POST['columns'][7]['search']['value'];
		$params['position']			= $_POST['columns'][8]['search']['value'];
		$params['vacancy_id'] 		= $vacancy_id;

		$list 	= $this->vacancy_model->employee_gets($params);
		$total 	= $this->vacancy_model->employee_gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 					= $i;
			$result['submitted_at']			= $item->submitted_at;
			$result['full_name']			= $item->full_name;
			$result['id_card']				= $item->id_card;
			$result['age']					= $item->age;
			$result['education_level']		= $item->education_level;
			$result['education_majors']		= $item->education_majors;
			$result['city_domisili']		= $item->city_domisili;
			$result['site_name']			= $item->site_name;
			$result['position']				= $item->position;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("candidate/preview/".$item->id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("candidate/curriculum_vitae/".$item->id).'">CV</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("approval/approval_history/".$item->id).'">Riwayat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function employee($vacancy_id = FALSE){
		$data['_TITLE_'] 		= 'Pelamar Diterima';
		$data['_PAGE_'] 		= 'vacancy/employee';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'vacancy';
		$data['vacancy_id'] 	= $vacancy_id;
		$this->load->model(array('vacancy_model'));
		$data['vacancy']		= $this->vacancy_model->get(array('id' => $vacancy_id, 'columns' => 'A.title'));
		$this->view($data);
	}

	public function employee_ajax($vacancy_id = FALSE){
		$this->load->model(array('vacancy_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, DATE_FORMAT(A.approved_at, '%d/%m/%Y') AS approved_at, A.registration_number, A.full_name, A.id_card, FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25) AS age, A.education_level, A.education_majors, B.name AS city_domisili, C.name AS site_name, E.name AS position"; 
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status_approval']	= 3;
		
		// $params['approved_at']		= $_POST['columns'][0]['search']['value'];
		$params['full_name']		= $_POST['columns'][1]['search']['value'];
		$params['id_card']			= $_POST['columns'][2]['search']['value'];
		$params['age']				= $_POST['columns'][3]['search']['value'];
		$params['education_level']	= $_POST['columns'][4]['search']['value'];
		$params['education_majors']	= $_POST['columns'][5]['search']['value'];
		$params['city_domisili']	= $_POST['columns'][6]['search']['value'];
		$params['site_name']		= $_POST['columns'][7]['search']['value'];
		$params['position']			= $_POST['columns'][8]['search']['value'];
		$params['vacancy_id'] 		= $vacancy_id;

		$list 	= $this->vacancy_model->employee_gets($params);
		$total 	= $this->vacancy_model->employee_gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 					= $i;
			$result['approved_at']			= $item->approved_at;
			$result['full_name']			= $item->full_name;
			$result['id_card']				= $item->id_card;
			$result['age']					= $item->age;
			$result['education_level']		= $item->education_level;
			$result['education_majors']		= $item->education_majors;
			$result['city_domisili']		= $item->city_domisili;
			$result['site_name']			= $item->site_name;
			$result['position']				= $item->position;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("candidate/preview/".$item->id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("candidate/curriculum_vitae/".$item->id).'">CV</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("approval/approval_history/".$item->id).'">Riwayat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function applicant($vacancy_id = FALSE){
		$this->load->model(array('site_model', 'company_model', 'position_model', 'branch_model', 'vacancy_model'));
		$data['_TITLE_'] 		= 'Daftar Pelamar';
		$data['_PAGE_'] 		= 'vacancy/applicant';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'vacancy';
		$data['vacancy_id'] 	= $vacancy_id;
		$data['vacancy']		= $this->vacancy_model->get(array('id' => $vacancy_id, 'columns' => 'A.title, A.site_id, A.company_id, A.position_id'));
		
		$list_site 		= $this->site_model->gets(array('columns' => 'A.id, A.name', 'branch_id' => $this->session->userdata('branch'), 'orderby' => 'A.id', 'order' => 'ASC'));
		
		$object 		= new stdClass();
		$object->id 	= 2;
		$object->name 	= 'INDIRECT';
		$indirect 		=  array($object);

		$data['list_site'] = array_merge($indirect, $list_site);
		$data['list_company'] 	= $this->company_model->gets(array('columns' => 'A.id, A.name, A.code', 'orderby' => 'A.name', 'order' => 'ASC'));
		$data['list_position'] 	= $this->position_model->gets(array('columns' => 'A.id, A.name'));
		$data['list_branch'] 	= $this->branch_model->gets(array('columns' => 'A.id, A.name'));
		$this->view($data);
	}

	public function applicant_ajax($vacancy_id = FALSE, $site_id = FALSE){

		$this->load->model(array('vacancy_model','psikotes_model','deret_angka_model','hitung_cepat_model','ketelitian_model','kraepelin_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, DATE_FORMAT(A.created_at, '%d/%m/%Y') AS date_registration, A.full_name, A.id_card, FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25) AS age, A.gender, A.education_level, A.education_majors, A.phone_number, B.name AS city_domisili, A.status_completeness, A.status_approval,D.is_tes_psikotes,D.is_tes_deret_angka,D.is_tes_hitung_cepat,D.is_ketelitian,D.is_kraepelin, A.followup_status, A.followup_note ";

		$params['orderby'] 			= $_POST['columns'][$column_index]['data'];
		$params['order']			= $_POST['order'][0]['dir'];
		$params['limit']			= $_POST['length'];
		$params['page']				=  $_POST['start'];

		$params['created_at']		= $_POST['columns'][1]['search']['value'];
		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['id_card']			= $_POST['columns'][3]['search']['value'];
		$params['age']				= $_POST['columns'][4]['search']['value'];
		$params['gender']			= $_POST['columns'][5]['search']['value'];
		$params['education_level']	= $_POST['columns'][6]['search']['value'];
		$params['education_majors']	= $_POST['columns'][7]['search']['value'];
		$params['phone_number']		= $_POST['columns'][8]['search']['value'];
		$params['city_domisili']	= $_POST['columns'][9]['search']['value'];
		$params['vacancy_id'] 		= $vacancy_id;

		$list 	= $this->vacancy_model->applicant_gets($params);
		$total 	= $this->vacancy_model->applicant_gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{	
			$result['id'] 				= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			if($item->status_approval > 0){
				$result['id'] = "";
			}
			$result['no'] 					= $i;
			$result['date_registration']	= $item->date_registration;
			$result['full_name']			= $item->full_name;
			$result['id_card']				= $item->id_card;
			$result['age']					= $item->age;
			$result['gender']				= $item->gender;
			$result['education_level']		= $item->education_level;
			$result['education_majors']		= $item->education_majors;
			$result['city_domisili']		= $item->city_domisili;
			$result['followup_status']		= $item->followup_status;
			$result['followup_description']	= $item->followup_note;
			if($item->status_completeness == 0){
				$result['status_completeness'] = '<span class="text-danger">Belum Lengkap</span>';
			}else{
				$result['status_completeness'] = '<span class="text-success">Lengkap</span>';
			}

			if($item->followup_status === 'Blokir'){
				$result['id'] = '';
			}

			// $result['followup_status']		= "";
			// $result['followup_description']	= "";
			// $followup = $this->vacancy_model->last_status(array('employee_id' => $item->id, 'site_id' => $site_id, 'columns' => 'A.status_approval, A.note'));
			// if($followup){	
			// 	$result['followup_status']		= $followup->status_approval;
			// 	$result['followup_description']	= $followup->note;
			// }

			$list_number 					= $item->phone_number;
			$phone_number					= "";
			if($list_number != ""){
				$phone_number = explode ("/", $list_number);
				$phone_number = preg_replace("/[^0-9]/", "", $phone_number[0]);
				$start = substr($phone_number, 0, 1 );
				if($start == '0'){
					$phone_number = '62'.substr($phone_number, 1,strlen($phone_number));
				}else if($start == '+'){
					$phone_number = substr($phone_number, 1,strlen($phone_number));
				}
				$phone_number = '<a href="http://wa.me/'.$phone_number.'" target="_blank">'.$item->phone_number.'</a>';
			}
			$result['phone_number']			= $phone_number;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("candidate/preview/".$item->id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("candidate/curriculum_vitae/".$item->id).'">CV</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("approval/approval_history/".$item->id).'">Riwayat</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("candidate/delete/".$item->id).'">Hapus</a>';
			
			//Tambahan Tes Online
			$iconTes = "";
			if($item->is_tes_psikotes==1 ||$item->is_tes_deret_angka==1 ||$item->is_tes_hitung_cepat==1 ||$item->is_ketelitian==1||$item->is_kraepelin==1){
				$psikotesData 		= $this->psikotes_model->count_psikotes_vacancy($item->id,$vacancy_id);
				$deretAngkaData 	= $this->deret_angka_model->count_deret_angka_vacancy($item->id,$vacancy_id);
				$deretAngkaData 	= $this->deret_angka_model->count_deret_angka_vacancy($item->id,$vacancy_id);
				$hitungCepatData 	= $this->hitung_cepat_model->count_hitung_cepat_vacancy($item->id,$vacancy_id);
				$ketelitianData 	= $this->ketelitian_model->count_ketelitian_vacancy($item->id,$vacancy_id);
				$kraepelinData 		= $this->kraepelin_model->count_kraepelin_vacancy($item->id,$vacancy_id);

				if($item->is_tes_psikotes==1){
					if(!empty($psikotesData)){
						$iconTes = $iconTes.'<a onclick="preview(this)" class="btn-sm btn-success btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("history_tes/result?tes=psikotes&doc_id=".$psikotesData[0]->id).'"><i class="fas fa-file-pdf"></i> &nbsp;DISC</a>';
					}else{
						$iconTes = $iconTes.'<a href="#" class="btn-sm btn-danger btn-action btn-block" style="cursor:pointer;">X DISC</a>';
					}
				}

				if($item->is_tes_deret_angka==1){
					if(!empty($deretAngkaData)){
						$iconTes = $iconTes.'<a onclick="previewHasil(this)" class="btn-sm btn-success btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("history_tes/result?tes=deret_angka&doc_id=".$deretAngkaData[0]->id).'"><i class="fas fa-file-pdf"></i> &nbsp;Deret Angka</a>';
					}else{
						$iconTes = $iconTes.'<a href="#" class="btn-sm btn-danger btn-action btn-block" style="cursor:pointer;">X Deret Angka</a>';
					};
				}
				if($item->is_tes_hitung_cepat==1){
					if(!empty($hitungCepatData)){
						$iconTes = $iconTes.'<a onclick="previewHasil(this)" class="btn-sm btn-success btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("history_tes/result?tes=hitung_cepat&doc_id=".$hitungCepatData[0]->id).'"><i class="fas fa-file-pdf"></i> &nbsp;Hitung Cepat</a>';
					}else{
						$iconTes = $iconTes.'<a href="#" class="btn-sm btn-danger btn-action btn-block" style="cursor:pointer;">X Hitung Cepat</a>';
					}
				}

				if($item->is_ketelitian==1){
					if(!empty($ketelitianData)){
						$iconTes = $iconTes.'<a onclick="previewHasil(this)" class="btn-sm btn-success btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("history_tes/result?tes=ketelitian&doc_id=".$ketelitianData[0]->id).'"><i class="fas fa-file-pdf"></i> &nbsp;Ketelitian</a>';
					}else{
						$iconTes = $iconTes.'<a href="#" class="btn-sm btn-danger btn-action btn-block" style="cursor:pointer;">X Ketelitian</a>';
					}
				}
				
				if($item->is_kraepelin==1){
					if(!empty($kraepelinData)){
						$iconTes = $iconTes.'<a onclick="previewHasil(this)" class="btn-sm btn-success btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("history_tes/result?tes=kraepelin&doc_id=".$kraepelinData[0]->id).'"><i class="fas fa-file-pdf"></i> &nbsp;Kraepelin</a>';
					}else{
						$iconTes = $iconTes.'<a href="#" class="btn-sm btn-danger btn-action btn-block" style="cursor:pointer;">X Kraepelin</a>';
					}
				}
			}


			$result['status_tes'] = $iconTes;
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function submit($vacancy_id = FALSE){

		$site_id 		= $this->input->post('site_id');
		$company_id 	= $this->input->post('company_id');
		$position_id	= $this->input->post('position_id');
		$placement_date	= $this->input->post('placement_date');

		foreach ($this->input->post('id') as $candidate_id)
		{
			$this->load->model(array('candidate_model', 'approval_model'));
			$data = (array) $this->candidate_model->get(array('id' => $candidate_id, 'columns' => 'A.id, A.status_nonjob'));
			if (empty($data)){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('vacancy/applicant/'.$vacancy_id));
			}else{
				$approval_status  = 1;
				$status_approval  = 'Pengajuan ke Client';
				if($site_id == '2' || $data['status_nonjob'] == 1){
					$approval_status = 2;
					$status_approval = 'Pengajuan ke Personalia';
				}

				$insert = array('id' => $candidate_id, 'status_approval' => $approval_status, 'position_id' => $position_id, 'site_id' => $site_id, 'company_id' => $company_id, 'submitted_at' => date('Y-m-d H:i:s'), 'submitted_by' => $this->session->userdata('user_id'), 'placement_date' => $placement_date);
				$result = $this->candidate_model->save($insert);

				$approval = array('id' => '', 'employee_id' => $data['id'] , 'site_id' => $site_id, 'company_id' => $company_id, 'position_id' => $position_id, 'status_approval' => 'Pengajuan', 'note' => $status_approval);
				$this->approval_model->save($approval);

				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pengajuan gagal.','danger'));
				redirect(base_url('vacancy/applicant/'.$vacancy_id));
				}
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Pegajuan Sukses.','success'));
		redirect(base_url('vacancy/applicant/'.$vacancy_id));
	}

	public function followup($vacancy_id = FALSE, $site_id = FALSE, $position_id = FALSE, $company_id = FALSE){

		$note	= $this->input->post('note_followup');
		$status	= $this->input->post('status_followup');
		foreach ($this->input->post('id') as $candidate_id)
		{
			$this->load->model(array('candidate_model', 'approval_model'));

			$approval = array('id' => '', 'employee_id' => $candidate_id , 'site_id' => $site_id, 'company_id' => $company_id, 'position_id' => $position_id, 'status_approval' => $status, 'note' => $note);
			$result = $this->approval_model->save($approval);
			$this->candidate_model->save(array('id' => $candidate_id, 'followup_status' => $status, 'followup_note' => $note));
			if (!$result) {
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Update followup gagal.','danger'));
			redirect(base_url('vacancy/applicant/'.$vacancy_id));
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Update followup.','success'));
		redirect(base_url('vacancy/applicant/'.$vacancy_id));
	}

	public function applicant_export($vacancy_id = FALSE)
	{
		$this->load->model(array('vacancy_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$params = $this->input->get();
		$title = 'pelamar_lowongan';
		$files = glob(FCPATH."files/".$title."/*");

		$params['columns'] 		= "A.id, A.registration_number, A.full_name, A.id_card, A.date_birth, A.address_domisili, A.phone_number, A.email, A.religion, A.marital_status, A.gender, A.heigh, A.weigh, A.education_level, A.education_majors, B.name AS city_domisili";

		$params['vacancy_id'] 		= $vacancy_id;
		$list 	= $this->vacancy_model->applicant_gets($params);

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Nomor Pendaftaran");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("C".$i, "NIK (KTP)");
		$excel->getActiveSheet()->setCellValue("D".$i, "Tanggal Lahir");
		$excel->getActiveSheet()->setCellValue("E".$i, "Kota Domisili");
		$excel->getActiveSheet()->setCellValue("F".$i, "Alamat Domisili");
		$excel->getActiveSheet()->setCellValue("G".$i, "No. Telephon/HP");
		$excel->getActiveSheet()->setCellValue("H".$i, "Email");
		$excel->getActiveSheet()->setCellValue("I".$i, "Agama");
		$excel->getActiveSheet()->setCellValue("J".$i, "Status Pernikahan");
		$excel->getActiveSheet()->setCellValue("K".$i, "Jenis Kelamin");
		$excel->getActiveSheet()->setCellValue("L".$i, "Tinggi");
		$excel->getActiveSheet()->setCellValue("M".$i, "Berat");
		$excel->getActiveSheet()->setCellValue("N".$i, "Pendidikan Terakhir");
		$excel->getActiveSheet()->setCellValue("O".$i, "Jurusan");

		$i=2;
		foreach ($list as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->registration_number);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->id_card);
			$date = new DateTime($item->date_birth);
			$excel->getActiveSheet()->setCellValue("D".$i, PHPExcel_Shared_Date::PHPToExcel($date));
			$excel->getActiveSheet()->getStyle("D".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			$excel->getActiveSheet()->setCellValue("E".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_domisili));
			$excel->getActiveSheet()->setCellValue("F".$i, $item->address_domisili);
			$excel->getActiveSheet()->setCellValueExplicit("G".$i, $item->phone_number);
			$excel->getActiveSheet()->setCellValue("H".$i, $item->email);
			$excel->getActiveSheet()->setCellValue("I".$i, $item->religion);
			$excel->getActiveSheet()->setCellValue("J".$i, $item->marital_status);
			$excel->getActiveSheet()->setCellValue("K".$i, $item->gender);
			$excel->getActiveSheet()->setCellValue("L".$i, $item->heigh);
			$excel->getActiveSheet()->setCellValue("M".$i, $item->weigh);
			$excel->getActiveSheet()->setCellValue("N".$i, $item->education_level);
			$excel->getActiveSheet()->setCellValue("O".$i, $item->education_majors);

			$i++;
		}
		$excel->getActiveSheet()->setTitle($title);
		$excel->setActiveSheetIndex(0);
		$sheet = $excel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells( true );
		foreach( $cellIterator as $cell ) {
			$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		}

		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function send($vacancy_id = FALSE){
		if($this->input->post()){
			$this->load->model(array('wappin_model'));
			$config_id = 1;
			$wappin 		= $this->wappin_model->get(array('id' => $config_id, 'columns' => 'A.access_token, A.base_url'));
			if($wappin){
				$message['token'] 	= "Authorization: Bearer ".$wappin->access_token;
				$message['base_url']= $wappin->base_url;
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Melakukan Koneksi API.','danger'));
				redirect(base_url('vacancy/applicant/'.$vacancy_id));
				die();
			}

			if($_FILES['message-image']['name'] != ""){

				$image_path = FCPATH.'files/whatsapp/';
				$config['upload_path'] 	= $image_path;
				$config['overwrite']  	= TRUE;
				$config['allowed_types']= 'jpg|jpeg|png';
				$config['file_name'] 	= $this->session->userdata('user_id');

				$this->load->library('upload', $config, 'image');
				if (!$this->image->do_upload('message-image')) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> '.$this->image->display_errors().'.','danger'));
					redirect(base_url('vacancy/applicant/'.$vacancy_id));
					die();
				}

				$message['image']		= base_url('files/whatsapp/'.$this->image->data()['file_name']);
				$message['message']		= $this->input->post('message-personal');
			}

			$send_success = 0;
			$send_failed = 0;

			$this->load->model(array('employee_model', 'approval_model', 'candidate_model'));
			$id 		= $this->input->post('id');
			foreach ($id as $employee_id) {
				$employee 	= $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.phone_number, A.full_name, A.followup_status, A.followup_note'));
				if($employee){
					if($employee->phone_number != ''){
						$phone_number = explode ("/", $employee->phone_number);
						$employee->phone_number = preg_replace("/[^0-9]/", "", $phone_number[0]);
						$start = substr($employee->phone_number, 0, 1 );
						if($start == '0'){
							$employee->phone_number = '62'.substr($employee->phone_number, 1,strlen($employee->phone_number));
						}else if($start == '+'){
							$employee->phone_number = substr($employee->phone_number, 1,strlen($employee->phone_number));
						}
						$message['phone_number'] = $employee->phone_number;
						$message['name'] = $employee->full_name;
						$send = $this->send_media($message);
						if($send){
							$send_success = $send_success+1;
							if($employee->followup_status == '' 
								&& $employee->followup_note == ''){

								$approval = array('id' => '', 'employee_id' => $employee_id , 'site_id' => 0, 'company_id' => 0, 'position_id' => 0, 'status_approval' => 'Hubungi', 'note' => 'Dihubungi Melalui Brodcast whatsapp');
								$result = $this->approval_model->save($approval);
								$this->candidate_model->save(array('id' => $employee_id, 'followup_status' => 'Hubungi', 'followup_note' => 'Dihubungi Melalui Brodcast whatsapp'));
								}
						}else{
							$send_failed = $send_failed+1;
						}
					}else{
						$send_failed = $send_failed+1;
					}
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> '.$send_success.' Pesan berhasil dikirim, '.$send_failed.' Pesan Gagal Dikirim','success'));
				redirect(base_url('vacancy/applicant/'.$vacancy_id));
		}else{
			redirect(base_url());
		}
	}

	public function preview_chart($vacancy_id=FALSE)
	{
		$this->load->model('vacancy_model');
		$data['_TITLE_'] 		= 'Preview Lowongan Pekerjaan';
		$data['_PAGE_']	 		= 'vacancy/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'vacancy';

		$data['id'] = $vacancy_id;
		if (!$vacancy_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('vacancy/list'));
		}
		$data['vacancy_id'] = $vacancy_id;
		$this->load->view('vacancy/preview_chart', $data);
	}

	public function chart_applicant($vacancy_id=FALSE){

		$this->load->model(array('vacancy_model', 'applicant_model'));
		$labels 		= array();
		$needs 		= array();
		$candidate 		= array();
		$submision 		= array();
		$accept 		= array();
		
		$vacancy = $this->vacancy_model->get(array('id' => $vacancy_id, 'columns' => 'A.start_date, A.end_date, A.needs'));
		$start_time 	= strtotime($vacancy->start_date);
		$end_time 	= strtotime($vacancy->end_date);

		for ( $i = $start_time; $i <= $end_time; $i = $i + 86400 ) {
			$c_date = date( 'Y-m-d', $i);
			$c_candidate = $this->applicant_model->gets(array('vacancy_id' => $vacancy_id, 'created_at' => $c_date), TRUE);
			$c_accept = $this->applicant_model->approved_gets(array('vacancy_id' => $vacancy_id, 'approved_at' => $c_date));
			$c_submision = $this->applicant_model->approved_gets(array('vacancy_id' => $vacancy_id, 'submitted_at' => $c_date), TRUE);
			array_push($labels, date('d/m/y', $i));
			array_push($candidate, $c_candidate);
			array_push($submision, $c_submision);
			array_push($accept, $c_accept);
			array_push($needs, $vacancy->needs);
		}
		$result = array('labels' => $labels, 'candidate' => $candidate, 'submision' => $submision, 'accept' => $accept, 'needs' => $needs);
		echo json_encode($result);
	}

	private function send_media($data = array()){
		$url 		= $data['base_url'].'/v1/messages';
		$request 	= array(
			'to' => $data['phone_number'],
			'type'	=> 'template',
			'template' => array(
				'name'	=> $this->config->item('global_template_name'),
				'namespace' => $this->config->item('global_template_namespace'),
				'language'	=> array(
					'policy' => 'deterministic',
					'code'	=> 'id'
				),
				'components' => array(
					array(
						'type'		=> 'header',
						'parameters'=> array(
							array(
								'type' => 'image',
								'image'	=> array(
									'link' => $data['image']
								)
							)
						)
					),
					array(
						'type' => 'body',
						'parameters'=> array(
							array(
								'type' => 'text',
								'text'	=> $data['name']
							),
							array(
								'type' => 'text',
								'text'	=> $data['message']
							)
						)
					)
				)
			),
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $data['token']));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
		$response = curl_exec($ch);
		$result = json_decode($response, true);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if($httpcode == 200){
			return true;
		}else{
			return false;
		}
	}
}
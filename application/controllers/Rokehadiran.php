<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rokehadiran extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
		if($this->session->userdata('site') == ''){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan pilih site bisnis.','danger'));
			redirect(base_url());	
		}
	}

	public function list($date = false){

		if(!$date){
			$date = date('d-m-Y');
		}
		$this->load->model(array('kehadiran_model', 'site_model'));
		
		$data['_TITLE_'] 		= 'Rekap Kehadiran';
		$data['_PAGE_'] 		= 'rokehadiran/list';
		$data['_MENU_PARENT_'] 	= 'roemployee';
		$data['_MENU_'] 		= 'rokehadiran';
		$data['last_period'] 	= '';
		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address'));
		$last_period 			= $this->kehadiran_model->gets(array('site_id' => $this->session->userdata('site'), 'columns' => 'DATE_FORMAT(MAX(A.periode), "%d-%m-%Y") AS periode'));
		if($last_period){
			$data['last_period'] = $last_period[0]->periode;
		}
		
		$data['date'] = $date;
		
		$this->view($data);
	}

	public function list_ajax($date = false){
		
		if(!$date){
			$date = date('d-m-Y');
		}

		$this->load->model(array('kehadiran_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.employee_id, B.id_card, B.full_name, A.hari_kerja, A.masuk_kerja, A.shift_malam, A.hari_libur, A.izin, A.cuti, A.sakit, A.alpha, A.non_aktif, A.telat, A.lembur, A.uang_lembur, A.uang_makan";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		// $params['pin_finger']		= $_POST['columns'][1]['search']['value'];
		$params['id_card']	= $_POST['columns'][0]['search']['value'];
		$params['full_name']		= $_POST['columns'][1]['search']['value'];
		
		$params['site_id']		= $this->session->userdata('site');
		$params['periode']		= substr($date, 6, 4).'-'.substr($date, 3, 2).'-'.substr($date, 0, 2);

		$list 	= $this->kehadiran_model->gets($params);
		$total 	= $this->kehadiran_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{			
			$result['no'] 				= $i;
			$result['id_card']			= $item->id_card;
			$result['full_name']		= $item->full_name;
			$result['hari_kerja']		= $item->hari_kerja;
			$result['masuk_kerja']		= $item->masuk_kerja;
			$result['shift_malam']		= $item->shift_malam;
			$result['hari_libur']		= $item->hari_libur;
			$result['izin']				= $item->izin;
			$result['cuti']				= $item->cuti;
			$result['sakit']			= $item->sakit;
			$result['alpha']			= $item->alpha;
			$result['non_aktif']		= $item->non_aktif;
			$result['telat']			= $item->telat;
			$result['lembur']			= $item->lembur;
			$result['uang_lembur']		= rupiah_round($item->uang_lembur);
			$result['uang_makan']		= rupiah_round($item->uang_makan);
			$result['action'] 			= '';
				// '<a class="btn-sm btn-success btn-block" href="'.base_url("roattendance/employee/".$item->employee_id).'">Detail</i></a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function export($date = false){	
		if(!$date){
			$date = date('m-Y');
		}

		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = 'rekap_kehadiran';
		$files = glob(FCPATH."files/".$title."/*");


		$this->load->model(array('kehadiran_model'));

		$params['columns'] 		= "A.id, A.employee_id, B.id_card, B.full_name, A.hari_kerja, A.masuk_kerja, A.shift_malam, A.hari_libur, A.izin, A.cuti, A.sakit, A.alpha, A.non_aktif, A.telat, A.lembur, A.uang_lembur, A.uang_makan";
		$params['orderby'] 		= 'B.full_name';
		$params['order']		= 'ASC';
			
		$params['site_id']		= $this->session->userdata('site');
		$params['periode']		= substr($date, 6, 4).'-'.substr($date, 3, 2).'-'.substr($date, 0, 2);
		$list 					= $this->kehadiran_model->gets($params);

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "NIK KTP");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nama Karyawan");
		$excel->getActiveSheet()->setCellValue("C".$i, "Hari Kerja");
		$excel->getActiveSheet()->setCellValue("D".$i, "Masuk Kerja");
		$excel->getActiveSheet()->setCellValue("E".$i, "Shift Malam");
		$excel->getActiveSheet()->setCellValue("F".$i, "Hari Libur");
		$excel->getActiveSheet()->setCellValue("G".$i, "Izin");
		$excel->getActiveSheet()->setCellValue("H".$i, "Cuti");
		$excel->getActiveSheet()->setCellValue("I".$i, "Sakit");
		$excel->getActiveSheet()->setCellValue("J".$i, "Alpha");
		$excel->getActiveSheet()->setCellValue("K".$i, "Non Aktif");
		$excel->getActiveSheet()->setCellValue("L".$i, "Telat");
		$excel->getActiveSheet()->setCellValue("M".$i, "Lembur");
		$excel->getActiveSheet()->setCellValue("N".$i, "Uang Makan");
		$excel->getActiveSheet()->setCellValue("O".$i, "Uang Lembur");

		$i=2;
		foreach ($list as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValue("C".$i, $item->hari_kerja);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->masuk_kerja);
			$excel->getActiveSheet()->setCellValue("E".$i, $item->shift_malam);
			$excel->getActiveSheet()->setCellValue("F".$i, $item->hari_libur);
			$excel->getActiveSheet()->setCellValue("G".$i, $item->izin);
			$excel->getActiveSheet()->setCellValue("H".$i, $item->cuti);
			$excel->getActiveSheet()->setCellValue("I".$i, $item->sakit);
			$excel->getActiveSheet()->setCellValue("J".$i, $item->alpha);
			$excel->getActiveSheet()->setCellValue("K".$i, $item->non_aktif);
			$excel->getActiveSheet()->setCellValue("L".$i, $item->telat);
			$excel->getActiveSheet()->setCellValue("M".$i, $item->lembur);
			$excel->getActiveSheet()->setCellValue("N".$i, $item->uang_makan);
			$excel->getActiveSheet()->setCellValue("O".$i, $item->uang_lembur);
			$i++;
		}
		$excel->getActiveSheet()->setTitle('Rekap Kehadiran');
		$sheet = $excel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells( true );
		foreach( $cellIterator as $cell ) {
			$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		}
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function import($date = false){
		if(!$_FILES['file']['name'] == ""){
			$this->load->library("Excel");
			$this->load->model(array('employee_model', 'kehadiran_model'));
			
			$date = $this->input->post('date');

			$params['periode']		= substr($date, 6, 4).'-'.substr($date, 3, 2).'-'.substr($date, 0, 2);
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			$message_warning= ""; 
			for ($row = 2; $row <= $rend; $row++) {

				$id_card 	= 	replace_null($sheet->getCell('A'.$row)->getValue());				
				$employee  	= $this->employee_model->get(array('id_card' => $id_card, 'columns' => 'A.id'));
				if(!$employee){
					$message_warning .= $id_card.', ';
					continue;
				}

				$params['id']	= '';
				$params['employee_id'] 	= $employee->id;
				
				$kehadiran  	= $this->kehadiran_model->get(array('employee_id' => $employee->id, 'periode' => $params['periode'], 'columns' => 'A.id'));
				if($kehadiran){
					$params['id']	= $kehadiran->id;
				}
				$params['hari_kerja'] 	= replace_null($sheet->getCell('C'.$row)->getValue());
				$params['masuk_kerja'] 	= replace_null($sheet->getCell('D'.$row)->getValue());
				$params['hari_libur'] 	= replace_null($sheet->getCell('E'.$row)->getValue());
				$params['shift_malam'] 	= replace_null($sheet->getCell('F'.$row)->getValue());
				$params['izin'] 		= replace_null($sheet->getCell('G'.$row)->getValue());
				$params['cuti'] 		= replace_null($sheet->getCell('H'.$row)->getValue());
				$params['sakit'] 		= replace_null($sheet->getCell('I'.$row)->getValue());
				$params['alpha'] 		= replace_null($sheet->getCell('J'.$row)->getValue());
				$params['non_aktif'] 	= replace_null($sheet->getCell('K'.$row)->getValue());
				$params['telat'] 		= replace_null($sheet->getCell('L'.$row)->getValue());
				$params['lembur'] 		= replace_null($sheet->getCell('M'.$row)->getValue());
				$params['uang_makan'] 	= replace_null($sheet->getCell('N'.$row)->getValue());
				$params['uang_lembur'] 	= replace_null($sheet->getCell('O'.$row)->getValue());
				 
				$this->kehadiran_model->save($params);
			}

			if($message_warning != ''){
				$this->session->set_flashdata('message', message_box('<strong>Peringatan!</strong>  NIK : '.$message_warning.', belum terdaftar pada site ini.','warning'));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
		}

		redirect(base_url('rokehadiran/list/'.$date));
	}


	// public function uang($date = false){
	// 	if(!$_FILES['file']['name'] == ""){
	// 		$this->load->library("Excel");
	// 		$this->load->model(array('employee_model', 'kehadiran_model'));
			
	// 		$excelreader 	= new PHPExcel_Reader_Excel2007();
	// 		$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
	// 		$sheet  		= $loadexcel->getActiveSheet(0);
			
	// 		$rend			= $sheet->getHighestRow();
	// 		$message_warning= ""; 
	// 		for ($row = 8; $row <= $rend; $row++) {
	// 			$id_card 	= 	replace_null($sheet->getCell('D'.$row)->getOldCalculatedValue());
				
	// 			if($id_card == ''){
	// 				continue;
	// 			}
	
	// 			$employee  	= $this->employee_model->get(array('id_card' => $id_card, 'columns' => 'A.id'));
	// 			if(!$employee){
	// 				$message_warning .= $id_card.', ';
	// 				continue;
	// 			}

	// 			$kehadiran = $this->kehadiran_model->get(array('employee_id' => $employee->id, 'periode' => '2023-08-15', 'columns' => 'A.id'));
	// 			if(!$kehadiran){
	// 				continue;
	// 			}
	// 			$uang_makan 	= 	replace_null($sheet->getCell('AM'.$row)->getOldCalculatedValue());
	// 			$uang_lembur 	= 	replace_null($sheet->getCell('N'.$row)->getOldCalculatedValue());
	// 			$id_card 		= 	replace_null($sheet->getCell('D'.$row)->getOldCalculatedValue());
				
	// 			print_prev($id_card.' - '.$employee->id." - ".$kehadiran->id." - ".$uang_makan." - ".$uang_lembur);				 
	// 			$this->kehadiran_model->save(array('id' => $kehadiran->id, 'uang_lembur' => $uang_lembur, 'uang_makan' => $uang_makan));
	// 		}

	// 		if($message_warning != ''){
	// 			$this->session->set_flashdata('message', message_box('<strong>Peringatan!</strong>  NIK : '.$message_warning.', belum terdaftar pada site ini.','warning'));
	// 		}else{
	// 			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
	// 		}
	// 	}else{
	// 		$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
	// 	}

	// 	redirect(base_url('rokehadiran/list/'.$date));
	// }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Roresign extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
		if($this->session->userdata('site') == ''){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan pilih site bisnis.','danger'));
			redirect(base_url());	
		}
	}

	public function submit(){
		foreach ($this->input->post('id') as $employee_id)
		{
			$this->load->model(array('employee_model', 'approval_model'));
			$data = (array) $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.site_id, A.company_id, A.position_id'));
			if (empty($data)){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('roresign/list'));
			}else{
				$insert = array('id' => $employee_id, 'status_approval' => 3);
				$result = $this->employee_model->save($insert);
				
				$approval = array('id' => '', 'employee_id' => $employee_id , 'site_id' => $data['site_id'], 'company_id' => $data['company_id'], 'position_id' => $data['position_id'], 'note' => 'Resign Dibatalkan','status_approval' => 'Resign Dibatalkan');
				$this->approval_model->save($approval);

				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pengajuan gagal.','danger'));
					redirect(base_url('roresign/list'));
				}
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Pegajuan Sukses.','success'));
		redirect(base_url('roresign/list'));
	}

	public function list(){
		$this->load->model(array('site_model'));
		$data['_TITLE_'] 		= 'Pengajuan Resign';
		$data['_PAGE_'] 		= 'roresign/list';
		$data['_MENU_PARENT_'] 	= 'roemployee';
		$data['_MENU_'] 		= 'roresign';
		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address'));
		$this->view($data);
	}

	public function list_ajax(){
		$this->load->model(array('roresign_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.employee_number, A.id_card, A.full_name, A.document_resign, A.resign_note, A.resign_burden, A.resign_type, DATE_FORMAT(A.resign_submit, "%d/%m/%Y") AS resign_submit';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']		= 1;
		
		$params['status_approval'] 		= 4;
		$params['site_id']				=  $this->session->userdata('site');
		
		$params['employee_number']		= $_POST['columns'][2]['search']['value'];
		$params['id_card']				= $_POST['columns'][3]['search']['value'];
		$params['full_name']			= $_POST['columns'][4]['search']['value'];
		$params['resign_submit']		= $_POST['columns'][5]['search']['value'];
		$params['resign_type']			= $_POST['columns'][6]['search']['value'];
		$params['resign_note']			= $_POST['columns'][7]['search']['value'];
		$params['resign_burden']		= $_POST['columns'][8]['search']['value'];

		$list 	= $this->roresign_model->gets($params);
		$total 	= $this->roresign_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$document_resign				= '';
			if(isset($item->document_resign)){
				$document_resign = '<a href="'.$item->document_resign.'" target="_blank">Dokumen</a>';
			}

			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['employee_number']		= $item->employee_number;
			$result['id_card']				= $item->id_card;
			$result['full_name']			= $item->full_name;
			$result['resign_submit']		= $item->resign_submit;
			$result['resign_type']			= $item->resign_type;
			$result['resign_note']			= $item->resign_note;
			$result['resign_burden']		= $item->resign_burden;
			$result['document_resign']		= $document_resign;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("roemployee/preview/".$item->id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("approval/approval_history/".$item->id).'">Riwayat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}

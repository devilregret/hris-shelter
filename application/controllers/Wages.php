<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wages extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('sales'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,10))){
			redirect(base_url());
		}
	}

	public function preview($city_id=FALSE)
	{
		$this->load->model('city_model');
		$data['_TITLE_'] 		= 'Preview Upah Mimimum Kota';
		$data['_PAGE_']	 		= 'wages/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'wages';

		$data['id'] = $city_id;

		
		if (!$city_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('wages/list'));
		}

		$data['preview'] = $this->city_model->preview(array('id' => $city_id));
		$this->load->view('wages/preview', $data);
	}

	public function form($city_id = FALSE)
	{
		$this->load->model(array('city_model'));

		$data['id'] 			= '';
		$data['name']			= '';
		$data['address'] 		= '';
		$data['province_id']	= '';
		$data['minimum_salary']	= '';
		if($this->input->post()){
			$data['id'] 			= $this->input->post('id');
			$data['minimum_salary']	= str_replace(".", "", $this->input->post('minimum_salary'));
			
			$this->form_validation->set_rules('id', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$insert = array('id' => $data['id'], 'minimum_salary' => $data['minimum_salary']);
				$save_id	 	= $this->city_model->save($insert);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('wages/list'));
		}

		if ($city_id)
		{
			$data = (array) $this->city_model->get(array('id' => $city_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('wages/list'));
			}
		}

		$data['_TITLE_'] 		= 'UMK';
		$data['_PAGE_'] 		= 'wages/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'wages';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'UMK';
		$data['_PAGE_'] 		= 'wages/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'wages';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('city_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.name, A.minimum_salary, B.name AS province_name';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['name']				= $_POST['columns'][1]['search']['value'];
		$params['province_name']	= $_POST['columns'][2]['search']['value'];
		$params['minimum_salary']	= $_POST['columns'][3]['search']['value'];
		
		$list 	= $this->city_model->gets($params);
		$total 	= $this->city_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 				= $i;
			$result['name'] 			= $item->name;
			$result['province_name']	= $item->province_name;
			$result['minimum_salary']	= nominal_rupiah($item->minimum_salary);
			$result['action'] 			=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("wages/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("wages/form/".$item->id).'">Ubah</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function export()
	{

		$this->load->model(array('city_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = 'minimum_salary';
		$files = glob(FCPATH."files/".$title."/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Nama Kota");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nama Provinsi");
		$excel->getActiveSheet()->setCellValue("C".$i, "UMK");

		$params 			= $this->input->get();
		$params['columns'] 	= 'A.id, A.name, A.minimum_salary, B.name AS province_name';
		$list_wages 		= $this->city_model->gets($params);
		
		$i=2;
		foreach ($list_wages as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->name);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->province_name);
			$excel->getActiveSheet()->setCellValue("C".$i, $item->minimum_salary);
			$i++;
		}
		$excel->getActiveSheet()->setTitle('umk');
				$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function import(){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('city_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {
				$data['id'] 			= '';
				$data['name']			= replace_null($sheet->getCell('A'.$row)->getValue());
				$wages = $this->city_model->get(array('name' => $data['name'], 'columns' => 'A.id'));
				if($wages){
					$data['id'] = $wages->id;
					$data['minimum_salary'] = replace_null($sheet->getCell('C'.$row)->getValue());
					$save_id = $this->city_model->save($data);
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			redirect(base_url('wages/list'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
			redirect(base_url('wages/list'));
		}
	}
}
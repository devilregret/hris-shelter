<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Accurate extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function authorize(){
		echo '<!DOCTYPE html>
		<html>
		<body>
		<h2>Authorize</h2>
		<form action="https://account.accurate.id/oauth/authorize" method="post" name="form">
		<label for="fname">Client Id:</label><br>
		<input type="text" name="client_id" value="e44c5a4b-6c9b-4e86-ad8e-33cc2346d794" style="width:350px;" /><br>
		<label for="lname">Response Type:</label><br>
		<input type="text" name="response_type" value="code" style="width:350px;" /><br>
		<label for="lname">Redirect URI:</label><br>
		<input type="text" name="redirect_uri" value="https://hris.shelterapp2.co.id/accurate/callback" style="width:350px;" /><br>
		<label for="lname">Scope:</label><br>
		<input type="text" name="scope" value="item_view customer_view project_view branch_view" style="width:350px;" /><br>
		<button type="submit">Submit</button>
		</form> 
		</body>
		<script type="text/javascript">
		window.onload=function(){
			document.forms["form"].submit();
		}
		</script>
		</html>';
	}

	public function callback()
	{
		$this->load->model(array('accurate_model'));
		$accurate 		= $this->accurate_model->get(array('id' => 1, 'columns' => 'A.id, A.client_id, A.client_secret, A.callback'));


		$clientId 		= $accurate->client_id; //'e44c5a4b-6c9b-4e86-ad8e-33cc2346d794';
		$clientSecret 	= $accurate->client_secret; //'292f1a4e1b4538d82555b6927ddc5413';
		
		$accessToken 	= base64_encode($clientId.':'.$clientSecret);
		$authorization 	= "Authorization: Basic ".$accessToken;

		$params = array("code" => $this->input->get('code'), "grant_type" => "authorization_code", "redirect_uri" => $accurate->callback);
		$url = 'https://account.accurate.id/oauth/token?' . http_build_query($params);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		$result = json_decode($response, true);
		print_prev($result);
		if(isset($result['access_token']) && isset($result['refresh_token'])){
			$this->accurate_model->save(array('id' => 1, 'access_token' => $result['access_token'], 'refresh_token' => $result['refresh_token']));
		}
		curl_close($ch);
	}

	public function refresh()
	{
		$this->load->model(array('accurate_model'));
		$accurate 		= $this->accurate_model->get(array('id' => 1, 'columns' => 'A.id, A.client_id, A.client_secret, A.refresh_token'));


		$clientId 		= $accurate->client_id; //'e44c5a4b-6c9b-4e86-ad8e-33cc2346d794';
		$clientSecret 	= $accurate->client_secret; //'292f1a4e1b4538d82555b6927ddc5413';
		
		$accessToken 	= base64_encode($clientId.':'.$clientSecret);
		$authorization 	= "Authorization: Basic ".$accessToken;

		$params = array("grant_type" => "refresh_token", "refresh_token" => $accurate->refresh_token);
		$url = 'https://account.accurate.id/oauth/token?' . http_build_query($params);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		$result = json_decode($response, true);
		print_prev($result);
		if(isset($result['access_token']) && isset($result['refresh_token'])){
			$this->accurate_model->save(array('id' => 1, 'access_token' => $result['access_token'], 'refresh_token' => $result['refresh_token']));
		}
		curl_close($ch);
	}


	public function syncronize_site(){
		$this->load->model(array('accurate_model', 'proyek_accurate_model'));
		$accurate 		= $this->accurate_model->get(array('id' => 1, 'columns' => 'A.id, A.access_token'));
		$authorization = "Authorization: Bearer ".$accurate->access_token;
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
		curl_setopt($ch, CURLOPT_URL, "https://account.accurate.id/api/db-list.do");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$response = curl_exec($ch); 
		$resultBU = json_decode($response, true);
		curl_close($ch);

		if(isset($resultBU['d'])){
			// $this->proyek_accurate_model->nonactive_all();
			foreach($resultBU['d'] AS $item){
				$BUId = $item['id'];
				if($BUId == 423160){
					$BUName = 'SN'; 
				}else if($BUId == 433997){
					$BUName = 'SNI';
				}else if($BUId == 491420){
					$BUName = 'ION';
				}

				$ch = curl_init(); 
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
				curl_setopt($ch, CURLOPT_URL, "https://account.accurate.id/api/open-db.do?id=".$BUId);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
				$response = curl_exec($ch); 
				$resultSession = json_decode($response, true);
				curl_close($ch);

				$header = array('Content-Type: application/json' , $authorization, "X-Session-ID: ".$resultSession['session']);
				$projectURL = $resultSession['host']."/accurate/api/project/list.do?sp.pageSize=100&sp.sort=name|asc";
				$ch = curl_init(); 
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
				curl_setopt($ch, CURLOPT_URL, $projectURL);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
				$response = curl_exec($ch); 
				$resultProject = json_decode($response, true);
				curl_close($ch);

				if(isset($resultProject['d'])){
					for($page=1; $page <= $resultProject['sp']['pageCount']; $page++){

						$header = array('Content-Type: application/json' , $authorization, "X-Session-ID: ".$resultSession['session']);
						$projectURL = $resultSession['host']."/accurate/api/project/list.do?sp.pageSize=100&sp.sort=name|asc&fields=id,name,no,address&sp.page=".$page;

						$ch = curl_init(); 
						curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
						curl_setopt($ch, CURLOPT_URL, $projectURL);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
						$response = curl_exec($ch); 
						$resultPrpject = json_decode($response, true);
						curl_close($ch);

						foreach($resultProject['d'] AS $item){
							$projectName = $item['name'];
							$projectNo = $item['no'];
							$projectId = $item['id'];
								$projectBranch = '';
							if($item['branchId'] == 50){
								$projectBranch = 'Head Office';
							}else if($item['branchId'] == 101 ||  $item['branchId'] == 1801){
								$projectBranch = 'Central';
							}else if($item['branchId'] == 150 ||  $item['branchId'] == 1751 ){
								$projectBranch = 'East';
							}else if($item['branchId'] == 250){
								$projectBranch = 'Makasar';
							}else if($item['branchId'] == 52 ||  $item['branchId'] == 1750){
								$projectBranch = 'West';
							}

							// $data['id']				= $item['id'];
							$data['db_id']			= $BUId;
							$data['customer_id']	= 0;
							$data['branch_id']		= 0;
							$data['nomor_proyek']	= $item['no'];
							$data['nama_proyek']	= $item['name'];
							$data['kontrak_mulai']	= '';
							$data['kontrak_selesai']= '';
							$data['is_active']		= 1;
							if($item['customerId']){
								$data['customer_id'] = $item['customerId'];
							}
							if($item['branchId']){
								$data['branch_id'] = $item['branchId'];
							}
							if($item['startDate'] != ''){
								$data['kontrak_mulai']	= substr($item['startDate'], 6, 4).'-'.substr($item['startDate'], 3, 2).'-'.substr($item['startDate'], 0, 2);
							}
							if($item['finishDate'] != ''){
								$data['kontrak_selesai']	= substr($item['finishDate'], 6, 4).'-'.substr($item['finishDate'], 3, 2).'-'.substr($item['finishDate'], 0, 2);
							}
							
							$exist = $this->proyek_accurate_model->get(array('columns' => 'A.id', 'db_id' => $data['db_id'], 'nomor_proyek' => $data['nomor_proyek']));
							if($exist){
								$data['id']	= $exist->id;
							}
							$this->proyek_accurate_model->replace($data);
						}						
					}
				}
			}
		}
	}

	public function detail_site($site_id  = FALSE){
		$this->load->model(array('accurate_model', 'proyek_accurate_model'));
		$accurate 		= $this->accurate_model->get(array('id' => 1, 'columns' => 'A.id, A.access_token'));
		$authorization = "Authorization: Bearer ".$accurate->access_token;

		$proyek = $this->proyek_accurate_model->get(array('id' => $site_id));
		if($proyek){
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
			curl_setopt($ch, CURLOPT_URL, "https://account.accurate.id/api/open-db.do?id=".$proyek->db_id);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			$response = curl_exec($ch); 
			$resultSession = json_decode($response, true);
			curl_close($ch);
			$header = array('Content-Type: application/json' , $authorization, "X-Session-ID: ".$resultSession['session']);
			$proyekURL = $resultSession['host']."/accurate/api/project/detail.do?id=".$proyek->id;

			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_URL, $proyekURL);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			$response = curl_exec($ch); 
			$resultProyek = json_decode($response, true);
			curl_close($ch);

			// print_prev($resultProyek);

			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
			curl_setopt($ch, CURLOPT_URL, "https://account.accurate.id/api/open-db.do?id=".$proyek->db_id);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			$response = curl_exec($ch); 
			$resultSession = json_decode($response, true);
			curl_close($ch);
			$header = array('Content-Type: application/json' , $authorization, "X-Session-ID: ".$resultSession['session']);
			$customerURL = $resultSession['host']."/accurate/api/customer/detail.do?id=".$proyek->customer_id;

			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_URL, $customerURL);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			$response = curl_exec($ch); 
			$resultCustomer = json_decode($response, true);
			curl_close($ch);

			// print_prev($resultCustomer);
		}
	}

	public function syncronize_branch(){

		$this->load->model(array('accurate_model'));
		$accurate 		= $this->accurate_model->get(array('id' => 1, 'columns' => 'A.id, A.access_token'));
		$authorization = "Authorization: Bearer ".$accurate->access_token;
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
		curl_setopt($ch, CURLOPT_URL, "https://account.accurate.id/api/db-list.do");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$response = curl_exec($ch); 
		$resultBU = json_decode($response, true);
		curl_close($ch);
		if(isset($resultBU['d'])){
			foreach($resultBU['d'] AS $item){
				$BUId = $item['id'];
				$BUName = $item['alias']; 

				$ch = curl_init(); 
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
				curl_setopt($ch, CURLOPT_URL, "https://account.accurate.id/api/open-db.do?id=".$BUId);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
				$response = curl_exec($ch); 
				$resultSession = json_decode($response, true);
				curl_close($ch);
				print_prev($resultSession);
				$header = array('Content-Type: application/json' , $authorization, "X-Session-ID: ".$resultSession['session']);
				$customerURL = $resultSession['host']."/accurate/api/branch/list.do";
				$ch = curl_init(); 
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
				curl_setopt($ch, CURLOPT_URL, $customerURL);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
				$response = curl_exec($ch); 
				$resultBranch = json_decode($response, true);
				curl_close($ch);

				if(isset($resultBranch['d'])){
					foreach($resultBranch['d'] AS $item){
						$branchName = $item['name'];
						$branchId 	= $item['id'];
						echo $BUName.' - '.$branchId.' - '.$branchName;
					}
				}
			}
		}
	}

	public function syncronize_customer(){

		$this->load->model(array('accurate_model', 'proyek_accurate_model', 'site_model', 'client_model'));
		$list_proyek = $this->proyek_accurate_model->gets(array('columns' => 'A.id, A.db_id, A.customer_id', 'kontrak_selesai' => '2021-01-01','orderby' => 'A.db_id', 'order' => 'ASC'));

		$accurate 		= $this->accurate_model->get(array('id' => 1, 'columns' => 'A.id, A.access_token'));
		$authorization = "Authorization: Bearer ".$accurate->access_token;
		foreach($list_proyek AS $item){	
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
			curl_setopt($ch, CURLOPT_URL, "https://account.accurate.id/api/open-db.do?id=".$item->db_id);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			$response = curl_exec($ch); 
			$resultSession = json_decode($response, true);
			curl_close($ch);

			$header = array('Content-Type: application/json' , $authorization, "X-Session-ID: ".$resultSession['session']);
			$customerURL = $resultSession['host']."/accurate/api/customer/detail.do?id=".$item->customer_id;

			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_URL, $customerURL);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			$response = curl_exec($ch); 
			$resultCustomer = json_decode($response, true);
			curl_close($ch);

			if(isset($resultCustomer['d']['id'])){
				$client = $this->client_model->get(array('name' => $resultCustomer['d']['name'], 'columns' => 'A.id, A.name, A.address'));
				if($client){
					$data_client['id']						= $client->id;
					$data_client['accurate_customer_id']	= $resultCustomer['d']['id'];
					$data_client['accurate_customer_no']	= $resultCustomer['d']['customerNo'];

					if($client->address == ''){
						$data_client['address']	= $resultCustomer['d']['billStreet'];
					}
					$this->client_model->save($data_client);
				}
			}	
		}
	}

	// public function  replace_site(){
	// 	$this->load->model(array('site_model', 'proyek_accurate_model'));
	// 	$list_site = $this->site_model->gets(array('columns' => 'A.id, A.name, A.proyek_id, A.proyek_number'));
	// 	foreach($list_site AS $item){
	// 		if($item->proyek_id > 0){
	// 			$proyek = $this->proyek_accurate_model->get(array('id' => $item->proyek_id, 'columns' => 'A.nomor_proyek'));
	// 			$this->site_model->save(array('id' => $item->id, 'proyek_number' => $proyek->nomor_proyek));
	// 			print_prev($item);
	// 			print_prev($proyek);
	// 		}
	// 	}
	// }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employeeshift extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function site(){
		if($this->input->post('site_id')){
			$this->session->set_userdata('site',$this->input->post('site_id'));
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Site sudah dipilih.','success'));
		redirect(base_url('employeeshift/list'));
	}

	public function list(){
		
		$this->load->model(array('site_model'));
		
		$data['_TITLE_'] 		= 'Shift Karyawan';
		$data['_PAGE_'] 		= 'employeeshift/list';
		$data['_MENU_PARENT_'] 	= 'payroll_employee';
		$data['_MENU_'] 		= 'employeeshift';
		$data['site_id']		= $this->session->userdata('site');
		$data['site']			= $this->site_model->get_site(array('id' => $data['site_id'], 'columns' => 'A.id, A.name, A.address'));
		$branch_id				= '';
		if($this->session->userdata('branch') > 1){
			$branch_id 	= $this->session->userdata('branch');
		}

		if(in_array($this->session->userdata('role'), $this->config->item('personalia'))){
			$data['list_site']		= $this->site_model->gets_ro_site(array('columns' => 'A.id, A.name, B.code AS company_code', 'indirect' => true, 'branch_id' => $branch_id)); 
		}else{
			$data['list_site']		= $this->site_model->gets_ro_site(array('columns' => 'A.id, A.name, B.code AS company_code', 'branch_id' => $branch_id));
		}		
		$this->view($data);
	}

	public function list_ajax(){

		$this->load->model(array('employeeshift_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id AS employee_id, B.id, A.full_name, A.employee_number, B.shift, D.name AS position_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['employee_number']	= $_POST['columns'][3]['search']['value'];
		$params['position_name']	= $_POST['columns'][4]['search']['value'];
		$params['shift']			= $_POST['columns'][5]['search']['value'];
		
		$params['site_id']		=  $this->session->userdata('site');

		$list 	= $this->employeeshift_model->gets($params);
		$total 	= $this->employeeshift_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{			
			$result['id']				= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->employee_id.'">';
			$result['no'] 				= $i;
			$result['full_name']		= $item->full_name;
			$result['employee_number']	= $item->employee_number;
			$result['shift']			= $item->shift;
			$result['position_name']	= $item->position_name;
			$result['action'] 			=
				'<a class="btn-sm btn-success btn-block" href="'.base_url("employeeshift/form/".$item->employee_id."/".$item->id).'">Ubah</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function form($employee_id = FALSE)
	{
		$this->load->model(array('employeeshift_model', 'employee_model', 'position_model'));
		$data['employee_id'] 	= $employee_id;
		$data['id']				= '';
		$data['position']		= '';
		$data['shift']			= '';

		if($this->input->post()){
			$insert['employee_id']	= $data['employee_id'] 	= $this->input->post('employee_id');
			$insert['shift']		= $data['shif'] 		= $this->input->post('shift');
			$insert['id']			= $data['id'] 			= $this->input->post('id');

			$employee = $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.id, A.full_name, A.employee_number, A.site_id'));
			if($employee){
				$this->form_validation->set_rules('shift', '', 'required');
				if ($this->form_validation->run() == FALSE)
				{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
				}else{
					$shift = $this->employeeshift_model->get(array('columns' => 'A.id', 'employee_id' => $insert['employee_id']));
					if($shift){
						$insert['id']	= $data['id'] = $shift->id;
					}
					$this->employeeshift_model->save($insert);
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}
				redirect(base_url('employeeshift/list/'.$employee->site_id));
			}
		}

		if ($employee_id)
		{
			$employee = $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.id, A.full_name, A.employee_number, A.site_id, A.position_id'));
			$data['employee'] = $employee;
			if (!$employee)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('employeeshift/list/'.$employee->site_id));
			}else{
				$position 		= $this->position_model->get(array('id' => $employee->position_id, 'columns' => 'A.name'));
				if($position){
					$data['position'] = $position->name;
				}
				$shift = $this->employeeshift_model->get(array('employee_id' => $employee_id, 'columns' => 'A.shift'));
				if($shift){
					$data['shift'] = $shift->shift;
				}
			}
		}

		$data['_TITLE_'] 		= 'Shift Karyawan';
		$data['_PAGE_'] 		= 'employeeshift/form';
		$data['_MENU_PARENT_'] 	= 'payroll_employee';
		$data['_MENU_'] 		= 'employeeshift';
		return $this->view($data);
	}

	public function save(){

		$shift 	= $this->input->post('shift_id');
		foreach ($this->input->post('id') as $employee_id)
		{
			$this->load->model(array('employeeshift_model'));
			$insert['id']			= '';
			
			$exist 					= $this->employeeshift_model->get(array('columns'=>'A.id', 'employee_id'=>$employee_id));
			if($exist){
				$insert['id']		= $exist->id;
			}

			$insert['employee_id']	= $employee_id;
			$insert['shift']		= $shift;
			$result = $this->employeeshift_model->save($insert);
			if (!$result) {
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pengajuan gagal.','danger'));
				redirect(base_url('employeeshift/list'));
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Pegajuan Sukses.','success'));
		redirect(base_url('employeeshift/list'));
	}
}
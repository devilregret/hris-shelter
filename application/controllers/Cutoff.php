
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cutoff extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function list(){
		
		$this->load->model(array('site_model'));

		$data['_TITLE_'] 		= 'Cut Off';
		$data['_PAGE_'] 		= 'cutoff/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'cutoff';
		$this->view($data);
	}

	public function list_ajax(){

		$this->load->model(array('cutoff_model', 'site_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "B.code AS company_code, A.id, A.name AS site_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['company_code']	= $_POST['columns'][1]['search']['value'];
		$params['site_name']	= $_POST['columns'][2]['search']['value'];	
		
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		$list 	= $this->site_model->gets_ro_site($params);
		$total 	= $this->site_model->gets_ro_site($params, TRUE);
		foreach($list as $item)
		{
			$cutoff 				= $this->cutoff_model->get(array('site_id'=> $item->id, 'columns' => 'A.id, A.start_date, A.end_date'));
			$cutoff_date 			= "";
			if($cutoff){
				$cutoff_date 		= $cutoff->start_date.' - '.$cutoff->end_date;
				$cutoff_id 			= $cutoff->id;
			}
			$result['no'] 				= $i;
			$result['company_code']		= $item->company_code;
			$result['site_name']		= $item->site_name;
			$result['cutoff']			= $cutoff_date;
			
			$result['action'] 		=
				// '<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("cutoff/preview/".$item->id).'">Lihat</a>
				'<a class="btn-sm btn-success btn-block" href="'.base_url("cutoff/form/".$item->id).'">Ubah</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}

	public function form($site_id = FALSE)
	{
		$this->load->model(array('cutoff_model', 'formula_model'));

		$data['id'] 			= '';
		$data['site_id'] 		= $site_id;
		$data['start_date']		= '';
		$data['end_date']		= '';

		if($this->input->post()){
			$data['id'] 			= $this->input->post('id');
			$data['site_id'] 		= $this->input->post('site_id');
			$data['start_date'] 	= $this->input->post('start_date');
			$data['end_date']		= $this->input->post('end_date');

			$this->form_validation->set_rules('start_date', '', 'required');
			$this->form_validation->set_rules('end_date', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$save_id	 	= $this->cutoff_model->save($data);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('cutoff/list'));
		}

		if ($site_id)
		{
			$payroll = (array) $this->cutoff_model->get(array('site_id' => $site_id));
			if ($payroll)
			{
				$data = $payroll;
			}
		}

		$data['list_formula'] 	= $this->formula_model->gets(array());
		$data['_TITLE_'] 		= 'Cut Off';
		$data['_PAGE_'] 		= 'cutoff/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'cutoff';
		return $this->view($data);
	}

	public function preview($site_id = FALSE, $cutoff_id=FALSE)
	{
		$this->load->model(array('cutoff_model', 'site_model'));
		$data['_TITLE_'] 		= 'Preview Cut Off';
		$data['_PAGE_']	 		= 'workday/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'workday';
		
		$data['cutoff'] = $this->cutoff_model->preview(array('site_id' => $site_id, 'columns' => 'A.*'));
		$data['site'] 	= $this->site_model->get(array('site_id' => $site_id, 'columns' => 'A.name, A.address'));
		
		$this->load->view('cutoff/preview', $data);
	}

	public function delete($cutoff_id = false)
	{
		$this->load->model('cutoff_model');
		if ($cutoff_id)
		{
			$data =  $this->cutoff_model->get(array('id' => $cutoff_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $cutoff_id, 'is_active' => 0);
				$result = $this->cutoff_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('cutoff/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('cutoff/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('cutoff/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('cutoff/list'));
		}
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Development extends Frontend_Controller {

	public function list($view = 'all'){
		$data['_TITLE_'] 		= 'Daftar Request';
		$data['_PAGE_'] 		= 'development/list';
		$data['_MENU_PARENT_'] 	= 'development';
		$data['_MENU_'] 		= 'development';
		$data['view']			= $view;
		$this->view($data);
	}

	public function list_ajax($view = 'all'){
		$this->load->model(array('development_model', 'user_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.title, A.description, UPPER(A.status) AS status, A.target, A.cost, A.priority, DATE_FORMAT(A.created_at, "%Y-%m-%d") AS created_at, DATE_FORMAT(A.updated_at, "%Y-%m-%d") AS updated_at, B.name AS position, C.name AS city, A.developer_id, A.created_by, A.updated_by';

		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		if($this->session->userdata('role') != 101){
			$params['employee_id']	= $this->session->userdata('user_id');
			$params['position_id']	= $this->session->userdata('position_id');
		}else{
			$params['developer_id']	= $this->session->userdata('user_id');
		}

		$params['title']		= $_POST['columns'][0]['search']['value'];
		$params['description']	= $_POST['columns'][1]['search']['value'];

		if($view != 'all'){
			$params['status'] = $view;
		}
		$list 	= $this->development_model->gets($params);
		$total 	= $this->development_model->gets($params, TRUE);
		
		$data 	= array();
		foreach($list as $item)
		{
			$result['title']		= $item->title;
			$result['description']	= $item->description;
			$result['developer'] 	= '';
			$result['created_at']	= $item->created_at;
			$result['created_by']	= '';
			$result['updated_at']	= $item->updated_at;
			$result['updated_by']	= '';

			$developer = $this->user_model->get(array('columns' => 'A.full_name', 'id' => $item->developer_id));
			if($developer){
				$result['developer'] = $developer->full_name;
			}
			$create = $this->user_model->get(array('columns' => 'A.full_name', 'id' => $item->created_by));
			if($create){
				$result['created_by'] = $create->full_name;
			}
			$update = $this->user_model->get(array('columns' => 'A.full_name', 'id' => $item->updated_by));
			if($update){
				$result['updated_by'] = $update->full_name;
			}
			
			if($item->status == 'NEW'){
				$result['status'] = '<span class="text-primary">'.$item->status.'</span>';
			}else if($item->status == 'PROCESSED'){
				$result['status'] = '<span class="text-warning">'.$item->status.'</span>';
			}else if($item->status == 'CLOSED'){
				$result['status'] = '<span class="text-danger">'.$item->status.'</span>';
			}else{
				$result['status'] = '<span class="text-success">'.$item->status.'</span>';
			}
			$result['action'] 		=
				// '<a class="btn-sm btn-info btn-action btn-block" target="_blank" href="'. base_url("development/preview/".$item->id).'">Lihat</a>'
				'<a class="btn-sm btn-success btn-block" href="'.base_url("development/form/".$item->id).'">Update</a>';
			array_push($data, $result);
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function form($task_id = FALSE)
	{
		$this->load->model(array('user_model', 'development_model', 'position_model', 'city_model'));

		$data['id']				= '';		
		$data['title']			= '';
		$data['description'] 	= '';
		$data['status'] 		= '';
		$data['target'] 		= '';
		$data['cost'] 			= '';
		$data['developer_id'] 	= '';
		$data['priority'] 		= '';
		$data['position_id'] 	= '';
		$data['city_id'] 		= '';
		if($this->input->post()){
			$data['id'] 			= $this->input->post('id');
			$data['title'] 			= $this->input->post('title');
			$data['description'] 	= $this->input->post('description');
			$data['status'] 		= $this->input->post('status');
			$data['target'] 		= $this->input->post('target');
			$data['cost'] 			= $this->input->post('cost');
			$data['developer_id'] 	= $this->input->post('developer_id');
			$data['priority'] 		= $this->input->post('priority');
			$data['position_id'] 	= $this->input->post('position_id');
			$data['city_id'] 		= $this->input->post('city_id');

			$this->form_validation->set_rules('title', '', 'required');
			$this->form_validation->set_rules('description', '', 'required');
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{

				$save_id	 	= $this->development_model->save($data);
				$detail['id'] 				= '';
				$detail['development_id'] 	= $save_id;
				$detail['description'] 		= $data['description'];
				$detail['status'] 			= $data['status'];
				$detail['target'] 			= $data['target'];
				$detail['cost'] 			= $data['cost'];
				$detail['developer_id']	 	= $data['developer_id'];
				$detail['priority'] 		= $data['priority'];
				$detail['position_id'] 		= $data['position_id'];
				$detail['city_id'] 			= $data['city_id'];

				$this->development_model->save_detail($detail);
				if ($save_id) {
					$task_id = $save_id;
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('development/form/'.$task_id));
		}

		if ($task_id)
		{
			$data = (array) $this->development_model->get(array('id' => $task_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('development/list'));
			}
		}
		$data['detail']			= array();
		if($data['id'] != ''){
			$list_detail = $this->development_model->gets_detail(array('development_id' => $data['id'], 'columns' => 'A.id, A.description, UPPER(A.status) AS status, A.target, A.cost, A.priority, DATE_FORMAT(A.created_at, "%Y-%m-%d") AS created_at, B.name AS position, C.name AS city, A.developer_id, A.created_by', 'orderby' => 'A.created_at', 'order' => 'DESC'));

			foreach($list_detail as $item){
				$detail 	= array();
				$detail['description']	= $item->description;
				$detail['target'] 		= $item->target;
				$detail['cost'] 		= rupiah_round($item->cost);
				$detail['priority']		= $item->priority;
				$detail['position']		= $item->position;
				$detail['city']			= $item->city;
				$detail['developer'] 	= '';
				$detail['created_at']	= $item->created_at;
				$detail['created_by']	= '';

				$developer = $this->user_model->get(array('columns' => 'A.full_name', 'id' => $item->developer_id));
				if($developer){
					$detail['developer'] = $developer->full_name;
				}
				$create = $this->user_model->get(array('columns' => 'A.full_name', 'id' => $item->created_by));
				if($create){
					$detail['created_by'] = $create->full_name;
				}
			
				if($item->status == 'NEW'){
					$detail['status'] = '<span class="text-primary">'.$item->status.'</span>';
				}else if($item->status == 'PROCESSED'){
					$detail['status'] = '<span class="text-warning">'.$item->status.'</span>';
				}else if($item->status == 'CLOSED'){
					$detail['status'] = '<span class="text-danger">'.$item->status.'</span>';
				}else{
					$detail['status'] = '<span class="text-success">'.$item->status.'</span>';
				}
				
				array_push($data['detail'], $detail);
			}
		}

		if($this->session->userdata('role') != 101){
			$data['list_developer'] = $this->user_model->gets(array('columns' => 'A.id, A.full_name', 'role_in' => array(101)));
		}else{
			$data['list_developer'] = $this->user_model->gets(array('columns' => 'A.id, A.full_name', 'id' => $this->session->userdata('user_id')));
		}

		$data['list_division'] 	= $this->position_model->gets_indirect(array('columns' => 'A.id, A.name', 'site_id' => 2, 'group_by' => 'A.id'));
		$data['list_city'] 		= $this->city_model->gets(array('columns' => 'A.id, A.name'));

		$data['_TITLE_'] 		= 'Form Request';
		$data['_PAGE_'] 		= 'development/form';
		$data['_MENU_PARENT_'] 	= 'development';
		$data['_MENU_'] 		= 'development';
		return $this->view($data);
	}

	public function preview($development_id = FALSE){
		$this->load->model(array('development_model', 'user_model'));

		if (!$development_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('development/list'));
		}

		$data['preview'] = $this->development_model->preview(array('id' => $development_id, 'columns' => 'A.id, A.title, A.description, UPPER(A.status) AS status, A.target, A.cost, A.priority, DATE_FORMAT(A.created_at, "%Y-%m-%d") AS created_at, DATE_FORMAT(A.updated_at, "%Y-%m-%d") AS updated_at, B.name AS position, C.name AS city, A.developer_id, A.created_by, A.updated_by'));

		if($data['preview']->status == 'NEW'){
			$data['preview']->status = '<span class="text-primary">'.$data['preview']->status.'</span>';
		}else if($data['preview']->status == 'PROCESSED'){
			$data['preview']->status = '<span class="text-warning">'.$data['preview']->status.'</span>';
		}else if($data['preview']->status == 'CLOSED'){
			$data['preview']->status = '<span class="text-danger">'.$data['preview']->status.'</span>';
		}else{
			$data['preview']->status = '<span class="text-success">'.$data['preview']->status.'</span>';
		}
		$data['preview']->cost = rupiah_round($data['preview']->cost);
		$developer = $this->user_model->get(array('columns' => 'A.full_name', 'id' => $data['preview']->developer_id));
		if($developer){
			$data['preview']->developer = $developer->full_name;
		}
		$create = $this->user_model->get(array('columns' => 'A.full_name', 'id' => $data['preview']->created_by));
		if($create){
			$data['preview']->created_by = $create->full_name;
		}
		$update = $this->user_model->get(array('columns' => 'A.full_name', 'id' => $data['preview']->updated_by));
		if($update){
			$data['preview']->updated_by = $update->full_name;
		}

		$data['detail']			= array();
		if($development_id != ''){
			$list_detail = $this->development_model->gets_detail(array('development_id' => $development_id, 'columns' => 'A.id, A.description, UPPER(A.status) AS status, A.target, A.cost, A.priority, DATE_FORMAT(A.created_at, "%Y-%m-%d") AS created_at, B.name AS position, C.name AS city, A.developer_id, A.created_by', 'orderby' => 'A.created_at', 'order' => 'DESC'));

			foreach($list_detail as $item){
				$detail 	= array();
				$detail['description']	= $item->description;
				$detail['target'] 		= $item->target;
				$detail['cost'] 		= rupiah_round($item->cost);
				$detail['priority']		= $item->priority;
				$detail['position']		= $item->position;
				$detail['city']			= $item->city;
				$detail['developer'] 	= '';
				$detail['created_at']	= $item->created_at;
				$detail['created_by']	= '';

				$developer = $this->user_model->get(array('columns' => 'A.full_name', 'id' => $item->developer_id));
				if($developer){
					$detail['developer'] = $developer->full_name;
				}
				$create = $this->user_model->get(array('columns' => 'A.full_name', 'id' => $item->created_by));
				if($create){
					$detail['created_by'] = $create->full_name;
				}
			
				if($item->status == 'NEW'){
					$detail['status'] = '<span class="text-primary">'.$item->status.'</span>';
				}else if($item->status == 'PROCESSED'){
					$detail['status'] = '<span class="text-warning">'.$item->status.'</span>';
				}else if($item->status == 'CLOSED'){
					$detail['status'] = '<span class="text-danger">'.$item->status.'</span>';
				}else{
					$detail['status'] = '<span class="text-success">'.$item->status.'</span>';
				}
				
				array_push($data['detail'], $detail);
			}
		}

		$data['_TITLE_'] 		= 'Preview Development';
		$data['_PAGE_']	 		= 'development/preview';
		$data['_MENU_PARENT_'] 	= 'development';
		$data['_MENU_'] 		= 'development';

		// $data['_TITLE_'] 		= 'Form Development';
		// $data['_PAGE_'] 		= 'development/form';
		// $data['_MENU_PARENT_'] 	= 'development';
		// $data['_MENU_'] 		= 'development';
		return $this->view($data);
		
		// $this->load->view('development/preview', $data);
	}
}
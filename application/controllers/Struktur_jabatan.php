<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Struktur_jabatan extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,5,6))){
			redirect(base_url());
		}
	}

	public function delete($struktur_id = false)
	{
		$this->load->model('struktur_jabatan_model');
		if ($struktur_id)
		{
			$data =  $this->struktur_jabatan_model->get(array('id' => $struktur_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $struktur_id, 'is_active' => 0);
				$result = $this->struktur_jabatan_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('struktur_jabatan/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('struktur_jabatan/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('struktur_jabatan/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('struktur_jabatan/list'));
		}
	}

	public function preview($position_id = FALSE)
	{
		$this->load->model('position_model');
		$data['_TITLE_'] 		= 'Preview Jabatan';
		$data['_PAGE_']	 		= 'position/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'position';

		$data['id'] = $position_id;

		
		if (!$position_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('position/list'));
		}

		$data['preview'] = $this->position_model->preview(array('id' => $position_id));
		$this->load->view('position/preview', $data);
	}

	public function form($struktur_id = FALSE)
	{
		$this->load->model(array('struktur_jabatan_model', 'branch_model'));

		$data['id'] 				= '';
		$data['struktur_name']		= '';
		$data['branch_id']			= '';
		$data['parent_id']			= '';
		$data['level_id']			= '';

		if($this->input->post()){
			$data['id'] 			= $this->input->post('id');
			$data['parent_id'] 		= $this->input->post('parent_id');
			$data['branch_id'] 		= $this->input->post('branch_id');
			$data['level_id'] 		= $this->input->post('level_id');
			$data['struktur_name'] 		= $this->input->post('struktur_name');
			
			$this->form_validation->set_rules('parent_id', '', 'required');
			$this->form_validation->set_rules('branch_id', '', 'required');
			$this->form_validation->set_rules('level_id', '', 'required');
			$this->form_validation->set_rules('struktur_name', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$save_id	 	= $this->struktur_jabatan_model->save($data);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('struktur_jabatan/list'));
		}

		if ($struktur_id)
		{
			$data = (array) $this->struktur_jabatan_model->get(array('id' => $struktur_id));
			// print_r($data);
			// die();
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('struktur_jabatan/list'));
			}
		}

		$data['list_branch'] 	= $this->branch_model->gets(array());
		$data['list_posisi_level'] 	= $this->struktur_jabatan_model->getMasterLevel();
		$data['list_parent'] 	= $this->struktur_jabatan_model->getMasterParent();

		$data['_TITLE_'] 		= 'Struktur Approval';
		$data['_PAGE_'] 		= 'struktur_jabatan/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'struktur_approval';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Struktur Jabatan';
		$data['_PAGE_'] 		= 'struktur_jabatan/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'struktur_jabatan';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('struktur_jabatan_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.struktur_name, B.level_name as child_name, D.level_name as parent_name, A.branch_id, E.name as branch_name';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['name']			= $_POST['columns'][1]['search']['value'];
		$params['description']	= $_POST['columns'][2]['search']['value'];

		$list 	= $this->struktur_jabatan_model->getStruktur($params);
		$total 	= $this->struktur_jabatan_model->getStruktur($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		// echo "<script>console.log('Debug Objects: ssdsd' );</script>";
		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['name'] 		= $item->struktur_name;
			$result['level'] 		= $item->child_name;
			$result['parent'] 		= $item->parent_name;
			$result['branch'] 		= $item->branch_name;
			$result['action'] 		=
				'<a class="btn-sm btn-success btn-block" href="'.base_url("struktur_jabatan/form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("struktur_jabatan/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function export()
	{

		$this->load->model(array('position_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = 'jabatan';
		$files = glob(FCPATH."files/".$title."/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Nama Jabatan");
		$excel->getActiveSheet()->setCellValue("B".$i, "Tipe Jabatan");

		$params 			= $this->input->get();
		$params['columns'] 	= 'A.id, A.name, A.description';
		$list_position 		= $this->position_model->gets($params);
		
		$i=2;
		foreach ($list_position as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->name);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->description);
			$i++;
		}
		$excel->getActiveSheet()->setTitle('Jabatan');
		
		$excel->createSheet();
		$excel->setActiveSheetIndex(1);
		$excel->getActiveSheet()->setCellValue('A1', 'Nama Tipe');
		$excel->getActiveSheet()->setCellValue('A2', 'Security');
		$excel->getActiveSheet()->setCellValue('A3', 'Non Security');
		$excel->getActiveSheet()->setCellValue('A4', 'Produksi');
		$excel->getActiveSheet()->setTitle('Tipe Jabatan');

		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function import(){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('position_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {
				$data['id'] 			= '';
				$data['name']			= '';
				$data['description']	= '';
				$data['name'] 			= replace_null($sheet->getCell('A'.$row)->getValue());
				$position = $this->position_model->get(array('is_name' => $data['name'], 'columns' => 'A.id'));
				if($position){
					continue;
				}
				$data['description'] 			= replace_null($sheet->getCell('B'.$row)->getValue());
				$save_id = $this->position_model->save($data);
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			redirect(base_url('position/list'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
			redirect(base_url('position/list'));
		}
	}
}
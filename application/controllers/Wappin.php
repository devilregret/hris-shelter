<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Wappin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function callback()
	{
		$entityBody = file_get_contents('php://input');
		$this->load->model(array('wappin_model'));
		$this->wappin_model->save_log(array('status' => $entityBody));
		$response['status'] = '000';
		echo json_encode($response);
	}
	
	public function authorize(){
		$this->load->model(array('wappin_model'));
		$config_id = 1;
		$wappin 		= $this->wappin_model->get(array('id' => $config_id, 'columns' => 'A.username, A.password, A.base_url'));
		if($wappin){
			$accessToken 	= base64_encode($wappin->username.':'.$wappin->password);
			$authorization 	= "Authorization: Basic ".$accessToken;
			$url = $wappin->base_url.'/v1/users/login';

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			$result = json_decode($response, true);
			$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if($httpcode != 200){
				sleep(60);
				redirect(base_url('wappin/authorize'));
			}else{
				$this->wappin_model->save(array('id' => $config_id, 'access_token' => $result['users'][0]['token']));
			}
			curl_close($ch);	
		}
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Client_Report extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		$role 	= array_merge($role, array(100, 51));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,3,8,50,51))){
			redirect(base_url());
		}
	}

	public function payroll_regular(){
		$this->load->model(array('site_model'));
		$site_id 				= $this->session->userdata('client_site');
		$data['_TITLE_'] 		= 'Daftar Payroll Regular';
		$data['_PAGE_'] 		= 'report_client/payroll_regular';
		$data['_MENU_PARENT_'] 	= 'report';
		$data['_MENU_'] 		= 'payroll_regular';
		$data['site'] 			= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.id, A.name, A.address'));
		$this->view($data);
	}
	
	public function payroll_regular_ajax(){

		$site_id 				= $this->session->userdata('client_site');
		$this->load->model(array('payroll_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "C.id AS site_id, C.name AS site_name, D.code AS company_code, COUNT(A.employee_id) AS employee_count, SUM(A.salary) AS summary_payroll, SUM(A.basic_salary) AS summary_basic, MIN(A.periode_start) AS periode_start, MAX(A.periode_end) AS periode_end ";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];

		$params['site_name']	= $_POST['columns'][1]['search']['value'];
		$params['company_code']	= $_POST['columns'][2]['search']['value'];
		$params['payment']		= array('Selesai');
		$params['site_id']		= $site_id;

		$list 	= $this->payroll_model->report($params);
		$total 	= $this->payroll_model->report($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['no'] 				= $i;
			$result['site_name']		= $item->site_name;
			$result['company_code']		= $item->company_code;
			$period 					= "";
			if($item->periode_start){
				$start_date = substr($item->periode_start, 8, 2).' '.get_month(substr($item->periode_start, 5, 2));
				$end_date 	= substr($item->periode_end, 8, 2).' '.get_month(substr($item->periode_end, 5, 2)).' '.substr($item->periode_end, 0, 4);
				$period 	= $start_date.' - '.$end_date;
			}
			$result['period']			= $period;
			$result['employee_count']	= $item->employee_count;
			$result['summary_payroll']	= format_rupiah((int) $item->summary_payroll);
			$result['summary_basic']	= format_rupiah((int) $item->summary_basic);
			$result['action'] 			= 
				'<a class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" href="'. base_url("client_report/payroll_regular_detail/".$item->periode_end).'">Detail</a>
				<a class="btn-sm btn-success btn-action btn-block"  style="cursor:pointer;" href="'. base_url("client_report/payroll_regular_export/".$item->periode_end."/".$item->periode_start).'">Export</a>
				';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function payroll_regular_detail($periode_end = FALSE){
		$this->load->model(array('site_model', 'payroll_model', 'employee_model'));
		$data['_TITLE_'] 		= 'Payroll Karyawan Regular';
		$data['_PAGE_'] 		= 'report_client/payroll_regular_detail';
		$data['_MENU_PARENT_'] 	= 'report';
		$data['_MENU_'] 		= 'payroll_regular';
		$site_id 				= $this->session->userdata('client_site');
		$params['status']			= 1;
		$params['status_approval'] 	= 3;
		$params['site_id'] 			= $site_id;

		$data['site']			= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.id, A.name, A.address'));
		$data['employee']		= $this->employee_model->gets($params, TRUE);
		$data['resume']			= $this->payroll_model->resume(array('site_id' => $site_id, 'periode_end' => $periode_end));

		$params['columns'] 		= "A.id, B.full_name AS employee_name, D.name AS position, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.salary, A.basic_salary, A.overtime_hour, A.attendance ";
		$params['payment']		= 'Selesai';

		$date=date_create($periode_end);
		$params['periode_end']	= date_format($date,"d/m/Y");
		$data['periode_end']	= $periode_end;
		$data['payroll'] 		= $this->payroll_model->gets($params);
		$this->view($data);
	}

	public function payroll_regular_export($end_period = FALSE, $start_period = FALSE)
	{
		$site_id 				= $this->session->userdata('client_site');
		$this->load->library('word');
		$this->load->model(array('report_template_model','site_model','payroll_model'));
		$template = $this->report_template_model->get(array('id' => 1, 'columns' => 'A.content, A.title'));
		
		$site 	= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.name'));
		if(!$site){
			redirect(base_url('payroll_report/detail/'.$site_id.'/'.$end_period));
		}

		$start_date = substr($start_period, 8, 2).' '.get_month(substr($start_period, 5, 2));
		$end_date 	= substr($end_period, 8, 2).' '.get_month(substr($end_period, 5, 2)).' '.substr($end_period, 0, 4);

		$params['columns'] 		= "A.id AS payroll_id, C.id AS site_id, C.name AS site_name, D.code AS company_code, COUNT(A.employee_id) AS employee_count, SUM(A.salary) AS summary_payroll, SUM(A.basic_salary) AS summary_basic, MIN(A.periode_start) AS periode_start, MAX(A.periode_end) AS periode_end ";
		$params['site_id']		= $site_id;
		$params['end_period']	= $end_period;
		$params['orderby']		= "A.periode_end";
		$params['order']		= "DESC";
		$params['payment']		= array('Selesai');
		$list_payroll = $this->payroll_model->report($params);
		
		$TUNJANGAN_KEMAHALAN	= 0;
		$TUNJANGAN_LEMBUR	 	= 0;
		$TRANSPORT_ALLOWANCE	= 0;
		$MEAL_ALLOW 			= 0;
		$TUNJANGAN_KEHADIRAN	= 0;
		$KELEBIHAN_JAM_KERJA	= 0;
		$OT_REG 				= 0;
		$OTN 					= 0;
		$OT_REQUEST				= 0;
		$SHIFT_MALAM			= 0;
		$TUNJANGAN_LUAR_KOTA	= 0;
		$KEKURANGAN_GAJI 		= 0;
		$TUNJAB 				= 0;
		$TUNJANGAN_KOMPLEKSITAS = 0;
		$BPJS_KES_PERUSAHAAN 	= 0;
		$BPJS_TK_PERUSAHAAN 	= 0;
		$GROSS_SALLARY 			= 0;
		$BIAYA_DITANGGUNG 		= 0;

		$BPJS_KS_KARYAWAN 		= 0;
		$BPJS_TK_KARYAWAN 		= 0;
		$KAPORLAP_KARYAWAN 		= 0;
		$PPH21 					= 0;
		$POTONGAN_FINANCE 		= 0;
		$JAMINAN_PENSIUN 		= 0;
		$POTONGAN_TABUNGAN_GADA = 0;
		$POTONGAN_THP_GROSS 	= 0;
		$TAKE_HOME_PAY  		= 0;

		foreach ($list_payroll as $item) {
			$payroll = $this->payroll_model->gets_detail(array('payroll_id' => $item->payroll_id, 'columns' => 'A.name, A.category, A.value'));
			foreach($payroll AS $detail){
				if($detail->category == 'potongan'){
					$GROSS_SALLARY += $detail->value;
				}
				if($detail->category == 'potongan'){
					$POTONGAN_THP_GROSS += $detail->value;
				}
				if (str_contains(strtoupper($detail->name), 'KEMAHALAN')) {
					$TUNJANGAN_KEMAHALAN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'LEMBUR')) {
					$TUNJANGAN_LEMBUR += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'TRANSPORT')) {
					$TRANSPORT_ALLOWANCE += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'MEAL')) {
					$MEAL_ALLOW += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'KEHADIRAN')) {
					$TUNJANGAN_KEHADIRAN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'JAM KERJA')) {
					$KELEBIHAN_JAM_KERJA += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'REG')) {
					$OT_REG += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'OTN')) {
					$OTN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'REQUEST')) {
					$OT_REQUEST += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'SHIFT')) {
					$SHIFT_MALAM += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'LUAR KOTA')) {
					$TUNJANGAN_LUAR_KOTA += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'KEKURANGAN GAJI')) {
					$KEKURANGAN_GAJI += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'TUNJAB')) {
					$TUNJAB += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'BPJS KES PERUSAHAAN')) {
					$BPJS_KES_PERUSAHAAN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'BPJS TK PERUSAHAAN')) {
					$BPJS_TK_PERUSAHAAN += $detail->value;
					continue;
				}
				if($detail->category == 'potongan'){
					$BIAYA_DITANGGUNG += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'BPJS KS KARYAWAN')) {
					$BPJS_KS_KARYAWAN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'BPJS TK KARYAWAN')) {
					$BPJS_TK_KARYAWAN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'KAPORLAP')) {
					$KAPORLAP_KARYAWAN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'PPH21')) {
					$PPH21 += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'JAMINAN PENSIUN')) {
					$JAMINAN_PENSIUN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'GADA')) {
					$POTONGAN_TABUNGAN_GADA += $detail->value;
					continue;
				}
				if($detail->category == 'pendapatan'){
					$POTONGAN_FINANCE += $detail->value;
					continue;
				}
			}
		}

		$summary 	= '<p style="font-family: Tahoma;font-size: 12px;">Laporan Gaji '.$site->name.'<br>Periode '.$start_date." - ".$end_date.'</p>';
		$summary 	.= '<span style="font-family: Tahoma;font-size: 12px;"><strong>TOTAL PENDAPATAN</strong></span><br>';
		$summary 	.= '<table  width="100%">';
		$summary  	.= '<tr>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">GAJI POKOK</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">TUNJANGAN LEMBUR</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">TUNJANGAN KEHADIRAN</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">KELEBIHAN JAM KERJA</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">TUNJANGAN LUAR KOTA</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">KEKURANGAN GAJI</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">LAIN-LAIN</td>
		</tr>';
		$summary  	.= '<tr>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($item->summary_basic).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($TUNJANGAN_LEMBUR).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($TUNJANGAN_KEHADIRAN).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($KELEBIHAN_JAM_KERJA).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($TUNJANGAN_LUAR_KOTA).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($KEKURANGAN_GAJI).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($TUNJAB+$TUNJANGAN_KOMPLEKSITAS+$MEAL_ALLOW+$TRANSPORT_ALLOWANCE+$TUNJANGAN_KEMAHALAN).'</td>
		</tr>';
		$summary 	.= '</table><p>&nbsp;<p>';
		$summary 	.= '<span style="font-family: Tahoma;font-size: 12px;"><strong>TOTAL POTONGAN</strong></span><br>';
		$summary 	.= '<table width="100%">';
		$summary  	.= '<tr>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">BPJS KS KARYAWAN</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">BPJS TK KARYAWAN</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">KAPORLAP KARYAWAN</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">PPH21/BLN</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">LAIN-LAIN</td>
		</tr>';
		$summary  	.= '<tr>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($BPJS_KS_KARYAWAN).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($BPJS_TK_KARYAWAN).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($KAPORLAP_KARYAWAN).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($PPH21).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($POTONGAN_FINANCE+$JAMINAN_PENSIUN+$POTONGAN_TABUNGAN_GADA).'</td>
		</tr>';
		$summary 	.= '</table>';

		$params 					= array();
		$params['status']			= 1;
		$params['status_approval'] 	= 3;
		$params['site_id'] 			= $site_id;
		$params['columns'] 		= "A.id, B.full_name AS employee_name, D.name AS position, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.salary, A.basic_salary, A.overtime_hour, A.attendance ";
		// $params['columns'] 			= "A.id, B.full_name AS employee_name, D.name AS position, B.bank_account, B.bank_name, B.bank_account_name, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.salary, A.basic_salary, A.overtime_calculation, A.bpjs_ks, (A.bpjs_jht + A.bpjs_jp) AS bpjs_tk, A.tax_calculation ";
		$params['payment']			= 'Selesai';

		$date=date_create($end_period);
		$params['periode_end']		= date_format($date,"d/m/Y");
		$list_payroll 				= $this->payroll_model->gets($params);

		$summary 	.= '<p>&nbsp;<p><span style="font-family: Tahoma;font-size: 12px;"><strong>DAFTAR KARYAWAN</strong></span><br>';
		$summary 	.= '<table width="100%">';
		$summary 	.= '<tr>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">NO</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">NAMA</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">POSISI</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">KEHADIRAN</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">LEMBUR</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">GAJI POKOK</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">THP</td>
		</tr>';
		$no = 1;
		foreach ($list_payroll as $item) {
		$summary 	.= '<tr>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.$no.'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.$item->employee_name.'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.$item->position.'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.$item->attendance.'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.$item->overtime_hour.'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($item->basic_salary).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($item->salary).'</td></tr>';
			$no++;
		}
		$summary 	.= '</table>';
		$this->generate_PDF(array('basepath' => FCPATH.'files/payroll_report_detail/', 'title' => $template->title, 'content' => $summary));
		redirect(base_url('files/payroll_report_detail/'.strtolower(str_replace(" ", "_",$template->title)).'.pdf'));
	}

	public function payroll_casual(){
		$this->load->model(array('site_model'));
		$data['_TITLE_'] 		= 'Daftar Payroll Casual';
		$data['_PAGE_'] 		= 'report_client/payroll_casual';
		$data['_MENU_PARENT_'] 	= 'report';
		$data['_MENU_'] 		= 'payroll_casual';
		$site_id 				= $this->session->userdata('client_site');
		$data['site'] 			= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.id, A.name, A.address'));
		$this->view($data);
	}
	
	public function payroll_casual_ajax(){

		$this->load->model(array('employee_casual_payroll_model'));
		$column_index = $_POST['order'][0]['column']; 

		$site_id 				= $this->session->userdata('client_site');
		$params['columns'] 		= "C.id AS site_id, C.name AS site_name, D.code AS company_code, COUNT(A.employee_id) AS employee_count, SUM(A.salary) AS summary_payroll, SUM(A.basic_salary) AS summary_basic, MIN(A.periode_start) AS periode_start, MAX(A.periode_end) AS periode_end ";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];

		$params['site_name']	= $_POST['columns'][1]['search']['value'];
		$params['company_code']	= $_POST['columns'][2]['search']['value'];
		$params['status']		= 'Selesai';
		$params['site_id']		= $site_id;

		$list 	= $this->employee_casual_payroll_model->report($params);
		$total 	= $this->employee_casual_payroll_model->report($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['no'] 				= $i;
			$result['site_name']		= $item->site_name;
			$result['company_code']		= $item->company_code;
			$period 					= "";
			if($item->periode_start){
				$start_date = substr($item->periode_start, 8, 2).' '.get_month(substr($item->periode_start, 5, 2));
				$end_date 	= substr($item->periode_end, 8, 2).' '.get_month(substr($item->periode_end, 5, 2)).' '.substr($item->periode_end, 0, 4);
				$period 	= $start_date.' - '.$end_date;
			}
			$result['period']			= $period;
			$result['employee_count']	= $item->employee_count;
			$result['summary_payroll']	= format_rupiah((int) $item->summary_payroll);
			$result['summary_basic']	= format_rupiah((int) $item->summary_basic);
			$result['action'] 			= 
				'<a class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" href="'. base_url("client_report/payroll_casual_detail/".$item->periode_end).'">Detail</a>
				<a class="btn-sm btn-success btn-action btn-block"  style="cursor:pointer;" href="'. base_url("client_report/payroll_casual_export/".$item->periode_end."/".$item->periode_start).'">Export</a>
				';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function payroll_casual_detail($periode_end = FALSE){
		$this->load->model(array('site_model', 'employee_casual_payroll_model', 'employee_model'));
		$data['_TITLE_'] 		= 'Payroll Karyawan Casual';
		$data['_PAGE_'] 		= 'report_client/payroll_casual_detail';
		$data['_MENU_PARENT_'] 	= 'report';
		$data['_MENU_'] 		= 'payroll_casual';
		$site_id 				= $this->session->userdata('client_site');
		$params['site_id'] 		= $site_id;

		$data['site']			= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.id, A.name, A.address'));
		$data['employee']		= $this->employee_model->gets($params, TRUE);
		$data['resume']			= $this->employee_casual_payroll_model->resume(array('site_id' => $site_id, 'periode_end' => $periode_end));

		$params['columns'] 		= "A.id, B.full_name AS employee_name, D.name AS position, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.salary, A.basic_salary, A.total_shift, A.ppn, A.pph ";
		$params['status']		= 'Selesai';

		$date=date_create($periode_end);
		$params['periode_end']	= date_format($date,"d/m/Y");
		$data['periode_end']	= $periode_end;
		$data['payroll'] 		= $this->employee_casual_payroll_model->gets($params);
		$this->view($data);
	}

	public function payroll_casual_export($end_period = FALSE, $start_period = FALSE)
	{
		$site_id 				= $this->session->userdata('client_site');
		$this->load->library('word');
		$this->load->model(array('report_template_model','site_model','employee_casual_payroll_model'));
		$template = $this->report_template_model->get(array('id' => 1, 'columns' => 'A.content, A.title'));
		
		$site 	= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.name'));
		if(!$site){
			redirect(base_url('payroll_report/detail/'.$site_id.'/'.$end_period));
		}

		$start_date = substr($start_period, 8, 2).' '.get_month(substr($start_period, 5, 2));
		$end_date 	= substr($end_period, 8, 2).' '.get_month(substr($end_period, 5, 2)).' '.substr($end_period, 0, 4);

		$params['columns'] 		= "A.id AS payroll_id, C.id AS site_id, C.name AS site_name, D.code AS company_code, COUNT(A.employee_id) AS employee_count, SUM(A.salary) AS summary_payroll, SUM(A.basic_salary) AS summary_basic, SUM(A.management_fee) AS management_fee, SUM(A.ppn) AS ppn, SUM(A.pph) AS pph, SUM(A.total_shift) AS total_shift, MIN(A.periode_start) AS periode_start, MAX(A.periode_end) AS periode_end ";
		$params['site_id']		= $site_id;
		$params['end_period']	= $end_period;
		$params['orderby']		= "A.periode_end";
		$params['order']		= "DESC";
		$params['status']		= 'Selesai';
		
		$list_payroll = $this->employee_casual_payroll_model->report($params);
		foreach ($list_payroll as $item) {
		}
		$summary 	= '<p style="font-family: Tahoma;font-size: 12px;">Laporan Gaji '.$site->name.'<br>Periode '.$start_date." - ".$end_date.'</p>';
		$summary 	.= '<span style="font-family: Tahoma;font-size: 12px;"><strong>TOTAL PENDAPATAN</strong></span><br>';
		$summary 	.= '<table  width="100%">';
		$summary  	.= '<tr>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">GAJI POKOK</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">MANAGEMENT FEE</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">PPN</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">PPH</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">TOTAL SHIFT</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">THP</td>
		</tr>';
		$summary  	.= '<tr>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($item->summary_basic).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($item->management_fee).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($item->ppn).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($item->pph).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($item->total_shift).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($item->summary_payroll).'</td>
		</tr>';
		$summary 	.= '</table>';

		$params 					= array();
		$params['site_id'] 			= $site_id;
		$params['columns'] 		= "A.id, B.full_name AS employee_name, D.name AS position, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.salary, A.basic_salary, A.total_shift, A.ppn, A.pph ";
		$params['payment']			= 'Selesai';

		$date=date_create($end_period);
		$params['periode_end']		= date_format($date,"d/m/Y");
		$list_payroll 				= $this->employee_casual_payroll_model->gets($params);
		$summary 	.= '<p>&nbsp;<p><span style="font-family: Tahoma;font-size: 12px;"><strong>DAFTAR KARYAWAN</strong></span><br>';
		$summary 	.= '<table width="100%">';
		$summary 	.= '<tr>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">NO</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">NAMA</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">POSISI</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">TOTAL SHIFT</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">GAJI POKOK</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px; text-align:center;">THP</td>
		</tr>';
		$no = 1;
		foreach ($list_payroll as $item) {
		$summary 	.= '<tr>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.$no.'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.$item->employee_name.'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.$item->position.'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.$item->total_shift.'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($item->basic_salary).'</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">&nbsp;'.rupiah_round($item->salary).'</td></tr>';
			$no++;
		}
		$summary 	.= '</table>';
		$this->generate_PDF(array('basepath' => FCPATH.'files/payroll_report_detail/', 'title' => $template->title, 'content' => $summary));
       	redirect(base_url('files/payroll_report_detail/'.strtolower(str_replace(" ", "_",$template->title)).'.pdf'));
	}

	private function generate_Word($data = array()){
		$this->load->library('Word');
		$filename = strtolower(str_replace(" ", "_",$data['title']));
		$phpWord = new PHPWord();
		$phpWord->createDoc($data['content'],$filename,1);
		die();
	}

	private function generate_PDF($data = array())
	{	
		$this->load->library("Pdf");
		ob_start();
		$pdf = new TCPDF("L", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// $pdf->SetProtection(array('print', 'copy'), $data['email']->pdf_password, null, 0, null);
		// $pdf->SetHeaderData($data['image'], '181', '', null);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shelter');
		$pdf->SetTitle($data['title']);
		$pdf->SetSubject($data['title']);
		$pdf->SetKeywords('Shelter');

		// $pdf->setPrintHeader(true);
		// $pdf->setPrintFooter(false);

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$pdf->SetFont('times', '', 10);
		$pdf->AddPage();
		$pdf->writeHTML($data['content'], true, false, true, false, '');

		$base_path  = $data['basepath'];
		if(!is_dir($base_path)){
			mkdir($base_path, 0777, true);
		}
		// echo $base_path.strtolower(str_replace(" ", "_",$data['title'])).'.pdf';
		// die();
		return $pdf->Output($base_path.strtolower(str_replace(" ", "_",$data['title'])).'.pdf', 'F');
	}
}
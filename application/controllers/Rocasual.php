<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rocasual extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
		if($this->session->userdata('site') == ''){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan pilih site bisnis.','danger'));
			redirect(base_url());	
		}
	}

	public function department(){
		$this->load->model(array('site_model'));
		$data['_TITLE_'] 		= 'Daftar Departemen';
		$data['_PAGE_'] 		= 'rocasual/department';
		$data['_MENU_PARENT_'] 	= 'rocasual';
		$data['_MENU_'] 		= 'rodepartment';
		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address'));
		$this->view($data);
	}

	public function department_ajax(){
		$this->load->model(array('department_casual_model'));
		$column_index = $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];		
		$params['site_id']		= $this->session->userdata('site');

		$params['name']			= $_POST['columns'][1]['search']['value'];

		$list 	= $this->department_casual_model->gets($params);
		$total 	= $this->department_casual_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 					= $i;
			$result['name']		= $item->name;
			
			$result['action'] 				=
				'<a class="btn-sm btn-success btn-block" href="'.base_url("rocasual/department_form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("rocasual/department_delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function department_form($department_id = FALSE)
	{
		$this->load->model(array('department_casual_model'));

		$data['id'] 		= '';
		$data['name']		= '';
		$data['site_id'] 	=  $this->session->userdata('site');
		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['name'] 		= $this->input->post('name');
			
			$this->form_validation->set_rules('name', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$save_id	 	= $this->department_casual_model->save($data);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('rocasual/department'));
		}

		if ($department_id)
		{
			$data = (array) $this->department_casual_model->get(array('id' => $department_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('rocasual/department'));
			}
		}

		$data['_TITLE_'] 		= 'Kota';
		$data['_PAGE_'] 		= 'rocasual/department_form';
		$data['_MENU_PARENT_'] 	= 'rocasual';
		$data['_MENU_'] 		= 'rodepartment';
		return $this->view($data);
	}

	public function department_delete($department_id = false)
	{
		$this->load->model('department_casual_model');
		if ($department_id)
		{
			$insert = array('id' => $department_id, 'is_active' => 0);
			$result = $this->department_casual_model->save($insert);
			if ($result) {
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
				redirect(base_url('rocasual/department'));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
				redirect(base_url('rocasual/department'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('rocasual/department'));
		}
	}

	public function employee_form($employee_id=FALSE)
	{
		$this->load->model(array('employee_casual_model'));

		$data['id']					= '';
		$data['employee_number']	= '';
		$data['pin_finger']			= '';
		$data['id_card']			= '';
		$data['full_name']			= '';
		$data['phone_number']		= '';
		$data['bank_name']			= '';
		$data['bank_account']		= '';
		$data['bank_account_name']	= '';
		$data['address']			= '';
		$data['site_id']			= $this->session->userdata('site');

		if($this->input->post()){
			$data['id'] 				= $this->input->post('id');
			$data['employee_number']	= $this->input->post('employee_number');
			$data['pin_finger']			= $this->input->post('employee_number');
			$data['id_card']			= $this->input->post('id_card');
			$data['full_name']			= $this->input->post('full_name');
			$data['phone_number']		= $this->input->post('phone_number');
			$data['bank_name']			= $this->input->post('bank_name');
			$data['bank_account']		= $this->input->post('bank_account');
			$data['bank_account_name']	= $this->input->post('bank_account_name');
			$data['address']			= $this->input->post('address');
			if($data['id'] == ''){
				$data['status']			= 'On Call'; 
			}
			
			$this->form_validation->set_rules('employee_number', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$save_id	 	= $this->employee_casual_model->save($data);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('rocasual/list'));
		}

		if ($employee_id)
		{
			$data = (array) $this->employee_casual_model->get(array('id' => $employee_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('rocasual/list'));
			}
		}

		$data['_TITLE_'] 		= 'Form Karyawan';
		$data['_PAGE_'] 		= 'rocasual/employee_form';
		$data['_MENU_PARENT_'] 	= 'rocasual';
		$data['_MENU_'] 		= 'rocasual';
		return $this->view($data);
	}

	public function list(){
		$this->load->model(array('site_model','employee_casual_model', 'department_casual_model', 'position_model'));
		$data['_TITLE_'] 		= 'Karyawan';
		$data['_PAGE_'] 		= 'rocasual/list';
		$data['_MENU_PARENT_'] 	= 'rocasual';
		$data['_MENU_'] 		= 'rocasual';
		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address'));
		$data['total'] 			= $this->employee_casual_model->gets(array(), TRUE);
		$data['list_department']= $this->department_casual_model->gets(array('columns' => 'A.id, A.name', 'site_id' => $this->session->userdata('site')));
		$data['list_position']	= $this->position_model->gets(array('columns' => 'A.id, A.name'));
		$this->view($data);
	}

	public function list_ajax(){
		$this->load->model(array('employee_casual_model'));
		$column_index = $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.pin_finger, A.full_name, A.phone_number, A.bank_name, A.bank_account, A.bank_account_name, A.status, B.name AS department_name, C.name AS position_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];		
		$params['site_id']		=  $this->session->userdata('site');

		$params['employee_number']		= $_POST['columns'][1]['search']['value'];
		$params['pin_finger']			= $_POST['columns'][2]['search']['value'];
		$params['id_card']				= $_POST['columns'][3]['search']['value'];
		$params['full_name']			= $_POST['columns'][4]['search']['value'];
		$params['phone_number']			= $_POST['columns'][5]['search']['value'];
		$params['bank_name']			= $_POST['columns'][6]['search']['value'];
		$params['bank_account']			= $_POST['columns'][7]['search']['value'];
		$params['bank_account_name']	= $_POST['columns'][8]['search']['value'];
		$params['position_name']		= $_POST['columns'][9]['search']['value'];
		$params['department_name']		= $_POST['columns'][10]['search']['value'];
		
		$list 	= $this->employee_casual_model->gets($params);
		$total 	= $this->employee_casual_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['employee_number']		= $item->employee_number;
			$result['pin_finger']			= $item->pin_finger;
			$result['id_card']				= $item->id_card;
			$result['full_name']			= $item->full_name;
			$result['phone_number']			= $item->phone_number;
			$result['bank_name']			= $item->bank_name;
			$result['bank_account']			= $item->bank_account;
			$result['bank_account_name']	= $item->bank_account_name;
			$result['position_name']		= $item->position_name;
			$result['department_name']		= $item->department_name;
			$result['status']				= $item->status;
			
			$result['action'] 				=
				'<a class="btn-sm btn-success btn-block" href="'.base_url("rocasual/employee_form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("rocasual/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function delete($employee_id = false)
	{
		$this->load->model('employee_casual_model');
		if ($employee_id)
		{
			$insert = array('id' => $employee_id, 'is_active' => 0);
			$result = $this->employee_casual_model->save($insert);
			if ($result) {
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
				redirect(base_url('rocasual/list/'.$type));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
				redirect(base_url('rocasual/list/'.$type));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('rocasual/list/direct'));
		}
	}

	public function export()
	{
		$this->load->model(array('employee_casual_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = "Data Karyawan";
		$title = 'daftar_karyawan';
		$files = glob(FCPATH."files/".$title."/*");

		$params = $this->input->get();

		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.pin_finger, A.full_name, A.phone_number, A.bank_name, A.bank_account, A.bank_account_name, A.status, B.name AS department_name, C.name AS position_name, A.address";

		$params['orderby']		= "A.full_name";
		$params['order']		= "ASC";
		$params['site_id']			=  $this->session->userdata('site');


		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "No");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nomor Karyawan");
		$excel->getActiveSheet()->setCellValue("C".$i, "PIN Finger");
		$excel->getActiveSheet()->setCellValue("D".$i, "Nomor KTP");
		$excel->getActiveSheet()->setCellValue("E".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("F".$i, "Nomor Telp/Wa");
		$excel->getActiveSheet()->setCellValue("G".$i, "Nama Bank");
		$excel->getActiveSheet()->setCellValue("H".$i, "No Rekening");
		$excel->getActiveSheet()->setCellValue("I".$i, "Atas Nama");
		$excel->getActiveSheet()->setCellValue("J".$i, "Alamat");
		$excel->getActiveSheet()->setCellValue("K".$i, "Jabatan");
		$excel->getActiveSheet()->setCellValue("L".$i, "Departemen");

		$list_employee = $this->employee_casual_model->gets($params);
		$i=2;
		$no = 1;
		foreach ($list_employee as $item) {
			$excel->getActiveSheet()->setCellValue("A".$i, $no);
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->employee_number);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->pin_finger);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->pin_finger);
			$excel->getActiveSheet()->setCellValueExplicit("D".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValue("E".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("F".$i, $item->phone_number);
			$excel->getActiveSheet()->setCellValueExplicit("G".$i, $item->bank_name);
			$excel->getActiveSheet()->setCellValueExplicit("H".$i, $item->bank_account);
			$excel->getActiveSheet()->setCellValueExplicit("I".$i, $item->bank_account_name);
			$excel->getActiveSheet()->setCellValueExplicit("J".$i, $item->address);
			$excel->getActiveSheet()->setCellValueExplicit("K".$i, $item->position_name);
			$excel->getActiveSheet()->setCellValueExplicit("L".$i, $item->department_name);
			$no++;
			$i++;
		}
		$excel->getActiveSheet()->setTitle('Daftar Karyawan');
		$sheet = $excel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells( true );
		foreach( $cellIterator as $cell ) {
			$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		}
		$excel->createSheet();
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function import(){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('employee_casual_model', 'department_casual_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {
				$data['id']					= '';
				$data['employee_number']	= replace_null($sheet->getCell('B'.$row)->getValue());
				$data['pin_finger']			= replace_null($sheet->getCell('C'.$row)->getValue());
				$data['id_card']			= replace_null($sheet->getCell('D'.$row)->getValue());
				$data['full_name']			= replace_null($sheet->getCell('E'.$row)->getValue());
				$data['phone_number']		= replace_null($sheet->getCell('F'.$row)->getValue());
				$data['bank_name']			= replace_null($sheet->getCell('G'.$row)->getValue());
				$data['bank_account']		= replace_null($sheet->getCell('H'.$row)->getValue());
				$data['bank_account_name']	= replace_null($sheet->getCell('i'.$row)->getValue());
				$data['address']			= replace_null($sheet->getCell('J'.$row)->getValue());
				$department_name			= replace_null($sheet->getCell('L'.$row)->getValue());
				$data['site_id']			= $this->session->userdata('site');
				if($data['employee_number'] == ''){
					continue;
				}
				$department                 = $this->department_casual_model->get(array('name' => $department_name, 'columns' => 'A.id'));
				if($department){
				    $data['department_casual_id'] = $department->id;
				}
				// print_prev($department_name);
				// print_prev($department);
				// die();
				$employee = $this->employee_casual_model->get(array('employee_number' => $data['employee_number'], 'site_id' => $data['site_id'], 'columns' => 'A.id'));
				if($employee){
					$data['id'] 	= $employee->id;
				}
				$save_id = $this->employee_casual_model->save($data);
			}

			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
					redirect(base_url('rocasual/list'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
					redirect(base_url('rocasual/list'));
		}
	}

	public function save_status()
	{
		if($this->input->post()){
			$this->load->model(array('employee_casual_model'));
		
			$id 					= $this->input->post('id');
			$status 				= $this->input->post('status');
			$position_id 			= $this->input->post('position_id');
			$department_casual_id 	= $this->input->post('department_casual_id');
			foreach ($id as $employee_id) {
				$data['id']			= $employee_id;
				$data['status']		= $status;
				if($position_id != ""){
					$data['position_id']			= $position_id;
				}
				if($department_casual_id != ""){
					$data['department_casual_id']	= $department_casual_id;
				}

				$result =  $this->employee_casual_model->save($data);
				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger'));
					redirect(base_url('rocasual/list'));
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
			redirect(base_url('rocasual/list'));
		}
	}

	public function schedule($status = 'oncall'){
		$this->load->model(array('site_model', 'employee_casual_model', 'department_casual_model'));
		$data['_TITLE_'] 		= 'Karyawan';
		$data['_PAGE_'] 		= 'rocasual/schedule';
		$data['_MENU_PARENT_'] 	= 'rocasual';
		$is_status 				= 'Regular';
		$menu 					= 'roregular';
		if($status == 'oncall'){
			$menu 				= 'rooncall';
			$is_status 			= 'On Call';
		}

		$data['date_start'] 		= date('Y-m-01');
		$data['date_finish'] 		= date('Y-m-d');
		if($this->input->get()){
			$data['date_start']		= $this->input->get('date_start');
			$data['date_finish']	= $this->input->get('date_finish');
		}

		$data['_MENU_'] 			= $menu;
		$data['status'] 			= $status;
		$data['site'] 				= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address'));
		$data['total'] 				= $this->employee_casual_model->gets(array('is_status' => $is_status), TRUE);
		$data['list_department'] 	= $this->department_casual_model->gets(array('site_id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name'));
		$this->view($data);
	}

	public function schedule_ajax($status = 'oncall', $date_start = FALSE, $date_finish = FALSE){
		$this->load->model(array('employee_casual_schedule_model'));
		$is_status 				= 'Regular';
		if($status == 'oncall'){
			$is_status 			= 'On Call';
		}

		$column_index = $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, B.employee_number, B.id_card, B.pin_finger, B.full_name, A.date AS date_raw, DATE_FORMAT(A.date, '%d/%m/%Y') AS date, C.name AS department, A.department_id";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];		
		$params['site_id']		= $this->session->userdata('site');
		$params['is_status']	= $is_status;
		$params['date_start']	= $date_start;
		$params['date_finish']	= $date_finish;

		$params['employee_number']		= $_POST['columns'][1]['search']['value'];
		$params['pin_finger']			= $_POST['columns'][2]['search']['value'];
		$params['id_card']				= $_POST['columns'][3]['search']['value'];
		$params['full_name']			= $_POST['columns'][4]['search']['value'];
		$params['date']					= $_POST['columns'][5]['search']['value'];
		$params['department']			= $_POST['columns'][6]['search']['value'];
		
		$list 	= $this->employee_casual_schedule_model->gets($params);
		$total 	= $this->employee_casual_schedule_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['employee_number']		= $item->employee_number;
			$result['pin_finger']			= $item->pin_finger;
			$result['id_card']				= $item->id_card;
			$result['full_name']			= $item->full_name;
			$result['date']					= $item->date;
			$result['department']			= $item->department;			
			$result['action'] 				=
				'<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("rocasual/schedule_delete/".$status.'/'.$date_start.'/'.$date_finish.'/'.$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function schedule_save($status = 'oncall')
	{
		if($this->input->post()){
			$this->load->model(array('employee_casual_schedule_model'));
		
			$id 			= $this->input->post('id');
			$date_start 	= $this->input->post('date_start');
			$date_finish 	= $this->input->post('date_finish');
			$department_id 	= $this->input->post('template_id');

			$start_time = strtotime($date_start);
			$end_time = strtotime($date_finish);

			foreach ($id as $employee_id) {
				for ( $i = $start_time; $i <= $end_time; $i = $i + 86400 ) {
					$current_date 			= date( 'Y-m-d', $i );
					$data['employee_id'] 	= $employee_id;
					$data['date'] 			= $current_date;
					$data['department_id'] 	= $department_id;
					$data['id'] 			= '';
					$data['is_active'] 		= 1;
					$schedule 				= $this->employee_casual_schedule_model->get(array('employee_id' => $data['employee_id'], 'date' => $data['date'], 'department_id' => $data['department_id'], 'columns' =>'A.id'));
					if($schedule){
						$data['id'] 		= $schedule->id;
					}
					$result =  $this->employee_casual_schedule_model->save($data);
					if (!$result) {
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger'));
						redirect(base_url('rocasual/schedule/'.$status.'?date_start='.$date_start.'&date_finish='.$date_finish));
					}
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
			redirect(base_url('rocasual/schedule/'.$status.'?date_start='.$date_start.'&date_finish='.$date_finish));
		}
	}

	public function schedule_delete($status = 'oncall', $date_start = FALSE, $date_finish = FALSE, $schedule_id = FALSE)
	{
		if ($schedule_id)
		{
			$this->load->model(array('employee_casual_schedule_model'));
			$result = $this->employee_casual_schedule_model->save(array('id' => $schedule_id, 'is_active' => 0));
			if ($result) {
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
				redirect(base_url('rocasual/schedule/'.$status.'?date_start='.$date_start.'&date_finish='.$date_finish));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
				redirect(base_url('rocasual/schedule/'.$status.'?date_start='.$date_start.'&date_finish='.$date_finish));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('rocasual/schedule/'.$status.'?date_start='.$date_start.'&date_finish='.$date_finish));		
		}
	}

	public function schedule_delete_all($status = 'oncall')
	{
		if($this->input->post()){
			$this->load->model(array('employee_casual_schedule_model'));
		
			$ids 			= $this->input->post('id');
			foreach ($ids as $id) {
				$result = $this->employee_casual_schedule_model->save(array('id' => $id, 'is_active' => 0));
				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger'));
					redirect(base_url('rocasual/schedule/'.$status.'/'.$date_start.'/'.$date_finish));
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
			redirect(base_url('rocasual/schedule/'.$status.'?date_start='.$date_start.'&date_finish='.$date_finish));
		}
	}
	
	public function employee_ajax($status = 'oncall'){
		$this->load->model(array('employee_casual_model'));
		$is_status 				= 'Regular';
		if($status == 'oncall'){
			$is_status 			= 'On Call';
		}

		$column_index = $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.pin_finger, A.full_name, A.phone_number, A.bank_name, A.bank_account, A.bank_account_name, A.status";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];		
		$params['site_id']		= $this->session->userdata('site');
		$params['is_status']	= $is_status;

		$params['employee_number']		= $_POST['columns'][1]['search']['value'];
		$params['pin_finger']			= $_POST['columns'][2]['search']['value'];
		$params['id_card']				= $_POST['columns'][3]['search']['value'];
		$params['full_name']			= $_POST['columns'][4]['search']['value'];
		$params['phone_number']			= $_POST['columns'][5]['search']['value'];
		$params['bank_name']			= $_POST['columns'][6]['search']['value'];
		$params['bank_account']			= $_POST['columns'][7]['search']['value'];
		$params['bank_account_name']	= $_POST['columns'][8]['search']['value'];
		$params['status']				= $_POST['columns'][9]['search']['value'];
		
		$list 	= $this->employee_casual_model->gets($params);
		$total 	= $this->employee_casual_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['employee_number']		= $item->employee_number;
			$result['pin_finger']			= $item->pin_finger;
			$result['id_card']				= $item->id_card;
			$result['full_name']			= $item->full_name;
			$result['phone_number']			= $item->phone_number;
			$result['bank_name']			= $item->bank_name;
			$result['bank_account']			= $item->bank_account;
			$result['bank_account_name']	= $item->bank_account_name;
			$result['status']				= $item->status;
			
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("rocasual/preview/".$item->id).'">Lihat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function attendance($date_start = FALSE, $date_finish = FALSE){
		$this->load->model(array('site_model','employee_casual_model', 'payroll_config_casual_model', 'department_casual_model'));
		$data['_TITLE_'] 		= 'Absensi Karyawan';
		$data['_PAGE_'] 		= 'rocasual/attendance';
		$data['_MENU_PARENT_'] 	= 'rocasual';
		$data['_MENU_'] 		= 'roattendance';
		$data['list_department']= $this->department_casual_model->gets(array('site_id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name'));
		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address, A.attendance_machine'));
		$data['total'] 			= $this->employee_casual_model->gets(array(), TRUE);

		$data['date_start'] 		= date('01/m/Y');
		$data['date_finish'] 		= date('d/m/Y');

		if($this->input->post()){
			$data['date_start']		= $this->input->post('date_start');
			$data['date_finish']	= $this->input->post('date_finish');
		}

		$data['pdate_start'] 		= date("Y-m-d", strtotime(str_replace('/', '-', $data['date_start'])));
		$data['pdate_finish'] 		= date("Y-m-d", strtotime(str_replace('/', '-', $data['date_finish'])));

		if($date_start){
			$data['pdate_start'] 	= $date_start;
			$data['date_start'] 	= date("d/m/Y", strtotime($date_start));
		}
		if($date_finish){
			$data['pdate_finish'] 	= $date_finish;
			$data['date_finish'] 	= date("d/m/Y", strtotime($date_finish));
		}

		$this->view($data);
	}

	public function attendance_ajax($date_start = FALSE, $date_finish = FALSE){
		$this->load->model(array('employee_casual_attendance_model'));
		$column_index = $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, B.employee_number, B.pin_finger, B.id_card, B.full_name, B.status AS status_employee, DATE_FORMAT(A.date, '%d/%m/%Y') AS date, DATE_FORMAT(A.date_in, '%d/%m/%Y %H:%i:%s') AS date_in, DATE_FORMAT(A.date_out, '%d/%m/%Y %H:%i:%s') AS date_out, A.overtime, C.name AS department, A.status AS status_attendance";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];		
		$params['site_id']		= $this->session->userdata('site');
		$params['date_start']	= $date_start;
		$params['date_finish']	= $date_finish;

		$params['pin_finger']			= $_POST['columns'][1]['search']['value'];
		$params['full_name']			= $_POST['columns'][2]['search']['value'];
		$params['status_employee']		= $_POST['columns'][3]['search']['value'];
		$params['department']			= $_POST['columns'][4]['search']['value'];
		$params['overtime']				= $_POST['columns'][5]['search']['value'];
		$params['date']					= $_POST['columns'][6]['search']['value'];
		$params['status_attendance']	= $_POST['columns'][7]['search']['value'];
		
		$list 	= $this->employee_casual_attendance_model->gets($params);
		$total 	= $this->employee_casual_attendance_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			
			$result['pin_finger']			= $item->pin_finger;
			$result['full_name']			= $item->full_name;
			$result['status_employee']		= $item->status_employee;
			$result['department']			= $item->department;
			$result['overtime']				= $item->overtime;
			$result['date']					= $item->date;
			$result['date_check']			= '<strong>IN :</strong> '.$item->date_in.'<br><strong>OUT :</strong> '.$item->date_out;
			$result['action'] 				= '';
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['status_attendance']	= '<p class="text-danger">'.$item->status_attendance.'<p>';
			if($item->status_attendance ==  'Disetujui'){
				$result['status_attendance']		= '<p class="text-success">'.$item->status_attendance.'</p>';
			}
			if($item->status_attendance ==  'Pengajuan'){
				$result['status_attendance']		= '<p class="text-warning">'.$item->status_attendance.'</p>';
			}
			if($item->status_attendance == 'Draft' || $item->status_attendance == 'Ditolak'){
				$result['action'] 				=
					'<a class="btn-sm btn-success btn-action btn-block"  style="cursor:pointer;" href="'. base_url("rocasual/attendance_form/".$date_start."/".$date_finish."/".$item->id).'">Ubah</a>
					<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("rocasual/attendance_delete/".$date_start."/".$date_finish."/".$item->id).'">Hapus</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function attendance_form($date_start = FALSE, $date_finish = FALSE, $attendance_id = FALSE)
	{
		$this->load->model(array('employee_casual_attendance_model', 'employee_casual_model'));

		$data['id'] 		= '';
		$data['employee_id']= '';
		$data['date'] 		= date('d/m/Y');
		$data['date_in'] 	= date('d/m/Y');
		$data['date_out'] 	= date('d/m/Y');
		$data['check_in'] 	= '08:00:00';
		$data['check_out'] 	= '17:00:00';
		if($this->input->post()){
			$data['id'] 			= $this->input->post('id');
			$data['employee_id'] 	= $this->input->post('employee_id');
			$data['date'] 			= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date'))));
			$data['check_in'] 		= $this->input->post('check_in');
			$data['check_out'] 		= $this->input->post('check_out');
			$data['date_in'] 		= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_in')))).' '.$this->input->post('check_in');
			$data['date_out'] 		= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_out')))).' '.$this->input->post('check_out');
			$data['is_active'] 		= 1;

			$this->form_validation->set_rules('employee_id', '', 'required');
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$save_id	 	= $this->employee_casual_attendance_model->save($data);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('rocasual/attendance/'.$date_start.'/'.$date_finish));
		}

		if ($attendance_id)
		{
			$data = (array) $this->employee_casual_attendance_model->get(array('id' => $attendance_id, 'columns' => 'A.id, A.employee_id, A.date, A.date_in, A.date_out, A.check_in, A.check_out'));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('rocasual/attendance/'.$date_start.'/'.$date_finish));
			}
			$data['date'] 		= date("d/m/Y", strtotime(str_replace('/', '-', $data['date'])));
			$data['date_in'] 	= date("d/m/Y", strtotime(str_replace('/', '-', $data['date_in'])));
			$data['date_out'] 	= date("d/m/Y", strtotime(str_replace('/', '-', $data['date_out'])));
		}

		$data['date_start'] = $date_start;
		$data['date_finish']= $date_finish;
		$params['columns'] 		= "A.id, A.employee_number, A.full_name";
		$params['site_id']		=  $this->session->userdata('site');		
		$data['list_employee'] 	= $this->employee_casual_model->gets($params);
		$data['_TITLE_'] 		= 'Absensi Karyawan';
		$data['_PAGE_'] 		= 'rocasual/attendance_form';
		$data['_MENU_PARENT_'] 	= 'rocasual';
		$data['_MENU_'] 		= 'roattendance';
		return $this->view($data);
	}

	public function attendance_delete($date_start = FALSE, $date_finish = FALSE, $attendance_id = FALSE)
	{
		if ($attendance_id)
		{
			$this->load->model(array('employee_casual_attendance_model'));
			$result = $this->employee_casual_attendance_model->save(array('id' => $attendance_id, 'is_active' => 0));
			if ($result) {
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
				redirect(base_url('rocasual/attendance/'.$date_start.'/'.$date_finish));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
				redirect(base_url('rocasual/attendance/'.$date_start.'/'.$date_finish));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('rocasual/attendance/'.$date_start.'/'.$date_finish));
		}
	}

	public function attendance_import($date_start = FALSE, $date_finish = FALSE){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('employee_casual_attendance_model', 'employee_casual_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 0; $row <= $rend; $row++) {

				$pin_finger	= replace_null($sheet->getCell('A'.$row)->getValue());
				if($pin_finger == ''){
				    continue;
				}
				$employee = $this->employee_casual_model->get(array('pin_finger' => $pin_finger, 'site_id' => $this->session->userdata('site'), 'columns' => 'A.id'));
				if($employee){
					$date = $sheet->getCell('D'.$row)->getValue();
					$data['id'] 			= '';
					$data['date'] 			= '';
					$data['employee_id']	= $employee->id;

					$data['check_in'] 	=  $sheet->getCell('E'.$row)->getValue();
					$data['check_out'] 	=  $sheet->getCell('F'.$row)->getValue();

					if($date != ''){
						$data['date'] 		= substr($date, 6, 4).'-'.substr($date, 3, 2).'-'.substr($date, 0, 2);
						$data['date_in'] 	= $data['date'].' '.$data['check_in'];
						if($data['check_out'] != ''){
							$data['date_out'] 	= $data['date'].' '.$data['check_out'];
							if((int) substr($data['check_in'], 0, 2) > (int) substr($data['check_out'], 0, 2)){
								  $date_out = strtotime($data['date']);
								  $date_out = strtotime("+1 day", $date_out);
								  $data['date_out'] = date('Y-m-d', $date_out).' '.$data['check_out'];
							}
						}
					}
					
					$attendance = $this->employee_casual_attendance_model->get($data);
					if($attendance){
						$data['id'] 		= $attendance->id;					
					}
					$save_id = $this->employee_casual_attendance_model->save($data);
				}

			}

			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			redirect(base_url('rocasual/attendance/'.$date_start.'/'.$date_finish));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
			redirect(base_url('rocasual/attendance/'.$date_start.'/'.$date_finish));
		}
	}

	public function attendance_syncronize(){

		$this->load->model(array('employee_casual_attendance_model', 'employee_casual_model', 'site_model'));
		if($this->input->post()){
			$params['api_token'] 	= 'aVwAQh5GSsaO8EyPYSnTkfxOzNCSMbhs1Sk3IQpyC94PB4IjgFmH1pgYxsa9';
			$params['id_mesin'] 	= $this->input->post('attendance_machine');
			$params['tgl_mulai'] 	= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_start'))));
			$params['tgl_akhir'] 	= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_finish'))));

			$this->site_model->save(array('id' => $this->session->userdata('site'), 'attendance_machine' => $params['id_mesin']));

			$curl = curl_init();
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			
			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://presensi.shelterapp.co.id/api/getRekapPresensi',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS => json_encode($params),
			  CURLOPT_HTTPHEADER => array(
			    'Content-Type: application/json'
			  ),
			));
			$response = curl_exec($curl);
			curl_close($curl);
			$response = json_decode($response);
			if(isset($response->data)){
				foreach($response->data as $item)
				{
					$employee = $this->employee_casual_model->get(array('employee_number' => $item->id_pegawai, 'columns' => 'A.id'));
					if($employee){
						$data['id'] 			= '';
						$data['employee_id']	= $employee->id;
						$data['date']			= substr($item->absen_in, 0,10);
						$data['check_in']		= substr($item->absen_in, 11,8);
						$data['check_out']		= substr($item->absen_out, 11,8);
						$data['date_in']		= $item->absen_in;
						$data['date_out']		= $item->absen_out;
						$attendance = $this->employee_casual_attendance_model->get($data);
						if($attendance){
							$data['id'] 		= $attendance->id;					
						}
						$save_id = $this->employee_casual_attendance_model->save($data);
					}
				}

				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disinkronisasi.','success'));
				redirect(base_url('rocasual/attendance/'.$params['tgl_mulai'].'/'.$params['tgl_akhir']));
			}else{
				if(isset($response->message)){
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> '.$response->message.'.','danger'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Sinkronisasi absensi gagal dilakukan.','danger'));
				}
				redirect(base_url('rocasual/attendance/'.$params['tgl_mulai'].'/'.$params['tgl_akhir']));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Sinkronisasi absensi gagal dilakukan.','danger'));
			redirect(base_url('rocasual/attendance/'.$params['tgl_mulai'].'/'.$params['tgl_akhir']));
		}
	}

	public function attendance_status($date_start = FALSE, $date_finish = FALSE, $status = 'oncall')
	{
		if($this->input->post()){
			$this->load->model(array('employee_casual_attendance_model'));
		
			$data['department_id'] 	= $this->input->post('department');
			$data['overtime']		= $this->input->post('overtime');
			$data['status'] 		= $this->input->post('status');
			foreach ($this->input->post('id') AS $attendance_id) {
				$data['id'] = $attendance_id;
				$result 	=  $this->employee_casual_attendance_model->save($data);
				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger'));
					redirect(base_url('rocasual/attendance/'.$date_start.'/'.$date_finish));
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
			redirect(base_url('rocasual/attendance/'.$date_start.'/'.$date_finish));
		}
	}

	public function attendance_export()
	{
		$this->load->model(array('employee_casual_attendance_model', 'site_model', 'company_model', 'department_casual_model'));
		$params 	= $this->input->get();
		$params['columns'] 		= "B.pin_finger, B.full_name, B.status AS status_employee, A.date_in, A.date_out, C.name AS department";

		$params['site_id']	= $this->session->userdata('site');
		$list 				= $this->employee_casual_attendance_model->gets($params);
		$site 				= $this->site_model->get_site(array('id' => $params['site_id'], 'columns' => 'A.id, A.company_id, A.name, A.attendance_machine'));
		$company 			= $this->company_model->get(array('id' => $site->company_id, 'columns' => 'A.name'));
		$periode 			= date("d-m-Y", strtotime($params['date_start'])).' s/d '.date("d-m-Y", strtotime($params['date_finish']));
		
		$data['content'] 	= ''; 
		$data['title'] 		=  preg_replace("/[^A-Za-z0-9]/", "", strtolower($site->name));
		$data['basepath'] 	= FCPATH.'files/absensi/';
		$data['footer']		= '        Tanggal Cetak : '.date('d-m-Y H:i:s').'        Oleh : '.$this->session->userdata('name');
		$data['header'] 	= '<p>&nbsp;</p>
		<table cellspacing="0" cellpadding="1" border="1"><tr><td style="text-align: center;">LAPORAN SCAN LOG</td></tr></table>
		<table cellspacing="1" cellpadding="1" style="text-align: center;font-size:10px;"><tr><td>Nama Perusahaan : '.$company->name.'</td><td>Tanggal Periode : '.$periode.'</td></tr></table><br><br>
		<table cellspacing="0" cellpadding="1" border="1" style="text-align: center;font-size:10px;"><tr><td width="70px">PIN</td><td width="200px">Nama</td><td width="150px">Tanggal</td><td width="70px">Jam</td><td width="70px">Status</td><td width="70px">Verifikasi</td><td width="80px">SN Mesin</td></tr></table><br>';
		$list_department  = $this->department_casual_model->gets(array('site_id' => $params['site_id'], 'columns' => 'A.id, A.name'));
		foreach($list_department as $department){
			$params['department_id'] = $department->id;
			$params['order_by']		 = 'B.pin_finger, A.date';
			$params['order']		 = 'ASC';
			$employee_attendance = $this->employee_casual_attendance_model->gets($params);
			if($employee_attendance){
				$data['content'] .=  '<table cellspacing="1" cellpadding="1" style="text-align: left;font-size:10px;"><tr><td>&nbsp; Jabatan / Departemen / Unit Kerja : On Call / '.$department->name.' / '.$site->name.'</td></tr></table>';
				$data['content'] .=  '<table cellspacing="0" cellpadding="2" border="1" style="font-size:10px;">';
				foreach($employee_attendance as $attendance){
					$date_in = get_day(date('l', strtotime($attendance->date_in))).', '.date('d-m-Y', strtotime($attendance->date_in));
					$clock_in = substr($attendance->date_in, 11, 8);
					$date_out = get_day(date('l', strtotime($attendance->date_out))).', '.date('d-m-Y', strtotime($attendance->date_out));
					$clock_out = substr($attendance->date_out, 11, 8);
					$data['content'] .= '<tr><td width="70px">'.$attendance->pin_finger.'</td><td width="200px">'.$attendance->full_name.'</td><td width="150px">'.$date_in.'</td><td width="70px">'.$clock_in.'</td><td width="70px">Clock in</td><td width="70px">FP</td><td width="80px">'.$site->attendance_machine.'</td></tr>
					<tr><td width="70px">'.$attendance->pin_finger.'</td><td width="200px">'.$attendance->full_name.'</td><td width="150px">'.$date_out.'</td><td width="70px">'.$clock_out.'</td><td width="70px">Clock Out</td><td width="70px">FP</td><td width="80px">'.$site->attendance_machine.'</td></tr>';
				}
				$data['content'] .= '</table><br>';
			}
		}

		$this->generate_PDF($data);
		redirect(base_url('files/absensi/'.$data['title'].'.pdf'));
	}

	private function generate_PDF($data = array())
	{	
		$this->load->library("Pdf");
		ob_start();
		$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->setHeaderData($ln='', $lw=0, $ht='', $data['header'], $tc=array(0,0,0), $lc=array(0,0,0));
		$pdf->setCustomFooterText($data['footer']);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shelter');
		$pdf->SetTitle($data['title']);
		$pdf->SetSubject($data['title']);
		$pdf->SetKeywords('Shelter');

		$pdf->setPrintHeader(true);
		$pdf->setPrintFooter(true);

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(5, 27, 5);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$pdf->SetFont('times', '', 10);
		$pdf->AddPage();
		$pdf->writeHTML($data['content'], true, false, true, false, '');

		$base_path  = $data['basepath'];
		if(!is_dir($base_path)){
			mkdir($base_path, 0777, true);
		}
		return $pdf->Output($base_path.strtolower(str_replace(" ", "_",$data['title'])).'.pdf', 'F');
	}

	public function payroll_export($type = 'normal')
	{
		$this->load->model(array('site_model', 'company_model', 'employee_casual_attendance_model', 'department_casual_model', 'employee_casual_model', 'payroll_config_casual_model'));

		$overtime = 'Tidak';
		if($type == 'overtime'){
			$overtime = 'Ya';
		}

		$site_id		= $this->session->userdata('site');
		// $date_start		= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_start'))));
		// $date_finish	= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_finish'))));
		
		$basic_salary	= 0;
		$mf				= 0;
		$ppn			= 0;
		$pph			= 0;
		$config 				= $this->payroll_config_casual_model->get(array('site_id' => $this->session->userdata('site')));
		if($config){
			$basic_salary 	= $config->basic_salary;
			$management_fee = $config->management_fee;
			$ppn 			= $config->ppn;
			$pph 			= $config->pph;
		}

		$params = $this->input->get();

		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$site = $this->site_model->get(array('id' => $site_id, 'columns' => 'A.name, A.company_id'));
		if(!$site){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Site tidak ditemukan','danger'));
			redirect(base_url('rocasual/attendance'));
		}

		$title = "payroll_casual";
		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
			->setLastModifiedBy($this->session->userdata('name'))
			->setTitle($title)
			->setSubject($title)
			->setDescription($title)
			->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);
		
		$i=1;
		$company_name 	= '';
		$company 		= $this->company_model->get(array('id' => $site->company_id, 'columns' => 'A.name'));
		if($company){
			$company_name = $company->name; 
		}

		$params['site_id']  = $site_id;
		$params['columns']  = 'MIN(A.date) AS periode_start, MAX(A.date) AS periode_end';
		$periode = $this->employee_casual_attendance_model->gets($params);
		if(!$periode){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan','danger'));
			redirect(base_url('rocasual/attendance'));
		}

		$periode_start 	= $periode[0]->periode_start;
		$periode_end	= $periode[0]->periode_end;

		$excel->getActiveSheet()->setCellValue("A".$i, "Rekap Gaji ( Casual On Call ) ".$company_name." site : ".$site->name." SURABAYA Periode ".substr($periode_start, 8, 2).' - '.substr($periode_end, 8, 2).' '.get_month(substr($periode_end, 5, 2)).' '.substr($periode_end, 0, 4));
		$excel->getActiveSheet()->mergeCells('A1:AE1');
		$excel->getActiveSheet()->getStyle('A1:AE1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('A1:AE1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$excel->getActiveSheet()->getStyle('A1:AE1')->getFont()->setBold( true );
		$i++;
		$list_department  = $this->department_casual_model->gets(array('site_id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name'));
		foreach($list_department as $department){
			$params['department_id']  = $department->id;
			$params['columns']  = 'A.employee_id';
			$params['groupby']  = 'A.employee_id';
			$employee_attendance = $this->employee_casual_attendance_model->gets($params);
			if($employee_attendance){
				$excel->getActiveSheet()->setCellValue("A".$i, "No");
				$excel->getActiveSheet()->setCellValue("B".$i, "Nomor");
				$excel->getActiveSheet()->setCellValue("C".$i, "Nama");
				$excel->getActiveSheet()->setCellValue("D".$i, "Nomor Rekening");
				$excel->getActiveSheet()->setCellValue("E".$i, "Nama Bank");
				$excel->getActiveSheet()->setCellValue("F".$i, "Nama Pemilik Rekening");
				$excel->getActiveSheet()->setCellValue("G".$i, "Bagian");
				$alpha = 'H';
				for ($j = strtotime($periode_start); $j <= strtotime($periode_end); $j = $j + 86400 ) {
					$excel->getActiveSheet()->setCellValue($alpha.$i, date('d', $j));
					$alpha++;
				}
				$excel->getActiveSheet()->setCellValue($alpha.$i, "Total Shift");
				$alpha++;
				$excel->getActiveSheet()->setCellValue($alpha.$i, "Gaji Pokok");
				$alpha++;
				$excel->getActiveSheet()->setCellValue($alpha.$i, "Total Salary");
				$alpha++;
				$excel->getActiveSheet()->setCellValue($alpha.$i, "Management Fee 7.5%");
				$alpha++;
				$excel->getActiveSheet()->setCellValue($alpha.$i, "PPN 11%");
				$alpha++;
				$excel->getActiveSheet()->setCellValue($alpha.$i, "PPH23");
				$alpha++;
				$excel->getActiveSheet()->setCellValue($alpha.$i, "BPJS");
				$alpha++;
				$excel->getActiveSheet()->setCellValue($alpha.$i, "Grand Total");
				$excel->getActiveSheet()->getStyle('A'.$i.':'.$alpha.$i)->getFont()->setBold( true );
				$i++;
				$no = 1;
				foreach($employee_attendance as $employee_id){
					$employee = $this->employee_casual_model->get(array('id' => $employee_id->employee_id, 'columns' => 'A.pin_finger, A.full_name, A.bank_account, A.bank_name, A.bank_account_name'));
					if($employee){
						$excel->getActiveSheet()->setCellValue("A".$i, $no);
						$excel->getActiveSheet()->setCellValueExplicit("B".$i, $employee->pin_finger);
						$excel->getActiveSheet()->setCellValue("C".$i, $employee->full_name);
						$excel->getActiveSheet()->setCellValueExplicit("D".$i, $employee->bank_account);
						$excel->getActiveSheet()->setCellValue("E".$i, $employee->bank_name);
						$excel->getActiveSheet()->setCellValue("F".$i, $employee->bank_account_name);
						$excel->getActiveSheet()->setCellValue("G".$i, $department->name);
						$alphabet = 'H';
						$total_shift = 0;
						for ($j = strtotime($periode_start); $j <= strtotime($periode_end); $j = $j + 86400 ) {
							$shift = $this->employee_casual_attendance_model->get(array('columns' => 'COUNT(A.employee_id) AS count_employee', 'department_id' => $department->id, 'employee_id' => $employee_id->employee_id, 'date' => date('Y-m-d', $j), 'overtime' => $overtime));
							if($shift){
								$excel->getActiveSheet()->setCellValue($alphabet.$i, $shift->count_employee);
								$total_shift = $total_shift + $shift->count_employee;
							}else{
								$excel->getActiveSheet()->setCellValue($alphabet.$i, 0);
							}
							$alphabet++;
						}
						$salary_employee 	= $total_shift * $basic_salary;
						$mf_employee 		= ($salary_employee * $mf)/100;
						$ppn_employee 		= (($mf_employee * $ppn)/100);
						$pph_employee 		= (($mf_employee * $pph)/100);
						$grand_employee 	= ($salary_employee + $mf_employee + $ppn_employee) - $pph_employee;
						$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, $total_shift);
						$alphabet++;
						$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($basic_salary));
						$alphabet++;
						$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($salary_employee));
						$alphabet++;
						$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($mf_employee));
						$alphabet++;
						$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($ppn_employee));
						$alphabet++;
						$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($pph_employee));
						$alphabet++;
						$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, 0);
						$alphabet++;
						$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($grand_employee));
						$i++;
						$no++;
					}
				}

				$excel->getActiveSheet()->setCellValue("A".$i, "Total Casual Masuk Per Harinya");
				$excel->getActiveSheet()->mergeCells('A'.$i.':G'.$i);
				$excel->getActiveSheet()->getStyle('A'.$i.':G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$excel->getActiveSheet()->getStyle('A'.$i.':G'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$alphabet = 'H';
				$total_shift = 0;
				for ($j = strtotime($periode_start); $j <= strtotime($periode_end); $j = $j + 86400 ) {
					$shift = $this->employee_casual_attendance_model->get(array('columns' => 'COUNT(A.employee_id) AS count_employee', 'department_id' => $department->id, 'date' => date('Y-m-d', $j), 'overtime' => $overtime));
					if($shift){
						$excel->getActiveSheet()->setCellValue($alphabet.$i, $shift->count_employee);
						$total_shift = $total_shift + $shift->count_employee;
					}else{
						$excel->getActiveSheet()->setCellValue($alphabet.$i, 0);
					}
					$alphabet++;
				}
				
				$salary_department	= $total_shift * $basic_salary;
				$mf_department 		= ($salary_department * $mf)/100;
				$ppn_department 	= (($mf_department * $ppn)/100);
				$pph_department 	= (($mf_department * $pph)/100);
				$grand_department 	= ($salary_department + $mf_department + $ppn_department) - $pph_department;
				$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, $total_shift);
				$alphabet++;
				$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, 0);
				$alphabet++;
				$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($salary_department));
				$alphabet++;
				$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($mf_department));
				$alphabet++;
				$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($ppn_department));
				$alphabet++;
				$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($pph_department));
				$alphabet++;
				$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, 0);
				$alphabet++;
				$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($grand_department));
				$excel->getActiveSheet()->getStyle('A'.$i.':'.$alphabet.$i)->getFont()->setBold( true );
				$i++;
				$i++;
				$i++;
				$i++;
			}
		}

		$excel->getActiveSheet()->setCellValue("A".$i, "Grand Total Casual Masuk Per Harinya");
		$excel->getActiveSheet()->mergeCells('A'.$i.':G'.$i);
		$excel->getActiveSheet()->getStyle('A'.$i.':G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('A'.$i.':G'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$alphabet = 'H';
		$total_shift = 0;
		for ($j = strtotime($periode_start); $j <= strtotime($periode_end); $j = $j + 86400 ) {
			$shift = $this->employee_casual_attendance_model->get(array('columns' => 'COUNT(A.employee_id) AS count_employee', 'date' => date('Y-m-d', $j), 'overtime' => $overtime));
			if($shift){
				$excel->getActiveSheet()->setCellValue($alphabet.$i, $shift->count_employee);
				$total_shift = $total_shift + $shift->count_employee;
			}else{
				$excel->getActiveSheet()->setCellValue($alphabet.$i, 0);
			}
			$alphabet++;
		}
				
		$salary_grand	= $total_shift * $basic_salary;
		$mf_grand 		= ($salary_grand * $mf)/100;
		$ppn_grand 		= (($mf_grand * $ppn)/100);
		$pph_grand 		= (($mf_grand * $pph)/100);
		$grand 			= ($salary_grand + $mf_grand + $ppn_grand) - $pph_grand;
		
		$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, $total_shift);
		$alphabet++;
		$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, 0);
		$alphabet++;
		$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($salary_grand));
		$alphabet++;
		$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($mf_grand));
		$alphabet++;
		$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($ppn_grand));
		$alphabet++;
		$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($pph_grand));
		$alphabet++;
		$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, 0);
		$alphabet++;
		$excel->getActiveSheet()->setCellValueExplicit($alphabet.$i, rupiah_round($grand));
		$excel->getActiveSheet()->getStyle('A'.$i.':'.$alphabet.$i)->getFont()->setBold( true );
		
		$excel->getActiveSheet()->setTitle('Harian Lepas');
		$sheet = $excel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells( true );
		foreach( $cellIterator as $cell ) {
			$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		}
		$excel->createSheet();
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = preg_replace("/[^a-zA-Z0-9]/", "", strtolower($site->name)).'-'.$type;
		$objWriter->save(FCPATH."files/payroll_casual/".$filename.".xlsx");
		redirect(base_url("files/payroll_casual/".$filename.".xlsx"));
		exit;
	}

	public function payroll_preview($date_start = FALSE, $date_finish = FALSE){
		$this->load->model(array('site_model','employee_casual_model', 'payroll_config_casual_model', 'department_casual_model'));
		$data['_TITLE_'] 		= 'Absensi Karyawan';
		$data['_PAGE_'] 		= 'rocasual/payroll_preview';
		$data['_MENU_PARENT_'] 	= 'rocasual';
		$data['_MENU_'] 		= 'ropayroll_preview';
		$data['list_department']= $this->department_casual_model->gets(array('site_id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name'));
		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address, A.attendance_machine'));

		$data['total'] 			= $this->employee_casual_model->gets(array(), TRUE);

		$data['date_start'] 		= date('01/m/Y');
		$data['date_finish'] 		= date('d/m/Y');

		if($this->input->post()){
			$data['date_start']		= $this->input->post('date_start');
			$data['date_finish']	= $this->input->post('date_finish');
		}

		$data['pdate_start'] 		= date("Y-m-d", strtotime(str_replace('/', '-', $data['date_start'])));
		$data['pdate_finish'] 		= date("Y-m-d", strtotime(str_replace('/', '-', $data['date_finish'])));

		if($date_start){
			$data['pdate_start'] 	= $date_start;
			$data['date_start'] 	= date("d/m/Y", strtotime($date_start));
		}
		if($date_finish){
			$data['pdate_finish'] 	= $date_finish;
			$data['date_finish'] 	= date("d/m/Y", strtotime($date_finish));
		}

		$data['basic_salary'] 	= 0;
		$data['management_fee'] = 0;
		$data['ppn'] 			= 0;
		$data['pph'] 			= 0;
		$config 				= $this->payroll_config_casual_model->get(array('site_id' => $this->session->userdata('site')));
		if($config){
			$data['basic_salary'] 	= $config->basic_salary;
			$data['management_fee'] = $config->management_fee;
			$data['ppn'] 			= $config->ppn;
			$data['pph'] 			= $config->pph;
		}

		$this->view($data);
	}



	public function payroll_preview_ajax($date_start = FALSE, $date_finish = FALSE){
		$this->load->model(array('employee_casual_attendance_model'));
		$column_index = $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, B.employee_number, B.pin_finger, B.id_card, B.full_name, B.status AS status_employee, DATE_FORMAT(A.date, '%d/%m/%Y') AS date, A.check_in, A.check_out, A.overtime, C.name AS department, A.status AS status_attendance";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];		
		$params['site_id']		= $this->session->userdata('site');
		$params['date_start']	= $date_start;
		$params['date_finish']	= $date_finish;

		$params['pin_finger']			= $_POST['columns'][1]['search']['value'];
		$params['full_name']			= $_POST['columns'][2]['search']['value'];
		$params['status_employee']		= $_POST['columns'][3]['search']['value'];
		$params['date']					= $_POST['columns'][4]['search']['value'];
		$params['check_in']				= $_POST['columns'][5]['search']['value'];
		$params['check_out']			= $_POST['columns'][6]['search']['value'];
		$params['department']			= $_POST['columns'][7]['search']['value'];
		$params['overtime']				= $_POST['columns'][8]['search']['value'];
		$params['status_attendance']	= $_POST['columns'][9]['search']['value'];
		
		$list 	= $this->employee_casual_attendance_model->gets($params);
		$total 	= $this->employee_casual_attendance_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			
			$result['pin_finger']			= $item->pin_finger;
			$result['full_name']			= $item->full_name;
			$result['status_employee']		= $item->status_employee;
			$result['date']					= $item->date;
			$result['check_in']				= $item->check_in;
			$result['check_out']			= $item->check_out;
			$result['department']			= $item->department;
			$result['overtime']				= $item->overtime;
			$result['status_attendance']	= '<p class="text-danger">'.$item->status_attendance.'</p>';
			if($item->status_attendance ==  'Disetujui'){
				$result['status_attendance']		= '<p class="text-success">'.$item->status_attendance.'</p>';
			}
			if($item->status_attendance ==  'Pengajuan'){
				$result['status_attendance']		= '<p class="text-warning">'.$item->status_attendance.'</p>';
			}

			$result['action'] 				= '';
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			if($item->status_attendance == 'Draft' || $item->status_attendance == 'Ditolak'){
				$result['action'] 				=
					'<a class="btn-sm btn-success btn-action btn-block"  style="cursor:pointer;" href="'. base_url("rocasual/attendance_form/".$date_start."/".$date_finish."/".$item->id).'">Ubah</a>
					<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("rocasual/attendance_delete/".$date_start."/".$date_finish."/".$item->id).'">Hapus</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function payroll_final($date_start = FALSE, $date_finish = FALSE){

		$this->load->model(array('site_model','employee_casual_model', 'payroll_config_casual_model'));
		$data['_TITLE_'] 		= 'Payroll Karyawan';
		$data['_PAGE_'] 		= 'rocasual/payroll_final';
		$data['_MENU_PARENT_'] 	= 'rocasual';
		$data['_MENU_'] 		= 'ropayroll_final';
		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address, A.attendance_machine'));
		$data['total'] 			= $this->employee_casual_model->gets(array(), TRUE);

		$data['date_start'] 		= date('01/m/Y');
		$data['date_finish'] 		= date('d/m/Y');

		if($this->input->get()){
			$data['date_start']		= $this->input->get('date_start');
			$data['date_finish']	= $this->input->get('date_finish');
		}

		if($this->input->post()){
			$data['date_start']		= $this->input->post('date_start');
			$data['date_finish']	= $this->input->post('date_finish');
		}

		$data['pdate_start'] 		= date("Y-m-d", strtotime(str_replace('/', '-', $data['date_start'])));
		$data['pdate_finish'] 		= date("Y-m-d", strtotime(str_replace('/', '-', $data['date_finish'])));

		if($date_start){
			$data['pdate_start'] 	= $date_start;
			$data['date_start'] 	= date("d/m/Y", strtotime($date_start));
		}
		if($date_finish){
			$data['pdate_finish'] 	= $date_finish;
			$data['date_finish'] 	= date("d/m/Y", strtotime($date_finish));
		}

		$data['basic_salary'] 	= 0;
		$data['management_fee'] = 0;
		$data['ppn'] 			= 0;
		$data['pph'] 			= 0;
		$config 				= $this->payroll_config_casual_model->get(array('site_id' => $this->session->userdata('site')));
		if($config){
			$data['basic_salary'] 	= $config->basic_salary;
			$data['management_fee'] = $config->management_fee;
			$data['ppn'] 			= $config->ppn;
			$data['pph'] 			= $config->pph;
		}

		$this->view($data);
	}

	public function payroll_final_ajax($date_start = FALSE, $date_finish = FALSE){
		$this->load->model(array('employee_casual_payroll_model'));
		$column_index = $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.status AS status_approval, B.employee_number, B.full_name, B.status AS status_employee, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.total_shift, A.basic_salary, A.management_fee, A.ppn, A.pph, A.salary";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];		
		$params['site_id']		=  $this->session->userdata('site');
		$params['date_start']	= $date_start;
		$params['date_finish']	= $date_finish;

		$params['employee_number']		= $_POST['columns'][1]['search']['value'];
		$params['full_name']			= $_POST['columns'][2]['search']['value'];
		$params['status_employee']		= $_POST['columns'][3]['search']['value'];
		$params['status_approval']		= $_POST['columns'][4]['search']['value'];
		
		$list 	= $this->employee_casual_payroll_model->gets($params);
		$total 	= $this->employee_casual_payroll_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['employee_number']		= $item->employee_number;
			$result['full_name']			= $item->full_name;
			$result['status_employee']		= $item->status_employee;
			$result['periode']				= $item->periode_start.' - '.$item->periode_end;
			$result['total_shift']			= $item->total_shift;
			$result['basic_salary']			= rupiah_round($item->basic_salary);
			$result['total_salary']			= rupiah_round($item->basic_salary * $item->total_shift);
			$result['management_fee']		= rupiah_round($item->management_fee);
			$result['ppn']					= rupiah_round($item->ppn);
			$result['pph']					= rupiah_round($item->pph);
			$result['salary']				= rupiah_round($item->salary);
			$result['status_approval']	= '<p class="text-danger">'.$item->status_approval.'</p>';
			if($item->status_approval ==  'Selesai'){
				$result['status_approval']		= '<p class="text-success">'.$item->status_approval.'</p>';
			}
			if($item->status_approval ==  'Menunggu'){
				$result['status_approval']		= '<p class="text-warning">'.$item->status_approval.'</p>';
			}

			$result['id'] 					= '';
			if($item->status_approval == 'Draft' || $item->status_approval == 'Menunggu'|| $item->status_approval == 'Ditolak'){
				$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			}

			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function payroll_count()
	{
		if($this->input->post()){
			$this->load->model(array('employee_casual_attendance_model', 'employee_casual_payroll_model', 'payroll_config_casual_model'));
			$params['site_id']		= $this->session->userdata('site');
			$params['columns']		= 'MIN(A.date) AS periode_start, MAX(A.date) AS periode_end';
			
			$params['date_start']	= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_start'))));
			$params['date_finish']	= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_finish'))));
			$basic_salary	= str_replace('.', '', $this->input->post('basic_salary'));
			$mf				= $this->input->post('management_fee');
			$ppn			= $this->input->post('ppn');
			$pph			= $this->input->post('pph');

			$this->payroll_config_casual_model->save(array('site_id' => $params['site_id'], 'basic_salary' => $basic_salary, 'management_fee' => $mf, 'ppn' => $ppn, 'pph' => $pph));
			
			$periode = $this->employee_casual_attendance_model->gets($params);
			if($periode){
				$data['periode_start'] 	= $params['date_start']; //$periode[0]->periode_start;
				$data['periode_end']	= $params['date_finish']; //$periode[0]->periode_end;

				$params['columns']		= 'COUNT(A.id) AS total_shift, A.employee_id';
				$params['groupby'] 		= 'employee_id';
				$list_employee = $this->employee_casual_attendance_model->gets($params);
				foreach ($list_employee as $employee) {
					$data['id']				= '';
					$total_salary 			= $employee->total_shift * $basic_salary;
					$data['basic_salary'] 	= $basic_salary;
					$data['employee_id'] 	= $employee->employee_id;
					$data['total_shift'] 	= $employee->total_shift;
					$data['management_fee'] = ($total_salary * $mf)/100;
					$data['ppn'] 			= ($data['management_fee'] * $ppn)/100;
					$data['pph'] 			= ($data['management_fee'] * $pph)/100;
					$data['salary']			= ($total_salary + $data['management_fee'] + $data['ppn']) - $data['pph'];
					$data['is_active'] 		= 1;
					$payroll = $this->employee_casual_payroll_model->get(array('employee_id' => $employee->employee_id, 'range_start' => $data['periode_start'], 'range_end' => $data['periode_end'], 'columns' => 'A.id, A.status'));
					if($payroll){
						$data['id'] 	= $payroll->id;
						if($payroll->status != 'Draft'){
							continue;
						}
					}
					$result = $this->employee_casual_payroll_model->save($data);
					if (!$result) {
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger'));
						redirect(base_url('rocasual/payroll/'.$params['date_start'].'/'.$params['date_finish']));
					}
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
			redirect(base_url('rocasual/payroll_final/'.$params['date_start'].'/'.$params['date_finish']));
		}
	}

	public function payroll_approve($date_start = FALSE, $date_finish = FALSE)
	{
		if($this->input->post()){
			$this->load->model(array('employee_casual_payroll_model'));
		
			$data['status'] 		= $this->input->post('status');
			foreach ($this->input->post('id') AS $payroll_id) {
				$data['id'] = $payroll_id;
				$result 	=  $this->employee_casual_payroll_model->save($data);
				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger'));
					redirect(base_url('rocasual/payroll_final/'.$date_start.'/'.$date_finish));
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
			redirect(base_url('rocasual/payroll_final/'.$date_start.'/'.$date_finish));
		}
	}

	public function payroll_delete($date_start = FALSE, $date_finish = FALSE)
	{
		if($this->input->post()){
			$this->load->model(array('employee_casual_payroll_model'));
		
			$data['is_active'] 		= 0;
			foreach ($this->input->post('id') AS $payroll_id) {
				$data['id'] = $payroll_id;
				$result 	=  $this->employee_casual_payroll_model->save($data);
				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger'));
					redirect(base_url('rocasual/payroll_final/'.$date_start.'/'.$date_finish));
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
			redirect(base_url('rocasual/payroll_final/'.$date_start.'/'.$date_finish));
		}
	}
}
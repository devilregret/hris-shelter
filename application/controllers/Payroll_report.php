<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payroll_report extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		$role 	= array_merge($role, $this->config->item('accounting'));
		$role 	= array_merge($role, $this->config->item('directur'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function site(){
		$data['_TITLE_'] 		= 'Daftar Site Bisnis';
		$data['_PAGE_'] 		= 'payroll_report/site';
		$data['_MENU_PARENT_'] 	= 'payroll_report';
		$data['_MENU_'] 		= 'payroll_site';
		$this->view($data);
	}
	
	public function site_ajax(){

		$this->load->model(array('site_model'));

		$column_index = $_POST['order'][0]['column']; 
		$params['columns'] 		= "A.id AS site_id, A.code AS site_code, A.name AS site_name, C.code AS company_code, F.name AS branch_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];

		$params['company_code']		= $_POST['columns'][1]['search']['value'];
		$params['code']		= $_POST['columns'][2]['search']['value'];
		$params['name']		= $_POST['columns'][3]['search']['value'];
		$params['branch']		= $_POST['columns'][4]['search']['value'];

		if(in_array($this->session->userdata('role'), $this->config->item('personalia'))){
			$params['indirect'] = TRUE;
		}

		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		
		$list 	= $this->site_model->gets($params);
		$total 	= $this->site_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['no'] 				= $i;
			$result['company_code']		= $item->company_code;
			$result['site_code']		= $item->site_code;
			$result['site_name']		= $item->site_name;
			$result['branch_name']		= $item->branch_name;
			$result['action'] 			= 
				'<a class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" href="'. base_url("payroll_report/list/".$item->site_id).'">Detail</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function list($site_id = FALSE){
		$this->load->model(array('site_model'));
		$data['site'] 			= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.id, A.name, A.address'));
		$data['_TITLE_'] 		= 'Laporan Payroll';
		$data['_PAGE_'] 		= 'payroll_report/list';
		$data['_MENU_PARENT_'] 	= 'payroll_report';
		$data['_MENU_'] 		= 'payroll_site';
		$this->view($data);
	}

	public function list_ajax($site_id = FALSE){
		$this->load->model(array('payroll_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "C.id AS site_id, C.name AS site_name, D.code AS company_code, COUNT(A.employee_id) AS employee_count, SUM(A.salary) AS summary_payroll, SUM(A.basic_salary) AS summary_basic, MIN(A.periode_start) AS periode_start, MAX(A.periode_end) AS periode_end ";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];

		$params['site_name']	= $_POST['columns'][1]['search']['value'];
		$params['company_code']	= $_POST['columns'][2]['search']['value'];
		$params['payment']		= array('Selesai');
		$params['site_id']		= $site_id;

		$list 	= $this->payroll_model->report($params);
		$total 	= $this->payroll_model->report($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['no'] 				= $i;
			$result['site_name']		= $item->site_name;
			$result['company_code']		= $item->company_code;
			$period 					= "";
			if($item->periode_start){
				$start_date = substr($item->periode_start, 8, 2).' '.get_month(substr($item->periode_start, 5, 2));
				$end_date 	= substr($item->periode_end, 8, 2).' '.get_month(substr($item->periode_end, 5, 2)).' '.substr($item->periode_end, 0, 4);
				$period 	= $start_date.' - '.$end_date;
			}
			$result['period']			= $period;
			$result['employee_count']	= $item->employee_count;
			$result['summary_payroll']	= format_rupiah($item->summary_payroll);
			$result['summary_basic']	= format_rupiah($item->summary_basic);
			$result['action'] 			= 
				'<a class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" href="'. base_url("payroll_report/detail/".$item->site_id."/".$item->periode_end).'">Detail</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function detail($site_id=FALSE, $periode_end = FALSE){
		$this->load->model(array('site_model', 'payroll_model', 'employee_model'));
		$data['_TITLE_'] 		= 'Payroll Karyawan';
		$data['_PAGE_'] 		= 'payroll_report/detail';
		$data['_MENU_PARENT_'] 	= 'payroll_report';
		$data['_MENU_'] 		= 'payroll_site';
		$params['status']		= 1;
		$params['status_approval'] 		= 3;
		$params['site_id'] 		= $site_id;

		$data['site']			= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.id, A.name, A.address'));
		$data['employee']		= $this->employee_model->gets($params, TRUE);
		$data['resume']			= $this->payroll_model->resume(array('site_id' => $site_id, 'periode_end' => $periode_end));

		$params['columns'] 		= "A.id, B.full_name AS employee_name, D.name AS position, B.bank_account, B.bank_name, B.bank_account_name, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.salary, A.basic_salary, A.overtime_calculation, A.bpjs_ks, (A.bpjs_jht + A.bpjs_jp) AS bpjs_tk, A.tax_calculation ";
		$params['payment']		= 'Selesai';

		$date=date_create($periode_end);
		$params['periode_end']	= date_format($date,"d/m/Y");
		$data['periode_end']	= $periode_end;
		$data['payroll'] 		= $this->payroll_model->gets($params);
		$this->view($data);
	}

	public function detail_export_indirect($site_id = FALSE, $end_period = FALSE, $start_period = FALSE)
	{
		$this->load->model(array('company_model','site_model','payroll_model'));
		$site 	= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.name'));
		if(!$site){
			redirect(base_url('payroll_report/detail/'.$site_id.'/'.$end_period));
		}
		$start_date = substr($start_period, 8, 2).' '.get_month(substr($start_period, 5, 2));
		$end_date 	= substr($end_period, 8, 2).' '.get_month(substr($end_period, 5, 2)).' '.substr($end_period, 0, 4);
		$params['columns'] 		= "B.full_name, B.bank_account, A.salary";
		$params['site_id']		= $site_id;
		$params['end_period']	= $end_period;
		$params['orderby']		= "A.periode_end";
		$params['order']		= "DESC";
		$params['in_payment']		= array('Selesai');
		
		$title = 'payroll_report';
		$files = glob(FCPATH."files/".$title."/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');
		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
		->setLastModifiedBy($this->session->userdata('name'))
		->setTitle($title)
		->setSubject($title)
		->setDescription($title)
		->setKeywords($title);
		
		$row = 2;
		$list_company = $this->company_model->gets(array('columns' => 'A.name, A.code, A.id'));
		foreach($list_company AS $company){
			$excel->getActiveSheet()->setCellValue("A".$row, $company->name);
			$excel->getActiveSheet()->mergeCells('A'.$row.':E'.$row);
			$excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold(true);
			$row++;

			$excel->getActiveSheet()->setCellValue("A".$row, 'PENGGAJIAN INDIRECT  STAF ');
			$excel->getActiveSheet()->mergeCells('A'.$row.':E'.$row);
			$excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold(true);
			$row++;

			$excel->getActiveSheet()->setCellValue("A".$row, 'BULAN '.get_month(substr($start_period, 5, 2)).' '.substr($end_period, 0, 4));
			$excel->getActiveSheet()->mergeCells('A'.$row.':C'.$row);
			$excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->getFont()->setBold(true);
			$row++;

			$row++;
			$excel->getActiveSheet()->setCellValue("A".$row, 'NO');
			$excel->getActiveSheet()->setCellValue("B".$row, 'No Rekening');
			$excel->getActiveSheet()->setCellValue("C".$row, 'Bisnis Unit');
			$excel->getActiveSheet()->setCellValue("D".$row, 'Nama');
			$excel->getActiveSheet()->setCellValue("E".$row, 'Total Gaji');			
			$excel->getActiveSheet()->mergeCells('A'.$row.':E'.$row);
			$excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold(true);
			$row++;

			$params['company_id']	= $company->id;
			$list_payroll = $this->payroll_model->gets($params);

			$salary_company = 0;
			$no = 1;
			foreach($list_payroll AS $payroll){
				$excel->getActiveSheet()->setCellValueExplicit("A".$row, $no);
				$excel->getActiveSheet()->setCellValueExplicit("B".$row, $payroll->bank_account);
				$excel->getActiveSheet()->setCellValueExplicit("C".$row, $company->code);
				$excel->getActiveSheet()->setCellValueExplicit("D".$row, $payroll->full_name);
				$excel->getActiveSheet()->setCellValueExplicit("E".$row, $payroll->salary);
				$salary_company = $salary_company + $payroll->salary;
				$no++;
				$row++;
			}
			$excel->getActiveSheet()->setCellValue("A".$row, 'TOTAL');
			$excel->getActiveSheet()->setCellValue("B".$row, '');
			$excel->getActiveSheet()->setCellValue("C".$row, '');
			$excel->getActiveSheet()->setCellValue("D".$row, '');
			$excel->getActiveSheet()->setCellValue("E".$row, $salary_company);
			$excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
			$excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold(true);
			$row++;
			$row++;

			$date_now = 'Surabaya, '.date('d').' '.get_month(date('m')).' '.date('Y');
			$excel->getActiveSheet()->setCellValue("A".$row, $date_now);
			$excel->getActiveSheet()->mergeCells('A'.$row.':E'.$row);
			$row++;

			$excel->getActiveSheet()->setCellValue("A".$row, '');
			$excel->getActiveSheet()->setCellValue("B".$row, '');
			$excel->getActiveSheet()->setCellValue("C".$row, '');
			$excel->getActiveSheet()->setCellValue("D".$row, '');
			$excel->getActiveSheet()->setCellValue("E".$row, '');
			$excel->getActiveSheet()->mergeCells('A'.$row.':E'.$row);
			$row++;

			$excel->getActiveSheet()->setCellValue("A".$row, '');
			$excel->getActiveSheet()->setCellValue("B".$row, '');
			$excel->getActiveSheet()->setCellValue("C".$row, '');
			$excel->getActiveSheet()->setCellValue("D".$row, '');
			$excel->getActiveSheet()->setCellValue("E".$row, '');
			$excel->getActiveSheet()->mergeCells('A'.$row.':E'.$row);
			$row++;

			$excel->getActiveSheet()->setCellValue("A".$row, '');
			$excel->getActiveSheet()->setCellValue("B".$row, '');
			$excel->getActiveSheet()->setCellValue("C".$row, '');
			$excel->getActiveSheet()->setCellValue("D".$row, '');
			$excel->getActiveSheet()->setCellValue("E".$row, '');
			$excel->getActiveSheet()->mergeCells('A'.$row.':E'.$row);
			$row++;
			
			$excel->getActiveSheet()->setCellValue("A".$row, '');
			$excel->getActiveSheet()->setCellValue("B".$row, 'MIFTAKHUL ARIF');
			$excel->getActiveSheet()->setCellValue("C".$row, '');
			$excel->getActiveSheet()->setCellValue("D".$row, '');
			$excel->getActiveSheet()->setCellValue("E".$row, 'HARI WAHYUDIN');
			$excel->getActiveSheet()->mergeCells('B'.$row.':D'.$row);
			$excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold(true);
			$row++;

			$excel->getActiveSheet()->setCellValue("A".$row, '');
			$excel->getActiveSheet()->setCellValue("B".$row, 'GM HC');
			$excel->getActiveSheet()->setCellValue("C".$row, '');
			$excel->getActiveSheet()->setCellValue("D".$row, '');
			$excel->getActiveSheet()->setCellValue("E".$row, 'DIR OPS & BISNIS');
			$excel->getActiveSheet()->mergeCells('B'.$row.':D'.$row);
			$row++;
		}

		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;

	}

	public function detail_export($site_id = FALSE, $end_period = FALSE, $start_period = FALSE)
	{
		$this->load->library('word');
		$this->load->model(array('report_template_model','site_model','payroll_model'));
		$template = $this->report_template_model->get(array('id' => 1, 'columns' => 'A.content, A.title'));
		
		$site 	= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.name'));
		if(!$site){
			redirect(base_url('payroll_report/detail/'.$site_id.'/'.$end_period));
		}

		$start_date = substr($start_period, 8, 2).' '.get_month(substr($start_period, 5, 2));
		$end_date 	= substr($end_period, 8, 2).' '.get_month(substr($end_period, 5, 2)).' '.substr($end_period, 0, 4);
		$template->content = str_replace("{NAMA_SITE}", $site->name, $template->content);
		$template->content = str_replace("{PERIODE}", $start_date." - ".$end_date, $template->content);

		$params['columns'] 		= "A.id AS payroll_id, C.id AS site_id, C.name AS site_name, D.code AS company_code, COUNT(A.employee_id) AS employee_count, SUM(A.salary) AS summary_payroll, SUM(A.basic_salary) AS summary_basic, MIN(A.periode_start) AS periode_start, MAX(A.periode_end) AS periode_end ";
		$params['site_id']		= $site_id;
		$params['end_period']	= $end_period;
		$params['orderby']		= "A.periode_end";
		$params['order']		= "DESC";
		$params['payment']		= array('Selesai');
		$list_payroll = $this->payroll_model->report($params);
		
		$TUNJANGAN_KEMAHALAN	= 0;
		$TUNJANGAN_LEMBUR	 	= 0;
		$TRANSPORT_ALLOWANCE	= 0;
		$MEAL_ALLOW 			= 0;
		$TUNJANGAN_KEHADIRAN	= 0;
		$KELEBIHAN_JAM_KERJA	= 0;
		$OT_REG 				= 0;
		$OTN 					= 0;
		$OT_REQUEST				= 0;
		$SHIFT_MALAM			= 0;
		$TUNJANGAN_LUAR_KOTA	= 0;
		$KEKURANGAN_GAJI 		= 0;
		$TUNJAB 				= 0;
		$TUNJANGAN_KOMPLEKSITAS = 0;
		$BPJS_KES_PERUSAHAAN 	= 0;
		$BPJS_TK_PERUSAHAAN 	= 0;
		$GROSS_SALLARY 			= 0;
		$BIAYA_DITANGGUNG 		= 0;

		$BPJS_KS_KARYAWAN 		= 0;
		$BPJS_TK_KARYAWAN 		= 0;
		$KAPORLAP_KARYAWAN 		= 0;
		$PPH21 					= 0;
		$POTONGAN_FINANCE 		= 0;
		$JAMINAN_PENSIUN 		= 0;
		$POTONGAN_TABUNGAN_GADA = 0;
		$POTONGAN_THP_GROSS 	= 0;
		$TAKE_HOME_PAY  		= 0;

		foreach ($list_payroll as $item) {
			$payroll = $this->payroll_model->gets_detail(array('payroll_id' => $item->payroll_id, 'columns' => 'A.name, A.category, A.value'));
			foreach($payroll AS $detail){
				if($detail->category == 'potongan'){
					$GROSS_SALLARY += $detail->value;
				}
				if($detail->category == 'potongan'){
					$POTONGAN_THP_GROSS += $detail->value;
				}
				if (str_contains(strtoupper($detail->name), 'KEMAHALAN')) {
					$TUNJANGAN_KEMAHALAN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'LEMBUR')) {
					$TUNJANGAN_LEMBUR += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'TRANSPORT')) {
					$TRANSPORT_ALLOWANCE += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'MEAL')) {
					$MEAL_ALLOW += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'KEHADIRAN')) {
					$TUNJANGAN_KEHADIRAN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'JAM KERJA')) {
					$KELEBIHAN_JAM_KERJA += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'REG')) {
					$OT_REG += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'OTN')) {
					$OTN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'REQUEST')) {
					$OT_REQUEST += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'SHIFT')) {
					$SHIFT_MALAM += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'LUAR KOTA')) {
					$TUNJANGAN_LUAR_KOTA += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'KEKURANGAN GAJI')) {
					$KEKURANGAN_GAJI += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'TUNJAB')) {
					$TUNJAB += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'BPJS KES PERUSAHAAN')) {
					$BPJS_KES_PERUSAHAAN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'BPJS TK PERUSAHAAN')) {
					$BPJS_TK_PERUSAHAAN += $detail->value;
					continue;
				}
				if($detail->category == 'potongan'){
					$BIAYA_DITANGGUNG += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'BPJS KS KARYAWAN')) {
					$BPJS_KS_KARYAWAN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'BPJS TK KARYAWAN')) {
					$BPJS_TK_KARYAWAN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'KAPORLAP')) {
					$KAPORLAP_KARYAWAN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'PPH21')) {
					$PPH21 += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'JAMINAN PENSIUN')) {
					$JAMINAN_PENSIUN += $detail->value;
					continue;
				}
				if (str_contains(strtoupper($detail->name), 'GADA')) {
					$POTONGAN_TABUNGAN_GADA += $detail->value;
					continue;
				}
				if($detail->category == 'pendapatan'){
					$POTONGAN_FINANCE += $detail->value;
					continue;
				}
			}
		}

		$summary 	= '<strong>PENDAPATAN</strong><table  border="1" cellspacing="0" cellpadding="0" width="100%" style="font-family: Tahoma;font-size: 12px;">';
		$summary  	.= '<tr><th>GAJI POKOK</th><th>TUNJANGAN LEMBUR</th><th>TUNJANGAN KEHADIRAN</th><th>KELEBIHAN JAM KERJA</th><th>TUNJANGAN LUAR KOTA</th><th>KEKURANGAN GAJI</th>LAIN-LAIN<th></tr>';
		$summary  	.= '<tr><td>'.rupiah($item->summary_basic).'</td><td>'.rupiah($TUNJANGAN_LEMBUR).'</td><td>'.rupiah($TUNJANGAN_KEHADIRAN).'</td><td>'.rupiah($KELEBIHAN_JAM_KERJA).'</td><td>'.rupiah($TUNJANGAN_LUAR_KOTA).'</td><td>'.rupiah($KEKURANGAN_GAJI).'</td>'.rupiah($TUNJAB+$TUNJANGAN_KOMPLEKSITAS+$MEAL_ALLOW+$TRANSPORT_ALLOWANCE+$TUNJANGAN_KEMAHALAN).'<td></tr>';
		$summary 	.= '</table>';

		$summary 	.= '<strong>POTONGAN</strong><table  border="1" cellspacing="0" cellpadding="0" width="100%">';
		$summary  	.= '<tr><th>BPJS KS KARYAWAN</th><th>BPJS TK KARYAWAN</th><th>KAPORLAP KARYAWAN</th><th>PPH21/BLN</th><th>LAIN-LAIN</th></tr>';
		$summary  	.= '<tr><td>'.rupiah($BPJS_KS_KARYAWAN).'</td><td>'.rupiah($BPJS_TK_KARYAWAN).'</td><td>'.rupiah($KAPORLAP_KARYAWAN).'</td><td>'.rupiah($PPH21).'</td><td>'.rupiah($POTONGAN_FINANCE+$JAMINAN_PENSIUN+$POTONGAN_TABUNGAN_GADA).'</td></tr>';
		$summary 	.= '</table>';
		$template->content = str_replace("{TABEL_SUMMARY_LAPORAN}", $summary, $template->content);

		$params 					= array();
		$params['status']			= 1;
		$params['status_approval'] 	= 3;
		$params['site_id'] 			= $site_id;
		$params['columns'] 			= "A.id, B.full_name AS employee_name, D.name AS position, B.bank_account, B.bank_name, B.bank_account_name, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.salary, A.basic_salary, A.overtime_calculation, A.bpjs_ks, (A.bpjs_jht + A.bpjs_jp) AS bpjs_tk, A.tax_calculation ";
		$params['payment']			= 'Selesai';

		$date=date_create($end_period);
		$params['periode_end']		= date_format($date,"d/m/Y");
		$list_payroll 				= $this->payroll_model->gets($params);

		$detail 	= '<br style="page-break-before: always"><table  border="1" cellspacing="0" cellpadding="0" width="100%" style="font-family: Tahoma;font-size: 12px;">';
		$detail 	.= '<tr><th>NO</th><th>NAMA</th><th>DEPARTMENT</th><th>BANK</th><th>ATAS NAMA</th><th>REKENING</th><th>THP</th></tr>';
		$no = 1;
		foreach ($list_payroll as $item) {
		$detail 	.= '<tr><td>'.$no.'</td><td>'.$item->employee_name.'</td><td>'.$item->position.'</td><td>'.$item->bank_name.'</td><td>'.$item->employee_name.'</td><td>'.$item->bank_account.'</td><td>'.rupiah($item->salary).'</td></tr>';
			$no++;
		}
		$detail 	.= '</table>';
		$template->content = str_replace("{TABEL_DETAIL_LAPORAN}", $detail, $template->content);
		$this->generate_Word(array('title' => $template->title, 'content' => $template->content));
	}

	private function generate_Word($data = array()){
		$this->load->library('Word');
		$filename = strtolower(str_replace(" ", "_",$data['title']));
		$phpWord = new PHPWord();
		$phpWord->createDoc($data['content'],$filename,1);
		die();
	}
}
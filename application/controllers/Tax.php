<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tax extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('accounting'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,15,16))){
			redirect(base_url());
		}
	}

	public function delete($tax_id = false)
	{
		$this->load->model('setting_tax_model');
		if ($tax_id)
		{
			$data =  $this->setting_tax_model->get(array('id' => $tax_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $tax_id, 'is_active' => 0);
				$result = $this->setting_tax_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('tax/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal dihapus.','danger'));
					redirect(base_url('tax/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('tax/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('tax/list'));
		}
	}

	public function preview($tax_id = FALSE)
	{
		$this->load->model('setting_tax_model');
		$data['_TITLE_'] 		= 'Preview Pengaturan Pajak';
		$data['_PAGE_']	 		= 'tax/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'tax';

		$data['id'] = $tax_id;

		
		if (!$tax_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('tax/list'));
		}

		$data['preview'] = $this->setting_tax_model->preview(array('id' => $tax_id));
		$this->load->view('tax/preview', $data);
	}

	public function form($tax_id = FALSE)
	{
		$this->load->model(array('setting_tax_model'));

		$data['id'] 			= '';
		$data['code']			= '';
		$data['description'] 	= '';
		$data['minimal']		= '';
		$data['tax']			= '';
		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['code'] 		= strtoupper($this->input->post('code'));
			$data['description']= $this->input->post('description');
			$data['minimal'] 	= $this->input->post('minimal');
			$data['tax'] 		= $this->input->post('tax');
			
			$this->form_validation->set_rules('code', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				// $insert = array('id' => $data['id'], 'name' => $data['name'], 'province_id' => $data['province_id']);
				$save_id	 	= $this->setting_tax_model->save($data);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('tax/list'));
		}

		if ($tax_id)
		{
			$data = (array) $this->setting_tax_model->get(array('id' => $tax_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('tax/list'));
			}
		}

		$data['_TITLE_'] 		= 'Pengaturan Pajak';
		$data['_PAGE_'] 		= 'tax/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'tax';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Pengaturan Pajak';
		$data['_PAGE_'] 		= 'tax/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'tax';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('setting_tax_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.code, A.description, A.minimal, A.tax';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['code']			= $_POST['columns'][1]['search']['value'];
		$params['description']	= $_POST['columns'][2]['search']['value'];
		$params['minimal']		= $_POST['columns'][3]['search']['value'];
		$params['tax']			= $_POST['columns'][4]['search']['value'];
		
		$list 	= $this->setting_tax_model->gets($params);
		$total 	= $this->setting_tax_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['code'] 		= $item->code;
			$result['description']	= $item->description;
			$result['minimal']		= $item->minimal;
			$result['tax']			= $item->tax;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("tax/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("tax/form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("tax/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}
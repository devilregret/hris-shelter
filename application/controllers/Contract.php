<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		$role 	= array_merge($role, $this->config->item('relation_officer_security'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function delete($contract_id = false)
	{
		$this->load->model('contract_model');
		if ($contract_id)
		{
			$data =  $this->contract_model->get(array('id' => $contract_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $contract_id, 'is_active' => 0);
				$result = $this->contract_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('contract/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('contract/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('contract/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('contract/list'));
		}
	}

	public function preview($contract_id = FALSE)
	{
		$this->load->model('contract_model');
		$data['_TITLE_'] 		= 'Preview Template Kontrak';
		$data['_PAGE_']	 		= 'contract/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'contract';

		$data['id'] = $contract_id;

		
		if (!$contract_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('contract/list'));
		}

		$data['preview'] = $this->contract_model->preview(array('id' => $contract_id));
		$this->load->view('contract/preview', $data);
	}

	public function form($contract_id = FALSE)
	{
		$this->load->model(array('contract_model'));

		$data['id'] 		= '';
		$data['title']		= '';
		$data['branch_id']	= '';
		$data['type']		= '';
		$data['content']	= '';

		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['title'] 		= $this->input->post('title');
			$data['branch_id']	= $this->input->post('branch_id');
			$data['type']		= $this->input->post('type');
			$data['content']	= $this->input->post('content');
			
			$this->form_validation->set_rules('title', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$insert = array('id' => $data['id'], 'title' => $data['title'], 'branch_id' => $data['branch_id'], 'type' => $data['type'], 'content' => $data['content']);
				
				if($this->input->post('action') == 'duplicate'){
					$insert['id'] = '';
				}

				$save_id	 	= $this->contract_model->save($insert);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('contract/list'));
		}

		if ($contract_id)
		{
			$data = (array) $this->contract_model->get(array('id' => $contract_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('contract/list'));
			}
		}

		$data['_TITLE_'] 		= 'Template Kontrak';
		$data['_PAGE_'] 		= 'contract/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'contract';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Template Kontrak';
		$data['_PAGE_'] 		= 'contract/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'contract';
		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('contract_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.title, A.type, B.name AS branch_name';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['title']		= $_POST['columns'][1]['search']['value'];
		$params['type']			= $_POST['columns'][2]['search']['value'];
		$params['branch_name']	= $_POST['columns'][3]['search']['value'];
		
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}

		$list 	= $this->contract_model->gets($params);
		$total 	= $this->contract_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['title'] 		= $item->title;
			$result['type'] 		= $item->type;
			$result['branch_name'] 	= $item->branch_name;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("contract/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("contract/form/".$item->id).'">Ubah</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function preview_contract($contract_id = FALSE)
	{
		$data['_TITLE_'] 		= 'Preview Kontrak Karyawan';
		$data['_PAGE_']	 		= 'contract/preview_contract';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'contract';

		$this->load->model('employee_contract_model');
		$data['id'] = $contract_id;

		
		if (!$contract_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('contract/list'));
		}

		$data['preview'] = $this->employee_contract_model->get(array('id' => $contract_id, 'columns' => 'A.contract'));
		$this->load->view('contract/preview_contract', $data);
	}

	public function pdf($contract_id = FALSE){
		$this->load->model('employee_contract_model');
		$contract = $this->employee_contract_model->get(array('id' => $contract_id, 'columns' => 'A.contract'));
		$data['content'] = $contract->contract;
		$data['title'] = 'Cetak Kontrak Kerja';
		$data['basepath'] = FCPATH.'files/';
		$this->generate_PDF($data);
		redirect(base_url('files/'.strtolower(str_replace(" ", "_",$data['title'])).'.pdf'));
	}

	private function generate_PDF($data = array())
	{	
		$this->load->library("Pdf");
		ob_start();
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// $pdf->SetProtection(array('print', 'copy'), $data['email']->pdf_password, null, 0, null);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shelter');
		$pdf->SetTitle($data['title']);
		$pdf->SetSubject($data['title']);
		$pdf->SetKeywords('Shelter');

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$pdf->SetFont('times', '', 10);
		$pdf->AddPage();
		$pdf->writeHTML($data['content'], true, false, true, false, '');

		$base_path  = $data['basepath'];
		if(!is_dir($base_path)){
			mkdir($base_path, 0777, true);
		}
		return $pdf->Output($base_path.strtolower(str_replace(" ", "_",$data['title'])).'.pdf', 'F');
	}
}
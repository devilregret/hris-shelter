<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roemployeeshift extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();


		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
		if($this->session->userdata('site') == ''){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan pilih site bisnis.','danger'));
			redirect(base_url());	
		}
	}

	public function list(){
		
		$this->load->model(array('site_model'));
		
		$data['_TITLE_'] 		= 'Shift Karyawan';
		$data['_PAGE_'] 		= 'roemployeeshift/list';
		$data['_MENU_PARENT_'] 	= 'roemployee';
		$data['_MENU_'] 		= 'roemployeeshift';
		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address'));
		
		$this->view($data);
	}

	public function list_ajax(){
		
		$this->load->model(array('roemployeeshift_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id AS employee_id, B.id, A.full_name, A.employee_number, B.shift AS shift, C.name AS position_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['employee_number']	= $_POST['columns'][3]['search']['value'];
		$params['position_name']	= $_POST['columns'][4]['search']['value'];
		$params['shift']			= $_POST['columns'][5]['search']['value'];
		
		$params['site_id']		=  $this->session->userdata('site');

		$list 	= $this->roemployeeshift_model->gets($params);
		$total 	= $this->roemployeeshift_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{			
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->employee_id.'">';
			$result['no'] 				= $i;
			$result['full_name']		= $item->full_name;
			$result['employee_number']	= $item->employee_number;
			$result['shift']			= $item->shift;
			$result['position_name']	= $item->position_name;
			$result['action'] 			=
				'<a class="btn-sm btn-success" href="'.base_url("roemployeeshift/form/".$item->employee_id."/".$item->id).'"><i class="fas fa-edit"></i></a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function save(){

		$shift_id 	= $this->input->post('shift_id');
		foreach ($this->input->post('id') as $employee_id)
		{
			$this->load->model(array('roemployeeshift_model'));
			$insert['id']			= '';
			
			$exist 					= $this->roemployeeshift_model->get(array('columns'=>'A.id', 'employee_id'=>$employee_id));
			if($exist){
				$insert['id']		= $exist->id;
			}

			$insert['employee_id']	= $employee_id;
			$insert['shift']		= $shift_id;
			$result = $this->roemployeeshift_model->save($insert);
			if (!$result) {
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pengajuan gagal.','danger'));
				redirect(base_url('roemployeeshift/list'));
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Pegajuan Sukses.','success'));
		redirect(base_url('roemployeeshift/list'));
	}
}
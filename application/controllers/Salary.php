<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Salary extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function list(){
		$data['_TITLE_'] 		= 'Format Gaji Karyawan';
		$data['_PAGE_'] 		= 'salary/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'salary';
		$this->view($data);
	}

	public function list_ajax(){

		$this->load->model(array('site_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "B.code AS company_code, A.id, A.name AS site_name, A.address AS site_address";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['company_code']	= $_POST['columns'][1]['search']['value'];
		$params['site_name']	= $_POST['columns'][2]['search']['value'];
		$params['site_address']	= $_POST['columns'][3]['search']['value'];
		
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		$list 	= $this->site_model->gets_ro_site($params);
		$total 	= $this->site_model->gets_ro_site($params, TRUE);
		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['company_code']	= $item->company_code;
			$result['site_name']	= $item->site_name;
			$result['site_address']	= $item->site_address;
			$result['action'] 		=
				'
				<a class="btn-sm btn-success btn-block" href="'.base_url("salary/form/".$item->id).'">Ubah</a>
				<a class="btn-sm btn-primary btn-block" href="'.base_url("salary/site/".$item->id).'">Detail</a>';
			array_push($data, $result);
			$i++;
		}
				
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}

	public function site($site_id = FALSE){
		
		$this->load->model(array('site_model'));
		
		$data['_TITLE_'] 		= 'Format Gaji';
		$data['_PAGE_'] 		= 'salary/site';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'salary';
		if(!$site_id){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('salary/list'));
		}
		if(!in_array($this->session->userdata('role'), $this->config->item('administrator')) && $site_id <= 2){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('salary/list'));
		}
		$data['site']			= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.id, A.name, A.address'));
		$this->view($data);
	}

	public function site_ajax($site_id = FALSE){

		$this->load->model(array('employee_salary_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id AS employee_id, A.full_name, A.employee_number, A.id_card, B.basic_salary, D.name AS position_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['id_card']			= $_POST['columns'][1]['search']['value'];
		$params['employee_number']	= $_POST['columns'][2]['search']['value'];
		$params['full_name']		= $_POST['columns'][3]['search']['value'];
		$params['position_name']	= $_POST['columns'][4]['search']['value'];
		$params['basic_salary']		= $_POST['columns'][5]['search']['value'];
		
		$params['site_id']		=  $site_id;

		$list 	= $this->employee_salary_model->gets($params);
		$total 	= $this->employee_salary_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{			
			$result['no'] 				= $i;
			$result['id_card']			= $item->id_card;
			$result['employee_number']	= $item->employee_number;
			$result['full_name']		= $item->full_name;
			$result['basic_salary']		= $item->basic_salary;
			$result['position_name']	= $item->position_name;
			$result['action'] 			= '<a class="btn-sm btn-success btn-block" href="'.base_url("salary/form_employee/".$site_id."/".$item->employee_id).'">Ubah</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function export($site_id = FALSE)
	{
		$this->load->model(array('employee_salary_model', 'cutoff_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$params 				= $this->input->get();
		$params['columns'] 		= "A.id AS employee_id, A.full_name, A.employee_number, A.id_card, B.join_date, B.basic_salary, B.bpjs_ks, B.bpjs_tk, B.bpjs_ks_employee, B.bpjs_tk_employee, D.name AS position_name";
		$params['orderby'] 		= 'A.full_name';
		$params['order']		= 'ASC';	
		$params['site_id']		= $site_id;

		$title = 'format_gaji';
		$files = glob(FCPATH."files/".$title."/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
		->setLastModifiedBy($this->session->userdata('name'))
		->setTitle($title)
		->setSubject($title)
		->setDescription($title)
		->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);
		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "NO");
		$excel->getActiveSheet()->setCellValue("B".$i, "NIK");
		$excel->getActiveSheet()->setCellValue("C".$i, "NIP");
		$excel->getActiveSheet()->setCellValue("D".$i, "Nama Karyawan");
		$excel->getActiveSheet()->setCellValue("E".$i, "Jabatan");
		$excel->getActiveSheet()->setCellValue("F".$i, "Tanggal Masuk");
		$excel->getActiveSheet()->setCellValue("G".$i, "Gaji Pokok");
		$excel->getActiveSheet()->setCellValue("H".$i, "BPJS Kesehatan");
		$excel->getActiveSheet()->setCellValue("I".$i, "BPJS Ketenagakerjaan");
		$excel->getActiveSheet()->setCellValue("J".$i, "Potongan BPJS Kesehatan");
		$excel->getActiveSheet()->setCellValue("K".$i, "Potongan BPJS Ketenagakerjaan");

		$header_column = 'L';
		$list_componet = $this->employee_salary_model->gets_detail(array('site_id' => $site_id, 'columns' => 'A.name', 'groupby' => 'A.name',));
		foreach($list_componet AS $value){
			$excel->getActiveSheet()->setCellValue($header_column.$i, $value->name);
			$header_column++;
		}

		$list 	= $this->employee_salary_model->gets($params);
		$i++;
		$no = 1;
		foreach($list as $item)
		{
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $no);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->employee_number);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValue("E".$i, $item->position_name);
			
			$date = new DateTime($item->join_date);
			$excel->getActiveSheet()->setCellValue("F".$i, PHPExcel_Shared_Date::PHPToExcel($date));
			$excel->getActiveSheet()->getStyle("F".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);

			$excel->getActiveSheet()->setCellValueExplicit("G".$i, $item->basic_salary);
			$excel->getActiveSheet()->setCellValueExplicit("H".$i, $item->bpjs_ks);
			$excel->getActiveSheet()->setCellValueExplicit("I".$i, $item->bpjs_tk);
			$excel->getActiveSheet()->setCellValueExplicit("J".$i, $item->bpjs_ks_employee);
			$excel->getActiveSheet()->setCellValueExplicit("K".$i, $item->bpjs_tk_employee);

			$column = 'L';
			foreach($list_componet AS $value){
				$detail = $this->employee_salary_model->get_detail(array('employee_id' => $item->employee_id, 'name' => $value->name, 'A.value'));
				if($detail){
					$excel->getActiveSheet()->setCellValue($column.$i, $detail->value);
				}
				$column++;
			}
			$no++;
			$i++;
		}
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function import($site_id = FALSE)
	{
		if(!$_FILES['file']['name'] == ""){
			$this->load->model(array('employee_salary_model', 'employee_model'));
			$this->load->library("Excel");
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			$rend			= $sheet->getHighestRow();

			$salary_component = array();
			foreach( range('L', 'ZZ') as $elements) {
				$title = $sheet->getCell($elements.'1')->getValue();
				if($title ==  null){
					break;
				}
				$salary_component[$elements] = $title;
			}

			for ($row = 2; $row <= $rend; $row++) {
				$id_card				= replace_null($sheet->getCell('B'.$row)->getValue());
				if($id_card == ''){
					continue;
				}

				$employee = $this->employee_model->get(array('id_card' => $id_card, 'columns' => 'A.id'));
				if(!$employee){
					continue;
				}
				$data['employee_id']		= $employee->id;
				$excel_date 				= $sheet->getCell('F'.$row)->getValue();
				$unix_date 					= ($excel_date - 25569) * 86400;
				$excel_date 				= 25569 + ($unix_date / 86400);
				$unix_date 					= ($excel_date - 25569) * 86400;
				$data['join_date']			= gmdate("Y-m-d", $unix_date);
				$data['basic_salary']		= $sheet->getCell('G'.$row)->getValue();
				if($data['basic_salary'] == null){
					continue;
				}
				$data['bpjs_ks']			= $sheet->getCell('H'.$row)->getValue();
				$data['bpjs_tk']			= $sheet->getCell('I'.$row)->getValue();
				$data['bpjs_ks_employee']	= $sheet->getCell('J'.$row)->getValue();
				$data['bpjs_tk_employee']	= $sheet->getCell('K'.$row)->getValue();
				$this->employee_salary_model->save($data);

				if(!empty($salary_component)){
					$this->employee_salary_model->delete_detail(array('employee_id' => $data['employee_id']));
					$detail['employee_id'] 	= $employee->id;
					foreach($salary_component AS $key => $value ){
						$detail['value']= replace_null($sheet->getCell($key.$row)->getValue());
						$detail['name']	= $value;
						$this->employee_salary_model->save_detail($detail);
					}
				}
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
		}
		redirect(base_url('salary/site/'.$site_id));
	}

	public function form($site_id = FALSE)
	{
		$this->load->model(array('site_salary_model'));

		if($this->input->post()){
			$data['site_id'] 			= $this->input->post('site_id');
			$data['basic_salary']		= str_replace(".", "", $this->input->post('basic_salary'));
			$data['bpjs_ks']			= str_replace(".", "", $this->input->post('bpjs_ks'));
			$data['bpjs_tk']			= str_replace(".", "", $this->input->post('bpjs_tk'));
			$data['bpjs_ks_employee']	= str_replace(".", "", $this->input->post('bpjs_ks_employee'));
			$data['bpjs_tk_employee']	= str_replace(".", "", $this->input->post('bpjs_tk_employee'));

			$this->form_validation->set_rules('site_id', '', 'required');
			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$this->site_salary_model->save($data);
				$this->site_salary_model->delete_detail(array('site_id' => $data['site_id']));
				
				$salary_detail_name		= $this->input->post('salary_detail_name');
				// $salary_detail_category	= $this->input->post('salary_detail_category');
				$salary_detail_value	= $this->input->post('salary_detail_value');
				foreach ($salary_detail_name AS $key => $value ) {
					if($salary_detail_value[$key] == ''){
						continue;
					}
					$data_detail['site_id']		= $data['site_id'];
					$data_detail['name'] 		= $salary_detail_name[$key];
					$data_detail['value']		= str_replace(".", "", $salary_detail_value[$key]);
					
					$this->site_salary_model->save_detail($data_detail);

				}
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			}
			redirect(base_url('salary/form/'.$data['site_id']));
			exit();
		}

		if ($site_id)
		{
			$data = (array) $this->site_salary_model->get(array('site_id' => $site_id, 'columns' => 'A.id AS site_id, A.name AS site_name, A.address AS site_address, B.basic_salary, B.bpjs_ks, B.bpjs_tk, B.bpjs_ks_employee, B.bpjs_tk_employee'));
	
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('salary/list'));
		}

		$data['salary_detail'] 	= $this->site_salary_model->gets_detail(array('site_id' => $site_id));
		$data['_TITLE_'] 		= 'Format Gaji';
		$data['_PAGE_'] 		= 'salary/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'salary';
		return $this->view($data);
	}

	public function form_employee($site_id = FALSE, $employee_id = FALSE)
	{
		$this->load->model(array('employee_salary_model'));

		if($this->input->post()){
			$data['employee_id'] 		= $this->input->post('employee_id');
			$data['basic_salary']		= str_replace(".", "", $this->input->post('basic_salary'));
			$data['bpjs_ks']			= str_replace(".", "", $this->input->post('bpjs_ks'));
			$data['bpjs_tk']			= str_replace(".", "", $this->input->post('bpjs_tk'));
			$data['bpjs_ks_employee']	= str_replace(".", "", $this->input->post('bpjs_ks_employee'));
			$data['bpjs_tk_employee']	= str_replace(".", "", $this->input->post('bpjs_tk_employee'));

			$this->form_validation->set_rules('employee_id', '', 'required');
			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$this->employee_salary_model->save($data);
				$this->employee_salary_model->delete_detail(array('employee_id' => $data['employee_id']));
				
				$salary_detail_name		= $this->input->post('salary_detail_name');
				$salary_detail_value	= $this->input->post('salary_detail_value');
				foreach ($salary_detail_name AS $key => $value ) {
					if($salary_detail_value[$key] == ''){
						continue;
					}
					$data_detail['employee_id']	= $data['employee_id'];
					$data_detail['name'] 		= $salary_detail_name[$key];
					$data_detail['value']		= str_replace(".", "", $salary_detail_value[$key]);
					
					$this->employee_salary_model->save_detail($data_detail);

				}
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			}
			redirect(base_url('salary/form_employee/'.$site_id.'/'.$employee_id));
			exit();
		}

		if ($employee_id)
		{
			$data = (array) $this->employee_salary_model->get(array('employee_id' => $employee_id, 'columns' => 'A.id AS employee_id, A.full_name, A.id_card, B.basic_salary, B.bpjs_ks, B.bpjs_tk, B.bpjs_ks_employee, B.bpjs_tk_employee'));
	
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('salary/site/'.$site_id));
		}

		$data['salary_detail'] 	= $this->employee_salary_model->gets_detail(array('employee_id' => $employee_id));
		$data['site_id'] 		= $site_id;
		$data['_TITLE_'] 		= 'Format Gaji';
		$data['_PAGE_'] 		= 'salary/form_employee';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'salary';
		return $this->view($data);
	}

	public function generate($site_id = FALSE){
		if($site_id){
			$this->load->model(array('site_salary_model', 'employee_model', 'employee_salary_model'));
			$salary = $this->site_salary_model->get(array('site_id' => $site_id, 'columns' => 'B.basic_salary, B.bpjs_ks, B.bpjs_tk, B.bpjs_ks_employee, B.bpjs_tk_employee'));
			if($salary->basic_salary != ""){
				$salary_detail 	= $this->site_salary_model->gets_detail(array('site_id' => $site_id, 'columns' => 'A.name, A.value'));
				$list_employee = $this->employee_model->gets(array('site_id' => $site_id, 'status_approval' => 3, 'status_nonjob' => 0, 'status' => 1, 'columns' => 'A.id'));
				foreach($list_employee AS $employee){
					$check = $this->employee_salary_model->get(array('employee_id' => $employee->id, 'columns' => 'B.employee_id'));
					
					if($check->employee_id != ''){
						continue;
					}
					$salary = (array) $salary;
					$salary['employee_id'] = $employee->id;
					$this->employee_salary_model->save($salary);
					foreach($salary_detail AS $detail){
						$detail = (array) $detail;
						$detail['employee_id'] = $employee->id;
						$this->employee_salary_model->save_detail($detail);
					}
				}
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil digenerate.','success'));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Format Gaji pada site belum diatur.','danger'));				
			}
			redirect(base_url('salary/site/'.$site_id));
			die();
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('salary/list'));
		}
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Employee_contract extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function delete_contract($type = FALSE, $employee_id = FALSE, $contract_id = false)
	{
		$this->load->model('employee_contract_model');
		if ($contract_id){
            $data = (array) $this->employee_contract_model->get(array('id' => $contract_id));
			if (empty($data)){
                $this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
                redirect(base_url('employee_contract/contract/'.$type.'/'.$employee_id));
            }else{
				$insert = array('id' => $contract_id, 'is_active' => 0);
				$result = $this->employee_contract_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil dihapus.','success'));
					redirect(base_url('employee_contract/contract/'.$type.'/'.$employee_id));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal dihapus.','danger'));
					redirect(base_url('employee_contract/contract/'.$type.'/'.$employee_id));
				}
			}
        }else{
        	$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
            redirect(base_url('employee_contract/list/'.$type));
		}
    }

	public function save_contract($employee_id = FALSE)
	{
		if($this->input->post()){
			$this->load->model(array('employee_model', 'contract_model', 'employee_contract_model', 'employee_salary_model', 'city_model'));
		
			$ids 		= $this->input->post('id');
			$template_id= $this->input->post('template_id');
			$duration	= str_replace(" ", "", $this->input->post('duration'));
			$template 	= $this->contract_model->get(array('id' => $template_id, 'columns' => 'A.type, A.content'));
			$contract_type 		= '';
			$content 	= '';
			if($template){
				$contract_type 	= $template->type;
				$content 	= $template->content;
			}
			
			$insert['contract_type'] 	= $contract_type;
			$insert['contract_start'] 	= substr($duration, 6, 4).'-'.substr($duration, 3, 2).'-'.substr($duration, 0, 2);
			$insert['contract_end'] 	= substr($duration, 17, 4).'-'.substr($duration, 14, 2).'-'.substr($duration, 11, 2);
				
			$content = str_replace("{KONTRAK_MULAI}", $insert['contract_start'], $content);
			$content = str_replace("{KONTRAK_SELESAI}", $insert['contract_end'], $content);
			foreach ($ids as $employee_id) {
				$employee 	= $this->employee_model->get_detail(array('id' => $employee_id, 'columns' => 'A.full_name, A.employee_number, A.id_card, A.gender, A.date_birth, A.marital_status, A.city_card_id, A.site_id, A.address_card, B.code AS site_code, B.name AS site_name, B.address AS site_address, C.code AS company_code, C.name AS company_name, C.name AS company_address, D.name AS position, E.name AS city_birth'));
				$type 		= 'indirect';
				if($employee->site_id != '2'){
					$type 	= 'direct';
				}
				$month 		= substr($params['duration'], 3, 2);
				$nomor_pkwt = $employee->employee_number.'/'.$template->type.'-'.$type.'/HRD-'.$employee->company_code.'/'.$employee->site_code.'/'.get_roman_month($month).'/'.substr($params['duration'], 6, 4);
			
				$tmp = $content;
				$tmp = str_replace("{NOMOR_KONTRAK}", $nomor_pkwt, $tmp);
				$tmp = str_replace("{NOMOR_KARYAWAN}", $employee->employee_number, $tmp);
				$tmp = str_replace("{NAMA_LENGKAP}",$employee->full_name,$tmp);
				$tmp = str_replace("{NIK}", $employee->id_card, $tmp);

				$birth_date	= substr($employee->date_birth, 8, 2)." ".get_month(substr($employee->date_birth, 5, 2))." ".substr($employee->date_birth, 0, 4);
				$birth_place = str_replace("KOTA", "", strtoupper($employee->city_birth));
				$birth_place = str_replace("KABUPATEN", "", strtoupper($employee->city_birth));
				$tmp = str_replace("{TANGGAL_LAHIR}", $birth_date, $tmp);
				$tmp = str_replace("{TEMPAT_LAHIR}",$birth_place,$tmp);
				$tmp = str_replace("{JENIS_KELAMIN}",$employee->gender,$tmp);
				$tmp = str_replace("{STATUS_PERNIKAHAN}",$employee->marital_status,$tmp);

				$address_card	= $employee->address_card;
				$tmp = str_replace("{ALAMAT_KTP}",$address_card,$tmp);
				$tmp = str_replace("{JABATAN}",$employee->position,$tmp);
				$tmp = str_replace("{NAMA_SITE}",$employee->site_name,$tmp);
				$tmp = str_replace("{ALAMAT_SITE}",$employee->site_address,$tmp);
				$tmp = str_replace("{NAMA_BISNIS_UNIT}",$employee->company_name,$tmp);
				$tmp = str_replace("{ALAMAT_BISNIS_UNIT}",$employee->company_address,$tmp);

				$manager = $this->employee_model->get(array('position_id' => 628, 'site_id' => 2, 'columns' => 'A.full_name'));
				if($manager){
					$tmp = str_replace("{MANAGER_HRD}",$manager->full_name,$tmp);
				}
				$salary = $this->employee_salary_model->get(array('id' => $employee_id, 'columns' => 'B.basic_salary'));
				if($salary->basic_salary != ''){
					$tmp = str_replace("{GAJI POKOK}",format_rupiah($basic_salary),$tmp);
				}

				$pb 	= '';
				$super 	= '';
				if($template_id == 14 || $template_id == 15){
					$template_pb = $this->contract_model->get(array('id' => 11, 'columns' => 'A.content'));
					$pb 		 = $template_pb->content;

					$template_super = $this->contract_model->get(array('id' => 12, 'columns' => 'A.content'));
					$super 		 	= $template_super->content;
				}else if($template_id == 9 || $template_id == 10){
					$template_pb = $this->contract_model->get(array('id' => 6, 'columns' => 'A.content'));
					$pb 		 = $template_pb->content;

					$template_super = $this->contract_model->get(array('id' => 7, 'columns' => 'A.content'));
					$super 		 	= $template_super->content;
				}else if($template_id == 4 || $template_id == 5){
					$template_pb = $this->contract_model->get(array('id' => 1, 'columns' => 'A.content'));
					$pb 		 = $template_pb->content;

					$template_super = $this->contract_model->get(array('id' => 2, 'columns' => 'A.content'));
					$super 		 	= $template_super->content;
				}

				$tmp_pb = $pb;
				$tmp_pb = str_replace("{NOMOR_KONTRAK}", $nomor_pkwt, $tmp_pb);
				$tmp_pb = str_replace("{NOMOR_KARYAWAN}", $employee->employee_number, $tmp_pb);
				$tmp_pb = str_replace("{NAMA_LENGKAP}",$employee->full_name,$tmp_pb);
				$tmp_pb = str_replace("{NIK}", $employee->id_card, $tmp_pb);
				$tmp_pb = str_replace("{TANGGAL_LAHIR}", $birth_date, $tmp_pb);
				$tmp_pb = str_replace("{TEMPAT_LAHIR}",$birth_place,$tmp_pb);
				$tmp_pb = str_replace("{JENIS_KELAMIN}",$employee->gender,$tmp_pb);
				$tmp_pb = str_replace("{STATUS_PERNIKAHAN}",$employee->marital_status,$tmp_pb);
				$tmp_pb = str_replace("{ALAMAT_KTP}",$address_card,$tmp_pb);
				$tmp_pb = str_replace("{JABATAN}",$employee->position,$tmp_pb);
				$tmp_pb = str_replace("{NAMA_SITE}",$employee->site_name,$tmp_pb);
				$tmp_pb = str_replace("{ALAMAT_SITE}",$employee->site_address,$tmp_pb);
				$tmp_pb = str_replace("{NAMA_BISNIS_UNIT}",$employee->company_name,$tmp_pb);
				$tmp_pb = str_replace("{ALAMAT_BISNIS_UNIT}",$employee->company_address,$tmp_pb);
				$tmp_pb = $tmp_pb."<div width='100%' style='page-break-after:always;'><hr></div>";
				
				$tmp_super = $super;
				$tmp_super = str_replace("{NOMOR_KONTRAK}", $nomor_pkwt, $tmp_super);
				$tmp_super = str_replace("{NOMOR_KARYAWAN}", $employee->employee_number, $tmp_super);
				$tmp_super = str_replace("{NAMA_LENGKAP}",$employee->full_name,$tmp_super);
				$tmp_super = str_replace("{NIK}", $employee->id_card, $tmp_super);
				$tmp_super = str_replace("{TANGGAL_LAHIR}", $birth_date, $tmp_super);
				$tmp_super = str_replace("{TEMPAT_LAHIR}",$birth_place,$tmp_super);
				$tmp_super = str_replace("{JENIS_KELAMIN}",$employee->gender,$tmp_super);
				$tmp_super = str_replace("{STATUS_PERNIKAHAN}",$employee->marital_status,$tmp_super);
				$tmp_super = str_replace("{ALAMAT_KTP}",$address_card,$tmp_super);
				$tmp_super = str_replace("{JABATAN}",$employee->position,$tmp_super);
				$tmp_super = str_replace("{NAMA_SITE}",$employee->site_name,$tmp_super);
				$tmp_super = str_replace("{ALAMAT_SITE}",$employee->site_address,$tmp_super);
				$tmp_super = str_replace("{NAMA_BISNIS_UNIT}",$employee->company_name,$tmp_super);
				$tmp_super = str_replace("{ALAMAT_BISNIS_UNIT}",$employee->company_address,$tmp_super);
				$tmp_super = $tmp_super."<div width='100%' style='page-break-after:always;'><hr></div>";
				
				$insert['employee_id'] = $employee_id;
				$insert['contract'] 	= $tmp.'<hr>'.$tmp_pb.'<hr>'.$tmp_super;


				$result =  $this->employee_contract_model->save($insert);
				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger'));
					redirect(base_url('employee_contract/list/'.$type));
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
			redirect(base_url('employee_contract/list/'.$type));
		}
	}

	public function preview_contract()
	{
		$this->load->model(array('employee_model','contract_model','employee_salary_model'));
		
		$data['_TITLE_'] 		= 'Cetak Kontrak Kerja';
		$data['_PAGE_']	 		= 'employee/preview';
		$data['_MENU_PARENT_'] 	= 'personalia';
		$data['_MENU_'] 		= 'employee';
		$params = $this->input->get();
		$start = "";
		$end = "";
		if(isset($params['duration'])){
			$start 	= substr($params['duration'], 0, 2).' '.get_month(substr($params['duration'], 3, 2)).' '.substr($params['duration'], 6, 4);
			$end 	= substr($params['duration'], 11, 2).' '.get_month(substr($params['duration'], 14, 2)).' '.substr($params['duration'], 17, 4);
		}

		$template = $this->contract_model->get(array('id' => $params['template'], 'columns', 'A.content'));
		$content = '';
		if($template){
			$content = $template->content;
		}

		$pb 	= '';
		$super 	= '';
		if($params['template'] == 14 || $params['template'] == 15){
			$template_pb = $this->contract_model->get(array('id' => 11, 'columns' => 'A.content'));
			$pb 		 = $template_pb->content;

			$template_super = $this->contract_model->get(array('id' => 12, 'columns' => 'A.content'));
			$super 		 	= $template_super->content;
		}else if($params['template'] == 9 || $params['template'] == 10){
			$template_pb = $this->contract_model->get(array('id' => 6, 'columns' => 'A.content'));
			$pb 		 = $template_pb->content;

			$template_super = $this->contract_model->get(array('id' => 7, 'columns' => 'A.content'));
			$super 		 	= $template_super->content;
		}else if($params['template'] == 4 || $params['template'] == 5){
			$template_pb = $this->contract_model->get(array('id' => 1, 'columns' => 'A.content'));
			$pb 		 = $template_pb->content;

			$template_super = $this->contract_model->get(array('id' => 2, 'columns' => 'A.content'));
			$super 		 	= $template_super->content;
		}

		$content = str_replace("{KONTRAK_MULAI}", $start, $content);
		$content = str_replace("{KONTRAK_SELESAI}", $end, $content);
		$ids = explode(",", $params['id']);
		$result = "";
		foreach ($ids as $employee_id) {
			$employee 	= $this->employee_model->get_detail(array('id' => $employee_id, 'columns' => 'A.full_name, A.employee_number, A.id_card, A.gender, A.date_birth, A.marital_status, A.city_card_id, A.site_id, A.address_card, B.code AS site_code, B.name AS site_name, B.address AS site_address, C.code AS company_code, C.name AS company_name, C.name AS company_address, D.name AS position, E.name AS city_birth'));
			$type 		= 'INDIRECT';
			if($employee->site_id != '2'){
				$type 	= 'DIRECT';
			}
			$month 		= substr($params['duration'], 3, 2);
			$nomor_pkwt = $employee->employee_number.'/'.$template->type.'-'.$type.'/HRD-'.$employee->company_code.'/'.$employee->site_code.'/'.get_roman_month($month).'/'.substr($params['duration'], 6, 4);
			$tmp = $content;
			$tmp = str_replace("{NOMOR_KONTRAK}", $nomor_pkwt, $tmp);
			$tmp = str_replace("{NOMOR_KARYAWAN}", $employee->employee_number, $tmp);
			$tmp = str_replace("{NAMA_LENGKAP}",$employee->full_name,$tmp);
			$tmp = str_replace("{NIK}", $employee->id_card, $tmp);

			$birth_date	= substr($employee->date_birth, 8, 2)." ".get_month(substr($employee->date_birth, 5, 2))." ".substr($employee->date_birth, 0, 4);
			$birth_place = str_replace("KOTA", "", strtoupper($employee->city_birth));
			$birth_place = str_replace("KABUPATEN", "", strtoupper($employee->city_birth));
			$tmp = str_replace("{TANGGAL_LAHIR}", $birth_date, $tmp);
			$tmp = str_replace("{TEMPAT_LAHIR}",$birth_place,$tmp);
			$tmp = str_replace("{JENIS_KELAMIN}",$employee->gender,$tmp);
			$tmp = str_replace("{STATUS_PERNIKAHAN}",$employee->marital_status,$tmp);

			$address_card	= $employee->address_card;
			$tmp = str_replace("{ALAMAT_KTP}",$address_card,$tmp);
			$tmp = str_replace("{JABATAN}",$employee->position,$tmp);
			$tmp = str_replace("{NAMA_SITE}",$employee->site_name,$tmp);
			$tmp = str_replace("{ALAMAT_SITE}",$employee->site_address,$tmp);
			$tmp = str_replace("{NAMA_BISNIS_UNIT}",$employee->company_name,$tmp);
			$tmp = str_replace("{ALAMAT_BISNIS_UNIT}",$employee->company_address,$tmp);

			$manager = $this->employee_model->get(array('position_id' => 628, 'site_id' => 2, 'columns' => 'A.full_name'));
			if($manager){
				$tmp = str_replace("{MANAGER_HRD}",$manager->full_name,$tmp);
			}
			$salary = $this->employee_salary_model->get(array('id' => $employee_id, 'columns' => 'B.basic_salary'));
			if($salary->basic_salary != ''){
				$tmp = str_replace("{GAJI POKOK}",format_rupiah($basic_salary),$tmp);
			}

			$result .= $tmp."<div width='100%' style='page-break-after:always;'><hr></div>";

			$tmp_pb = $pb;
			$tmp_pb = str_replace("{NOMOR_KONTRAK}", $nomor_pkwt, $tmp_pb);
			$tmp_pb = str_replace("{NOMOR_KARYAWAN}", $employee->employee_number, $tmp_pb);
			$tmp_pb = str_replace("{NAMA_LENGKAP}",$employee->full_name,$tmp_pb);
			$tmp_pb = str_replace("{NIK}", $employee->id_card, $tmp_pb);
			$tmp_pb = str_replace("{TANGGAL_LAHIR}", $birth_date, $tmp_pb);
			$tmp_pb = str_replace("{TEMPAT_LAHIR}",$birth_place,$tmp_pb);
			$tmp_pb = str_replace("{JENIS_KELAMIN}",$employee->gender,$tmp_pb);
			$tmp_pb = str_replace("{STATUS_PERNIKAHAN}",$employee->marital_status,$tmp_pb);
			$tmp_pb = str_replace("{ALAMAT_KTP}",$address_card,$tmp_pb);
			$tmp_pb = str_replace("{JABATAN}",$employee->position,$tmp_pb);
			$tmp_pb = str_replace("{NAMA_SITE}",$employee->site_name,$tmp_pb);
			$tmp_pb = str_replace("{ALAMAT_SITE}",$employee->site_address,$tmp_pb);
			$tmp_pb = str_replace("{NAMA_BISNIS_UNIT}",$employee->company_name,$tmp_pb);
			$tmp_pb = str_replace("{ALAMAT_BISNIS_UNIT}",$employee->company_address,$tmp_pb);
			$result .= $tmp_pb."<div width='100%' style='page-break-after:always;'><hr></div>";

			$tmp_super = $super;
			$tmp_super = str_replace("{NOMOR_KONTRAK}", $nomor_pkwt, $tmp_super);
			$tmp_super = str_replace("{NOMOR_KARYAWAN}", $employee->employee_number, $tmp_super);
			$tmp_super = str_replace("{NAMA_LENGKAP}",$employee->full_name,$tmp_super);
			$tmp_super = str_replace("{NIK}", $employee->id_card, $tmp_super);
			$tmp_super = str_replace("{TANGGAL_LAHIR}", $birth_date, $tmp_super);
			$tmp_super = str_replace("{TEMPAT_LAHIR}",$birth_place,$tmp_super);
			$tmp_super = str_replace("{JENIS_KELAMIN}",$employee->gender,$tmp_super);
			$tmp_super = str_replace("{STATUS_PERNIKAHAN}",$employee->marital_status,$tmp_super);
			$tmp_super = str_replace("{ALAMAT_KTP}",$address_card,$tmp_super);
			$tmp_super = str_replace("{JABATAN}",$employee->position,$tmp_super);
			$tmp_super = str_replace("{NAMA_SITE}",$employee->site_name,$tmp_super);
			$tmp_super = str_replace("{ALAMAT_SITE}",$employee->site_address,$tmp_super);
			$tmp_super = str_replace("{NAMA_BISNIS_UNIT}",$employee->company_name,$tmp_super);
			$tmp_super = str_replace("{ALAMAT_BISNIS_UNIT}",$employee->company_address,$tmp_super);
			$result .= $tmp_super."<div width='100%' style='page-break-after:always;'><hr></div>";
		}
		$data['content'] = $result;   
		$this->load->view('employee_contract/preview_contract', $data);
	}

	public function pdf_contract($type = FALSE)
	{
		if($this->input->post()){
			$this->load->model(array('employee_model', 'contract_model', 'employee_contract_model', 'employee_salary_model', 'city_model'));
			
			$files = glob(FCPATH."files/kontrak_karyawan_pdf/*");
			foreach($files as $file){
				gc_collect_cycles();
				if(is_file($file)) {
					unlink($file);
				}
			}
			$zip_name 			= "kontrak_karyawan_pdf.zip";
			$data['basepath'] 	= FCPATH."files/kontrak_karyawan_pdf/";

			$ids 		= $this->input->post('id');
			$template_id= $this->input->post('template_id');
			$duration	= str_replace(" ", "", $this->input->post('duration'));
			$template 	= $this->contract_model->get(array('id' => $template_id, 'columns' => 'A.type, A.content'));
			$start = "";
			$end = "";
			if(isset($duration)){
				$start 	= substr($duration, 0, 2).' '.get_month(substr($duration, 3, 2)).' '.substr($duration, 6, 4);
				$end 	= substr($duration, 11, 2).' '.get_month(substr($duration, 14, 2)).' '.substr($duration, 17, 4);
			}
			$contract_type 		= '';
			$content 	= '';
			if($template){
				$contract_type 	= $template->type;
				$content 	= $template->content;
			}
			$content = str_replace("{KONTRAK_MULAI}", $start, $content);
			$content = str_replace("{KONTRAK_SELESAI}", $end, $content);

			$data['content'] 	= '';
			$data['title'] 		=  'kontrak_karyawan_'.$type;
			foreach ($ids as $employee_id) {
				$employee 	= $this->employee_model->get_detail(array('id' => $employee_id, 'columns' => 'A.full_name, A.employee_number, A.id_card, A.gender, A.date_birth, A.marital_status, A.city_card_id, A.site_id, A.address_card, B.code AS site_code, B.name AS site_name, B.address AS site_address, C.code AS company_code, C.name AS company_name, C.name AS company_address, D.name AS position, E.name AS city_birth'));
				$type 		= 'INDIRECT';
				if($employee->site_id != '2'){
					$type 	= 'DIRECT';
				}
				$month 		= substr($duration, 3, 2);
				$nomor_pkwt = $employee->employee_number.'/'.$template->type.'-'.$type.'/HRD-'.$employee->company_code.'/'.$employee->site_code.'/'.get_roman_month($month).'/'.substr($duration, 6, 4);
				$tmp = $content;
				$tmp = str_replace("{NOMOR_KONTRAK}", $nomor_pkwt, $tmp);
				$tmp = str_replace("{NOMOR_KARYAWAN}", $employee->employee_number, $tmp);
				$tmp = str_replace("{NAMA_LENGKAP}",$employee->full_name,$tmp);
				$tmp = str_replace("{NIK}", $employee->id_card, $tmp);

				$birth_date	= substr($employee->date_birth, 8, 2)." ".get_month(substr($employee->date_birth, 5, 2))." ".substr($employee->date_birth, 0, 4);
				$birth_place = str_replace("KOTA ", "", strtoupper($employee->city_birth));
				$birth_place = str_replace("KABUPATEN ", "", strtoupper($employee->city_birth));
				$tmp = str_replace("{TANGGAL_LAHIR}", $birth_date, $tmp);
				$tmp = str_replace("{TEMPAT_LAHIR}",$birth_place,$tmp);
				$tmp = str_replace("{JENIS_KELAMIN}",$employee->gender,$tmp);
				$tmp = str_replace("{STATUS_PERNIKAHAN}",$employee->marital_status,$tmp);

				$address_card	= $employee->address_card;
				$tmp = str_replace("{ALAMAT_KTP}",$address_card,$tmp);
				$tmp = str_replace("{JABATAN}",$employee->position,$tmp);
				$tmp = str_replace("{NAMA_SITE}",$employee->site_name,$tmp);
				$tmp = str_replace("{ALAMAT_SITE}",$employee->site_address,$tmp);
				$tmp = str_replace("{NAMA_BISNIS_UNIT}",$employee->company_name,$tmp);
				$tmp = str_replace("{ALAMAT_BISNIS_UNIT}",$employee->company_address,$tmp);

				$manager = $this->employee_model->get(array('position_id' => 628, 'site_id' => 2, 'columns' => 'A.full_name'));
				if($manager){
					$tmp = str_replace("{MANAGER_HRD}",$manager->full_name,$tmp);
				}
				$salary = $this->employee_salary_model->get(array('id' => $employee_id, 'columns' => 'B.basic_salary'));
				if($salary->basic_salary != ''){
					$tmp = str_replace("{GAJI POKOK}",format_rupiah($basic_salary),$tmp);
				}
				$data['content'] = $tmp;
				$data['title'] 	 =  preg_replace("/[^A-Za-z0-9 -]/", "", strtolower($employee->full_name."-".$employee->id_card.'-'.'kontrak-karyawan'));
				$data['footer']		= 'HRD/'.$employee->company_code.'/'.$employee->site_code.'/'.date('Y');
				$this->generate_PDF($data);

				$pb 	= '';
				$super 	= '';
				if($template_id == 14 || $template_id == 15){
					$template_pb = $this->contract_model->get(array('id' => 11, 'columns' => 'A.content'));
					$pb 		 = $template_pb->content;

					$template_super = $this->contract_model->get(array('id' => 12, 'columns' => 'A.content'));
					$super 		 	= $template_super->content;
				}else if($template_id == 9 || $template_id == 10){
					$template_pb = $this->contract_model->get(array('id' => 6, 'columns' => 'A.content'));
					$pb 		 = $template_pb->content;

					$template_super = $this->contract_model->get(array('id' => 7, 'columns' => 'A.content'));
					$super 		 	= $template_super->content;
				}else if($template_id == 4 || $template_id == 5){
					$template_pb = $this->contract_model->get(array('id' => 1, 'columns' => 'A.content'));
					$pb 		 = $template_pb->content;

					$template_super = $this->contract_model->get(array('id' => 2, 'columns' => 'A.content'));
					$super 		 	= $template_super->content;
				}

				$tmp_pb = $pb;
				$tmp_pb = str_replace("{NOMOR_KONTRAK}", $nomor_pkwt, $tmp_pb);
				$tmp_pb = str_replace("{NOMOR_KARYAWAN}", $employee->employee_number, $tmp_pb);
				$tmp_pb = str_replace("{NAMA_LENGKAP}",$employee->full_name,$tmp_pb);
				$tmp_pb = str_replace("{NIK}", $employee->id_card, $tmp_pb);
				$tmp_pb = str_replace("{TANGGAL_LAHIR}", $birth_date, $tmp_pb);
				$tmp_pb = str_replace("{TEMPAT_LAHIR}",$birth_place,$tmp_pb);
				$tmp_pb = str_replace("{JENIS_KELAMIN}",$employee->gender,$tmp_pb);
				$tmp_pb = str_replace("{STATUS_PERNIKAHAN}",$employee->marital_status,$tmp_pb);
				$tmp_pb = str_replace("{ALAMAT_KTP}",$address_card,$tmp_pb);
				$tmp_pb = str_replace("{JABATAN}",$employee->position,$tmp_pb);
				$tmp_pb = str_replace("{NAMA_SITE}",$employee->site_name,$tmp_pb);
				$tmp_pb = str_replace("{ALAMAT_SITE}",$employee->site_address,$tmp_pb);
				$tmp_pb = str_replace("{NAMA_BISNIS_UNIT}",$employee->company_name,$tmp_pb);
				$tmp_pb = str_replace("{ALAMAT_BISNIS_UNIT}",$employee->company_address,$tmp_pb);
				$result .= $tmp_pb."<div width='100%' style='page-break-after:always;'><hr></div>";
				$data['content'] = $tmp_pb;
				$data['title'] =  preg_replace("/[^A-Za-z0-9 -]/", "", strtolower($employee->full_name."-".$employee->id_card.'-'.'perjanjian-bersama'));
				$data['footer']		= '';
				$this->generate_PDF($data);

				$tmp_super = $super;
				$tmp_super = str_replace("{NOMOR_KONTRAK}", $nomor_pkwt, $tmp_super);
				$tmp_super = str_replace("{NOMOR_KARYAWAN}", $employee->employee_number, $tmp_super);
				$tmp_super = str_replace("{NAMA_LENGKAP}",$employee->full_name,$tmp_super);
				$tmp_super = str_replace("{NIK}", $employee->id_card, $tmp_super);
				$tmp_super = str_replace("{TANGGAL_LAHIR}", $birth_date, $tmp_super);
				$tmp_super = str_replace("{TEMPAT_LAHIR}",$birth_place,$tmp_super);
				$tmp_super = str_replace("{JENIS_KELAMIN}",$employee->gender,$tmp_super);
				$tmp_super = str_replace("{STATUS_PERNIKAHAN}",$employee->marital_status,$tmp_super);
				$tmp_super = str_replace("{ALAMAT_KTP}",$address_card,$tmp_super);
				$tmp_super = str_replace("{JABATAN}",$employee->position,$tmp_super);
				$tmp_super = str_replace("{NAMA_SITE}",$employee->site_name,$tmp_super);
				$tmp_super = str_replace("{ALAMAT_SITE}",$employee->site_address,$tmp_super);
				$tmp_super = str_replace("{NAMA_BISNIS_UNIT}",$employee->company_name,$tmp_super);
				$tmp_super = str_replace("{ALAMAT_BISNIS_UNIT}",$employee->company_address,$tmp_super);
				$result .= $tmp_super."<div width='100%' style='page-break-after:always;'><hr></div>";
				$data['content'] = $tmp_super;
				$data['title'] =  preg_replace("/[^A-Za-z0-9 -]/", "", strtolower($employee->full_name."-".$employee->id_card.'-'.'surat-pernyataan'));
				$data['footer']		= '';
				$this->generate_PDF($data);
			}

			$rootPath = $data['basepath'];
			$zip = new ZipArchive();
			$zip->open('files/'.$zip_name, ZipArchive::CREATE | ZipArchive::OVERWRITE);
			$files = new RecursiveIteratorIterator(
				new RecursiveDirectoryIterator($rootPath),
				RecursiveIteratorIterator::LEAVES_ONLY
			);

			foreach ($files as $name => $file)
			{
				if (!$file->isDir())
				{
					$filePath = $file->getRealPath();
					$relativePath = substr($filePath, strlen($rootPath));
					$zip->addFile($filePath, $relativePath);
				}
			}
			$zip->close();
			redirect(base_url('files/'.$zip_name));
			die();
		}
		$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
		redirect(base_url('employee_contract/list/'.$type));
	}

	public function contract($type = FALSE, $employee_id = FALSE)
	{
		if (!$employee_id)
		{
			$data["message"] = message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger');
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
			redirect(base_url('employee_contract/list/'.$type));
		}

		$this->load->model(array('employee_model', 'employee_contract_model', 'contract_model'));
		$data['employee'] = $this->employee_model->get(array('id'=>$employee_id, 'columns' => 'A.id, A.full_name, A.id_card, A.address_card, A.site_id'));
		$data['list_contract'] = $this->employee_contract_model->gets(array('employee_id'=>$employee_id, 'columns' => 'A.id, A.contract_type, DATE_FORMAT(A.contract_start, "%d/%m/%Y") AS contract_start, DATE_FORMAT(A.contract_end, "%d/%m/%Y") AS contract_end'));

		// $data['list_template'] 	= $this->contract_model->gets(array());
		$data['_TITLE_'] 		= 'Kontrak Karyawan '.$data['employee']->full_name;
		$data['_PAGE_'] 		= 'employee_contract/contract';
		$data['_MENU_PARENT_'] 	= $type;
		$data['_MENU_'] 		= 'employee_contract_'.$type;
		
		$this->view($data);
	}

	public function export()
	{
		$this->load->model(array('employee_model', 'city_model', 'site_model', 'employee_skill_model', 'position_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = "Data Karyawan";
		$params = $this->input->get();
		
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		if(isset($params['expired'])){
    		if($params['expired'] == ''){
    			unset($params['expired']);
    		}
		}

		if($params['status'] == 'kontrak_karyawan'){
			$params['status'] = 1;
			$params['status_approval'] 	= 3;
			$params['status_nonjob'] 	= 0;
			$title = 'kontrak_karyawan_'.$params['type'];
			$files = glob(FCPATH."files/".$title."/*");
		}

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		if(isset($params['type'])){
			if($params['type'] == 'indirect'){
				$params['site_id'] = 2;
			}else{
				$params['not_site_id']		= 2;
			}
			unset($params['type']);
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Nomor Induk Karyawan");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("C".$i, "Nomor Identitas (KTP/SIM)");
		$excel->getActiveSheet()->setCellValue("D".$i, "Masa Berlaku (KTP/SIM)");
		$excel->getActiveSheet()->setCellValue("E".$i, "Tempat Lahir");
		$excel->getActiveSheet()->setCellValue("F".$i, "Tanggal Lahir");
		$excel->getActiveSheet()->setCellValue("G".$i, "Kota Sesuai KTP");
		$excel->getActiveSheet()->setCellValue("H".$i, "Alamat Sesuai KTP");
		$excel->getActiveSheet()->setCellValue("I".$i, "Kota Domisili");
		$excel->getActiveSheet()->setCellValue("J".$i, "Alamat Domisili");
		$excel->getActiveSheet()->setCellValue("K".$i, "Status Rumah");
		$excel->getActiveSheet()->setCellValue("L".$i, "No. Telephon/HP");
		$excel->getActiveSheet()->setCellValue("M".$i, "Email");
		$excel->getActiveSheet()->setCellValue("N".$i, "Facebook");
		$excel->getActiveSheet()->setCellValue("O".$i, "Instagram");
		$excel->getActiveSheet()->setCellValue("P".$i, "Twitter");
		$excel->getActiveSheet()->setCellValue("Q".$i, "Agama");
		$excel->getActiveSheet()->setCellValue("R".$i, "Status Pernikahan");
		$excel->getActiveSheet()->setCellValue("S".$i, "Jenis Kelamin");
		$excel->getActiveSheet()->setCellValue("T".$i, "Tinggi");
		$excel->getActiveSheet()->setCellValue("U".$i, "Berat");
		$excel->getActiveSheet()->setCellValue("V".$i, "Pendidikan Terakhir");
		$excel->getActiveSheet()->setCellValue("W".$i, "Jurusan");
		$excel->getActiveSheet()->setCellValue("X".$i, "Site Bisnis");
		$excel->getActiveSheet()->setCellValue("Y".$i, "Jabatan");
		$excel->getActiveSheet()->setCellValue("Z".$i, "Branch");
		$excel->getActiveSheet()->setCellValue("AA".$i, "Nama Ibu");
		$excel->getActiveSheet()->setCellValue("AB".$i, "Nama Pasangan");
		$excel->getActiveSheet()->setCellValue("AC".$i, "Nama Anak");
		$excel->getActiveSheet()->setCellValue("AD".$i, "Kode PTKP");
		$excel->getActiveSheet()->setCellValue("AE".$i, "BPJS TK");
		$excel->getActiveSheet()->setCellValue("AF".$i, "Asuransi Kesehatan");
		$excel->getActiveSheet()->setCellValue("AG".$i, "No Rekening");
		$excel->getActiveSheet()->setCellValue("AH".$i, "Atas Nama");
		$excel->getActiveSheet()->setCellValue("AI".$i, "NPWP");
		$excel->getActiveSheet()->setCellValue("AJ".$i, "Hasil Interview");
		$excel->getActiveSheet()->setCellValue("AK".$i, "Surat Lamaran Pekerjaan");
		$excel->getActiveSheet()->setCellValue("AL".$i, "Lampiran Riwayat Hidup");
		$excel->getActiveSheet()->setCellValue("AM".$i, "Foto KTP");
		$excel->getActiveSheet()->setCellValue("AN".$i, "Foto KK");
		$excel->getActiveSheet()->setCellValue("AO".$i, "Foto");
		$excel->getActiveSheet()->setCellValue("AP".$i, "Surat Sehat");
		$excel->getActiveSheet()->setCellValue("AQ".$i, "Foto Buku Nikah");
		$excel->getActiveSheet()->setCellValue("AR".$i, "SKCK");
		$excel->getActiveSheet()->setCellValue("AS".$i, "FOTO NPWP");
		$excel->getActiveSheet()->setCellValue("AT".$i, "Keahlian/sertifikat:nomor:url Dokumen,");

		$params['columns'] 		= 'A.*, B.name AS site_name, F.name AS branch_name, G.name AS city_birth, H.name AS city_card, I.name AS city_domisili, A.date_birth, E.name AS position_name';

		$list_candidate = $this->employee_model->gets($params);
		$i=2;
		foreach ($list_candidate as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->employee_number);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->id_card_period);
			$excel->getActiveSheet()->setCellValue("E".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_birth));

			$date = new DateTime($item->date_birth);
			$excel->getActiveSheet()->setCellValue("F".$i, PHPExcel_Shared_Date::PHPToExcel($date));
			$excel->getActiveSheet()->getStyle("F".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			
			// $excel->getActiveSheet()->setCellValue("F".$i, $item->date_birth2);
			$excel->getActiveSheet()->setCellValue("G".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_card));
			$excel->getActiveSheet()->setCellValue("H".$i, $item->address_card);
			$excel->getActiveSheet()->setCellValue("I".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_domisili));
			$excel->getActiveSheet()->setCellValue("J".$i, $item->address_domisili);
			$excel->getActiveSheet()->setCellValue("K".$i, $item->status_residance);
			$excel->getActiveSheet()->setCellValueExplicit("L".$i, $item->phone_number);
			$excel->getActiveSheet()->setCellValue("M".$i, $item->email);
			$excel->getActiveSheet()->setCellValue("N".$i, $item->socmed_fb);
			$excel->getActiveSheet()->setCellValue("O".$i, $item->socmed_ig);
			$excel->getActiveSheet()->setCellValue("P".$i, $item->socmed_tw);
			$excel->getActiveSheet()->setCellValue("Q".$i, $item->religion);
			$excel->getActiveSheet()->setCellValue("R".$i, $item->marital_status);
			$excel->getActiveSheet()->setCellValue("S".$i, $item->gender);
			$excel->getActiveSheet()->setCellValue("T".$i, $item->heigh);
			$excel->getActiveSheet()->setCellValue("U".$i, $item->weigh);
			$excel->getActiveSheet()->setCellValue("V".$i, $item->education_level);
			$excel->getActiveSheet()->setCellValue("W".$i, $item->education_majors);

			$excel->getActiveSheet()->setCellValue("X".$i, $item->site_name);
			$excel->getActiveSheet()->setCellValue("Y".$i, $item->position_name);
			$excel->getActiveSheet()->setCellValue("Z".$i, $item->branch_name);
			$excel->getActiveSheet()->setCellValue("AA".$i, $item->family_mother);
			$excel->getActiveSheet()->setCellValue("AB".$i, $item->family_mate);
			$excel->getActiveSheet()->setCellValue("AC".$i, $item->family_child);
			$excel->getActiveSheet()->setCellValue("AD".$i, $item->ptkp);
			$excel->getActiveSheet()->setCellValue("AE".$i, $item->benefit_labor);
			$excel->getActiveSheet()->setCellValue("AF".$i, $item->benefit_health);
			$excel->getActiveSheet()->setCellValueExplicit("AG".$i, $item->bank_account);
			$excel->getActiveSheet()->setCellValue("AH".$i, $item->bank_account_name);
			$excel->getActiveSheet()->setCellValue("AI".$i, $item->tax_number);
			$excel->getActiveSheet()->setCellValue("AJ".$i, $item->document_interview);
			$excel->getActiveSheet()->setCellValue("AM".$i, $item->document_id_card);
			$excel->getActiveSheet()->setCellValue("AN".$i, $item->document_family_card);
			$excel->getActiveSheet()->setCellValue("AO".$i, $item->document_photo);
			$excel->getActiveSheet()->setCellValue("AP".$i, $item->document_doctor_note);
			$excel->getActiveSheet()->setCellValue("AQ".$i, $item->document_marriage_certificate);
			$excel->getActiveSheet()->setCellValue("AR".$i, $item->document_police_certificate);
			$excel->getActiveSheet()->setCellValue("AS".$i, $item->document_tax_number);

			$list_skill 	= $this->employee_skill_model->gets(array('employee_id' => $item->id, 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));
			$skill = "";
			foreach ($list_skill as $item_skill) {
				$skill .=" ".$item_skill->certificate_name.":".$item_skill->certificate_number.":".$item_skill->certificate_document.",";
			}

			$excel->getActiveSheet()->setCellValue("AT".$i, $skill);
			$i++;
		}
		$excel->getActiveSheet()->setTitle($title);
		
		$excel->createSheet();
		$excel->setActiveSheetIndex(1);
		$excel->getActiveSheet()->setCellValue('A1', 'ID Kota');
		$excel->getActiveSheet()->setCellValue('B1', 'Nama Kota');

		$list_city	= $this->city_model->gets(array('orderby' => 'A.name', 'order' => 'ASC'));
		$j=2;
		foreach ($list_city as $item) {
			$excel->getActiveSheet()->setCellValue("A".$j,$item->id);
			$excel->getActiveSheet()->setCellValue("B".$j,$item->name);
			$j++;
		}
		$excel->getActiveSheet()->setTitle('Daftar Kota');

		$excel->createSheet();
		$excel->setActiveSheetIndex(2);
		$excel->getActiveSheet()->setCellValue('A1', 'ID Site');
		$excel->getActiveSheet()->setCellValue('B1', 'Nama Site Bisnis');
		$excel->getActiveSheet()->setCellValue('C1', 'Nama Unit Kontrak');

		$list_site 	= $this->site_model->gets(array('orderby' => 'A.name', 'order' => 'ASC'));
		$k=2;
		foreach ($list_site as $item) {
			$excel->getActiveSheet()->setCellValue("A".$k,$item->id);
			$excel->getActiveSheet()->setCellValue("B".$k,$item->name);
			$excel->getActiveSheet()->setCellValue("C".$k,$item->company_name);
			$k++;
		}
		$excel->getActiveSheet()->setTitle('Daftar Site');
		
		$excel->createSheet();
		$excel->setActiveSheetIndex(3);
		$excel->getActiveSheet()->setCellValue('A1', 'ID Jabatan');
		$excel->getActiveSheet()->setCellValue('B1', 'Nama Jabatan');

		$list_position 	= $this->position_model->gets(array('orderby' => 'A.name', 'order' => 'ASC'));
		$l=2;
		foreach ($list_position as $item) {
			$excel->getActiveSheet()->setCellValue("A".$l,$item->id);
			$excel->getActiveSheet()->setCellValue("B".$l,$item->name);
			$l++;
		}
		$excel->getActiveSheet()->setTitle('Daftar Jabatan');
		
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function export_contract($type = FALSE)
	{
		$this->load->model(array('employee_model', 'employee_contract_model', 'city_model', 'branch_model', 'position_model', 'employee_salary_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = 'kontrak_karyawan_'.$type;
		$files = glob(FCPATH."files/".$title."/*");
		
		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "NO");
		$excel->getActiveSheet()->setCellValue("B".$i, "NAMA LENGKAP");
		$excel->getActiveSheet()->setCellValue("C".$i, "DOJ");
		$excel->getActiveSheet()->setCellValue("D".$i, "SITE");
		$excel->getActiveSheet()->setCellValue("E".$i, "STATUS");
		$excel->getActiveSheet()->setCellValue("F".$i, "NO.KONTRAK");
		$excel->getActiveSheet()->setCellValue("G".$i, "START");
		$excel->getActiveSheet()->setCellValue("H".$i, "END");
		$excel->getActiveSheet()->setCellValue("I".$i, "KAB/KOTA");
		$excel->getActiveSheet()->setCellValue("J".$i, "CABANG");
		$excel->getActiveSheet()->setCellValue("K".$i, "POSISI");
		$excel->getActiveSheet()->setCellValue("L".$i, "NOMINAL UPAH");
		$excel->getActiveSheet()->setCellValue("M".$i, "NAMA LENGKAP");
		$excel->getActiveSheet()->setCellValue("N".$i, "NIK");
		$excel->getActiveSheet()->setCellValue("O".$i, "TEMPAT LAHIR");
		$excel->getActiveSheet()->setCellValue("P".$i, "TANGGAL LAHIR");
		$excel->getActiveSheet()->setCellValue("Q".$i, "JENIS KELAMIN");
		$excel->getActiveSheet()->setCellValue("R".$i, "ALAMAT");
		$excel->getActiveSheet()->setCellValue("S".$i, "AGAMA");
		$excel->getActiveSheet()->setCellValue("T".$i, "STATUS PERNIKAHAN");
		$excel->getActiveSheet()->setCellValue("U".$i, "TANGGAL LAHIR MANUAL");
		$excel->getActiveSheet()->setCellValue("V".$i, "DOJ MANUAL");
		$excel->getActiveSheet()->setCellValue("W".$i, "START MANUAL");
		$excel->getActiveSheet()->setCellValue("X".$i, "END MANUAL");
		$excel->getActiveSheet()->setCellValue("Y".$i, "MANUAL NOMINAL UPAH");
		$excel->getActiveSheet()->setCellValue("Z".$i, "BULAN SURAT");
		$excel->getActiveSheet()->setCellValue("AA".$i, "TAHUN SURAT");
		$excel->getActiveSheet()->setCellValue("AB".$i, "NICKNAME SITE");
		$excel->getActiveSheet()->setCellValue("AC".$i, "BENDERA");

		$id 		= $this->input->post('id');
		$template_id= $this->input->post('template_id');
		$duration	= str_replace(" ", "", $this->input->post('duration'));
		$contract_start = substr($duration, 6, 4).'-'.substr($duration, 3, 2).'-'.substr($duration, 0, 2);
		$date_start 	= new DateTime($contract_start);
		$text_start_date= substr($contract_start, 8, 2).' '.get_month(substr($contract_start, 5, 2)).' '.substr($contract_start, 0, 4);

		$contract_end 	= substr($duration, 17, 4).'-'.substr($duration, 14, 2).'-'.substr($duration, 11, 2);
		$date_end 		= new DateTime($contract_end);
		$text_end_date 	= substr($contract_end, 8, 2).' '.get_month(substr($contract_end, 5, 2)).' '.substr($contract_end, 0, 4);

		$join_date 		= $contract_start;

		$month_contract = get_roman_month(date('m'));
		$year_contract 	= date('Y');

		$i=2;
		foreach ($id as $employee_id) {
			$employee = $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.id, A.full_name, A.id_card, A.city_birth_id AS birth_place, A.date_birth, A.position_id, A.gender, A.address_card, A.religion, A.marital_status, B.name AS site_name, B.city_id AS site_city, B.branch_id AS site_branch, B.code AS site_code, C.code AS company_code'));

			$contract = $this->employee_contract_model->get(array('employee_id' => $employee_id, 'columns' => 'MIN(A.contract_start) AS contract_start'));
			if($contract){
				if($contract->contract_start != ''){
					$join_date = $contract->contract_start;
				}
			}

			$birth_place_name = "";
			$birth_place_city 		= $this->city_model->get(array('id' => $employee->birth_place, 'columns' => 'A.name'));
			if($birth_place_city){
				$birth_place_name = str_replace(array('Kota', 'Kabupaten'), '', $birth_place_city->name);
			}

			$site_city_name = "";
			$site_city 		= $this->city_model->get(array('id' => $employee->site_city, 'columns' => 'A.name'));
			if($site_city){
				$site_city_name = $site_city->name;	
			}

			$site_branch_name = "";
			$site_branch 	= $this->branch_model->get(array('id' => $employee->site_branch, 'columns' => 'A.name'));
			if($site_branch){
				$site_branch_name = $site_branch->name;	
			}

			$position_name = "";
			$position 		= $this->position_model->get(array('id' => $employee->position_id, 'columns' => 'A.name'));
			if($position){
				$position_name = $position->name;	
			}

			$salary_value = 0;
			$salary 		= $this->employee_salary_model->get(array('employee_id' => $employee->id, 'columns' => 'B.basic_salary'));
			if($salary){
				$salary_value = $salary->basic_salary;	
			}

			$excel->getActiveSheet()->setCellValue("A".$i, ($i-1));
			$excel->getActiveSheet()->setCellValue("B".$i, $employee->full_name);
			
			$date_join = new DateTime($join_date);
			$excel->getActiveSheet()->setCellValue("C".$i, PHPExcel_Shared_Date::PHPToExcel($date_join));
			$excel->getActiveSheet()->getStyle("C".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);

			$excel->getActiveSheet()->setCellValue("D".$i, $employee->site_name);
			$excel->getActiveSheet()->setCellValue("E".$i, "AKTIF");
			$excel->getActiveSheet()->setCellValue("F".$i, "");

			$excel->getActiveSheet()->setCellValue("G".$i, PHPExcel_Shared_Date::PHPToExcel($date_start));
			$excel->getActiveSheet()->getStyle("G".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);

			$excel->getActiveSheet()->setCellValue("H".$i, PHPExcel_Shared_Date::PHPToExcel($date_end));
			$excel->getActiveSheet()->getStyle("H".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);

			$excel->getActiveSheet()->setCellValue("I".$i, $site_city_name);
			$excel->getActiveSheet()->setCellValue("J".$i, $site_branch_name);
			$excel->getActiveSheet()->setCellValue("K".$i, $position_name);
			$excel->getActiveSheet()->setCellValue("L".$i, $salary_value);
			$excel->getActiveSheet()->setCellValue("M".$i, $employee->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("N".$i, $employee->id_card);
			$excel->getActiveSheet()->setCellValue("O".$i, $birth_place_name);

			$date_birth = new DateTime($employee->date_birth);
			$excel->getActiveSheet()->setCellValue("P".$i, PHPExcel_Shared_Date::PHPToExcel($date_birth));
			$excel->getActiveSheet()->getStyle("P".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);

			$excel->getActiveSheet()->setCellValue("Q".$i, $employee->gender);
			$excel->getActiveSheet()->setCellValue("R".$i, $employee->address_card);
			$excel->getActiveSheet()->setCellValue("S".$i, $employee->religion);
			$excel->getActiveSheet()->setCellValue("T".$i, $employee->marital_status);

			$text_birth_date 	= substr($employee->date_birth, 8, 2).' '.get_month(substr($employee->date_birth, 5, 2)).' '.substr($employee->date_birth, 0, 4);
			$text_join_date 	= substr($join_date, 8, 2).' '.get_month(substr($join_date, 5, 2)).' '.substr($join_date, 0, 4);

			
			$excel->getActiveSheet()->setCellValue("U".$i, $text_birth_date);
			$excel->getActiveSheet()->setCellValue("V".$i, $text_join_date);
			$excel->getActiveSheet()->setCellValue("W".$i, $text_start_date);
			$excel->getActiveSheet()->setCellValue("X".$i, $text_end_date);
			$excel->getActiveSheet()->setCellValue("Y".$i, $salary_value);
			$excel->getActiveSheet()->setCellValue("Z".$i, $month_contract);
			$excel->getActiveSheet()->setCellValue("AA".$i, $year_contract);
			$excel->getActiveSheet()->setCellValue("AB".$i, $employee->site_code);
			$excel->getActiveSheet()->setCellValue("AC".$i, $employee->company_code);
			$i++;
		}

		$excel->getActiveSheet()->setTitle('Kontrak Karyawan');
		$sheet = $excel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells( true );
		foreach( $cellIterator as $cell ) {
			$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		}
		$excel->createSheet();
		$excel->setActiveSheetIndex(0);
		
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function delete($employee_id = false)
	{
		$this->load->model('employee_model');
		if ($employee_id)
		{
			$data =  $this->employee_model->get(array('id' => $employee_id, 'status' => 0, 'columns' => 'A.id, B.name AS site_name'));

			if ($data)
			{

				$type = "direct";
				if(strtolower($data->site_name) == 'indirect'){
					$type = "indirect";
				} 

				$insert = array('id' => $employee_id, 'is_active' => 0);
				$result = $this->employee_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('employee_contract/list/'.$type));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('employee_contract/list/'.$type));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('employee_contract/list/direct'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('employee_contract/list/direct'));
		}
	}

	public function preview($employee_id=FALSE)
	{
		$this->load->model(array('employee_model','employee_education_model', 'employee_emergency_model', 'employee_history_model', 'employee_language_model', 'employee_reference_model', 'employee_skill_model'));
		$data['_TITLE_'] 		= 'Preview Karyawan';
		$data['_PAGE_']	 		= 'employee_contract/preview';
		$data['_MENU_PARENT_'] 	= 'personalia';
		$data['_MENU_'] 		= 'employee_contract';

		$data['id'] = $employee_id;

		if (!$employee_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('employee_contract/list'));
		}

		$data['list_education'] = $this->employee_education_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.institute, A.start, A.end'));
		$data['list_emergency'] = $this->employee_emergency_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.relation, A.phone, A.address'));
		$data['list_history'] 	= $this->employee_history_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.company_name, A.position, A.period,  A.salary, A.company_address, A.resign_note'));
		$data['list_language'] 	= $this->employee_language_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.language, A.verbal, A.write'));
		$data['list_reference'] = $this->employee_reference_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.company_name, A.position, A.phone'));
		$data['list_skill'] 	= $this->employee_skill_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));

		$data['preview'] = $this->employee_model->preview(array('id' => $employee_id));
		$data['preview']->date_birth = format_date_ina($data['preview']->date_birth);
		$data['preview']->city_birth = str_replace(array('Kota', 'Kabupaten'), '', $data['preview']->city_birth);
		$this->load->view('employee_contract/preview', $data);
	}

	public function form($type = FALSE, $employee_id = FALSE)
	{
		$this->load->model(array('city_model', 'province_model', 'employee_model', 'company_model', 'site_model', 'branch_model', 'employee_skill_model', 'position_model', 'setting_tax_model', 'auth_model'));
		
		$data['id']								= $employee_id;
		$data['registration_number']			= '';
		$data['employee_number']				= '';
		$data['full_name']						= '';
		$data['id_card']						= '';
		$data['id_card_period']					= 'SEUMUR HIDUP';
		$data['city_birth_id']					= '';
		$data['date_birth']						= '';
		$data['city_card_id']					= '';
		$data['address_card']					= '';
		$data['city_domisili_id'] 				= '';
		$data['address_domisili'] 				= '';
		$data['status_residance'] 				= '';
		$data['phone_number'] 					= '';
		$data['email'] 							= '';
		$data['socmed_fb'] 						= '';
		$data['socmed_ig'] 						= '';
		$data['socmed_tw'] 						= '';
		$data['religion'] 						= '';
		$data['marital_status']					= '';
		$data['gender'] 						= '';
		$data['heigh'] 							= '';
		$data['weigh'] 							= '';
		$data['clothing_size'] 					= '';
		$data['shoe_size'] 						= '';
		$data['education_level']	 			= '';
		$data['education_majors'] 				= '';

		$data['bank_name'] 						= '';
		$data['bank_account'] 					= '';
		$data['bank_account_name'] 				= '';
		$data['bank_account_create'] 			= 'ya';
		$data['tax_number'] 					= '';
		$data['health_insurance'] 				= 'ya';
		$data['health_insurance_note'] 			= '';
		$data['health_insurance_company'] 		= 'ya';

		$data['ptkp']							= '';
		$data['benefit_labor_note']				= '';
		$data['benefit_labor']					= '';
		$data['benefit_health_note']			= '';
		$data['benefit_health']					= '';
		$data['company_id']						= '';
		$data['site_id']						= '';
		$data['branch_id']						= '';
		$data['position_id']					= '';

		$data['family_mother']					= '';
		$data['family_mate']					= '';
		$data['family_child']					= '';

		$data['document_photo']					= '';
		$data['document_id_card']				= '';
		$data['document_marriage_certificate']	= '';
		$data['document_tax_number']			= '';
		$data['document_family_card']			= '';

		if($this->input->post()){
			$this->form_validation->set_rules('id_card', 'Nomor KTP', 'required|numeric|min_length[16]|max_length[17]');

			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$data['id_card']		= $params['id_card']			= $this->input->post('id_card');
				$data['id']				= $params['id']					= $this->input->post('id');
				$data['employee_number']= $params['employee_number']	= $this->input->post('employee_number');
				$result 		= (array) $this->employee_model->get(array('id_card' => $data['id_card']));
				if(!empty($result)){
					if($data['id'] == ''){
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Nomor KTP sudah terdaftar.','danger'));
						redirect(base_url('employee_contract/list/'.$type));
					}
					if($params['employee_number'] != $result['employee_number'] && $params['employee_number'] != ''){
						$exist_number = $this->employee_model->get(array('employee_number' => $params['employee_number']));
						if($exist_number){
							$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Nomor Induk Karyawan sudah terdaftar.','danger'));
							redirect(base_url('employee_contract/list/'.$type));
						}	
					}
					$data = $result;
				}

				$data['registration_number']= $params['registration_number']= $this->input->post('registration_number');
				$data['full_name']			= $params['full_name']			= $this->input->post('full_name');
				$data['city_birth_id']		= $params['city_birth_id'] 		= $this->input->post('city_birth_id');
				$data['date_birth']			= $params['date_birth'] 		= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_birth'))));
				$data['address_card']		= $params['address_card'] 		= $this->input->post('address_card');
				$data['city_card_id']		= $params['city_card_id'] 		= $this->input->post('city_card_id');
				$data['address_domisili']	= $params['address_domisili'] 	= $this->input->post('address_domisili');
				$data['city_domisili_id']	= $params['city_domisili_id'] 	= $this->input->post('city_domisili_id');
				if($data['city_domisili_id'] == ''){
					$data['city_domisili_id']	= $params['city_domisili_id'] = $data['city_card_id'];
				}
				if($data['address_domisili'] == ''){
					$data['address_domisili']	= $params['address_domisili'] = $data['address_card'];
				}

				$data['status_residance']	= $params['status_residance'] 	= strtolower($this->input->post('status_residance'));
				$data['phone_number']		= $params['phone_number'] 		= $this->input->post('phone_number');
				$data['email']				= $params['email'] 				= $this->input->post('email');
				$data['socmed_fb']			= $params['socmed_fb'] 			= $this->input->post('socmed_fb');
				$data['socmed_ig']			= $params['socmed_ig'] 			= $this->input->post('socmed_ig');
				$data['socmed_tw']			= $params['socmed_tw'] 			= $this->input->post('socmed_tw');
				$data['religion']			= $params['religion'] 			= $this->input->post('religion');
				$data['marital_status']		= $params['marital_status'] 	= $this->input->post('marital_status');
				$data['gender']				= $params['gender'] 			= $this->input->post('gender');
				$data['heigh']				= $params['heigh'] 				= $this->input->post('heigh');
				$data['weigh']				= $params['weigh'] 				= $this->input->post('weigh');
				$data['clothing_size']		= $params['clothing_size'] 		= $this->input->post('clothing_size');
				$data['shoe_size']			= $params['shoe_size'] 			= $this->input->post('shoe_size');
				$data['education_level']	= $params['education_level'] 	= $this->input->post('education_level');
				$data['education_majors']	= $params['education_majors'] 	= strtoupper($this->input->post('education_majors'));

				$params['health_insurance_note']	= $data['health_insurance_note'];
				$params['health_insurance']			= $data['health_insurance'];
				$params['health_insurance_company']	= $data['health_insurance_company'];
				$params['bank_account_create']		= $data['bank_account_create'];
				$params['id_card_period']			= $data['id_card_period'];

				$data['bank_name']					= $params['bank_name'] 				= $this->input->post('bank_name');
				$data['bank_account']				= $params['bank_account'] 			= $this->input->post('bank_account');
				$data['bank_account_name']			= $params['bank_account_name'] 		= $this->input->post('bank_account_name');
				$data['ptkp']						= $params['ptkp'] 					= strtoupper($this->input->post('ptkp'));
				
				$benefit_labor_note					= "";
				$labor = 0;
				foreach($this->input->post('benefit_labor_choice') AS $item){
					if($labor > 0){
						$benefit_labor_note .= ",".$item;
					}else{
						$benefit_labor_note .= $item;
					}
					$labor++;
				}
			
				$data['benefit_labor_note']			= $params['benefit_labor_note'] 	= $benefit_labor_note;
				$data['benefit_labor']				= $params['benefit_labor'] 			= $this->input->post('benefit_labor');

				$data['benefit_health_note']		= $params['benefit_health_note'] 	= $this->input->post('benefit_health_note');
				$data['benefit_health']				= $params['benefit_health'] 		= $this->input->post('benefit_health');
				
				$data['tax_number']					= $params['tax_number'] 			= $this->input->post('tax_number');

				$data['family_mother']				= $params['family_mother'] 			= $this->input->post('family_mother');
				$data['family_mate']				= $params['family_mate'] 			= $this->input->post('family_mate');
				$data['family_child']				= $params['family_child'] 			= $this->input->post('family_child');

				$data['company_id']					= $params['company_id'] 			= $this->input->post('company_id');
				$data['site_id']					= $params['site_id'] 				= $this->input->post('site_id');
				$data['branch_id']					= $params['branch_id'] 				= $this->input->post('branch_id');
				$data['position_id']				= $params['position_id'] 			= $this->input->post('position_id');

				$url_document = 'files/document/'.$data['id_card'];
				$document_path = FCPATH.$url_document;
				if(!is_dir($document_path)){
					mkdir($document_path, 0755, TRUE);
				}

				$config['upload_path'] 	= $document_path;
				$config['overwrite']  	= TRUE;
				$config['allowed_types']= 'jpg|jpeg|png|pdf|doc|docx';
				
				if($_FILES['document_photo']['name'] != ""){
					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'foto';
					$this->load->library('upload', $config, 'document_photo');
					if (!$this->document_photo->do_upload('document_photo')) {
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Proses upload Foto gagal.','danger'));
					} else {
						$document 	= $this->document_photo->data();
						$params['document_photo'] = $data['document_photo'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_id_card']['name'] != ""){
					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'ktp';
					$this->load->library('upload', $config, 'document_id_card');

					if (!$this->document_id_card->do_upload('document_id_card')) {
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Proses upload Foto KTP gagal.','danger'));
					} else {
						$document = $this->document_id_card->data();
						$params['document_id_card'] =	$data['document_id_card'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_marriage_certificate']['name'] != ""){
					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'buku-nikah';
					$this->load->library('upload', $config, 'document_marriage_certificate');

					if (!$this->document_marriage_certificate->do_upload('document_marriage_certificate')) {
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Proses upload Foto Surat Nikah gagal.','danger'));
					} else {
						$document = $this->document_marriage_certificate->data();
						$params['document_marriage_certificate'] =	$data['document_marriage_certificate'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_tax_number']['name'] != ""){
					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'npwp';
					$this->load->library('upload', $config, 'document_tax_number');

					if (!$this->document_tax_number->do_upload('document_tax_number')) {
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Proses upload Foto NPWP gagal.','danger'));
					} else {
						$document = $this->document_tax_number->data();
						$params['document_tax_number'] =	$data['document_tax_number'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_family_card']['name'] != ""){
					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'kartu-keluarga';
					$this->load->library('upload', $config, 'document_family_card');

					if (!$this->document_family_card->do_upload('document_family_card')) {
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Proses upload Foto Kartu Keluarga gagal.','danger'));
					} else {
						$document = $this->document_family_card->data();
						$params['document_family_card'] =	$data['document_family_card'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				$params['status']			= 1;
				$params['status_approval']	= 3;
				$params['is_active']		= 1;
				$save_id = $this->employee_model->save($params);
				if ($save_id) {
					$user = $this->auth_model->get(array('employee_id' => $data['id'], 'columns' => 'A.id'));
					if($user){
						$this->auth_model->save(array('id' => $user->id, 'username' => $params['id_card'], 'full_name' => $params['full_name'], 'email' => $params['email']));
					}
					$data['id'] = $save_id;
					if($data['registration_number'] == ''){
						$registration_number 		= 10000+$save_id;
						$this->employee_model->save(array('id' => $save_id, 'registration_number' => $registration_number, 'approved_at' => date('Y-m-d H:i:s')));
					}

					if($params['employee_number'] == ''){
						$site =  $this->site_model->get(array('id' => $data['site_id'], 'columns' => 'A.id, A.company_id'));
						$employee_number = $this->employee_number($site->company_id);
						$this->employee_model->save(array('id' => $save_id, 'employee_number' => $employee_number));
					}

					$skill_id				= $this->input->post('skill_id');
					$skill					= $this->input->post('skill');
					$certificate_number		= $this->input->post('certificate_number');
					$skill_certificate		= $this->input->post('skill_certificate');

					$files = $_FILES;
					foreach ($skill_id as $key => $value ) {
						if($skill[$key] == ''){
							continue;
						}
						$params_skill['employee_id']		= $data['id'];
						$params_skill['id']					= $skill_id[$key];
						$params_skill['certificate_name']	= $skill[$key];
						$params_skill['certificate_number']	= $certificate_number[$key];

						$temp_skill_id	=	$this->employee_skill_model->save($params_skill);
						
						if($files['skill_certificate']['name'][$key] == ''){
							continue;
						}
						
						$_FILES['skill_certificate']['name']	= $files['skill_certificate']['name'][$key];
				        $_FILES['skill_certificate']['type']	= $files['skill_certificate']['type'][$key];
				        $_FILES['skill_certificate']['tmp_name']= $files['skill_certificate']['tmp_name'][$key];
				        $_FILES['skill_certificate']['error']	= $files['skill_certificate']['error'][$key];
				        $_FILES['skill_certificate']['size']	= $files['skill_certificate']['size'][$key];  

						$this->load->library('upload');
						$config['upload_path'] 	= $document_path;
						$config['overwrite']  	= TRUE;
						$config['allowed_types']= 'jpg|jpeg|png';
						$config['file_name'] 	= 'certificate-'.$temp_skill_id;
						$this->upload->initialize($config);

						if($this->upload->do_upload('skill_certificate')){
							$document = $this->upload->data();	
							$params_skill['certificate_document'] = base_url($url_document.'/'.$document['file_name']);
							$params_skill['id']	= $temp_skill_id;
							$this->employee_skill_model->save($params_skill);
						}
					}
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger'));
				}
				redirect(base_url('employee_contract/list/'.$type));
			}
		}else{
			$result = (array) $this->employee_model->get(array('id' => $data['id']));
			if(!empty($result)){
				$data = $result;
			}
		}
		$data['list_skill'] 	= $this->employee_skill_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));

		$data['_TITLE_']		= 'Kontrak Karyawan';
		$data['_PAGE_']			= 'employee_contract/form';
		$data['_MENU_PARENT_']	= $type;
		$data['_MENU_']			= 'employee_contract_'.$type;
		$data['type']			= $type;
		$data['list_city'] 		= $this->city_model->gets(array('columns' => 'A.id, A.name, B.name AS province_name'));
		$data['list_company'] 	= $this->company_model->gets(array('columns' => 'A.id, A.name'));
		$data['list_site'] 		= $this->site_model->gets(array('columns' => 'A.id, A.name'));
		$data['list_tax'] 		= $this->setting_tax_model->gets(array('columns' => 'A.id, A.code, A.description'));
		$data['list_branch'] 	= $this->branch_model->gets(array('columns' => 'A.id, A.name'));
		$data['list_position'] 	= $this->position_model->gets(array('columns' => 'A.id, A.name'));

		$this->view($data);
	}

	public function list($type = FALSE, $expired = FALSE){
		$this->load->model(array('contract_model'));
		$data['_TITLE_'] 		= 'Karyawan';
		$data['_PAGE_'] 		= 'employee_contract/list';
		$data['_MENU_PARENT_'] 	= $type;
		$data['_MENU_'] 		= 'employee_contract_'.$type;
		$data['type'] 			= $type;
		$data['expired'] 		= $expired;

		$params['not_type']		= array('ADENDUM', 'PB', 'SUPER', 'A.id, A.title, B.name AS branch_name');
		$params['branch_id']	= '';
		$params['columns']		= 'A.id, A.title, B.name AS branch_name';
		
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		$data['list_template'] = $this->contract_model->gets($params);
		
		$this->view($data);
	}

	public function list_ajax($type = FALSE, $expired = FALSE){
		$this->load->model(array('employee_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.id_card, A.employee_number, A.full_name, A.site_id, C.code AS company_code, B.name AS site_name, E.name AS position, D.contract_type, DATE_FORMAT(D.contract_end, '%d/%m/%Y') AS contract_end, D.contract_end AS raw_end, J.code AS business_code, K.code AS ptkp, F.name AS branch_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']		= 1;
		$params['status_nonjob'] 	= 0;
		$params['status_approval'] 	= 3;
		$params['branch_id']	= '';

		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		if($type == 'direct'){
			$params['not_site_id']		= 2;
		}else{
			$params['site_id']		= 2;
		}

		if($expired == 'expired'){
			$params['expired']	 	= 0;
		}else if($expired){
			$params['expired']	 	= $expired;
		}

		$params['id_card']			= $_POST['columns'][1]['search']['value'];
		$params['employee_number']	= $_POST['columns'][2]['search']['value'];
		$params['full_name']		= $_POST['columns'][3]['search']['value'];
		$params['company_code']		= $_POST['columns'][4]['search']['value'];
		$params['site_name']		= $_POST['columns'][5]['search']['value'];
		$params['branch_name']		= $_POST['columns'][6]['search']['value'];
		$params['business_code']	= $_POST['columns'][7]['search']['value'];
		$params['position']			= $_POST['columns'][8]['search']['value'];
		$params['ptkp']				= $_POST['columns'][9]['search']['value'];
		$params['contract_type']	= $_POST['columns'][10]['search']['value'];
		$params['contract_end']		= $_POST['columns'][11]['search']['value'];

		$list 	= $this->employee_model->gets($params);
		$total 	= $this->employee_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$type 	= 'indirect';
			if($item->site_id != 2){
				$type = 'direct';
			}
			
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['id_card']				= $item->id_card;
			$result['employee_number']		= $item->employee_number;
			$result['full_name']			= $item->full_name;
			$result['company_code']			= $item->company_code;
			$result['site_name']			= $item->site_name;
			$result['branch_name']			= $item->branch_name;
			$result['business_code']		= $item->business_code;
			$result['position']				= $item->position;
			$result['ptkp']					= $item->ptkp;
			$result['contract_type']		= $item->contract_type;
			$contract_end					= "";
			if($item->raw_end){
				$now 		= date("Y-m-d");
				$dt2 = new DateTime("+1 month");
				$next_month = $dt2->format("Y-m-d");
				if($item->raw_end < $now){
					$contract_end = '<span class="text-danger">'.$item->contract_end.'</span>';
				}else if($item->raw_end < $next_month){
					$contract_end = '<span class="text-warning">'.$item->contract_end.'</span>';
				}else{
					$contract_end = '<span class="text-success">'.$item->contract_end.'</span>';	
				}
			}else{
				$result['contract_end']		= $item->contract_end;
			}
			$result['contract_end']			= $item->contract_end;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("employee_contract/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("employee_contract/form/".$type."/".$item->id).'">Ubah</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("employee_contract/contract/".$type."/".$item->id).'">Kontrak</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("salary/form_employee/".$item->site_id."/".$item->id).'">Gaji</i></a>
				<a class="btn-sm btn-danger btn-block" href="'.base_url("employee_contract/resign/".$type."/".$item->id).'">Resign</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("employee_contract/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	private function employee_number($site_id){
		$employee_number = "";
		$this->load->model('employee_model');

		$employee_number = $site_id.date('ym');

		$site = $this->employee_model->last_employee_number(array('prefix' => $employee_number));

		$inc = 1;
		if($site){
			// $inc = intval(substr($site->employee_number, 13, 5))+1;
			$inc = intval(substr($site->employee_number, 5, 5))+1;
		}		
		$space = "";
		for ($i=strlen($inc); $i <5; $i++) {
			$space .= "0";
		}

		$employee_number = $employee_number.$space.$inc;		
		return $employee_number;
	}

	public function import($type = FALSE){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('employee_model', 'site_model', 'city_model', 'branch_model', 'position_model', 'company_model', 'approval_model', 'user_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			for ($row = 2; $row <= $rend; $row++) {
				$data['id'] 					= '';
				$data['registration_number']	= '';
				$data['status']					= 1;
				$data['status_approval']		= 3;
				$data['status_nonjob']			= 0;
				$data['approved_at']			= date('Y-m-d H:i:s');
				$data['resign_submit'] 			= null;
				
				$data['id_card'] 				= replace_null($sheet->getCell('C'.$row)->getValue());
				if($data['id_card'] == ''){
					continue;
				}
			
				if(!is_numeric($data['id_card'])){
					continue;					
				}
				if(strlen($data['id_card']) < 15 || strlen($data['id_card']) > 18){
					continue;
				}

				$candidate = $this->employee_model->get(array('id_card' => $data['id_card'], 'columns' => 'A.id, A.registration_number, A.status, A.status_approval, A.status_nonjob'));
			    
			   
				if($candidate){					
					if($candidate->status <= 2){
						$data['id']					= $candidate->id;
						$data['registration_number']= $candidate->registration_number;
					}
				}
                
				$data['full_name'] 				= replace_null($sheet->getCell('B'.$row)->getValue());
				$city_birth 					= $this->city_model->get(array('name' => replace_null($sheet->getCell('D'.$row)->getValue()), 'columns' => 'A.id'));
				if($city_birth){
					$data['city_birth_id'] 	= $data['city_card_id'] = $data['city_domisili_id'] = $city_birth->id;
				}

				$excel_date = $sheet->getCell('E'.$row)->getValue();
				$unix_date = ($excel_date - 25569) * 86400;
				$excel_date = 25569 + ($unix_date / 86400);
				$unix_date = ($excel_date - 25569) * 86400;
				$data['date_birth']		= gmdate("Y-m-d", $unix_date);
				$data['address_card']	= $data['address_domisili'] = replace_null($sheet->getCell('F'.$row)->getValue());
				
				$data['site_id'] = 1;
				$site 	= $this->site_model->get(array('name' => replace_null($sheet->getCell('G'.$row)->getValue()), 'columns' => 'A.id'));
				if($site){
					$data['site_id']			= $site->id;
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Site '.replace_null($sheet->getCell('G'.$row)->getValue())." belum terdaftar.",'danger'));
					redirect(base_url('employee_contract/list/'.$type));
				}
				
				$data['position_id'] = 0;
				$position 	= $this->position_model->get(array('name' => replace_null($sheet->getCell('H'.$row)->getValue()), 'columns' => 'A.id'));
				if($position){
					$data['position_id'] 			= $position->id;
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Posisi/Jabatan '.replace_null($sheet->getCell('H'.$row)->getValue())." belum terdaftar.",'danger'));
					redirect(base_url('employee_contract/list/'.$type));
				}
				
				$data['company_id'] = 1;
				$company 	= $this->company_model->get(array('code' => replace_null(strtoupper($sheet->getCell('I'.$row)->getValue())), 'columns' => 'A.id'));
				if($company){
					$data['company_id'] 			= $company->id;
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Unit Kontrak '.replace_null($sheet->getCell('I'.$row)->getValue())." belum terdaftar.",'danger'));
					redirect(base_url('employee_contract/list/'.$type));
				}

				$branch 	= $this->branch_model->get(array('name' => replace_null($sheet->getCell('J'.$row)->getValue()), 'columns' => 'A.id'));
				if($branch){
					$data['branch_id'] 			= $branch->id;
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Branch '.replace_null($sheet->getCell('J'.$row)->getValue())." belum terdaftar.",'danger'));
					redirect(base_url('employee_contract/list/'.$type));
				}
				$data['employee_number'] 		= replace_null($sheet->getCell('K'.$row)->getValue());
				if($data['employee_number'] == ''){
					$employee_number = $this->employee_number($data['company_id']);
				}

				$excel_date = $sheet->getCell('L'.$row)->getValue();
				$unix_date = ($excel_date - 25569) * 86400;
				$excel_date = 25569 + ($unix_date / 86400);
				$unix_date = ($excel_date - 25569) * 86400;
				$data['approved_at'] = gmdate("Y-m-d", $unix_date)." 00:00:00";

				$save_id = "";
				if($candidate){
					if($candidate->status_approval > 2 && $candidate->status_nonjob < 1){
						$save_id = $this->employee_model->save(array('id' => $data['id'], 'position_id' => $data['position_id'], 'company_id' => $data['company_id'], 'branch_id' => $data['branch_id'], 'site_id' => $data['site_id']));
					}else{
						$save_id = $this->employee_model->save($data);
					}
				}else{
					$save_id = $this->employee_model->save($data);
				}

				if ($save_id) {
					if($data['registration_number'] == ''){
						$registration_number 		= 10000+$save_id;
						$this->employee_model->save(array('id' => $save_id, 'registration_number' => $registration_number));
					}
					$approval = array('id' => '', 'employee_id' => $save_id , 'site_id' => $data['site_id'] , 'company_id' => $data['company_id'], 'position_id' => $data['position_id'], 'note' => 'import karyawan', 'status_approval' => 'Diterima');
					$this->approval_model->save($approval);
					$user_exist = $this->user_model->get(array('employee_id' => $save_id, 'columns' => 'A.id'));
					
					if(!$user_exist){
						$user['username'] 			= $data['id_card'];
						$user['full_name'] 			= $data['full_name'];
						$user['password']			= generate_password($data['id_card']);
						$user['role_id']			= 1;
						$user['company_id']			= $data['company_id'];
						$user['branch_id']			= $data['branch_id'];
						$user['employee_id']		= $save_id;
						$user['id']					= '';
						$this->user_model->save($user);
					}
				}
			}
            
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			redirect(base_url('employee_contract/list/'.$type));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
			redirect(base_url('employee_contract/list/'.$type));
		}
	}

	public function import_contract($type = FALSE){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('employee_model', 'employee_contract_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {
				
				$id_card 				= replace_null($sheet->getCell('A'.$row)->getValue());
				if($id_card == ''){
					continue;
				}

				$employee = $this->employee_model->get(array('id_card' => $id_card, 'columns' => 'A.id'));
				if($employee){
					$excel_date_start = $sheet->getCell('B'.$row)->getValue();
					$unix_date_start = ($excel_date_start - 25569) * 86400;
					$excel_date_start = 25569 + ($unix_date_start / 86400);
					$unix_date_start = ($excel_date_start - 25569) * 86400;

					$excel_date_end = $sheet->getCell('C'.$row)->getValue();
					$unix_date_end = ($excel_date_end - 25569) * 86400;
					$excel_date_end = 25569 + ($unix_date_end / 86400);
					$unix_date_end = ($excel_date_end - 25569) * 86400;

					$data['employee_id']	= $employee->id;
					$data['contract_type']	= 'PKWT';
					$data['contract_start']		= gmdate("Y-m-d", $unix_date_start);
					$data['contract_end']		= gmdate("Y-m-d", $unix_date_end);
					$save_id = $this->employee_contract_model->save($data);
				}
			}

			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			redirect(base_url('employee_contract/list/'.$type));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
			redirect(base_url('employee_contract/list/'.$type));
		}
	}

	public function import_resign($type = FALSE){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('employee_model', 'approval_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {
				$data['id'] 					= '';
				$data['registration_number']	= '';
				$data['status']					= 1;
				$data['status_approval']		= 3;
				$data['status_nonjob']			= 0;
				$data['approved_at']			= date('Y-m-d H:i:s');
				$data['resign_submit'] 			= null;
				
				$data['id_card'] 				= replace_null($sheet->getCell('C'.$row)->getValue());
				if($data['id_card'] == ''){
					continue;
				}

				$candidate = $this->employee_model->get(array('id_card' => $data['id_card'], 'columns' => 'A.id, A.registration_number, A.status, A.site_id, A.company_id, A.position_id'));
				if($candidate){
					$employee_array = array('id' => $candidate->id, 'status_approval' => 0, 'status' => 0, 'document_resign' => '', 'resign_note' => 'data impor', 'resign_burden' => '-', 'resign_type' => 'data import', 'resign_submit' => '2023-09-07', 'site_id' => 1, 'followup_status' => 'Resign', 'followup_note' => '');
					$approval = array('id' => '', 'employee_id' => $candidate->id , 'site_id' => $candidate->site_id, 'company_id' => $candidate->company_id, 'position_id' => $candidate->position_id, 'note' => 'data import', 'status_approval' => 'Resign');

					$this->employee_model->save($employee_array);
					$this->approval_model->save($approval);
				}
			}

			die();
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			redirect(base_url('employee_contract/list/'.$type));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
			redirect(base_url('employee_contract/list/'.$type));
		}
	}

	public function import_nonjob($type = FALSE){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('employee_model', 'site_model', 'city_model', 'branch_model', 'position_model', 'company_model', 'approval_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {
				$data['id'] 					= '';
				$data['registration_number']	= '';
				$data['status']					= 1;
				$data['status_approval']		= 3;
				$data['status_nonjob']			= 0;
				$data['approved_at']			= date('Y-m-d H:i:s');
				$data['resign_submit'] 			= null;
				
				$data['id_card'] 				= replace_null($sheet->getCell('C'.$row)->getValue());
				if($data['id_card'] == ''){
					continue;
				}

				$candidate = $this->employee_model->get(array('id_card' => $data['id_card'], 'columns' => 'A.id, A.registration_number, A.status, A.status_nonjob'));
				if($candidate){
					if($candidate->status_nonjob > 0){
						$data['full_name'] 				= replace_null($sheet->getCell('B'.$row)->getValue());
						$city_birth 					= $this->city_model->get(array('name' => replace_null($sheet->getCell('D'.$row)->getValue()), 'columns' => 'A.id'));
						if($city_birth){
							$data['city_birth_id'] 	= $data['city_card_id'] = $data['city_domisili_id'] = $city_birth->id;
						}

						$excel_date = $sheet->getCell('E'.$row)->getValue();
						$unix_date = ($excel_date - 25569) * 86400;
						$excel_date = 25569 + ($unix_date / 86400);
						$unix_date = ($excel_date - 25569) * 86400;
						$data['date_birth']		= gmdate("Y-m-d", $unix_date);
						$data['address_card']	= $data['address_domisili'] = replace_null($sheet->getCell('F'.$row)->getValue());
						
						$data['site_id'] = 1;
						$site 	= $this->site_model->get(array('name' => replace_null($sheet->getCell('G'.$row)->getValue()), 'columns' => 'A.id'));
						if($site){
							$data['site_id']			= $site->id;
						}else{
							continue;
						}
						
						$data['position_id'] = 0;
						$position 	= $this->position_model->get(array('name' => replace_null($sheet->getCell('H'.$row)->getValue()), 'columns' => 'A.id'));
						if($position){
							$data['position_id'] 			= $position->id;
						}else{
							continue;
						}
						
						$data['company_id'] = 1;
						$company 	= $this->company_model->get(array('code' => replace_null(strtoupper($sheet->getCell('I'.$row)->getValue())), 'columns' => 'A.id'));
						if($company){
							$data['company_id'] 			= $company->id;
						}else{
							continue;
						}

						$branch 	= $this->branch_model->get(array('name' => replace_null($sheet->getCell('J'.$row)->getValue()), 'columns' => 'A.id'));
						if($branch){
							$data['branch_id'] 			= $branch->id;
						}else{
							continue;
						}
						$data['employee_number'] 		= replace_null($sheet->getCell('K'.$row)->getValue());
						if($data['employee_number'] == ''){
							$employee_number = $this->employee_number($data['company_id']);
						}

						$excel_date = $sheet->getCell('L'.$row)->getValue();
						$unix_date = ($excel_date - 25569) * 86400;
						$excel_date = 25569 + ($unix_date / 86400);
						$unix_date = ($excel_date - 25569) * 86400;
						$data['approved_at'] = gmdate("Y-m-d", $unix_date)." 00:00:00";

						$save_id = $this->employee_model->save($data);
						if ($save_id) {
							if($data['registration_number'] == ''){
								$registration_number 		= 10000+$save_id;
								$this->employee_model->save(array('id' => $save_id, 'registration_number' => $registration_number));
							}
							$approval = array('id' => '', 'employee_id' => $save_id , 'site_id' => $data['site_id'] , 'company_id' => $data['company_id'], 'position_id' => $data['position_id'], 'note' => 'import karyawan', 'status_approval' => 'Diterima');
							$this->approval_model->save($approval);
						}
						print_prev($data);
					}
				}
			}

			die();
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			redirect(base_url('employee_contract/list/'.$type));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
			redirect(base_url('employee_contract/list/'.$type));
		}
	}

	public function resign($type = FALSE, $employee_id = FALSE){
		$this->load->model(array('employee_model', 'approval_model', 'user_model', 'resign_note_model'));
		
		if($this->input->post()){
			$employee_id= $this->input->post('employee_id');
			$id_card 	= $this->input->post('id_card');
			$resign_doc = "";
			$url_document 	= 'files/document/'.$id_card;
			$document_path 	= FCPATH.$url_document;
			if(!is_dir($document_path)){
				mkdir($document_path, 0755, TRUE);
			}
			
			$config['upload_path'] 	= $document_path;
			$config['overwrite']  	= TRUE;
			$config['allowed_types']= 'jpg|jpeg|png';
			if($_FILES['document_resign']['name'] != ""){
				$config['file_name'] 	= 'resign';
				$this->load->library('upload', $config, 'document_resign');

				if (!$this->document_resign->do_upload('document_resign')) {
					$data['message']= message_box('<strong>Gagal!</strong> '.$this->document_resign->display_errors().'.','danger');
				} else {
					$document 	= $this->document_resign->data();
					$resign_doc = base_url($url_document.'/'.$document['file_name']);
				}
			}

			$resign_note 	= $this->input->post('resign_note');
			$resign_burden 	= $this->input->post('resign_burden');
			$resign_type 	= $this->input->post('resign_type');
			$resign_submit 	= $this->input->post('resign_submit');
			$site_id 		= $this->input->post('site_id');
			$company_id 	= $this->input->post('company_id');
			$position_id 	= $this->input->post('position_id');
			$resign_note 	= $resign_type ." - ". $resign_note;
			$emp = $this->employee_model->save(array('id' => $employee_id, 'status_approval' => 0, 'status' => 0, 'document_resign' => $resign_doc, 'resign_note' => $resign_note, 'resign_burden' => $resign_burden, 'resign_type' => $resign_type, 'resign_submit' => $resign_submit, 'site_id' => 1, 'followup_status' => $resign_type, 'followup_note' => $resign_note));
			
			$approval = array('id' => '', 'employee_id' => $employee_id , 'site_id' => $site_id, 'company_id' => $company_id, 'position_id' => $position_id, 'note' => $resign_note,'status_approval' => 'Resign');
			$this->approval_model->save($approval);

			$user = $this->user_model->get(array('employee_id' => $employee_id,'columns' => 'A.id'));
			if($user){
				$this->user_model->save(array('id' => $user->id, 'is_active' => 0,'role_id' => 1));
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			redirect(base_url('employee_contract/list/'.$type));
		}
		
		$data['employee'] 	= $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.id, A.employee_number, A.full_name, A.id_card, A.site_id, A.company_id, A.position_id, A.address_card, DATE_FORMAT(A.date_birth, "%d/%m/%Y") AS date_birth, A.company_id'));

		$this->load->model(array('contract_model'));

		$data['list_resign_note'] 	= $this->resign_note_model->gets(array('columns' => 'A.id, A.name'));
		$data['_TITLE_'] 			= 'Karyawan '.$data['employee']->full_name;
		$data['_PAGE_'] 			= 'employee_contract/resign';
		$data['_MENU_PARENT_'] 		= $type;
		$data['_MENU_'] 			= 'employee_contract_'.$type;
		$data['type'] 				= $type;
		$this->view($data);
	}
	
	public function card($type = FALSE){
		$this->load->model('candidate_model');
		$id_card = $this->input->post('id_card');

		$candidate = $this->candidate_model->check_card(array('id_card' => $id_card, 'columns' => 'A.id, A.full_name, A.is_active'));
		$response = array(
			'description' 		=> 'NIK Belum Terdaftar',
			'id' 			=> '0'
		);
		if($candidate){
			if($candidate->is_active == 0){
				$this->candidate_model->save(array('id' => $candidate->id, 'is_active' => 1));	
			}
			$response = array(
				'description' 		=> 'NIK Sudah Terdaftar dengan nama : '.$candidate->full_name,
				'id' 			=> $candidate->id
			);
			echo json_encode($response);
			die();
		}
		echo json_encode($response);
	} 

	public function nonjob($type = FALSE)
	{
		if($this->input->post()){
			$this->load->model(array('employee_model'));
			$ids 		= $this->input->post('id');
			foreach ($ids as $employee_id) {
				$employee 	= $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.full_name'));
				if(!$employee){
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data Karyawan tidak ditemukan.','danger'));
					redirect(base_url('employee_contract/list/'.$type));
				}
				$save_id = $this->employee_model->save(array('id' => $employee_id, 'status_nonjob' => 1));
			}
			$this->session->set_flashdata('message', message_box('<strong>Berhasil!</strong> Data Karyawan berhasil diproses.','success'));
			redirect(base_url('employee_contract/list/'.$type));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data Karyawan tidak ditemukan.','danger'));
			redirect(base_url('employee_contract/list/'.$type));
		}
	}

	private function generate_PDF($data = array())
	{	
		$this->load->library("Pdf");
		ob_start();
		$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
		// $pdf->setHeaderData($ln='', $lw=0, $ht='', $data['header'], $tc=array(0,0,0), $lc=array(0,0,0));
		$pdf->setCustomFooterTextContract($data['footer']);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shelter');
		$pdf->SetTitle($data['title']);
		$pdf->SetSubject($data['title']);
		$pdf->SetKeywords('Shelter');

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(true);

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(15, 15, 17, 10);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$pdf->SetFont('times', '', 10);
		$pdf->AddPage();
		$pdf->writeHTML($data['content'], true, false, true, false, '');

		$base_path  = $data['basepath'];
		if(!is_dir($base_path)){
			mkdir($base_path, 0777, true);
		}
		return $pdf->Output($base_path.strtolower(str_replace(" ", "_",$data['title'])).'.pdf', 'F');
	}

	// private function generate_Word($data = array()){
	// 	$this->load->library('Word');
	// 	$filename = strtolower(str_replace(" ", "_",$data['title']));
	// 	$phpWord = new PHPWord();
	// 	$phpWord->createDoc($data['content'],$data['basepath'].$filename,0);
	// }
}
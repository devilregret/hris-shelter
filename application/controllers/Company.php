<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52))){
			redirect(base_url());
		}
	}

	public function delete($company_id = false)
	{
		$this->load->model('company_model');
		if ($company_id)
		{
			$data =  $this->company_model->get(array('id' => $company_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $company_id, 'is_active' => 0);
				$result = $this->company_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('company/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('company/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('company/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('company/list'));
		}
	}

	public function preview($company_id=FALSE)
	{
		$this->load->model('company_model');
		$data['_TITLE_'] 		= 'Preview Bisnis Unit';
		$data['_PAGE_']	 		= 'company/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'company';

		$data['id'] = $company_id;

		
		if (!$company_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('company/list'));
		}

		$data['preview'] = $this->company_model->preview(array('id' => $company_id));
		$this->load->view('company/preview', $data);
	}

	public function form($company_id = FALSE)
	{
		$this->load->model(array('company_model','city_model'));

		$data['id'] 		= '';
		$data['code']		= '';
		$data['name']		= '';
		$data['address'] 	= '';
		$data['city_id'] 	= '';

		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['code'] 		= $this->input->post('code');
			$data['name'] 		= $this->input->post('name');
			$data['address'] 	= $this->input->post('address');
			$data['city_id'] 	= $this->input->post('city_id');
			
			$this->form_validation->set_rules('name', '', 'required');
			$this->form_validation->set_rules('code', '', 'required');
			$this->form_validation->set_rules('city_id', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$insert = array('id' => $data['id'], 'name' => $data['name'], 'code' => $data['code'], 'address' => $data['address'], 'city_id' => $data['city_id']);
				$save_id	 	= $this->company_model->save($insert);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('company/list'));
		}

		if ($company_id)
		{
			$data = (array) $this->company_model->get(array('id' => $company_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('company/list'));
			}
		}

		$data['list_city'] 		= $this->city_model->list(array());

		$data['_TITLE_'] 		= 'Bisnis Unit';
		$data['_PAGE_'] 		= 'company/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'company';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Bisnis Unit';
		$data['_PAGE_'] 		= 'company/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'company';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('company_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.code, A.name, A.address, B.name AS city_name';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['code']			= $_POST['columns'][1]['search']['value'];
		$params['name']			= $_POST['columns'][2]['search']['value'];
		$params['city_name']	= $_POST['columns'][3]['search']['value'];
		$params['address']		= $_POST['columns'][4]['search']['value'];
		
		$list 	= $this->company_model->gets($params);
		$total 	= $this->company_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 		= $i;
			$result['code'] 	= $item->code;
			$result['name'] 	= $item->name;
			$result['city']		= $item->city_name;
			$result['address']	= $item->address;
			$result['action'] 	=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("company/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("company/form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("company/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}
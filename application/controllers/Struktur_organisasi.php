<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Struktur_organisasi extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,5,6))){
			redirect(base_url());
		}
	}

	// public function delete($struktur_id = false)
	// {
	// 	$this->load->model('struktur_jabatan_model');
	// 	if ($struktur_id)
	// 	{
	// 		$data =  $this->struktur_jabatan_model->get(array('id' => $struktur_id, 'columns' => 'A.id'));

	// 		if ($data)
	// 		{
	// 			$insert = array('id' => $struktur_id, 'is_active' => 0);
	// 			$result = $this->struktur_jabatan_model->save($insert);
	// 			if ($result) {
	// 				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
	// 				redirect(base_url('struktur_jabatan/list'));
	// 			}else{
	// 				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
	// 				redirect(base_url('struktur_jabatan/list'));
	// 			}
	// 		}else{
	// 			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
	// 			redirect(base_url('struktur_jabatan/list'));
	// 		}
	// 	}else{
	// 		$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
	// 		redirect(base_url('struktur_jabatan/list'));
	// 	}
	// }

	// public function preview($position_id = FALSE)
	// {
	// 	$this->load->model('position_model');
	// 	$data['_TITLE_'] 		= 'Preview Jabatan';
	// 	$data['_PAGE_']	 		= 'position/preview';
	// 	$data['_MENU_PARENT_'] 	= 'setting';
	// 	$data['_MENU_'] 		= 'position';

	// 	$data['id'] = $position_id;

		
	// 	if (!$position_id)
	// 	{
	// 		$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
	// 		redirect(base_url('position/list'));
	// 	}

	// 	$data['preview'] = $this->position_model->preview(array('id' => $position_id));
	// 	$this->load->view('position/preview', $data);
	// }

	public function form($struktur_id = FALSE)
	{
		$this->load->model(array('struktur_organisasi_model', 'position_model'));

		$data['id'] 				= '';
		$data['position_id']			= '';
		$data['parent_id']			= '';

		if($this->input->post()){
			$data['id'] 			= $this->input->post('id');
			$data['parent_id'] 		= $this->input->post('parent_id');
			$data['position_id'] 	= $this->input->post('position_id');
			$tmpEmployee 			= $this->input->post('employee_id');
			
			$this->form_validation->set_rules('parent_id', '', 'required');
			$this->form_validation->set_rules('position_id', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$save_id	 	= $this->struktur_organisasi_model->save($data, $tmpEmployee);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('struktur_organisasi/list'));
		}

		if ($struktur_id)
		{
			$data = (array) $this->struktur_organisasi_model->get(array('id' => $struktur_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('struktur_organisasi/list'));
			}
		}

		$data['list_posisi'] 	= $this->position_model->gets(array('company' => 3));
		$data['list_parent'] 	= $this->struktur_organisasi_model->getMasterParent();

		$data['_TITLE_'] 		= 'Struktur Organisasi';
		$data['_PAGE_'] 		= 'struktur_organisasi/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'struktur_organisasi';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Struktur Organisasi';
		$data['_PAGE_'] 		= 'struktur_organisasi/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'struktur_organisasi';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('struktur_organisasi_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, E.name as parent_name, B.name as posisi_name, C.full_name as employee_name ';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['name']			= $_POST['columns'][1]['search']['value'];
		$params['description']	= $_POST['columns'][2]['search']['value'];

		$list 	= $this->struktur_organisasi_model->getStrukturOrganisasi($params);
		$total 	= $this->struktur_organisasi_model->getStrukturOrganisasi($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		// echo "<script>console.log('Debug Objects: ssdsd' );</script>";
		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['parent_name'] 		= $item->parent_name;
			$result['posisi_name'] 		= $item->posisi_name;
			$result['employee_name'] 	= $item->employee_name;
			$result['action'] 		=
				'<a class="btn-sm btn-success btn-block" href="'.base_url("struktur_organisasi/form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("struktur_organisasi/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function list_employee_by_position(){
		$this->load->model(array('employee_model'));
		
		$data 	= array();
		$employee 	= $this->employee_model->gets(array('position_id' => $_POST['position_id'], 'columns' => 'A.id, A.full_name'));
		// print_r($_POST['position_id']);
		// die();
		// echo "<script>console.log('Debug Objects: ssdsd' );</script>";
		foreach($employee as $item)
		{
			$result['full_name'] 	= $item->full_name;
			$result['id'] 			= $item->id;

			array_push($data, $result);
		}
		
		$response = array(
		// 	"iTotalRecords" 		=> $total,
		// 	"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		header('Content-Type: application/json');
		echo json_encode($response);
		// echo json_encode($data);
	}

	public function export()
	{

		$this->load->model(array('position_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = 'jabatan';
		$files = glob(FCPATH."files/".$title."/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Nama Jabatan");
		$excel->getActiveSheet()->setCellValue("B".$i, "Tipe Jabatan");

		$params 			= $this->input->get();
		$params['columns'] 	= 'A.id, A.name, A.description';
		$list_position 		= $this->position_model->gets($params);
		
		$i=2;
		foreach ($list_position as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->name);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->description);
			$i++;
		}
		$excel->getActiveSheet()->setTitle('Jabatan');
		
		$excel->createSheet();
		$excel->setActiveSheetIndex(1);
		$excel->getActiveSheet()->setCellValue('A1', 'Nama Tipe');
		$excel->getActiveSheet()->setCellValue('A2', 'Security');
		$excel->getActiveSheet()->setCellValue('A3', 'Non Security');
		$excel->getActiveSheet()->setCellValue('A4', 'Produksi');
		$excel->getActiveSheet()->setTitle('Tipe Jabatan');

		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	// public function import(){

	// 	if(!$_FILES['file']['name'] == ""){

	// 		$this->load->model(array('position_model'));
			
	// 		$this->load->library("Excel");
			
	// 		$excelreader 	= new PHPExcel_Reader_Excel2007();
	// 		$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
	// 		$sheet  		= $loadexcel->getActiveSheet(0);
			
	// 		$rend			= $sheet->getHighestRow();
			
	// 		for ($row = 2; $row <= $rend; $row++) {
	// 			$data['id'] 			= '';
	// 			$data['name']			= '';
	// 			$data['description']	= '';
	// 			$data['name'] 			= replace_null($sheet->getCell('A'.$row)->getValue());
	// 			$position = $this->position_model->get(array('is_name' => $data['name'], 'columns' => 'A.id'));
	// 			if($position){
	// 				continue;
	// 			}
	// 			$data['description'] 			= replace_null($sheet->getCell('B'.$row)->getValue());
	// 			$save_id = $this->position_model->save($data);
	// 		}
	// 		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
	// 		redirect(base_url('position/list'));
	// 	}else{
	// 		$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
	// 		redirect(base_url('position/list'));
	// 	}
	// }
}
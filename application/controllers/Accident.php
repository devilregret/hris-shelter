<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accident extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('benefit'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,5,6,13,14))){
			redirect(base_url());
		}
	}

	public function preview($template_id = FALSE)
	{
		$this->load->model('accident_model');
		$data['_TITLE_'] 		= 'Preview Template Keterangan Kerja';
		$data['_PAGE_'] 		= 'accident/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'accident';

		$data['id'] = $template_id;

		
		if (!$template_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('accident/list'));
		}

		$data['preview'] = $this->accident_model->preview(array('id' => $template_id));
		$this->load->view('accident/preview', $data);
	}
	
	public function form($template_id = FALSE)
	{
		$this->load->model(array('accident_model'));

		$data['id'] 		= '';
		$data['title']		= '';
		$data['content']	= '';

		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['title']		= $this->input->post('title');
			$data['content']	= $this->input->post('content');
			
			$this->form_validation->set_rules('title', '', 'required');
			$this->form_validation->set_rules('content', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$insert = array('id' => $data['id'], 'title' => $data['title'], 'content' => $data['content']);
				
				$save_id	 	= $this->accident_model->save($insert);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('accident/list'));
		}

		if ($template_id)
		{
			$data = (array) $this->accident_model->get(array('id' => $template_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('accident/list'));
			}
		}

		$data['_TITLE_'] 		= 'Template Laporan Kecelakaan';
		$data['_PAGE_'] 		= 'accident/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'accident';

		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Template Laporan Kecelakaan';
		$data['_PAGE_'] 		= 'accident/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'accident';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('accident_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.title, D.name AS company_name';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['company_name']	= $_POST['columns'][1]['search']['value'];
		$params['title']	= $_POST['columns'][2]['search']['value'];

		$list 	= $this->accident_model->gets($params);
		$total 	= $this->accident_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['title'] 		= $item->title;
			$result['company_name'] = $item->company_name;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("accident/preview/".$item->id).'">Preview</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("accident/form/".$item->id).'">Ubah</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}
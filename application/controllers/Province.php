<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Province extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52))){
			redirect(base_url());
		}
	}

	public function delete($province_id = false)
	{
		$this->load->model('province_model');
		if ($province_id)
		{
			$data =  $this->province_model->get(array('id' => $province_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $province_id, 'is_active' => 0);
				$result = $this->province_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('province/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('province/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('province/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('province/list'));
		}
	}

	public function preview($province_id=FALSE)
	{
		$this->load->model('province_model');
		$data['_TITLE_'] 		= 'Preview Kota';
		$data['_PAGE_']	 		= 'province/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'province';

		$data['id'] = $province_id;

		
		if (!$province_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('province/list'));
		}

		$data['preview'] = $this->province_model->preview(array('id' => $province_id));
		$this->load->view('province/preview', $data);
	}

	public function form($province_id = FALSE)
	{
		$this->load->model(array('province_model','branch_model'));

		$data['id'] 		= '';
		$data['name']		= '';
		$data['branch_id']	= '';
		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['name'] 		= $this->input->post('name');
			$data['branch_id']= $this->input->post('branch_id');
			
			$this->form_validation->set_rules('name', '', 'required');
			$this->form_validation->set_rules('branch_id', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$insert = array('id' => $data['id'], 'name' => $data['name'], 'branch_id' => $data['branch_id']);
				$save_id	 	= $this->province_model->save($insert);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('province/list'));
		}

		if ($province_id)
		{
			$data = (array) $this->province_model->get(array('id' => $province_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('province/list'));
			}
		}

		$data['list_branch'] 	= $this->branch_model->gets(array('columns' => 'A.id, A.name'));
		$data['_TITLE_'] 		= 'Provinsi';
		$data['_PAGE_'] 		= 'province/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'province';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] = 'Provinsi';
		$data['_PAGE_'] = 'province/list';
		$data['_MENU_PARENT_'] = 'setting';
		$data['_MENU_'] = 'province';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('province_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.name AS name, B.name AS branch_name';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['name']			= $_POST['columns'][1]['search']['value'];
		$params['branch_name']	= $_POST['columns'][2]['search']['value'];
		
		$list 	= $this->province_model->gets($params);
		$total 	= $this->province_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['name'] 		= $item->name;
			$result['branch_name']	= $item->branch_name;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("province/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("province/form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("province/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History_tes extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('recruitment'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function list(){
		$this->load->model(array('vacancy_model', 'province_model', 'site_model', 'position_model', 'company_model','history_tes_model'));

		$data['_TITLE_'] 		= 'History Tes';
		$data['_PAGE_'] 		= 'history_tes/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'history_tes';
                                                                
		$data['list_province'] 		= $this->history_tes_model->get_list_data_combo_province();
		$data['list_site'] 			= $this->history_tes_model->get_list_data_combo_site();
		$data['list_position'] 		= $this->history_tes_model->get_list_data_combo_position();
		$data['list_company'] 		= $this->history_tes_model->get_list_data_combo_company();
		$data['list_lowongan'] 		= $this->history_tes_model->get_list_data_combo_lowongan();
		$data['list_employee'] 		= $this->history_tes_model->get_list_data_combo_employee();

		$this->view($data);
	}

	public function list_ajax(){
		$this->load->model(array('history_tes_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, DATE_FORMAT(A.tgl_tes, '%d/%m/%Y') AS tgl_tes,B.title, H.keterangan as stes,A.tes,A.nama,A.usia,A.jk,A.kategori, G.name AS position_name,F.name AS site_name,E.name AS company_name,D.name AS branch_name, C.name AS province_name ,A.doc_id";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['stes']				= $_POST['columns'][2]['search']['value'];
		$params['nama']				= $_POST['columns'][3]['search']['value'];
		$params['tgl_tes']			= $_POST['columns'][6]['search']['value'];
		$params['kategori']			= $_POST['columns'][7]['search']['value'];
		$params['title']			= $_POST['columns'][8]['search']['value'];
		$params['position_name']	= $_POST['columns'][9]['search']['value'];
		$params['site_name']		= $_POST['columns'][10]['search']['value'];
		$params['company_name']		= $_POST['columns'][11]['search']['value'];
		$params['branch_name']		= $_POST['columns'][12]['search']['value'];
		$params['province_name']	= $_POST['columns'][13]['search']['value'];

		if($this->session->userdata('branch') > 1){
			$params['session_branch']	= $this->session->userdata('branch');
		}

		$list 	= $this->history_tes_model->gets($params);
		$total 	= $this->history_tes_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 					= $i;
			$result['id'] 					= $item->id;
			$result['tgl_tes'] 				= $item->tgl_tes;
			$result['stes'] 				= $item->stes;
			$result['nama']					= $item->nama;
			$result['usia']					= $item->usia;
			$result['jk']					= $item->jk;
			$result['kategori']				= $item->kategori;
			$result['title']				= $item->title;
			$result['position_name']		= $item->position_name;
			$result['site_name']			= $item->site_name;
			$result['company_name']			= $item->company_name;
			$result['branch_name']			= $item->branch_name;
			$result['province_name']		= $item->province_name;

			$result['action'] 				=
				'<a onclick="previewHasil(this)" class="btn-sm btn-primary btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("history_tes/"."result?id=".$item->id).'"><i class="fas fa-file-pdf"></i> &nbsp;Lihat</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("history_tes/delete/".$item->tes."/".$item->doc_id).'"><i class="fas fa-trash"></i> &nbsp;Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}
	public function delete($ptes,$pdocid)
	{
		$this->load->model('psikotes_model');
		$this->load->model('deret_angka_model');
		$this->load->model('hitung_cepat_model');
		$this->load->model('ketelitian_model');
		$this->load->model('kraepelin_model');

		$data['id'] = $pdocid;
		if($ptes == 'psikotes'){
			$this->psikotes_model->delete($data);
		}else if ($ptes == 'deret_angka'){
			$this->deret_angka_model->delete($data);
		}else if ($ptes == 'hitung_cepat'){
			$this->hitung_cepat_model->delete($data);
		}else if ($ptes == 'ketelitian'){
			$this->ketelitian_model->delete($data);
		}else if ($ptes == 'kraepelin'){
			$this->kraepelin_model->delete($data);
		}

		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
		redirect(base_url('history_tes/list'));
	}

	public function result (){
		$this->load->model(array('history_tes_model','psikotes_model','deret_angka_model','hitung_cepat_model','ketelitian_model','kraepelin_model'));
		$company_id 	= isset($_GET['company_id'])?$_GET['company_id']:null;
		$site_id 		= isset($_GET['site_id'])?$_GET['site_id']:null;
		$position_id 	= isset($_GET['position_id'])?$_GET['position_id']:null;
		$vacancy_id 	= isset($_GET['vacancy_id'])?$_GET['vacancy_id']:null;
		$employee_id 	= isset($_GET['employee_id'])?$_GET['employee_id']:null;
		$order 			= isset($_GET['order'])?$_GET['order']:null;
		$selectTes		= isset($_GET['tes'])?$_GET['tes']:null;
		$id				= isset($_GET['id'])?$_GET['id']:null;
		$doc_id			= isset($_GET['doc_id'])?$_GET['doc_id']:null;

		$listTes 					= $this->history_tes_model->getListHistoryTes($selectTes,$company_id,$site_id,$position_id,$vacancy_id,$employee_id,$order,$id,$doc_id);
		$deret_angka_list_deret		= $this->deret_angka_model->getDeret();
		$deret_angka_list_jawaban 	= $this->deret_angka_model->getJawaban();
		$hitung_cepat_list_jawaban	= $this->hitung_cepat_model->getJawaban();

		foreach ($listTes as $key => $value) {
			if($value->tes == 'psikotes'){ //Psikotes
				$psikotes				= $this->psikotes_model->get_data_psikotes($value->doc_id);
				$psikotes_d 			= $this->psikotes_model->get_data_psikotes_d($value->doc_id);
				$psikotes_result 		= $this->psikotes_model->get_data_psikotes_result($value->doc_id);
				$psikotes_result_disc 	= $this->psikotes_model->get_data_psikotes_result_disc($value->doc_id);
				$psikotes_result_line 	= $this->psikotes_model->get_data_psikotes_result_line($value->doc_id);
				$psikotes_employee 		= $this->psikotes_model->getEmployeeInfo($psikotes[0]->m_employee_id);

				$obj = (object) array(
					'psikotes' 				=> $psikotes,
					'psikotes_d' 			=> $psikotes_d,
					'psikotes_result' 		=> $psikotes_result,
					'psikotes_result_disc' 	=> $psikotes_result_disc,
					'psikotes_result_line' 	=> $psikotes_result_line,
					'psikotes_employee' 	=> $psikotes_employee,
				);

				$value->result = $obj;
			} else if($value->tes == 'deret_angka') { //Deret Angka
				$deret_angka				= $this->deret_angka_model->get_data_deret_angka($value->doc_id);
				$deret_angka_d				= $this->deret_angka_model->get_data_deret_angka_d($value->doc_id);
				$deret_angka_employee 		= $this->psikotes_model->getEmployeeInfo($deret_angka[0]->m_employee_id);

				$obj = (object) array(
					'deret_angka' 				=> $deret_angka,
					'deret_angka_d' 			=> $deret_angka_d,
					'deret_angka_employee' 		=> $deret_angka_employee,
					'deret_angka_list_deret' 	=> $deret_angka_list_deret,
					'deret_angka_list_jawaban' 	=> $deret_angka_list_jawaban,
				);
				$value->result = $obj;
			} else if($value->tes == 'hitung_cepat') { //Hitung Cepat
				$hitung_cepat 			= $this->hitung_cepat_model->get_data_hitung_cepat($value->doc_id);
				$hitung_cepat_d 		= $this->hitung_cepat_model->get_data_hitung_cepat_d($value->doc_id);	
				$hitung_cepat_employee 	= $this->psikotes_model->getEmployeeInfo($hitung_cepat[0]->m_employee_id);

				$obj = (object) array(
					'hitung_cepat' 				=> $hitung_cepat,
					'hitung_cepat_d' 			=> $hitung_cepat_d,
					'hitung_cepat_employee' 	=> $hitung_cepat_employee,
					'hitung_cepat_list_jawaban'	=> $hitung_cepat_list_jawaban,
				);
				$value->result = $obj;
			} else if($value->tes == 'ketelitian') { // Ketelitian
				$ketelitian = $this->ketelitian_model->get_data_ketelitian($value->doc_id);
				$ketelitian_d = $this->ketelitian_model->get_data_ketelitian_d($value->doc_id);
				$ketelitian_employee = $this->psikotes_model->getEmployeeInfo($ketelitian[0]->m_employee_id);
				
				$obj = (object) array(
					'ketelitian' 			=> $ketelitian,
					'ketelitian_d' 			=> $ketelitian_d,
					'ketelitian_employee' 	=> $ketelitian_employee,
				);
				$value->result = $obj;
			} else if($value->tes == 'kraepelin') { // Kraepelin
				$kraepelin			= $this->kraepelin_model->get_data_kraepelin($value->doc_id);
				$kraepelin_d		= $this->kraepelin_model->get_data_kraepelin_d($value->doc_id);
				$kraepelin_employee	= $this->psikotes_model->getEmployeeInfo($kraepelin[0]->m_employee_id);
				$grade 				= $kraepelin_employee[0]->education_level;
				$jurusan 			= $kraepelin_employee[0]->education_majors;
				if($grade == 'S1'||$grade == 'S2'||$grade == 'S3'){
					if($jurusan=='IPS'){
						$grade = 'S1 IPS';
					}else{
						$grade = 'S1 IPA';
					}
				}else{
					$grade = "SMA";
				}

				$kraepelin[0]->nilai_kecepatan_kerja 			= $this->kraepelin_model->convert_nilai($grade,'panker',$kraepelin[0]->kecepatan_kerja);
				$kraepelin[0]->nilai_ketelitian_kerja 			= $this->kraepelin_model->convert_nilai($grade,'tianker',$kraepelin[0]->ketelitian_kerja);
				$kraepelin[0]->nilai_keajegan_kerja_range 		= $this->kraepelin_model->convert_nilai($grade,'janker range',$kraepelin[0]->keajegan_kerja_range);
				$kraepelin[0]->nilai_keajegan_kerja_average_d 	= $this->kraepelin_model->convert_nilai($grade,'janker average',$kraepelin[0]->keajegan_kerja_average_d);
				$kraepelin[0]->nilai_ketahanan_kerja 			= $this->kraepelin_model->convert_nilai($grade,'hanker',$kraepelin[0]->ketahanan_kerja);
				
				$obj = (object) array(
					'kraepelin' 			=> $kraepelin,
					'kraepelin_d' 			=> $kraepelin_d,
					'kraepelin_employee' 	=> $kraepelin_employee,
				);
				$value->result = $obj;
			}
		}

		// SUMMARY
		$data['is_hanya_hasil']	= $this->input->get('is_hanya_hasil');
		$data['list_data']		= $listTes;
		$data['petugas']		= $this->session->userdata('name');
		$data['_TITLE_'] 		= 'Summary Hasil Tes';
		$data['_PAGE_'] 		= 'history_tes/result';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'history_tes';
		return $this->view($data);
	}

	public function export()
	{
		$this->load->model(array('history_tes_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = "Hasil Tes";
		$title = 'hasil_tes';
		$files = glob(FCPATH."files/".$title."/*");

		$params = $this->input->get();

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "ID");
		$excel->getActiveSheet()->setCellValue("B".$i, "Bisnis Unit");
		$excel->getActiveSheet()->setCellValue("C".$i, "Site Bisnis");
		$excel->getActiveSheet()->setCellValue("D".$i, "Provinsi");
		$excel->getActiveSheet()->setCellValue("E".$i, "Posisi");
		$excel->getActiveSheet()->setCellValue("F".$i, "Judul Lowongan");
		$excel->getActiveSheet()->setCellValue("G".$i, "Tes");
		$excel->getActiveSheet()->setCellValue("H".$i, "Nama");
		$excel->getActiveSheet()->setCellValue("I".$i, "Usia (Thn)");
		$excel->getActiveSheet()->setCellValue("J".$i, "L/P");
		$excel->getActiveSheet()->setCellValue("K".$i, "Tgl Tes");
		$excel->getActiveSheet()->setCellValue("L".$i, "Tgl Dimulai");
		$excel->getActiveSheet()->setCellValue("M".$i, "Tgl Selesai");
		$excel->getActiveSheet()->setCellValue("N".$i, "Persentase");
		$excel->getActiveSheet()->setCellValue("O".$i, "Persentase Minimal");
		$excel->getActiveSheet()->setCellValue("P".$i, "Kategori");
		$excel->getActiveSheet()->setCellValue("Q".$i, "Jawaban Benar");
		$excel->getActiveSheet()->setCellValue("R".$i, "Jawaban Salah");
		$excel->getActiveSheet()->setCellValue("S".$i, "Tidak Diisi");
		$excel->getActiveSheet()->setCellValue("T".$i, "Petugas");

		// return print_r($params['tes']);

		$list_candidate = $this->history_tes_model->get_export_tes($params);
		$i=2;
		foreach ($list_candidate as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->id);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->company_name);
			$excel->getActiveSheet()->setCellValue("C".$i, $item->site_name);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->province_name);
			$excel->getActiveSheet()->setCellValue("E".$i, $item->position_name);
			$excel->getActiveSheet()->setCellValue("F".$i, $item->title);
			$excel->getActiveSheet()->setCellValue("G".$i, $item->tes);
			$excel->getActiveSheet()->setCellValue("H".$i, $item->nama);
			$excel->getActiveSheet()->setCellValue("I".$i, $item->usia);
			$excel->getActiveSheet()->setCellValue("J".$i, $item->jk);
			$excel->getActiveSheet()->setCellValue("K".$i, $item->tgl_tes);
			$excel->getActiveSheet()->setCellValue("L".$i, $item->tes_dimulai);
			$excel->getActiveSheet()->setCellValue("M".$i, $item->tes_selesai);
			$excel->getActiveSheet()->setCellValueExplicit("N".$i, $item->persentase);
			$excel->getActiveSheet()->setCellValueExplicit("O".$i, $item->persentase_minimal);
			$excel->getActiveSheet()->setCellValue("P".$i, $item->kategori);
			$excel->getActiveSheet()->setCellValueExplicit("Q".$i, $item->jawaban_benar);
			$excel->getActiveSheet()->setCellValueExplicit("R".$i, $item->jawaban_salah);
			$excel->getActiveSheet()->setCellValueExplicit("S".$i, $item->tidak_diisi);
			$excel->getActiveSheet()->setCellValue("T".$i, $item->petugas);

			// $date = new DateTime($item->contract_end);
			// $excel->getActiveSheet()->setCellValue("K".$i, PHPExcel_Shared_Date::PHPToExcel($date));
			// $excel->getActiveSheet()->getStyle("K".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			$i++;
		}
		$excel->getActiveSheet()->setTitle('Hasil Tes');
		
		$excel->createSheet();
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}


}
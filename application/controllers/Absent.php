<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absent extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!in_array($this->session->userdata('role'), array(2,3,8))){
			redirect(base_url());
		}
		if($this->session->userdata('site') == ''){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan pilih site bisnis.','danger'));
			redirect(base_url());	
		}
	}

	public function list(){
		
		$this->load->model(array('site_model'));

		$data['_TITLE_'] 		= 'Potongan';
		$data['_PAGE_'] 		= 'absent/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'absent';
		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address'));
		$this->view($data);
	}

	public function list_ajax(){

		$this->load->model(array('absent_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.name, A.value";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['name']			= $_POST['columns'][1]['search']['value'];
		$params['value']		= $_POST['columns'][2]['search']['value'];
		$params['site_id']		= $this->session->userdata('site');
		$list 	= $this->absent_model->gets($params);
		$total 	= $this->absent_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{			
			$result['no'] 			= $i;
			$result['name']			= $item->name;
			$result['value']		= $item->value;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action"  style="cursor:pointer;" data-href="'. base_url("absent/preview/".$item->id).'"><i class="fas fa-eye text-white"></i></a>
				<a class="btn-sm btn-success" href="'.base_url("absent/form/".$item->id).'"><i class="fas fa-edit"></i></a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger" style="cursor:pointer;" data-href="'.base_url("absent/delete/".$item->id).'"><i class="far fa-trash-alt text-white"></i></a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function form($absent_id = FALSE)
	{
		$this->load->model(array('absent_model'));

		$data['id'] 		= '';
		$data['name']		= 'Mangkir';
		$data['value']		= 0;

		if($this->input->post()){
			$data['id'] 	= $this->input->post('id');
			$data['name'] 	= $this->input->post('name');
			$data['value']	= str_replace(".", "", $this->input->post('value'));
			
			$this->form_validation->set_rules('name', '', 'required');
			$this->form_validation->set_rules('value', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$insert = array(
					'id' => $data['id'], 
					'name' => $data['name'], 
					'value' => $data['value'],
					'site_id' => $this->session->userdata('site'));
				$save_id	 	= $this->absent_model->save($insert);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('absent/list'));
		}

		if ($absent_id)
		{
			$data = (array) $this->absent_model->get(array('id' => $absent_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('absent/list'));
			}
		}

		$data['_TITLE_'] 		= 'Potongan';
		$data['_PAGE_'] 		= 'absent/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'absent';
		return $this->view($data);
	}

	public function preview($absent_id=FALSE)
	{
		$this->load->model(array('absent_model'));
		$data['_TITLE_'] 		= 'Preview Potongan';
		$data['_PAGE_']	 		= 'absent/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'absent';
		
		if (!$absent_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('absent/list'));
		}

		$data['preview'] = $this->absent_model->preview(array('id' => $absent_id, 'columns' => 'A.*'));
		$this->load->view('absent/preview', $data);
	}

	public function delete($absent_id = false)
	{
		$this->load->model('absent_model');
		if ($absent_id)
		{
			$data =  $this->absent_model->get(array('id' => $absent_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $absent_id, 'is_active' => 0);
				$result = $this->absent_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('absent/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('absent/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('absent/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('absent/list'));
		}
	}
}
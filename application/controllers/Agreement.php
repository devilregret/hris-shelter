<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agreement extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('recruitment'));
		$role   = array_merge($role, $this->config->item('directur'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}
	
	public function list(){
		$data['_TITLE_'] 		= 'Perjanjian Kerjasama';
		$data['_PAGE_'] 		= 'agreement/list';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= 'agreement';
		$this->view($data);
	}

	public function list_ajax(){
		$this->load->model(array('agreement_model'));

		$column_index 			= $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.agency_name, DATE_FORMAT(A.start_contract, '%d/%m/%Y') AS start_contract, DATE_FORMAT(A.end_contract , '%d/%m/%Y') AS end_contract, A.document, B.name AS city_name, C.name AS province_name, D.name AS branch_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];

		$params['agency_name']		= $_POST['columns'][0]['search']['value'];
		$params['start_contract']	= $_POST['columns'][1]['search']['value'];
		$params['end_contract']		= $_POST['columns'][2]['search']['value'];
		$params['city_name']		= $_POST['columns'][3]['search']['value'];
		$params['province_name']	= $_POST['columns'][4]['search']['value'];
		$params['branch_name']		= $_POST['columns'][5]['search']['value'];

		if($this->session->userdata('branch') > 1){
			$params['branch_id']	= $this->session->userdata('branch');
		}

		$list 	= $this->agreement_model->gets($params);
		$total 	= $this->agreement_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['agency_name'] 		= $item->agency_name;
			$result['start_contract']	= $item->start_contract;
			$result['end_contract']		= $item->end_contract;
			$result['city_name'] 		= $item->city_name;
			$result['province_name'] 	= $item->province_name;
			$result['branch_name']		= $item->branch_name;
			$result['document']			= '';
			if($item->document != ''){
				$result['document'] 	= '<a href="'.$item->document.'" target="_blank" alt="Perjanjian Kerjasama" >Dokumen</a>';
			}
			$result['action'] 			=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("agreement/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("agreement/form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("agreement/delete/".$item->id).'">Hapus</a>';
	
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function form($agreement_id = FALSE)
	{
		$this->load->model(array('agreement_model', 'city_model'));

		$data['id'] 			= '';
		$data['agency_name'] 	= '';
		$data['start_contract']	= '';
		$data['end_contract']	= '';
		$data['city_id'] 		= '';

		if($this->input->post()){
			$data['id'] 			= $this->input->post('id');
			$data['agency_name']	= $this->input->post('agency_name');
			$data['start_contract']	= $this->input->post('start_contract');
			$data['end_contract']	= $this->input->post('end_contract');
			$data['city_id']		= $this->input->post('city_id');
			
			$this->form_validation->set_rules('agency_name', '', 'required');
			$this->form_validation->set_rules('start_contract', '', 'required');
			$this->form_validation->set_rules('end_contract', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$save_id	= $this->agreement_model->save($data);
				if (!$save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));		
					redirect(base_url('agreement/list'));
					die();
				}

				$url_file = 'files/agreement/'.$save_id;
				$file_path = FCPATH.$url_file;
				if(!is_dir($file_path)){
					mkdir($file_path, 0755, TRUE);
				}

				$config['upload_path'] 	= $file_path;
				$config['overwrite']  	= TRUE;
				$config['allowed_types']= 'jpg|jpeg|png|pdf';
				if($_FILES['document']['name'] != ""){
					$config['allowed_types']= 'jpg|jpeg|png|pdf';
					$config['file_name'] 	= 'agreement';
					$this->load->library('upload', $config, 'document');

					if (!$this->document->do_upload('document')) {
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> '.$this->file->display_errors().'.','danger'));
					} else {
						$document 	= $this->document->data();
						$save_id = $this->agreement_model->save(array('id' => $save_id, 'document' => base_url($url_file.'/'.$document['file_name'])));
					}
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));		
			redirect(base_url('agreement/list'));
		}

		if ($agreement_id)
		{
			$data = (array) $this->agreement_model->get(array('id' => $agreement_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('agreement/list'));
			}
		}else{
			$data['document']		= '';
		}

		if($this->session->userdata('branch') > 1){
			$data['list_city'] 	= $this->city_model->gets(array('branch_id' => $this->session->userdata('branch'), 'columns' => 'A.id, A.name, B.name AS province_name'));
		}else{
			$data['list_city'] 	= $this->city_model->gets(array('columns' => 'A.id, A.name, B.name AS province_name'));
		}

		$data['_TITLE_'] 		= 'Perjanjian Kerjasama';
		$data['_PAGE_'] 		= 'agreement/form';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= 'agreement';
		return $this->view($data);
	}

	public function preview($agreement_id=FALSE)
	{
		$this->load->model('agreement_model');
		$data['_TITLE_'] 		= 'Preview Perjanjian Kerjasama';
		$data['_PAGE_']	 		= 'agreement/preview';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= 'agreement';

		$data['id'] = $agreement_id;

		
		if (!$agreement_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('agreement/list'));
		}

		$data['preview'] = $this->agreement_model->preview(array('id' => $agreement_id));
		$this->load->view('agreement/preview', $data);
	}

	public function delete($agreement_id = false)
	{
		$this->load->model('agreement_model');
		if ($agreement_id)
		{
			$data =  $this->agreement_model->get(array('id' => $agreement_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $agreement_id, 'is_active' => 0);
				$result = $this->agreement_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('agreement/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('agreement/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('agreement/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('agreement/list'));
		}
	}
}
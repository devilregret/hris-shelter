<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Accurate_report extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		$role 	= array_merge($role, $this->config->item('accounting'));
		$role 	= array_merge($role, $this->config->item('directur'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function site(){
		$data['_TITLE_'] 		= 'Daftar Site Bisnis';
		$data['_PAGE_'] 		= 'accurate_report/site';
		$data['_MENU_PARENT_'] 	= 'accurate_report';
		$data['_MENU_'] 		= 'accurate_site';

		$this->view($data);
	}
	
	public function site_ajax(){

		$this->load->model(array('proyek_accurate_model', 'payroll_model'));

		$column_index = $_POST['order'][0]['column'];

		$this->load->model(array('proyek_accurate_model'));
		$list_site = $this->proyek_accurate_model->gets_site(array('columns' => 'A.id, A.proyek_id'));

		$params['list_proyek'] = array();
		foreach($list_site AS $item){
			$proyek		= explode(",", $item->proyek_id);
			foreach($proyek as $key) {
				array_push($params['list_proyek'],$key);
			}
		}

		$params['columns']	= 'A.id';
		$params['group_by'] = 'A.nomor_proyek';
		
		$params['nomor_proyek']		= $_POST['columns'][0]['search']['value'];
		$params['nama_proyek']		= $_POST['columns'][1]['search']['value'];
		$params['kode_pelanggan']	= $_POST['columns'][2]['search']['value'];
		$total 	= count($this->proyek_accurate_model->gets_report($params));

		$params['columns']	= 'A.id, A.nomor_proyek, A.nama_proyek, B.accurate_customer_no AS kode_pelanggan, C.id AS site_id';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];

		
		$list 	= $this->proyek_accurate_model->gets_report($params);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['nomor_proyek']			= $item->nomor_proyek;
			$result['nama_proyek']			= $item->nama_proyek;
			$result['kode_pelanggan']		= $item->kode_pelanggan;
			$result['periode']				= '';
			$result['headcount']			= '';
			$result['gaji_pokok']			= '';
			$result['benefit_perusahaan']	= '';
			$result['pendapatan_lain']		= '';
			$result['potongan_benefit']		= '';
			$result['pph21']				= '';
			$result['potongan_lain']		= '';
			$result['thp']					= '';
			$periode_end = '';
			$last_payroll	= $this->payroll_model->last_payroll(array('site_id' => $item->site_id, 'payment' => 'Selesai', 'columns' => 'MAX(A.periode_start) AS periode_start, MAX(A.periode_end) AS periode_end'));
			if($last_payroll){
				$start_date = substr($last_payroll->periode_start, 8, 2).' '.get_month(substr($last_payroll->periode_start, 5, 2));
				$end_date 	= substr($last_payroll->periode_end, 8, 2).' '.get_month(substr($last_payroll->periode_end, 5, 2)).' '.substr($last_payroll->periode_end, 0, 4);
				$result['periode'] 	= $start_date.' - '.$end_date;

				$periode_end 			= $last_payroll->periode_end;
				$filter['site_id']		= $item->site_id;
				$filter['payment']		= 'Selesai';
				$filter['periode_end']	= $last_payroll->periode_end;
				$filter['columns']		= 'COUNT(A.id) AS headcount, SUM(A.basic_salary) AS basic_salary, SUM(A.bpjs_ks_company) AS bpjs_ks_company, SUM(A.bpjs_jht_company) AS bpjs_jht_company, SUM(A.bpjs_ks) AS bpjs_ks, SUM(A.bpjs_jht) AS bpjs_jht, SUM(A.tax_calculation) AS tax_calculation ,SUM(A.salary) AS salary';
				
				$summary_payroll	= $this->payroll_model->last_payroll($filter);
				
				$result['headcount']			= $summary_payroll->headcount;
				$result['gaji_pokok']			= format_rupiah($summary_payroll->basic_salary);
				$result['benefit_perusahaan']	= format_rupiah(($summary_payroll->bpjs_ks_company+$summary_payroll->bpjs_jht_company));
				$result['potongan_benefit']		= format_rupiah(($summary_payroll->bpjs_ks+$summary_payroll->bpjs_jht));
				$result['pph21']				= format_rupiah($summary_payroll->tax_calculation);
				$result['thp']					= format_rupiah($summary_payroll->salary);
				

				$filter['columns']		= 'SUM(A.value) AS value';
				$filter['category']		= 'pendapatan';
				$summary_pendapatan		= $this->payroll_model->gets_detail($filter);
				if($summary_pendapatan){
					$result['pendapatan_lain']	= format_rupiah($summary_pendapatan[0]->value);
				}
				$filter['category']		= 'pendapatan';
				$summary_potongan				= $this->payroll_model->gets_detail($filter);
				if($summary_potongan){
					$result['potongan_lain']	= format_rupiah($summary_potongan[0]->value);
				}
			}

			$result['action'] 			= 
				'<a class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" href="'. base_url("accurate_report/list/".$item->id."/".$item->site_id).'">Detail</a>
				 <a class="btn-sm btn-success btn-action btn-block"  style="cursor:pointer;" href="'. base_url("accurate_report/export/".$item->id."/".$item->site_id."/".$periode_end).'">Export</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function list($proyek_id = FALSE, $site_id = FALSE){
		$this->load->model(array('site_model'));
		$data['site'] 			= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.id, A.name, A.address'));
		$data['proyek_id'] 		= $proyek_id;
		$data['_TITLE_'] 		= 'Laporan Payroll';
		$data['_PAGE_'] 		= 'accurate_report/list';
		$data['_MENU_PARENT_'] 	= 'accurate_report';

		$data['_MENU_'] 		= 'accurate_site';
		$this->view($data);
	}

	public function list_ajax($proyek_id = FALSE, $site_id = FALSE){
		$this->load->model(array('payroll_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "C.id AS site_id, C.name AS site_name, D.code AS company_code, COUNT(A.employee_id) AS employee_count, SUM(A.salary) AS summary_payroll, SUM(A.basic_salary) AS summary_basic, MIN(A.periode_start) AS periode_start, MAX(A.periode_end) AS periode_end ";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];

		$params['site_name']	= $_POST['columns'][1]['search']['value'];
		$params['company_code']	= $_POST['columns'][2]['search']['value'];
		$params['payment']		= array('Selesai');
		$params['site_id']		= $site_id;

		$list 	= $this->payroll_model->report($params);
		$total 	= $this->payroll_model->report($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['no'] 				= $i;
			$result['site_name']		= $item->site_name;
			$result['company_code']		= $item->company_code;
			$period 					= "";
			if($item->periode_start){
				$start_date = substr($item->periode_start, 8, 2).' '.get_month(substr($item->periode_start, 5, 2));
				$end_date 	= substr($item->periode_end, 8, 2).' '.get_month(substr($item->periode_end, 5, 2)).' '.substr($item->periode_end, 0, 4);
				$period 	= $start_date.' - '.$end_date;
			}
			$result['period']			= $period;
			$result['employee_count']	= $item->employee_count;
			$result['summary_payroll']	= format_rupiah($item->summary_payroll);
			$result['summary_basic']	= format_rupiah($item->summary_basic);
			$result['action'] 			= 
				'<a class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" href="'. base_url("accurate_report/detail/".$proyek_id."/".$item->site_id."/".$item->periode_end).'">Detail</a>
				<a class="btn-sm btn-success btn-action btn-block"  style="cursor:pointer;" href="'. base_url("accurate_report/export/".$proyek_id."/".$item->site_id."/".$item->periode_end).'">Export</a>
				';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function detail($proyek_id = FALSE, $site_id=FALSE, $periode_end = FALSE){
		$this->load->model(array('site_model', 'payroll_model', 'employee_model'));
		$data['_TITLE_'] 		= 'Payroll Karyawan';
		$data['_PAGE_'] 		= 'accurate_report/detail';
		$data['_MENU_PARENT_'] 	= 'accurate_report';
		$data['_MENU_'] 		= 'accurate_site';
		// $params['status']			= 1;
		// $params['status_approval'] 	= 3;
		$params['site_id'] 		= $site_id;

		$data['proyek_id'] 		= $proyek_id;
		$data['site']			= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.id, A.name, A.address'));
		$data['employee']		= $this->employee_model->gets($params, TRUE);
		$data['resume']			= $this->payroll_model->resume(array('site_id' => $site_id, 'periode_end' => $periode_end));

		$params['columns'] 		= "A.id, B.full_name AS employee_name, D.name AS position, B.bank_account, B.bank_name, B.bank_account_name, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.salary, A.basic_salary, A.overtime_calculation, A.bpjs_ks, (A.bpjs_jht + A.bpjs_jp) AS bpjs_tk, A.tax_calculation ";
		$params['payment']		= 'Selesai';

		$date=date_create($periode_end);
		$params['periode_end']	= date_format($date,"d/m/Y");
		$data['periode_end']	= $periode_end;
		$data['payroll'] 		= $this->payroll_model->gets($params);
		$this->view($data);
	}

	// public function detail_export($proyek_id = FALSE, $site_id=FALSE, $periode_end = FALSE)
	// {
	// 	$this->load->model(array('site_model', 'payroll_model', 'employee_model'));
	// 	$title = 'payroll_report';
	// 	$files = glob(FCPATH."files/".$title."/*");

	// 	foreach($files as $file){
	// 		gc_collect_cycles();
	// 		if(is_file($file)) {
	// 			unlink($file);
	// 		}
	// 	}

	// 	$this->load->library("Excel");
	// 	$this->load->library('PHPExcel/iofactory');
	// 	$excel = new PHPExcel();
	// 	$excel->getProperties()->setCreator($this->session->userdata('name'))
	// 	->setLastModifiedBy($this->session->userdata('name'))
	// 	->setTitle($title)
	// 	->setSubject($title)
	// 	->setDescription($title)
	// 	->setKeywords($title);
		
	// 	$date=date_create($periode_end);
		
	// 	$params['status']		    = 1;
	// 	$params['status_approval'] 	= 3;
	// 	$params['site_id'] 		    = $site_id;
	// 	$params['columns'] 		    = "A.id, B.full_name AS employee_name, D.name AS position, B.bank_account, B.bank_name, B.bank_account_name, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.salary, A.basic_salary, A.overtime_calculation, A.bpjs_ks, (A.bpjs_jht + A.bpjs_jp) AS bpjs_tk, A.tax_calculation ";
	// 	$params['payment']		    = 'Selesai';
	// 	$params['periode_end']	    = date_format($date,"d/m/Y");
	// 	$payroll 		            = $this->payroll_model->gets($params);
		
	// 	$i=1;
	// 	$excel->getActiveSheet()->setCellValue("A".$i, "No");
	// 	$excel->getActiveSheet()->setCellValue("B".$i, "Nama");
	// 	$excel->getActiveSheet()->setCellValue("C".$i, "Departemen");
	// 	$excel->getActiveSheet()->setCellValue("D".$i, "Bank");
	// 	$excel->getActiveSheet()->setCellValue("E".$i, "Atas Nama");
	// 	$excel->getActiveSheet()->setCellValue("F".$i, "Nomor Rekening");
	// 	$excel->getActiveSheet()->setCellValue("G".$i, "THP");
	// 	$i=2;
	// 	$no=1;
	// 	foreach ($payroll as $item) {
	// 		$excel->getActiveSheet()->setCellValueExplicit("A".$i, $no);
	// 		$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->employee_name);
	// 		$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->position);
	// 		$excel->getActiveSheet()->setCellValueExplicit("D".$i, $item->bank_name);
	// 		$excel->getActiveSheet()->setCellValueExplicit("E".$i, $item->employee_name);
	// 		$excel->getActiveSheet()->setCellValueExplicit("F".$i, $item->bank_account);
	// 		$excel->getActiveSheet()->setCellValueExplicit("G".$i, format_rupiah($item->salary));
	// 		$i++;
	// 		$no++;
	// 	}
	// 	$excel->getActiveSheet()->setTitle('Daftar Karyawan');
	// 	$sheet = $excel->getActiveSheet();
	// 	$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
	// 	$cellIterator->setIterateOnlyExistingCells( true );
	// 	foreach( $cellIterator as $cell ) {
	// 		$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
	// 	}
	// 	$excel->createSheet();
	// 	$excel->setActiveSheetIndex(0);
	// 	$objWriter = IOFactory::createWriter($excel, 'Excel2007');
	// 	header('Content-type: application/vnd.ms-excel');
	// 	header('Content-Disposition: attachment;filename="'.$title.'"');
	// 	$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
	// 	$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
	// 	redirect(base_url("files/".$title."/".$filename.".xlsx"));
	// 	exit;

	// }

	public function export($proyek_id = FALSE, $site_id = FALSE, $end_period = FALSE)
	{
		$this->load->model(array('proyek_accurate_model', 'payroll_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$params 			= $this->input->get();
		$title = 'payroll_report';
		$files = glob(FCPATH."files/payroll_report/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excelreader    = new PHPExcel_Reader_Excel2007();
        $formula_path 	= FCPATH.'files/template_accurate.xlsx';
		$excel_base     = $excelreader->load($formula_path);
        $sheet_base     = $excel_base->getActiveSheet(0);
        $cend_base		= $sheet_base->getHighestColumn();
		
		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);		
		$excel->setActiveSheetIndex(0);
		$column = 'A';
		$row = 1;
		while ($column !== $cend_base) {
			$rc = $column.'1';
			$excel->getActiveSheet()->setCellValueExplicit($rc, $sheet_base->getCell($rc)->getValue());
			$excel->getActiveSheet()->duplicateStyle($sheet_base->getStyle($rc), $rc);
	        
	        $rc2 = $column.'2';
	        $start = substr($sheet_base->getCell($rc2)->getValue(), 0, 1);
	        if($start === '='){
				$excel->getActiveSheet()->setCellValue($rc2, $sheet_base->getCell($rc2)->getValue());
			}
	        $column++;
	    }

	    $site 	= $this->proyek_accurate_model->get_customer(array('columns' => 'A.nomor_proyek, A.branch_id, B.accurate_customer_no, B.name', 'id' => $proyek_id));
		if($site){
			$branch = "";
			if($site->branch_id == 50){
				$branch = 'HEAD OFFICE';
			}else if($site->branch_id == 101 ||  $site->branch_id == 1801){
				$branch = 'CENTRAL';
			}else if($site->branch_id == 150 ||  $site->branch_id == 1751 ){
				$branch = 'EAST';
			}else if($site->branch_id == 250){
				$branch = 'MAKASAR';
			}else if($site->branch_id == 52 ||  $site->branch_id == 1750){
				$branch = 'WEST';
			}

			$excel->getActiveSheet()->setCellValueExplicit("B2", $site->name);
			$excel->getActiveSheet()->setCellValueExplicit("AL2", $site->nomor_proyek);
			$excel->getActiveSheet()->setCellValueExplicit("AM2", $site->accurate_customer_no);
			$excel->getActiveSheet()->setCellValueExplicit("AN2", $branch);

			$period = substr($end_period, 8, 2).'/'.substr($end_period, 5, 2).'/'.substr($end_period, 0, 4);
			$excel->getActiveSheet()->setCellValueExplicit("AO2", $period);

			 $filter['site_id']		= $site_id;
			 $filter['payment']		= 'Selesai';
			 $filter['periode_end']	= $end_period;
			 $filter['columns']		= 'COUNT(A.id) AS headcount, SUM(A.basic_salary) AS basic_salary, SUM(A.overtime_calculation) AS overtime_calculation, SUM(A.bpjs_ks_company) AS bpjs_ks_company, SUM(A.bpjs_jht_company) AS bpjs_jht_company, SUM(A.bpjs_ks) AS bpjs_ks, SUM(A.bpjs_jht) AS bpjs_jht, SUM(A.tax_calculation) AS tax_calculation ,SUM(A.salary) AS salary';
			$payroll	= $this->payroll_model->last_payroll($filter);
			if($payroll){
				$excel->getActiveSheet()->setCellValue("A2", $payroll->headcount);
				$excel->getActiveSheet()->setCellValue("C2", $payroll->basic_salary);
				$excel->getActiveSheet()->setCellValue("F2", $payroll->overtime_calculation);
				$excel->getActiveSheet()->setCellValue("S2", $payroll->bpjs_ks_company);
				$excel->getActiveSheet()->setCellValue("T2", $payroll->bpjs_jht_company);
				$excel->getActiveSheet()->setCellValue("AB2", $payroll->bpjs_ks);
				$excel->getActiveSheet()->setCellValue("AC2", $payroll->bpjs_jht);
				$excel->getActiveSheet()->setCellValue("AE2", $payroll->tax_calculation);
				
			}

			$potongan_lain	 	= 0;
			$filter['columns']		= 'SUM(A.value) AS value';
			$filter['category']		= 'potongan';
			$potongan		= $this->payroll_model->gets_detail($filter);
			if($potongan){
				$potongan_lain = $potongan[0]->value;
			}

			$filter['name']			= 'kaporlap';
			$kaporlap				= $this->payroll_model->gets_detail($filter);
			if($kaporlap){
				if($kaporlap[0]->value != ''){
					$excel->getActiveSheet()->setCellValue("AD2", $kaporlap[0]->value);
					$potongan_lain = $potongan_lain - $kaporlap[0]->value;
				}
			}

			$filter['name']			= 'gada';
			$gada				= $this->payroll_model->gets_detail($filter);
			if($gada){
				if($gada[0]->value != ''){
					$excel->getActiveSheet()->setCellValue("AH2", $gada[0]->value);
					$potongan_lain = $potongan_lain - $gada[0]->value;
				}
			}

			$excel->getActiveSheet()->setCellValue("AF2", $gada[0]->value);
			
			// $pendapatan_lain	 	= 0;
			$filter['columns']		= 'SUM(A.value) AS value';
			$filter['category']		= 'pendapatan';
			$filter['name']			= 'kemahalan';
			$kemahalan		= $this->payroll_model->gets_detail($filter);
			if($kemahalan){
				$excel->getActiveSheet()->setCellValue("E2", $kemahalan[0]->value);
			}

			$filter['name']			= 'transport';
			$transport		= $this->payroll_model->gets_detail($filter);
			if($transport){
				$excel->getActiveSheet()->setCellValue("G2", $transport[0]->value);
			}

			$filter['name']			= 'makan';
			$meal		= $this->payroll_model->gets_detail($filter);
			if($meal){
				$excel->getActiveSheet()->setCellValue("H2", $meal[0]->value);
			}

			$filter['name']			= 'kehadiran';
			$kehadiran		= $this->payroll_model->gets_detail($filter);
			if($kehadiran){
				$excel->getActiveSheet()->setCellValue("I2", $kehadiran[0]->value);
			}

			$filter['name']			= 'kelebihan jam';
			$kelebihan_jam		= $this->payroll_model->gets_detail($filter);
			if($kelebihan_jam){
				$excel->getActiveSheet()->setCellValue("J2", $kelebihan_jam[0]->value);
			}

			$filter['name']			= 'OT REG';
			$otreg		= $this->payroll_model->gets_detail($filter);
			if($otreg){
				$excel->getActiveSheet()->setCellValue("K2", $otreg[0]->value);
			}

			$filter['name']			= 'OTN';
			$otn		= $this->payroll_model->gets_detail($filter);
			if($otn){
				$excel->getActiveSheet()->setCellValue("L2", $otn[0]->value);
			}

			$filter['name']			= 'OT REQUEST';
			$otn_request		= $this->payroll_model->gets_detail($filter);
			if($otn_request){
				$excel->getActiveSheet()->setCellValue("M2", $otn_request[0]->value);
			}

			$filter['name']			= 'shift malam';
			$shift_malam		= $this->payroll_model->gets_detail($filter);
			if($shift_malam){
				$excel->getActiveSheet()->setCellValue("N2", $shift_malam[0]->value);
			}

			$filter['name']			= 'luar kota';
			$luar_kota		= $this->payroll_model->gets_detail($filter);
			if($luar_kota){
				$excel->getActiveSheet()->setCellValue("O2", $luar_kota[0]->value);
			}

			$filter['name']			= 'kurang gaji';
			$kurang_gaji		= $this->payroll_model->gets_detail($filter);
			if($kurang_gaji){
				$excel->getActiveSheet()->setCellValue("P2", $kurang_gaji[0]->value);
			}

			$filter['name']			= 'jabatan';
			$jabatan		= $this->payroll_model->gets_detail($filter);
			if($jabatan){
				$excel->getActiveSheet()->setCellValue("Q2", $jabatan[0]->value);
			}

			$filter['name']			= 'Kompleksitas';
			$Kompleksitas		= $this->payroll_model->gets_detail($filter);
			if($Kompleksitas){
				$excel->getActiveSheet()->setCellValue("R2", $Kompleksitas[0]->value);
			}

			$excel->getActiveSheet()->getStyle('C2:AJ2')->getNumberFormat()->setFormatCode('#,###,###');
		}

	    $sheet = $excel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells( true );
		foreach( $cellIterator as $cell ) {
			$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		}

		$excel->getActiveSheet()->setTitle('Pasang Data');
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function detail_export($proyek_id = FALSE, $site_id = FALSE, $end_period = FALSE)
	{
		$this->load->model(array('proyek_accurate_model', 'payroll_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$params 			= $this->input->get();
		$title = 'payroll_report';
		$files = glob(FCPATH."files/payroll_report/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excelreader    = new PHPExcel_Reader_Excel2007();
        $formula_path 	= FCPATH.'files/template_accurate.xlsx';
		$excel_base     = $excelreader->load($formula_path);
        $sheet_base     = $excel_base->getActiveSheet(0);
        $cend_base		= $sheet_base->getHighestColumn();
		
		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);		
		$excel->setActiveSheetIndex(0);
		$column = 'A';
		$row = 1;
		while ($column !== $cend_base) {
			$rc = $column.'1';
			$excel->getActiveSheet()->setCellValueExplicit($rc, $sheet_base->getCell($rc)->getValue());
			$excel->getActiveSheet()->duplicateStyle($sheet_base->getStyle($rc), $rc);
	        $column++;
	    }
	    $excel->getActiveSheet()->setCellValueExplicit("A1", "NIK KTP");
	    $excel->getActiveSheet()->setCellValueExplicit("B1", "NAMA KARYAWAN");
	    
	    $site 	= $this->proyek_accurate_model->get_customer(array('columns' => 'A.nomor_proyek, A.branch_id, B.accurate_customer_no, B.name', 'id' => $proyek_id));
		if($site){
			$branch = "";
			if($site->branch_id == 50){
				$branch = 'HEAD OFFICE';
			}else if($site->branch_id == 101 ||  $site->branch_id == 1801){
				$branch = 'CENTRAL';
			}else if($site->branch_id == 150 ||  $site->branch_id == 1751 ){
				$branch = 'EAST';
			}else if($site->branch_id == 250){
				$branch = 'MAKASAR';
			}else if($site->branch_id == 52 ||  $site->branch_id == 1750){
				$branch = 'WEST';
			}

			$excel->getActiveSheet()->setCellValueExplicit("B2", $site->name);
			$excel->getActiveSheet()->setCellValueExplicit("AL2", $site->nomor_proyek);
			$excel->getActiveSheet()->setCellValueExplicit("AM2", $site->accurate_customer_no);
			$excel->getActiveSheet()->setCellValueExplicit("AN2", $branch);

			$period = substr($end_period, 8, 2).'/'.substr($end_period, 5, 2).'/'.substr($end_period, 0, 4);
			$excel->getActiveSheet()->setCellValueExplicit("AO2", $period);

			$params['site_id'] 		    = $site_id;
			$params['columns'] 		    = "A.id, B.id_card, B.full_name, A.salary, A.basic_salary, A.overtime_calculation, A.bpjs_ks_company, A.bpjs_jht_company, A.bpjs_ks, A.bpjs_jht, A.tax_calculation ";
			$params['payment']		    = 'Selesai';
			$date=date_create($end_period);
			$params['periode_end']		= date_format($date,"d/m/Y");
			
			$list_payroll				= $this->payroll_model->gets($params);			
			$row = 2;
			foreach($list_payroll AS $item){
				$column = 'A';
				while ($column !== $cend_base) {
			        $rc2 = $column.$row;
					$value = $sheet_base->getCell($column.'2')->getValue();
					if(substr($value, 0, 1) === '='){
						$value = str_replace('2', $row, $value);
					}
					$excel->getActiveSheet()->setCellValue($rc2, $value);
			        $column++;
			    }

			    $excel->getActiveSheet()->setCellValueExplicit("A".$row, $item->id_card);
				$excel->getActiveSheet()->setCellValueExplicit("B".$row, $item->full_name);
				$excel->getActiveSheet()->setCellValue("C".$row, $item->basic_salary);
				$excel->getActiveSheet()->setCellValue("F".$row, $item->overtime_calculation);
				$excel->getActiveSheet()->setCellValue("S".$row, $item->bpjs_ks_company);
				$excel->getActiveSheet()->setCellValue("T".$row, $item->bpjs_jht_company);
				$excel->getActiveSheet()->setCellValue("AB".$row, $item->bpjs_ks);
				$excel->getActiveSheet()->setCellValue("AC".$row, $item->bpjs_jht);
				$excel->getActiveSheet()->setCellValue("AE".$row, $item->tax_calculation);

				$potongan_lain	 	= 0;
				$filter['columns']		= 'SUM(A.value) AS value';
				$filter['category']		= 'potongan';
				$filter['payroll_id']	= $item->id;
				$potongan		= $this->payroll_model->gets_detail($filter);
				if($potongan){
					$potongan_lain = $potongan[0]->value;
				}

				$filter['name']			= 'kaporlap';
				$kaporlap				= $this->payroll_model->gets_detail($filter);
				if($kaporlap){
					if($kaporlap[0]->value != ''){
						$excel->getActiveSheet()->setCellValue("AD".$row, $kaporlap[0]->value);
						$potongan_lain = $potongan_lain - $kaporlap[0]->value;
					}
				}

				$filter['name']			= 'gada';
				$gada				= $this->payroll_model->gets_detail($filter);
				if($gada){
					if($gada[0]->value != ''){
						$excel->getActiveSheet()->setCellValue("AH".$row, $gada[0]->value);
						$potongan_lain = $potongan_lain - $gada[0]->value;
					}
				}

				$excel->getActiveSheet()->setCellValue("AF".$row, $gada[0]->value);
				
				// $pendapatan_lain	 	= 0;
				$filter['columns']		= 'SUM(A.value) AS value';
				$filter['category']		= 'pendapatan';
				$filter['name']			= 'kemahalan';
				$kemahalan		= $this->payroll_model->gets_detail($filter);
				if($kemahalan){
					$excel->getActiveSheet()->setCellValue("E".$row, $kemahalan[0]->value);
				}

				$filter['name']			= 'transport';
				$transport		= $this->payroll_model->gets_detail($filter);
				if($transport){
					$excel->getActiveSheet()->setCellValue("G".$row, $transport[0]->value);
				}

				$filter['name']			= 'makan';
				$meal		= $this->payroll_model->gets_detail($filter);
				if($meal){
					$excel->getActiveSheet()->setCellValue("H".$row, $meal[0]->value);
				}

				$filter['name']			= 'kehadiran';
				$kehadiran		= $this->payroll_model->gets_detail($filter);
				if($kehadiran){
					$excel->getActiveSheet()->setCellValue("I".$row, $kehadiran[0]->value);
				}

				$filter['name']			= 'kelebihan jam';
				$kelebihan_jam		= $this->payroll_model->gets_detail($filter);
				if($kelebihan_jam){
					$excel->getActiveSheet()->setCellValue("J".$row, $kelebihan_jam[0]->value);
				}

				$filter['name']			= 'OT REG';
				$otreg		= $this->payroll_model->gets_detail($filter);
				if($otreg){
					$excel->getActiveSheet()->setCellValue("K".$row, $otreg[0]->value);
				}

				$filter['name']			= 'OTN';
				$otn		= $this->payroll_model->gets_detail($filter);
				if($otn){
					$excel->getActiveSheet()->setCellValue("L".$row, $otn[0]->value);
				}

				$filter['name']			= 'OT REQUEST';
				$otn_request		= $this->payroll_model->gets_detail($filter);
				if($otn_request){
					$excel->getActiveSheet()->setCellValue("M".$row, $otn_request[0]->value);
				}

				$filter['name']			= 'shift malam';
				$shift_malam		= $this->payroll_model->gets_detail($filter);
				if($shift_malam){
					$excel->getActiveSheet()->setCellValue("N".$row, $shift_malam[0]->value);
				}

				$filter['name']			= 'luar kota';
				$luar_kota		= $this->payroll_model->gets_detail($filter);
				if($luar_kota){
					$excel->getActiveSheet()->setCellValue("O".$row, $luar_kota[0]->value);
				}

				$filter['name']			= 'kurang gaji';
				$kurang_gaji		= $this->payroll_model->gets_detail($filter);
				if($kurang_gaji){
					$excel->getActiveSheet()->setCellValue("P".$row, $kurang_gaji[0]->value);
				}

				$filter['name']			= 'jabatan';
				$jabatan		= $this->payroll_model->gets_detail($filter);
				if($jabatan){
					$excel->getActiveSheet()->setCellValue("Q".$row, $jabatan[0]->value);
				}

				$filter['name']			= 'Kompleksitas';
				$Kompleksitas		= $this->payroll_model->gets_detail($filter);
				if($Kompleksitas){
					$excel->getActiveSheet()->setCellValue("R".$row, $Kompleksitas[0]->value);
				}

				$excel->getActiveSheet()->getStyle('C'.$row.':AJ'.$row)->getNumberFormat()->setFormatCode('#,###,###');
			    $row++;
			}
		}

	    $sheet = $excel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells( true );
		foreach( $cellIterator as $cell ) {
			$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		}

		$excel->getActiveSheet()->setTitle('Pasang Data');
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}
}
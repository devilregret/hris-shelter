<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluation extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,5,6))){
			redirect(base_url());
		}
	}

	public function activity(){
		$data['_TITLE_'] 		= 'Catatan Pekerjaan';
		$data['_PAGE_'] 		= 'evaluation/activity';
		$data['_MENU_PARENT_'] 	= 'evaluation';
		$data['_MENU_'] 		= 'activity_employee';

		$this->view($data);

	}

	public function activity_ajax(){
		$this->load->model(array('activity_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.employee_id, B.full_name AS employee_name, C.name AS branch_name, D.code AS company_code, E.name AS position_name, DATE_FORMAT(A.date, "%d/%m/%Y") AS date, A.time_start, A.time_finish, A.activity, A.result';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];

		$params['employee_name']= $_POST['columns'][0]['search']['value'];
		$params['position_name']= $_POST['columns'][1]['search']['value'];
		$params['branch_name']	= $_POST['columns'][2]['search']['value'];
		$params['company_code']	= $_POST['columns'][3]['search']['value'];
		$params['date']			= $_POST['columns'][4]['search']['value'];
		$params['time_start']	= $_POST['columns'][5]['search']['value'];
		$params['time_finish']	= $_POST['columns'][6]['search']['value'];
		$params['activity']		= $_POST['columns'][7]['search']['value'];
		$params['result']		= $_POST['columns'][8]['search']['value'];
		
		$list 	= $this->activity_model->gets_employee($params);
		$total 	= $this->activity_model->gets_employee($params, TRUE);
		
		$data 	= array();
		foreach($list as $item)
		{
			$result['employee_name']= $item->employee_name;
			$result['position_name']= $item->position_name;
			$result['branch_name'] 	= $item->branch_name;
			$result['company_code'] = $item->company_code;
			$result['date'] 		= $item->date;
			$result['time_start'] 	= $item->time_start;
			$result['time_finish']	= $item->time_finish;
			$result['activity']		= $item->activity;
			$result['result']		= $item->result;
			array_push($data, $result);
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function jobdesc(){
		$data['_TITLE_'] 		= 'Deskripsi Pekerjaan';
		$data['_PAGE_'] 		= 'evaluation/jobdesc';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'jobdesc';

		$this->view($data);

	}

	public function jobdesc_ajax(){
		$this->load->model(array('job_description_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.position_id, B.name AS position_name, LEFT (C.description, 100)AS description';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['groupby']		= 'A.position_id';
		$params['site_id']		= 2;

		$params['position_name']= $_POST['columns'][0]['search']['value'];
		$params['description']	= $_POST['columns'][1]['search']['value'];

		$list 	= $this->job_description_model->gets($params);
		$total 	= $this->job_description_model->gets($params, TRUE);
		
		$data 	= array();
		foreach($list as $item)
		{
			$result['position_name']	= $item->position_name;
			$result['description']		= $item->description;
			$result['action'] 		=
				'<a class="btn-sm btn-success btn-block" href="'.base_url("evaluation/form_jobdesc/".$item->position_id).'">Ubah</a>';
			array_push($data, $result);
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function form_jobdesc($position_id = FALSE)
	{
		$this->load->model(array('job_description_model', 'position_model'));

		$data['position_id'] 	= $position_id;
		$data['description']	= '';

		if($this->input->post()){
			$data['position_id'] 	= $this->input->post('position_id');
			$data['description'] 	= $this->input->post('description');
			
			$this->form_validation->set_rules('position_id', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{				
				$save_id	 	= $this->job_description_model->save($data);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('evaluation/jobdesc'));
		}

		if ($position_id)
		{
			$position = $this->position_model->get(array('id' => $position_id, 'columns' => 'A.name'));
			if (!$position)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('evaluation/jobdesc'));
				exit;
			}

			$jobdesc = (array) $this->job_description_model->get(array('position_id' => $position_id, 'columns' => 'A.position_id, A.description'));
			if(!empty($jobdesc)){
				$data = $jobdesc;
			}
			$data['position_name']	= $position->name;
		}
		$data['_TITLE_'] 		= 'Deskripsi Pekerjaan';
		$data['_PAGE_'] 		= 'evaluation/form_jobdesc';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'jobdesc';
		return $this->view($data);
	}

	public function activity_import(){
		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('employee_model', 'activity_model'));
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {
				$data['employee_id'] 		= '';
				$data['date']				= '';
				$data['time_start']			= '08:00';
				$data['time_finish']		= '17:00';
				$data['activity']			= '';
				$data['result'] 			= '';
				$data['is_active'] 			= 1;
				
				$id_card 				= replace_null($sheet->getCell('D'.$row)->getValue());
				if($id_card == ''){
					continue;
				}

				$employee = $this->employee_model->get(array('id_card' => $id_card, 'columns' => 'A.id, A.site_id'));
				if($employee){
					$data['employee_id']	= $employee->id;
				}else{
					continue;
				}
				$excel_date = $sheet->getCell('F'.$row)->getValue();
				$unix_date = ($excel_date - 25569) * 86400;
				$excel_date = 25569 + ($unix_date / 86400);
				$unix_date = ($excel_date - 25569) * 86400;
				$data['date'] 		= gmdate("Y-m-d", $unix_date);
				$data['activity'] 	= replace_null($sheet->getCell('G'.$row)->getValue());
				$data['result'] 	= replace_null($sheet->getCell('J'.$row)->getValue());

				$excel_date = $sheet->getCell('H'.$row)->getValue();
				$unix_date = ($excel_date - 25569) * 86400;
				$excel_date = 25569 + ($unix_date / 86400);
				$unix_date = ($excel_date - 25569) * 86400;
				$data['time_start'] 		= gmdate("H:i", $unix_date);

				$excel_date = $sheet->getCell('I'.$row)->getValue();
				$unix_date = ($excel_date - 25569) * 86400;
				$excel_date = 25569 + ($unix_date / 86400);
				$unix_date = ($excel_date - 25569) * 86400;
				$data['time_finish'] 		= gmdate("H:i", $unix_date);
				// print_prev($data);
				$activity 	= $this->activity_model->get($data);
				if($activity){
					$this->activity_model->update($data);
				}else{
					$this->activity_model->save($data);
				}
			}
			// die();
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			redirect(base_url('evaluation/activity'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
			redirect(base_url('evaluation/activity'));
		}
	}

	public function target(){
		$data['_TITLE_'] 		= 'Sasaran Mutu';
		$data['_PAGE_'] 		= 'evaluation/target';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'target';

		$this->view($data);

	}

	public function target_ajax(){
		$this->load->model(array('evaluation_target_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.name AS name';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['name']			= $_POST['columns'][1]['search']['value'];
		
		$list 	= $this->evaluation_target_model->gets($params);
		$total 	= $this->evaluation_target_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['name'] 		= $item->name;
			$result['action'] 		=
				'<a class="btn-sm btn-success btn-block" href="'.base_url("evaluation/target_form/".$item->id).'">Ubah</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function target_form($target_id = FALSE)
	{
		$this->load->model(array('evaluation_target_model'));

		$data['id'] 		= '';
		$data['name']		= '';
		$data['description']= '';
		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['name'] 		= $this->input->post('name');
			$data['description']= $this->input->post('description');
			
			$this->form_validation->set_rules('name', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$save_id	 	= $this->evaluation_target_model->save($data);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('evaluation/target'));
		}

		if ($target_id)
		{
			$data = (array) $this->evaluation_target_model->get(array('id' => $target_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('evaluation/target'));
			}
		}

		$data['_TITLE_'] 		= 'Form Sasaran Mutu';
		$data['_PAGE_'] 		= 'evaluation/target_form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'target';
		return $this->view($data);
	}
}
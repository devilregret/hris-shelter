<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('benefit'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,6,13,14))){
			redirect(base_url());
		}
	}

	public function labor()
	{

		$this->load->model(array('employee_model', 'upload_report_model'));
		$report  = $this->upload_report_model->get(array('id' => 1, 'columns' => 'A.id, A.failed, A.success'));
		$report_failed 	= $report->failed;
		$report_success = $report->success;

		if($this->input->post()){
			if(!$_FILES['file']['name'] == ""){
				$array = explode(".", $_FILES['file']['name']);
				$name = $array[0];
				$ext = $array[1];
				if($ext == 'zip'){
					
					$success_count = 0;
					$fail_list = "";
					$url_card = 'files/card/labor/';
					$path = FCPATH.'files/card/labor_temp/';
					$location = $path . $_FILES['file']['name'];
					
					if(move_uploaded_file($_FILES['file']['tmp_name'], $location)){
						$zip = new ZipArchive;
						if($zip->open($location)){
							$zip->extractTo($path);
							$zip->close();
						}
						$files = scandir($path);
						foreach($files as $file){
							$array_file = explode(".", $file);
							$file_name = $array_file[0];
							$file_ext = $array_file[1];
							$allowed_ext = array('jpg', 'png', 'jpeg', 'pdf', 'PDF');
							if(in_array($file_ext, $allowed_ext)){
								$employee = $this->employee_model->get(array('benefit_labor' => $file_name, 'columns' => 'A.id'));
								if($employee){
									$report_success = $report_success . $file_name . ',';
									$success_count ++;
									$this->employee_model->save(array('id' => $employee->id, 'benefit_labor_card' => $url_card.$file));
									copy($path.'/'.$file, FCPATH.$url_card.$file); 
								}else{
									$report_failed = $report_failed . $file_name . ','; 
									if($fail_list == ''){
										$fail_list = "<br> No KPJ berikut : ".$file_name;
									}else{
										$fail_list .= ", ".$file_name;
									}
								}
							}
							if($file == '.' || $file == '..'){
								continue;
							}
							unlink($path.'/'.$file);
						}
					}
					$this->upload_report_model->save(array('id' => 1, 'failed' => $report_failed, 'success' => $report_success));

					if($fail_list == ''){
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> '.$success_count.' kartu berhasil diupload.','success'));
					}else{
						$this->session->set_flashdata('message', message_box('<strong>Peringatan!</strong> <br>'.$success_count.' kartu berhasil diupload. '.$fail_list.' belum terdaftar di HRIS','warning'));
					}
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan pilih file dengan format <strong>.zip</strong>.','danger'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
			}
			redirect(base_url('upload/labor'));
		}
		
		$data['failed']			= $report_failed;
		$data['report_id']		= 1;
		$data['_TITLE_'] 		= 'Upload Kartu Ketenagakerjaan';
		$data['_PAGE_']	 		= 'upload/labor';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'labor_setting';
		$this->view($data);
	}

	public function health()
	{
		$this->load->model(array('employee_model', 'upload_report_model'));
		$report  = $this->upload_report_model->get(array('id' => 2, 'columns' => 'A.id, A.failed, A.success'));
		$report_failed 	= $report->failed;
		$report_success = $report->success;

		if($this->input->post()){
			if(!$_FILES['file']['name'] == ""){
				$array = explode(".", $_FILES['file']['name']);
				$name = $array[0];
				$ext = $array[1];
				if($ext == 'zip'){
					$this->load->model(array('employee_model'));
					$success_count = 0;
					$fail_list = "";
					$url_card = 'files/card/health/';
					$path = FCPATH.'files/card/health_temp/';
					$location = $path . $_FILES['file']['name'];
					
					if(move_uploaded_file($_FILES['file']['tmp_name'], $location)){
						$zip = new ZipArchive;
						if($zip->open($location)){
							$zip->extractTo($path);
							$zip->close();
						}
						$files = scandir($path);
						foreach($files as $file){
							$array_file = explode(".", $file);
							$file_name = $array_file[0];
							$file_ext = $array_file[1];
							$allowed_ext = array('jpg', 'png', 'jpeg', 'pdf', 'PDF');
							if(in_array($file_ext, $allowed_ext)){
								$employee = $this->employee_model->get(array('benefit_health' => $file_name, 'columns' => 'A.id'));
								if($employee){
									$report_success = $report_success . $file_name . ',';
									$success_count ++;
									$this->employee_model->save(array('id' => $employee->id, 'benefit_health_card' => $url_card.$file));
									copy($path.'/'.$file, FCPATH.$url_card.$file); 
								}else{
									$report_failed = $report_failed . $file_name . ',';
									if($fail_list == ''){
										$fail_list = "<br> NO KIS berikut : ".$file_name;
									}else{
										$fail_list .= ", ".$file_name;
									}
								}
							}
							if($file == '.' || $file == '..'){
								continue;
							}
							unlink($path.'/'.$file);
						}
					}
					$this->upload_report_model->save(array('id' => 2, 'failed' => $report_failed, 'success' => $report_success));
					if($fail_list == ''){
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> '.$success_count.' kartu berhasil diupload.','success'));
					}else{
						$this->session->set_flashdata('message', message_box('<strong>Peringatan!</strong> <br>'.$success_count.' kartu berhasil diupload. '.$fail_list.' belum terdaftar sebagai karyawan','warning'));
					}
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan pilih file dengan format <strong>.zip</strong>.','danger'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
			}
			redirect(base_url('upload/health'));
		}

		$data['failed']			= $report_failed;
		$data['report_id']		= 2;
		$data['_TITLE_'] 		= 'Upload Kartu Asuransi';
		$data['_PAGE_']	 		= 'upload/health';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'health_setting';
		$this->view($data);
	}

	public function delete($report_id = FALSE)
	{
		$this->load->model('upload_report_model');
		if ($report_id)
		{
			$result = $this->upload_report_model->save(array('id' => $report_id, 'failed' => '', 'success' => ''));
			if ($result) {
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal dihapus.','danger'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
		}
		if($report_id == 1){
			redirect(base_url('upload/labor'));
		}else{
			redirect(base_url('upload/health'));
		}
	}

	public function export($report_id = FALSE)
	{
		$this->load->model(array('upload_report_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = 'gagal_upload';
		$files = glob(FCPATH."files/".$title."/*");


		$report  = $this->upload_report_model->get(array('id' => $report_id, 'columns' => 'A.id, A.failed'));
		$report_failed 	= $report->failed;

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		if($report_id == 1){
			$excel->getActiveSheet()->setCellValue("A".$i, "Nomor KPJ");
		}else{
			$excel->getActiveSheet()->setCellValue("A".$i, "Nomor KIS");
		}
		$i=2;
		$id_card = explode(",", $report_failed);
		foreach ($id_card as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item);
			$i++;
		}
		$excel->getActiveSheet()->setTitle('Daftar NIK');
		
		$excel->createSheet();
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}
}
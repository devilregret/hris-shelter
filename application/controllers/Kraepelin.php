<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kraepelin extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role   = array_merge($role, $this->config->item('employee'));
		$role   = array_merge($role, $this->config->item('recruitment'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function form() {
		$this->load->model(array('kraepelin_model','psikotes_model','vacancy_model'));
	
		$employeeId = $this->session->userdata('employee_id');
		$data['data_employee'] 			= $this->psikotes_model->getEmployeeInfo($employeeId);
		$data['vacancy_id'] 			= $_GET['vacancy'];
		$data['data_vacancy']			= $this->vacancy_model->getDataForTes($_GET['vacancy']);

		$data['_TITLE_'] 		= 'Kraepelin';
		$data['_PAGE_'] 		= 'kraepelin/form';
		$data['_MENU_PARENT_'] 	= 'tes_online';
		$data['_MENU_'] 		= 'kraepelin';
		return $this->view($data);
	}

	public function saveform(){
		$this->load->model(array('kraepelin_model','vacancy_model','history_tes_model'));
		if($this->input->post()){
			$dataVacancy 						= $this->vacancy_model->getDataForTes($this->input->post('vacancy_id'));

			// Data Master
			$data['m_employee_id'] 	= $this->session->userdata('employee_id');
			$data['company_id'] 	= $dataVacancy[0]->company_id;
			$data['branch_id'] 		= $dataVacancy[0]->branch_id;
			$data['site_id'] 		= $dataVacancy[0]->site_id;
			$data['province_id'] 	= $dataVacancy[0]->province_id;
			$data['position_id'] 	= $dataVacancy[0]->position_id;
			$data['nama'] 			= $this->input->post('full_name');
			$data['usia']			= $this->input->post('umur');
			$data['jk']				= $this->input->post('jk');
			$data['tgl_tes']		= $this->input->post('tgl_tes');
			$data['tes_dimulai']	= $this->input->post('tes_mulai');
			$data['jml_baris']		= $this->input->post('jumlah_baris_kraepelin');
			$data['jml_kolom']		= $this->input->post('jumlah_kolom_kraepelin');
			$data['m_vacancy_id']	= $this->input->post('vacancy_id');
			$data['tes_selesai']	= date('Y-m-d H:i:s');
			$data['id']				= '';
			$save_id				= $this->kraepelin_model->save($data);

			//save detail
			$jawabanBenar 	= 0;
			$jawabanSalah 	= 0;
			$tidakDiisi 	= 0;
			$jumlahSemua	= 0;
			$titikTertinggi = 0;
			$titikTerendah	= 999;
			$titik = 0;
			$arrtabelpersiapan1 = [];
			$arrtabelpersiapan2 = [];

			for ($k=0; $k < $data['jml_kolom']; $k++) {
				$kolomBenar = 0;


				//hitung Titik
				if($titik>0&&$titik<$titikTerendah){
					$titikTerendah	= $titik;
				}else if($titik>$titikTertinggi){
					$titikTertinggi	= $titik;
				}
				
				$titik = 0;
				for ($b=0; $b < $data['jml_baris'] ; $b++) {
					$soal = 'soal_'.$k;
					$nextSoal = 'next_soal_'.$k;
					$jawaban = 'kolom_'.$k;

					$vsoal = (int)$this->input->post($soal)[$b];
					$vnextsoal =(int)$this->input->post($nextSoal)[$b];
					$vjawaban = $this->input->post($jawaban)[$b];
					$isBenar = 0;

					if($vjawaban == ""){
						$tidakDiisi++;
					}else{
						$kunci = fmod(($vsoal+$vnextsoal),10);
						if($kunci==$vjawaban){
							$isBenar =1;
						}else{
							$jawabanSalah++;
						}
						$jawabanBenar++;
						$kolomBenar++;
						$titik++;
					}

					$datad['id']				='';
					$datad['t_kraepelin_id']	= $save_id;
					$datad['baris'] 			= $b;
					$datad['kolom'] 			= $k;
					$datad['angka_1'] 			= $vsoal;
					$datad['angka_2'] 			= $vnextsoal;
					$datad['jawaban'] 			= $vjawaban;
					$datad['is_benar'] 			= $isBenar;
					$save_d_id					= $this->kraepelin_model->saveD($datad);

					$jumlahSemua++;
				}

				// tabel persiapan 1
				$exist = false;
				foreach ($arrtabelpersiapan1 as $key => $value) {
					if($kolomBenar == $value->y){
						$exist = true;
						$value->f = $value->f+1;
					}
				}

				if(!$exist){
					$obj = (object) array(
						'y' => $kolomBenar,
						'f'	=> 1,
						'fy_mean' => 0,
						'is_summary' =>0
					);
					array_push($arrtabelpersiapan1,$obj);
				}

				// Tabel Persiapan 2 = Hanker

				$obj = (object) array(
					'x' 			=> $k+1,
					'x_2' 			=> ($k+1)*2,
					'y'				=> $kolomBenar,
					'xy'			=> ($k+1)*$kolomBenar,
					'is_summary' 	=> 0
				);
				array_push($arrtabelpersiapan2,$obj);
			}

			//hitung f.y
			$sumf = 0;
			$sumy = 0;
			$sumfy = 0;
			$meanfy = 0;
			foreach ($arrtabelpersiapan1 as $key => $value) {
				$value->fy = $value->f*$value->y;

				$sumy = $sumy+$value->y;
				$sumf = $sumf+$value->f;
				$sumfy = $sumfy+$value->fy;
			}
			$meanfy = $sumfy/$sumf;

			$obj = (object) array(
				'y' => $sumf,
				'f'	=> $sumf,
				'fy' => $sumfy,
				'fy_mean' => $meanfy,
				'is_summary' =>1
			);
			array_push($arrtabelpersiapan1,$obj);

			//hitung d dan fd
			$sumd = 0;
			$sumfd = 0;
			foreach ($arrtabelpersiapan1 as $key => $value) {
				if($value->is_summary==0){
					$value->d = abs($value->y-$meanfy);
					$value->fd = $value->d*$value->f;
					$sumd = $sumd+$value->d;
					$sumfd = $sumfd+$value->fd;
				}else{
					$value->d = $sumd;
					$value->fd = $sumfd;
				}
			}

			// hitung tabel persiapan 2
			$sumx = 0;
			$sumx_2 = 0;
			$sumy = 0;
			$sumxy = 0;

			foreach ($arrtabelpersiapan2 as $key => $value) {
				$sumx = $sumx+$value->x;
				$sumx_2 = $sumx_2+$value->x_2;
				$sumy = $sumy+$value->y;
				$sumxy = $sumxy+$value->xy;
			}
			$obj = (object) array(
				'x' 			=> $sumx,
				'x_2' 			=> $sumx_2,
				'y'				=> $sumy,
				'xy'			=> $sumxy,
				'is_summary' 	=> 1
			);
			array_push($arrtabelpersiapan2,$obj);

			//Perhitungan
			$i = $sumxy*$sumf;
			$ii = $sumx;
			$iii = $sumy;
			$iv = $sumf*$sumx_2-($sumx*$sumx);
			$v = $sumy/$sumf;
			$vi = $sumxy/$sumf;
			$b = ($i-($ii*$iii))/$iv;
			$a = $v-($b*$vi);
			$x50y50 = $a+($b*50);
			$x0y0 = $a+($b*0);

			//SKORING FINAL
			$kecepatanKerja = $sumfy/$sumf;
			$ketelitianKerja = $jawabanSalah;
			$keajeganKerjaRange = $titikTertinggi-$titikTerendah;
			$keajeganKerjaAverageD = $sumfd/$sumf;
			$ketahananKerja = $x50y50-$x0y0;
			
			//SAVE TABEL PERSIAPAN
			foreach ($arrtabelpersiapan1 as $key => $value) {
				$persiapan1['t_kraepelin_id'] 	= $save_id;
				$persiapan1['is_summary'] 		= $value->is_summary;
				$persiapan1['y'] 				= $value->y;
				$persiapan1['f'] 				= $value->f;
				$persiapan1['fy'] 				= $value->fy;
				$persiapan1['d'] 				= $value->d;
				$persiapan1['fd'] 				= $value->fd;
				$persiapan1['fy_mean'] 			= $value->fy_mean;
				$save_persiapan1_id				= $this->kraepelin_model->savePersiapan1($persiapan1);
			}
			foreach ($arrtabelpersiapan2 as $key => $value) {
				$persiapan2['t_kraepelin_id'] 	= $save_id;
				$persiapan2['is_summary'] 		= $value->is_summary;
				$persiapan2['x'] 				= $value->x;
				$persiapan2['x_2'] 				= $value->x_2;
				$persiapan2['y'] 				= $value->y;
				$persiapan2['xy'] 				= $value->xy;
				$save_persiapan2_id				= $this->kraepelin_model->savePersiapan2($persiapan2);
			}

			//update main
			$dataUpdate['titik_tertinggi']		= $titikTertinggi;
			$dataUpdate['titik_terendah']		= $titikTerendah;
			$dataUpdate['selisih_titik']		= $titikTertinggi-$titikTerendah;

			$dataUpdate['jawaban_benar']		= $jawabanBenar;
			$dataUpdate['jawaban_salah']		= $jawabanSalah;
			$dataUpdate['tidak_diisi']			= $tidakDiisi;

			$dataUpdate['kecepatan_kerja']		= $kecepatanKerja;
			$dataUpdate['ketelitian_kerja']		= $ketelitianKerja;
			$dataUpdate['keajegan_kerja_range']		= $keajeganKerjaRange;
			$dataUpdate['keajegan_kerja_average_d']	= $keajeganKerjaAverageD;
			$dataUpdate['ketahanan_kerja']		= $ketahananKerja;
			$dataUpdate['id']					= $save_id;

			$updateId	= $this->kraepelin_model->updateHasil($dataUpdate);

			//save history
			$history['tes']					='kraepelin';
			$history['doc_id']				=$save_id;
			$history['company_id']			=$data['company_id'] ;
			$history['branch_id']			=$data['branch_id'] ;
			$history['site_id']				=$data['site_id'] ;
			$history['m_employee_id']		=$data['m_employee_id'] ;
			$history['province_id']			=$data['province_id'] ;
			$history['position_id']			=$data['position_id'] ;
			$history['m_vacancy_id']		=$data['m_vacancy_id'] ;
			$history['nama']				=$data['nama'] ;
			$history['usia']				=$data['usia'] ;
			$history['jk']					=$data['jk'] ;
			$history['tgl_tes']				=$data['tgl_tes'] ;
			$history['tes_dimulai']			=$data['tes_dimulai'] ;
			$history['tes_selesai']			=$data['tes_selesai'] ;
			$history['jawaban_benar']		=$dataUpdate['jawaban_benar'] ;
			$history['jawaban_salah']		=$dataUpdate['jawaban_salah'] ;
			$history['tidak_diisi']			=$dataUpdate['tidak_diisi'] ;
			$history_id						= $this->history_tes_model->save($history);

			if (!$save_id) {
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));		
				redirect(base_url('appeal/applied'));
				die();
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));		
			redirect(base_url('appeal/applied'));

		}
	}

	public function result ($kraepelin_id){
		$this->load->model(array('psikotes_model','kraepelin_model'));
		$data['data_kraepelin']		= $this->kraepelin_model->get_data_kraepelin($kraepelin_id);
		
		if (empty($data['data_kraepelin'])){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data Kraepelin tidak ditemukan.','danger'));
			redirect(base_url('vacancy/applicant'));
		}

		$data['data_kraepelin_d']		= $this->kraepelin_model->get_data_kraepelin_d($kraepelin_id);	
		$data['data_employee'] 			= $this->psikotes_model->getEmployeeInfo($data['data_kraepelin'][0]->m_employee_id);
		$data['is_hanya_hasil']			= $this->input->get('is_hanya_hasil');

		//update petugas
		
		$data['_TITLE_'] 		= 'Hasil Kraepelin '.$data['data_kraepelin'][0]->nama;
		$data['_PAGE_'] 		= 'kraepelin/result';
		$data['_MENU_PARENT_'] 	= 'tes_online';
		$data['_MENU_'] 		= 'kraepelin';
		return $this->view($data);
	}

	public function updatelolos($kraepelin_id){
		$this->load->model(array('kraepelin_model'));
		$dataUpdate['id']	= $kraepelin_id;
		$dataUpdate['kategori'] = $this->input->post('kategori');
		$dataUpdate['petugas']	= $this->session->userdata('name');

		$result	= $this->kraepelin_model->updateHasil($dataUpdate);

		if ($result) {
			$response['code'] 		= 200;
			$response['description']= "data berhasil diupdate.";
			$response['petugas']= $this->session->userdata();
		}

		echo json_encode($response);
	}

}
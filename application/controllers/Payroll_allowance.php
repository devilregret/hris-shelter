<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Payroll_allowance extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,6,11,12))){
			redirect(base_url());
		}
		require APPPATH.'third_party/PHPMailer/Exception.php';
		require APPPATH.'third_party/PHPMailer/PHPMailer.php';
		require APPPATH.'third_party/PHPMailer/SMTP.php';
	}

	public function site(){
		if($this->input->post('site_id')){
			$this->session->set_userdata('site',$this->input->post('site_id'));
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Site sudah dipilih.','success'));
		redirect(base_url('payroll_allowance/list'));
	}

	public function process(){
		$this->load->model(array('allowance_model'));
		foreach ($this->input->post('id') as $payroll_id)
		{
			$this->payroll_model->save(array('payment' => 'Selesai', 'id' => $payroll_id));
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diproses.','success'));
		redirect(base_url('payroll_allowance/list'));
	}

	public function list(){
		
		$this->load->model(array('allowance_config_model', 'site_model'));
		
		$data['_TITLE_'] 		= 'THR Karyawan';
		$data['_PAGE_'] 		= 'payroll_allowance/list';
		$data['_MENU_PARENT_'] 	= 'payroll_employee';
		$data['_MENU_'] 		= 'allowance';
		$data['site_id']		= $this->session->userdata('site');
		$data['site'] 			= $this->allowance_config_model->get(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address, B.site_id, B.row_title, B.row_start, B.row_end, B.column_employee, B.column_income, B.column_outcome, B.column_net, B.column_attendance'));

		$branch_id				= '';
		if($this->session->userdata('branch') > 1){
			$branch_id 	= $this->session->userdata('branch');
		}
		$data['list_site']		= $this->site_model->gets(array('columns' => 'A.id, A.name, C.name AS company_name, C.code AS company_code, F.name AS branch_name', 'branch_id' => $branch_id));
		$this->view($data);
	}

	public function list_ajax(){

		$this->load->model(array('payroll_allowance_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, B.full_name AS employee_name, B.email AS employee_email, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.salary, A.payment, A.email";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		

		$params['employee_name']	= $_POST['columns'][2]['search']['value'];
		$params['employee_email']	= $_POST['columns'][3]['search']['value'];
		$params['periode_start']	= $_POST['columns'][4]['search']['value'];
		$params['periode_end']		= $_POST['columns'][6]['search']['value'];
		$params['salary']			= $_POST['columns'][7]['search']['value'];
		$params['payment']			= $_POST['columns'][8]['search']['value'];
		$params['email']			= $_POST['columns'][9]['search']['value'];
		
		$params['site_id']			= $this->session->userdata('site');

		$list 	= $this->payroll_allowance_model->gets($params);
		$total 	= $this->payroll_allowance_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 				= $i;
			$result['employee_name']	= $item->employee_name;
			$result['employee_email']	= $item->employee_email;
			$result['periode_start']	= $item->periode_start;
			$result['periode_end']		= $item->periode_end;
			$result['salary']			= $item->salary;
			$result['payment']			= '<p class="text-warning">'.$item->payment.'</p>';
			if($item->payment ==  'Selesai'){
				$result['payment']			= '<p class="text-success">'.$item->payment.'</p>';
			}
			$result['email']		= '<p class="text-warning">'.$item->email.'</p>';
			if($item->email ==  'Terkirim'){
				$result['email']			= '<p class="text-success">'.$item->email.'</p>';
			}else if($item->email == 'Gagal'){
				$result['email']			= '<p class="text-danger">'.$item->email.'</p>';
			}

			$result['action'] 			=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("payroll_allowance/preview/".$item->id).'">Lihat</a>
				<a href="'.base_url('payroll_allowance/pdf/' . $item->id).'" style="cursor:pointer;" class="btn-sm btn-warning btn-block" target="_blank">PDF</a>';
			if($item->payment == 'Menunggu'){
				$result['action'] .= 
				'&nbsp;<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("payroll_allowance/delete/".$item->id).'">Hapus</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function preview_payroll()
	{
		$this->load->model(array('payroll_allowance_model', 'employee_model', 'position_model'));
		$params = $this->input->get();
		$ids = explode(",", $params['id']);
		
		$payroll = array();
		foreach ($ids as $payroll_id) {
			$detail_payroll = array();
			$detail_payroll['income']			= $this->payroll_allowance_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'pendapatan', 'columns' => 'A.name, A.value'));
			$detail_payroll['outcome']		= $this->payroll_allowance_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'potongan', 'columns' => 'A.name, A.value'));
			$detail_payroll['payroll']		= $this->payroll_model->get(array('id' => $payroll_id, 'columns' => 'A.id, A.employee_id , A.periode_start, A.periode_end, A.salary'));
			$detail_payroll['employee']		= $this->employee_model->get(array('id' => $detail_payroll['payroll']->employee_id,'columns' => 'A.full_name, A.bank_account, A.employee_number, A.position_id, B.name AS site_name'));
			$detail_payroll['position']		= $this->position_model->get(array('id' => $detail_payroll['employee']->position_id,'columns' => 'A.name, B.code AS company_code'));
			$payroll[$payroll_id] = $detail_payroll;
		}
		$data['payroll']		= $payroll;
		$data['_TITLE_'] 		= 'Preview THR';
		$data['_PAGE_']	 		= 'payroll_allowance/preview_payroll';
		$data['_MENU_PARENT_'] 	= 'payroll_employee';
		$data['_MENU_'] 		= 'allowance';
		$this->load->view('payroll_allowance/preview_payroll', $data);
	}

	public function mail()
	{
		$this->load->model(array('payroll_allowance_model', 'employee_model', 'position_model', 'email_model'));
		$params = $this->input->get();
		$ids = explode(",", $params['id']);
		
		$email_template 	= $this->email_model->get(array('id' => 4));
		$email['email'] 	= $email_template->email_sender;
		$email['sender'] 	= $email_template->name_sender;
		$email['carbon_copy']= $email_template->carbon_copy;
		$email['subject'] 	= $email_template->subject;
		$email['subject']	= str_replace("{PASSWORD}", $email_template->password, $email['subject']);
		$email['message'] 	= $email_template->content;
		$email['message'] 	= str_replace("{PASSWORD}", $email_template->password, $email['message']);

		foreach ($ids as $payroll_id) {
			$income			= $this->payroll_allowance_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'pendapatan', 'columns' => 'A.name, A.value'));
			$outcome		= $this->payroll_allowance_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'potongan', 'columns' => 'A.name, A.value'));
			$payroll		= $this->payroll_allowance_model->get(array('id' => $payroll_id, 'columns' => 'A.id, A.employee_id , A.periode_start, A.periode_end, A.salary'));
			$employee		= $this->employee_model->get(array('id' => $payroll->employee_id,'columns' => 'A.email, A.id_card, A.full_name, A.bank_account, A.employee_number, A.position_id, B.name AS site_name'));
			$position		= $this->position_model->get(array('id' => $employee->position_id,'columns' => 'A.name, B.name AS company_name, B.code AS company_code'));
			if($employee->email == ""){
				$this->payroll_allowance_model->save(array('email' => 'Gagal', 'id' => $payroll_id));
				continue;
			}

			$start_date = substr($payroll->periode_start, 8, 2).' '.get_month(substr($payroll->periode_start, 5, 2));
			$end_date 	= substr($payroll->periode_end, 8, 2).' '.get_month(substr($payroll->periode_end, 5, 2)).' '.substr($payroll->periode_end, 0, 4);

			$html  = '<table width="100%">';
			$html .= '<tr><td width="25%">PERIODE BULAN</td><td> : '.$start_date.' - '.$end_date.'</td></tr>
					<tr><td>NO REKENING</td><td> : '.$employee->bank_account.'</td></tr>
					<tr><td>NAMA</td><td> : '.$employee->full_name.'</td></tr>
					<tr><td>NOMOR KARYAWAN</td><td> : '.$employee->employee_number.'</td></tr>
					<tr><td>JABATAN</td><td> : '.$position->name.'</td></tr>
					<tr><td>SITE</td><td> : '.$employee->site_name.'</td></tr>';
			$html .= '</table><table width="100%">';
			$html .= '<tr><td width="60%">&nbsp;</td><td><strong>Gaji Bersih diterima : '.format_rupiah($payroll->salary).'</strong></td></tr>';
			$html .= '</table><br><br><table width="100%">';
			$html .= '<tr><td width="50%"><table>';
			$html .= '<tr><td><strong>Pendapatan</strong></td><td>&nbsp;</td></tr>';
			foreach( $income AS $item):
				$html .= '<tr><td >'.$item->name.'</td><td>:&nbsp;&nbsp;'.format_rupiah($item->value).'</td></tr>';
			endforeach;
			$html .= '</table></td>';
			$html .= '<td width="50%"><table>';
			$html .= '<tr><td><strong>Potongan</strong></td><td>&nbsp;</td></tr>';
			foreach($outcome AS $item):
				$html .= '<tr><td >'.$item->name.'</td><td>:&nbsp;&nbsp;'.format_rupiah($item->value).'</td></tr>';
			endforeach;
			$html .= '</table></td></tr>';
			$html .= '</table>';
			// $html .='</table></td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr></table><div syle="width=100%"></hr></div><br pagebreak="true"/>';
			$data['content'] 	= $html; 
			$data['title'] 		= $employee->id_card;
			$data['basepath'] 	= FCPATH.'files/payroll_karyawan/';
			$data['image']		= strtolower($position->company_code).'.jpg';
			$data['company']	= $position->company_name; 
			$data['password']	= $email_template->password; 
			$this->generate_PDF($data);

			$email['attachment']= $data['basepath'].$data['title'].".pdf";
			$email['receiver'] 	= $employee->email;
			$email['subject']	= str_replace("{NAMA_LENGKAP}", $employee->full_name, $email['subject']);
			$email['message'] 	= str_replace("{NAMA_LENGKAP}", $employee->full_name, $email['message']);
			$email['subject']	= str_replace("{PERIODE}", $start_date.' - '.$end_date, $email['subject']);
			$email['message'] 	= str_replace("{PERIODE}", $start_date.' - '.$end_date, $email['message']);
			if($this->send_mail($email)){
				$this->payroll_allowance_model->save(array('email' => 'Terkirim', 'id' => $payroll_id));
			}else{
				$this->payroll_allowance_model->save(array('email' => 'Gagal', 'id' => $payroll_id));
			}
		}

		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Email berhasil dikirim.','success'));
		redirect(base_url('payroll_allowance/list'));
	}

	public function preview($payroll_id=FALSE)
	{
		$this->load->model(array('payroll_allowance_model', 'employee_model', 'position_model'));
		$data['income']			= $this->payroll_allowance_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'pendapatan', 'columns' => 'A.name, A.value'));
		$data['outcome']		= $this->payroll_allowance_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'potongan', 'columns' => 'A.name, A.value'));
		$data['payroll']		= $this->payroll_allowance_model->get(array('id' => $payroll_id, 'columns' => 'A.id, A.employee_id , A.periode_start, A.periode_end, A.salary'));
		$data['employee']		= $this->employee_model->get(array('id' => $data['payroll']->employee_id,'columns' => 'A.full_name, A.bank_account, A.employee_number, A.position_id, B.name AS site_name'));
		$data['position']		= $this->position_model->get(array('id' => $data['employee']->position_id,'columns' => 'A.name, B.code AS company_code'));

		$data['_TITLE_'] 		= 'Preview Gaji Karyawan';
		$data['_PAGE_']	 		= 'ropayroll/preview';
		$data['_MENU_PARENT_'] 	= 'payroll_employee';
		$data['_MENU_'] 		= 'allowance';
		$this->load->view('payroll_allowance/preview', $data);
	}

	public function pdf($payroll_id=FALSE)
	{
		$this->load->model(array('payroll_allowance_model', 'employee_model', 'position_model'));
		$income			= $this->payroll_allowance_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'pendapatan', 'columns' => 'A.name, A.value'));
		$outcome		= $this->payroll_allowance_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'potongan', 'columns' => 'A.name, A.value'));
		$payroll		= $this->payroll_allowance_model->get(array('id' => $payroll_id, 'columns' => 'A.id, A.employee_id , A.periode_start, A.periode_end, A.salary'));
		$employee		= $this->employee_model->get(array('id' => $payroll->employee_id,'columns' => 'A.full_name, A.bank_account, A.employee_number, A.position_id, B.name AS site_name'));
		$position		= $this->position_model->get(array('id' => $employee->position_id,'columns' => 'A.name, B.name AS company_name, B.code AS company_code'));

		if (!$payroll_id){
			$data["message"] = message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger');
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
			redirect(base_url('payroll_allowance/list'));
		}

		$start_date = substr($payroll->periode_start, 8, 2).' '.get_month(substr($payroll->periode_start, 5, 2));
		$end_date 	= substr($payroll->periode_end, 8, 2).' '.get_month(substr($payroll->periode_end, 5, 2)).' '.substr($payroll->periode_end, 0, 4);

		$html  = '<table width="100%">';
		$html .= '<tr><td width="25%">PERIODE BULAN</td><td> : '.$start_date.' - '.$end_date.'</td></tr>
				<tr><td>NO REKENING</td><td> : '.$employee->bank_account.'</td></tr>
				<tr><td>NAMA</td><td> : '.$employee->full_name.'</td></tr>
				<tr><td>NOMOR KARYAWAN</td><td> : '.$employee->employee_number.'</td></tr>
				<tr><td>JABATAN</td><td> : '.$position->name.'</td></tr>
				<tr><td>SITE</td><td> : '.$employee->site_name.'</td></tr>';
		$html .= '</table><table width="100%">';
		$html .= '<tr><td width="60%">&nbsp;</td><td><strong>Gaji Bersih diterima : '.format_rupiah($payroll->salary).'</strong></td></tr>';
		$html .= '</table><br><br><table width="100%">';
		$html .= '<tr><td width="50%"><table>';
		$html .= '<tr><td><strong>Pendapatan</strong></td><td>&nbsp;</td></tr>';
		foreach( $income AS $item):
			$html .= '<tr><td >'.$item->name.'</td><td>:&nbsp;&nbsp;'.format_rupiah($item->value).'</td></tr>';
		endforeach;
		$html .= '</table></td>';
		$html .= '<td width="50%"><table>';
		$html .= '<tr><td><strong>Potongan</strong></td><td>&nbsp;</td></tr>';
		foreach($outcome AS $item):
			$html .= '<tr><td >'.$item->name.'</td><td>:&nbsp;&nbsp;'.format_rupiah($item->value).'</td></tr>';
		endforeach;
		$html .= '</table></td></tr>';
		$html .= '</table>';
		$html .='</table></td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr></table><div syle="width=100%"></hr></div><br pagebreak="true"/>';

		$data['content'] 	= $html; 
		$data['title'] 		= 'Salary';
		$data['basepath'] 	= FCPATH.'files/';
		$data['image']		= strtolower($position->company_code).'.jpg';
		$data['company']	= $position->company_name; 
		$this->generate_PDF($data);
		redirect(base_url('files/'.strtolower(str_replace(" ", "_",$data['title'])).'.pdf'));
	}

	private function generate_PDF($data = array())
	{	
		$this->load->library("Pdf");
		ob_start();
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		if(isset($data['password'])){
			$pdf->SetProtection(array('print', 'copy'), $data['password'], null, 0, null);
		}
		$pdf->SetHeaderData($data['image'], '181', '', null);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shelter');
		$pdf->SetTitle($data['title']);
		$pdf->SetSubject($data['title']);
		$pdf->SetKeywords('Shelter');

		// $pdf->setPrintHeader(true);
		// $pdf->setPrintFooter(false);

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$pdf->SetFont('times', '', 10);
		$pdf->AddPage();
		$pdf->writeHTML($data['content'], true, false, true, false, '');

		$base_path  = $data['basepath'];
		if(!is_dir($base_path)){
			mkdir($base_path, 0777, true);
		}
		return $pdf->Output($base_path.strtolower(str_replace(" ", "_",$data['title'])).'.pdf', 'F');
	}

	public function import(){
		$this->load->model(array('payroll_config_model', 'employee_model', 'payroll_allowance_model'));
		if($_FILES['file']['name'] != ""){
			
			$config['site_id'] 			= $this->session->userdata('site');
			$config['row_start'] 		= $this->input->post('row_start');
			$config['row_end'] 			= $this->input->post('row_end');
			$config['row_title'] 		= $this->input->post('row_title');
			$config['column_employee'] 	= strtoupper($this->input->post('column_employee'));
			$config['column_income'] 	= strtoupper($this->input->post('column_income'));
			$config['column_outcome'] 	= strtoupper($this->input->post('column_outcome'));
			$config['column_net'] 		= strtoupper($this->input->post('column_net'));
			$config['column_attendance']= strtoupper($this->input->post('column_attendance'));
			$this->payroll_config_model->save($config);

			$periode_start				= $this->input->post('periode_start');
			$periode_end				= $this->input->post('periode_end');
			
			$this->load->library("Excel");
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);

			$title 			= array();
			$column_income  = explode(",", preg_replace("/[^A-Z]/", "", $config['column_income']));
			$column_outcome = explode(",", preg_replace("/[^A-Z,]/", "", $config['column_outcome']));
			foreach($column_income AS $item){
				$title[$item] = replace_null($sheet->getCell($item.$config['row_title'])->getCalculatedValue());
			}
			foreach($column_outcome AS $item){
				$title[$item] = replace_null($sheet->getCell($item.$config['row_title'])->getCalculatedValue());
			}

			for ($row = $config['row_start']; $row <= $config['row_end']; $row++) {
				$employee_number 	= replace_null($sheet->getCell($config['column_employee'].$row)->getCalculatedValue());
				if($employee_number == ""){
					continue;
				}
				$employee = $this->employee_model->get(array('employee_number' => $employee_number, 'site_id' => $this->session->userdata('site'), 'columns' => 'A.id'));
				if($employee){
					$payroll = $this->payroll_allowance_model->get(array('employee_id' => $employee->id, 'periode_start' => $periode_start, 'periode_end' => $periode_end, 'columns' => 'A.id, A.payment'));
					
					if($payroll){
						if($payroll->payment == 'Selesai'){
							continue;
						}

						$this->payroll_allowance_model->save(array('id' => $payroll->id, 'is_active' => 0));
						$this->payroll_allowance_model->delete_detail(array('payroll_id' => $payroll->id));
					}

					$salary_net 		= round(replace_null($sheet->getCell($config['column_net'].$row)->getOldCalculatedValue()));
					if($salary_net == ""){
						$salary_net = round(replace_null($sheet->getCell($config['column_net'].$row)->getCalculatedValue()));
					}
					$attendance 		= replace_null($sheet->getCell($config['column_attendance'].$row)->getOldCalculatedValue());
					if($attendance == ""){
						$attendance = round(replace_null($sheet->getCell($config['column_attendance'].$row)->getCalculatedValue()));
					}

					$insert['id']				= '';	
					$insert['employee_id'] 		=  $employee->id;
					$insert['periode_start'] 	=  $periode_start;
					$insert['periode_end'] 		=  $periode_end;
					$insert['salary'] 			=  $salary_net;
					$insert['payment'] 			=  'Menunggu';
					$insert['email'] 			=  'Menunggu';
					$insert['attendance'] 		=  $attendance;
					$payroll_id = $this->payroll_allowance_model->save($insert);
					foreach($column_income AS $item){
						$insert_detail['payroll_id']= $payroll_id;
						$insert_detail['name'] 		= $title[$item];
						$insert_detail['category'] 	= 'pendapatan';
						$insert_detail['value'] 	= round(replace_null($sheet->getCell($item.$row)->getOldCalculatedValue()));
						if($insert_detail['value'] == ""){
							$insert_detail['value'] = round(replace_null($sheet->getCell($item.$row)->getCalculatedValue()));
						}
						$this->payroll_allowance_model->save_detail($insert_detail);
					}

					foreach($column_outcome AS $item){
						$insert_detail['payroll_id']= $payroll_id;
						$insert_detail['name'] 		= $title[$item];
						$insert_detail['category'] 	= 'potongan';
						$insert_detail['value'] 	= round(replace_null($sheet->getCell($item.$row)->getOldCalculatedValue()));
						if($insert_detail['value'] == ""){
							$insert_detail['value'] = round(replace_null($sheet->getCell($item.$row)->getCalculatedValue()));
						}
						$this->payroll_allowance_model->save_detail($insert_detail);
					}
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			redirect(base_url('payroll_allowance/list'));
		}
	}

	public function export()
	{
		$this->load->model(array('payroll_allowance_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title 				= "Data THR";
		$params 			= $this->input->get();
		$params['site_id']	= $this->session->userdata('site');

		$title = 'payroll_karyawan';
		$files = glob(FCPATH."files/".$title."/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Nomor Karyawan");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("C".$i, "Nomor Rekening");
		$excel->getActiveSheet()->setCellValue("D".$i, "E-mail");
		$excel->getActiveSheet()->setCellValue("E".$i, "Gaji Bersih");
		$excel->getActiveSheet()->setCellValue("F".$i, "Status Pembayaran");

		$params['columns'] 		= "A.id, B.full_name, B.email, B.employee_number, B.bank_account, A.salary, A.payment";
		$params['orderby']		= "B.full_name";
		$params['order']		= "ASC";
		$list_payroll = $this->payroll_allowance_model->gets($params);
		$i=2;
		foreach ($list_payroll as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->employee_number);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->bank_account);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->email);
			$excel->getActiveSheet()->setCellValueExplicit("E".$i, $item->salary);
			$excel->getActiveSheet()->setCellValueExplicit("F".$i, $item->payment);
			$i++;
		}
		$excel->getActiveSheet()->setTitle($title);
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	private function send_mail($data = array()){
        $mail 				= new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPOptions 	= array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
					)
				);
		// $mail->SMTPDebug 	= SMTP::DEBUG_SERVER;
		$mail->Host 		= $this->config->item('host');
		$mail->Port 		= $this->config->item('port');
		$mail->SMTPSecure 	= PHPMailer::ENCRYPTION_STARTTLS;
		$mail->SMTPAuth 	= true;
		$mail->SMTPSecure 	= 'ssl';
		$mail->Username 	= $this->config->item('email');
		$mail->Password 	= $this->config->item('password');
		$mail->setFrom($data['email'], $data['sender']);
		$mail->addAddress($data['receiver']);
		$mail->Subject 		= $data['subject'];
		$mail->msgHTML($data['message']);

		if($data['carbon_copy'] != ''){
			$mail->AddCC($data['carbon_copy']);
		}
		if(isset($data['attachment'])){
			$mail->addAttachment($data['attachment']);
		}
		if (!$mail->send()) {
			return FALSE;
		} else {
			return TRUE;
		}
	}
}
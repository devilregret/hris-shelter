<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rotrial extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
		if($this->session->userdata('site') == ''){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan pilih site bisnis.','danger'));
			redirect(base_url());	
		}
	}

	public function attendance($date = false){

		if(!$date){
			$date = date('d-m-Y');
		}
		$this->load->model(array('kehadiran_trial_model', 'site_model'));


		$data['_TITLE_'] 		= 'Rekap Kehadiran';
		$data['_PAGE_'] 		= 'rotrial/attendance';
		$data['_MENU_PARENT_'] 	= 'rotrial';
		$data['_MENU_'] 		= 'rotrial_attendance';
		$data['last_period'] 	= '';
		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address'));
		$last_period 			= $this->kehadiran_trial_model->gets(array('site_id' => $this->session->userdata('site'), 'columns' => 'DATE_FORMAT(MAX(A.periode), "%d-%m-%Y") AS periode'));
		if($last_period){
			$data['last_period'] = $last_period[0]->periode;
		}
		
		$data['date'] = $date;
		
		$this->view($data);
	}

	public function attendance_ajax($date = false){

		if(!$date){
			$date = date('d-m-Y');
		}

		$this->load->model(array('kehadiran_trial_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.employee_id, B.id_card, B.full_name, A.hari_kerja, A.masuk_kerja, A.shift_malam, A.hari_libur, A.izin, A.cuti, A.sakit, A.alpha, A.non_aktif, A.telat, A.lembur, A.uang_lembur, A.uang_makan";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['id_card']	= $_POST['columns'][0]['search']['value'];
		$params['full_name']		= $_POST['columns'][1]['search']['value'];
		
		$params['site_id']		= $this->session->userdata('site');
		$params['periode']		= substr($date, 6, 4).'-'.substr($date, 3, 2).'-'.substr($date, 0, 2);

		$list 	= $this->kehadiran_trial_model->gets($params);
		$total 	= $this->kehadiran_trial_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{			
			$result['no'] 				= $i;
			$result['id_card']			= $item->id_card;
			$result['full_name']		= $item->full_name;
			$result['hari_kerja']		= $item->hari_kerja;
			$result['masuk_kerja']		= $item->masuk_kerja;
			$result['shift_malam']		= $item->shift_malam;
			$result['hari_libur']		= $item->hari_libur;
			$result['izin']				= $item->izin;
			$result['cuti']				= $item->cuti;
			$result['sakit']			= $item->sakit;
			$result['alpha']			= $item->alpha;
			$result['non_aktif']		= $item->non_aktif;
			$result['telat']			= $item->telat;
			$result['lembur']			= $item->lembur;
			$result['uang_lembur']		= rupiah_round($item->uang_lembur);
			$result['uang_makan']		= rupiah_round($item->uang_makan);
			$result['action'] 			= '';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function export_attendance($date = false){	
		if(!$date){
			$date = date('m-Y');
		}

		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = 'rekap_kehadiran';
		$files = glob(FCPATH."files/".$title."/*");


		$this->load->model(array('kehadiran_trial_model'));

		$params['columns'] 		= "A.id, A.employee_id, B.id_card, B.full_name, A.hari_kerja, A.masuk_kerja, A.shift_malam, A.hari_libur, A.izin, A.cuti, A.sakit, A.alpha, A.non_aktif, A.telat, A.lembur, A.uang_lembur, A.uang_makan";
		$params['orderby'] 		= 'B.full_name';
		$params['order']		= 'ASC';
			
		$params['site_id']		= $this->session->userdata('site');
		$params['periode']		= substr($date, 6, 4).'-'.substr($date, 3, 2).'-'.substr($date, 0, 2);
		$list 					= $this->kehadiran_trial_model->gets($params);

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "NIK KTP");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nama Karyawan");
		$excel->getActiveSheet()->setCellValue("C".$i, "Hari Kerja");
		$excel->getActiveSheet()->setCellValue("D".$i, "Masuk Kerja");
		$excel->getActiveSheet()->setCellValue("E".$i, "Shift Malam");
		$excel->getActiveSheet()->setCellValue("F".$i, "Hari Libur");
		$excel->getActiveSheet()->setCellValue("G".$i, "Izin");
		$excel->getActiveSheet()->setCellValue("H".$i, "Cuti");
		$excel->getActiveSheet()->setCellValue("I".$i, "Sakit");
		$excel->getActiveSheet()->setCellValue("J".$i, "Alpha");
		$excel->getActiveSheet()->setCellValue("K".$i, "Non Aktif");
		$excel->getActiveSheet()->setCellValue("L".$i, "Telat");
		$excel->getActiveSheet()->setCellValue("M".$i, "Lembur");
		$excel->getActiveSheet()->setCellValue("N".$i, "Uang Makan");
		$excel->getActiveSheet()->setCellValue("O".$i, "Uang Lembur");

		$i=2;
		foreach ($list as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValue("C".$i, $item->hari_kerja);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->masuk_kerja);
			$excel->getActiveSheet()->setCellValue("E".$i, $item->shift_malam);
			$excel->getActiveSheet()->setCellValue("F".$i, $item->hari_libur);
			$excel->getActiveSheet()->setCellValue("G".$i, $item->izin);
			$excel->getActiveSheet()->setCellValue("H".$i, $item->cuti);
			$excel->getActiveSheet()->setCellValue("I".$i, $item->sakit);
			$excel->getActiveSheet()->setCellValue("J".$i, $item->alpha);
			$excel->getActiveSheet()->setCellValue("K".$i, $item->non_aktif);
			$excel->getActiveSheet()->setCellValue("L".$i, $item->telat);
			$excel->getActiveSheet()->setCellValue("M".$i, $item->lembur);
			$excel->getActiveSheet()->setCellValue("N".$i, $item->uang_makan);
			$excel->getActiveSheet()->setCellValue("O".$i, $item->uang_lembur);
			$i++;
		}
		$excel->getActiveSheet()->setTitle('Rekap Kehadiran');
		$sheet = $excel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells( true );
		foreach( $cellIterator as $cell ) {
			$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		}
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function import_attendance($date = false){
		if(!$_FILES['file']['name'] == ""){
			$this->load->library("Excel");
			$this->load->model(array('employee_model', 'kehadiran_trial_model'));
			
			$date = $this->input->post('date');

			$params['periode']		= substr($date, 6, 4).'-'.substr($date, 3, 2).'-'.substr($date, 0, 2);
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			$message_warning= ""; 
			for ($row = 2; $row <= $rend; $row++) {

				$id_card 	= 	replace_null($sheet->getCell('A'.$row)->getValue());				
				$employee  	= $this->employee_model->get(array('id_card' => $id_card, 'columns' => 'A.id'));
				if(!$employee){
					$message_warning .= $id_card.', ';
					continue;
				}

				$params['id']	= '';
				$params['employee_id'] 	= $employee->id;
				
				$kehadiran  	= $this->kehadiran_trial_model->get(array('employee_id' => $employee->id, 'periode' => $params['periode'], 'columns' => 'A.id'));
				if($kehadiran){
					$params['id']	= $kehadiran->id;
				}
				$params['hari_kerja'] 	= replace_null($sheet->getCell('C'.$row)->getValue());
				$params['masuk_kerja'] 	= replace_null($sheet->getCell('D'.$row)->getValue());
				$params['hari_libur'] 	= replace_null($sheet->getCell('E'.$row)->getValue());
				$params['shift_malam'] 	= replace_null($sheet->getCell('F'.$row)->getValue());
				$params['izin'] 		= replace_null($sheet->getCell('G'.$row)->getValue());
				$params['cuti'] 		= replace_null($sheet->getCell('H'.$row)->getValue());
				$params['sakit'] 		= replace_null($sheet->getCell('I'.$row)->getValue());
				$params['alpha'] 		= replace_null($sheet->getCell('J'.$row)->getValue());
				$params['non_aktif'] 	= replace_null($sheet->getCell('K'.$row)->getValue());
				$params['telat'] 		= replace_null($sheet->getCell('L'.$row)->getValue());
				$params['lembur'] 		= replace_null($sheet->getCell('M'.$row)->getValue());
				$params['uang_makan'] 	= replace_null($sheet->getCell('N'.$row)->getValue());
				$params['uang_lembur'] 	= replace_null($sheet->getCell('O'.$row)->getValue());
				 
				$this->kehadiran_trial_model->save($params);
			}

			if($message_warning != ''){
				$this->session->set_flashdata('message', message_box('<strong>Peringatan!</strong>  NIK : '.$message_warning.', belum terdaftar pada site ini.','warning'));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
		}

		redirect(base_url('rotrial/attendance/'.$date));
	}

	public function payroll($view = 'last', $config_id = FALSE){

		$this->load->model(array('payroll_trial_model', 'employee_model', 'site_model'));
		$data['_TITLE_'] 		= 'Payroll Karyawan';
		$data['_PAGE_'] 		= 'rotrial/payroll';
		$data['_MENU_PARENT_'] 	= 'rotrial';
		$data['_MENU_'] 		= 'rotrial_payroll';
		$data['site_id']		= $this->session->userdata('site');
		$data['view']			= $view;

		$last_payroll 		= $this->payroll_trial_model->last_payroll(array('site_id' => $data['site_id'], 'payment' => 'Selesai', 'columns' => 'MAX(A.periode_start) AS last_payroll'));
		if($last_payroll){
			$data['resume_payroll'] = $this->payroll_trial_model->last_payroll(array('site_id' => $data['site_id'], 'preview' => $last_payroll->last_payroll, 'payment' => 'Selesai', 'columns' => 'COUNT(A.employee_id) AS employee_payroll, SUM(A.salary) AS summary_payroll, MAX(A.periode_start) AS periode_start, MAX(A.periode_end) AS periode_end'));
		}

		$last_approval 		= $this->payroll_trial_model->last_payroll(array('site_id' => $data['site_id'], 'in_payment' => array('Menunggu', 'Draft', 'Ditolak'), 'columns' => 'MAX(A.periode_start) AS last_approval'));
		if($last_approval){
			$data['resume_approval'] = $this->payroll_trial_model->last_payroll(array('site_id' => $data['site_id'], 'preview' => $last_approval->last_approval, 'in_payment' => array('Menunggu', 'Draft', 'Ditolak'), 'columns' => 'COUNT(A.employee_id) AS employee_payroll, SUM(A.salary) AS summary_payroll, MAX(A.periode_start) AS periode_start, MAX(A.periode_end) AS periode_end'));
		}

		$params['status']		= 1;
		$params['status_approval'] 		= 3;
		$params['site_id'] 		= $data['site_id'];
		$data['employee']		= $this->employee_model->gets($params, TRUE);
		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address'));
		$this->view($data);
	}

	public function payroll_ajax($view = FALSE){
		
		$this->load->model(array('payroll_trial_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, B.full_name AS employee_name, B.phone_number, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.bpjs_ks_company, A.bpjs_jht_company, A.salary, A.payment, A.email, A.note";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['position_type']= 'all';
		$params['employee_name']	= $_POST['columns'][2]['search']['value'];
		$params['phone_number']		= $_POST['columns'][3]['search']['value'];
		$params['periode_start']	= $_POST['columns'][4]['search']['value'];
		$params['periode_end']		= $_POST['columns'][5]['search']['value'];
		$params['bpjs_ks_company']	= $_POST['columns'][6]['search']['value'];
		$params['bpjs_jht_company']	= $_POST['columns'][7]['search']['value'];
		$params['salary']			= $_POST['columns'][8]['search']['value'];
		$params['payment']			= $_POST['columns'][9]['search']['value'];
		$params['email']			= $_POST['columns'][10]['search']['value'];
		$params['note']				= $_POST['columns'][11]['search']['value'];
		
		$params['site_id']			= $this->session->userdata('site');
		if($view == 'last'){
			$params['in_payment'] 	= array('Menunggu', 'Draft', 'Ditolak');
			$last_payroll 				= $this->payroll_trial_model->last_payroll(array('site_id' => $params['site_id'], 'in_payment' => $params['in_payment'], 'columns' => 'MAX(A.periode_start) AS last_payroll'));
			if($last_payroll){
				$params['preview']		= $last_payroll->last_payroll;
			}
		}
		$list 	= $this->payroll_trial_model->gets($params);
		$total 	= $this->payroll_trial_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 				= $i;
			$result['employee_name']	= $item->employee_name;
			$result['phone_number']		= $item->phone_number;
			$result['periode_start']	= $item->periode_start;
			$result['periode_end']		= $item->periode_end;
			$result['bpjs_ks_company']	= format_rupiah((double) $item->bpjs_ks_company);
			$result['bpjs_jht_company']	= format_rupiah((double) $item->bpjs_jht_company);
			$result['salary']			= format_rupiah((double) $item->salary);
			$result['note']				= $item->note;
			$result['payment']			= '<p class="text-danger">'.$item->payment.'</p>';
			if($item->payment ==  'Selesai'){
				$result['payment']		= '<p class="text-success">'.$item->payment.'</p>';
				$result['id']			= "";
			}
			if($item->payment ==  'Menunggu'){
				$result['payment']		= '<p class="text-warning">'.$item->payment.'</p>';
			}

			$result['email']			= '<p class="text-warning">'.$item->email.'</p>';
			if($item->email ==  'Terkirim'){
				$result['email']		= '<p class="text-success">'.$item->email.'</p>';
			}else if($item->email == 'Gagal'){
				$result['email']		= '<p class="text-danger">'.$item->email.'</p>';
			}
			
			$result['action'] 			=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("rotrial/preview_payroll/".$item->id).'">Lihat</a>';
			if($item->payment == 'Draft'){
				$result['action'] .= 
				'<a href="'.base_url('rotrial/form_payroll/' . $item->id).'" style="cursor:pointer;" class="btn-sm btn-success btn-block">Ubah</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}

	public function preview_payroll($payroll_id=FALSE)
	{
		$this->load->model(array('payroll_trial_model', 'employee_model', 'position_model'));
		$data['income']			= $this->payroll_trial_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'pendapatan', 'columns' => 'A.name, A.value'));
		$data['outcome']		= $this->payroll_trial_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'potongan', 'columns' => 'A.name, A.value'));
		$data['payroll']		= $this->payroll_trial_model->get(array('id' => $payroll_id, 'columns' => 'A.id, A.employee_id, A.periode_start, A.periode_end, A.salary, A.overtime_hour, A.overtime_calculation, A.attendance, A.basic_salary, A.bpjs_ks, A.bpjs_jht, A.bpjs_jp, A.tax_calculation, A.salary_note'));
		$data['employee']		= $this->employee_model->get(array('id' => $data['payroll']->employee_id,'columns' => 'A.full_name, A.bank_account, A.employee_number, A.position_id, B.name AS site_name, C.code AS company_code'));
		$data['position']		= $this->position_model->get(array('id' => $data['employee']->position_id,'columns' => 'A.name'));

		$data['_TITLE_'] 		= 'Preview Gaji Karyawan';
		$data['_PAGE_']	 		= 'rotrial/preview_payroll';
		$data['_MENU_PARENT_'] 	= 'rotrial';
		$data['_MENU_'] 		= 'rotrial_payroll';
		$this->load->view('rotrial/preview_payroll', $data);
	}

	public function form_payroll($payroll_id = FALSE)
	{
		$this->load->model(array('payroll_trial_model', 'employee_model'));

		if($this->input->post()){
			$data['id'] 					= $this->input->post('payroll_id');
			$data['salary']					= $this->input->post('salary');
			$data['overtime_hour']			= $this->input->post('overtime_hour');
			$data['overtime_calculation']	= $this->input->post('overtime_calculation');
			$data['attendance']				= $this->input->post('attendance');
			$data['basic_salary']			= $this->input->post('basic_salary');
			$data['bpjs_ks']				= $this->input->post('bpjs_ks');
			$data['bpjs_jht']				= $this->input->post('bpjs_jht');
			$data['bpjs_ks_company']		= $this->input->post('bpjs_ks_company');
			$data['bpjs_jht_company']		= $this->input->post('bpjs_jht_company');
			$data['tax_calculation']		= $this->input->post('tax_calculation');
			$data['salary_note']			= $this->input->post('salary_note');

			$this->form_validation->set_rules('payroll_id', '', 'required');
			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$this->payroll_trial_model->save($data);
				$this->payroll_trial_model->delete_detail(array('payroll_id' => $data['id']));
				
				$payroll_detail_name		= $this->input->post('payroll_detail_name');
				$payroll_detail_category	= $this->input->post('payroll_detail_category');
				$payroll_detail_value		= $this->input->post('payroll_detail_value');
				foreach ($payroll_detail_name AS $key => $value ) {
					if($payroll_detail_value[$key] == ''){
						continue;
					}
					$data_detail['payroll_id']	= $data['id'];
					$data_detail['name'] 		= $payroll_detail_name[$key];
					$data_detail['category'] 	= $payroll_detail_category[$key];
					$data_detail['value']		= $payroll_detail_value[$key];
					
					$this->payroll_trial_model->save_detail($data_detail);

				}
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			}
			redirect(base_url('rotrial/form_payroll/'.$data['id']));
			exit();
		}

		if ($payroll_id)
		{
			$data = (array) $this->payroll_trial_model->get(array('id' => $payroll_id, 'columns' => 'A.id, A.employee_id, A.periode_start, A.periode_end, A.salary, A.overtime_hour, A.overtime_calculation, A.attendance, A.basic_salary, A.bpjs_ks, A.bpjs_jht, A.bpjs_ks_company, A.bpjs_jht_company, A.tax_calculation, A.note, A.salary_note'));
	
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('rotrial/payroll'));
		}
		// print_r($data['employee_id']);
		// die();
		$data['employee']		= $this->employee_model->get(array('id' => $data['employee_id'], 'columns' => 'A.id_card, A.full_name'));
		if(!$data['employee']){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data karyawan tidak ditemukan.','danger'));
			redirect(base_url('rotrial/payroll'));
		}

		$data['payroll_detail'] = $this->payroll_trial_model->gets_detail(array('payroll_id' => $payroll_id));
		$data['_TITLE_'] 		= 'Gaji Karyawan';
		$data['_PAGE_'] 		= 'rotrial/form_payroll';
		$data['_MENU_PARENT_'] 	= 'rotrial';
		$data['_MENU_'] 		= 'rotrial_payroll';
		return $this->view($data);
	}

	public function payroll_count(){
		$site_id	= $this->session->userdata('site');
		$date_start	= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_start'))));
		$date_end	= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_finish'))));

		$this->load->model(array('kehadiran_trial_model'));
		$total 	= $this->kehadiran_trial_model->gets(array('site_id' => $site_id, 'periode_start' => $date_start, 'periode_end' => $date_end), TRUE);
		$limit 	= 50;
		$loop 	= (int) ($total/50);
		// for($i=0; $i<=$loop; $i++){
		// 	$start = $i*$limit;
		// 	$this->count($start, $limit, $date_start, $date_end);			
		// }
		// // print_prev($loop);
		// // print_prev($total);
		// // die();
		// $this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Gaji Karyawan berhasil diproses.','success'));

		redirect(base_url('rotrial/count/0/'.$loop.'/'.$date_start.'/'.$date_end));
		
	}

	public function count($start, $end, $date_start, $date_end){
		
		$limit 	= 50;
		$page 	= $start * 50;
		$site_id	= $this->session->userdata('site');

		$this->load->model(array('kehadiran_trial_model', 'payroll_trial_model', 'formula_employee_model', 'setting_tax_model'));

		$formula_path = FCPATH.'files/formula/'.$site_id.'.xlsx';
		
		$this->load->library("Excel");
        $this->load->library('PHPExcel/iofactory');

		$list_absensi = $this->kehadiran_trial_model->gets(array('site_id' => $site_id, 'periode_start' => $date_start, 'periode_end' => $date_end, 'columns' => 'A.employee_id, A.hari_kerja, A.masuk_kerja, A.shift_malam, A.alpha, A.telat, A.lembur, A.uang_lembur, A.uang_makan, B.benefit_health, B.benefit_labor, B.ptkp', 'limit' => $limit, 'page' => $page));

		foreach($list_absensi AS $absensi){
			$excelreader    = new PHPExcel_Reader_Excel2007();
	        $excel          = $excelreader->load($formula_path);
	        $sheet      = $excel->getActiveSheet(0);

			$payroll_employee['id'] 			= '';
			$payroll = $this->payroll_trial_model->get(array('employee_id' => $absensi->employee_id, 'periode_start' => $date_start, 'periode_end' => $date_end, 'columns' => 'A.id, A.payment'));

			if($payroll){
				if($payroll->payment == 'Pengajuan' || $payroll->payment == 'Selesai'){
					continue;
				}
				$payroll_employee['id'] = $payroll->id;	
			}

			$payroll_employee['employee_id'] 			= $absensi->employee_id;
			$payroll_employee['periode_start'] 			= $date_start;
			$payroll_employee['periode_end'] 			= $date_end;
			$payroll_employee['is_active'] 				= 1;
			$payroll_employee['payment'] 				= 'Draft';
			$payroll_employee['email'] 					= 'Menunggu';
			$payroll_employee['salary'] 				= 0;
			$payroll_employee['overtime_hour'] 			= 0;
			$payroll_employee['overtime_calculation'] 	= 0;
			$payroll_employee['attendance'] 			= 0;
			$payroll_employee['basic_salary'] 			= 0;
			$payroll_employee['bpjs_ks'] 				= 0;
			$payroll_employee['bpjs_jht'] 				= 0;
			$payroll_employee['bpjs_jp'] 				= 0;
			$payroll_employee['bpjs_ks_company'] 		= 0;
			$payroll_employee['bpjs_jht_company'] 		= 0;
			$payroll_employee['bpjs_jp_company'] 		= 0;
			$payroll_employee['tax_calculation'] 		= 0;
			$payroll_employee['note'] 					= '';
			$payroll_employee['salary_note'] 			= '';

			$list_formula = $this->formula_employee_model->gets(array('employee_id' => $absensi->employee_id));

			
			if(!$list_formula){
				continue;
			}


			if($list_formula[16]->kolom != ''){
				$sheet->setCellValue($list_formula[16]->kolom."4", check_numeric($list_formula[16]->nilai));
	        	$payroll_employee['basic_salary'] 			= $list_formula[16]->nilai;
	    	}
	        if($list_formula[0]->kolom != ''){
				$sheet->setCellValue($list_formula[0]->kolom."4", check_numeric($absensi->hari_kerja));
	    	}
	    	if($list_formula[1]->kolom != ''){
	    		$sheet->setCellValue($list_formula[1]->kolom."4", check_numeric($absensi->masuk_kerja));
		        $payroll_employee['attendance'] 			= $absensi->masuk_kerja;
			}
	        if($list_formula[2]->kolom != ''){
				$sheet->setCellValue($list_formula[2]->kolom."4", check_numeric($absensi->shift_malam));
			}
	        if($list_formula[3]->kolom != ''){
				$sheet->setCellValue($list_formula[3]->kolom."4", check_numeric($absensi->alpha));
			}
	        if($list_formula[4]->kolom != ''){
	        	$sheet->setCellValue($list_formula[4]->kolom."4", check_numeric($absensi->telat));
	        }
	        if($list_formula[5]->kolom != ''){
	        	$sheet->setCellValue($list_formula[5]->kolom."4", check_numeric($absensi->lembur));
				$payroll_employee['overtime_hour'] 			= $absensi->lembur;
			}
	        if($list_formula[6]->kolom != ''){
	        	$sheet->setCellValue($list_formula[6]->kolom."4", check_numeric($absensi->uang_lembur));
				$payroll_employee['overtime_calculation'] 			= $absensi->uang_lembur;
			}
			if($absensi->benefit_labor != ''){
		        if($list_formula[11]->kolom != ''){
					$payroll_employee['bpjs_jht_company'] 		= $sheet->getCell($list_formula[11]->kolom.'4')->getCalculatedValue();
				}
		        if($list_formula[13]->kolom != ''){
					$payroll_employee['bpjs_jht'] 				= $sheet->getCell($list_formula[13]->kolom.'4')->getCalculatedValue();
				}
			}
			if($absensi->benefit_health != ''){
		        if($list_formula[12]->kolom != ''){
					$payroll_employee['bpjs_ks_company'] 		= $sheet->getCell($list_formula[12]->kolom.'4')->getCalculatedValue();
				}
		        if($list_formula[14]->kolom != ''){
					$payroll_employee['bpjs_ks'] 				= $sheet->getCell($list_formula[14]->kolom.'4')->getCalculatedValue();
				}
			}

			$payroll_id = $this->payroll_trial_model->save($payroll_employee);
			$payroll_employee['id'] = $payroll_id;
			$this->payroll_trial_model->delete_detail(array('payroll_id' => $payroll_id));


			if($list_formula[7]->kolom != ''){
	        	
	        	$payroll_employee_detail['payroll_id']	= $payroll_id;
				$payroll_employee_detail['name'] 		= 'UANG MAKAN';
				$payroll_employee_detail['is_active'] 	= 1;
				$payroll_employee_detail['category']	= 'POTONGAN';
				$payroll_employee_detail['value']   	= $sheet->setCellValue($list_formula[7]->kolom."4", check_numeric($absensi->uang_lembur));
				// $payroll_employee['overtime_calculation'] 			= $absensi->uang_lembur;
			}

			for ($i = 17; $i < count($list_formula); $i++){

		        if($list_formula[$i]->kolom == ''){
		        	continue;
		        }

				$payroll_employee_detail['payroll_id']	= $payroll_id;
				$payroll_employee_detail['name'] 		= $list_formula[$i]->nama;
				$payroll_employee_detail['is_active'] 	= 1;	
				$payroll_employee_detail['category']	= strtolower($list_formula[$i]->kategori);
				
				if($list_formula[$i]->kategori == 'FORMULA'){
					$payroll_employee_detail['value']   = $sheet->getCell($list_formula[$i]->kolom.'4')->getCalculatedValue();
				}else{
			        $sheet->setCellValue($list_formula[$i]->kolom."4", check_numeric($list_formula[$i]->nilai));
					$payroll_employee_detail['value']   = $list_formula[$i]->nilai;
				}

				$this->payroll_trial_model->save_detail($payroll_employee_detail);
			}

			$payroll_employee['salary'] 				= $sheet->getCell($list_formula[15]->kolom.'4')->getCalculatedValue();
			if($absensi->ptkp != ''){
				$tax  = $this->setting_tax_model->get(array('code' => $absensi->ptkp, 'columns' => 'A.minimal'));
				if($tax){
					$sheet->setCellValue($list_formula[9]->kolom."4", check_numeric($tax->minimal));
	        		$payroll_employee['tax_calculation'] 		= $sheet->getCell($list_formula[10]->kolom.'4')->getCalculatedValue();
	        	}
	    	}
	        $payroll_id = $this->payroll_trial_model->save($payroll_employee);

		}
		if($start <= $end){
			$loop = $start+1;
			redirect(base_url('rotrial/count/'.$loop.'/'.$end.'/'.$date_start.'/'.$date_end));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Gaji Karyawan berhasil diproses.','success'));
			redirect(base_url('rotrial/payroll'));
		}
	}
}
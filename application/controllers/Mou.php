<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mou extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('sales'));
		$role 	= array_merge($role, $this->config->item('legal'));
		$role 	= array_merge($role, $this->config->item('recruitment'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}
	
	public function approval(){
		$data['_TITLE_'] 		= 'Pengajuan Penawaran';
		$data['_PAGE_'] 		= 'mou/approval';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= 'approval';

		$this->view($data);
	}

	public function approval_ajax(){
		$this->load->model(array('cooperation_model', 'position_model'));

		$column_index 			= $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.status, A.position_id, B.name AS company_name, C.name AS branch_name, D.name AS site_name, E.full_name AS sales_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['status']		= 'Pengajuan MOU';

		$params['company_name']		= $_POST['columns'][1]['search']['value'];
		$params['branch_name']		= $_POST['columns'][2]['search']['value'];
		$params['site_name']		= $_POST['columns'][3]['search']['value'];
		$params['sales_name']		= $_POST['columns'][4]['search']['value'];

		if($this->session->userdata('role') == 29){
			$params['sales_id']		=  $this->session->userdata('user_id');
		}

		$list 	= $this->cooperation_model->gets($params);
		$total 	= $this->cooperation_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$position 		= "";
			$list_job 		= $this->position_model->gets(array('columns' => 'A.name','list_id' => explode(",",$item->position_id)));
			$first 			= true;
			foreach($list_job as $job){
				if($first){
					$position .= $job->name;
					$first = false;
				}else{
					$position .= ', '.$job->name;
				}
			}

			$result['id']				= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 				= $i;
			$result['company_name']		= $item->company_name;
			$result['branch_name']		= $item->branch_name;
			$result['site_name'] 		= $item->site_name;
			$result['sales_name'] 		= $item->sales_name;
			$result['position']			= $position;
			$result['status']			= $item->status;

			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("cooperation/preview_contract/".$item->id).'">Kontrak</a>';
	
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Daftar MOU';
		$data['_PAGE_'] 		= 'mou/list';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= 'mou';

		$this->view($data);
	}

	public function list_ajax(){
		$this->load->model(array('cooperation_model', 'position_model'));

		$column_index 			= $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.status, A.position_id, B.name AS company_name, C.name AS branch_name, D.name AS site_name, E.full_name AS sales_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['status']		= 'MOU';

		$params['company_name']		= $_POST['columns'][1]['search']['value'];
		$params['branch_name']		= $_POST['columns'][2]['search']['value'];
		$params['site_name']		= $_POST['columns'][3]['search']['value'];
		$params['sales_name']		= $_POST['columns'][4]['search']['value'];

		if($this->session->userdata('role') == 29){
			$params['sales_id']		=  $this->session->userdata('user_id');
		}

		$list 	= $this->cooperation_model->gets($params);
		$total 	= $this->cooperation_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$position 		= "";
			$list_job 		= $this->position_model->gets(array('columns' => 'A.name','list_id' => explode(",",$item->position_id)));
			$first 			= true;
			foreach($list_job as $job){
				if($first){
					$position .= $job->name;
					$first = false;
				}else{
					$position .= ', '.$job->name;
				}
			}

			$result['id']				= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 				= $i;
			$result['company_name']		= $item->company_name;
			$result['branch_name']		= $item->branch_name;
			$result['site_name'] 		= $item->site_name;
			$result['sales_name'] 		= $item->sales_name;
			$result['position']			= $position;
			$result['status']			= $item->status;

			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("cooperation/preview_contract/".$item->id).'">Kontrak</a>';
	
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function process($action = FALSE)
	{
		if($this->input->post()){
			$this->load->model(array('cooperation_model', 'cooperation_history_model'));
		
			$id 		= $this->input->post('id');
			foreach ($id as $cooperation_id) {
				if($action == 'reject'){
					$this->cooperation_model->save(array('id' => $cooperation_id, 'status' => 'Penawaran'));
					$this->cooperation_history_model->save_approval(array('cooperation_id' => $cooperation_id, 'status' => 'Ditolak'));
				}else if($action == 'approve'){
					$this->cooperation_model->save(array('id' => $cooperation_id, 'status' => 'MOU'));
					$this->cooperation_history_model->save_approval(array('cooperation_id' => $cooperation_id, 'status' => 'Diterima'));
				}else{
					continue;
				}

			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>Pengajuan MOU berhasil diproses.','success'));
		}
		redirect(base_url('mou/approval'));
	}
	
	public function legality(){
		$data['_TITLE_'] 		= 'Daftar Perizinan';
		$data['_PAGE_'] 		= 'mou/legality';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= 'legality';

		$this->view($data);
	}

	public function legality_ajax(){
		$this->load->model(array('legality_model'));

		$column_index 			= $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.name, A.regulation, B.code AS company_code, C.name AS branch_name, DATE_FORMAT(A.date_start, '%d/%m/%Y') AS date_start, DATE_FORMAT(A.date_end, '%d/%m/%Y') AS date_end, A.note, A.files";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['status']		= 'Pengajuan MOU';

		$params['name']				= $_POST['columns'][1]['search']['value'];
		$params['regulation']		= $_POST['columns'][2]['search']['value'];
		$params['company_code']		= $_POST['columns'][3]['search']['value'];
		$params['branch_name']		= $_POST['columns'][4]['search']['value'];
		$params['date_start']		= $_POST['columns'][5]['search']['value'];
		$params['date_end']			= $_POST['columns'][6]['search']['value'];
		$params['note']				= $_POST['columns'][7]['search']['value'];

		$list 	= $this->legality_model->gets($params);
		$total 	= $this->legality_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 				= $i;
			$result['name']				= $item->name;
			$result['regulation']		= $item->regulation;
			$result['company_code']		= $item->company_code;
			$result['branch_name']		= $item->branch_name;
			$result['date_start'] 		= $item->date_start;
			$result['date_end'] 		= '-';
			if($item->date_end != '00/00/0000'){
				$result['date_end'] 	= $item->date_end;
			}
			$result['note']				= $item->note;
			$result['files'] 			= '';
			if($item->files != ''){
				$result['files']			= '<a href="'.$item->files.'"" target="_blank">Dokumen</a>';
			}

			$result['action'] 		=
				'<a class="btn-sm btn-success btn-block" href="'.base_url("mou/legality_form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("mou/legality_delete/".$item->id).'">Hapus</a>';
	
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function legality_form($legality_id = FALSE)
	{
		$this->load->model(array('legality_model', 'company_model', 'branch_model'));
		
		$data['id']				= $legality_id;
		$data['name']			= '';
		$data['company_id']		= '';
		$data['branch_id']		= '';
		$data['date_start']		= '';
		$data['date_end']		= '';
		$data['note']			= '';
		$data['regulation']		= '';
		$data['files']			= '';

		if($this->input->post()){
			$this->form_validation->set_rules('name', 'Nama', 'required');

			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$data['id']			= $this->input->post('id');
				$data['name']		= $this->input->post('name');
				$data['company_id']	= $this->input->post('company_id');
				$data['branch_id']	= $this->input->post('branch_id');
				$data['date_start']	= $this->input->post('date_start');
				$data['date_end']	= $this->input->post('date_end');
				$data['note']		= $this->input->post('note');
				$data['regulation']	= $this->input->post('regulation');

				$url_document = 'files/legality';
				$document_path = FCPATH.$url_document;
				$config['upload_path'] 	= $document_path;
				$config['overwrite']  	= TRUE;
				$config['allowed_types']= 'jpg|jpeg|png|pdf|doc|docx';
				if($_FILES['document']['name'] != ""){
					$config['file_name'] 	=  preg_replace("/[^a-zA-Z0-9]/", "", $data['name'].$data['id']);
					$this->load->library('upload', $config, 'document');
					if (!$this->document->do_upload('document')) {
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Proses upload Foto gagal.','danger'));
						redirect(base_url('mou/legality'));
					} else {
						$document 	= $this->document->data();
						$data['files'] = base_url($url_document.'/'.$document['file_name']);
					}
				}
				$this->legality_model->save($data);
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
				redirect(base_url('mou/legality'));
			}
		}
		
		if ($legality_id)
		{
			$data = (array) $this->legality_model->get(array('id' => $legality_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('mou/legality'));
			}
		}

		$data['_TITLE_']		= 'Form Perizinan';
		$data['_PAGE_']			= 'mou/legality_form';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= 'legality';
		$data['list_company'] 	= $this->company_model->gets(array('columns' => 'A.id, A.code, A.name'));
		$data['list_branch'] 	= $this->branch_model->gets(array('columns' => 'A.id, A.name'));
		$this->view($data);
	}

	public function legality_delete($legality_id = false)
	{
		$this->load->model('legality_model');
		if ($legality_id)
		{
			$data =  $this->legality_model->get(array('id' => $legality_id, 'columns' => 'A.id'));
			if ($data)
			{
				$result = $this->legality_model->save(array('id' => $legality_id, 'is_active' => 0));
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
				redirect(base_url('mou/legality'));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('mou/legality'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('mou/legality'));
		}
	}

	public function contract($type = 'registration'){
		$data['_TITLE_'] 		= 'Daftar Kontrak Kerjasama';
		$data['_PAGE_'] 		= 'mou/contract';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= $type;
		$data['type'] 			= $type;

		$this->view($data);
	}

	public function contract_ajax($type = FALSE){
		$this->load->model(array('mou_model'));
		$column_index 			= $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.company_name, B.code AS old_company_code, C.code AS company_code, D.name AS province_name, E.name AS city_name, DATE_FORMAT(A.contract_start, '%d/%m/%Y') AS contract_start, DATE_FORMAT(A.contract_end, '%d/%m/%Y') AS contract_end, A.headcount, A.crm, A.status_registration, A.status_mou, A.files";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['status_mou']	= 'DAFTAR';
		if($type == 'recontract'){
			$params['status_mou'] = 'REKONTRAK';
		}
		$params['company_name']			= $_POST['columns'][0]['search']['value'];
		$params['company_code']			= $_POST['columns'][1]['search']['value'];
		$params['province_name']		= $_POST['columns'][2]['search']['value'];
		$params['city_name']			= $_POST['columns'][3]['search']['value'];
		$params['contract_start']		= $_POST['columns'][4]['search']['value'];
		$params['contract_end']			= $_POST['columns'][5]['search']['value'];
		$params['headcount']			= $_POST['columns'][6]['search']['value'];
		$params['crm']					= $_POST['columns'][7]['search']['value'];
		$params['status_registration']	= $_POST['columns'][8]['search']['value'];

		$list 	= $this->mou_model->gets($params);
		$total 	= $this->mou_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 					= $i;
			$result['company_name']			= $item->company_name;
			$result['company_code']			= 'Awal : <strong>'.$item->old_company_code.'</strong><br> Baru : <strong>'.$item->company_code.'</strong>';
			$result['province_name']		= $item->province_name;
			$result['city_name']			= $item->city_name;
			$result['contract_start'] 		= $item->contract_start;
			$result['contract_end'] 		= $item->contract_end;
			$result['headcount'] 			= $item->headcount;
			$result['crm']					= $item->crm;
			$result['status_registration'] 	= $item->status_registration;
			$result['files'] 			= '';
			if($item->files != ''){
				$result['files']			= '<a href="'.$item->files.'"" target="_blank">Dokumen</a>';
			}
			$type = 'registration';
			if($item->status_mou == 'REKONTRAK'){
				$type = 'recontract';
			}
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("mou/contract_preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("mou/contract_form/".$type.'/'.$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("mou/contract_delete/".$type.'/'.$item->id).'">Hapus</a>';
	
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function contract_preview($contract_id=FALSE)
	{
		$this->load->model('mou_model');
		$data['_TITLE_'] 		= 'Preview Kerjasama';
		$data['_PAGE_']	 		= 'mou/contract_preview';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= 'registration';

		$params['id']			= $contract_id;
		$params['columns'] 		= "A.id, A.company_name, B.code AS old_company_code, C.code AS company_code, D.name AS province_name, E.name AS city_name, DATE_FORMAT(A.contract_start, '%d/%m/%Y') AS contract_start, DATE_FORMAT(A.contract_end, '%d/%m/%Y') AS contract_end, A.headcount, A.basic_salary, A.basic_salary2, A.bpjs_tk, A.bpjs_ks, A.labor_insurance, A.health_insurance, A.ohc, A.kaporlap, A.chemicals, A.training, A.date_invoice, A.date_payment, A.crm, A.status_registration, A.status_mou, A.files";
		
		$preview = $this->mou_model->gets($params);
		$data['preview'] = $preview[0];
		$this->load->view('mou/contract_preview', $data);
	}

	public function contract_form($type = FALSE, $contract_id = FALSE)
	{
		$this->load->model(array('mou_model', 'company_model', 'province_model', 'city_model'));
		
		$data['id']					= $contract_id;
		$data['company_name']		= '';
		$data['begining_company_id']= '';
		$data['now_company_id']		= '';
		$data['province_id']		= '';
		$data['city_id']			= '';
		$data['contract_start']		= '';
		$data['contract_end']		= '';
		$data['headcount']			= 0;
		$data['basic_salary']		= '';
		$data['basic_salary2']		= '';
		$data['bpjs_tk']			= '';
		$data['bpjs_ks']			= '';
		$data['labor_insurance']	= '';
		$data['health_insurance']	= '';
		$data['ohc']				= '';
		$data['kaporlap']			= '';
		$data['chemicals']			= '';
		$data['training']			= '';
		$data['date_invoice']		= '';
		$data['date_payment']		= '';
		$data['crm']				= '';
		$data['status']				= 'Proses';
		$data['status_registration']= '';
		$data['files']				= '';
		
		$data['status_mou']			= 'DAFTAR';
		if($type == 'recontract'){
			$data['status_mou']			= 'REKONTRAK';	
		}

		if($this->input->post()){
			$this->form_validation->set_rules('company_name', 'Nama Perusahaan', 'required');

			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
				redirect(base_url('mou/contract/'.$type));
			}else{
				$data['id']					= $this->input->post('id');
				$data['company_name']		= $this->input->post('company_name');
				$data['begining_company_id']= $this->input->post('begining_company_id');
				$data['now_company_id']		= $this->input->post('now_company_id');
				$data['province_id']		= $this->input->post('province_id');
				$data['city_id']			= $this->input->post('city_id');
				$data['contract_start']		= $this->input->post('contract_start');
				$data['contract_end']		= $this->input->post('contract_end');
				$data['headcount']			= $this->input->post('headcount');
				$data['basic_salary']		= $this->input->post('basic_salary');
				$data['basic_salary2']		= $this->input->post('basic_salary2');
				$data['bpjs_tk']			= $this->input->post('bpjs_tk');
				$data['bpjs_ks']			= $this->input->post('bpjs_ks');
				$data['labor_insurance']	= $this->input->post('labor_insurance');
				$data['health_insurance']	= $this->input->post('health_insurance');
				$data['ohc']				= $this->input->post('ohc');
				$data['kaporlap']			= $this->input->post('kaporlap');
				$data['chemicals']			= $this->input->post('chemicals');
				$data['training']			= $this->input->post('training');
				$data['date_invoice']		= $this->input->post('date_invoice');
				$data['date_payment']		= $this->input->post('date_payment');
				$data['crm']				= $this->input->post('crm');
				$data['status_registration']= $this->input->post('status_registration');
				$data['status_mou'] 		= $this->input->post('status_mou');

				$url_document = 'files/mou';
				$document_path = FCPATH.$url_document;
				$config['upload_path'] 	= $document_path;
				$config['overwrite']  	= TRUE;
				$config['allowed_types']= 'jpg|jpeg|png|pdf|doc|docx';
				if($_FILES['document']['name'] != ""){
					$config['file_name'] 	=  preg_replace("/[^a-zA-Z0-9]/", "", $data['company_name'].$data['id']);
					$this->load->library('upload', $config, 'document');
					if (!$this->document->do_upload('document')) {
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Proses upload Foto gagal.','danger'));
						redirect(base_url('mou/contract/'.$type));
					} else {
						$document 	= $this->document->data();
						$data['files'] = base_url($url_document.'/'.$document['file_name']);
					}
				}
				$this->mou_model->save($data);
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
				redirect(base_url('mou/contract/'.$type));
			}
		}
		
		if ($contract_id)
		{
			$data = (array) $this->mou_model->get(array('id' => $contract_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('mou/contract/'.$type));
			}
		}

		$data['_TITLE_']		= 'Form Kerjasama';
		$data['_PAGE_'] 		= 'mou/contract_form';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= $type;
		$data['type'] 			= $type;
		$data['list_company'] 	= $this->company_model->gets(array('columns' => 'A.id, A.code'));
		$data['list_province'] 	= $this->province_model->gets(array('columns' => 'A.id, A.name'));
		$data['list_city'] 		= $this->city_model->gets(array('columns' => 'A.id, A.name'));
		$this->view($data);
	}

	public function contract_delete($type = FALSE, $contract_id = false)
	{
		$this->load->model('mou_model');
		if ($contract_id)
		{
			$data =  $this->mou_model->get(array('id' => $contract_id, 'columns' => 'A.id'));
			if ($data)
			{
				$result = $this->mou_model->save(array('id' => $contract_id, 'is_active' => 0));
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
				redirect(base_url('mou/contract/'.$type));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('mou/contract/'.$type));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('mou/contract/'.$type));
		}
	}
}
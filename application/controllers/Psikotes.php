<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Psikotes extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role   = array_merge($role, $this->config->item('employee'));
		$role   = array_merge($role, $this->config->item('recruitment'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}
	
	public function list(){
		$data['_TITLE_'] 		= 'Psikotes Online';
		$data['_PAGE_'] 		= 'psikotes/list';
		$data['_MENU_PARENT_'] 	= 'tes_online';
		$data['_MENU_'] 		= 'psikotes';
		$this->view($data);
	}

	public function list_ajax(){
		$this->load->model(array('psikotes_model'));

		$column_index 			= $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.nama,Concat(A.usia,' Tahun') as usia,A.jk, DATE_FORMAT(A.tgl_tes, '%d/%m/%Y') AS tgl_tes,A.deskripsi_kepribadian,A.job_match";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];

		$params['nama']						= $_POST['columns'][0]['search']['value'];
		$params['usia']						= $_POST['columns'][1]['search']['value'];
		$params['jk']						= $_POST['columns'][2]['search']['value'];
		$params['tgl_tes']					= $_POST['columns'][3]['search']['value'];
		$params['deskripsi_kepribadian']	= $_POST['columns'][4]['search']['value'];
		$params['job_match']				= $_POST['columns'][5]['search']['value'];

		if($this->session->userdata('branch') > 1){
			$params['branch_id']	= $this->session->userdata('branch');
		}

		$list 	= $this->psikotes_model->gets($params);
		$total 	= $this->psikotes_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['nama'] 					= $item->nama;
			$result['usia']						= $item->usia;
			$result['jk']						= $item->jk;
			$result['tgl_tes'] 					= $item->tgl_tes;
			$result['deskripsi_kepribadian'] 	= $item->deskripsi_kepribadian;
			$result['job_match'] 				= $item->job_match;
			$result['action'] 			=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("psikotes/result/".$item->id).'">Lihat Hasil</a>'.
				//  '<a class="btn-sm btn-success btn-block" href="'.base_url("psikotes/preview/".$item->id).'">Lihat Data</a>'.
				'<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("psikotes/delete/".$item->id).'">Hapus</a>';
	
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function form()
	{
		$this->load->model(array('psikotes_model','vacancy_model'));

		$employeeId = $this->session->userdata('employee_id');
		$data['data_employee'] 			= $this->psikotes_model->getEmployeeInfo($employeeId);
		$data['list_nomor_psikotes'] 	= $this->psikotes_model->get_list_nomor_psikotes();
		$data['list_master_psikotes'] 	= $this->psikotes_model->get_list_master_psikotes();
		$data['vacancy_id'] 			= $_GET['vacancy'];
		$data['data_vacancy']			= $this->vacancy_model->getDataForTes($_GET['vacancy']);

		$data['_TITLE_'] 		= 'Psikotes Online';
		$data['_PAGE_'] 		= 'psikotes/form';
		$data['_MENU_PARENT_'] 	= 'tes_online';
		$data['_MENU_'] 		= 'psikotes';
		return $this->view($data);
	}

	public function saveform(){
		$this->load->model(array('psikotes_model','vacancy_model','history_tes_model'));
		if($this->input->post()){
			$dataVacancy 						= $this->vacancy_model->getDataForTes($this->input->post('vacancy_id'));

			$arrD = [];
			$arrPk = ["P","K","PK"];
			
			// Data Master
			$data['m_employee_id'] 	= $this->session->userdata('employee_id');
			$data['company_id'] 	= $dataVacancy[0]->company_id;
			$data['branch_id'] 		= $dataVacancy[0]->branch_id;
			$data['site_id'] 		= $dataVacancy[0]->site_id;
			$data['province_id'] 	= $dataVacancy[0]->province_id;
			$data['position_id'] 	= $dataVacancy[0]->position_id;
			$data['nama'] 			= $this->input->post('full_name');
			$data['usia']			= $this->input->post('umur');
			$data['jk']				= $this->input->post('jk');
			$data['tgl_tes']		= $this->input->post('tgl_tes');
			$data['tes_dimulai']	= $this->input->post('tes_mulai');
			$data['m_vacancy_id']	= $this->input->post('vacancy_id');
			$data['tes_selesai']	= date('Y-m-d H:i:s');
			$data['id']				= '';
			$data['nama_file']		= $this->input->post('full_name')." (".$this->input->post('jk')." - ".$this->input->post('umur')." th)"; // CONCIERGE GRAHA GOLF - ANANTA WAHYU IRYANTO (L - 24 th)

			$save_id	= $this->psikotes_model->save($data);

			// Data Tes
			$dataNomor = $this->psikotes_model->get_list_nomor_psikotes();
			foreach ($dataNomor as $key => $element) {
				$nvalp 	= 'poin_'.$element->nomor.'_p';
				$nvalk 	= 'poin_'.$element->nomor.'_k';
				$mPsip	= $this->psikotes_model->getMPsikotes($this->input->post($nvalp));
				$mPsik 	= $this->psikotes_model->getMPsikotes($this->input->post($nvalk));

				$dataDp['id']				= '';
				$dataDp['m_psikotes_id']	= $this->input->post($nvalp);
				$dataDp['nomor'] 			= $mPsip->nomor;
				$dataDp['pk']	 			= "P";
				$dataDp['poin']				= $mPsip->poin;
				$dataDp['disc']				= $mPsip->disc_p;
				$dataDp['t_psikotes_id']	= $save_id;

				$dataDk['id']				= '';
				$dataDk['m_psikotes_id']	= $this->input->post($nvalk);
				$dataDk['nomor'] 			= $mPsik->nomor;
				$dataDk['pk'] 				= "K";
				$dataDk['poin']				= $mPsik->poin;
				$dataDk['disc']				= $mPsik->disc_k;
				$dataDk['t_psikotes_id']	= $save_id;

				$save_d_p_id	= $this->psikotes_model->saveD($dataDp);
				$save_d_k_id	= $this->psikotes_model->saveD($dataDk);
				
			};

			// Data Result line
			foreach ($arrPk as $key => $element) {
				$dataLine['t_psikotes_id'] 	= $save_id;
				$dataLine['pk'] 			= $element;
				$dataLine['id']				= '';
				if ($element=="PK") {
					$dataLine['poin_d'] = $this->psikotes_model->countDiscPk($save_id,"D");
					$dataLine['poin_i'] = $this->psikotes_model->countDiscPk($save_id,"I");
					$dataLine['poin_s'] = $this->psikotes_model->countDiscPk($save_id,"S");
					$dataLine['poin_c'] = $this->psikotes_model->countDiscPk($save_id,"C");
					$dataLine['poin_*'] = null;
				}else{
					$dataLine['poin_d'] = $this->psikotes_model->countDisc($save_id,"D",$element);
					$dataLine['poin_i'] = $this->psikotes_model->countDisc($save_id,"I",$element);
					$dataLine['poin_s'] = $this->psikotes_model->countDisc($save_id,"S",$element);
					$dataLine['poin_c'] = $this->psikotes_model->countDisc($save_id,"C",$element);
					$dataLine['poin_*'] = $this->psikotes_model->countDisc($save_id,"*",$element);
				}

				$dataLine['convert_d'] = $this->psikotes_model->convertDisc("d",$element,$dataLine['poin_d']);
				$dataLine['convert_i'] = $this->psikotes_model->convertDisc("i",$element,$dataLine['poin_i']);
				$dataLine['convert_s'] = $this->psikotes_model->convertDisc("s",$element,$dataLine['poin_s']);
				$dataLine['convert_c'] = $this->psikotes_model->convertDisc("c",$element,$dataLine['poin_c']);

				$saveLineId	= $this->psikotes_model->saveResultLine($dataLine);
			}

			//data result disc
			$arrKodeDisc = ['C','D','C-D','I-D','I-D-C','I-D-S','I-S-D','S-D-C ','D-I','D-I-S','D-S ','C-I-S','C-S-I','I-S-C / I-C-S','S','C-S','S-C','D-C','D-I-C','D-S-I','D-S-C','D-C-I','D-C-S','I','I-S','I-C','I-C-D','I-C-S','S-D','S-I','S-D-I','S-I-D','S-I-C','S-C-D','S-C-I','C-I','C-D-I','C-D-S','C-I-D','C-S-D'];
			foreach ($arrPk as $key => $element) {
				$dataDisc['t_psikotes_id'] 	= $save_id;
				$dataDisc['pk'] 			= $element;
				$r							= $this->psikotes_model->getResultLine($save_id,$element);
				for ($index=0; $index < 40 ; $index++) {
					$poin = 0;
					$kodeDisc = $arrKodeDisc[$index];
					$d = $r->convert_d;
					$i = $r->convert_i;
					$s = $r->convert_s;
					$c = $r->convert_c;

					if($index==0&&$d<=0&&$i<=0&&$s<=0&&$c>0){
						$poin=1;
					}else if($index==1&&$d>0&&$i<=0&&$s<=0&&$c<=0){
						$poin=1;
					}else if($index==2&&$d>0&&$i<=0&&$s<=0&&$c>0&&$c>=$d){
						$poin=1;
					}else if($index==3&&$d>0&&$i>0&&$s<=0&&$c<=0&&$i>=$d){
						$poin=1;
					}else if($index==4&&$d>0&&$i>0&&$s<=0&&$c>0&&$i>=$d&&$d>=$c){
						$poin=1;
					}else if($index==5&&$d>0&&$i>0&&$s>0&&$c<=0&&$i>=$d&&$d>=$s){
						$poin=1;
					}else if($index==6&&$d>0&&$i>0&&$s>0&&$c<=0&&$i>=$s&&$s>=$d){
						$poin=1;
					}else if($index==7&&$d>0&&$i<=0&&$s>0&&$c>0&&$s>=$d&&$d>=$c){
						$poin=1;
					}else if($index==8&&$d>0&&$i>0&&$s<=0&&$c<=0&&$d>=$i){
						$poin=1;
					}else if($index==9&&$d>0&&$i>0&&$s>0&&$c<=0&&$d>=$i&&$i>=$s){
						$poin=1;
					}else if($index==10&&$d>0&&$i<=0&&$s>0&&$c<=0&&$d>=$s){
						$poin=1;
					}else if($index==11&&$d<=0&&$i>0&&$s>0&&$c>0&&$c>=$i&&$i>=$s){
						$poin=1;
					}else if($index==12&&$d<=0&&$i>0&&$s>0&&$c>0&&$c>=$s&&$s>=$i){
						$poin=1;
					}else if($index==13&&$d<=0&&$i>0&&$s>0&&$c>0&&$i>=$s&&$i>=$c){
						$poin=1;
					}else if($index==14&&$d<=0&&$i<=0&&$s>0&&$c<=0){
						$poin=1;
					}else if($index==15&&$d<=0&&$i<=0&&$s>0&&$c>0&&$c>=$s){
						$poin=1;
					}else if($index==16&&$d<=0&&$i<=0&&$s>0&&$c>0&&$s>=$c){
						$poin=1;
					}else if($index==17&&$i<=0&&$s<=0&&$d>0&&$c>0&&$d>=$c){
						$poin=1;
					}else if($index==18&&$d>0&&$i>0&&$c>0&&$s<=0&&$d>=$i&&$i>=$c){
						$poin=1;
					}else if($index==19&&$d>0&&$s>0&&$i>0&&$c<=0&&$d>=$s&&$s>=$i){
						$poin=1;
					}else if($index==20&&$d>0&&$s>0&&$c>0&&$i<=0&&$d>=$s&&$s>=$c){
						$poin=1;
					}else if($index==21&&$d>0&&$i>0&&$c>0&&$s<=0&&$d>=$c&&$c>=$i){
						$poin=1;
					}else if($index==22&&$d>0&&$s>0&&$c>0&&$i<=0&&$d>=$c&&$c>=$s){
						$poin=1;
					}else if($index==23&&$d<=0&&$s<=0&&$c<=0&&$i>0){
						$poin=1;
					}else if($index==24&&$i>0&&$s>0&&$d<=0&&$c<=0&&$i>=$s){
						$poin=1;
					}else if($index==25&&$i>0&&$c>0&&$d<=0&&$s<=0&&$i>=$c){
						$poin=1;
					}else if($index==26&&$d>0&&$i>0&&$c>0&&$s<=0&&$i>=$c&&$c>=$d){
						$poin=1;
					}else if($index==27&&$d<=0&&$i>0&&$s>0&&$c>0&&$i>=$c&&$c>=$s){
						$poin=1;
					}else if($index==28&&$d>0&&$i<=0&&$s>0&&$c<=0&&$s>=$d){
						$poin=1;
					}else if($index==29&&$i>0&&$s>0&&$d<=0&&$c<=0&&$s>=$i){
						$poin=1;
					}else if($index==30&&$d>0&&$i>0&&$s>0&&$c<=0&&$s>=$d&&$d>=$i){
						$poin=1;
					}else if($index==31&&$d>0&&$i>0&&$s>0&&$c<=0&&$s>=$i&&$i>=$d){
						$poin=1;
					}else if($index==32&&$i>0&&$s>0&&$c>0&&$d<=0&&$s>=$i&&$i>=$c){
						$poin=1;
					}else if($index==33&&$d>0&&$i<=0&&$s>0&&$c>0&&$s>=$c&&$c>=$d){
						$poin=1;
					}else if($index==34&&$i>0&&$s>0&&$c>0&&$d<=0&&$s>=$c&&$c>=$i){
						$poin=1;
					}else if($index==35&&$i>0&&$c>0&&$d<=0&&$s<=0&&$c>=$i){
						$poin=1;
					}else if($index==36&&$d>0&&$i>0&&$c>0&&$s<=0&&$c>=$d&&$d>=$i){
						$poin=1;
					}else if($index==37&&$d>0&&$s>0&&$c>0&&$i<=0&&$c>=$d&&$d>=$s){
						$poin=1;
					}else if($index==38&&$d>0&&$i>0&&$c>0&&$s<=0&&$c>=$i&&$i>=$d){
						$poin=1;
					}else if($index==39&&$d>0&&$s>0&&$c>0&&$i<=0&&$c>=$s&&$s>=$d){
						$poin=1;
					}

					$dataDisc['id']			= '';
					$dataDisc['kode_disc'] 	= $kodeDisc;
					$dataDisc['poin'] 		= $poin;
					$dataDisc['urutan'] 	= $index+1;

					$saveLineId	= $this->psikotes_model->saveResultDisc($dataDisc);
				}
			}

			//simpulan
			$deskripsi = "";
			$jobMatch = "";
			foreach ($arrPk as $key => $element) {
				//ambil poin dari result disc
				$poin = $this->psikotes_model->getResultPoinDisc($save_id,$element);

				//ambil result dari poin
				$result = $this->psikotes_model->getResultFromPoin($poin);
				if ($result == null) {
					$dataResult['kepribadian'] 					= "";
					$dataResult['kepribadian_detail'] 			= "";
					$dataResult['m_psikotes_result_disc_id'] 	= null;
				}else{
					$dataResult['kepribadian'] 					= $result->karakter;
					$dataResult['kepribadian_detail'] 			= $result->karakter_detail;
					$dataResult['m_psikotes_result_disc_id'] 	= $result->id;
				}
				$dataResult['id'] 					= '';
				$dataResult['t_psikotes_id'] 		= $save_id;
				$dataResult['pk'] 					= $element;

				if ($element=="PK") {
					if ($result != null) {
						$deskripsi 	= $result->deskripsi_kepribadian;
						$jobMatch 	= $result->job_match;
					}
				}
				$saveResultId	= $this->psikotes_model->saveResult($dataResult);
			}

			//update deskripsi dan jobmatch
			$dataUpdate['deskripsi_kepribadian']		= $deskripsi;
			$dataUpdate['job_match']					= $jobMatch;
			$dataUpdate['id']							= $save_id;
			$updateId	= $this->psikotes_model->updateDeskripsi($dataUpdate);
			
			//save history
			$history['tes']					='psikotes';
			$history['doc_id']				=$save_id;
			$history['company_id']			=$data['company_id'] ;
			$history['branch_id']			=$data['branch_id'] ;
			$history['site_id']				=$data['site_id'] ;
			$history['m_employee_id']		=$data['m_employee_id'] ;
			$history['province_id']			=$data['province_id'] ;
			$history['position_id']			=$data['position_id'] ;
			$history['m_vacancy_id']		=$data['m_vacancy_id'] ;
			$history['nama']				=$data['nama'] ;
			$history['usia']				=$data['usia'] ;
			$history['jk']					=$data['jk'] ;
			$history['tgl_tes']				=$data['tgl_tes'] ;
			$history['tes_dimulai']			=$data['tes_dimulai'] ;
			$history['tes_selesai']			=$data['tes_selesai'] ;
			$history_id						= $this->history_tes_model->save($history);


			if (!$save_id) {
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));		
				redirect(base_url('appeal/applied'));
				die();
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));		
			redirect(base_url('appeal/applied'));
		}
	}

	public function result ($psikotes_id){
		$this->load->model(array('psikotes_model'));
		$data['data_psikotes']				= $this->psikotes_model->get_data_psikotes($psikotes_id);
		
		if (empty($data['data_psikotes'])){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data DISC tidak ditemukan.','danger'));
			redirect(base_url('vacancy/applicant'));
		}

		$data['data_psikotes_d']			= $this->psikotes_model->get_data_psikotes_d($psikotes_id);
		$data['data_psikotes_result']		= $this->psikotes_model->get_data_psikotes_result($psikotes_id);
		$data['data_psikotes_result_disc'] 	= $this->psikotes_model->get_data_psikotes_result_disc($psikotes_id);
		$data['data_psikotes_result_line'] 	= $this->psikotes_model->get_data_psikotes_result_line($psikotes_id);
		$data['data_employee'] 				= $this->psikotes_model->getEmployeeInfo($data['data_psikotes'][0]->m_employee_id);

		//update petugas
		$data['petugas']				= $this->session->userdata('name');

		$data['_TITLE_'] 		= 'Hasil Psikotes '.$data['data_psikotes'][0]->nama_file;
		$data['_PAGE_'] 		= 'psikotes/result';
		$data['_MENU_PARENT_'] 	= 'tes_online';
		$data['_MENU_'] 		= 'psikotes';
		return $this->view($data);
	}
	
	public function delete($psikotes_id = false)
	{
		$this->load->model('psikotes_model');
		if ($psikotes_id)
		{
			$data =  $this->psikotes_model->get_data_psikotes($psikotes_id);
			if (!empty($data))
			{
				$dataUpdate['id'] = $psikotes_id;
				$result 	= $this->psikotes_model->delete($dataUpdate);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('psikotes/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal dihapus.','danger'));
					redirect(base_url('psikotes/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('psikotes/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('psikotes/list'));
		}
	}

}
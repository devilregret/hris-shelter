<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Employee extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		$role 	= array_merge($role, $this->config->item('relation_officer_security'));
		$role 	= array_merge($role, $this->config->item('benefit'));
		$role   = array_merge($role, $this->config->item('directur'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function preview($employee_id=FALSE)
	{
		$this->load->model(array('employee_model','employee_education_model', 'employee_emergency_model', 'employee_history_model', 'employee_language_model', 'employee_reference_model', 'employee_skill_model'));
		$data['_TITLE_'] 		= 'Preview Karyawan';
		$data['_PAGE_']	 		= 'employee/preview';
		$data['_MENU_PARENT_'] 	= 'personalia';
		$data['_MENU_'] 		= 'employee';

		$data['id'] = $employee_id;

		if (!$employee_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('employee/list'));
		}

		$data['list_education'] = $this->employee_education_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.institute, A.start, A.end'));
		$data['list_emergency'] = $this->employee_emergency_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.relation, A.phone, A.address'));
		$data['list_history'] 	= $this->employee_history_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.company_name, A.position, A.period,  A.salary, A.company_address, A.resign_note'));
		$data['list_language'] 	= $this->employee_language_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.language, A.verbal, A.write'));
		$data['list_reference'] = $this->employee_reference_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.company_name, A.position, A.phone'));
		$data['list_skill'] 	= $this->employee_skill_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));

		$data['preview'] = $this->employee_model->preview(array('id' => $employee_id));
		$data['preview']->date_birth = format_date_ina($data['preview']->date_birth);
		$data['preview']->city_birth = str_replace(array('Kota', 'Kabupaten'), '', $data['preview']->city_birth);
		$this->load->view('employee/preview', $data);
	}

	public function list($type = "", $company = "", $branch = ""){
		if($type == ""){
			redirect(base_url());
		}
		$data['_TITLE_'] 		= 'Karyawan';
		$data['_PAGE_'] 		= 'employee/list';
		$data['_MENU_PARENT_'] 	= 'employee';
		$data['_MENU_'] 		= $type;
		$data['type'] 			= $type;
		$data['company'] 		= $company;
		$data['branch'] 		= $branch;
		
		$this->view($data);
	}

	public function list_ajax($type = FALSE, $company = "", $branch =""){
		$this->load->model(array('employee_model'));

		$column_index = $_POST['order'][0]['column']; 
		


		$params['columns'] 		= "A.id, A.employee_number, A.full_name, B.code AS employee_company, C.name AS employee_branch, D.name AS position_name, E.name AS site_name, F.code AS site_company, G.name AS site_branch";

		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']		= 1;
		$params['status_nonjob'] 	= 0;

		$params['employee_number']	= $_POST['columns'][1]['search']['value'];
		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['employee_company']	= $_POST['columns'][4]['search']['value'];
		$params['site_name']		= $_POST['columns'][5]['search']['value'];
		$params['site_company']		= $_POST['columns'][6]['search']['value'];
		$params['position_name']	= $_POST['columns'][7]['search']['value'];

		$params['status_approval'] 	= 3;
		if($type == 'direct'){
			$params['not_site_id']		= 2;
			$params['site_branch']		= $_POST['columns'][3]['search']['value'];
		}else{
			$params['site_id']			= 2;
			$params['employee_branch']	= $_POST['columns'][3]['search']['value'];
		}

		if($company != ""){
			$params['business_code'] = $company;
		}

		if($branch != ""){
			$params['branch_id'] = $branch;
		}

		$list 	= $this->employee_model->employee_gets($params);
		$total 	= $this->employee_model->employee_gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['no'] 					= $i;
			$result['employee_number']		= $item->employee_number;
			$result['full_name']			= $item->full_name;
			if($type == 'direct'){
				$result['branch_name']		= $item->site_branch;
			}else{
				$result['branch_name']		= $item->employee_branch;
			}
			$result['employee_company']		= $item->employee_company;
			$result['site_name']			= $item->site_name;
			$result['position_name']		= $item->position_name;
			$result['site_company']			= $item->site_company;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("employee/preview/".$item->id).'">Lihat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function submission($type = FALSE){
		$this->load->model(array('site_model', 'position_model', 'company_model'));
		$data['_TITLE_'] 		= 'Pengajuan kandidat '.$type;
		$data['_PAGE_'] 		= 'employee/submission';
		$data['_MENU_PARENT_'] 	= $type;
		$data['_MENU_'] 		= 'submission_'.$type;

		$branch_id 	= '';
		if($this->session->userdata('branch') > 1){
			$branch_id = $this->session->userdata('branch');
		}

		$data['list_company'] 	= $this->company_model->gets(array('columns' => 'A.id, A.name, A.code', 'orderby' => 'A.name', 'order' => 'ASC'));
		$data['list_position'] 	= $this->position_model->gets(array());
		
		if($type == 'direct'){
			$data['type']			= $type;
			$data['list_site'] 		= $this->site_model->gets(array('orderby' => 'A.name', 'order' => 'ASC', 'columns' => 'A.id, A.name', 'branch_id' => $branch_id));
		}else{
			$data['type']			= $type;
			$data['list_site'] 		= $this->site_model->gets(array('orderby' => 'A.name', 'id' => 2, 'indirect' => TRUE, 'order' => 'ASC', 'columns' => 'A.id, A.name', 'branch_id' => $branch_id));
		}

		$this->view($data);
	}

	public function submission_ajax($type = FALSE){
		$this->load->model(array('candidate_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.registration_number, A.full_name, A.id_card, FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25) AS age, A.education_level, A.education_majors, B.name AS city_domisili, GROUP_CONCAT(C.certificate_name) AS certificate, I.name AS site_name, J.name AS company_name, K.name AS position, N.full_name AS submitted_by, DATE_FORMAT(A.placement_date, '%d/%m/%Y') AS placement_date, A.document_cv";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		$params['status_approval'] 	= 2;
		if($type == 'direct'){
			$params['not_site_id']		= 2;
		}else{
			$params['site_id']		= 2;
		}
		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['id_card']			= $_POST['columns'][3]['search']['value'];
		$params['age']				= $_POST['columns'][4]['search']['value'];
		$params['education_level']	= $_POST['columns'][5]['search']['value'];
		$params['education_majors']	= $_POST['columns'][6]['search']['value'];
		$params['city_domisili']	= $_POST['columns'][7]['search']['value'];
		$params['certificate']		= $_POST['columns'][8]['search']['value'];
		$params['site_name']		= $_POST['columns'][9]['search']['value'];
		$params['company_name']		= $_POST['columns'][10]['search']['value'];
		$params['position']			= $_POST['columns'][11]['search']['value'];
		$params['placement_date']	= $_POST['columns'][12]['search']['value'];
		$params['submitted_by']		= $_POST['columns'][13]['search']['value'];

		$list 	= $this->candidate_model->gets($params);
		$total 	= $this->candidate_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['full_name']			= $item->full_name;
			$result['id_card']				= $item->id_card;
			$result['age']					= $item->age;
			$result['education_level']		= $item->education_level;
			$result['education_majors']		= $item->education_majors;
			$result['city_domisili']		= $item->city_domisili;
			$result['certificate']			= $item->certificate;
			$result['site_name']			= $item->site_name;
			$result['company_name']			= $item->company_name;
			$result['position']				= $item->position;
			$result['placement_date']		= $item->placement_date;
			$result['submitted_by']			= $item->submitted_by;
			$result['document_cv']			= '';
			if($item->document_cv){
				$result['document_cv']		= '<span class="text-success"><a href="'.$item->document_cv.'">Curriculum Vitae (CV)</a></span>';
			}
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("employee/preview/".$item->id).'">Lihat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function approval(){
		$this->load->model(array('employee_model', 'site_model'));

		$id 		= $this->input->post('id');
		$action	 	= $this->input->post('action');
		$site_id	= $this->input->post('site_id');
		$position_id= $this->input->post('position_id');
		$company_id	= $this->input->post('company_id');
		$note		= $this->input->post('note');
		$type = 'direct';
		if($site_id == '2'){
			$type = 'indirect';
		}

		foreach ($id as $candidate_id) {
			$this->load->model(array('employee_model', 'approval_model', 'employee_salary_model'));
			$data = (array) $this->employee_model->get(array('id' => $candidate_id, 'columns' => 'A.id, A.site_id, A.company_id, A.position_id, A.status_nonjob'));

			if (empty($data)){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('employee/submission/'.$type));
			}else{
				$status_approval = 'Diterima';
				$employee_number = '';
				if($action == 'reject'){
					$status_approval = 'Ditolak';
					$site_id = 1;
					$position_id = 1;
					$company_id = 1;
					
					$insert = array('id' => $candidate_id, 'status_approval' => 0, 'status' => 0, 'site_id' => $site_id, 'company_id' => $company_id, 'position_id' => $position_id);
					if($data['status_nonjob'] == 1){
						$insert['status_approval'] = 'Diterima';
					}
					$result = $this->employee_model->save($insert);
				}else{
					if($site_id == ''){
						$site_id = $data['site_id'];
					}
					if($position_id == ''){
						$position_id = $data['position_id'];
					}
					if($company_id == ''){
						$company_id = $data['company_id'];
					}

					$site =  $this->site_model->get(array('id' => $site_id, 'columns' => 'A.id, A.company_id'));
					$employee_number = $this->employee_number($site->company_id);
					$insert = array('id' => $candidate_id, 'status_approval' => 3, 'status' => 1, 'site_id' => $site_id, 'company_id' => $company_id, 'position_id' => $position_id, 'employee_number' => $employee_number, 'approved_at' => date('Y-m-d H:i:s'), 'approved_by' => $this->session->userdata('user_id'), 'resign_submit' => null);
					if($data['status_nonjob'] == 1){
						$insert['status_nonjob'] 	= 0;
					}
					
					$result = $this->employee_model->save($insert);
				}

				$approval = array('id' => '', 'employee_id' => $data['id'] , 'site_id' => $site_id, 'company_id' => $company_id, 'position_id' => $position_id, 'status_approval' => $status_approval, 'note' => $note);
				$this->approval_model->save($approval);

				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pengajuan gagal.','danger'));
					redirect(base_url('employee/submission/'.$type));
				}
			}
		}
		if($action == 'reject'){
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Calon karyawan berhasil ditolak.','success'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Calon karyawan berhasil diterima.','success'));
		}
		redirect(base_url('employee/submission/'.$type));
	}

	private function employee_number($site_id){
		$employee_number = "";
		$this->load->model('employee_model');

		$employee_number = $site_id.date('ym');

		$site = $this->employee_model->last_employee_number(array('prefix' => $employee_number));

		$inc = 1;
		if($site){
			// $inc = intval(substr($site->employee_number, 13, 5))+1;
			$inc = intval(substr($site->employee_number, 5, 5))+1;
		}		
		$space = "";
		for ($i=strlen($inc); $i <5; $i++) {
			$space .= "0";
		}

		$employee_number = $employee_number.$space.$inc;		
		return $employee_number;
	}

	private function generate_PDF($data = array())
	{	
		$this->load->library("Pdf");
		ob_start();
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// $pdf->SetProtection(array('print', 'copy'), $data['email']->pdf_password, null, 0, null);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shelter');
		$pdf->SetTitle($data['title']);
		$pdf->SetSubject($data['title']);
		$pdf->SetKeywords('Shelter');

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$pdf->SetFont('times', '', 10);
		$pdf->AddPage();
		$pdf->writeHTML($data['content'], true, false, true, false, '');

		$base_path  = $data['basepath'];
		if(!is_dir($base_path)){
			mkdir($base_path, 0777, true);
		}
		return $pdf->Output($base_path.strtolower(str_replace(" ", "_",$data['title'])).'.pdf', 'F');
	}

	public function export()
	{
		$this->load->model(array('employee_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = "Data Karyawan";
		$params = $this->input->get();

		$params['status'] = 1;
		$params['status_approval'] 	= 3;
		$params['status_nonjob'] 	= 0;
		$title = 'kontrak_karyawan_'.$params['type'];
		$files = glob(FCPATH."files/".$title."/*");
		
		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$type = $params['type'];
		if(isset($params['type'])){
			if($params['type'] == 'indirect'){
				$params['site_id'] = 2;
			}else{
				$params['not_site_id']		= 2;
			}
			unset($params['type']);
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("B".$i, "NIK KTP");
		$excel->getActiveSheet()->setCellValue("C".$i, "Jenis Kelamin");
		$excel->getActiveSheet()->setCellValue("D".$i, "Tempat Lahir");
		$excel->getActiveSheet()->setCellValue("E".$i, "Tanggal Lahir");
		$excel->getActiveSheet()->setCellValue("F".$i, "Alamat Lengkap");
		$excel->getActiveSheet()->setCellValue("G".$i, "Nomor BPJS TK");
		$excel->getActiveSheet()->setCellValue("H".$i, "Nomor BPJS KS");
		$excel->getActiveSheet()->setCellValue("I".$i, "Jabatan");
		$excel->getActiveSheet()->setCellValue("J".$i, "Nama Site");
		$excel->getActiveSheet()->setCellValue("K".$i, "Alamat Site");
		$excel->getActiveSheet()->setCellValue("L".$i, "Branch");

		$params['columns'] 		= 'A.full_name, A.id_card, A.gender, A.benefit_labor, A.benefit_health, A.date_birth, D.name AS position_name, E.name AS site_name, F.address AS site_address, H.name AS city_birth, I.name AS city_domisili, C.name AS employee_branch, G.name AS site_branch';

		$list_candidate = $this->employee_model->employee_gets($params);
		$i=2;
		foreach ($list_candidate as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->gender);
			$excel->getActiveSheet()->setCellValueExplicit("D".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_birth));
			$date = new DateTime($item->date_birth);
			$excel->getActiveSheet()->setCellValue("E".$i, PHPExcel_Shared_Date::PHPToExcel($date));
			$excel->getActiveSheet()->getStyle("E".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			$excel->getActiveSheet()->setCellValue("F".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_domisili));
			$excel->getActiveSheet()->setCellValueExplicit("G".$i, $item->benefit_labor);
			$excel->getActiveSheet()->setCellValueExplicit("H".$i, $item->benefit_health);
			$excel->getActiveSheet()->setCellValueExplicit("I".$i, $item->position_name);
			$excel->getActiveSheet()->setCellValueExplicit("J".$i, $item->site_name);
			$excel->getActiveSheet()->setCellValueExplicit("K".$i, $item->site_address);
			if($type == 'indirect'){
				$excel->getActiveSheet()->setCellValueExplicit("L".$i, $item->employee_branch);
			}else{
				$excel->getActiveSheet()->setCellValueExplicit("L".$i, $item->site_branch);
			}
			$i++;
		}
		$excel->getActiveSheet()->setTitle('Daftar Karyawan');	
		$excel->setActiveSheetIndex(0);
		$sheet = $excel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells( true );
		foreach( $cellIterator as $cell ) {
			$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		}
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}
}
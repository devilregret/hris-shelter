<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Information extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('recruitment'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function delete($information_id = false)
	{
		$this->load->model('information_model');
		if ($information_id)
		{
			$data =  $this->information_model->get(array('id' => $information_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $information_id, 'is_active' => 0);
				$result = $this->information_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('information/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('information/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('information/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('information/list'));
		}
	}

	public function preview($information_id=FALSE)
	{
		$this->load->model('information_model');
		$data['_TITLE_'] 		= 'Preview Informasi';
		$data['_PAGE_']	 		= 'information/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'information';

		$data['id'] = $information_id;

		
		if (!$information_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('information/list'));
		}

		$data['preview'] = $this->information_model->preview(array('id' => $information_id));
		$this->load->view('information/preview', $data);
	}

	public function form($information_id = FALSE)
	{
		$this->load->model(array('information_model', 'branch_model'));

		$data['id'] 		= '';
		$data['branch_id']	= $params['session_branch']	= $this->session->userdata('branch');
		$data['title']		= '';
		$data['content'] 	= '';
		$data['start_date'] = '';
		$data['end_date'] 	= '';

		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['branch_id'] 	= $this->input->post('branch_id');
			$data['title'] 		= $this->input->post('title');
			$data['content'] 	= $this->input->post('content');
			$data['start_date'] = $this->input->post('start_date');
			$data['end_date']	= $this->input->post('end_date');
			
			$this->form_validation->set_rules('title', '', 'required');
			$this->form_validation->set_rules('content', '', 'required');
			$this->form_validation->set_rules('start_date', '', 'required');
			$this->form_validation->set_rules('end_date', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				
				$save_id	= $this->information_model->save($data);
				
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('information/list'));
		}

		if ($information_id)
		{
			$data = (array) $this->information_model->get(array('id' => $information_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('information/list'));
			}
		}

		$data['list_branch'] 	= $this->branch_model->gets(array());

		$data['_TITLE_'] 		= 'Informasi';
		$data['_PAGE_'] 		= 'information/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'information';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Informasi';
		$data['_PAGE_'] 		= 'information/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'information';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('information_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.title, DATE_FORMAT(A.start_date, '%Y-%m-%d') AS start_date, DATE_FORMAT(A.end_date , '%Y-%m-%d') AS end_date, B.name AS branch_name ";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['title']		= $_POST['columns'][1]['search']['value'];
		$params['start_date']	= $_POST['columns'][2]['search']['value'];
		$params['end_date']		= $_POST['columns'][3]['search']['value'];
		$params['branch_name']	= $_POST['columns'][4]['search']['value'];

		if($this->session->userdata('branch') > 1){
			$params['session_branch']	= $this->session->userdata('branch');
		}
		
		$list 	= $this->information_model->gets($params);
		$total 	= $this->information_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['title'] 		= $item->title;
			$result['start_date'] 	= $item->start_date;
			$result['end_date']		= $item->end_date;
			$result['branch_name']	= $item->branch_name;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("information/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("information/form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("information/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}
}
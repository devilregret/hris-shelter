<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payroll extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function site(){
		if($this->input->post('site_id')){
			$this->session->set_userdata('site',$this->input->post('site_id'));
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Site sudah dipilih.','success'));
		redirect(base_url('payroll/list'));
	}

	public function process($type = 'list'){
		$this->load->model(array('payroll_model', 'position_model', 'employee_model'));
		$process_type = $this->input->post('process_type');
		if($process_type == 'pdf'){

			$files = glob(FCPATH."files/slip_gaji/*");
			foreach($files as $file){
				gc_collect_cycles();
				if(is_file($file)) {
					unlink($file);
				}
			}
			$data['basepath'] = FCPATH."files/slip_gaji/";

			foreach ($this->input->post('id') as $payroll_id)
			{
				$income			= $this->payroll_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'pendapatan', 'columns' => 'A.name, A.value'));
				$outcome		= $this->payroll_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'potongan', 'columns' => 'A.name, A.value'));
				$payroll		= $this->payroll_model->get(array('id' => $payroll_id, 'columns' => 'A.id, A.employee_id , A.periode_start, A.periode_end, A.salary, A.overtime_hour, A.overtime_calculation, A.attendance, A.basic_salary, A.bpjs_ks, A.bpjs_jht, A.bpjs_jp, A.tax_calculation, A.salary_note'));
				$employee		= $this->employee_model->get(array('id' => $payroll->employee_id,'columns' => 'A.email, A.id_card, A.full_name, A.bank_account, A.employee_number, A.position_id, B.name AS site_name, C.code AS company_code'));
				$position		= $this->position_model->get(array('id' => $employee->position_id,'columns' => 'A.name, B.name AS company_name'));
				
				$start_date = substr($payroll->periode_start, 8, 2).' '.get_month(substr($payroll->periode_start, 5, 2));
				$end_date 	= substr($payroll->periode_end, 8, 2).' '.get_month(substr($payroll->periode_end, 5, 2)).' '.substr($payroll->periode_end, 0, 4);

				$html  = '<table width="100%">';
				$html .= '<tr><td width="25%">PERIODE BULAN</td><td> : '.$start_date.' - '.$end_date.'</td></tr>
						<tr><td>NO REKENING</td><td> : '.$employee->bank_account.'</td></tr>
						<tr><td>NAMA</td><td> : '.$employee->full_name.'</td></tr>
						<tr><td>NOMOR KARYAWAN</td><td> : '.$employee->employee_number.'</td></tr>
						<tr><td>JABATAN</td><td> : '.$position->name.'</td></tr>
						<tr><td>SITE</td><td> : '.$employee->site_name.'</td></tr>';
				if($payroll->attendance){
					$html .= '<tr><td>JUMLAH KEHADIRAN</td><td> : '.$payroll->attendance.'</td></tr>';
				}
				if($payroll->overtime_hour){
					$html .= '<tr><td>JUMLAH JAM LEMBUR</td><td> : '.$payroll->overtime_hour.'</td></tr>';
				}
				$html .= '</table><table width="100%">';
				$html .= '<tr><td width="60%">&nbsp;</td><td><strong>Gaji Bersih diterima : '.format_rupiah($payroll->salary).'</strong></td></tr>';
				$html .= '</table><br><br><table width="100%">';
				$html .= '<tr><td width="50%"><table>';
				$html .= '<tr><td><strong>Pendapatan</strong></td><td></td><td></td></tr>';
				if($payroll->basic_salary){
					$html .= "<tr><td>Gaji Pokok</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->basic_salary)."</td></tr>";
				}else{
					$html .= "<tr><td>Gaji Pokok</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
				}
				if($payroll->overtime_calculation){
					$html .= "<tr><td>Lembur</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->overtime_calculation)."</td></tr>";
				}
				foreach( $income AS $item):
					$html .= "<tr><td >".$item->name."</td><td>: Rp.</td><td style='float:right'>".rupiah($item->value)."</td></tr>";
				endforeach;
				$html .= '</table></td>';
				$html .= '<td width="50%"><table>';
				$html .= '<tr><td><strong>Potongan</strong></td><td></td><td></td></tr>';
				if($payroll->bpjs_ks){
					$html .= "<tr><td>BPJS Kesehatan</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->bpjs_ks)."</td></tr>";
				}else{
					$html .= "<tr><td>BPJS Kesehatan</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";	
				}
				if($payroll->bpjs_jht){
					$html .= "<tr><td>BPJS Ketenagakerjaan</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->bpjs_jht)."</td></tr>";
				}else{
					$html .= "<tr><td>BPJS Ketenagakerjaan</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
				}
				
				if($payroll->tax_calculation){
					$html .= "<tr><td>PPH21</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->tax_calculation)."</td></tr>";
				}else{
					$html .= "<tr><td>PPH21</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
				}
				foreach($outcome AS $item):
					$html .= "<tr><td >".$item->name."</td><td>: Rp.</td><td style='float:right'>".rupiah($item->value)."</td></tr>";
				endforeach;
				$html .= '</table></td></tr>';
				$html .= '</table>';
				$html .= '<table width="100%"><tr><td><strong>Catatan : </strong>'.$payroll->salary_note.'</td></tr></table><br>';
				$data['content'] 	= $html; 
				$data['title'] 		=  preg_replace("/[^A-Za-z0-9 -]/", "", strtolower($employee->full_name."-".$employee->id_card));
				$data['image']		= strtolower($employee->company_code).'.jpg';
				$data['company']	= $position->company_name; 
				$data['password']	= $employee->id_card; 
				$this->generate_PDF($data);
			}

			$rootPath = FCPATH."files/slip_gaji/";
			$zip = new ZipArchive();
			$zip->open('files/slip_gaji.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);
			$files = new RecursiveIteratorIterator(
				new RecursiveDirectoryIterator($rootPath),
				RecursiveIteratorIterator::LEAVES_ONLY
			);

			foreach ($files as $name => $file)
			{
				if (!$file->isDir())
				{
					$filePath = $file->getRealPath();
					$relativePath = substr($filePath, strlen($rootPath));
					$zip->addFile($filePath, $relativePath);
				}
			}
			$zip->close();
			redirect(base_url('files/slip_gaji.zip'));
			die();
		}else{
			foreach ($this->input->post('id') as $payroll_id)
			{
				$data =  $this->payroll_model->get(array('id' => $payroll_id, 'columns' => 'A.id'));
				if (empty($data)){
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
					redirect(base_url('ropayroll/'.$type));
				}else{
					$note 		= $this->input->post('note');
					if($process_type == 'submission'){
						$insert = array('id' => $payroll_id, 'payment' => 'Selesai', 'note' => $note);	
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diproses.','success'));
			
					}else if($process_type == 'cancel'){
						$insert = array('id' => $payroll_id, 'payment' => 'Ditolak', 'note' => $note);	
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil dibatalkan.','success'));
					}
					$result = $this->payroll_model->save($insert);

					if (!$result) {
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal diproses.','danger'));
						redirect(base_url('payroll/'.$type));
					}
				}
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diproses.','success'));
		redirect(base_url('payroll/'.$type));
	}

	public function list($view = 'last', $config_id = FALSE){
		
		$this->load->model(array('payroll_config_model', 'site_model', 'payroll_model', 'employee_model'));
		$data['_TITLE_'] 		= 'Payroll Karyawan';
		$data['_PAGE_'] 		= 'payroll/list';
		$data['_MENU_PARENT_'] 	= 'payroll_employee';
		$data['_MENU_'] 		= 'payroll';
		$data['site_id']		= $this->session->userdata('site');
		$data['list_config']	= $this->payroll_config_model->gets(array('site_id' => $data['site_id'], 'columns' => 'A.id, A.config_name'));
		$data['view']			= $view;
		$config 				= array('columns' => 'A.id, A.name, A.address, B.id AS config_id, B.config_name, B.site_id, B.row_title, B.row_start, B.row_end, B.column_employee, B.column_income, B.column_outcome, B.column_net, B.column_attendance, B.column_basic_salary, B.column_overtime_hour, B.column_overtime_calculation, B.column_bpjs_jht, B.column_bpjs_jp, B.column_bpjs_ks, B.column_tax_calculation, B.column_note, B.column_bpjs_ks_company, B.column_bpjs_jp_company, B.column_bpjs_jht_company');
		
		if($config_id){
			if($config_id == 'new'){
				$config['site_id'] = 1;
			}else{
				$config['id']	= $config_id;
			}
		}else{
			$config['site_id']	= $data['site_id'];
		}
		$data['site']	= $this->payroll_config_model->get($config);

		$last_payroll 		= $this->payroll_model->last_payroll(array('site_id' => $data['site_id'], 'payment' => 'Selesai', 'columns' => 'MAX(A.periode_start) AS last_payroll'));
		if($last_payroll){
			$data['resume_payroll'] = $this->payroll_model->last_payroll(array('site_id' => $data['site_id'], 'preview' => $last_payroll->last_payroll, 'payment' => 'Selesai', 'columns' => 'COUNT(A.employee_id) AS employee_payroll, SUM(A.salary) AS summary_payroll, MAX(A.periode_start) AS periode_start, MAX(A.periode_end) AS periode_end'));
		}

		$last_approval 		= $this->payroll_model->last_payroll(array('site_id' => $data['site_id'], 'payment' => 'Menunggu', 'columns' => 'MAX(A.periode_start) AS last_approval'));
		if($last_approval){
			$data['resume_approval'] = $this->payroll_model->last_payroll(array('site_id' => $data['site_id'], 'preview' => $last_approval->last_approval, 'payment' => 'Menunggu', 'columns' => 'COUNT(A.employee_id) AS employee_payroll, SUM(A.salary) AS summary_payroll, MAX(A.periode_start) AS periode_start, MAX(A.periode_end) AS periode_end'));
		}
		
		$params['status']		= 1;
		$params['status_approval'] 		= 3;
		$params['site_id'] 		= $data['site_id'];
		$data['employee']		= $this->employee_model->gets($params, TRUE);

		$branch_id				= '';
		if($this->session->userdata('branch') > 1){
			$branch_id 	= $this->session->userdata('branch');
		}
		if($this->session->userdata('branch') > 1){
			$data['list_site']		= $this->site_model->gets(array('columns' => 'A.id, A.name, C.code AS company_code', 'branch_id' => $branch_id));
		}else if(in_array($this->session->userdata('role'), array(2,15,16,17,18,19,20,21))){
			$data['list_site']		= $this->site_model->gets(array('columns' => 'A.id, A.name, C.code AS company_code', 'indirect' => true, 'branch_id' => $branch_id)); 
		}else{
			$data['list_site']		= $this->site_model->gets(array('columns' => 'A.id, A.name, C.code AS company_code', 'branch_id' => $branch_id));
		}
		$this->view($data); 
	}

	public function list_ajax($view = FALSE){

		$this->load->model(array('payroll_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, B.full_name AS employee_name, B.phone_number, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.salary, A.payment, A.email, A.note, A.tax_calculation, A.bpjs_ks_company, A.bpjs_jht_company";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];

		$params['employee_name']	= $_POST['columns'][2]['search']['value'];
		$params['phone_number']		= $_POST['columns'][3]['search']['value'];
		$params['periode_start']	= $_POST['columns'][4]['search']['value'];
		$params['periode_end']		= $_POST['columns'][5]['search']['value'];
		$params['bpjs_ks_company']	= $_POST['columns'][6]['search']['value'];
		$params['bpjs_jht_company']	= $_POST['columns'][7]['search']['value'];
		$params['tax_calculation']	= $_POST['columns'][8]['search']['value'];
		$params['payment']			= $_POST['columns'][9]['search']['value'];
		$params['email']			= $_POST['columns'][10]['search']['value'];
		$params['note']				= $_POST['columns'][11]['search']['value'];
		$params['site_id']			= $this->session->userdata('site');	
		
// 		if($this->session->userdata('branch') > 2){
			$params['not_payment']		= array('Draft', 'Ditolak');
// 		}
// 		$params['not_payment']		= array('Draft', 'Ditolak');

		if($view == 'last'){
			$last_payroll 				= $this->payroll_model->last_payroll(array('site_id' => $params['site_id'], 'not_payment' => $params['not_payment'], 'columns' => 'MAX(A.periode_start) AS last_payroll'));
			if($last_payroll){
				$params['preview']		= $last_payroll->last_payroll;
			}
		}
		$list 	= $this->payroll_model->gets($params);
		$total 	= $this->payroll_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 				= $i;
			$result['employee_name']	= $item->employee_name;
			$result['phone_number']		= $item->phone_number;
			$result['periode_start']	= $item->periode_start;
			$result['periode_end']		= $item->periode_end;
			$result['note']				= $item->note;
			$result['bpjs_ks_company']	= format_rupiah((double) $item->bpjs_ks_company);
			$result['bpjs_jht_company']	= format_rupiah((double) $item->bpjs_jht_company);
			$result['tax_calculation']	= format_rupiah((double) $item->tax_calculation);
			$result['salary']			= format_rupiah((double) $item->salary);
			$result['payment']			= '<p class="text-warning">'.$item->payment.'</p>';
			if($item->payment ==  'Selesai'){
				$result['payment']			= '<p class="text-success">'.$item->payment.'</p>';
			}
			$result['email']		= '<p class="text-warning">'.$item->email.'</p>';
			if($item->email ==  'Terkirim'){
				$result['email']			= '<p class="text-success">'.$item->email.'</p>';
			}else if($item->email == 'Gagal'){
				$result['email']			= '<p class="text-danger">'.$item->email.'</p>';
			}
			$result['action'] 			=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("ropayroll/preview/".$item->id).'">Lihat</a>
				<a href="'.base_url('ropayroll/pdf/' . $item->id).'" style="cursor:pointer;" class="btn-sm btn-warning btn-block" target="_blank">PDF</a>
				<a href="'.base_url('payroll/whatsapp/list?id=' . $item->id).'" style="cursor:pointer;" class="btn-sm btn-secondary btn-block">Whatsapp</a>';
			if($item->payment == 'Menunggu'){
				$result['action'] .= 
				'<a href="'.base_url('payroll/form/' . $item->id).'" style="cursor:pointer;" class="btn-sm btn-success btn-block">Ubah</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function delete($payroll_id = false)
	{
		$this->load->model('payroll_model');
		if ($payroll_id)
		{
			$data =  $this->payroll_model->get(array('id' => $payroll_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $payroll_id, 'is_active' => 0);
				$result = $this->payroll_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('payroll/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal dihapus.','danger'));
					redirect(base_url('payroll/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('payroll/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('payroll/list'));
		}
	}

	public function preview_payroll()
	{
		$this->load->model(array('payroll_model', 'employee_model', 'position_model'));
		$params = $this->input->get();
		$ids = explode(",", $params['id']);
		
		$payroll = array();
		foreach ($ids as $payroll_id) {
			$detail_payroll = array();
			$detail_payroll['income']			= $this->payroll_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'pendapatan', 'columns' => 'A.name, A.value'));
			$detail_payroll['outcome']		= $this->payroll_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'potongan', 'columns' => 'A.name, A.value'));
			$detail_payroll['payroll']		= $this->payroll_model->get(array('id' => $payroll_id, 'columns' => 'A.id, A.employee_id , A.periode_start, A.periode_end, A.salary, A.overtime_hour, A.overtime_calculation, A.attendance, A.basic_salary, A.bpjs_ks, A.bpjs_jht, A.bpjs_jp, A.tax_calculation, A.salary_note'));
			$detail_payroll['employee']		= $this->employee_model->get(array('id' => $detail_payroll['payroll']->employee_id,'columns' => 'A.full_name, A.bank_account, A.employee_number, A.position_id, B.name AS site_name, C.code AS company_code'));
			$detail_payroll['position']		= $this->position_model->get(array('id' => $detail_payroll['employee']->position_id,'columns' => 'A.name'));
			$payroll[$payroll_id] = $detail_payroll;
		}
		$data['payroll']		= $payroll;
		$data['_TITLE_'] 		= 'Preview Gaji Karyawan';
		$data['_PAGE_']	 		= 'payroll/preview_payroll';
		$data['_MENU_PARENT_'] 	= 'payroll_employee';
		$data['_MENU_'] 		= 'payroll';
		$this->load->view('payroll/preview_payroll', $data);
	}

	public function whatsapp($type = 'list')
	{
		$params = $this->input->get();
		$ids = explode(",", $params['id']);
		
		$this->load->model(array('wappin_model'));
		$config_id = 1;
		$wappin 		= $this->wappin_model->get(array('id' => $config_id, 'columns' => 'A.access_token, A.base_url'));
		if($wappin){
			$message['token'] 	= "Authorization: Bearer ".$wappin->access_token;
			$message['base_url']= $wappin->base_url;
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Melakukan Koneksi API.','danger'));
			redirect(base_url('payroll/'.$type));
			die();
		}
		
		foreach ($ids as $payroll_id) {
			$this->load->model(array('payroll_model', 'employee_model', 'position_model', 'email_model'));
			
			$income			= $this->payroll_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'pendapatan', 'columns' => 'A.name, A.value'));
			$outcome		= $this->payroll_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'potongan', 'columns' => 'A.name, A.value'));
			$payroll		= $this->payroll_model->get(array('id' => $payroll_id, 'columns' => 'A.id, A.employee_id , A.periode_start, A.periode_end, A.salary, A.overtime_hour, A.overtime_calculation, A.attendance, A.basic_salary, A.bpjs_ks, A.bpjs_jht, A.bpjs_jp, A.tax_calculation, A.salary_note'));
			$employee		= $this->employee_model->get(array('id' => $payroll->employee_id,'columns' => 'A.email, A.phone_number, A.id_card, A.full_name, A.bank_account, A.employee_number, A.position_id, B.name AS site_name, C.code AS company_code'));
			$position		= $this->position_model->get(array('id' => $employee->position_id,'columns' => 'A.name, B.name AS company_name'));
			if($employee->phone_number == ""){
				$this->payroll_model->save(array('email' => 'Gagal', 'id' => $payroll_id));
				continue;
			}

			$start_date 	= substr($payroll->periode_start, 8, 2).' '.get_month(substr($payroll->periode_start, 5, 2));
			$end_date 		= substr($payroll->periode_end, 8, 2).' '.get_month(substr($payroll->periode_end, 5, 2)).' '.substr($payroll->periode_end, 0, 4);
			$data['periode']= $start_date.' - '.$end_date;
			$html  = '<table width="100%">';
			$html .= '<tr><td width="25%">PERIODE BULAN</td><td> : '.$data['periode'].'</td></tr>
					<tr><td>NO REKENING</td><td> : '.$employee->bank_account.'</td></tr>
					<tr><td>NAMA</td><td> : '.$employee->full_name.'</td></tr>
					<tr><td>NOMOR KARYAWAN</td><td> : '.$employee->employee_number.'</td></tr>
					<tr><td>JABATAN</td><td> : '.$position->name.'</td></tr>
					<tr><td>SITE</td><td> : '.$employee->site_name.'</td></tr>';
			if($payroll->attendance){
				$html .= '<tr><td>JUMLAH KEHADIRAN</td><td> : '.$payroll->attendance.'</td></tr>';
			}
			if($payroll->overtime_hour){
				$html .= '<tr><td>JUMLAH JAM LEMBUR</td><td> : '.$payroll->overtime_hour.'</td></tr>';
			}
			$html .= '</table><table width="100%">';
			$html .= '<tr><td width="60%">&nbsp;</td><td><strong>Gaji Bersih diterima : '.format_rupiah($payroll->salary).'</strong></td></tr>';
			$html .= '</table><br><br><table width="100%">';
			$html .= '<tr><td width="50%"><table>';
			$html .= '<tr><td><strong>Pendapatan</strong></td><td></td><td></td></tr>';
			if($payroll->basic_salary){
				$html .= "<tr><td>Gaji Pokok</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->basic_salary)."</td></tr>";
			}else{
				$html .= "<tr><td>Gaji Pokok</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
			}
			if($payroll->overtime_calculation){
				$html .= "<tr><td>Lembur</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->overtime_calculation)."</td></tr>";
			}
			foreach( $income AS $item):
				$html .= "<tr><td >".$item->name."</td><td>: Rp.</td><td style='float:right'>".rupiah($item->value)."</td></tr>";
			endforeach;
			$html .= '</table></td>';
			$html .= '<td width="50%"><table>';
			$html .= '<tr><td><strong>Potongan</strong></td><td></td><td></td></tr>';
			if($payroll->bpjs_ks){
				$html .= "<tr><td>BPJS Kesehatan</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->bpjs_ks)."</td></tr>";
			}else{
				$html .= "<tr><td>BPJS Kesehatan</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";	
			}
			if($payroll->bpjs_jht){
				$html .= "<tr><td>BPJS Ketenagakerjaan</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->bpjs_jht)."</td></tr>";
			}else{
					$html .= "<tr><td>BPJS Ketenagakerjaan</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
			}
			if($payroll->tax_calculation){
				$html .= "<tr><td>PPH21</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->tax_calculation)."</td></tr>";
			}else{
				$html .= "<tr><td>PPH21</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
			}
			foreach($outcome AS $item):
				$html .= "<tr><td >".$item->name."</td><td>: Rp.</td><td style='float:right'>".rupiah($item->value)."</td></tr>";
			endforeach;
			$html .= '</table></td></tr>';
			$html .= '</table>';
			$html .= '<table width="100%"><tr><td><strong>Catatan : </strong>'.$payroll->salary_note.'</td></tr></table><br>';
			$data['content'] 	= $html; 
			$data['title'] 		=  preg_replace("/[^A-Za-z0-9]/", "", strtolower($employee->employee_number.$data['periode']));
			$data['basepath'] 	= FCPATH.'files/payroll_karyawan/';
			$data['image']		= strtolower($employee->company_code).'.jpg';
			$data['company']	= $position->company_name; 
			$data['password']	= $employee->id_card; 
			$this->generate_PDF($data);

			$email_template 		= $this->email_model->get(array('id' => 4));
			$message['subject'] 	= $email_template->subject;
			$message['message'] 	= $email_template->content;

			$message['attachment'] 	= $data['basepath'].$data['title'].'pdf';
			$message['url'] 		= base_url("files/payroll_karyawan/".$data['title'].'.pdf');
			$message['phone_number']= $employee->phone_number;
			$message['periode']		= $data['periode'];
			$message['name']		= $employee->full_name;
			if($this->send_whatsapp($message)){
				$this->payroll_model->save(array('email' => 'Terkirim', 'id' => $payroll_id));
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Slip gaji berhasil dikirim.','success'));
			}else{
				$this->payroll_model->save(array('email' => 'Gagal', 'id' => $payroll_id));
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong>  Slip gaji gagal dikirim ke nomor : '.$employee->phone_number,'danger'));
				redirect(base_url('payroll/'.$type));
				die();
			}
		}
		redirect(base_url('payroll/'.$type));
	}

	private function generate_PDF($data = array())
	{	
		$this->load->library("Pdf");
		ob_start();
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		if(isset($data['password'])){
			$pdf->SetProtection(array('print', 'copy'), $data['password'], null, 0, null);
		}
		$pdf->SetHeaderData($data['image'], '181', '', null);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shelter');
		$pdf->SetTitle($data['title']);
		$pdf->SetSubject($data['title']);
		$pdf->SetKeywords('Shelter');

		// $pdf->setPrintHeader(true);
		// $pdf->setPrintFooter(false);

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$pdf->SetFont('times', '', 10);
		$pdf->AddPage();
		$pdf->writeHTML($data['content'], true, false, true, false, '');

		$base_path  = $data['basepath'];
		if(!is_dir($base_path)){
			mkdir($base_path, 0777, true);
		}
		return $pdf->Output($base_path.strtolower(str_replace(" ", "_",$data['title'])).'.pdf', 'F');
	}

	public function form($payroll_id = FALSE)
	{
		$this->load->model(array('payroll_model','employee_model'));
		if($this->input->post()){
			$data['id'] 				= $this->input->post('payroll_id');
			$data['salary'] 			= str_replace(".", "", $this->input->post('salary'));
			$data['basic_salary']		= str_replace(".", "", $this->input->post('basic_salary'));
			$data['bpjs_ks_company']	= str_replace(".", "", $this->input->post('bpjs_ks_company'));
			$data['bpjs_jht_company']	= str_replace(".", "", $this->input->post('bpjs_jht_company'));
			$data['bpjs_ks']			= str_replace(".", "", $this->input->post('bpjs_ks'));
			$data['bpjs_jht']			= str_replace(".", "", $this->input->post('bpjs_jht'));
			$data['tax_calculation']	= str_replace(".", "", $this->input->post('tax_calculation'));

			$this->form_validation->set_rules('payroll_id', '', 'required');
			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{

				$this->payroll_model->save($data);
				$this->payroll_model->delete_detail(array('payroll_id' => $data['id']));
				
				$payroll_detail_name		= $this->input->post('payroll_detail_name');
				$payroll_detail_category	= $this->input->post('payroll_detail_category');
				$payroll_detail_value		= $this->input->post('payroll_detail_value');

				foreach ($payroll_detail_name AS $key => $value ) {
					if($payroll_detail_value[$key] == ''){
						continue;
					}
					$data_detail['payroll_id']	= $data['id'];
					$data_detail['name'] 		= $payroll_detail_name[$key];
					$data_detail['category'] 	= $payroll_detail_category[$key];
					$data_detail['value'] 		= str_replace(".", "", $payroll_detail_value[$key]);
					print_prev($data_detail);
					$this->payroll_model->save_detail($data_detail);

				}
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			}
			redirect(base_url('payroll/form/'.$data['id']));
			exit();
		}
		
		$data = (array) $this->payroll_model->get(array('id' => $payroll_id, 'columns' => 'A.id AS payroll_id, A.employee_id, A.salary, A.basic_salary, A.bpjs_ks, A.bpjs_jht, A.bpjs_ks_company, A.bpjs_jht_company, A.tax_calculation'));
		if (empty($data))
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('payroll/list'));
		}
		$data['employee']		= $this->employee_model->get(array('id' => $data['employee_id'], 'columns' => 'A.full_name, A.address_card'));
		$data['payroll_detail']	= $this->payroll_model->gets_detail(array('payroll_id' => $payroll_id, 'columns' => 'A.name, A.category, A.value'));
		$data['_TITLE_'] 		= 'Gaji Karyawan';
		$data['_PAGE_'] 		= 'payroll/form';
		$data['_MENU_PARENT_'] 	= 'payroll_employee';
		$data['_MENU_'] 		= 'payroll';
		return $this->view($data);
	}

	private function validate($value){
		$result = false;
		if(substr($value, 0, 1) == "="){
			$result = true;
		}
		return $result;
	}

	public function import(){
		$this->load->model(array('payroll_config_model', 'employee_model', 'payroll_model'));
		if($_FILES['file']['name'] != ""){
			
			$config['id'] 							= $this->input->post('config_id');
			$config['config_name'] 					= $this->input->post('config_name');
			$config['site_id'] 						= $this->session->userdata('site');
			$config['row_start'] 					= $this->input->post('row_start');
			$config['row_end'] 						= $this->input->post('row_end');
			$config['row_title'] 					= $this->input->post('row_title');
			$config['column_employee'] 				= strtoupper($this->input->post('column_employee'));
			$config['column_income'] 				= strtoupper($this->input->post('column_income'));
			$config['column_outcome'] 				= strtoupper($this->input->post('column_outcome'));
			$config['column_net'] 					= strtoupper($this->input->post('column_net'));
			$config['column_attendance']			= strtoupper($this->input->post('column_attendance'));
			$config['column_overtime_hour']			= strtoupper($this->input->post('column_overtime_hour'));
			$config['column_overtime_calculation']	= strtoupper($this->input->post('column_overtime_calculation'));
			$config['column_basic_salary']			= strtoupper($this->input->post('column_basic_salary'));
			$config['column_bpjs_ks']				= strtoupper($this->input->post('column_bpjs_ks'));
			$config['column_bpjs_jht']				= strtoupper($this->input->post('column_bpjs_jht'));
			$config['column_bpjs_jp']				= strtoupper($this->input->post('column_bpjs_jp'));
			$config['column_bpjs_ks_company']		= strtoupper($this->input->post('column_bpjs_ks_company'));
			$config['column_bpjs_jht_company']		= strtoupper($this->input->post('column_bpjs_jht_company'));
			$config['column_bpjs_jp_company']		= strtoupper($this->input->post('column_bpjs_jp_company'));
			$config['column_tax_calculation']		= strtoupper($this->input->post('column_tax_calculation'));
			$config['column_note'] 					= strtoupper($this->input->post('column_note'));
			$this->payroll_config_model->save($config);

			$periode_start				= $this->input->post('periode_start');
			$periode_end				= $this->input->post('periode_end');
			
			$this->load->library("Excel");
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);

			$title 			= array();
			$column_income	= array();
			$column_outcome	= array();
			if($config['column_income']){
				$column_income  = explode(",", preg_replace("/[^A-Z,]/", "", $config['column_income']));
			}
			if($config['column_outcome']){
				$column_outcome = explode(",", preg_replace("/[^A-Z,]/", "", $config['column_outcome']));
			}

			foreach($column_income AS $item){
				$title[$item] = replace_null($sheet->getCell($item.$config['row_title'])->getCalculatedValue());
			}
			foreach($column_outcome AS $item){
				$title[$item] = replace_null($sheet->getCell($item.$config['row_title'])->getCalculatedValue());
			}

			$total 		= $config['row_end'] - $config['row_start'];
			$success 	= 0;
			$list 		= '';
			for ($row = $config['row_start']; $row <= $config['row_end']; $row++) {
				$employee_number 		= replace_null($sheet->getCell($config['column_employee'].$row)->getValue());
				if($this->validate($employee_number)){
					$employee_number 	= replace_null($sheet->getCell($config['column_employee'].$row)->getOldCalculatedValue());
				}
				if($employee_number == ""){
					continue;
				}
				$employee_number = preg_replace("/[^0-9,]/", "", $employee_number);

				$employee = $this->employee_model->get(array('id_card' => $employee_number, 'columns' => 'A.id'));
				if($employee){
					$payroll = $this->payroll_model->get(array('employee_id' => $employee->id, 'periode_start' => $periode_start, 'periode_end' => $periode_end, 'columns' => 'A.id, A.payment'));
			
					if($payroll){
						if($payroll->payment == 'Selesai'){
							continue;
						}
						$this->payroll_model->save(array('id' => $payroll->id, 'is_active' => 0));
						$this->payroll_model->delete_detail(array('payroll_id' => $payroll->id));
						$success = $success +1;
					}

					$salary_net 		= replace_null($sheet->getCell($config['column_net'].$row)->getValue());
					if($this->validate($salary_net)){
						$salary_net 	= replace_null($sheet->getCell($config['column_net'].$row)->getOldCalculatedValue());
					}

					if($config['column_attendance'] != ""){
						$attendance 			= replace_null($sheet->getCell($config['column_attendance'].$row)->getValue());
						if($this->validate($attendance)){
							$attendance 		= replace_null($sheet->getCell($config['column_attendance'].$row)->getOldCalculatedValue());
						}
						$insert['attendance'] 	=  $attendance;
					}

					if($config['column_overtime_hour'] != ""){
						$overtime_hour 			= replace_null($sheet->getCell($config['column_overtime_hour'].$row)->getValue());
						if($this->validate($overtime_hour)){
							$overtime_hour 		= replace_null($sheet->getCell($config['column_overtime_hour'].$row)->getOldCalculatedValue());
						}
						$insert['overtime_hour']=  $overtime_hour;
					}

					if($config['column_overtime_calculation'] != ""){
						$overtime_calculation 			= replace_null($sheet->getCell($config['column_overtime_calculation'].$row)->getValue());
						if($this->validate($overtime_calculation)){
							$overtime_calculation 		= replace_null($sheet->getCell($config['column_overtime_calculation'].$row)->getOldCalculatedValue());
						}
						$insert['overtime_calculation'] =  $overtime_calculation;
					}
					
					if($config['column_basic_salary'] != ""){
						$basic_salary 			= replace_null($sheet->getCell($config['column_basic_salary'].$row)->getValue());
						if($this->validate($basic_salary)){
							$basic_salary 		= replace_null($sheet->getCell($config['column_basic_salary'].$row)->getOldCalculatedValue());
						}
						$insert['basic_salary'] =  $basic_salary;
					}
					
					if($config['column_bpjs_ks'] != ""){
						$bpjs_ks 			= replace_null($sheet->getCell($config['column_bpjs_ks'].$row)->getValue());
						if($this->validate($bpjs_ks)){
							$bpjs_ks 		= replace_null($sheet->getCell($config['column_bpjs_ks'].$row)->getOldCalculatedValue());
						}
						$insert['bpjs_ks'] 	=  $bpjs_ks;
					}
					
					if($config['column_bpjs_jht'] != ""){
						$bpjs_jht 			= replace_null($sheet->getCell($config['column_bpjs_jht'].$row)->getValue());
						if($this->validate($bpjs_jht)){
							$bpjs_jht 		= replace_null($sheet->getCell($config['column_bpjs_jht'].$row)->getOldCalculatedValue());
						}
						$insert['bpjs_jht'] =  $bpjs_jht;
					}
					
					if($config['column_bpjs_jp'] != ""){
						$bpjs_jp 			= replace_null($sheet->getCell($config['column_bpjs_jp'].$row)->getValue());
						if($this->validate($bpjs_jp)){
							$bpjs_jp 		= replace_null($sheet->getCell($config['column_bpjs_jp'].$row)->getOldCalculatedValue());
						}
						$insert['bpjs_jp'] 	=  $bpjs_jp;
					}

					if($config['column_bpjs_ks_company'] != ""){
						$bpjs_ks_company 			= replace_null($sheet->getCell($config['column_bpjs_ks_company'].$row)->getValue());
						if($this->validate($bpjs_ks_company)){
							$bpjs_ks_company 		= replace_null($sheet->getCell($config['column_bpjs_ks_company'].$row)->getOldCalculatedValue());
						}
						$insert['bpjs_ks_company'] 	=  $bpjs_ks_company;
					}
					
					if($config['column_bpjs_jht_company'] != ""){
						$bpjs_jht_company 			= replace_null($sheet->getCell($config['column_bpjs_jht_company'].$row)->getValue());
						if($this->validate($bpjs_jht_company)){
							$bpjs_jht_company 		= replace_null($sheet->getCell($config['column_bpjs_jht_company'].$row)->getOldCalculatedValue());
						}
						$insert['bpjs_jht_company'] 	=  $bpjs_jht_company;
					}
					
					if($config['column_bpjs_jp_company'] != ""){
						$bpjs_jp_company 			= replace_null($sheet->getCell($config['column_bpjs_jp_company'].$row)->getValue());
						if($this->validate($bpjs_jp_company)){
							$bpjs_jp_company 		= replace_null($sheet->getCell($config['column_bpjs_jp_company'].$row)->getOldCalculatedValue());
						}
						$insert['bpjs_jp_company'] 	=  $bpjs_jp_company;
					}
					
					if($config['column_tax_calculation'] != ""){
						$tax_calculation 			= replace_null($sheet->getCell($config['column_tax_calculation'].$row)->getValue());
						if($this->validate($tax_calculation)){
							$tax_calculation 		= replace_null($sheet->getCell($config['column_tax_calculation'].$row)->getOldCalculatedValue());
						}
						$insert['tax_calculation'] 	=  $tax_calculation;
					}

					if($config['column_note'] != ""){
						$salary_note 		= replace_null($sheet->getCell($config['column_note'].$row)->getValue());
						$insert['salary_note'] 	=  $salary_note;
					}

					$insert['id']				= '';	
					$insert['employee_id'] 		=  $employee->id;
					$insert['periode_start'] 	=  $periode_start;
					$insert['periode_end'] 		=  $periode_end;
					$insert['salary'] 			=  $salary_net;
					$insert['payment'] 			=  'Menunggu';
					$insert['email'] 			=  'Menunggu';
					$payroll_id = $this->payroll_model->save($insert);
					foreach($column_income AS $item){
						$insert_detail['payroll_id']= $payroll_id;
						$insert_detail['name'] 		= $title[$item];
						$insert_detail['category'] 	= 'pendapatan';

						$insert_detail['value'] 			= replace_null($sheet->getCell($item.$row)->getValue());
						if($this->validate($insert_detail['value'])){
							$insert_detail['value'] 		= replace_null($sheet->getCell($item.$row)->getOldCalculatedValue());
						}
						$this->payroll_model->save_detail($insert_detail);
					}

					foreach($column_outcome AS $item){
						$insert_detail['payroll_id']= $payroll_id;
						$insert_detail['name'] 		= $title[$item];
						$insert_detail['category'] 	= 'potongan';

						$insert_detail['value'] 			= replace_null($sheet->getCell($item.$row)->getValue());
						if($this->validate($insert_detail['value'])){
							$insert_detail['value'] 		= replace_null($sheet->getCell($item.$row)->getOldCalculatedValue());
						}
						$this->payroll_model->save_detail($insert_detail);
					}
				}else{
					$list = $list." ".$employee_number.",";
				}
			}
			if($success == 0){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan, silahkan cek kolom NIK untuk memastikan NIK karywan sudah benar.','danger'));
			}else if($success < $total){
				$this->session->set_flashdata('message', message_box('<strong>Peringatan!</strong> Data yang berhasil diinputkan : '.$success.' dari '.$total.', pastikan NIK yang diimport sesuai dengan data NIK di HRIS. <br> Berikut daftar NIK yang gagal diimport : '.$list,'warning'));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			}
			$this->generate_payroll();
			redirect(base_url('payroll/list'));
		}
	}

	public function export($view = FALSE)
	{
		$this->load->model(array('payroll_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$params 			= $this->input->get();
		$params['site_id']	= $this->session->userdata('site');
		if($view == 'last'){
			$last_payroll 				= $this->payroll_model->last_payroll(array('site_id' => $params['site_id'], 'payment' => 'Menunggu', 'columns' => 'MAX(A.periode_start) AS last_payroll'));
			if($last_payroll){
				$params['preview']		= $last_payroll->last_payroll;
			}
		}

		$title = 'payroll_karyawan';
		$files = glob(FCPATH."files/".$title."/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}
		
		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
		->setLastModifiedBy($this->session->userdata('name'))
		->setTitle($title)
		->setSubject($title)
		->setDescription($title)
		->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Nomor Karyawan");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("C".$i, "Nomor Rekening");
		$excel->getActiveSheet()->setCellValue("D".$i, "Nama Bank");
		$excel->getActiveSheet()->setCellValue("E".$i, "BPJS KS Perusahaan");
		$excel->getActiveSheet()->setCellValue("F".$i, "BPJS TK Perusahaan");
		$excel->getActiveSheet()->setCellValue("G".$i, "Potongan BPJS Kesehatan");
		$excel->getActiveSheet()->setCellValue("H".$i, "Potongan BPJS Ketenagakerjaan");
		$excel->getActiveSheet()->setCellValue("I".$i, "PPH 21");
		$header_column = 'J';

		$list_componet = $this->payroll_model->gets_detail(array('site_id' => $params['site_id'], 'columns' => 'A.name', 'groupby' => 'A.name',));
		foreach($list_componet AS $value){
			$excel->getActiveSheet()->setCellValue($header_column.$i, $value->name);
			$header_column++;
		}
		$excel->getActiveSheet()->setCellValue($header_column.$i, "Gaji Bersih");

		$params['columns'] 		= "B.employee_number, B.full_name, B.bank_account, B.bank_name, A.id AS payroll_id, A.salary, A.tax_calculation, A.bpjs_ks_company, A.bpjs_jht_company, A.bpjs_ks, A.bpjs_jht, A.tax_calculation";
		$params['orderby'] 		= 'B.full_name';
		$params['order']		= 'ASC';

		$list 	= $this->payroll_model->gets($params);
		$i++;
		$no = 1;
		foreach($list as $item)
		{
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->employee_number);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->bank_account);
			$excel->getActiveSheet()->setCellValueExplicit("D".$i, $item->bank_name);
			$excel->getActiveSheet()->setCellValueExplicit("E".$i, $item->bpjs_ks_company);
			$excel->getActiveSheet()->setCellValueExplicit("F".$i, $item->bpjs_jht_company);
			$excel->getActiveSheet()->setCellValueExplicit("G".$i, '-'.$item->bpjs_ks);
			$excel->getActiveSheet()->setCellValueExplicit("H".$i, '-'.$item->bpjs_jht);
			$excel->getActiveSheet()->setCellValueExplicit("I".$i, '-'.$item->tax_calculation);
			
			$column = 'J';
			foreach($list_componet AS $value){
				$detail = $this->payroll_model->get_detail(array('payroll_id' => $item->payroll_id, 'name' => $value->name, 'A.category, A.value'));
				if($detail){
					if($detail->category == 'potongan'){
						$detail->value = '-'.$detail->value;	
					}
					$excel->getActiveSheet()->setCellValueExplicit($column.$i, $detail->value);
				}
				$column++;
			}
			$excel->getActiveSheet()->setCellValueExplicit($column.$i, $item->salary);
			$no++;
			$i++;
		}
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function send_whatsapp($data = array()){
		$result = false;
		if($data['phone_number'] != ''){
			$phone_number = explode ("/", $data['phone_number']);
			$data['phone_number'] = preg_replace("/[^0-9]/", "", $phone_number[0]);
			$start = substr($data['phone_number'], 0, 1 );
			if($start == '0'){
				$data['phone_number'] = '62'.substr($data['phone_number'], 1,strlen($data['phone_number']));
			}else if($start == '+'){
				$data['phone_number'] = substr($data['phone_number'], 1,strlen($data['phone_number']));
			}
			$result = $this->send_message($data);
		}
		return $result;
	}

	private function send_message($data = array()){
		$url 		= $data['base_url'].'/v1/messages';
		$request 	= array(
			'to' => $data['phone_number'],
			'type'	=> 'template',
			'template' => array(
				'name'	=> $this->config->item('reset_payroll_name'),
				'namespace' => $this->config->item('reset_payroll_namespace'),
				'language'	=> array(
					'policy' => 'deterministic',
					'code'	=> 'id'
				),
				'components' => array(
					array(
						'type' => 'body',
						'parameters'=> array(
							array(
								'type' => 'text',
								'text'	=> $data['name']
							),
							array(
								'type' => 'text',
								'text'	=> $data['periode']
							),
							array(
								'type' => 'text',
								'text'	=> $data['url']
							)
						)
					)
				)
			),
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $data['token']));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
		$response = curl_exec($ch);
		$result = json_decode($response, true);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if($httpcode == 200){
			return true;
		}else{
			return false;
		}
	}

	public function delete_config($config_id = false)
	{
		$this->load->model('payroll_config_model');
		if ($config_id)
		{
			$data =  $this->payroll_config_model->get(array('id' => $config_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $config_id, 'is_active' => 0);
				$result = $this->payroll_config_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil dihapus.','success'));
					redirect(base_url('payroll/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal dihapus.','danger'));
					redirect(base_url('payroll/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('payroll/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
			redirect(base_url('payroll/list'));
		}
	}

	public function generate()
	{
		$this->load->model(array('cutoff_model', 'employee_model', 'attendance_model', 'employee_salary_model', 'payroll_model', 'setting_tax_model'));
		$params['periode'] 		= $this->input->post('periode');
		$params['site_id']		= $this->session->userdata('site');
		if(!$params['periode']){
			$params['periode'] = date('m');
		}

		$cutoff					= $this->cutoff_model->get(array('site_id' => $params['site_id'], 'columns' => 'A.start_date, A.end_date, A.formula_id'));
		if($cutoff){
			$params['date_start']  			= date('Y-'.$params['periode'].'-'.$cutoff->start_date);
			$params['date_end'] 			= date('Y-'.$params['periode'].'-'.$cutoff->end_date);
			if($params['date_start']  > $params['date_end']){
				$params['date_start'] 	= date('Y-m-d', strtotime($params['date_start']."-1 months"));
			}
			if($cutoff->formula_id == 2){
				$this->indirect($params);
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Formula Perhitungan belum diatur, silahkan diatur <a href="'.base_url('cutoff/list').'">disini</>.','danger'));				
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Cutoff belum ada, silahkan diatur <a href="'.base_url('cutoff/list').'">disini</>.','danger'));
		}
	}


	private function indirect($params = array()){
		$list_employee = $this->employee_model->gets_ptkp(array('site_id' => $params['site_id'], 'status' => 1, 'status_approval' => 3, 'columns' => 'A.id, B.minimal, A.resign_submit'));
		$list_resign   = $this->employee_model->gets_ptkp(array('site_id' => $params['site_id'], 'resign_submit' => date('Y-m'), 'columns' => 'A.id, B.minimal, A.resign_submit'));
		
		$list_employee = (object) array_merge((array) $list_employee, (array) $list_resign);
		foreach($list_employee AS $employee){
			$payroll_employee['id'] 			= '';
			$payroll = $this->payroll_model->get(array('employee_id' => $employee->id, 'periode_start' => $params['date_start'], 'periode_end' => $params['date_end'], 'columns' => 'A.id, A.payment'));
			if($payroll){
				if($payroll->payment == 'Pengajuan' || $payroll->payment == 'Selesai'){
					continue;
				}
				$payroll_employee['id'] = $payroll->id;	
			}

			$payroll_employee['employee_id'] 			= $employee->id;
			$payroll_employee['periode_start'] 			= $params['date_start'];
			$payroll_employee['periode_end'] 			= $params['date_end'];
			$payroll_employee['is_active'] 				= 1;
			$payroll_employee['payment'] 				= 'Menunggu';
			$payroll_employee['email'] 					= 'Menunggu';
			$payroll_employee['salary'] 				= 0;
			$payroll_employee['overtime_hour'] 			= 0;
			$payroll_employee['overtime_calculation'] 	= 0;
			$payroll_employee['attendance'] 			= 0;
			$payroll_employee['basic_salary'] 			= 0;
			$payroll_employee['bpjs_ks'] 				= 0;
			$payroll_employee['bpjs_jht'] 				= 0;
			$payroll_employee['bpjs_jp'] 				= 0;
			$payroll_employee['bpjs_ks_company'] 		= 0;
			$payroll_employee['bpjs_jht_company'] 		= 0;
			$payroll_employee['bpjs_jp_company'] 		= 0;
			$payroll_employee['tax_calculation'] 		= 0;
			$payroll_employee['note'] 					= '';
			$payroll_employee['salary_note'] 			= '';

			$salary = $this->employee_salary_model->get_salary(array('employee_id' => $employee->id, 'columns' => 'A.basic_salary, A.bpjs_ks, A.bpjs_tk, A.bpjs_ks_employee, A.bpjs_tk_employee, A.join_date'));
			
			if(!$salary){
				continue;
			}

			$basic_salary = evalmath($salary->basic_salary);
			$payroll_employee['basic_salary'] = $basic_salary;

			$bpjs_ks = evalmath($salary->bpjs_ks);
			$payroll_employee['bpjs_ks_company'] = $bpjs_ks;

			$bpjs_tk = evalmath($salary->bpjs_tk);
			$payroll_employee['bpjs_jht_company'] = $bpjs_tk;

			$bpjs_ks_employee = evalmath($salary->bpjs_ks_employee);
			$payroll_employee['bpjs_ks'] = $bpjs_ks_employee;

			$bpjs_tk_employee = evalmath($salary->bpjs_tk_employee);
			$payroll_employee['bpjs_jht'] = $bpjs_tk_employee;

			$payroll_id = $this->payroll_model->save($payroll_employee);
			$this->payroll_model->delete_detail(array('payroll_id' => $payroll_id));
				
			$detail_potongan = 0;
			$detail_pendapatan = 0;
			$salary_detail = $this->employee_salary_model->gets_detail(array('employee_id' => $employee->id, 'columns' => 'A.name, A.value'));

			foreach($salary_detail AS $detail){
				$payroll_employee_detail['payroll_id']	= $payroll_id;
				$payroll_employee_detail['name'] 		= $detail->name;
				$payroll_employee_detail['is_active'] 	= 1;	
					
				if($detail->value == '' || $detail->value == 0){
					continue;
				}

				if(substr($detail->value, 0, 1) == '-'){
					$payroll_employee_detail['category']= 'potongan';
					$payroll_employee_detail['value']   = substr($detail->value, 1);
					$detail_potongan 					= $detail_potongan + $payroll_employee_detail['value'];
				}else{
					$payroll_employee_detail['category']= 'pendapatan';
					$payroll_employee_detail['value']   = $detail->value;
					$detail_pendapatan 					= $detail_pendapatan + $payroll_employee_detail['value'];
				}
				
				$this->payroll_model->save_detail($payroll_employee_detail);
			}

			$timeStart 	= strtotime($salary->join_date);
			$timeEnd 	= strtotime($params['date_end']);
			$monthservice 	= 1 + (date("Y",$timeEnd)-date("Y",$timeStart))*12;
			$monthservice 	+= date("m",$timeEnd)-date("m",$timeStart);
			$yearService = $this->yearService($monthservice);
			$this->payroll_model->save_detail(array('payroll_id' => $payroll_id, 'name' => 'MASA KERJA', 'is_active' => 1, 'category' => 'pendapatan', 'value' => $yearService));
			$gross 		= $basic_salary + $detail_pendapatan + $yearService + $bpjs_ks + $bpjs_tk;
			$feePosition= $this->feePosition($gross);
			$grossYear 	= ($gross-$feePosition-($bpjs_ks_employee+$bpjs_tk_employee))*12;
			$tax 		= $this->getTax(array('ptkp' => $employee->minimal, 'grossYear'  => $grossYear));
			
			$this->payroll_model->save(array('id' => $payroll_id, 'tax_calculation' => $tax));
			$full_salary= $gross - ($bpjs_ks_employee + $bpjs_tk_employee + $tax);

			$params['employee_id'] 	= $employee->id;
			$params['resign_submit']= $employee->resign_submit;
			$params['join_date']	= '';
			if($monthservice == 0){
				$params['join_date']	= $salary->join_date;	
			}

			$attendance = $this->checkAttendance($params);
			$reduction 	= $full_salary/30;
			if($params['resign_submit'] == '' || $params['join_date'] == ''){
				$full_salary = ($full_salary/25) * $attendance['present'];
			}

			$reduction_late = 0;
			if($attendance['late'] > 0){
				$reduction_late = $attendance['late'] * ($reduction/8);
				$this->payroll_model->save_detail(array('payroll_id' => $payroll_id, 'name' => 'Potongan Keterlambatan', 'is_active' => 1, 'category' => 'potongan', 'value' => $reduction_late));
			}

			$reduction_absent = 0;
			if($attendance['not_present'] > 0){
				$reduction_absent = $attendance['not_present'] * $reduction;
				$this->payroll_model->save_detail(array('payroll_id' => $payroll_id, 'name' => 'Potongan Tidak Masuk Kerja', 'is_active' => 1, 'category' => 'potongan', 'value' => $reduction_absent));
			}

			$salary = $full_salary - ($reduction_absent + $reduction_late + $detail_potongan);
			$this->payroll_model->save(array('id' => $payroll_id, 'salary' => $salary, 'attendance' => $attendance['present']));
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Gaji Karyawan berhasil diproses.','success'));
		redirect(base_url('payroll/list'));
	}

	private function checkAttendance($params = array()){
		$not_present	= 0;
		$present		= 0;
		$late			= 0;
		$date_start 	= $params['date_start'];
		$date_end 		= $params['date_end'];
		if($params['join_date'] != '' ){
			$date_start = $params['join_date'];
		}

		if($params['resign_submit'] != ''){
			$date_end 		= $params['resign_submit'];
		}
		
		$list_attendance = $this->attendance_model->gets(array('employee_id' => $params['employee_id'], 'date_start' => $date_start, 'date_end' => $date_end, 'columns' => 'A.date, A.schedule_start, A.schedule_end, A.attendance_start, A.attendance_end'));
		
		foreach($list_attendance AS $item){
			if($item->schedule_start == '00:00' && $item->schedule_end == '00:00'){
				continue;
			}
			if($item->attendance_start == '00:00' && $item->attendance_end == '00:00'){
				$not_present= $not_present + 1;
			}else{
				$present 	= $present + 1;
				$start 		= strtotime($item->date." ".$item->schedule_start);
				$end 		= strtotime($item->date." ".$item->attendance_start);
				$diff 		= ceil(($end - $start) / (60 * 60));
				if($diff > 0){
					$late = $late + $diff;
				}
			}
		}
		return array('not_present' => $not_present, 'present' => $present, 'late' => $late);
	}
	private function getTax($params = array()){
		$result = 0;
		$taxYear = $params['grossYear'] - $params['ptkp'];
		if($taxYear > 0){
			if($taxYear > 500000000){
				$result = ($taxYear*30)/100;
			}else if($taxYear > 250000000){
				$result = ($taxYear*25)/100;
			}else if($taxYear > 50000000){
				$result = ($taxYear*15)/100;
			}else{
				$result = ($taxYear*5)/100;
			}
			$result = $result/12;
		}else{
			$result = 0;
		}
		return $result;
	}
	private function feePosition($gross = 0){
		$result = (5*$gross)/100;
		if($result > 500000){
			$result = 500000;
		}
		return $result;
	}
	private function yearService($month = 0){
		$result = 0;
		if($month >= 216){
			$result = 350000;
		}else if($month >= 180){
			$result = 300000;
		}else if($month >= 144){
			$result = 250000;
		}else if($month >= 108){
			$result = 200000;
		}else if($month >= 72){
			$result = 150000;
		}else if($month >= 36){
			$result = 100000;
		}else if($month >= 12){
			$result = 50000;
		}else{
			$result = 0;
		}
		return $result;
	}

	public function casual($view = 'menunggu'){
		
		$this->load->model(array('site_model'));
		$data['_TITLE_'] 		= 'Payroll Karyawan';
		$data['_PAGE_'] 		= 'payroll/casual';
		$data['_MENU_PARENT_'] 	= 'payroll_employee';
		$data['_MENU_'] 		= 'payroll_casual';
		$data['site_id']		= $this->session->userdata('site');
		$data['site']			= $this->site_model->get(array('id' => $data['site_id'], 'columns' => 'A.id, A.name, A.address'));
		$data['view']			= $view;

		$branch_id				= '';
		if($this->session->userdata('branch') > 1){
			$branch_id 	= $this->session->userdata('branch');
		}

		if($this->input->post('site_id')){
			$this->session->set_userdata('site',$this->input->post('site_id'));
			redirect(base_url('payroll/casual/'.$view));
		}

		if(in_array($this->session->userdata('role'), array(2,15,16,17,18,19,20,21))){
			$data['list_site']		= $this->site_model->gets(array('columns' => 'A.id, A.name, C.code AS company_code', 'indirect' => true, 'branch_id' => $branch_id)); 
		}else{
			$data['list_site']		= $this->site_model->gets(array('columns' => 'A.id, A.name, C.code AS company_code', 'branch_id' => $branch_id));
		}
		$this->view($data); 
	}

	public function casual_ajax($view = FALSE){

		$this->load->model(array('employee_casual_payroll_model'));
		$column_index = $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.status AS status_approval, B.employee_number, B.full_name, B.status AS status_employee, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.total_shift, A.basic_salary, A.management_fee, A.ppn, A.pph, A.salary";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];		
		$params['site_id']		=  $this->session->userdata('site');
		$params['status_approval']		= $view;

		$params['employee_number']		= $_POST['columns'][1]['search']['value'];
		$params['full_name']			= $_POST['columns'][2]['search']['value'];
		$params['status_employee']		= $_POST['columns'][3]['search']['value'];
		// $params['status_approval']		= $_POST['columns'][4]['search']['value'];
		
		$list 	= $this->employee_casual_payroll_model->gets($params);
		$total 	= $this->employee_casual_payroll_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['employee_number']		= $item->employee_number;
			$result['full_name']			= $item->full_name;
			$result['status_employee']		= $item->status_employee;
			$result['periode']				= $item->periode_start.' - '.$item->periode_end;
			$result['total_shift']			= $item->total_shift;
			$result['basic_salary']			= rupiah_round($item->basic_salary);
			$result['total_salary']			= rupiah_round($item->basic_salary * $item->total_shift);
			$result['management_fee']		= rupiah_round($item->management_fee);
			$result['ppn']					= rupiah_round($item->ppn);
			$result['pph']					= rupiah_round($item->pph);
			$result['salary']				= rupiah_round($item->salary);
			$result['status_approval']	= '<p class="text-danger">'.$item->status_approval.'</p>';
			if($item->status_approval ==  'Selesai'){
				$result['status_approval']		= '<p class="text-success">'.$item->status_approval.'</p>';
			}
			if($item->status_approval ==  'Menunggu'){
				$result['status_approval']		= '<p class="text-warning">'.$item->status_approval.'</p>';
			}

			$result['id'] 					= '';
			if($item->status_approval == 'Menunggu'){
				$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			}

			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function casual_approve($view = FALSE)
	{
		if($this->input->post()){
			$this->load->model(array('employee_casual_payroll_model'));
		
			$data['status'] 		= $this->input->post('status');
			foreach ($this->input->post('id') AS $payroll_id) {
				$data['id'] = $payroll_id;
				$result 	=  $this->employee_casual_payroll_model->save($data);
				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger'));
					redirect(base_url('rocasual/payroll/'.$date_start.'/'.$date_finish));
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
			redirect(base_url('payroll/casual/'.$view));
		}
	}

	public function security($view = 'menunggu'){
		
		$this->load->model(array('payroll_config_model', 'site_model', 'payroll_model', 'employee_model'));
		$data['_TITLE_'] 		= 'Payroll Karyawan';
		$data['_PAGE_'] 		= 'payroll/security';
		$data['_MENU_PARENT_'] 	= 'payroll_employee';
		$data['_MENU_'] 		= 'payroll_security';
		$data['view']			= $view;
		$this->view($data); 
	}

	public function security_ajax($view = FALSE){

		$this->load->model(array('payroll_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, B.full_name AS employee_name, B.phone_number, C.name AS site_name, D.name AS position_name, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.salary, A.payment, A.email, A.note, A.tax_calculation";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];

		$params['employee_name']	= $_POST['columns'][2]['search']['value'];
		$params['phone_number']		= $_POST['columns'][3]['search']['value'];
		$params['site_name']		= $_POST['columns'][4]['search']['value'];
		$params['position_name']	= $_POST['columns'][5]['search']['value'];
		$params['periode_start']	= $_POST['columns'][6]['search']['value'];
		$params['periode_end']		= $_POST['columns'][7]['search']['value'];
		$params['tax_calculation']	= $_POST['columns'][8]['search']['value'];
		$params['payment']			= $_POST['columns'][9]['search']['value'];
		$params['email']			= $_POST['columns'][10]['search']['value'];
		$params['note']				= $_POST['columns'][11]['search']['value'];
		
		$params['payment']			= $view;
		$params['position_type']	= 'Security';
		$list 	= $this->payroll_model->gets($params);
		$total 	= $this->payroll_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 				= $i;
			$result['employee_name']	= $item->employee_name;
			$result['phone_number']		= $item->phone_number;
			$result['position_name']	= $item->position_name;
			$result['site_name']		= $item->site_name;
			$result['periode_start']	= $item->periode_start;
			$result['periode_end']		= $item->periode_end;
			$result['note']				= $item->note;;
			$result['tax_calculation']	= format_rupiah((double) $item->tax_calculation);
			$result['salary']			= format_rupiah((double) $item->salary);
			$result['payment']			= '<p class="text-warning">'.$item->payment.'</p>';
			if($item->payment ==  'Selesai'){
				$result['payment']			= '<p class="text-success">'.$item->payment.'</p>';
			}
			$result['email']		= '<p class="text-warning">'.$item->email.'</p>';
			if($item->email ==  'Terkirim'){
				$result['email']			= '<p class="text-success">'.$item->email.'</p>';
			}else if($item->email == 'Gagal'){
				$result['email']			= '<p class="text-danger">'.$item->email.'</p>';
			}
			$result['action'] 			=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("ropayroll/preview/".$item->id).'">Lihat</a>
				<a href="'.base_url('ropayroll/pdf/' . $item->id).'" style="cursor:pointer;" class="btn-sm btn-warning btn-block">PDF</a>
				<a href="'.base_url('payroll/whatsapp/security?id=' . $item->id).'" style="cursor:pointer;" class="btn-sm btn-secondary btn-block">Whatsapp</a>';
			if($item->payment == 'Menunggu'){
				$result['action'] .= 
				'<a href="'.base_url('payroll/form/' . $item->id).'" style="cursor:pointer;" class="btn-sm btn-success btn-block">Ubah</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function security_approve($view = FALSE)
	{
		if($this->input->post()){
			$this->load->model(array('employee_casual_payroll_model'));
		
			$data['status'] 		= $this->input->post('status');
			foreach ($this->input->post('id') AS $payroll_id) {
				$data['id'] = $payroll_id;
				$result 	=  $this->employee_casual_payroll_model->save($data);
				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger'));
					redirect(base_url('rocasual/payroll/'.$date_start.'/'.$date_finish));
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
			redirect(base_url('payroll/casual/'.$view));
		}
	}

	public function security_export($view = FALSE)
	{
		$this->load->model(array('payroll_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$params 			= $this->input->get();
		$params['payment']			= $view;
		$params['position_type']	= 'Security';

		$title = 'payroll_security';
		$files = glob(FCPATH."files/".$title."/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}
		
		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
		->setLastModifiedBy($this->session->userdata('name'))
		->setTitle($title)
		->setSubject($title)
		->setDescription($title)
		->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Nomor Karyawan");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("C".$i, "Jabatan");
		$excel->getActiveSheet()->setCellValue("D".$i, "Site Bisnis");
		$excel->getActiveSheet()->setCellValue("E".$i, "Nomor Rekening");
		$excel->getActiveSheet()->setCellValue("F".$i, "Nama Bank");
		$excel->getActiveSheet()->setCellValue("G".$i, "BPJS KS Perusahaan");
		$excel->getActiveSheet()->setCellValue("H".$i, "BPJS TK Perusahaan");
		$excel->getActiveSheet()->setCellValue("I".$i, "Potongan BPJS Kesehatan");
		$excel->getActiveSheet()->setCellValue("J".$i, "Potongan BPJS Ketenagakerjaan");
		$excel->getActiveSheet()->setCellValue("K".$i, "PPH 21");
		$header_column = 'J';

		$list_componet = $this->payroll_model->gets_detail(array('site_id' => 1, 'columns' => 'A.name', 'groupby' => 'A.name',));
		foreach($list_componet AS $value){
			$excel->getActiveSheet()->setCellValue($header_column.$i, $value->name);
			$header_column++;
		}
		$excel->getActiveSheet()->setCellValue($header_column.$i, "Gaji Bersih");

		$params['columns'] 		= "B.employee_number, B.full_name, B.bank_account, B.bank_name, C.name AS position_name, D.name AS site_name, A.id AS payroll_id, A.salary, A.tax_calculation, A.bpjs_ks_company, A.bpjs_jht_company, A.bpjs_ks, A.bpjs_jht, A.tax_calculation";
		$params['orderby'] 		= 'B.full_name';
		$params['order']		= 'ASC';
		$list 	= $this->payroll_model->gets($params);
		$i++;
		$no = 1;
		foreach($list as $item)
		{
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->employee_number);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->position_name);
			$excel->getActiveSheet()->setCellValueExplicit("D".$i, $item->site_name);
			$excel->getActiveSheet()->setCellValueExplicit("E".$i, $item->bank_account);
			$excel->getActiveSheet()->setCellValueExplicit("F".$i, $item->bank_name);
			$excel->getActiveSheet()->setCellValueExplicit("G".$i, $item->bpjs_ks_company);
			$excel->getActiveSheet()->setCellValueExplicit("H".$i, $item->bpjs_jht_company);
			$excel->getActiveSheet()->setCellValueExplicit("I".$i, '-'.$item->bpjs_ks);
			$excel->getActiveSheet()->setCellValueExplicit("J".$i, '-'.$item->bpjs_jht);
			$excel->getActiveSheet()->setCellValueExplicit("K".$i, '-'.$item->tax_calculation);
			
			$column = 'J';
			foreach($list_componet AS $value){
				$detail = $this->payroll_model->get_detail(array('payroll_id' => $item->payroll_id, 'name' => $value->name, 'A.category, A.value'));
				if($detail){
					if($detail->category == 'potongan'){
						$detail->value = '-'.$detail->value;	
					}
					$excel->getActiveSheet()->setCellValueExplicit($column.$i, $detail->value);
				}
				$column++;
			}
			$excel->getActiveSheet()->setCellValueExplicit($column.$i, $item->salary);
			$no++;
			$i++;
		}
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

    public function generate_payroll(){
		$this->load->model(array('payroll_model', 'employee_model'));

		$list_payroll	= $this->payroll_model->generate_payroll(array('submission' => array('Draft', 'Menunggu'), 
		'site_id' => $this->session->userdata('site'), 
		'columns' => 'A.id, A.employee_id, A.attendance, A.overtime_hour, A.basic_salary, A.overtime_calculation, A.bpjs_ks, A.bpjs_jht, A.tax_calculation, A.salary, A.periode_start, A.periode_end, A.salary_note, B.full_name, B.employee_number, B.bank_account, C.code AS company_code, D.name AS site_name, E.name AS position'));

		foreach ($list_payroll as $payroll) {	
			$payroll_id 	= $payroll->id;
			$income			= $this->payroll_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'pendapatan', 'columns' => 'A.name, A.value'));
			$outcome		= $this->payroll_model->gets_detail(array('payroll_id' => $payroll_id, 'category' => 'potongan', 'columns' => 'A.name, A.value'));

			$start_date = substr($payroll->periode_start, 8, 2).' '.get_month(substr($payroll->periode_start, 5, 2));
			$end_date 	= substr($payroll->periode_end, 8, 2).' '.get_month(substr($payroll->periode_end, 5, 2)).' '.substr($payroll->periode_end, 0, 4);

			$kehadiran 		  = "";
			$jumlah_kehadiran = "";
			if($payroll->attendance){
				$kehadiran 			= 'JUMLAH KEHADIRAN <br>';
				$jumlah_kehadiran 	= ': '.$payroll->attendance;
			}
			$lembur 		= "";
			$jumlah_lembur 	= "";
			if($payroll->overtime_hour){
				$lembur 		= 'JUMLAH JAM LEMBUR <br>';
				$jumlah_lembur 	= $payroll->overtime_hour;
			}
			$gaji_pokok 	= "0";
			if($payroll->basic_salary){
				$gaji_pokok = $payroll->basic_salary;
			}
			$pendapatan = "";
			if($payroll->overtime_calculation){
				$pendapatan .= '<tr><td>Lembur</td><td>: Rp.</td><td style="float:right;">'.rupiah($payroll->overtime_calculation).'</td></tr>';
			}
			foreach( $income AS $item){
				if($item->name != ''){
				    $pendapatan .= "<tr><td >".$item->name." &nbsp;&nbsp;</td><td>: Rp.</td><td style='float:right;'>".rupiah($item->value)."</td></tr>";
				}
			}
			$potongan 	= "";
			$potongan .= '<tr><td>BPJS Kesehatan</td><td>: Rp.</td><td style="float:right;">'.rupiah($payroll->bpjs_ks).'</td></tr>';
			$potongan .= '<tr><td>BPJS Ketenagakerjaan</td><td>: Rp.</td><td style="float:right;">'.rupiah($payroll->bpjs_jht).'</td></tr>';
			$potongan .= '<tr><td>PPH21</td><td>: Rp.</td><td style="float:right;">'.rupiah($payroll->tax_calculation).'</td></tr>';
			
			foreach( $outcome AS $item){
				if($item->name != ''){
				    $potongan .= "<tr><td >".$item->name." &nbsp;&nbsp;</td><td>: Rp.</td><td style='float:right;'>".rupiah($item->value)."</td></tr>";
				}
			}

			$image_url = 'files/'.strtolower($payroll->company_code).'.jpg';
			$preview = '<section class="invoice">
				<div class="row">
					<div class="col-12">
						<h2 class="page-header">
							<img width="100%" src="'.base_url($image_url).'"/>
						</h2>
					</div>
				</div>
				<div class="row invoice-info m-4">
					<div class="col-sm-3 invoice-col">
						<address>
							PERIODE BULAN <br>
							NO REKENING <br>
							NAMA <br>
							NOMOR KARYAWAN <br>
							JABATAN <br>
							SITE <br>
							'.$kehadiran.'
							'.$lembur.'
						</address>
					</div>
					<div class="col-sm-6 invoice-col">
						<?php 
							
						?>
						<address>
							: '.$start_date.' - '.$end_date.'<br>
							: '.$payroll->bank_account.'<br>
							: '.$payroll->full_name.'<br>
							: '.$payroll->employee_number.'<br>
							: '.$payroll->position.'<br>
							: '.$payroll->site_name.'<br>
							  '.$jumlah_kehadiran.'<br>
							  '.$jumlah_lembur.'<br>
						</address>
					</div>
				</div>
				<div class="row invoice-info">
					<div class="col-sm-6 invoice-col">
					&nbsp;
					</div>
					<div class="col-sm-6 invoice-col">
						<address >
							<strong>Gaji bersih diterima : '.format_rupiah($payroll->salary).'</strong>
						</address>
					</div>
				</div>
				<div class="row invoice-info m-4">
					<div class="col-sm-6 invoice-col">
						<address>
							<table style="border:none">
								<tr><td><strong>Pendapatan</strong></td><td></td><td></td></tr>
								<tr><td>Gaji Pokok</td><td>: Rp.</td><td style="float:right;">'.rupiah($gaji_pokok).'</td></tr>
								'.$pendapatan.'
        					</table>
        				</address>
      				</div>
      				<div class="col-sm-6 invoice-col">
      					<address>
      						<table style="border:none">
      							<tr><td><strong>Potongan</strong></td><td></td><td></td></tr>
								'.$potongan.'
							</table>
						</address>
					</div>
				</div>
				<div class="row invoice-info m-4" style="font-size: 12px;">
					<address >
						<strong>Catatan : </strong>'.$payroll->salary_note.'
					</address>
				</div>
			</section>';

			$pdf  = '<table width="100%">';
			$pdf .= '<tr><td width="25%">PERIODE BULAN</td><td> : '.$start_date.' - '.$end_date.'</td></tr>
				<tr><td>NO REKENING</td><td> : '.$payroll->bank_account.'</td></tr>
				<tr><td>NAMA</td><td> : '.$payroll->full_name.'</td></tr>
				<tr><td>NOMOR KARYAWAN</td><td> : '.$payroll->employee_number.'</td></tr>
				<tr><td>JABATAN</td><td> : '.$payroll->position.'</td></tr>
				<tr><td>SITE</td><td> : '.$payroll->site_name.'</td></tr>';
			if($payroll->attendance){
				$pdf .= '<tr><td>JUMLAH KEHADIRAN</td><td> : '.$payroll->attendance.'</td></tr>';
			}
			if($payroll->overtime_hour){
				$pdf .= '<tr><td>JUMLAH JAM LEMBUR</td><td> : '.$payroll->overtime_hour.'</td></tr>';
			}

			$pdf .= '</table><table width="100%">';
			$pdf .= '<tr><td width="60%">&nbsp;</td><td><strong>Gaji Bersih diterima : '.format_rupiah($payroll->salary).'</strong></td></tr>';
			$pdf .= '</table><br><br><table width="100%">';
			$pdf .= '<tr><td width="50%"><table>';
			$pdf .= '<tr><td><strong>Pendapatan</strong></td><td></td><td></td></tr>';
			if($payroll->basic_salary){
				$pdf .= "<tr><td>Gaji Pokok</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->basic_salary)."</td></tr>";
			}else{
				$pdf .= "<tr><td>Gaji Pokok</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
			}
			if($payroll->overtime_calculation){
				$pdf .= "<tr><td>Lembur</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->overtime_calculation)."</td></tr>";
			}
			foreach( $income AS $item):
				if($item->name != ''){
				    $pdf .= "<tr><td >".$item->name."</td><td>: Rp.</td><td style='float:right'>".rupiah($item->value)."</td></tr>";
				}
			endforeach;
			$pdf .= '</table></td>';
			$pdf .= '<td width="50%"><table>';
			$pdf .= '<tr><td><strong>Potongan</strong></td><td></td><td></td></tr>';
			if($payroll->bpjs_ks){
				$pdf .= "<tr><td>BPJS Kesehatan</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->bpjs_ks)."</td></tr>";
			}else{
				$pdf .= "<tr><td>BPJS Kesehatan</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
			}
			if($payroll->bpjs_jht){
				$pdf .= "<tr><td>BPJS Ketenagakerjaan</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->bpjs_jht)."</td></tr>";
			}else{
				$pdf .= "<tr><td>BPJS Ketenagakerjaan</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
			}
			if($payroll->tax_calculation){
				$pdf .= "<tr><td>PPH21</td><td>: Rp.</td><td style='float:right;'>".rupiah($payroll->tax_calculation)."</td></tr>";
			}else{
				$pdf .= "<tr><td>PPH21</td><td>: Rp.</td><td style='float:right;'>".rupiah(0)."</td></tr>";
				
			}
			foreach($outcome AS $item):
				if($item->name != ''){
				    $pdf .= "<tr><td >".$item->name."</td><td>: Rp.</td><td style='float:right'>".rupiah($item->value)."</td></tr>";
				}
			endforeach;
			$pdf .= '</table></td></tr>';
			$pdf .= '</table>';
			$pdf .= '</table></td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr></table>';
			$pdf .= '<table width="100%"><tr><td><strong>Catatan : </strong>'.$payroll->salary_note.'</td></tr></table><br>';
			$pdf .= '<div syle="width=100%"></hr></div><br pagebreak="true"/>';

			$this->payroll_model->save(array('id' => $payroll->id, 'preview' => $preview, 'pdf' => $pdf, 'company_code' => $payroll->company_code));
			// print_prev($preview);
		}
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roposition extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();

		if(!in_array($this->session->userdata('role'), array(2,3,8))){
			redirect(base_url());
		}

		if($this->session->userdata('site') == ''){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan pilih site bisnis.','danger'));
			redirect(base_url());	
		}
	}

	public function list(){
		$data['_TITLE_'] 		= 'Jabatan';
		$data['_PAGE_'] 		= 'roposition/list';
		$data['_MENU_PARENT_'] 	= 'roemployee';
		$data['_MENU_'] 		= 'roposition';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('position_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.name, A.description, B.name AS company_name';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['name']			= $_POST['columns'][1]['search']['value'];
		$params['description']	= $_POST['columns'][2]['search']['value'];

		$list 	= $this->position_model->gets($params);
		$total 	= $this->position_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['name'] 		= $item->name;
			$result['description'] 	= $item->description;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action"  style="cursor:pointer;" data-href="'. base_url("roposition/preview/".$item->id).'"><i class="fas fa-eye text-white"></i></a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function preview($position_id = FALSE)
	{
		$this->load->model('position_model');
		$data['_TITLE_'] 		= 'Preview Jabatan';
		$data['_PAGE_']	 		= 'position/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'position';

		$data['id'] = $position_id;

		
		if (!$position_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('position/list'));
		}

		$data['preview'] = $this->position_model->preview(array('id' => $position_id));
		$this->load->view('position/preview', $data);
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Workday extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function list(){
		$data['_TITLE_'] 		= 'Hari Kerja';
		$data['_PAGE_'] 		= 'workday/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'workday';
		$this->view($data);
	}

	public function list_ajax(){

		$this->load->model(array('site_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "B.code AS company_code, A.id, A.name AS site_name, A.address AS site_address";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['company_code']	= $_POST['columns'][1]['search']['value'];
		$params['site_name']	= $_POST['columns'][2]['search']['value'];
		$params['site_address']	= $_POST['columns'][3]['search']['value'];
		
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		$list 	= $this->site_model->gets_ro_site($params);
		$total 	= $this->site_model->gets_ro_site($params, TRUE);
		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['company_code']	= $item->company_code;
			$result['site_name']	= $item->site_name;
			$result['site_address']	= $item->site_address;
			$result['action'] 		=
				'<a class="btn-sm btn-success btn-block" href="'.base_url("workday/site/".$item->id).'">Detail</a>';
			array_push($data, $result);
			$i++;
		}
				
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}

	public function site($site_id = FALSE){

		$this->load->model(array('site_model'));

		$data['_TITLE_'] 		= 'Hari Kerja';
		$data['_PAGE_'] 		= 'workday/site';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'workday';
		$data['site'] 			= $this->site_model->get_site(array('id' => $site_id, 'columns' => 'A.id, A.name, A.address'));
		$this->view($data);
	}

	public function site_ajax($site_id = FALSE){

		$this->load->model(array('workday_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.day, A.hours_start, A.hours_end, A.shift";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['day']			= $_POST['columns'][1]['search']['value'];
		$params['shift']		= $_POST['columns'][2]['search']['value'];
		$params['hours_start']	= $_POST['columns'][3]['search']['value'];
		$params['hours_end']	= $_POST['columns'][4]['search']['value'];
		$params['site_id']		= $site_id;
		
		$list 	= $this->workday_model->gets($params);
		$total 	= $this->workday_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{			
			$result['no'] 			= $i;
			$result['day']			= $item->day;
			$result['shift']		= $item->shift;
			$result['hours_start']	= $item->hours_start;
			$result['hours_end']	= $item->hours_end;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("workday/preview/".$site_id."/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("workday/form/".$site_id."/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("workday/delete/".$site_id."/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function form($site_id = FALSE, $workday_id = FALSE)
	{
		$this->load->model(array('workday_model'));

		$data['id'] 		= $workday_id;
		$data['site_id'] 	= $site_id;
		$data['shift']		= '';
		$data['day']		= '';
		$data['hours_start']= '';
		$data['hours_end']	= '';
		
		if($this->input->post()){
			$data['day']		= $this->input->post('day');
			$data['hours_start']= $this->input->post('hours_start');
			$data['hours_end'] 	= $this->input->post('hours_end');
			$data['shift'] 		= $this->input->post('shift');
			$data['site_id'] 	= $this->input->post('site_id');
			$data['id'] 		= $this->input->post('id');
			
			$this->form_validation->set_rules('day', '', 'required');
			$this->form_validation->set_rules('hours_start', '', 'required');
			$this->form_validation->set_rules('hours_end', '', 'required');
			$this->form_validation->set_rules('shift', '', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$this->workday_model->save($data);
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			}
			redirect(base_url('workday/site/'.$data['site_id']));
		}

		if ($workday_id)
		{
			$data = (array) $this->workday_model->get(array('id' => $workday_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('workday/site/'.$data['site_id']));
			}
		}
		$data['_TITLE_'] 		= 'Hari Kerja';
		$data['_PAGE_'] 		= 'workday/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'workday';
		return $this->view($data);
	}

	public function preview($site_id = FALSE, $workday_id=FALSE)
	{
		$this->load->model(array('workday_model'));
		$data['_TITLE_'] 		= 'Preview Hari Kerja';
		$data['_PAGE_']	 		= 'workday/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'workday';
		
		if (!$workday_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('workday/site/'.$site_id));
		}

		$data['preview'] = $this->workday_model->preview(array('id' => $workday_id));
		$this->load->view('workday/preview', $data);
	}

	public function delete($site_id = FALSE, $workday_id = false)
	{
		$this->load->model('workday_model');
		if ($workday_id)
		{
			$data =  $this->workday_model->get(array('id' => $workday_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $workday_id, 'is_active' => 0);
				$result = $this->workday_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('workday/site/'.$site_id));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('workday/site/'.$site_id));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('workday/site/'.$site_id));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('workday/site/'.$site_id));
		}
	}
}
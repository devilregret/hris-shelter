<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roworkday extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!in_array($this->session->userdata('role'), array(2,3,8))){
			redirect(base_url());
		}
	}

	public function list(){

		$this->load->model(array('site_model'));

		$data['_TITLE_'] 		= 'Hari Kerja';
		$data['_PAGE_'] 		= 'roworkday/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'roworkday';
		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address'));
		$this->view($data);
	}

	public function list_ajax(){

		$this->load->model(array('roworkday_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.day, A.hours_start, A.hours_end, B.shift";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['day']			= $_POST['columns'][1]['search']['value'];
		$params['shift']		= $_POST['columns'][2]['search']['value'];
		$params['hours_start']	= $_POST['columns'][3]['search']['value'];
		$params['hours_end']	= $_POST['columns'][4]['search']['value'];
		$params['site_id']		= $this->session->userdata('site');
		
		$list 	= $this->roworkday_model->gets($params);
		$total 	= $this->roworkday_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{			
			$result['no'] 			= $i;
			$result['day']			= $item->day;
			$result['shift']		= $item->shift;
			$result['hours_start']	= $item->hours_start;
			$result['hours_end']	= $item->hours_end;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action"  style="cursor:pointer;" data-href="'. base_url("roworkday/preview/".$item->id).'"><i class="fas fa-eye text-white"></i></a>
				<a class="btn-sm btn-success" href="'.base_url("roworkday/form/".$item->id).'"><i class="fas fa-edit"></i></a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger" style="cursor:pointer;" data-href="'.base_url("roworkday/delete/".$item->id).'"><i class="far fa-trash-alt text-white"></i></a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function form($workday_id = FALSE)
	{
		$this->load->model(array('roworkday_model', 'roshift_model'));

		$data['id'] 		= '';
		$data['shift_id']	= '';
		$data['day']		= '';
		$data['hours_start']= '';
		$data['hours_end']	= '';
		
		if($this->input->post()){
			$data['day']		= $insert['day']		= $this->input->post('day');
			$data['hours_start']= $insert['hours_start']= $this->input->post('hours_start');
			$data['hours_end'] 	= $insert['hours_end']	= $this->input->post('hours_end');
			$data['shift_id'] 	= $insert['shift_id']	= $this->input->post('shift_id');
			$data['site_id'] 	= $insert['site_id']	= $this->session->userdata('site');
			$data['id'] 		= $insert['id']			= $this->input->post('id');

			$insert['site_id']	= $data['site_id'];
			
			$this->form_validation->set_rules('day', '', 'required');
			$this->form_validation->set_rules('hours_start', '', 'required');
			$this->form_validation->set_rules('hours_end', '', 'required');
			$this->form_validation->set_rules('shift_id', '', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$this->roworkday_model->save($insert);
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			}
			redirect(base_url('roworkday/list'));
		}

		if ($workday_id)
		{
			$data = (array) $this->roworkday_model->get(array('id' => $workday_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('roworkday/list'));
			}
		}

		$data['shift']	= $this->roshift_model->gets(array('site_id' =>$this->session->userdata('site'), 'columns' => 'A.id, A.shift'));

		$data['_TITLE_'] 		= 'Hari Kerja';
		$data['_PAGE_'] 		= 'roworkday/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'roworkday';
		return $this->view($data);
	}

	public function preview($workday_id=FALSE)
	{
		$this->load->model(array('roworkday_model'));
		$data['_TITLE_'] 		= 'Preview Hari Kerja';
		$data['_PAGE_']	 		= 'workday/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'workday';
		
		if (!$workday_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('workday/list'));
		}

		$data['preview'] = $this->roworkday_model->preview(array('id' => $workday_id));
		$this->load->view('roworkday/preview', $data);
	}

	public function delete($workday_id = false)
	{
		$this->load->model('roworkday_model');
		if ($workday_id)
		{
			$data =  $this->roworkday_model->get(array('id' => $workday_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $workday_id, 'is_active' => 0);
				$result = $this->roworkday_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('roworkday/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('roworkday/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('roworkday/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('roworkday/list'));
		}
	}
}
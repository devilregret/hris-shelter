<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Scheduler extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		require APPPATH.'third_party/PHPMailer/Exception.php';
		require APPPATH.'third_party/PHPMailer/PHPMailer.php';
		require APPPATH.'third_party/PHPMailer/SMTP.php';
	}
	
	public function benefit_report()
	{
		$this->load->model(array('email_model', 'user_model', 'site_model', 'employee_model'));
		$role 		= array();
		$role 		= array_merge($role, $this->config->item('benefit'));
		$role 		= array_merge($role, $this->config->item('personalia'));		
		$receiver = $this->user_model->gets(array('role_in' => $role, 'columns' => 'A.email'));

		$email_template 	= $this->email_model->get(array('id' => 8));
		$email['email'] 	= $email_template->email_sender;
		$email['sender'] 	= $email_template->name_sender;
		$email['receiver'] 	= $receiver;
		$email['subject'] 	= $email_template->subject;
		$email['message'] 	= $email_template->content;

		$report 			= "";	
		$break_site 		= $this->site_model->gets_site(array('non_active' => TRUE, 'assurance' => TRUE, 'columns' => 'A.id, A.name, A.address, A.contract_status, A.health_insurance_status, A.labor_insurance_status, C.name AS company_name'));
		if($break_site){
			$report .= "<p><h3>Daftar Site Bisnis Putus Kontrak </h3></p>";
			$report .= '<table width="100%" cellpadding="0" border="1" cellspacing="0" style="min-width:100%;">
				<thead><tr>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">#</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Site Bisnis</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Alamat</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Bisnis Unit</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Status Kontrak</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Jumlah Karyawan</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Asuransi Kesehatan</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Asuransi Ketenagakerjaan</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">#</th>
				</tr></thead><tbody>';
		}
		
		$no = 1;
		foreach ($break_site as $item){	
			$count_employee	= $this->employee_model->count(array('site_id' => $item->id, 'status_approval' => 3), TRUE);
			$report .= '<tr>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$no.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$item->name.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px" >'.$item->address.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$item->company_name.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$item->contract_status.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$count_employee.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$item->health_insurance_status.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$item->labor_insurance_status.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px"><a style="cursor:pointer;" href="'.base_url("monitoring_benefit/site").'">Lihat</a></td></tr>';
				$no++;
		}
		if($break_site){
			$report .= '</table>';
		}

		$employee_resign 	= $this->employee_model->gets(array('assurance_active' => TRUE, 'columns' => 'A.id, A.id_card, A.full_name, A.address_card, B.name AS site_name, C.name AS company_name, A.benefit_health, A.benefit_labor'));
		if($employee_resign){
			$report .= "<p><h3>Daftar Karyawan Resign yang masih terdaftar asuransi</h3></p>";
			$report .= '<table width="100%" cellpadding="0" border="1" cellspacing="0" style="min-width:100%;">
				<thead><tr>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">#</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">NIK KTP</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Nama Karyawan</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Alamat Karyawan</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Site Bisnis</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Bisnis Unit</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Asuransi Kesehatan</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Asuransi Ketenagakerjaan</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">#</th>
				</tr></thead><tbody>';
		}
		
		$no = 1;
		foreach ($employee_resign as $item){
			$health_insurance_status	= ($item->benefit_health!=''? '<span class="text-success">Terdaftar</span>': '<span class="text-danger">Dihentikan</span>');
			$labor_insurance_status		= ($item->benefit_labor!=''? '<span class="text-success">Terdaftar</span>': '<span class="text-danger">Dihentikan</span>');
			$report .= '<tr>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$no.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$item->id_card.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px" >'.$item->full_name.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$item->address_card.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$item->site_name.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$item->company_name.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$health_insurance_status.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$labor_insurance_status.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px"><a style="cursor:pointer;" href="'.base_url("monitoring_benefit/employee").'">Lihat</a></td></tr>';
				$no++;
		}

		if($employee_resign){
			$report .= '</table>';
		}

		$employee_new 	= $this->employee_model->gets(array('assurance_non_active' => TRUE, 'columns' => 'A.id, A.id_card, A.full_name, A.address_card, B.name AS site_name, B.id AS site_id, C.name AS company_name, A.benefit_health, A.benefit_labor', 'page' => 0, 'limit' => 100));
		if($employee_new){
			$report .= "<p><h3>Daftar Karyawan Resign yang masih terdaftar asuransi</h3></p>";
			$report .= '<table width="100%" cellpadding="0" border="1" cellspacing="0" style="min-width:100%;">
				<thead><tr>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">#</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">NIK KTP</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Nama Karyawan</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Alamat Karyawan</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Site Bisnis</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Bisnis Unit</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Asuransi Kesehatan</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Asuransi Ketenagakerjaan</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">#</th>
				</tr></thead><tbody>';
		}
		
		$no = 1;
		foreach ($employee_new as $item){
			$report .= '<tr>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$no.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$item->id_card.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px" >'.$item->full_name.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$item->address_card.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$item->site_name.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$item->company_name.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$item->benefit_health.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$item->benefit_labor.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px"><a style="cursor:pointer;" href="'.base_url("benefit_employee/list?site=".$item->site_id).'">Lihat</a></td></tr>';
				$no++;
		}
		
		if($employee_new){
			$report .= '</table>';
		}
		
		$email['message'] 	= str_replace("{LAPORAN_BENEFIT}", $report, $email['message']);
		if($report != ""){
			$this->send_mail($email);
		}
	}

	public function contract_expired()
	{
		$this->load->model(array('email_model', 'employee_contract_model', 'user_model', 'company_model'));

		$role 		= array();
		$role 		= array_merge($role, $this->config->item('personalia'));		
		$receiver = $this->user_model->gets(array('role_in' => $role, 'columns' => 'A.email'));

		$email_template 	= $this->email_model->get(array('id' => 6));
		$email['email'] 	= $email_template->email_sender;
		$email['sender'] 	= $email_template->name_sender;
		$email['receiver'] 	= $receiver;
		$email['subject'] 	= $email_template->subject;
		$email['message'] 	= $email_template->content;

		$company 			= $this->company_model->gets(array('columns' => 'A.id, A.name'));
		$expired 			= "";	
		
		foreach ($company as $item){
			$expired .= "<p><h3>Daftar Kontrak Habis Pada Bisnis unit ".$item->name." </h3></p>";
			$list_expired['Kontrak Habis'] 	= array('day' => 0, 'summary' => $this->employee_contract_model->gets_expired(array('expired' => 0, 'company_id' => $item->id,'columns' => 'COUNT(A.employee_id) AS summary, C.name AS site_name, D.code AS company_code')));
			for($i=1; $i <= 30;$i++){
				$contract_expired = $this->employee_contract_model->gets_expired(array('expired' => $i, 'company_id' => $item->id, 'columns' => 'COUNT(A.employee_id) AS summary, C.name AS site_name, D.code AS company_code'));
				if($contract_expired){
					$detail_contract['day']		= $i;
					$detail_contract['summary']	= $contract_expired; 
					$list_expired['Kontrak Kurang '.$i.' Hari'] 	= $detail_contract;
				}
			}
			$expired .= '<table width="100%" cellpadding="0" border="1" cellspacing="0" style="min-width:100%;">
				<thead><tr>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">#</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Keterangan</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Detail</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Total</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">#</th>
				</tr></thead><tbody>';
			$no = 1;
			foreach ($list_expired as $key => $item){
				$total_summary = 0;
				$expired .= '<tr>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$no.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$key.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px" >
					<table width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" rules="rows"><tbody>';
					foreach ($item['summary'] as $detail){ 
						$total_summary += $detail->summary;
						$expired .= '<tr><td width="80%" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$detail->company_code.' - '.$detail->site_name.'</td>
								<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px;width:250px;">'.$detail->summary.' Karyawan</td></tr>';
					}
				$expired  .= '</tbody></table></td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$total_summary.' Karyawan</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px"><a style="cursor:pointer;" href="'.base_url("employee_contract/list/direct/".$item['day']).'">Lihat</a></td></tr>';
				$no++;
			}
			$expired .= '</tbody></table><br>';
		}		
		$email['message'] 	= str_replace("{KONTRAK_KARYAWAN}", $expired, $email['message']);
		print_prev($email['message']);
		die();
		if($expired != ""){
			$this->send_mail($email);
		}
	}

	public function new_applicant()
	{
		$this->load->model(array('email_model', 'candidate_model', 'user_model'));
		$role 		= array();
		$role 		= array_merge($role, $this->config->item('recruitment'));		
		$receiver = $this->user_model->gets(array('role_in' => $role, 'columns' => 'A.email'));
		
		$email_template 	= $this->email_model->get(array('id' => 7));
		$email['email'] 	= $email_template->email_sender;
		$email['sender'] 	= $email_template->name_sender;
		$email['receiver'] 	= $receiver;
		$email['subject'] 	= $email_template->subject;
		$email['message'] 	= $email_template->content;

		$list_new			= array();
		$new 				= "";	
		for($i=0; $i <= 60;$i++){
			$new_applicant = $this->candidate_model->gets_new(array('date' => $i, 'columns' => ' COUNt(A.id) AS summary, B.name AS preference'));
			if($new_applicant){
				$detail_contract['day']			= $i;
				$detail_contract['summary']		= $new_applicant;
				if($i == 0){
					$list_new['Pelamar Hari ini'] 	= $detail_contract;	
				} else{
					$date = date('Y-m-d', strtotime("-".$i." day"));
					$list_new['Pelamar pada tanggal '.$date] 	= $detail_contract;	
				}
			}
		}
		$new .= '<table width="100%" cellpadding="0" border="1" cellspacing="0" style="min-width:100%;">
				<thead><tr>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">#</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Keterangan</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Detail</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Total</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">#</th>
				</tr></thead><tbody>';
			$no = 1;
			foreach ($list_new as $key => $item){
				$total_summary = 0;
				$new .= '<tr>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$no.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$key.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">
					<table width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" rules="rows"><tbody>';
					foreach ($item['summary'] as $detail){ 
						$total_summary += $detail->summary;
						$new .= '<tr><td width="80%" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$detail->preference.'</td>
								<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px;width:250px;"">'.$detail->summary.' Pelamar</td></tr>';
					}
				$new  .= '</tbody></table></td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">'.$total_summary.' Pelamar</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px"><a style="cursor:pointer;" href="'.base_url("employee_contract/list/direct/".$item['day']).'">Lihat</a></td></tr>';
				$no++;
			}
			$new .= '</tbody></table>';
				
		$email['message'] 	= str_replace("{KONTRAK_KARYAWAN}", $new, $email['message']);
		if($new != ""){
			$this->send_mail($email);
		}
	}

	public function legal_expired()
	{
		$this->load->model(array('email_model', 'legality_model', 'user_model'));
		$role 		= array();
		$role 		= array_merge($role, $this->config->item('legal'));
		$role 		= array_merge($role, $this->config->item('directur'));		
		$receiver = $this->user_model->gets(array('role_in' => $role, 'columns' => 'A.email'));
		
		$email_template 	= $this->email_model->get(array('id' => 9));
		$email['email'] 	= $email_template->email_sender;
		$email['sender'] 	= $email_template->name_sender;
		$email['receiver'] 	= $receiver;
		$email['subject'] 	= $email_template->subject;
		$email['message'] 	= $email_template->content;

		$list_expired = $this->legality_model->gets_expired(array("expired_month" => 3, "columns" => "A.id, A.name, B.code AS company_code, C.name AS branch_name, DATE_FORMAT(A.date_start, '%d/%m/%Y') AS date_start, DATE_FORMAT(A.date_end, '%d/%m/%Y') AS date_end, A.note, A.files"));
		$content = '<table width="100%" cellpadding="0" border="1" cellspacing="0" style="min-width:100%;">
				<thead><tr>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">No</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">Nama Legalitas</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">Bisnis Unit</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">Branch</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">Berlaku</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">Berakhir</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">Catatan</th>
				<th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">Dokumen</th>
				</tr></thead><tbody>';
			$no = 1;
			foreach ($list_expired as $item){
				$content .= '<tr>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">'.$no.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">'.$item->name.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">'.$item->company_code.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">'.$item->branch_name.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">'.$item->date_start.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">'.$item->date_end.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">'.$item->note.'</td>
				<td style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:30px">'.$item->files.'</td></tr>';
				$no++;
			}
		$content .= '</tbody></table>';
		$email['message'] 	= str_replace("{DOKUMEN_LEGAL}", $content, $email['message']);
		if($list_expired){
			$this->send_mail($email);
		}
	}

	private function send_mail($data = array()){

        $mail 				= new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPOptions 	= array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
					)
				);
		// $mail->SMTPDebug 	= SMTP::DEBUG_SERVER;
		$mail->Host 		= $this->config->item('host');
		$mail->Port 		= $this->config->item('port');
		$mail->SMTPSecure 	= PHPMailer::ENCRYPTION_STARTTLS;
		$mail->SMTPAuth 	= true;
		$mail->SMTPSecure 	= 'ssl';
		$mail->Username 	= $this->config->item('email');
		$mail->Password 	= $this->config->item('password');
		$mail->setFrom($data['email'], $data['sender']);
		foreach ($data['receiver'] as $receiver){ 
			$mail->addAddress($receiver->email);
		}
		// if($data['carbon_copy'] != ''){
		// 	$mail->AddCC($data['carbon_copy']);
		// }

		$mail->Subject 		= $data['subject'];
		$mail->msgHTML($data['message']);
		if (!$mail->send()) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function reset_account()
	{
		$this->load->model(array('employee_model', 'user_model'));
		$list_employee = $this->employee_model->gets(array('site_id' => 2, 'columns' => 'A.id, A.full_name, A.id_card, A.email'));
		foreach($list_employee AS $employee){
			$user = $this->user_model->get(array('employee_id' => $employee->id));
			$data['id'] 		= $user->id; 
			$data['full_name'] 	= $employee->full_name; 
			$data['email'] 		= $employee->email; 
			$data['username'] 	= $employee->id_card;
			$this->user_model->save($data);
			if(!$data['id']){
			    continue;
			}
			print_prev($data);
		}
		// print_prev($list_employee);
	}

	public function reset_account_site($site_id = FALSE)
	{
		$this->load->model(array('employee_model', 'user_model'));
		if($site_id){
			$list_employee = $this->employee_model->gets(array('site_id' => $site_id, 'columns' => 'A.id, A.full_name, A.id_card, A.email'));
			foreach($list_employee AS $employee){
				$user = $this->user_model->get(array('employee_id' => $employee->id));
				$data['id'] 		= $user->id; 
				$data['full_name'] 	= $employee->full_name; 
				$data['email'] 		= $employee->email; 
				$data['username'] 	= $employee->id_card;
				$data['password'] 	= generate_password($employee->id_card);
				// $this->user_model->save($data);
				if(!$data['id']){
				    continue;
				}
				print_prev($data);
			}
		}
	}

	public function reset_branch_site()
	{
		$this->load->model(array('site_model'));
		$list_site = $this->site_model->gets(array('columns' => 'A.id, E.branch_id'));
		foreach($list_site AS $site){
			$this->site_model->save(array('id' => $site->id, 'branch_id' => $site->branch_id));
			print_prev($site);
		}
	}
}
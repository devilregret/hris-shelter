<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Roemployee extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
		if($this->session->userdata('site') == ''){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan pilih site bisnis.','danger'));
			redirect(base_url());	
		}
	}

	public function form($employee_id = FALSE)
	{
		$this->load->model(array('city_model', 'site_model', 'branch_model', 'position_model', 'province_model', 'preference_model', 'employee_model', 'setting_tax_model'));
		
		if($this->input->post()){
			$params['id'] 						= $this->input->post('id');
			$params['full_name'] 				= $this->input->post('full_name');
			$params['city_birth_id'] 			= $this->input->post('city_birth_id');
			$params['date_birth'] 				= $this->input->post('date_birth');
			$params['address_card'] 			= $this->input->post('address_card');
			$params['city_card_id'] 			= $this->input->post('city_card_id');
			$params['address_domisili'] 		= $this->input->post('address_domisili');
			$params['city_domisili_id'] 		= $this->input->post('city_domisili_id');
			$params['status_residance'] 		= $this->input->post('status_residance');
			$params['phone_number'] 			= $this->input->post('phone_number');
			$params['email'] 					= $this->input->post('email');
			$params['socmed_fb'] 				= $this->input->post('socmed_fb');
			$params['socmed_ig'] 				= $this->input->post('socmed_ig');
			$params['socmed_tw'] 				= $this->input->post('socmed_tw');
			$params['religion'] 				= $this->input->post('religion');
			$params['marital_status'] 			= $this->input->post('marital_status');
			$params['gender'] 					= $this->input->post('gender');
			$params['heigh'] 					= $this->input->post('heigh');
			$params['weigh'] 					= $this->input->post('weigh');
			$params['education_level'] 			= $this->input->post('education_level');
			$params['education_majors'] 		= $this->input->post('education_majors');
			$params['family_mother'] 			= $this->input->post('family_mother');
			$params['family_mate'] 				= $this->input->post('family_mate');
			$params['family_child'] 			= $this->input->post('family_child');
			$params['ptkp'] 					= $this->input->post('ptkp');
			$params['position_id'] 				= $this->input->post('position_id');
			$benefit_labor_note					= "";
			$labor = 0;
			foreach($this->input->post('benefit_labor_choice') AS $item){
				if($labor > 0){
					$benefit_labor_note .= ",".$item;
				}else{

					$benefit_labor_note .= $item;
				}
				$labor++;
			}
			
			$params['benefit_labor_note'] 		= $benefit_labor_note;
			$params['benefit_labor'] 			= $this->input->post('benefit_labor');
			$params['benefit_health_note'] 		= $this->input->post('benefit_health_note');
			$params['benefit_health'] 			= $this->input->post('benefit_health');
			$params['bank_name'] 				= $this->input->post('bank_name');
			$params['bank_account'] 			= $this->input->post('bank_account');
			$params['bank_account_name'] 		= $this->input->post('bank_account_name');
			$params['tax_number'] 				= $this->input->post('tax_number');
			$params['id'] 						= $this->input->post('id');
			
			$url_document = 'files/document/'.$data['id_card'];	
			$document_path = FCPATH.$url_document;
			if(!is_dir($document_path)){
				mkdir($document_path, 0755, TRUE);
			}

			$config['upload_path'] 	= $document_path;
			$config['overwrite']  	= TRUE;
			$config['allowed_types']= 'jpg|jpeg|png';
				
			if($_FILES['document_photo']['name'] != ""){
				$config['allowed_types']= 'jpg|jpeg|png';
				$config['file_name'] 	= 'foto';
				$this->load->library('upload', $config, 'document_photo');

				if (!$this->document_photo->do_upload('document_photo')) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Proses upload Foto gagal.','danger'));
				} else {
					$document 	= $this->document_photo->data();
					$params['document_photo'] = $data['document_photo'] = base_url($url_document.'/'.$document['file_name']);
				}
			}

			if($_FILES['document_id_card']['name'] != ""){
				$config['allowed_types']= 'jpg|jpeg|png';
				$config['file_name'] 	= 'ktp';
				$this->load->library('upload', $config, 'document_id_card');

				if (!$this->document_id_card->do_upload('document_id_card')) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Proses upload Foto KTP gagal.','danger'));
				} else {
					$document = $this->document_id_card->data();
					$params['document_id_card'] =	$data['document_id_card'] = base_url($url_document.'/'.$document['file_name']);
				}
			}

			if($_FILES['document_marriage_certificate']['name'] != ""){
				$config['allowed_types']= 'jpg|jpeg|png';
				$config['file_name'] 	= 'buku-nikah';
				$this->load->library('upload', $config, 'document_marriage_certificate');

				if (!$this->document_marriage_certificate->do_upload('document_marriage_certificate')) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Proses upload Foto Surat Nikah gagal.','danger'));
				} else {
					$document = $this->document_marriage_certificate->data();
					$params['document_marriage_certificate'] =	$data['document_marriage_certificate'] = base_url($url_document.'/'.$document['file_name']);
				}
			}

			if($_FILES['document_tax_number']['name'] != ""){
				$config['allowed_types']= 'jpg|jpeg|png';
				$config['file_name'] 	= 'npwp';
				$this->load->library('upload', $config, 'document_tax_number');

				if (!$this->document_tax_number->do_upload('document_tax_number')) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Proses upload Foto NPWP gagal.','danger'));
				} else {
					$document = $this->document_tax_number->data();
					$params['document_tax_number'] =	$data['document_tax_number'] = base_url($url_document.'/'.$document['file_name']);
				}
			}

			if($_FILES['document_family_card']['name'] != ""){
				$config['allowed_types']= 'jpg|jpeg|png';
				$config['file_name'] 	= 'kartu-keluarga';
				$this->load->library('upload', $config, 'document_family_card');

				if (!$this->document_family_card->do_upload('document_family_card')) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Proses upload Foto Kartu Keluarga gagal.','danger'));
				} else {
					$document = $this->document_family_card->data();
					$params['document_family_card'] =	$data['document_family_card'] = base_url($url_document.'/'.$document['file_name']);
				}
			}

			$save_id = $this->employee_model->save($params);
			if ($save_id) {
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong>  data gagal disimpan.','danger'));
			}
			redirect(base_url('roemployee/list'));
		}

		if ($employee_id)
		{
			$data = (array) $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.*'));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('roemployee/list'));
			}
		}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('roemployee/list'));
			
		}

		$data['list_city'] 		= $this->city_model->gets(array('columns' => 'A.id, A.name, B.name AS province_name'));
		$data['list_site'] 		= $this->site_model->gets(array('columns' => 'A.id, A.name, C.code AS company_code'));
		$data['list_tax'] 		= $this->setting_tax_model->gets(array('columns' => 'A.code, A.description'));
		$data['list_branch'] 	= $this->branch_model->gets(array('columns' => 'A.id, A.name'));
		$data['list_position'] 	= $this->position_model->gets(array('columns' => 'A.id, A.name'));

		// $data['list_setting_tax']	= $this->setting_tax_model->gets(array('columns' => 'A.code, A.description'));
		$data['_TITLE_']			= 'Karyawan';
		$data['_PAGE_']				= 'roemployee/form';
		$data['_MENU_PARENT_']		= 'roemployee';
		$data['_MENU_']				= 'roemployee';
		$this->view($data);
	}

	public function list(){
		$this->load->model(array('site_model'));
		$data['_TITLE_'] 		= 'Karyawan';
		$data['_PAGE_'] 		= 'roemployee/list';
		$data['_MENU_PARENT_'] 	= 'roemployee';
		$data['_MENU_'] 		= 'roemployee';

		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address'));
		$this->view($data);
	}

	public function list_ajax(){
		$this->load->model(array('employee_model', 'employee_contract_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.full_name, A.ptkp, A.phone_number, A.bank_name, A.bank_account, A.bank_account_name, B.code AS company_code, C.name AS position";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']		= 1;
		
		$params['status_approval'] 		= 3;
		$params['site_id']				=  $this->session->userdata('site');

		$params['employee_number']		= $_POST['columns'][0]['search']['value'];
		$params['id_card']				= $_POST['columns'][1]['search']['value'];
		$params['full_name']			= $_POST['columns'][2]['search']['value'];
		$params['ptkp']					= $_POST['columns'][3]['search']['value'];
		$params['phone_number']			= $_POST['columns'][4]['search']['value'];
		$params['bank_name']			= $_POST['columns'][5]['search']['value'];
		$params['bank_account']			= $_POST['columns'][6]['search']['value'];
		$params['bank_account_name']	= $_POST['columns'][7]['search']['value'];
		$params['company_code']			= $_POST['columns'][8]['search']['value'];
		$params['position']				= $_POST['columns'][9]['search']['value'];

		$list 	= $this->employee_model->ro_gets($params);
		$total 	= $this->employee_model->ro_gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 					= $i;
			$result['ptkp']					= $item->ptkp;
			$result['employee_number']		= $item->employee_number;
			$result['id_card']				= $item->id_card;
			$result['full_name']			= $item->full_name;
			$result['phone_number']			= $item->phone_number;
			$result['bank_name']			= $item->bank_name;
			$result['bank_account']			= $item->bank_account;
			$result['bank_account_name']	= $item->bank_account_name;
			$result['company_code']			= $item->company_code;
			$result['position']				= $item->position;
			$result['contract_start'] 		= '';
			
			$contract = $this->employee_contract_model->get(array('columns' => 'MIN(A.contract_start) AS contract_start, MAX(A.contract_end) AS contract_end','employee_id' => $item->id));
			$contract_end					= "";
			if($contract->contract_start != ''){
				$result['contract_start'] 	= date("d/m/Y", strtotime($contract->contract_start));
				$now 		= date("Y-m-d");
				$dt2 = new DateTime("+1 month");
				$next_month = $dt2->format("Y-m-d");
				if($contract->contract_end < $now){
					$contract_end = '<span class="text-danger">'.date("d/m/Y", strtotime($contract->contract_end)).'</span>';
				}else if($contract->contract_end < $next_month){
					$contract_end = '<span class="text-warning">'.date("d/m/Y", strtotime($contract->contract_end)).'</span>';
				}else{
					$contract_end = '<span class="text-success">'.date("d/m/Y", strtotime($contract->contract_end)).'</span>';	
				}
			}
			$result['contract_end'] 		= $contract_end;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("roemployee/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("roemployee/contract/".$item->id).'">Kontrak</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("roemployee/form/".$item->id).'">Ubah</i></a>
				<a class="btn-sm btn-danger btn-block" href="'.base_url("roemployee/resign/".$item->id).'">Resign</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function preview($employee_id=FALSE)
	{
		$this->load->model(array('employee_model','employee_education_model', 'employee_emergency_model', 'employee_history_model', 'employee_language_model', 'employee_reference_model', 'employee_skill_model'));
		$data['_TITLE_'] 		= 'Preview Karyawan';
		$data['_PAGE_']	 		= 'employee/preview';
		$data['_MENU_PARENT_'] 	= 'personalia';
		$data['_MENU_'] 		= 'employee';

		$data['id'] = $employee_id;

		if (!$employee_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('roemployee/list'));
		}

		$data['list_education'] = $this->employee_education_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.institute, A.start, A.end'));
		$data['list_emergency'] = $this->employee_emergency_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.relation, A.phone, A.address'));
		$data['list_history'] 	= $this->employee_history_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.company_name, A.position, A.period,  A.salary, A.company_address, A.resign_note'));
		$data['list_language'] 	= $this->employee_language_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.language, A.verbal, A.write'));
		$data['list_reference'] = $this->employee_reference_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.company_name, A.position, A.phone'));
		$data['list_skill'] 	= $this->employee_skill_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));

		$data['preview'] = $this->employee_model->preview(array('id' => $employee_id));
		$data['preview']->date_birth = format_date_ina($data['preview']->date_birth);
		$data['preview']->city_birth = str_replace(array('Kota', 'Kabupaten'), '', $data['preview']->city_birth);
		$this->load->view('roemployee/preview', $data);
	}

	public function contract($employee_id = FALSE)
	{
		if (!$employee_id)
		{
			$data["message"] = message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger');
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
			redirect(base_url('roemployee/list'));
		}

		$this->load->model(array('employee_model', 'employee_contract_model'));
		$data['employee'] = $this->employee_model->get(array('id'=>$employee_id, 'columns' => 'A.id, A.full_name, A.id_card, A.address_card, A.site_id'));
		$data['list_contract'] = $this->employee_contract_model->gets(array('employee_id'=>$employee_id, 'columns' => 'A.id, A.contract_type, DATE_FORMAT(A.contract_start, "%d/%m/%Y") AS contract_start, DATE_FORMAT(A.contract_end, "%d/%m/%Y") AS contract_end'));

		$data['_TITLE_'] 		= 'Kontrak Karyawan '.$data['employee']->full_name;
		$data['_PAGE_'] 		= 'roemployee/contract';
		$data['_MENU_PARENT_'] 	= 'roemployee';
		$data['_MENU_'] 		= 'roemployee';
		
		$this->view($data);
	}

	public function export()
	{
		$this->load->model(array('employee_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = "Data Karyawan";
		$title = 'daftar_karyawan';
		$files = glob(FCPATH."files/".$title."/*");

		$params = $this->input->get();
		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.full_name, A.ptkp, A.phone_number, A.bank_name, A.bank_account, A.bank_account_name, B.code AS company_code, C.name AS position";

		$params['orderby']		= "A.full_name";
		$params['order']		= "ASC";
		$params['status']		= 1;
		$params['status_approval'] 	= 3;
		$params['site_id']			=  $this->session->userdata('site');

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "ID Karyawan");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nomor KTP");
		$excel->getActiveSheet()->setCellValue("C".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("D".$i, "Unit Kontrak");
		$excel->getActiveSheet()->setCellValue("E".$i, "Jabatan");
		$excel->getActiveSheet()->setCellValue("F".$i, "Kode PTKP");
		$excel->getActiveSheet()->setCellValue("G".$i, "Nomor Whatsapp");
		$excel->getActiveSheet()->setCellValue("H".$i, "Nama Bank");
		$excel->getActiveSheet()->setCellValue("I".$i, "No Rekening");
		$excel->getActiveSheet()->setCellValue("J".$i, "Atas Nama");

		$list_candidate = $this->employee_model->ro_gets($params);
		$i=2;
		foreach ($list_candidate as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->employee_number);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValue("C".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->company_code);
			$excel->getActiveSheet()->setCellValue("E".$i, $item->position);
			$excel->getActiveSheet()->setCellValueExplicit("F".$i, $item->ptkp);
			$excel->getActiveSheet()->setCellValueExplicit("G".$i, $item->phone_number);
			$excel->getActiveSheet()->setCellValue("H".$i, $item->bank_name);
			$excel->getActiveSheet()->setCellValueExplicit("I".$i, $item->bank_account);
			$excel->getActiveSheet()->setCellValueExplicit("J".$i, $item->bank_account_name);
			$i++;
		}
		$excel->getActiveSheet()->setTitle('Daftar Karyawan');
		$excel->createSheet();
		$excel->setActiveSheetIndex(0);

		$sheet = $excel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells( true );
		foreach( $cellIterator as $cell ) {
			$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		}
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function resign($employee_id = FALSE){
		$this->load->model(array('employee_model', 'approval_model', 'resign_note_model'));
		
		if($this->input->post()){
			$employee_id= $this->input->post('employee_id');
			$id_card 	= $this->input->post('id_card');
			$resign_doc = "";
			$url_document 	= 'files/document/'.$id_card;
			$document_path 	= FCPATH.$url_document;
			if(!is_dir($document_path)){
				mkdir($document_path, 0755, TRUE);
			}
			
			$config['upload_path'] 	= $document_path;
			$config['overwrite']  	= TRUE;
			$config['allowed_types']= 'jpg|jpeg|png';
			if($_FILES['document_resign']['name'] != ""){
				$config['file_name'] 	= 'resign';
				$this->load->library('upload', $config, 'document_resign');

				if (!$this->document_resign->do_upload('document_resign')) {
					$data['message']= message_box('<strong>Gagal!</strong> '.$this->document_resign->display_errors().'.','danger');
				} else {
					$document 	= $this->document_resign->data();
					$resign_doc = base_url($url_document.'/'.$document['file_name']);
				}
			}

			$resign_note 	= $this->input->post('resign_note');
			$resign_burden 	= $this->input->post('resign_burden');
			$resign_type 	= $this->input->post('resign_type');
			$resign_submit 	= $this->input->post('resign_submit');
			$site_id 		= $this->input->post('site_id');
			$company_id 	= $this->input->post('company_id');
			$position_id 	= $this->input->post('position_id');
			$resign_note 	= $resign_type ." - ". $resign_note;
			$this->employee_model->save(array('id' => $employee_id, 'status_approval' => 4, 'document_resign' => $resign_doc, 'resign_note' => $resign_note, 'resign_burden' => $resign_burden, 'resign_type' => $resign_type, 'resign_submit' => $resign_submit, 'followup_status' => $resign_type, 'followup_note' => $resign_note));

			$approval = array('id' => '', 'employee_id' => $employee_id , 'site_id' => $site_id, 'company_id' => $company_id, 'position_id' => $position_id, 'note' => $resign_note,'status_approval' => 'Pengajuan Resign');
			$this->approval_model->save($approval);
			
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			redirect(base_url('roemployee/list'));
		}
		
		$data['employee'] 	= $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.id, A.employee_number, A.full_name, A.id_card, A.site_id, A.company_id, A.position_id, A.address_card, DATE_FORMAT(A.date_birth, "%d/%m/%Y") AS date_birth, A.company_id'));

		$data['list_resign_note'] 	= $this->resign_note_model->gets(array('columns' => 'A.id, A.name'));
		$data['_TITLE_'] 		= 'Karyawan '.$data['employee']->full_name;
		$data['_PAGE_'] 		= 'roemployee/resign';
		$data['_MENU_PARENT_'] 	= 'roemployee';
		$data['_MENU_'] 		= 'roemployee';
		
		$this->view($data);
	}

	public function import(){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('employee_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {
				$id_card			= replace_null($sheet->getCell('B'.$row)->getValue());
				$site_id			= $this->session->userdata('site');
				$candidate 			= $this->employee_model->get(array('id_card' => $id_card, 'site_id' => $site_id, 'columns' => 'A.id'));
				if(!$candidate){
					continue;
				}
				$data['id'] 					= $candidate->id;
				$data['ptkp']					= replace_null($sheet->getCell('F'.$row)->getValue());
				$data['phone_number']			= replace_null($sheet->getCell('G'.$row)->getValue());
				$data['bank_name']				= replace_null($sheet->getCell('H'.$row)->getValue());
				$data['bank_account']			= preg_replace("/[^0-9]/", "", replace_null($sheet->getCell('I'.$row)->getValue()));
				$data['bank_account_name']		= replace_null($sheet->getCell('J'.$row)->getValue());
				$save_id = $this->employee_model->save($data);
			}

			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
					redirect(base_url('roemployee/list'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
					redirect(base_url('roemployee/list'));
		}
	}

	// public function ptkp(){

	// 	if(!$_FILES['file']['name'] == ""){

	// 		$this->load->model(array('employee_model'));
			
	// 		$this->load->library("Excel");
			
	// 		$excelreader 	= new PHPExcel_Reader_Excel2007();
	// 		$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
	// 		$sheet  		= $loadexcel->getActiveSheet(0);
			
	// 		$rend			= $sheet->getHighestRow();
	// 		for ($row = 8; $row <= $rend; $row++) {
	// 			$id_card 	= 	replace_null($sheet->getCell('D'.$row)->getOldCalculatedValue());
				
	// 			if($id_card == ''){
	// 				continue;
	// 			}
	
	// 			$employee  	= $this->employee_model->get(array('id_card' => $id_card, 'columns' => 'A.id'));
	// 			if(!$employee){
	// 				// $message_warning .= $id_card.', ';
	// 				continue;
	// 			}

	// 			$code 	= 	replace_null($sheet->getCell('AZ'.$row)->getValue());
	// 			$ptkp 	= '';
	// 			if($code == 'M'){
	// 				$ptkp = 'K/0';
	// 			}else if($code == 'L'){
	// 				$ptkp = 'TK/0';
	// 			}else if($code == 'M1'){
	// 				$ptkp = 'K/1';
	// 			}else if($code == 'M2'){
	// 				$ptkp = 'K/2';
	// 			}
	// 			$this->employee_model->save(array('id' => $employee->id, 'ptkp' => $ptkp));
	// 			// print_prev($employee->id." - ".$code);
	// 			// $kehadiran = $this->kehadiran_model->get(array('employee_id' => $employee->id, 'periode' => '2023-08-15', 'columns' => 'A.id'));
	// 			// if(!$kehadiran){
	// 			// 	continue;
	// 			// }
	// 			// $uang_makan 	= 	replace_null($sheet->getCell('AM'.$row)->getOldCalculatedValue());
	// 			// $uang_lembur 	= 	replace_null($sheet->getCell('N'.$row)->getOldCalculatedValue());
	// 			// $id_card 		= 	replace_null($sheet->getCell('D'.$row)->getOldCalculatedValue());
				
	// 			// print_prev($id_card.' - '.$employee->id." - ".$kehadiran->id." - ".$uang_makan." - ".$uang_lembur);				 
	// 			// $this->kehadiran_model->save(array('id' => $kehadiran->id, 'uang_lembur' => $uang_lembur, 'uang_makan' => $uang_makan));
	// 		}
	// 		die();

	// 		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
	// 				redirect(base_url('roemployee/list'));
	// 	}else{
	// 		$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
	// 				redirect(base_url('roemployee/list'));
	// 	}
	// }
}
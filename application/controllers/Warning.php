<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warning extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('benefit'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,5,6,13,14))){
			redirect(base_url());
		}
	}

	public function delete($warning_id = false)
	{
		$this->load->model('warning_model');
		if ($warning_id)
		{
			$data =  $this->warning_model->get(array('id' => $contract_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $warning_id, 'is_active' => 0);
				$result = $this->warning_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('warning/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('warning/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('warning/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('warning/list'));
		}
	}

	public function preview($template_id = FALSE)
	{
		$this->load->model('warning_model');
		$data['_TITLE_'] 		= 'Preview Template Surat Peringatan';
		$data['_PAGE_'] 		= 'warning/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'warning';

		$data['id'] = $template_id;

		
		if (!$template_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('warning/list'));
		}

		$data['preview'] = $this->warning_model->preview(array('id' => $template_id));
		$this->load->view('warning/preview', $data);
	}
	
	public function form($template_id = FALSE)
	{
		$this->load->model(array('warning_model', 'company_model'));

		$data['id'] 		= '';
		$data['title']		= '';
		$data['company_id']	= '';
		$data['content']	= '';

		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['title']		= $this->input->post('title');
			$data['company_id']	= $this->input->post('company_id');
			$data['content']	= $this->input->post('content');
			
			$this->form_validation->set_rules('title', '', 'required');
			$this->form_validation->set_rules('content', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$insert = array('id' => $data['id'], 'company_id' => $data['company_id'], 'title' => $data['title'], 'content' => $data['content']);
				
				$save_id	 	= $this->warning_model->save($insert);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('warning/list'));
		}

		if ($template_id)
		{
			$data = (array) $this->warning_model->get(array('id' => $template_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('warning/list'));
			}
		}

		$data['list_company'] 	= $this->company_model->gets(array('columns' => 'A.id, A.name'));
		$data['_TITLE_'] 		= 'Template Surat Peringatan';
		$data['_PAGE_'] 		= 'warning/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'warning';

		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Template Surat Peringatan';
		$data['_PAGE_'] 		= 'warning/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'warning';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('warning_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.title, D.name AS company_name';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['company_name']	= $_POST['columns'][1]['search']['value'];
		$params['title']	= $_POST['columns'][2]['search']['value'];

		$list 	= $this->warning_model->gets($params);
		$total 	= $this->warning_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['title'] 		= $item->title;
			$result['company_name'] = $item->company_name;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("warning/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("warning/form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("warning/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function preview_warning($warning_id = FALSE)
	{
		$data['_TITLE_'] 		= 'Preview Surat Peringatan';
		$data['_PAGE_']	 		= 'warning/preview_warning';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'warning';

		$this->load->model('employee_warning_model');
		$data['id'] = $warning_id;

		$data['preview'] = $this->employee_warning_model->get(array('id' => $warning_id, 'columns' => 'A.warning'));
		$this->load->view('warning/preview_warning', $data);
	}

	public function pdf($warning_id = FALSE){
		$this->load->model('employee_warning_model');
		$warning = $this->employee_warning_model->get(array('id' => $warning_id, 'columns' => 'A.warning'));
		$data['content'] = $warning->warning;
		$data['title'] = 'Surat Peringatan';
		$data['basepath'] = FCPATH.'files/';
		$this->generate_PDF($data);
		redirect(base_url('files/'.strtolower(str_replace(" ", "_",$data['title'])).'.pdf'));
	}

	private function generate_PDF($data = array())
	{	
		$this->load->library("Pdf");
		ob_start();
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// $pdf->SetProtection(array('print', 'copy'), $data['email']->pdf_password, null, 0, null);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shelter');
		$pdf->SetTitle($data['title']);
		$pdf->SetSubject($data['title']);
		$pdf->SetKeywords('Shelter');

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$pdf->SetFont('times', '', 10);
		$pdf->AddPage();
		$pdf->writeHTML($data['content'], true, false, true, false, '');

		$base_path  = $data['basepath'];
		if(!is_dir($base_path)){
			mkdir($base_path, 0777, true);
		}
		return $pdf->Output($base_path.strtolower(str_replace(" ", "_",$data['title'])).'.pdf', 'F');
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Benefit extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('benefit'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function labor(){
		$this->load->model(array('employee_model', 'site_model', 'upload_report_model'));
		$data['_TITLE_'] 		= 'Asuransi Ketenagakerjaan';
		$data['_PAGE_'] 		= 'benefit/labor';
		$data['_MENU_PARENT_'] 	= 'benefit_employee';
		$data['_MENU_'] 		= 'labor';


		$report  = $this->upload_report_model->get(array('id' => 3, 'columns' => 'A.id, A.failed'));
		$report_failed 	= $report->failed;
		$branch_id				= '';
		if($this->session->userdata('branch') > 1){
			$branch_id 	= $this->session->userdata('branch');
		}
		$data['failed']			= $report_failed;
		$data['report_id']		= 3;
		$data['list_site']		= $this->site_model->gets(array('columns' => 'A.id, A.name, C.code AS company_code', 'indirect' => true));
		$data['list_npptk']		= $this->employee_model->gets_benefit_company(array('columns' => 'A.benefit_labor_company', 'groupby' => 'benefit_labor_company', 'orderby' => 'A.benefit_labor_company', 'order' => 'ASC', 'branch_id' => $branch_id));
		$data['list_nppks']		= $this->employee_model->gets_benefit_company(array('columns' => 'A.benefit_health_company', 'groupby' => 'benefit_health_company', 'orderby' => 'A.benefit_health_company', 'order' => 'ASC', 'branch_id' => $branch_id));
		
		$data['filter']			= $this->input->get('filter');
		$data['site_id']		= $this->input->get('site');
		$data['nppks']			= $this->input->get('nppks');
		$data['npptk']			= $this->input->get('npptk');
		
		$data['site']			= $this->site_model->get(array('columns' => 'A.id, A.name, A.address', 'id' => $data['site_id'], 'branch_id' => $branch_id));
		$data['employee']		= $this->employee_model->gets(array('site_id' => $data['site_id'] , 'status_approval' => 3, 'branch_id' => $branch_id), TRUE);
		$this->view($data);
	}

	public function labor_ajax(){
		$this->load->model(array('benefit_labor_model'));
		$column_index = $_POST['order'][0]['column']; 
		$params['columns'] 		= "A.periode, SUM(A.employee_payment + A.company_payment) AS total_payment, COUNT(DISTINCT A.employee_id) AS total_employee, C.name AS site_name, A.benefit_labor_company";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['groupby']		=  'A.periode';
		$params['site_id'] 				= $this->input->get('site');
		$params['is_labor_company']		= $this->input->get('npptk');
		$params['is_health_company'] 	= $this->input->get('nppks');
		$params['branch_id']			= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id'] 		= $this->session->userdata('branch');
		}

		if($params['site_id']){
			$params['groupby']		=  'A.periode, B.site_id';
		}else if($params['is_labor_company']){
			$params['groupby']		=  'A.periode, A.benefit_labor_company';
		}

		$list 	= $this->benefit_labor_model->gets($params);
		if($params['site_id']){
			$total 	= count($this->benefit_labor_model->gets(array('columns' => 'A.periode', 'groupby' => 'A.periode', 'site_id' => $params['site_id'], 'branch_id' => $params['branch_id'])));
		}else{
			$total 	= count($this->benefit_labor_model->gets(array('columns' => 'A.periode', 'groupby' => 'A.periode', 'branch_id' => $params['branch_id'])));
		}
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['periode']				= get_month(substr($item->periode, 5, 2)).' '.substr($item->periode, 0, 4);
			$result['site_name']			= '';
			$result['benefit_labor_company']= '';
			
			if($params['site_id']){
				$params['site_name']		=  $item->site_name;
				$result['benefit_labor_company']= $item->benefit_labor_company;
			}
			if($params['is_labor_company']){
				$params['site_name']		=  $item->site_name;
				$result['benefit_labor_company']= $item->benefit_labor_company;
			}

			$result['total_payment']		= nominal_rupiah($item->total_payment);
			$result['total_employee']		= $item->total_employee.' Karyawan';
			$result['action'] 				=
				'<a class="btn-sm btn-primary btn-block" href="'.base_url("benefit/labor_detail?site=".$params['site_id']."&nppks=".$params['is_health_company'] ."&npptk=".$params['is_labor_company']."&periode=".$item->periode).'">Detail</a> 
				<a class="btn-sm btn-success btn-block" target="_blank" href="'.base_url("benefit/labor_export?site_id=".$params['site_id']."&is_labor_company=".$params['is_labor_company']."&is_health_company=".$params['is_health_company'] ."&preview=".$item->periode).'">Export</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("benefit/labor_delete?site=".$params['site_id']."&nppks=".$params['is_health_company'] ."&npptk=".$params['is_labor_company']."&periode=".$item->periode).'">Hapus</a>';

			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}

	public function labor_delete(){
		$this->load->model(array('benefit_labor_model'));

		// $site_id		= $this->input->get('site');
		// $npptk			= $this->input->get('npptk');
		// $nppks			= $this->input->get('nppks');
		$periode		= $this->input->get('periode');

		$benefit			= $this->benefit_labor_model->delete_detail(array('periode' => $periode));

		redirect(base_url('benefit/labor'));
	}
	
	public function labor_detail(){
		$this->load->model(array('benefit_labor_model', 'site_model'));
		$data['_TITLE_'] 		= 'Asuransi Ketenagakerjaan';
		$data['_PAGE_'] 		= 'benefit/labor_detail';
		$data['_MENU_PARENT_'] 	= 'benefit_employee';
		$data['_MENU_'] 		= 'labor';

		$data['site_id']		= $this->input->get('site');
		$data['npptk']			= $this->input->get('npptk');
		$data['nppks']			= $this->input->get('nppks');
		$data['site']			= $this->site_model->get(array('columns' => 'A.id, A.name, A.address', 'id' => $data['site_id']));
		$data['periode']		= $this->input->get('periode');
		$data['total_employee'] = 0;
		$data['total_payment'] 	=  nominal_rupiah(0);

		$params['site_id'] 				= $data['site_id'];
		$params['is_labor_company']		= $data['npptk'];
		$params['is_health_company'] 	= $data['nppks'];
		$params['columns'] 			= "A.periode, SUM(A.employee_payment + company_payment) AS total_payment, COUNT(DISTINCT A.employee_id) AS total_employee";
		$params['groupby']			=  'A.periode';
		$params['preview']			=  $data['periode'];
		$benefit					= $this->benefit_labor_model->gets($params);
		if($benefit){
			$data['total_employee'] = $benefit[0]->total_employee;
			$data['total_payment'] 	=  nominal_rupiah($benefit[0]->total_payment);
		}
		$this->view($data);
	}

	public function labor_detail_ajax(){
		$this->load->model(array('benefit_labor_model'));
		$column_index = $_POST['order'][0]['column'];
		$params['columns'] 		= "B.id, B.id_card, B.full_name, A.card_number, A.periode, C.name AS site_name, A.benefit_labor_company";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['groupby']		=  'A.employee_id';
		$params['site_id']		=  $this->input->get('site');
		$params['is_labor_company']		=  $this->input->get('npptk');
		$params['is_health_company']	=  $this->input->get('nppks');
		$params['preview']				=  $this->input->get('periode');
		if($params['site_id']){
			$params['groupby']		=  'A.employee_id, B.site_id';
		}else if($params['is_labor_company']){
			$params['groupby']		=  'A.employee_id, A.benefit_labor_company';
		}
		$params['id_card']			= $_POST['columns'][1]['search']['value'];
		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['card_number']		= $_POST['columns'][3]['search']['value'];

		$list 	= $this->benefit_labor_model->gets($params);
		$total 	= count($this->benefit_labor_model->gets(array('site_id' => $params['site_id'], 'is_labor_company' => $params['is_labor_company'], 'is_health_company' => $params['is_health_company'] , 'preview' => $params['preview'], 'groupby' => 'A.employee_id', 'columns' => 'A.employee_id')));

		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['id_card']				= $item->id_card;
			$result['full_name']			= $item->full_name;
			$result['card_number']			= $item->card_number;
			$result['site_name']			= $item->site_name;
			$result['benefit_labor_company']= $item->benefit_labor_company;
			$result['jht_company']			= "-";
			$result['jht_employee']			= "-";
			$result['jp_company']			= "-";
			$result['jp_employee']			= "-";
			$result['jkk_company']			= "-";
			$result['jkm_company']			= "-";
			$jht = $this->benefit_labor_model->get(array('employee_id' => $item->id, 'periode' => $item->periode, 'program' => 'JHT', 'columns' => 'A.employee_payment, A.company_payment'));
			if($jht){
				$result['jht_company']		= nominal_rupiah($jht->company_payment);
				$result['jht_employee']		= nominal_rupiah($jht->employee_payment);
			}
			$jp = $this->benefit_labor_model->get(array('employee_id' => $item->id, 'periode' => $item->periode, 'program' => 'JP', 'columns' => 'A.employee_payment, A.company_payment'));
			if($jp){
				$result['jp_company']		= nominal_rupiah($jp->company_payment);
				$result['jp_employee']		= nominal_rupiah($jp->employee_payment);
			}
			$jkk = $this->benefit_labor_model->get(array('employee_id' => $item->id, 'periode' => $item->periode, 'program' => 'JKK', 'columns' => 'A.company_payment'));
			if($jkk){
				$result['jkk_company']		= nominal_rupiah($jkk->company_payment);
			}
			$jkm = $this->benefit_labor_model->get(array('employee_id' => $item->id, 'periode' => $item->periode, 'program' => 'JKM', 'columns' => 'A.company_payment'));
			if($jkm){
				$result['jkm_company']		= nominal_rupiah($jkm->company_payment);
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}

	public function labor_process()
	{
		$periode = $this->input->post('periode');
		$site_id = $this->input->post('site_id');
		$nppks = $this->input->post('nppks');
		$npptk = $this->input->post('npptk');
		foreach ($this->input->post('id') as $id)
		{
			$this->load->model(array('benefit_labor_model'));
			$result = $this->benefit_labor_model->update(array('employee_id' => $id, 'periode' => $periode, 'is_active' => 0));
			if ($result) {
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil dihapus.','success'));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal diproses.','danger'));
			}
		}
		redirect(base_url('benefit/labor_detail?site='.$site_id.'&nppks='.$nppks.'&npptk='.$npptk.'&periode='.$periode));
	}
	
	public function labor_export()
	{
		$this->load->model(array('benefit_labor_model', 'site_model', 'company_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$params 			= $this->input->get();
		$title 				= "ketenagakerjaan";
		$files = glob(FCPATH."files/ketenagakerjaan/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);
		$site = $this->site_model->get(array('id' => $params['site_id'], 'columns' => 'A.name, A.address, A.company_id'));
		if($site){
			$company = $this->company_model->get(array('id' => $site->company_id, 'columns' => 'A.name, A.address'));
			if($company){
				$excel->getActiveSheet()->setCellValue("A1", $company->name);
				$excel->getActiveSheet()->setCellValue("A2", $company->address);
				$excel->getActiveSheet()->mergeCells('A1:J1');
				$excel->getActiveSheet()->mergeCells('A2:J2');
			}

			$excel->getActiveSheet()->setCellValue("A4", $site->name);
			$excel->getActiveSheet()->setCellValue("A5", $site->address);
			$excel->getActiveSheet()->mergeCells('A4:J4');
			$excel->getActiveSheet()->mergeCells('A5:J5');

			$excel->getActiveSheet()->getStyle('A1:A5')->getFont()->setBold( true );
		}

		$excel->getActiveSheet()->setCellValue("A7", "NIK KTP");
		$excel->getActiveSheet()->setCellValue("B7", "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("C7", "Site Bisnis");
		$excel->getActiveSheet()->setCellValue("D7", "NPP Asuransi Ketenagakerjaan");
		$excel->getActiveSheet()->setCellValue("E7", "No KPJ");
		$excel->getActiveSheet()->setCellValue("F7", "JHT");
		$excel->getActiveSheet()->mergeCells('F7:G7');
		$excel->getActiveSheet()->getStyle('F7:G7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->setCellValue("H7", "I7");
		$excel->getActiveSheet()->mergeCells('H7:I7');
		$excel->getActiveSheet()->getStyle('H7:I7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->setCellValue("J7", "JKK");
		$excel->getActiveSheet()->setCellValue("K7", "JKM");
		$excel->getActiveSheet()->setCellValue("K7", "TOTAL");

		$excel->getActiveSheet()->setCellValue("A8", "NIK KTP");
		$excel->getActiveSheet()->setCellValue("B8", "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("C8", "Site Bisnis");
		$excel->getActiveSheet()->setCellValue("D8", "NPP Asuransi Ketenagakerjaan");
		$excel->getActiveSheet()->setCellValue("E8", "No KPJ");
		$excel->getActiveSheet()->setCellValue("F8", "Perusahaan");
		$excel->getActiveSheet()->setCellValue("G8", "Karyawan");
		$excel->getActiveSheet()->setCellValue("H8", "Perusahaan");
		$excel->getActiveSheet()->setCellValue("I8", "Karyawan");
		$excel->getActiveSheet()->setCellValue("J8", "JKK");
		$excel->getActiveSheet()->setCellValue("K8", "JKM");
		$excel->getActiveSheet()->setCellValue("L8", "TOTAL");
		$excel->getActiveSheet()->mergeCells('A7:A8');
		$excel->getActiveSheet()->getStyle('A7:A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('A7:A8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$excel->getActiveSheet()->mergeCells('B7:B8');
		$excel->getActiveSheet()->getStyle('B7:B8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('B7:B8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$excel->getActiveSheet()->mergeCells('C7:C8');
		$excel->getActiveSheet()->getStyle('C7:C8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('C7:C8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$excel->getActiveSheet()->mergeCells('D7:D8');
		$excel->getActiveSheet()->getStyle('D7:D8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('D7:D8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$excel->getActiveSheet()->mergeCells('E7:E8');
		$excel->getActiveSheet()->getStyle('E7:E8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('E7:E8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$excel->getActiveSheet()->mergeCells('J7:J8');
		$excel->getActiveSheet()->getStyle('J7:J8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('J7:J8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$excel->getActiveSheet()->mergeCells('K7:K8');
		$excel->getActiveSheet()->getStyle('K7:K8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('K7:K8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		$excel->getActiveSheet()->mergeCells('L7:L8');
		$excel->getActiveSheet()->getStyle('L7:L8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('L7:L8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				

		$params['columns'] 		= "B.id, B.id_card, B.full_name, A.card_number, A.periode, A.benefit_labor_company, C.name AS site_name";
		$params['orderby']		= "B.full_name";
		$params['order']		= "ASC";

		$params['groupby']		= "B.id_card";
		$list_benefit = $this->benefit_labor_model->gets($params);
		$jht_company = 0;
		$jht_employee = 0;
		$jp_company = 0;
		$jp_employee = 0;
		$jkk_company = 0;
		$jkm_company = 0;

		$i=9;
		foreach ($list_benefit as $item) {
			$total_payment = 0;
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValue("C".$i, $item->site_name);
			$excel->getActiveSheet()->setCellValueExplicit("D".$i, $item->benefit_labor_company);
			$excel->getActiveSheet()->setCellValueExplicit("E".$i, $item->card_number);
			$jht = $this->benefit_labor_model->get(array('employee_id' => $item->id, 'periode' => $item->periode, 'program' => 'JHT', 'columns' => 'A.employee_payment, A.company_payment'));
			if($jht){
				$excel->getActiveSheet()->setCellValue("F".$i, $jht->company_payment);
				$excel->getActiveSheet()->setCellValue("G".$i, $jht->employee_payment);
				$total_payment 	= $total_payment + ($jht->company_payment + $jht->employee_payment);
				$jht_company 	= $jht_company + $jht->company_payment;
				$jht_employee 	= $jht_employee + $jht->employee_payment;
			}
			$jp = $this->benefit_labor_model->get(array('employee_id' => $item->id, 'periode' => $item->periode, 'program' => 'JP', 'columns' => 'A.employee_payment, A.company_payment'));
			if($jp){
				$excel->getActiveSheet()->setCellValue("H".$i, $jp->company_payment);
				$excel->getActiveSheet()->setCellValue("I".$i, $jp->employee_payment);
				$total_payment 	= $total_payment + ($jp->company_payment + $jp->employee_payment);
				$jp_company 	= $jp_company + $jp->company_payment;
				$jp_employee 	= $jp_employee + $jp->employee_payment;
			}
			$jkk = $this->benefit_labor_model->get(array('employee_id' => $item->id, 'periode' => $item->periode, 'program' => 'JKK', 'columns' => 'A.company_payment'));
			if($jkk){
				$excel->getActiveSheet()->setCellValue("J".$i, $jkk->company_payment);
				$total_payment 	= $total_payment + $jkk->company_payment;
				$jkk_company 	= $jkk_company + $jkk->company_payment;
			}
			$jkm = $this->benefit_labor_model->get(array('employee_id' => $item->id, 'periode' => $item->periode, 'program' => 'JKM', 'columns' => 'A.company_payment'));
			if($jkm){
				$excel->getActiveSheet()->setCellValue("K".$i, $jkm->company_payment);
				$total_payment 	= $total_payment + $jkm->company_payment;
				$jkm_company 	= $jkm_company + $jkm->company_payment;
			}
			$excel->getActiveSheet()->setCellValue("L".$i, $total_payment);
			$i++;
		}
		$excel->getActiveSheet()->setCellValue("A".$i, "TOTAL PEMBAYARAN");
		$excel->getActiveSheet()->mergeCells('A'.$i.':D'.$i);
		$excel->getActiveSheet()->getStyle('A'.$i.':E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->setCellValue("F".$i, $jht_company);
		$excel->getActiveSheet()->setCellValue("G".$i, $jht_employee);
		$excel->getActiveSheet()->setCellValue("H".$i, $jp_company);
		$excel->getActiveSheet()->setCellValue("I".$i, $jp_employee);
		$excel->getActiveSheet()->setCellValue("J".$i, $jkk_company);
		$excel->getActiveSheet()->setCellValue("K".$i, $jkm_company);
		$excel->getActiveSheet()->setCellValue("L".$i, $jht_company+$jht_employee+$jp_company+$jp_employee+$jkk_company+$jkm_company);
		$excel->getActiveSheet()->getStyle('A'.$i.':K'.$i)->getFont()->setBold( true );
		$excel->getActiveSheet()->getStyle('L7:L'.$i)->getFont()->setBold( true );
		$excel->getActiveSheet()->setTitle($title);
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = "ketenagakerjaan"."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/ketenagakerjaan/".$filename.".xlsx");
		redirect(base_url("files/ketenagakerjaan/".$filename.".xlsx"));
		exit;
	}

	public function labor_import()
	{
		$site_id = '';
		if($_FILES['file']['name'] != ""){
			$this->load->model(array('benefit_labor_model', 'employee_model', 'upload_report_model'));
			$this->load->library("Excel");

			$report  = $this->upload_report_model->get(array('id' => 3, 'columns' => 'A.id, A.failed, A.success'));
			$report_failed 	= $report->failed;
			$report_success = $report->success;

			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			$rend			= $sheet->getHighestRow();
			
			$data['periode']= $this->input->post('periode').'-01';
			$site_id		= $this->input->post('site_id');
			$total 		= $rend - 3;
			$success 	= 0;
			$fail 		= "";
			for ($row = 3; $row <= $rend; $row++) {
				$id_card 	= replace_null($sheet->getCell('A'.$row)->getValue());
				$data['benefit_labor_company']	= replace_null($sheet->getCell('B'.$row)->getValue());
				$data['card_number']			= replace_null($sheet->getCell('C'.$row)->getValue());
				$employee 	= $this->employee_model->get(array('id_card' => $id_card, 'columns' => 'A.id'));
				if($employee){
					$success = $success +1;
					$data['is_active']  			= 1;
					$data['employee_id']			= $employee->id;
					$benefit_labor_note				= "";

					$data['program'] 			= 'JHT';
					$data['company_payment']	= replace_null($sheet->getCell('D'.$row)->getValue());
					$data['employee_payment']	= replace_null($sheet->getCell('E'.$row)->getValue());
					if($data['company_payment'] != '' || $data['employee_payment'] != ''){
						$benefit_labor_note		.= "JHT";
						$check_jht 	= $this->benefit_labor_model->get(array('employee_id' => $employee->id, 'periode' => $data['periode'], 'program' => 'JHT', 'columns' => 'A.id'));
						$data['id']		= '';
						if($check_jht){
							$data['id']		= $check_jht->id;
						}
						$this->benefit_labor_model->save($data);
					}
					
					$data['program'] 			= 'JP';
					$data['company_payment']	= replace_null($sheet->getCell('F'.$row)->getValue());
					$data['employee_payment']	= replace_null($sheet->getCell('G'.$row)->getValue());
					if($data['company_payment'] != '' || $data['employee_payment'] != ''){
						$benefit_labor_note		.= ",JP";
						$check_jp 	= $this->benefit_labor_model->get(array('employee_id' => $employee->id, 'periode' => $data['periode'], 'program' => 'JP', 'columns' => 'A.id'));
						$data['id']		= '';
						if($check_jp){
							$data['id']		= $check_jp->id;
						}
						$this->benefit_labor_model->save($data);
					}

					$data['program'] 			= 'JKK';
					$data['company_payment']	= replace_null($sheet->getCell('H'.$row)->getValue());
					$data['employee_payment']	= '';
					
					if($data['company_payment'] != ''){
						$benefit_labor_note		.= ",JKK";
						$check_jkk 	= $this->benefit_labor_model->get(array('employee_id' => $employee->id, 'periode' => $data['periode'], 'program' => 'JKK', 'columns' => 'A.id'));
						$data['id']		= '';
						if($check_jkk){
							$data['id']		= $check_jkk->id;
						}
						$this->benefit_labor_model->save($data);
					}

					$data['program'] 			= 'JKM';
					$data['company_payment']	= replace_null($sheet->getCell('I'.$row)->getValue());
					$data['employee_payment']	= '';

					if($data['company_payment'] != ''){
						$benefit_labor_note		.= ",JKM";
						$check_jkm 	= $this->benefit_labor_model->get(array('employee_id' => $employee->id, 'periode' => $data['periode'], 'program' => 'JKM', 'columns' => 'A.id'));
						$data['id']		= '';
						if($check_jkm){
							$data['id']		= $check_jkm->id;
						}
						$this->benefit_labor_model->save($data);
					}
					$this->employee_model->save(array('id' => $employee->id, 'benefit_labor_note' => $benefit_labor_note, 'benefit_labor' => $data['card_number'], 'benefit_labor_company' => $data['benefit_labor_company']));
				}else{
					$fail 		.= $id_card.", ";
					$report_failed = $report_failed . $id_card .'|'. $data['benefit_labor_company'] . '|' . $data['card_number'] . ',';
				}
			}

			$this->upload_report_model->save(array('id' => 3, 'failed' => $report_failed, 'success' => $report_success));
		}
		if($success == 0){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan, silahkan cek kolom NIK untuk memastikan NIK karyawan sudah benar.','danger'));
		}else if($success < $total){
			$this->session->set_flashdata('message', message_box('<strong>Peringatan!</strong> Data yang gagal diinputkan : '.$fail.', pastikan NIK yang diimport sesuai dengan data NIK di HRIS','warning'));	
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
		}
		redirect(base_url('benefit/labor'));
	}

	public function health(){

		$this->load->model(array('site_model', 'employee_model', 'health_assurance_model', 'upload_report_model'));
		$data['_TITLE_'] 		= 'Asuransi Kesehatan';
		$data['_PAGE_'] 		= 'benefit/health';
		$data['_MENU_PARENT_'] 	= 'benefit_employee';
		$data['_MENU_'] 		= 'health';
		$report  = $this->upload_report_model->get(array('id' => 4, 'columns' => 'A.id, A.failed'));
		$report_failed 	= $report->failed;
		$data['failed']			= $report_failed;
		$data['report_id']		= 4;
		$branch_id				= '';
		if($this->session->userdata('branch') > 1){
			$branch_id 	= $this->session->userdata('branch');
		}
		$data['list_site']		= $this->site_model->gets(array('columns' => 'A.id, A.name, C.code AS company_code', 'indirect' => TRUE));
		$data['list_npptk']		= $this->employee_model->gets_benefit_company(array('columns' => 'A.benefit_labor_company', 'groupby' => 'benefit_labor_company', 'orderby' => 'A.benefit_labor_company', 'order' => 'ASC', 'branch_id' => $branch_id));
		$data['list_nppks']		= $this->employee_model->gets_benefit_company(array('columns' => 'A.benefit_health_company', 'groupby' => 'benefit_health_company', 'orderby' => 'A.benefit_health_company', 'order' => 'ASC', 'branch_id' => $branch_id));
		$data['list_assurance']	= $this->health_assurance_model->gets(array('columns' => 'A.id, A.name'));
		
		$data['filter']			= $this->input->get('filter');
		$data['site_id']		= $this->input->get('site');
		$data['nppks']			= $this->input->get('nppks');
		$data['npptk']			= $this->input->get('npptk');

		$data['site']			= $this->site_model->get(array('columns' => 'A.id, A.name, A.address', 'id' => $data['site_id'], 'branch_id' => $branch_id));
		$data['employee']		= $this->employee_model->gets(array('site_id' => $data['site_id'] , 'status_approval' => 3, 'branch_id' => $branch_id), TRUE);
		$this->view($data);
	}

	public function health_ajax(){
		$this->load->model(array('benefit_health_model'));
		$column_index = $_POST['order'][0]['column']; 
		$params['columns'] 		= "A.periode, SUM(A.employee_payment + A.company_payment) AS total_payment, COUNT(A.employee_id) AS total_employee, C.name AS site_name, A.benefit_health_company";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['groupby']		=  'A.periode';
		$params['site_id'] 				= $this->input->get('site');
		$params['is_labor_company']		= $this->input->get('npptk');
		$params['is_health_company'] 	= $this->input->get('nppks');
		
		if($this->session->userdata('branch') > 1){
			$branch_id 	= $this->session->userdata('branch');
		}
		if($params['site_id']){
			$params['groupby']		=  'A.periode, B.site_id';
		}else if($params['is_health_company']){
			$params['groupby']		=  'A.periode, A.benefit_health_company';
		}

		$list 	= $this->benefit_health_model->gets($params);
		if($params['site_id']){
			$total 	= count($this->benefit_health_model->gets(array('columns' => 'A.periode', 'groupby' => 'A.periode', 'site_id' => $params['site_id'])));
		}else{
			$total 	= count($this->benefit_health_model->gets(array('columns' => 'A.periode', 'groupby' => 'A.periode')));
		}
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['periode']			= get_month(substr($item->periode, 5, 2)).' '.substr($item->periode, 0, 4);
			$result['site_name']			= '';
			$result['benefit_health_company']= '';
			
			if($params['site_id']){
				$params['site_name']		=  $item->site_name;
				$result['benefit_health_company']= $item->benefit_health_company;
			}
			if($params['is_health_company']){
				$params['site_name']		=  $item->site_name;
				$result['benefit_health_company']= $item->benefit_health_company;
			}
			$result['total_payment']	= nominal_rupiah($item->total_payment);
			$result['total_employee']	= $item->total_employee.' Karyawan';
			$result['action'] 			=
				'<a class="btn-sm btn-primary btn-block" href="'.base_url("benefit/health_detail?site=".$params['site_id']."&nppks=".$params['is_health_company'] ."&npptk=".$params['is_labor_company']."&periode=".$item->periode).'">Detail</a>
				<a class="btn-sm btn-success btn-block" target="_blank" href="'.base_url("benefit/health_export?site_id=".$params['site_id']."&is_labor_company=".$params['is_labor_company']."&is_health_company=".$params['is_health_company'] ."&preview=".$item->periode).'">Export</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("benefit/health_delete?site=".$params['site_id']."&nppks=".$params['is_health_company'] ."&npptk=".$params['is_labor_company']."&periode=".$item->periode).'">Hapus</a>';


			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}
	
	public function health_detail(){
		$this->load->model(array('benefit_health_model', 'site_model'));
		$data['_TITLE_'] 		= 'Asuransi Kesehatan';
		$data['_PAGE_'] 		= 'benefit/health_detail';
		$data['_MENU_PARENT_'] 	= 'benefit_employee';
		$data['_MENU_'] 		= 'health';
		$params['groupby']		=  'A.periode';

		$data['site_id']		= $this->input->get('site');
		$data['npptk']			= $this->input->get('npptk');
		$data['nppks']			= $this->input->get('nppks');
		$data['periode']		= $this->input->get('periode');

		$data['site']			= $this->site_model->get(array('columns' => 'A.id, A.name, A.address', 'site_id' => $data['site_id']));
		$data['total_employee'] = 0;
		$data['total_payment'] 	=  nominal_rupiah(0);

		$params['columns'] 		= "A.periode, SUM(A.employee_payment + company_payment) AS total_payment, COUNT(DISTINCT A.employee_id) AS total_employee";
		$params['site_id']		=  $data['site_id'];
		$params['preview']		=  $data['periode'];
		$benefit			= $this->benefit_health_model->gets($params);
		if($benefit){
			$data['total_employee'] = $benefit[0]->total_employee;
			$data['total_payment'] 	=  nominal_rupiah($benefit[0]->total_payment);
		}
		$this->view($data);
	}

	public function health_delete(){
		$this->load->model(array('benefit_health_model'));

		// $site_id		= $this->input->get('site');
		// $npptk			= $this->input->get('npptk');
		// $nppks			= $this->input->get('nppks');
		$periode		= $this->input->get('periode');

		$benefit			= $this->benefit_health_model->delete_detail(array('periode' => $periode));

		redirect(base_url('benefit/health'));
	}

	public function health_detail_ajax(){
		$this->load->model(array('benefit_health_model'));
		$column_index = $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, B.id_card, B.full_name, A.card_number, A.periode, A.company_payment, A.employee_payment, C.name AS site_name, A.benefit_health_company";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['site_id']		=  $this->input->get('site');
		$params['is_labor_company']		=  $this->input->get('npptk');
		$params['is_health_company']	=  $this->input->get('nppks');
		$params['preview']				=  $this->input->get('periode');
		if($params['site_id']){
			$params['groupby']		=  'A.employee_id, B.site_id';
		}else if($params['is_health_company']){
			$params['groupby']		=  'A.employee_id, A.benefit_health_company';
		}

		$params['id_card']			= $_POST['columns'][1]['search']['value'];
		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['card_number']		= $_POST['columns'][3]['search']['value'];

		$list 	= $this->benefit_health_model->gets($params);
		$total 	= count($this->benefit_health_model->gets(array('site_id' => $params['site_id'], 'is_labor_company' => $params['is_labor_company'], 'is_health_company' => $params['is_health_company'], 'preview' => $params['preview'], 'columns' => 'A.employee_id', 'groupby' => 'A.periode')));

		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{
			$result['id']				= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['id_card']			= $item->id_card;
			$result['full_name']		= $item->full_name;
			$result['card_number']		= $item->card_number;
			$result['site_name']			= $item->site_name;
			$result['benefit_health_company']= $item->benefit_health_company;
			$result['company_payment']	= $item->company_payment;
			$result['employee_payment']	= $item->employee_payment;
			
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}

	public function health_process()
	{

		$periode = $this->input->post('periode');
		$site_id = $this->input->post('site_id');
		$nppks = $this->input->post('nppks');
		$npptk = $this->input->post('npptk');
		foreach ($this->input->post('id') as $id)
		{
			$this->load->model(array('benefit_health_model'));
			$result = $this->benefit_health_model->save(array('id' => $id, 'is_active' => 0));
			if ($result) {
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil dihapus.','success'));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal diproses.','danger'));
			}
		}
		redirect(base_url('benefit/health_detail?site='.$site_id.'&nppks='.$nppks.'&npptk='.$npptk.'&periode='.$periode));
	}

	public function health_export()
	{
		$this->load->model(array('benefit_health_model','site_model','company_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');
		$params 			= $this->input->get();
		$title 				= "kesehatan";
		$files = glob(FCPATH."files/kesehatan/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$site = $this->site_model->get(array('id' => $params['site_id'], 'columns' => 'A.name, A.address, A.company_id'));
		if($site){
			$company = $this->company_model->get(array('id' => $site->company_id, 'columns' => 'A.name, A.address'));
			if($company){
				$excel->getActiveSheet()->setCellValue("A1", $company->name);
				$excel->getActiveSheet()->setCellValue("A2", $company->address);
				$excel->getActiveSheet()->mergeCells('A1:F1');
				$excel->getActiveSheet()->mergeCells('A2:F2');
			}

			$excel->getActiveSheet()->setCellValue("A4", $site->name);
			$excel->getActiveSheet()->setCellValue("A5", $site->address);
			$excel->getActiveSheet()->mergeCells('A4:F4');
			$excel->getActiveSheet()->mergeCells('A5:F5');
			$excel->getActiveSheet()->mergeCells('A6:F6');

			$excel->getActiveSheet()->getStyle('A1:A6')->getFont()->setBold( true );
		}

		$excel->getActiveSheet()->setCellValue("A8", "NIK KTP");
		$excel->getActiveSheet()->setCellValue("B8", "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("C8", "Site Bisnis");
		$excel->getActiveSheet()->setCellValue("D8", "NPP Asuransi Kesehatan");
		$excel->getActiveSheet()->setCellValue("E8", "NO KIS");
		$excel->getActiveSheet()->setCellValue("F8", "Pembayaran Perusahaan");
		$excel->getActiveSheet()->setCellValue("G8", "Pembayaran Karyawan");
		$excel->getActiveSheet()->setCellValue("H8", "Total");

		$params['columns'] 		= "B.id_card, B.full_name, A.card_number, A.assurance, A.employee_payment, A.company_payment, A.benefit_health_company, C.name AS site_name";
		$params['orderby']		= "B.full_name";
		$params['order']		= "ASC";
		$assurance 	= "";
		$company 	= 0;
		$employee 	= 0;
		$list_benefit = $this->benefit_health_model->gets($params);
		$i=9;
		foreach ($list_benefit as $item) {
			$assurance = $item->assurance;
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValue("C".$i, $item->site_name);
			$excel->getActiveSheet()->setCellValueExplicit("D".$i, $item->benefit_health_company);
			$excel->getActiveSheet()->setCellValueExplicit("E".$i, $item->card_number);
			$excel->getActiveSheet()->setCellValue("F".$i, $item->company_payment);
			$excel->getActiveSheet()->setCellValue("G".$i, $item->employee_payment);
			$excel->getActiveSheet()->setCellValue("H".$i, $item->company_payment + $item->employee_payment);
			$company 	= $company + $item->company_payment;
			$employee 	= $employee + $item->employee_payment;
			
			$i++;
		}

		$excel->getActiveSheet()->setCellValue("A6", $assurance);

		$excel->getActiveSheet()->setCellValue("A".$i, "TOTAL PEMBAYARAN");
		$excel->getActiveSheet()->mergeCells('A'.$i.':E'.$i);
		$excel->getActiveSheet()->getStyle('A'.$i.':E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->setCellValue("F".$i, $company);
		$excel->getActiveSheet()->setCellValue("G".$i, $employee);
		$excel->getActiveSheet()->setCellValue("H".$i, $company+$employee);

		$excel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->getFont()->setBold( true );
		$excel->getActiveSheet()->getStyle('H8:H'.$i)->getFont()->setBold( true );
		$excel->getActiveSheet()->setTitle($title);
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = "kesehatan"."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/kesehatan/".$filename.".xlsx");
		redirect(base_url("files/kesehatan/".$filename.".xlsx"));
		exit;
	}

	public function health_import()
	{
		if($_FILES['file']['name'] != ""){
			$this->load->model(array('benefit_health_model', 'employee_model', 'upload_report_model'));
			$this->load->library("Excel");

			$report  = $this->upload_report_model->get(array('id' => 4, 'columns' => 'A.id, A.failed, A.success'));
			$report_failed 	= $report->failed;
			$report_success = $report->success;

			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			$rend			= $sheet->getHighestRow();
			
			$data['periode']	= $this->input->post('periode').'-01';
			$data['assurance']	= $this->input->post('assurance');
			$site_id			= $this->input->post('site_id').'-01';
			$total 		= $rend - 3;
			$success 	= 0;
			$fail 		= "";
			for ($row = 2; $row <= $rend; $row++) {
				$id_card 	= replace_null($sheet->getCell('A'.$row)->getValue());
				$data['benefit_health_company']	= replace_null($sheet->getCell('B'.$row)->getValue());
				$data['card_number']			= replace_null($sheet->getCell('C'.$row)->getValue());

				$employee 	= $this->employee_model->get(array('id_card' => $id_card, 'columns' => 'A.id'));
				if($employee){
					$success = $success +1;
					$data['is_active']  			= 1;
					$data['employee_id']			= $employee->id;
					$data['company_payment']		= replace_null($sheet->getCell('D'.$row)->getValue());
					$data['employee_payment']		= replace_null($sheet->getCell('E'.$row)->getValue());
					
					if($data['company_payment'] != '' || $data['employee_payment'] != ''){
						$check 	= $this->benefit_health_model->get(array('employee_id' => $employee->id, 'periode' => $data['periode'], 'program' => $data['assurance'], 'columns' => 'A.id'));
						$data['id']		= '';
						if($check){
							$data['id']		= $check->id;
						}
						$this->benefit_health_model->save($data);
					}
					$this->employee_model->save(array('id' => $employee->id, 'benefit_health' => $data['card_number'], 'benefit_health_note' => $data['assurance'], 'benefit_health_company' => $data['benefit_health_company']));
				}else{
					$fail 		.= $id_card.", ";
					$report_failed = $report_failed . $id_card .'|'. $data['benefit_health_company'] . '|' . $data['card_number'] . ',';
				}
			}
			$this->upload_report_model->save(array('id' => 4, 'failed' => $report_failed, 'success' => $report_success));
		}
		if($success == 0){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan, silahkan cek kolom NIK untuk memastikan NIK karyawan sudah benar.','danger'));
		}else if($success < $total){
			$this->session->set_flashdata('message', message_box('<strong>Peringatan!</strong> Data yang gagal diinputkan : '.$fail.', pastikan NIK yang diimport sesuai dengan data NIK di HRIS','warning'));	
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
		}
		redirect(base_url('benefit/health'));
	}

	public function export($report_id = FALSE)
	{
		$this->load->model(array('upload_report_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = 'gagal_upload';
		$files = glob(FCPATH."files/".$title."/*");


		$report  = $this->upload_report_model->get(array('id' => $report_id, 'columns' => 'A.id, A.failed'));
		$report_failed 	= $report->failed;

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "NIK KTP");
		if($report_id == 3){
			$excel->getActiveSheet()->setCellValue("B".$i, "NPP Asuransi Ketenagakerjaan");
			$excel->getActiveSheet()->setCellValue("C".$i, "Nomor KPJ");
		}else{
			$excel->getActiveSheet()->setCellValue("B".$i, "NPP Asuransi Kesehatan");
			$excel->getActiveSheet()->setCellValue("C".$i, "Nomor KIS");
		}
		$i=2;
		$id_card = explode(",", $report_failed);
		foreach ($id_card as $item) {
			$detail = explode("|", $item);
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $detail[0]);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $detail[1]);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $detail[2]);
			$i++;
		}
		$excel->getActiveSheet()->setTitle('Daftar NIK');
		
		$excel->createSheet();
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function delete($report_id = FALSE)
	{
		$this->load->model('upload_report_model');
		if ($report_id)
		{
			$result = $this->upload_report_model->save(array('id' => $report_id, 'failed' => '', 'success' => ''));
			if ($result) {
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal dihapus.','danger'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
		}
		if($report_id == 3){
			redirect(base_url('benefit/labor'));
		}else{
			redirect(base_url('benefit/health'));
		}
	}
}
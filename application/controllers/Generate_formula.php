<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Generate_formula extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function generate_null($site_id = false){
		$this->load->model(array('formula_employee_model'));

		$list_employee = $this->formula_employee_model->gets_formula_null($site_id);

		foreach($list_employee AS $employee){
			$employee_exist = $this->formula_employee_model->gets_employee_with_fromula($site_id, $employee->position_id);
			// print_prev($employee->position_id);	
			// print_prev($employee_exist->id);		
			if($employee_exist){
			print_prev($employee_exist);		
				$this->formula_employee_model->save_formula_null($employee->id, $employee_exist->id);
			}
		}
	}

	public function import_skp(){

		$this->load->model(array('formula_employee_model', 'employee_model'));
			
			$this->load->library("Excel");

			// $file           = FCPATH."formula/formula1.xlsx";
			$formula_path = FCPATH.'files/formula/71.xlsx';			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($formula_path);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			

			// $list_employee 	= $this->formula_employee_model->gets_employee(array('site_id' => 71, 'columns'=> 'A.id'));
			// print_prev($list_employee);
			// die();
			for ($row = 3; $row <= $rend; $row++) {
				$id_card			= replace_null($sheet->getCell('D'.$row)->getValue());

				$employee = $this->employee_model->get(array('id_card' => $id_card, 'columns' => 'A.id, A.full_name'));
				if($employee){
					$employee_id = $employee->id;
					$this->formula_employee_model->delete(array('employee_id' => $employee_id));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '1',
							'kolom' 	=> '',
							'nama' 		=> 'HARI KERJA',
							'tipe' 		=> 'FORMULA',
							'kategori' 	=> 'PENDAPATAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '2',
							'kolom' 	=> 'L',
							'nama' 		=> 'MASUK KERJA',
							'tipe' 		=> 'FORMULA',
							'kategori' 	=> 'PENDAPATAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '3',
							'kolom' 	=> '',
							'nama' 		=> 'SHIFT MALAM',
							'tipe' 		=> 'FORMULA',
							'kategori' 	=> 'PENDAPATAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '4',
							'kolom' 	=> 'M',
							'nama' 		=> 'TIDAK MASUK',
							'tipe' 		=> 'FORMULA',
							'kategori' 	=> 'PENDAPATAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '5',
							'kolom' 	=> '',
							'nama' 		=> 'TERLAMBAT',
							'tipe' 		=> 'FORMULA',
							'kategori' 	=> 'PENDAPATAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '6',
							'kolom' 	=> 'P',
							'nama' 		=> 'JAM LEMBUR',
							'tipe' 		=> 'FORMULA',
							'kategori' 	=> 'PENDAPATAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '7',
							'kolom' 	=> 'R',
							'nama' 		=> 'UANG LEMBUR',
							'tipe' 		=> 'FOMULA',
							'kategori' 	=> 'PENDAPATAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '8',
							'kolom' 	=> 'AS',
							'nama' 		=> 'UANG MAKAN',
							'tipe' 		=> 'FOMULA',
							'kategori' 	=> 'PENDAPATAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '9',
							'kolom' 	=> 'BA',
							'nama' 		=> 'STATUS PTKP',
							'tipe' 		=> 'FOMULA',
							'kategori' 	=> 'POTONGAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '10',
							'kolom' 	=> 'BB',
							'nama' 		=> 'PTKP',
							'tipe' 		=> 'FOMULA',
							'kategori' 	=> 'POTONGAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '11',
							'kolom' 	=> 'BD',
							'nama' 		=> 'PPH21',
							'tipe' 		=> 'FOMULA',
							'kategori' 	=> 'POTONGAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '12',
							'kolom' 	=> 'AE',
							'nama' 		=> 'BPJS TK PERUSAHAAN',
							'tipe' 		=> 'FOMULA',
							'kategori' 	=> 'PENDAPATAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '13',
							'kolom' 	=> 'AF',
							'nama' 		=> 'BPJS KS PERUSAHAAN',
							'tipe' 		=> 'FORMULA',
							'kategori' 	=> 'PENDAPATAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '14',
							'kolom' 	=> 'AP',
							'nama' 		=> 'BPJS TK KARYAWAN',
							'tipe' 		=> 'FORMULA',
							'kategori' 	=> 'POTONGAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '15',
							'kolom' 	=> 'AQ',
							'nama' 		=> 'BPJS KS KARYAWAN',
							'tipe' 		=> 'FORMULA',
							'kategori' 	=> 'POTONGAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '16',
							'kolom' 	=> 'AU',
							'nama' 		=> 'GAJI BERSIH (THP)',
							'tipe' 		=> 'FORMULA',
							'kategori' 	=> 'PENDAPATAN',
							'nilai' 	=> ''
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '17',
							'kolom' 	=> 'K',
							'nama' 		=> 'GAJI POKOK',
							'tipe' 		=> 'NOMINAL',
							'kategori' 	=> 'PENDAPATAN',
							'nilai' 	=>  replace_null($sheet->getCell('K'.$row)->getCalculatedValue())
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '18',
							'kolom' 	=> 'S',
							'nama' 		=> 'KURANG BAYAR',
							'tipe' 		=> 'NOMINAL',
							'kategori' 	=> 'PENDAPATAN',
							'nilai' 	=>  '0'
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '19',
							'kolom' 	=> 'U',
							'nama' 		=> 'POTONGAN ABSEN',
							'tipe' 		=> 'FORMULA',
							'kategori' 	=> 'POTONGAN',
							'nilai' 	=>  '0'
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '20',
							'kolom' 	=> 'V',
							'nama' 		=> 'LEBIH BAYAR',
							'tipe' 		=> 'NOMINAL',
							'kategori' 	=> 'POTONGAN',
							'nilai' 	=>  '0'
						));

					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '21',
							'kolom' 	=> 'W',
							'nama' 		=> 'POTONGAN LAIN',
							'tipe' 		=> 'NOMINAL',
							'kategori' 	=> 'POTONGAN',
							'nilai' 	=>  '0'
						));


					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '22',
							'kolom' 	=> 'AH',
							'nama' 		=> 'ASURANSI',
							'tipe' 		=> 'NOMINAL',
							'kategori' 	=> 'POTONGAN',
							'nilai' 	=>  replace_null($sheet->getCell('AH'.$row)->getCalculatedValue())
						));


					$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> '23',
							'kolom' 	=> 'AI',
							'nama' 		=> 'SERAGAM',
							'tipe' 		=> 'NOMINAL',
							'kategori' 	=> 'POTONGAN',
							'nilai' 	=>  replace_null($sheet->getCell('AI'.$row)->getCalculatedValue())
						));
				}
			}
	}
}
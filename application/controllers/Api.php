<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Api extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}
	
	public function auth(){
		$this->load->model(array('api_authentication_model'));

		header('Content-Type: application/json; charset=utf-8');
		if (isset($_SERVER["REDIRECT_HTTP_AUTHORIZATION"])) {
            $auth = $_SERVER["REDIRECT_HTTP_AUTHORIZATION"];
            $auth_array = explode(" ", $auth);
            $un_pw = explode(":", base64_decode($auth_array[1]));
            $un = $un_pw[0];
            $pw = $un_pw[1];
            $auth = $this->api_authentication_model->get(array('client_key' => $un, 'secret_key' => $pw, 'columns' => 'A.id, A.application'));
			if($auth){
				$params['id']			= $auth->id;
				$params['updated_at'] 	= date('Y-m-d H:i:s');
				$params['token']	  	= hash('sha256', $un.$pw.$params['updated_at']);
				$this->api_authentication_model->save($params);
				$response = array(
					"data" 		=> array('token' => $params['token'], 'request_time' => $params['updated_at']),
					"message" 	=> "success",
				);
				echo json_encode($response);
				die();
			}else{
				header('HTTP/1.0 401 Unauthorized');
				$response = array(
					"data" 		=> array(),
					"message" 	=> "client key or secret key not valid",
				);
				echo json_encode($response);
			}
        }else if(isset($_SERVER["PHP_AUTH_USER"]) && isset($_SERVER["PHP_AUTH_PW"])){
			$auth = $this->api_authentication_model->get(array('client_key' => $_SERVER["PHP_AUTH_USER"], 'secret_key' => $_SERVER["PHP_AUTH_PW"], 'columns' => 'A.id, A.application'));
			if($auth){
				$params['id']			= $auth->id;
				$params['updated_at'] 	= date('Y-m-d H:i:s');
				$params['token']	  	=  hash('sha256', $_SERVER["PHP_AUTH_USER"].$_SERVER["PHP_AUTH_PW"].$params['updated_at']);
				$this->api_authentication_model->save($params);
				$response = array(
					"data" 		=> array('token' => $params['token'], 'request_time' => $params['updated_at']),
					"message" 	=> "success",
				);
				echo json_encode($response);
				die();
			}else{
				header('HTTP/1.0 401 Unauthorized');
				$response = array(
					"data" 		=> array(),
					"message" 	=> "client key or secret key not valid",
				);
				echo json_encode($response);
			}
		}else{
			header('HTTP/1.0 401 Unauthorized');
			$response = array(
				"data" 		=> array(),
				"message" 	=> "Authentication Not Found",
				"total" 	=> 0
			);
			echo json_encode($response);
		}
		exit;
	}

	function check_auth(){
		$headers = null;
		if (isset($_SERVER['Authorization'])) {
			$headers = trim($_SERVER["Authorization"]);
		}else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
			$headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
		}else if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
			$headers = trim($_SERVER["REDIRECT_HTTP_AUTHORIZATION"]);
		}elseif (function_exists('apache_request_headers')) {
			$requestHeaders = apache_request_headers();
			$requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
			if (isset($requestHeaders['Authorization'])) {
				$headers = trim($requestHeaders['Authorization']);
			}
		}

		if (!empty($headers)) {
			if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
				if(isset($matches[1])){
					$this->load->model(array('api_authentication_model'));
					$token = $this->api_authentication_model->get(array('token' => $matches[1], 'expired' => TRUE, 'columns' => 'A.id'));
					if($token){
						return true;
					}
				}
			}
		}
		return false;
	}

	// private function check_auth($token = ''){
	// 	$this->load->model(array('api_authentication_model'));
	// 	$token = $this->api_authentication_model->get(array('token' => $token, 'columns' => 'A.id'));
	// 	print_prev($token);
	// 	if($token){
	// 		return true;
	// 	}else{
	// 		return false;
	// 	}
	// }

	public function site($function = false){

		header('Content-Type: application/json; charset=utf-8');
		if(!$function){
			header('HTTP/1.0 400 Bad Request');
			$response = array(
				"data" 		=> array(),
				"message" 	=> "Method Not valid",
			);
			echo json_encode($response);
			exit;
		}

		if(!$this->check_auth()){
			header('HTTP/1.0 401 Unauthorized');
			$response = array(
				"data" 		=> array(),
				"message" 	=> "Invalid Token",
			);
			echo json_encode($response);
			exit;
		}

		if($function == 'list'){
			$this->load->model(array('site_model'));
			$request = json_decode(file_get_contents('php://input'), true);
			$params = $request;
			
			$params['columns'] = 'A.id, A.name AS nama, A.contract_start AS tanggal_kontrak, A.contract_end AS tanggal_berakhir, A.contract_value AS nominal_kontrak, A.updated_at AS terakhir_update, C.name AS cabang, B.code AS bisnis_unit, D.full_name AS nama_pic';
			$total 		= $this->site_model->gets_api($params, TRUE);
			$list_site 	= $this->site_model->gets_api($params);

			$response = array(
				"data" 		=> array('total' => $total, 'list' => $list_site),
				"message" 	=> "success",
			);
			echo json_encode($response);
			exit;
		}
	}

	function test_auth(){
		$accessToken 	= base64_encode('nsrL+/qC+hKGlocgMX7azg==:c9OUMtI1jpU8YFFAEQ0FbyaSUqaMJEs36NcrPBBCg0c=');
		$authorization 	= "Authorization: Basic ".$accessToken;
		$url 			= 'http://localhost/shelter/api/auth';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		$result = json_decode($response, true);
		print_prev($result);
		if(isset($result['access_token']) && isset($result['refresh_token'])){
			$this->accurate_model->save(array('id' => 1, 'access_token' => $result['access_token'], 'refresh_token' => $result['refresh_token']));
		}
		curl_close($ch);
		die();
	}

	// public function list_site(){
	// 	$this->load->model(array('site_model'));

	// 	$list_site = $this->site_model->gets_site(array('columns' => 'A.id, A.name AS nama, C.code AS bisnis_unit', 'orderby' => 'A.id', 'order' => 'ASC'));
	// 	$total 		= $this->site_model->gets_site(array('columns' => 'A.id, A.name AS nama, C.code AS bisnis_unit', 'orderby' => 'A.id', 'order' => 'ASC'), TRUE);
	// 	$response = array(
	// 		"data" 		=> $list_site,
	// 		"message" 	=> "success",
	// 		"total" 	=> $total,
	// 		"status" 	=> '200'
	// 	);
	// 	echo json_encode($response);
	// }
}
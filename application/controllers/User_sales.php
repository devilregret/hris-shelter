<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_sales extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('sales'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,10))){
			redirect(base_url());
		}
	}

	public function delete($user_id = false)
	{
		$this->load->model('user_model');
		if ($user_id)
		{
			$data =  $this->user_model->get(array('id' => $user_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $user_id, 'is_active' => 0);
				$result = $this->user_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('user_sales/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('user_sales/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('user_sales/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('user_sales/list'));
		}
	}

	public function preview($user_id=FALSE)
	{
		$this->load->model('user_model');
		$data['_TITLE_'] 		= 'Preview Data Sales';
		$data['_PAGE_']	 		= 'user_sales/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'user_sales';

		$data['id'] = $user_id;

		
		if (!$user_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('user_sales/list'));
		}

		$data['preview'] = $this->user_model->preview(array('id' => $user_id));
		$this->load->view('user_sales/preview', $data);
	}

	public function form($user_id = FALSE)
	{
		$this->load->model(array('user_model', 'branch_model','role_model', 'auth_model', 'company_model'));

		$data['id']		 	= $user_id;
		$data['full_name'] 	= '';
		$data['username'] 	= '';
		$data['email'] 		= '';
		$data['company_id']	= '';
		$data['branch_id']	= '';
		$data['role_id']	= '';
		$data['password'] 	= '';
		$data['repassword'] = '';
		$data['tab']		= 'profile';

		if($this->input->post('full_name')){

			$params['id']			= $data['id'] 			= $this->input->post('id');
			$params['full_name'] 	= $data['full_name'] 	= $this->input->post('full_name');
			$params['username'] 	= $data['username'] 	= $this->input->post('username');
			$params['email']		= $data['email']		= $this->input->post('email');
			$params['company_id'] 	= $data['company_id'] 	= $this->input->post('company_id');
			$params['branch_id'] 	= $data['branch_id'] 	= $this->input->post('branch_id');
			$params['role_id']		= $data['role_id']		= $this->input->post('role_id');

			$this->form_validation->set_rules('username', '', 'required');
			$this->form_validation->set_rules('full_name', '', 'required');
			$this->form_validation->set_rules('email', '', 'required|valid_email');
			$this->form_validation->set_rules('company_id', '', 'required');
			$this->form_validation->set_rules('branch_id', '', 'required');
			$this->form_validation->set_rules('role_id', '', 'required');

			if($params['id'] == ''){
				$data['password'] 	= $this->input->post('password');
				$data['repassword'] = $this->input->post('repassword');
				
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('repassword', 'Ulangi Password', 'required|matches[password]');
				$params['password']	= generate_password($data['password']);				
			}


			$exist = $this->auth_model->get(array('username' => $data['username'], 'email' => $data['email']));
			if($exist){
				if($exist->id != $params['id']){
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> username atau email yang anda masukkan sudah terdaftar.','danger'));
					redirect(base_url('user_sales/list'));
				}
			}

			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
				redirect(base_url('user_sales/list'));
			}else{
				$result	 		= $this->user_model->save($params);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
					redirect(base_url('user_sales/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
					redirect(base_url('user_sales/list'));
				}		
			}
		}else{
			if($this->input->post('password')){
				$data['tab']		= 'password';
				$data['password'] 	= $this->input->post('password');
				$data['repassword'] = $this->input->post('repassword');
				
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('repassword', 'Ulangi Password', 'required|matches[password]');
				if ($this->form_validation->run() == FALSE)
				{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
					redirect(base_url('user_sales/list'));
				}else{
					$params['id']		= $data['id'] 			= $this->input->post('id');
					$params['password']	= generate_password($data['password']);
					$result	 			= $this->user_model->save($params);
					if ($result) {
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
						redirect(base_url('user_sales/list'));
					}else{
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
						redirect(base_url('user_sales/list'));
					}	
				}
			}

			$account = $this->user_model->get(array('id' => $user_id, 'columns' => 'A.full_name, A.username, A.email, A.branch_id, A.role_id, A.company_id'));

			if($account){
				$data['full_name'] 	= $account->full_name;
				$data['username'] 	= $account->username;
				$data['email'] 		= $account->email;
				$data['branch_id'] 	= $account->branch_id;
				$data['role_id'] 	= $account->role_id;
				$data['company_id'] = $account->company_id;
			}
		}

		$data['list_company'] 	= $this->company_model->gets(array());
		$data['list_branch'] 	= $this->branch_model->gets(array());
		$data['list_role'] 		= $this->role_model->gets(array('id' => 9));

		$data['_TITLE_'] 		= 'Form Sales';
		$data['_PAGE_'] 		= 'user_sales/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'user_sales';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Daftar Sales';
		$data['_PAGE_'] 		= 'user_sales/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'user_sales';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('user_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.username, A.email, A.full_name, B.name AS role_name, C.name AS branch_name, D.name AS company_name';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['role_sales']	= TRUE;

		$params['username']		= $_POST['columns'][1]['search']['value'];
		$params['email']		= $_POST['columns'][2]['search']['value'];
		$params['full_name']	= $_POST['columns'][3]['search']['value'];
		$params['role_name']	= $_POST['columns'][4]['search']['value'];
		$params['branch_name']	= $_POST['columns'][5]['search']['value'];
		$params['company_name']	= $_POST['columns'][6]['search']['value'];
		
		$list 	= $this->user_model->gets($params);
		$total 	= $this->user_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		
		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['username'] 	= $item->username;
			$result['email'] 		= $item->email;
			$result['full_name']	= $item->full_name;
			$result['role_name']	= $item->role_name;
			$result['branch_name']	= $item->branch_name;
			$result['company_name']	= $item->company_name;
			$result['action'] 	=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("user_sales/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("user_sales/form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("user_sales/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}
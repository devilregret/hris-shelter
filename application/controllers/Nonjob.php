<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Nonjob extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		$role 	= array_merge($role, $this->config->item('relation_officer_security'));
		$role 	= array_merge($role, $this->config->item('recruitment'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function list(){
		$this->load->model(array('site_model', 'position_model', 'company_model'));
		$data['_TITLE_'] 		= 'Karyawan Non Job';
		$data['_PAGE_'] 		= 'nonjob/list';
		$data['_MENU_PARENT_'] 	= 'other';
		$data['_MENU_'] 		= 'nonjob';
		$data['list_company'] 	= $this->company_model->gets(array('columns' => 'A.id, A.name, A.code', 'orderby' => 'A.name', 'order' => 'ASC'));
		$data['list_position'] 	= $this->position_model->gets(array());

		$branch_id	= '';
		if($this->session->userdata('branch') > 1){
			$branch_id= $this->session->userdata('branch');
		}
		$data['list_site'] 		= $this->site_model->gets(array('orderby' => 'id', 'order' => 'ASC', 'indirect' =>TRUE, 'columns' => 'A.id, A.name', 'branch_id' => $branch_id));
		
		$this->view($data);
	}

	public function list_ajax(){
		$this->load->model(array('employee_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.id_card, A.employee_number, A.full_name, A.site_id, C.name AS company_name, B.name AS site_name, E.name AS position, D.contract_type, DATE_FORMAT(D.contract_end, '%d/%m/%Y') AS contract_end, D.contract_end AS raw_end";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		// $params['status']		= 1;
		$params['status_nonjob'] 	= 1;
		$params['status_approval'] 	= 3;
		
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		$params['id_card']			= $_POST['columns'][1]['search']['value'];
		$params['employee_number']	= $_POST['columns'][2]['search']['value'];
		$params['full_name']		= $_POST['columns'][3]['search']['value'];
		$params['company_name']		= $_POST['columns'][4]['search']['value'];
		$params['site_name']		= $_POST['columns'][5]['search']['value'];
		$params['position']			= $_POST['columns'][6]['search']['value'];
		$params['contract_type']	= $_POST['columns'][7]['search']['value'];
		$params['contract_end']		= $_POST['columns'][8]['search']['value'];

		$list 	= $this->employee_model->gets($params);
		$total 	= $this->employee_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$type 	= 'indirect';
			if($item->site_id != 2){
				$type = 'direct';
			}
			
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['id_card']				= $item->id_card;
			$result['employee_number']		= $item->employee_number;
			$result['full_name']			= $item->full_name;
			$result['company_name']			= $item->company_name;
			$result['site_name']			= $item->site_name;
			$result['position']				= $item->position;
			$result['contract_type']		= $item->contract_type;
			$contract_end					= "";
			if($item->raw_end){
				$now 		= date("Y-m-d");
				$dt2 = new DateTime("+1 month");
				$next_month = $dt2->format("Y-m-d");
				if($item->raw_end < $now){
					$contract_end = '<span class="text-danger">'.$item->contract_end.'</span>';
				}else if($item->raw_end < $next_month){
					$contract_end = '<span class="text-warning">'.$item->contract_end.'</span>';
				}else{
					$contract_end = '<span class="text-success">'.$item->contract_end.'</span>';	
				}
			}else{
				$result['contract_end']		= $item->contract_end;
			}
			$result['contract_end']			= $item->contract_end;

			$result['action'] 				= '';
			if(in_array($this->session->userdata('role'), $this->config->item('personalia'))){
				$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("employee_contract/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("employee_contract/form/".$type."/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("employee_contract/delete/".$item->id).'">Hapus</a>
				<a class="btn-sm btn-danger btn-block" href="'.base_url("employee_contract/resign/".$type."/".$item->id).'">Resign</a>';
			}
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function process(){
		$this->load->model(array('employee_model', 'site_model'));

		$id 		= $this->input->post('id');
		$site_id	= $this->input->post('site_id');
		$position_id= $this->input->post('position_id');
		$company_id	= $this->input->post('company_id');


		foreach ($id as $candidate_id) {
			$this->load->model(array('employee_model', 'approval_model'));
			$data = (array) $this->employee_model->get(array('id' => $candidate_id));

			if (empty($data)){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('nonjob/list'));
			}else{
				$insert = array('id' => $candidate_id, 'status_nonjob' => 0, 'site_id' => $site_id, 'company_id' => $company_id, 'position_id' => $position_id, 'approved_at' => date('Y-m-d H:i:s'), 'approved_by' => $this->session->userdata('user_id'), 'resign_submit' => null);
					$result = $this->employee_model->save($insert);

				$approval = array('id' => '', 'employee_id' => $data['id'] , 'site_id' => $site_id, 'company_id' => $company_id, 'position_id' => $position_id, 'status_approval' => 'Diterima');
				$this->approval_model->save($approval);

				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal diproses.','danger'));
					redirect(base_url('employee/submission/'.$type));
				}
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil diproses.','success'));
		redirect(base_url('nonjob/list'));
	}

	public function export()
	{
		$this->load->model(array('employee_model', 'city_model', 'site_model', 'employee_skill_model', 'position_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = "Data Karyawan";
		$params = $this->input->get();
		$params['status'] = 1;
		$params['status_approval'] 	= 3;
		$params['status_nonjob'] 	= 1;
		$title = 'karyawan_nonjob';
		$files = glob(FCPATH."files/".$title."/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		if(isset($params['type'])){
			if($params['type'] == 'indirect'){
				$params['site_id'] = 2;
			}else{
				$params['not_site_id']		= 2;
			}
			unset($params['type']);
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Nomor Induk Karyawan");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("C".$i, "Nomor Identitas (KTP/SIM)");
		$excel->getActiveSheet()->setCellValue("D".$i, "Masa Berlaku (KTP/SIM)");
		$excel->getActiveSheet()->setCellValue("E".$i, "Tempat Lahir");
		$excel->getActiveSheet()->setCellValue("F".$i, "Tanggal Lahir");
		$excel->getActiveSheet()->setCellValue("G".$i, "Kota Sesuai KTP");
		$excel->getActiveSheet()->setCellValue("H".$i, "Alamat Sesuai KTP");
		$excel->getActiveSheet()->setCellValue("I".$i, "Kota Domisili");
		$excel->getActiveSheet()->setCellValue("J".$i, "Alamat Domisili");
		$excel->getActiveSheet()->setCellValue("K".$i, "Status Rumah");
		$excel->getActiveSheet()->setCellValue("L".$i, "No. Telephon/HP");
		$excel->getActiveSheet()->setCellValue("M".$i, "Email");
		$excel->getActiveSheet()->setCellValue("N".$i, "Facebook");
		$excel->getActiveSheet()->setCellValue("O".$i, "Instagram");
		$excel->getActiveSheet()->setCellValue("P".$i, "Twitter");
		$excel->getActiveSheet()->setCellValue("Q".$i, "Agama");
		$excel->getActiveSheet()->setCellValue("R".$i, "Status Pernikahan");
		$excel->getActiveSheet()->setCellValue("S".$i, "Jenis Kelamin");
		$excel->getActiveSheet()->setCellValue("T".$i, "Tinggi");
		$excel->getActiveSheet()->setCellValue("U".$i, "Berat");
		$excel->getActiveSheet()->setCellValue("V".$i, "Pendidikan Terakhir");
		$excel->getActiveSheet()->setCellValue("W".$i, "Jurusan");
		$excel->getActiveSheet()->setCellValue("X".$i, "Site Bisnis");
		$excel->getActiveSheet()->setCellValue("Y".$i, "Jabatan");
		$excel->getActiveSheet()->setCellValue("Z".$i, "Branch");
		$excel->getActiveSheet()->setCellValue("AA".$i, "Nama Ibu");
		$excel->getActiveSheet()->setCellValue("AB".$i, "Nama Pasangan");
		$excel->getActiveSheet()->setCellValue("AC".$i, "Nama Anak");
		$excel->getActiveSheet()->setCellValue("AD".$i, "Kode PTKP");
		$excel->getActiveSheet()->setCellValue("AE".$i, "BPJS TK");
		$excel->getActiveSheet()->setCellValue("AF".$i, "Asuransi Kesehatan");
		$excel->getActiveSheet()->setCellValue("AG".$i, "No Rekening");
		$excel->getActiveSheet()->setCellValue("AH".$i, "Atas Nama");
		$excel->getActiveSheet()->setCellValue("AI".$i, "NPWP");
		$excel->getActiveSheet()->setCellValue("AJ".$i, "Hasil Interview");
		$excel->getActiveSheet()->setCellValue("AK".$i, "Surat Lamaran Pekerjaan");
		$excel->getActiveSheet()->setCellValue("AL".$i, "Lampiran Riwayat Hidup");
		$excel->getActiveSheet()->setCellValue("AM".$i, "Foto KTP");
		$excel->getActiveSheet()->setCellValue("AN".$i, "Foto KK");
		$excel->getActiveSheet()->setCellValue("AO".$i, "Foto");
		$excel->getActiveSheet()->setCellValue("AP".$i, "Surat Sehat");
		$excel->getActiveSheet()->setCellValue("AQ".$i, "Foto Buku Nikah");
		$excel->getActiveSheet()->setCellValue("AR".$i, "SKCK");
		$excel->getActiveSheet()->setCellValue("AS".$i, "FOTO NPWP");

		$params['columns'] 		= 'A.*, B.name AS site_name, F.name AS branch_name, G.name AS city_birth, H.name AS city_card, I.name AS city_domisili, A.date_birth, E.name AS position_name';

		$list_candidate = $this->employee_model->gets($params);
		$i=2;
		foreach ($list_candidate as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->employee_number);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->id_card_period);
			$excel->getActiveSheet()->setCellValue("E".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_birth));

			$date = new DateTime($item->date_birth);
			$excel->getActiveSheet()->setCellValue("F".$i, PHPExcel_Shared_Date::PHPToExcel($date));
			$excel->getActiveSheet()->getStyle("F".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			
			// $excel->getActiveSheet()->setCellValue("F".$i, $item->date_birth2);
			$excel->getActiveSheet()->setCellValue("G".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_card));
			$excel->getActiveSheet()->setCellValue("H".$i, $item->address_card);
			$excel->getActiveSheet()->setCellValue("I".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_domisili));
			$excel->getActiveSheet()->setCellValue("J".$i, $item->address_domisili);
			$excel->getActiveSheet()->setCellValue("K".$i, $item->status_residance);
			$excel->getActiveSheet()->setCellValueExplicit("L".$i, $item->phone_number);
			$excel->getActiveSheet()->setCellValue("M".$i, $item->email);
			$excel->getActiveSheet()->setCellValue("N".$i, $item->socmed_fb);
			$excel->getActiveSheet()->setCellValue("O".$i, $item->socmed_ig);
			$excel->getActiveSheet()->setCellValue("P".$i, $item->socmed_tw);
			$excel->getActiveSheet()->setCellValue("Q".$i, $item->religion);
			$excel->getActiveSheet()->setCellValue("R".$i, $item->marital_status);
			$excel->getActiveSheet()->setCellValue("S".$i, $item->gender);
			$excel->getActiveSheet()->setCellValue("T".$i, $item->heigh);
			$excel->getActiveSheet()->setCellValue("U".$i, $item->weigh);
			$excel->getActiveSheet()->setCellValue("V".$i, $item->education_level);
			$excel->getActiveSheet()->setCellValue("W".$i, $item->education_majors);

			$excel->getActiveSheet()->setCellValue("X".$i, $item->site_name);
			$excel->getActiveSheet()->setCellValue("Y".$i, $item->position_name);
			$excel->getActiveSheet()->setCellValue("Z".$i, $item->branch_name);
			$excel->getActiveSheet()->setCellValue("AA".$i, $item->family_mother);
			$excel->getActiveSheet()->setCellValue("AB".$i, $item->family_mate);
			$excel->getActiveSheet()->setCellValue("AC".$i, $item->family_child);
			$excel->getActiveSheet()->setCellValue("AD".$i, $item->ptkp);
			$excel->getActiveSheet()->setCellValue("AE".$i, $item->benefit_labor);
			$excel->getActiveSheet()->setCellValue("AF".$i, $item->benefit_health);
			$excel->getActiveSheet()->setCellValueExplicit("AG".$i, $item->bank_account);
			$excel->getActiveSheet()->setCellValue("AH".$i, $item->bank_account_name);
			$excel->getActiveSheet()->setCellValue("AI".$i, $item->tax_number);
			$excel->getActiveSheet()->setCellValue("AJ".$i, $item->document_interview);
			$excel->getActiveSheet()->setCellValue("AM".$i, $item->document_id_card);
			$excel->getActiveSheet()->setCellValue("AN".$i, $item->document_family_card);
			$excel->getActiveSheet()->setCellValue("AO".$i, $item->document_photo);
			$excel->getActiveSheet()->setCellValue("AP".$i, $item->document_doctor_note);
			$excel->getActiveSheet()->setCellValue("AQ".$i, $item->document_marriage_certificate);
			$excel->getActiveSheet()->setCellValue("AR".$i, $item->document_police_certificate);
			$excel->getActiveSheet()->setCellValue("AS".$i, $item->document_tax_number);

			$list_skill 	= $this->employee_skill_model->gets(array('employee_id' => $item->id, 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));
			$skill = "";
			foreach ($list_skill as $item_skill) {
				$skill .=" ".$item_skill->certificate_name.":".$item_skill->certificate_number.":".$item_skill->certificate_document.",";
			}

			$excel->getActiveSheet()->setCellValue("AT".$i, $skill);
			$i++;
		}
		$excel->getActiveSheet()->setTitle($title);
		
		$excel->createSheet();
		$excel->setActiveSheetIndex(1);
		$excel->getActiveSheet()->setCellValue('A1', 'ID Kota');
		$excel->getActiveSheet()->setCellValue('B1', 'Nama Kota');

		$list_city	= $this->city_model->gets(array('orderby' => 'A.name', 'order' => 'ASC'));
		$j=2;
		foreach ($list_city as $item) {
			$excel->getActiveSheet()->setCellValue("A".$j,$item->id);
			$excel->getActiveSheet()->setCellValue("B".$j,$item->name);
			$j++;
		}
		$excel->getActiveSheet()->setTitle('Daftar Kota');

		$excel->createSheet();
		$excel->setActiveSheetIndex(2);
		$excel->getActiveSheet()->setCellValue('A1', 'ID Site');
		$excel->getActiveSheet()->setCellValue('B1', 'Nama Site Bisnis');
		$excel->getActiveSheet()->setCellValue('C1', 'Nama Unit Kontrak');

		$list_site 	= $this->site_model->gets(array('orderby' => 'A.name', 'order' => 'ASC'));
		$k=2;
		foreach ($list_site as $item) {
			$excel->getActiveSheet()->setCellValue("A".$k,$item->id);
			$excel->getActiveSheet()->setCellValue("B".$k,$item->name);
			$excel->getActiveSheet()->setCellValue("C".$k,$item->company_name);
			$k++;
		}
		$excel->getActiveSheet()->setTitle('Daftar Site');
		
		$excel->createSheet();
		$excel->setActiveSheetIndex(3);
		$excel->getActiveSheet()->setCellValue('A1', 'ID Jabatan');
		$excel->getActiveSheet()->setCellValue('B1', 'Nama Jabatan');

		$list_position 	= $this->position_model->gets(array('orderby' => 'A.name', 'order' => 'ASC'));
		$l=2;
		foreach ($list_position as $item) {
			$excel->getActiveSheet()->setCellValue("A".$l,$item->id);
			$excel->getActiveSheet()->setCellValue("B".$l,$item->name);
			$l++;
		}
		$excel->getActiveSheet()->setTitle('Daftar Jabatan');
		
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function submit(){
		$type 			=$this->input->post('type');
		foreach ($this->input->post('id') as $employee_id)
		{
			$this->load->model(array('employee_model', 'approval_model'));
			$data = $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.company_id, A.site_id, A.position_id, A.resign_note'));
			if (!$data){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('resign/approval/'.$type));
			}else{
				if($this->input->post('action') == 'reject'){
					$insert = array('id' => $employee_id, 'status_approval' => 3);
					$result = $this->employee_model->save($insert);

					$approval = array('id' => '', 'employee_id' => $employee_id, 'site_id' => $data->site_id, 'company_id' => $data->company_id, 'position_id' => $data->position_id, 'status_approval' => 'Nonjob Ditolak', 'note' => $data->resign_note);
						$this->approval_model->save($approval);
				}else{
					$approval = array('id' => '', 'employee_id' => $employee_id, 'site_id' => $data->site_id, 'company_id' => $data->company_id, 'position_id' => $data->position_id, 'status_approval' => 'Nonjob', 'note' => $data->resign_note);
					$this->approval_model->save($approval);
					$this->employee_model->save(array('id' => $employee_id, 'status' => 1, 'status_approval' => 3, 'status_nonjob' => 1));
				}
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diupdate.','success'));
		redirect(base_url('nonjob/approval'));

	}

	public function approval($type = FALSE){
		$data['_TITLE_'] 		= 'Pengajuan Nonjob';
		$data['_PAGE_'] 		= 'nonjob/approval';
		$data['_MENU_PARENT_'] 	= 'other';
		$data['_MENU_'] 		= 'nonjob_approval';
		$this->view($data);
	}

	public function approval_ajax(){
		$this->load->model(array('roresign_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.employee_number, A.id_card, A.full_name, DATE_FORMAT(A.resign_submit, '%d/%m/%Y') AS resign_submit, A.resign_note, A.resign_burden, B.name AS site_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		// $params['status']		= 1;
		
		$params['status_approval'] 		= 5;
		
		$params['branch_id']	= '';
		// if($this->session->userdata('branch') > 1){
		// 	$params['branch_id']= $this->session->userdata('branch');
		// }
		
		$params['employee_number']		= $_POST['columns'][1]['search']['value'];
		$params['id_card']				= $_POST['columns'][2]['search']['value'];
		$params['full_name']			= $_POST['columns'][3]['search']['value'];
		$params['site_name']			= $_POST['columns'][4]['search']['value'];
		$params['resign_note']			= $_POST['columns'][5]['search']['value'];
		$params['resign_burden']		= $_POST['columns'][6]['search']['value'];
		$params['resign_submit']		= $_POST['columns'][7]['search']['value'];

		$list 	= $this->roresign_model->gets($params);
		$total 	= $this->roresign_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{

			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['employee_number']		= $item->employee_number;
			$result['id_card']				= $item->id_card;
			$result['full_name']			= $item->full_name;
			$result['site_name']			= $item->site_name;
			$result['resign_note']			= $item->resign_note;
			$result['resign_burden']		= $item->resign_burden;
			$result['resign_submit']		= $item->resign_submit;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("employee/preview/".$item->id).'">Lihat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring_benefit extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('benefit'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function site_preview($site_id=FALSE)
	{
		$this->load->model('site_model');
		$data['_TITLE_'] 		= 'Preview Site Bisnis';
		$data['_PAGE_']	 		= 'monitoring_benefit/preview';
		$data['_MENU_PARENT_'] 	= 'monitoring_benefit';
		$data['_MENU_'] 		= 'monitoring_site';

		$data['id'] = $site_id;

		
		if (!$site_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('monitoring_benefit/site'));
		}

		$data['preview'] = $this->site_model->preview(array('id' => $site_id));
		$this->load->view('monitoring_benefit/preview', $data);
	}

	public function site_process()
	{

		$this->load->model(array('site_model'));
		$action = $this->input->post('action');
		foreach ($this->input->post('id') as $id)
		{
			if($action == 'labor'){
				$this->site_model->save(array('id' => $id, 'labor_Insurance_status' => 'Dihentikan'));
				$this->site_model->update_employee(array('site_id' => $id, 'benefit_labor' => ''));
			}else if($action == 'health'){
				$result = $this->site_model->save(array('id' => $id, 'health_Insurance_status' => 'Dihentikan'));
				$this->site_model->update_employee(array('site_id' => $id, 'benefit_health' => ''));
			}

		}

		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diproses.','success'));
		redirect(base_url('monitoring_benefit/site'));
	}

	public function site(){
		$data['_TITLE_'] 		= 'Monitoring Site Bisnis';
		$data['_PAGE_'] 		= 'monitoring_benefit/site';
		$data['_MENU_PARENT_'] 	= 'monitoring_benefit';
		$data['_MENU_'] 		= 'monitoring_site';

		$this->view($data);

	}

	public function site_ajax(){
		$this->load->model(array('site_model', 'employee_model'));

		$column_index = $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.code, A.name, A.address, A.contract_status, A.health_insurance_status, A.labor_insurance_status, B.name AS city_name, C.name AS company_name, F.name AS branch, G.name AS client_name";
		$params['orderby'] 			= $_POST['columns'][$column_index]['data'];
		$params['order']			= $_POST['order'][0]['dir'];
		$params['limit']			= $_POST['length'];
		$params['page']				= $_POST['start'];
		$params['non_active']		= TRUE;
		$params['assurance']		= TRUE;

		$params['name']				= $_POST['columns'][1]['search']['value'];
		$params['client_name']		= $_POST['columns'][2]['search']['value'];
		$params['company_name']		= $_POST['columns'][3]['search']['value'];
		$params['address']			= $_POST['columns'][4]['search']['value'];
		$params['branch']			= $_POST['columns'][5]['search']['value'];
		
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		$list 	= $this->site_model->gets_site($params);
		$total 	= $this->site_model->gets_site($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['id']						= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['name'] 					= $item->name;
			$result['client_name']				= $item->client_name;
			$result['address']					= $item->address;
			$result['company_name']				= $item->company_name;
			$result['branch']					= $item->branch;
			$result['count_employee']			= $this->employee_model->count(array('site_id' => $item->id, 'status_approval' => 3), TRUE);
			$result['health_insurance_status']	= ($item->health_insurance_status=='Terdaftar'? '<span class="text-success">'.$item->health_insurance_status.'</span>': '<span class="text-danger">'.$item->health_insurance_status.'</span>');
			$result['labor_insurance_status']	= ($item->labor_insurance_status=='Terdaftar'? '<span class="text-success">'.$item->labor_insurance_status.'</span>': '<span class="text-danger">'.$item->labor_insurance_status.'</span>');
			if($item->contract_status == 'Aktif'){
				$result['contract_status'] = '<span class="text-success">'.$item->contract_status.'</span>';
			}else{
				$result['contract_status'] = '<span class="text-danger">'.$item->contract_status.'</span>';
			}

			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("monitoring_benefit/site_preview/".$item->id).'">Lihat</a>';
			array_push($data, $result);
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function employee_process()
	{

		$this->load->model(array('employee_model'));
		$action = $this->input->post('action');
		foreach ($this->input->post('id') as $id)
		{
			if($action == 'labor'){
				$this->employee_model->save(array('id' => $id, 'benefit_labor' => ''));
			}else if($action == 'health'){
				$this->employee_model->save(array('id' => $id, 'benefit_health' => ''));
			}

		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diproses.','success'));
		redirect(base_url('monitoring_benefit/employee'));
	}

	public function employee(){
		$data['_TITLE_'] 		= 'Monitoring Karyawan';
		$data['_PAGE_'] 		= 'monitoring_benefit/employee';
		$data['_MENU_PARENT_'] 	= 'monitoring_benefit';
		$data['_MENU_'] 		= 'monitoring_employee';

		$this->view($data);
	}

	public function employee_ajax(){
		$this->load->model(array('employee_model'));

		$column_index = $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.id_card, A.full_name, A.address_card, B.name AS site_name, C.name AS company_name, A.benefit_health, A.benefit_labor";
		$params['orderby'] 			= $_POST['columns'][$column_index]['data'];
		$params['order']			= $_POST['order'][0]['dir'];
		$params['limit']			= $_POST['length'];
		$params['page']				= $_POST['start'];
		$params['assurance_active']	= TRUE;

		$params['id_card']			= $_POST['columns'][1]['search']['value'];
		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['address_card']		= $_POST['columns'][3]['search']['value'];
		$params['site_name']		= $_POST['columns'][4]['search']['value'];
		$params['company_name']		= $_POST['columns'][5]['search']['value'];
		$params['benefit_health']	= $_POST['columns'][6]['search']['value'];
		$params['benefit_labor']	= $_POST['columns'][7]['search']['value'];
		
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		
		$list 	= $this->employee_model->gets($params);
		$total 	= $this->employee_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['id']						= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['id_card'] 					= $item->id_card;
			$result['full_name']				= $item->full_name;
			$result['address_card']				= $item->address_card;
			$result['site_name']				= $item->site_name;
			$result['company_name']				= $item->company_name;
			$result['benefit_health']			= $item->benefit_health;
			$result['benefit_labor']			= $item->benefit_labor;
			$result['health_insurance_status']	= ($item->benefit_health!=''? '<span class="text-success">Terdaftar</span>': '<span class="text-danger">Dihentikan</span>');
			$result['labor_insurance_status']	= ($item->benefit_labor!=''? '<span class="text-success">Terdaftar</span>': '<span class="text-danger">Dihentikan</span>');

			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("benefit_employee/preview/".$item->id).'">Lihat</a>';
			array_push($data, $result);
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function login(){
		$data['_TITLE_'] 		= 'Monitoring Karyawan Login';
		$data['_PAGE_'] 		= 'monitoring_benefit/login';
		$data['_MENU_PARENT_'] 	= 'monitoring_benefit';
		$data['_MENU_'] 		= 'login';

		$this->view($data);

	}

	public function login_ajax(){
		$this->load->model(array('user_model', 'employee_model'));

		$column_index = $_POST['order'][0]['column'];
		$params['columns'] 		= "H.id, H.id_card, H.full_name, B.name AS role_name, D.code AS company_code, C.name AS branch_name, DATE_FORMAT(A.updated_at, '%d/%m/%Y') AS last_login";
		$params['orderby'] 			= $_POST['columns'][$column_index]['data'];
		$params['order']			= $_POST['order'][0]['dir'];
		$params['limit']			= $_POST['length'];
		$params['page']				= $_POST['start'];
		$params['site_id']			= 2;

		$params['id_card']			= $_POST['columns'][0]['search']['value'];
		$params['full_name']		= $_POST['columns'][1]['search']['value'];
		$params['role_name']		= $_POST['columns'][2]['search']['value'];
		$params['company_code']		= $_POST['columns'][3]['search']['value'];
		$params['branch_name']		= $_POST['columns'][4]['search']['value'];
		$params['updated_at']		= $_POST['columns'][5]['search']['value'];
		
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		$list 	= $this->user_model->gets($params);
		$total 	= $this->user_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['id_card'] 				= $item->id_card;
			$result['full_name'] 			= $item->full_name;
			$result['role_name']			= $item->role_name;
			$result['company_code']			= $item->company_code;
			$result['branch_name']			= $item->branch_name;
			$result['last_login']			= $item->last_login;
			array_push($data, $result);
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}
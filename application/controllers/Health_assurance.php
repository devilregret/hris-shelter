<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Health_assurance extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('benefit'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,6,13,14))){
			redirect(base_url());
		}
	}

	public function delete($assurance_id = false)
	{
		$this->load->model('health_assurance_model');
		if ($assurance_id)
		{
			$data =  $this->health_assurance_model->get(array('id' => $assurance_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $assurance_id, 'is_active' => 0);
				$result = $this->health_assurance_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('health_assurance/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('health_assurance/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('health_assurance/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('health_assurance/list'));
		}
	}

	public function preview($assurance_id=FALSE)
	{
		$this->load->model('health_assurance_model');
		$data['_TITLE_'] 		= 'Preview Asuransi Kesehatan';
		$data['_PAGE_']	 		= 'health_assurance/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'health_assurance';

		$data['id'] = $assurance_id;

		
		if (!$assurance_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('health_assurance/list'));
		}

		$data['preview'] = $this->health_assurance_model->preview(array('id' => $assurance_id));
		$this->load->view('health_assurance/preview', $data);
	}

	public function form($assurance_id = FALSE)
	{
		$this->load->model(array('health_assurance_model'));

		$data['id'] 		= '';
		$data['name']		= '';
		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['name'] 		= $this->input->post('name');
			
			$this->form_validation->set_rules('name', '', 'required');
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$save_id	 	= $this->health_assurance_model->save($data);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('health_assurance/list'));
		}

		if ($assurance_id)
		{
			$data = (array) $this->health_assurance_model->get(array('id' => $assurance_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('health_assurance/list'));
			}
		}

		$data['_TITLE_'] 		= 'Asuransi Kesehatan';
		$data['_PAGE_'] 		= 'health_assurance/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'health_assurance';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Asuransi Kesehatan';
		$data['_PAGE_'] 		= 'health_assurance/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'health_assurance';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('health_assurance_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.name AS name';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['name']			= $_POST['columns'][1]['search']['value'];
		
		$list 	= $this->health_assurance_model->gets($params);
		$total 	= $this->health_assurance_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['name'] 		= $item->name;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("health_assurance/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("health_assurance/form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("health_assurance/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}
}
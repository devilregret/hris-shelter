<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Legal_report extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('sales'));
		$role 	= array_merge($role, $this->config->item('legal'));
		// $role 	= array_merge($role, $this->config->item('accounting'));
		$role 	= array_merge($role, $this->config->item('directur'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}
	
	public function legality($company = FALSE){
		$this->load->model(array('company_model'));
		$data['_TITLE_'] 		= 'Daftar Perizinan';
		$data['_PAGE_'] 		= 'legal_report/legality';
		$data['_MENU_PARENT_'] 	= 'report';
		$data['_MENU_'] 		= 'report_legality';
		$data['company']		= $company;
		$data['list_company'] 	= $this->company_model->gets(array('columns' => 'A.code'));
		$this->view($data);
	}

	public function legality_ajax($company = FALSE){
		$this->load->model(array('legality_model'));

		$column_index 			= $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.name, B.code AS company_code, C.name AS branch_name, DATE_FORMAT(A.date_start, '%d/%m/%Y') AS date_start, DATE_FORMAT(A.date_end, '%d/%m/%Y') AS date_end, A.note, A.files";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['status']		= 'Pengajuan MOU';

		$params['name']				= $_POST['columns'][1]['search']['value'];
		$params['company_code']		= $_POST['columns'][2]['search']['value'];
		$params['branch_name']		= $_POST['columns'][3]['search']['value'];
		$params['date_start']		= $_POST['columns'][4]['search']['value'];
		$params['date_end']			= $_POST['columns'][5]['search']['value'];
		$params['note']				= $_POST['columns'][6]['search']['value'];

		if($company){
			$params['company_code'] = $company;
		}
		$list 	= $this->legality_model->gets($params);
		$total 	= $this->legality_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 				= $i;
			$result['name']				= $item->name;
			$result['company_code']		= $item->company_code;
			$result['branch_name']		= $item->branch_name;
			$result['date_start'] 		= $item->date_start;
			$result['date_end'] 		= '-';
			if($item->date_end != '00/00/0000'){
				$result['date_end'] 	= $item->date_end;
			}
			$result['note']				= $item->note;
			$result['files'] 			= '';
			if($item->files != ''){
				$result['files']			= '<a href="'.$item->files.'"" target="_blank">Dokumen</a>';
			}

			$result['action'] 		=
				'';
	
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function contract(){
		$data['_TITLE_'] 		= 'Daftar Kontrak Kerjasama';
		$data['_PAGE_'] 		= 'legal_report/contract';
		$data['_MENU_PARENT_'] 	= 'report';
		$data['_MENU_'] 		= 'report_contract';
		$this->view($data);
	}

	public function contract_ajax(){
		$this->load->model(array('mou_model'));
		$column_index 			= $_POST['order'][0]['column'];
		$params['columns'] 		= "A.id, A.company_name, B.code AS old_company_code, C.code AS company_code, D.name AS province_name, E.name AS city_name, DATE_FORMAT(A.contract_start, '%d/%m/%Y') AS contract_start, DATE_FORMAT(A.contract_end, '%d/%m/%Y') AS contract_end, A.headcount, A.crm, A.status_registration, A.status_mou, A.files";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['company_name']			= $_POST['columns'][0]['search']['value'];
		$params['company_code']			= $_POST['columns'][1]['search']['value'];
		$params['province_name']		= $_POST['columns'][2]['search']['value'];
		$params['city_name']			= $_POST['columns'][3]['search']['value'];
		$params['contract_start']		= $_POST['columns'][4]['search']['value'];
		$params['contract_end']			= $_POST['columns'][5]['search']['value'];
		$params['headcount']			= $_POST['columns'][6]['search']['value'];
		$params['crm']					= $_POST['columns'][7]['search']['value'];
		$params['status_mou']			= $_POST['columns'][8]['search']['value'];
		$params['status_registration']	= $_POST['columns'][9]['search']['value'];

		$list 	= $this->mou_model->gets($params);
		$total 	= $this->mou_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 					= $i;
			$result['company_name']			= $item->company_name;
			$result['company_code']			= 'Awal : <strong>'.$item->old_company_code.'</strong><br> Baru : <strong>'.$item->company_code.'</strong>';
			$result['province_name']		= $item->province_name;
			$result['city_name']			= $item->city_name;
			$result['contract_start'] 		= $item->contract_start;
			$result['contract_end'] 		= $item->contract_end;
			$result['headcount'] 			= $item->headcount;
			$result['crm']					= $item->crm;
			$result['status_mou'] 			= $item->status_mou;
			$result['status_registration'] 	= $item->status_registration;
			$result['files'] 			= '';
			if($item->files != ''){
				$result['files']			= '<a href="'.$item->files.'"" target="_blank">Dokumen</a>';
			}
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("legal_report/contract_preview/".$item->id).'">Lihat</a>';
	
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function contract_preview($contract_id=FALSE)
	{
		$this->load->model('mou_model');
		$data['_TITLE_'] 		= 'Preview Kerjasama';
		$data['_PAGE_']	 		= 'mou/contract_preview';
		$data['_MENU_PARENT_'] 	= 'cooperation';
		$data['_MENU_'] 		= 'registration';

		$params['id']			= $contract_id;
		$params['columns'] 		= "A.id, A.company_name, B.code AS old_company_code, C.code AS company_code, D.name AS province_name, E.name AS city_name, DATE_FORMAT(A.contract_start, '%d/%m/%Y') AS contract_start, DATE_FORMAT(A.contract_end, '%d/%m/%Y') AS contract_end, A.headcount, A.basic_salary, A.basic_salary2, A.bpjs_tk, A.bpjs_ks, A.labor_insurance, A.health_insurance, A.ohc, A.kaporlap, A.chemicals, A.training, A.date_invoice, A.date_payment, A.crm, A.status_registration, A.status_mou, A.files";
		
		$preview = $this->mou_model->gets($params);
		$data['preview'] = $preview[0];
		$this->load->view('mou/contract_preview', $data);
	}
}
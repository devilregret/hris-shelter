<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Candidate extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('recruitment'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function list($type = 'all', $branch = FALSE){
		$this->load->model(array('site_model', 'company_model', 'position_model', 'branch_model', 'vacancy_model', 'candidate_model'));
		$data['_TITLE_'] 		= 'Daftar Pelamar';
		$data['_PAGE_'] 		= 'candidate/list';
		$data['_MENU_PARENT_'] 	= 'recruitment';
		$data['_MENU_'] 		= 'candidate';

		$data['type'] 			= $type;
		if($branch){
			$data['branch'] 	= $branch;
		}else{
			$data['branch'] 	= $this->session->userdata('branch');
		}

		$list_site 		= $this->site_model->gets(array('columns' => 'A.id, A.name', 'branch_id' => $this->session->userdata('branch'), 'orderby' => 'A.id', 'order' => 'ASC'));
		
		$object 		= new stdClass();
		$object->id 	= 2;
		$object->name 	= 'INDIRECT';
		$indirect 		=  array($object);

		$data['list_vacancy'] 	= $this->vacancy_model->gets(array('columns' => 'A.id, A.title, B.name AS province_name'));
		$data['list_site'] = array_merge($indirect, $list_site);
		$data['list_company'] 	= $this->company_model->gets(array('columns' => 'A.id, A.name, A.code', 'orderby' => 'A.name', 'order' => 'ASC'));
		$data['list_position'] 	= $this->position_model->gets(array('columns' => 'A.id, A.name'));
		$data['list_branch'] 	= $this->branch_model->gets(array('columns' => 'A.id, A.name'));

		$params['session_branch'] 	= '';
		if($branch > 1){
			$params['session_branch']	= $branch;
		}
		
		$data['candidate_active']		= $this->candidate_model->gets(array('session_branch' => $params['session_branch'], 'status_approval' => 0, 'active' => TRUE), TRUE);
		$data['candidate_nonactive']	= $this->candidate_model->gets(array('session_branch' => $params['session_branch'], 'status_approval' => 0, 'nonactive' => TRUE), TRUE);
		$data['candidate_all'] 			= $data['candidate_active'] + $data['candidate_nonactive'];
		$data['candidate_notcomplete']	= $this->candidate_model->gets(array('session_branch' => $params['session_branch'], 'status_approval' => 0, 'status_nonjob' => 0, 'status_completeness' => 0), TRUE);
		$data['candidate_nonjob']		= $this->candidate_model->gets(array('session_branch' => $params['session_branch'], 'status_approval' => 0, 'status_nonjob' => 1), TRUE);
		$data['candidate_notprocess']	= $this->candidate_model->gets(array('session_branch' => $params['session_branch'], 'status_approval' => 0, 'not_followup' => true), TRUE);
		$data['candidate_view']			= $this->candidate_model->gets(array('session_branch' => $params['session_branch'], 'status_approval' => 0, 'followup_status' => 'lihat'), TRUE);
		$data['candidate_call']			= $this->candidate_model->gets(array('session_branch' => $params['session_branch'], 'status_approval' => 0, 'followup_status' => 'hubungi'), TRUE);
		$data['candidate_process']		= $this->candidate_model->gets(array('session_branch' => $params['session_branch'], 'status_approval' => 0, 'followup_status' => 'proses'), TRUE);
		$data['candidate_reject']		= $this->candidate_model->gets(array('session_branch' => $params['session_branch'], 'status_approval' => 0, 'followup_status' => 'tolak'), TRUE);
		$data['candidate_block']		= $this->candidate_model->gets(array('session_branch' => $params['session_branch'], 'status_approval' => 0, 'followup_status' => 'blokir'), TRUE);
		$this->view($data);
	}

	public function list_ajax($type = 'all', $branch = 1){

		$this->load->model(array('candidate_model', 'approval_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.registration_number, DATE_FORMAT(A.created_at, '%d/%m/%Y') AS date_registration, DATE_FORMAT(A.updated_at, '%d/%m/%Y') AS last_update, A.full_name, A.id_card, FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25) AS age, A.gender, A.education_level, A.education_majors, A.phone_number, B.name AS city_domisili, CONCAT_WS(', ', D.name, E.name, F.name) AS preference_job, GROUP_CONCAT(C.certificate_name) AS certificate, L.name AS province_domisili, A.site_preference, A.status_completeness, A.status_nonjob, DATE_FORMAT(A.updated_at, '%Y-%m-%d') AS updated_at, CASE WHEN A.updated_at < NOW() - INTERVAL 3 MONTH THEN 'non aktif' ELSE 'aktif' END AS activated, A.followup_status, A.followup_note, A.document_cv ";
		$params['orderby'] 			= $_POST['columns'][$column_index]['data'];
		$params['order']			= $_POST['order'][0]['dir'];
		$params['limit']			= $_POST['length'];
		$params['page']				=  $_POST['start'];

		if($type == 'active'){
			$params['active'] 				= TRUE;
			// $params['status_completeness'] 	= 1;
			$params['status_approval'] 		= 0;
		}else if($type == 'nonactive'){
			$params['nonactive'] 	= TRUE;
			// $params['status_completeness'] 	= 1;
			$params['status_approval'] 	= 0;
		}else if($type == 'notcomplete'){
			$params['status_completeness'] 	= 0;
			$params['status_nonjob'] 		= 0;
			$params['status_approval'] 		= 0;
		}else if($type == 'nonjob'){
			$params['status_nonjob'] 		= 1;
			$params['status_approval'] 		= 0;
		}else if($type == 'not_followup'){
			$params['not_followup'] 		= true;
			$params['status_approval'] 		= 0;
		}else if($type == 'lihat'){
			$params['followup_status'] 	= 'lihat';
			$params['status_approval'] 		= 0;
		}else if($type == 'hubungi'){
			$params['followup_status'] 	= 'hubungi';
			$params['status_approval'] 		= 0;
		}else if($type == 'proses'){
			$params['followup_status'] 	= 'proses';
			$params['status_approval'] 		= 0;
		}else if($type == 'tolak'){
			$params['followup_status'] 	= 'tolak';
			$params['status_approval'] 		= 0;
		}else if($type == 'blokir'){
			$params['followup_status'] 	= 'blokir';
			$params['status_approval'] 		= 0;
		}else{
			$params['status_approval'] 		= 0;
		}

		$params['created_at']			= $_POST['columns'][1]['search']['value'];
		$params['updated_at']			= $_POST['columns'][2]['search']['value'];
		$params['registration_number']	= $_POST['columns'][3]['search']['value'];
		$params['full_name']			= $_POST['columns'][4]['search']['value'];
		$params['id_card']				= $_POST['columns'][5]['search']['value'];
		$params['age']					= $_POST['columns'][6]['search']['value'];
		$params['gender']				= $_POST['columns'][7]['search']['value'];
		$params['education_level']		= $_POST['columns'][8]['search']['value'];
		$params['education_majors']		= $_POST['columns'][9]['search']['value'];
		$params['phone_number']			= $_POST['columns'][10]['search']['value'];
		$params['city_domisili']		= $_POST['columns'][11]['search']['value'];
		$params['province_domisili']	= $_POST['columns'][12]['search']['value'];
		$params['preference_job']		= $_POST['columns'][13]['search']['value'];
		$params['site_preference']		= $_POST['columns'][14]['search']['value'];
		$params['certificate']			= $_POST['columns'][15]['search']['value'];

		if($branch > 1){
			$params['session_branch']	= $branch;
		}
		
		$list 	= $this->candidate_model->gets($params);
		$total 	= $this->candidate_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{	

			$result['id'] 				= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';

			$result['no'] 					= $i;
			$result['last_update']			= $item->last_update;
			$result['date_registration']	= $item->date_registration;
			$result['registration_number']	= $item->registration_number;
			$result['full_name']			= $item->full_name;
			$result['id_card']				= $item->id_card;
			$result['age']					= $item->age;
			$result['gender']				= $item->gender;
			$result['education_level']		= $item->education_level;
			$result['education_majors']		= $item->education_majors;
			$list_number 					= $item->phone_number;
			$phone_number					= "";
			if($list_number != ""){
				$phone_number = explode ("/", $list_number);
				$phone_number = preg_replace("/[^0-9]/", "", $phone_number[0]);
				$start = substr($phone_number, 0, 1 );
				if($start == '0'){
					$phone_number = '62'.substr($phone_number, 1,strlen($phone_number));
				}else if($start == '+'){
					$phone_number = substr($phone_number, 1,strlen($phone_number));
				}
				$phone_number = '<a href="http://wa.me/'.$phone_number.'" target="_blank">'.$item->phone_number.'</a>';
			}
			$result['phone_number']			= $phone_number;
			$result['city_domisili']		= $item->city_domisili; 
			$result['province_domisili']	= $item->province_domisili; 
			$result['preference_job']		= $item->preference_job;
			$result['site_preference']		= $item->site_preference;
			$result['certificate']			= $item->certificate;
			$result['followup_status']		= $item->followup_status;
			$result['followup_note']		= $item->followup_note;
			$result['document_cv']			= '';
			if($item->document_cv){
				$result['document_cv']		= '<span class="text-success"><a href="'.$item->document_cv.'">Curriculum Vitae (CV)</a></span>';
			}
			$approval_history  	= $this->approval_model->get(array('employee_id' => $item->id, 'status_approval' => $item->followup_status, 'columns' => 'DATE_FORMAT(MAX(A.created_at), "%d/%m/%Y %h:%i:%s") AS last_update'));
			if($approval_history){
				$result['followup_status'] = $result['followup_status'].'<br>'.$approval_history->last_update;
			}

			if($item->followup_status === 'Blokir'){
				$result['id'] = '';
			}

			$result['candidate_status']		= '<span class="text-success">'.$item->activated.'</span>';
			if($item->status_nonjob == 1){
				$result['candidate_status'] = '<span class="text-warning">Nonjob</span>';
			}else if($item->status_completeness == 0){
				$result['candidate_status'] = '<span class="text-danger">Belum Lengkap</span>';
			}else if($item->activated == 'non aktif'){
				$result['candidate_status']		= '<span class="text-warning">'.$item->activated.'</span>';
			}


			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("candidate/preview/".$item->id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("candidate/curriculum_vitae/".$item->id).'">CV</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("approval/approval_history/".$item->id).'">Riwayat</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("candidate/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function export_status($type = '')
	{
		$this->load->model(array('candidate_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$params = $this->input->get();
		$title = 'pelamar_lowongan';
		$files = glob(FCPATH."files/".$title."/*");

		$params['columns'] 		= "A.id, A.registration_number, A.full_name, A.id_card, A.date_birth, A.address_domisili, A.phone_number, A.email, A.religion, A.marital_status, A.gender, A.heigh, A.weigh, A.education_level, A.education_majors, B.name AS city_domisili";
		$params['followup_status'] 	= $type;
		$list 	= $this->candidate_model->gets($params);

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Nomor Pendaftaran");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("C".$i, "NIK (KTP)");
		$excel->getActiveSheet()->setCellValue("D".$i, "Tanggal Lahir");
		$excel->getActiveSheet()->setCellValue("E".$i, "Kota Domisili");
		$excel->getActiveSheet()->setCellValue("F".$i, "Alamat Domisili");
		$excel->getActiveSheet()->setCellValue("G".$i, "No. Telephon/HP");
		$excel->getActiveSheet()->setCellValue("H".$i, "Email");
		$excel->getActiveSheet()->setCellValue("I".$i, "Agama");
		$excel->getActiveSheet()->setCellValue("J".$i, "Status Pernikahan");
		$excel->getActiveSheet()->setCellValue("K".$i, "Jenis Kelamin");
		$excel->getActiveSheet()->setCellValue("L".$i, "Tinggi");
		$excel->getActiveSheet()->setCellValue("M".$i, "Berat");
		$excel->getActiveSheet()->setCellValue("N".$i, "Pendidikan Terakhir");
		$excel->getActiveSheet()->setCellValue("O".$i, "Jurusan");

		$i=2;
		foreach ($list as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->registration_number);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->id_card);
			$date = new DateTime($item->date_birth);
			$excel->getActiveSheet()->setCellValue("D".$i, PHPExcel_Shared_Date::PHPToExcel($date));
			$excel->getActiveSheet()->getStyle("D".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			$excel->getActiveSheet()->setCellValue("E".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_domisili));
			$excel->getActiveSheet()->setCellValue("F".$i, $item->address_domisili);
			$excel->getActiveSheet()->setCellValueExplicit("G".$i, $item->phone_number);
			$excel->getActiveSheet()->setCellValue("H".$i, $item->email);
			$excel->getActiveSheet()->setCellValue("I".$i, $item->religion);
			$excel->getActiveSheet()->setCellValue("J".$i, $item->marital_status);
			$excel->getActiveSheet()->setCellValue("K".$i, $item->gender);
			$excel->getActiveSheet()->setCellValue("L".$i, $item->heigh);
			$excel->getActiveSheet()->setCellValue("M".$i, $item->weigh);
			$excel->getActiveSheet()->setCellValue("N".$i, $item->education_level);
			$excel->getActiveSheet()->setCellValue("O".$i, $item->education_majors);

			$i++;
		}
		$excel->getActiveSheet()->setTitle($title);
		$excel->setActiveSheetIndex(0);
		$sheet = $excel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells( true );
		foreach( $cellIterator as $cell ) {
			$sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		}

		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function submit($type = 'all', $branch = 1){


		$vacancy_id		= $this->input->post('vacancy_id');
		$site_id 		= $this->input->post('site_id');
		$company_id 	= $this->input->post('company_id');
		$position_id	= $this->input->post('position_id');
		$placement_date	= $this->input->post('placement_date');

		foreach ($this->input->post('id') as $candidate_id)
		{
			$this->load->model(array('candidate_model', 'approval_model', 'applicant_model'));
			$data = (array) $this->candidate_model->get(array('id' => $candidate_id, 'columns' => 'A.id, A.status_nonjob'));
			if (empty($data)){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('candidate/list/'.$type.'/'.$branch));
			}else{
				 $applicant = $this->applicant_model->get(array('employee_id' => $candidate_id, 'vacancy_id' => $vacancy_id));
				 if(!$application){
				 	$this->applicant_model->save(array('employee_id' => $candidate_id, 'vacancy_id' => $vacancy_id, 'question' => ''));
				 }
				$approval_status  = 1;
				$status_approval  = 'Pengajuan ke Client';
				if($site_id == '2' || $data['status_nonjob'] == 1){
					$approval_status = 2;
					$status_approval = 'Pengajuan ke Personalia';
				}

				$insert = array('id' => $candidate_id, 'status_approval' => $approval_status, 'position_id' => $position_id, 'site_id' => $site_id, 'company_id' => $company_id, 'submitted_at' => date('Y-m-d H:i:s'), 'submitted_by' => $this->session->userdata('user_id'), 'placement_date' => $placement_date);
				$result = $this->candidate_model->save($insert);

				$approval = array('id' => '', 'employee_id' => $data['id'] , 'site_id' => $site_id, 'company_id' => $company_id, 'position_id' => $position_id, 'status_approval' => 'Pengajuan', 'note' => $status_approval);

				$this->approval_model->save($approval);

				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pengajuan gagal.','danger'));
				redirect(base_url('candidate/list/'.$type.'/'.$branch));
				}
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Pegajuan Sukses.','success'));
		redirect(base_url('candidate/list/'.$type.'/'.$branch));
	}

	public function export($type = 'all', $branch = 1)
	{
		$this->load->model(array('candidate_model', 'city_model', 'site_model','employee_education_model', 'employee_emergency_model', 'employee_history_model', 'employee_language_model', 'employee_reference_model', 'employee_skill_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = "Data Calon Karyawan";
		$params = $this->input->get();

		if($type == 'active'){
			$params['status_approval'] = 0;
			$params['status_completeness'] 	= 1;
			$params['active'] 		= TRUE;
			$title = 'pelamar_aktif';
			$files = glob(FCPATH."files/".$title."/*");
		}else if($type == 'nonactive'){
			$params['status_approval'] = 0;
			$params['status_completeness'] 	= 1;
			$params['nonactive'] 		= TRUE;
			$title = 'pelamar_non_aktif';
			$files = glob(FCPATH."files/".$title."/*");
		}else if($type == 'notcomplete'){ 
			$params['status_approval'] = 0;
			$params['status_nonjob'] 		= 0;
			$params['status_completeness'] 	= 0;			
			$title = 'data_pelamar_tidak_lengkap';
			$files = glob(FCPATH."files/".$title."/*");
		}else if($type == 'nonjob'){
			$params['status_nonjob'] 	= 1;
			$title = 'pelamar_non_aktif';
			$files = glob(FCPATH."files/".$title."/*");
		}else{
			$params['status_approval'] 		= 0;
			$title = 'pelamar_diterima';
			$files = glob(FCPATH."files/".$title."/*");
		}

		if($branch > 1){
			$params['session_branch']	= $branch;
		}
		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "Nomor Pendaftaran");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("C".$i, "Nomor Identitas (KTP/SIM)");
		$excel->getActiveSheet()->setCellValue("D".$i, "Masa Berlaku (KTP/SIM)");
		$excel->getActiveSheet()->setCellValue("E".$i, "Tempat Lahir");
		$excel->getActiveSheet()->setCellValue("F".$i, "Tanggal Lahir");
		$excel->getActiveSheet()->setCellValue("G".$i, "Kota Sesuai KTP");
		$excel->getActiveSheet()->setCellValue("H".$i, "Alamat Sesuai KTP");
		$excel->getActiveSheet()->setCellValue("I".$i, "Kota Domisili");
		$excel->getActiveSheet()->setCellValue("J".$i, "Alamat Domisili");
		$excel->getActiveSheet()->setCellValue("K".$i, "Status Rumah");
		$excel->getActiveSheet()->setCellValue("L".$i, "No. Telephon/HP");
		$excel->getActiveSheet()->setCellValue("M".$i, "Email");
		$excel->getActiveSheet()->setCellValue("N".$i, "Facebook");
		$excel->getActiveSheet()->setCellValue("O".$i, "Instagram");
		$excel->getActiveSheet()->setCellValue("P".$i, "Twitter");
		$excel->getActiveSheet()->setCellValue("Q".$i, "Agama");
		$excel->getActiveSheet()->setCellValue("R".$i, "Status Pernikahan");
		$excel->getActiveSheet()->setCellValue("S".$i, "Jenis Kelamin");
		$excel->getActiveSheet()->setCellValue("T".$i, "Tinggi");
		$excel->getActiveSheet()->setCellValue("U".$i, "Berat");
		$excel->getActiveSheet()->setCellValue("V".$i, "Pendidikan Terakhir");
		$excel->getActiveSheet()->setCellValue("W".$i, "Jurusan");
		$excel->getActiveSheet()->setCellValue("X".$i, "Sakit Ringan");
		$excel->getActiveSheet()->setCellValue("Y".$i, "Sakit Serius");
		$excel->getActiveSheet()->setCellValue("Z".$i, "Rawat RS");
		$excel->getActiveSheet()->setCellValue("AA".$i, "Tindak Kriminal");
		$excel->getActiveSheet()->setCellValue("AB".$i, "No Rekening");
		$excel->getActiveSheet()->setCellValue("AC".$i, "Atas Nama");
		$excel->getActiveSheet()->setCellValue("AD".$i, "Bersedia Membuka Rekening");
		$excel->getActiveSheet()->setCellValue("AE".$i, "NPWP");
		$excel->getActiveSheet()->setCellValue("AF".$i, "Memiliki BPJS Kesehatan?");
		$excel->getActiveSheet()->setCellValue("AG".$i, "Keterangan BPJSKS");
		$excel->getActiveSheet()->setCellValue("AH".$i, "Bersedia Mengikuti BPJSKS Perusahaan");
		$excel->getActiveSheet()->setCellValue("AI".$i, "Surat Lamaran Pekerjaan");
		$excel->getActiveSheet()->setCellValue("AJ".$i, "Lampiran Riwayat Hidup");
		$excel->getActiveSheet()->setCellValue("AK".$i, "Foto KTP");
		$excel->getActiveSheet()->setCellValue("AL".$i, "Foto KK");
		$excel->getActiveSheet()->setCellValue("AM".$i, "Foto");
		$excel->getActiveSheet()->setCellValue("AN".$i, "Surat Sehat");
		$excel->getActiveSheet()->setCellValue("AO".$i, "Foto Buku Nikah");
		$excel->getActiveSheet()->setCellValue("AP".$i, "SKCK");
		$excel->getActiveSheet()->setCellValue("AQ".$i, "FOTO NPWP");
		$excel->getActiveSheet()->setCellValue("AR".$i, "Rekomendasi");
		$excel->getActiveSheet()->setCellValue("AS".$i, "Pilihan Kerja");
		$excel->getActiveSheet()->setCellValue("AT".$i, "Keahlian/sertifikat:nomor:url Dokumen,");
		$excel->getActiveSheet()->setCellValue("AU".$i, "Pendidikan:mulai:Selesai,");
		$excel->getActiveSheet()->setCellValue("AV".$i, "Bahasa:Lisan:Tulisan,");
		$excel->getActiveSheet()->setCellValue("AW".$i, "Riwayat Pekerjaan:Posisi:Periode:Gaji:Alamat:Alasan Resign,");
		$excel->getActiveSheet()->setCellValue("AX".$i, "Kontak Referensi:Nama Perusahaan:Posisi:Nomor HP,");
		$excel->getActiveSheet()->setCellValue("AY".$i, "Kontak Darurat:hubungan:Nomor Telepon:Alamat,");

		$params['columns'] 		= "A.*, B.name AS city_domisili, G.name AS city_birth, H.name AS city_card, CONCAT_WS(', ', D.name, E.name, F.name) AS preference_job";

		$list_candidate = $this->candidate_model->gets($params);
		$i=2;
		foreach ($list_candidate as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $item->registration_number);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->id_card_period);
			$excel->getActiveSheet()->setCellValue("E".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_birth));
			$date = new DateTime($item->date_birth);
			$excel->getActiveSheet()->setCellValue("F".$i, PHPExcel_Shared_Date::PHPToExcel($date));
			$excel->getActiveSheet()->getStyle("F".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			$excel->getActiveSheet()->setCellValue("G".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_card));
			$excel->getActiveSheet()->setCellValue("H".$i, $item->address_card);
			$excel->getActiveSheet()->setCellValue("I".$i, str_replace(array('Kota', 'Kabupaten'), '', $item->city_domisili));
			$excel->getActiveSheet()->setCellValue("J".$i, $item->address_domisili);
			$excel->getActiveSheet()->setCellValue("K".$i, $item->status_residance);
			$excel->getActiveSheet()->setCellValueExplicit("L".$i, $item->phone_number);
			$excel->getActiveSheet()->setCellValue("M".$i, $item->email);
			$excel->getActiveSheet()->setCellValue("N".$i, $item->socmed_fb);
			$excel->getActiveSheet()->setCellValue("O".$i, $item->socmed_ig);
			$excel->getActiveSheet()->setCellValue("P".$i, $item->socmed_tw);
			$excel->getActiveSheet()->setCellValue("Q".$i, $item->religion);
			$excel->getActiveSheet()->setCellValue("R".$i, $item->marital_status);
			$excel->getActiveSheet()->setCellValue("S".$i, $item->gender);
			$excel->getActiveSheet()->setCellValue("T".$i, $item->heigh);
			$excel->getActiveSheet()->setCellValue("U".$i, $item->weigh);
			$excel->getActiveSheet()->setCellValue("V".$i, $item->education_level);
			$excel->getActiveSheet()->setCellValue("W".$i, $item->education_majors);
			$excel->getActiveSheet()->setCellValue("X".$i, $item->illness_minor);
			$excel->getActiveSheet()->setCellValue("Y".$i, $item->illness_serious);
			$excel->getActiveSheet()->setCellValue("Z".$i, $item->illness_treated);
			$excel->getActiveSheet()->setCellValue("AA".$i, $item->crime_notes);
			$excel->getActiveSheet()->setCellValue("AB".$i, $item->bank_account);
			$excel->getActiveSheet()->setCellValue("AC".$i, $item->bank_account_name);
			$excel->getActiveSheet()->setCellValue("AD".$i, $item->bank_account_create);
			$excel->getActiveSheet()->setCellValue("AE".$i, $item->tax_number);
			$excel->getActiveSheet()->setCellValue("AF".$i, $item->health_insurance);
			$excel->getActiveSheet()->setCellValue("AG".$i, $item->health_insurance_note);
			$excel->getActiveSheet()->setCellValue("AH".$i, $item->health_insurance_company);
			$excel->getActiveSheet()->setCellValue("AK".$i, $item->document_id_card);
			$excel->getActiveSheet()->setCellValue("AL".$i, $item->document_family_card);
			$excel->getActiveSheet()->setCellValue("AM".$i, $item->document_photo);
			$excel->getActiveSheet()->setCellValue("AN".$i, $item->document_doctor_note);
			$excel->getActiveSheet()->setCellValue("AO".$i, $item->document_marriage_certificate);
			$excel->getActiveSheet()->setCellValue("AP".$i, $item->document_police_certificate);
			$excel->getActiveSheet()->setCellValue("AQ".$i, $item->document_tax_number);
			$excel->getActiveSheet()->setCellValue("AR".$i, $item->recommendation);
			$excel->getActiveSheet()->setCellValue("AS".$i, $item->preference_job);

			$list_skill 	= $this->employee_skill_model->gets(array('employee_id' => $item->id, 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));
			$skill = "";
			foreach ($list_skill as $item_skill) {
				$skill .=" ".$item_skill->certificate_name.":".$item_skill->certificate_number.":".$item_skill->certificate_document.",";
			}
			
			$list_education = $this->employee_education_model->gets(array('employee_id' => $item->id, 'columns' => 'A.id, A.institute, A.start, A.end'));
			$education = "";
			foreach ($list_education as $item_education) {
				$education .=" ".$item_education->institute.":".$item_education->start.":".$item_education->end.",";
			}

			$list_language 	= $this->employee_language_model->gets(array('employee_id' => $item->id, 'columns' => 'A.id, A.language, A.verbal, A.write'));
			$language = "";
			foreach ($list_language as $item_language) {
				$language .=" ".$item_language->language.":".$item_language->verbal.":".$item_language->write.",";
			}

			$list_history 	= $this->employee_history_model->gets(array('employee_id' => $item->id, 'columns' => 'A.id, A.company_name, A.position, A.period, A.salary, A.company_address, A.resign_note'));
			$history = "";
			foreach ($list_history as $item_history) {
				$history .=" ".$item_history->company_name.":".$item_history->position.":".$item_history->period.":".$item_history->salary.":".$item_history->company_address.":".$item_history->resign_note.",";
			}

			$list_reference = $this->employee_reference_model->gets(array('employee_id' => $item->id, 'columns' => 'A.id, A.name, A.company_name, A.position, A.phone'));
			
			$reference  = "";
			foreach ($list_reference as $item_reference) {
				$reference  .=" ".$item_reference->name.":".$item_reference->company_name.":".$item_reference->position.":".$item_reference->phone.",";
			}

			$list_emergency = $this->employee_emergency_model->gets(array('employee_id' => $item->id, 'columns' => 'A.id, A.name, A.relation, A.phone, A.address'));
			
			$emergency = "";
			foreach ($list_emergency as $item_emergency) {
				$emergency .=" ".$item_emergency->name.":".$item_emergency->relation.":".$item_emergency->phone.":".$item_emergency->address.",";
			}

			$excel->getActiveSheet()->setCellValue("AT".$i, $skill);
			$excel->getActiveSheet()->setCellValue("AU".$i, $education);
			$excel->getActiveSheet()->setCellValue("AV".$i, $language);
			$excel->getActiveSheet()->setCellValue("AW".$i, $history);
			$excel->getActiveSheet()->setCellValue("AX".$i, $reference);
			$excel->getActiveSheet()->setCellValue("AX".$i, $emergency);

			$i++;
		}
		$excel->getActiveSheet()->setTitle($title);
		
		$excel->createSheet();
		$excel->setActiveSheetIndex(1);
		$excel->getActiveSheet()->setCellValue('A1', 'ID Kota');
		$excel->getActiveSheet()->setCellValue('B1', 'Nama Kota');

		$list_city	= $this->city_model->gets(array('orderby' => 'A.name', 'order' => 'ASC'));
		$j=2;
		foreach ($list_city as $item) {
			$excel->getActiveSheet()->setCellValue("A".$j,$item->id);
			$excel->getActiveSheet()->setCellValue("B".$j,$item->name);
			$j++;
		}
		$excel->getActiveSheet()->setTitle('Daftar Kota');

		$excel->createSheet();
		$excel->setActiveSheetIndex(2);
		$excel->getActiveSheet()->setCellValue('A1', 'ID Site');
		$excel->getActiveSheet()->setCellValue('B1', 'Nama Site');

		$list_site 	= $this->site_model->gets(array('bank'=>TRUE, 'orderby' => 'A.name', 'order' => 'ASC'));
		$k=2;
		foreach ($list_site as $item) {
			$excel->getActiveSheet()->setCellValue("A".$k,$item->id);
			$excel->getActiveSheet()->setCellValue("B".$k,$item->name);
			$k++;
		}
		$excel->getActiveSheet()->setTitle('Daftar Site');
		$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function delete($candidate_id = false)
	{
		$this->load->model('candidate_model');
		if ($candidate_id)
		{
			$data =  $this->candidate_model->get(array('id' => $candidate_id, 'status' => 0, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $candidate_id, 'is_active' => 0);
				$result = $this->candidate_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('candidate/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('candidate/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('candidate/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('candidate/list'));
		}
	}

	public function curriculum_vitae($candidate_id=FALSE)
	{
		$this->load->model(array('candidate_model','employee_education_model', 'employee_emergency_model', 'employee_history_model', 'employee_language_model', 'employee_reference_model', 'employee_skill_model', 'approval_model'));
		$data['_TITLE_'] 		= 'Preview Pelamar';
		$data['_PAGE_']	 		= 'candidate/preview';
		$data['_MENU_PARENT_'] 	= 'recruitment';
		$data['_MENU_'] 		= 'candidate';

		$data['id'] = $candidate_id;

		
		if (!$candidate_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('candidate/list'));
		}

		$data['list_education'] = $this->employee_education_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.institute, A.start, A.end'));
		$data['list_emergency'] = $this->employee_emergency_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.relation, A.phone, A.address'));
		$data['list_history'] 	= $this->employee_history_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.company_name, A.position, A.period,  A.salary, A.company_address, A.resign_note'));
		$data['list_language'] 	= $this->employee_language_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.language, A.verbal, A.write'));
		$data['list_reference'] = $this->employee_reference_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.company_name, A.position, A.phone'));
		$data['list_skill'] 	= $this->employee_skill_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));

		$data['preview'] = $this->candidate_model->preview(array('id' => $candidate_id));
		if($data['preview']->followup_status == '' 
			&& $data['preview']->followup_note == ''){

			$approval = array('id' => '', 'employee_id' => $candidate_id , 'site_id' => 0, 'company_id' => 0, 'position_id' => 0, 'status_approval' => 'Lihat', 'note' => 'CV Sudah Dilihat');
			$result = $this->approval_model->save($approval);
			$this->candidate_model->save(array('id' => $candidate_id, 'followup_status' => 'Lihat', 'followup_note' => 'CV Sudah Dilihat'));
		}
		$data['preview']->date_city_birth = str_replace(array('Kota', 'Kabupaten'), '', $data['preview']->city_birth).", ".format_date_ina($data['preview']->date_birth);
		$this->load->view('candidate/curriculum_vitae', $data);
	}

	public function preview($candidate_id=FALSE)
	{
		$this->load->model(array('candidate_model','employee_education_model', 'employee_emergency_model', 'employee_history_model', 'employee_language_model', 'employee_reference_model', 'employee_skill_model', 'approval_model'));
		$data['_TITLE_'] 		= 'Preview Pelamar';
		$data['_PAGE_']	 		= 'candidate/preview';
		$data['_MENU_PARENT_'] 	= 'recruitment';
		$data['_MENU_'] 		= 'candidate';

		$data['id'] = $candidate_id;

		if (!$candidate_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('candidate/list'));
		}

		$data['list_education'] = $this->employee_education_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.institute, A.start, A.end'));
		$data['list_emergency'] = $this->employee_emergency_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.relation, A.phone, A.address'));
		$data['list_history'] 	= $this->employee_history_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.company_name, A.position, A.period,  A.salary, A.company_address, A.resign_note'));
		$data['list_language'] 	= $this->employee_language_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.language, A.verbal, A.write'));
		$data['list_reference'] = $this->employee_reference_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.company_name, A.position, A.phone'));
		$data['list_skill'] 	= $this->employee_skill_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));

		$data['preview'] = $this->candidate_model->preview(array('id' => $candidate_id));
		if($data['preview']->followup_status == '' 
			&& $data['preview']->followup_note == ''){

			$approval = array('id' => '', 'employee_id' => $candidate_id , 'site_id' => 0, 'company_id' => 0, 'position_id' => 0, 'status_approval' => 'Lihat', 'note' => 'Detail Pelamar Sudah Dilihat');
			$result = $this->approval_model->save($approval);
			$this->candidate_model->save(array('id' => $candidate_id, 'followup_status' => 'Lihat', 'followup_note' => 'Detail Pelamar Sudah Dilihat'));
		}

		$data['preview']->date_birth = format_date_ina($data['preview']->date_birth);
		$data['preview']->city_birth = str_replace(array('Kota', 'Kabupaten'), '', $data['preview']->city_birth);
		$this->load->view('candidate/preview', $data);
	}

	public function activated(){
		$this->load->model(array('candidate_model'));
		$action = $this->input->post('action');
		foreach ($this->input->post('id') as $candidate_id)
		{
			$this->candidate_model->save(array('id' => $candidate_id));
		}
		
		redirect(base_url('candidate/list'));
	}
	
	public function approved($type = ''){
		$this->load->model(array('site_model'));
		$data['_TITLE_'] 		= 'Diterima';
		$data['_PAGE_'] 		= 'candidate/approved';
		$data['_MENU_PARENT_'] 	= 'recruitment_'.$type;
		$data['_MENU_'] 		= 'approved_'.$type;
		$data['type']			= $type;
		
		$this->view($data);

	}

	public function approved_ajax($type = ''){
		$this->load->model(array('candidate_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, DATE_FORMAT(A.approved_at, '%d/%m/%Y') AS approved_at, A.registration_number, A.full_name, A.id_card, FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25) AS age, A.education_level, A.education_majors, B.name AS city_domisili, C.name AS site_name, E.name AS position"; 
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']		= 1;
		$params['type']			= $type;
		
		$params['approved_at']		= $_POST['columns'][1]['search']['value'];
		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['id_card']			= $_POST['columns'][3]['search']['value'];
		$params['age']				= $_POST['columns'][4]['search']['value'];
		$params['education_level']	= $_POST['columns'][5]['search']['value'];
		$params['education_majors']	= $_POST['columns'][6]['search']['value'];
		$params['city_domisili']	= $_POST['columns'][7]['search']['value'];
		$params['site_name']		= $_POST['columns'][8]['search']['value'];
		$params['position']			= $_POST['columns'][9]['search']['value'];

		if($this->session->userdata('branch') > 1){
			$params['branch_id']	= $this->session->userdata('branch');
		}
		
		$list 	= $this->candidate_model->approved_gets($params);
		$total 	= $this->candidate_model->approved_gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 					= $i;
			$result['approved_at']			= $item->approved_at;
			$result['full_name']			= $item->full_name;
			$result['id_card']				= $item->id_card;
			$result['age']					= $item->age;
			$result['education_level']		= $item->education_level;
			$result['education_majors']		= $item->education_majors;
			$result['city_domisili']		= $item->city_domisili;
			$result['site_name']			= $item->site_name;
			$result['position']				= $item->position;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("candidate/preview/".$item->id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("candidate/curriculum_vitae/".$item->id).'">CV</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("approval/approval_history/".$item->id).'">Riwayat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function rejected($type = ''){
		$this->load->model(array('site_model'));
		$data['_TITLE_'] 		= 'Ditolak/Dibatalkan';
		$data['_PAGE_'] 		= 'candidate/rejected';
		$data['_MENU_PARENT_'] 	= 'recruitment_'.$type;
		$data['_MENU_'] 		= 'rejected_'.$type;
		$data['type']			= $type;
		
		$this->view($data);
	}

	public function rejected_ajax($type = ''){
		$this->load->model(array('approval_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.employee_id, DATE_FORMAT(A.created_at, '%d/%m/%Y') AS created_at, A.note, B.full_name, B.id_card, FLOOR(DATEDIFF(CURRENT_DATE, B.date_birth)/365.25) AS age, B.education_level, B.education_majors, D.name AS site_name, F.name AS position"; 
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']			= 1;
		$params['status_approval']	= 'Ditolak';
		$params['type']			= $type;
		
		$params['created_at']		= $_POST['columns'][1]['search']['value'];
		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['id_card']			= $_POST['columns'][3]['search']['value'];
		$params['age']				= $_POST['columns'][4]['search']['value'];
		$params['education_level']	= $_POST['columns'][5]['search']['value'];
		$params['education_majors']	= $_POST['columns'][6]['search']['value'];
		$params['site_name']		= $_POST['columns'][7]['search']['value'];
		$params['position']			= $_POST['columns'][8]['search']['value'];
		$params['note']				= $_POST['columns'][9]['search']['value'];

		if($this->session->userdata('branch') > 1){
			$params['branch_id']	= $this->session->userdata('branch');
		}

		$list 	= $this->approval_model->gets($params);
		$total 	= $this->approval_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 					= $i;
			$result['created_at']			= $item->created_at;
			$result['full_name']			= $item->full_name;
			$result['id_card']				= $item->id_card;
			$result['age']					= $item->age;
			$result['education_level']		= $item->education_level;
			$result['education_majors']		= $item->education_majors;
			$result['site_name']			= $item->site_name;
			$result['position']				= $item->position;
			$result['note']					= $item->note;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("candidate/preview/".$item->employee_id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("candidate/curriculum_vitae/".$item->employee_id).'">CV</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("approval/approval_history/".$item->employee_id).'">Riwayat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function send($type = 'all', $branch = 1){
		if($this->input->post()){

			$this->load->model(array('wappin_model'));
			$config_id = 1;
			$wappin 		= $this->wappin_model->get(array('id' => $config_id, 'columns' => 'A.access_token, A.base_url'));
			if($wappin){
				$message['token'] 	= "Authorization: Bearer ".$wappin->access_token;
				$message['base_url']= $wappin->base_url;
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Melakukan Koneksi API.','danger'));
				redirect(base_url('candidate/list/'.$type.'/'.$branch));
				die();
			}

			if($_FILES['message-image']['name'] != ""){

				$image_path = FCPATH.'files/whatsapp/';
				$config['upload_path'] 	= $image_path;
				$config['overwrite']  	= TRUE;
				$config['allowed_types']= 'jpg|jpeg|png';
				$config['file_name'] 	= $this->session->userdata('user_id');

				$this->load->library('upload', $config, 'image');
				if (!$this->image->do_upload('message-image')) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> '.$this->image->display_errors().'.','danger'));
					redirect(base_url('candidate/list/'.$type.'/'.$branch));
					die();
				}

				$message['image']		= base_url('files/whatsapp/'.$this->image->data()['file_name']);
				$message['message']		= $this->input->post('message-personal');
			}

			$send_success = 0;
			$send_failed = 0;

			$this->load->model(array('employee_model', 'approval_model', 'candidate_model'));
			$id 		= $this->input->post('id');
			foreach ($id as $employee_id) {
				$employee 	= $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.phone_number, A.full_name, A.followup_status, A.followup_note'));
				if($employee){
					if($employee->phone_number != ''){
						$phone_number = explode ("/", $employee->phone_number);
						$employee->phone_number = preg_replace("/[^0-9]/", "", $phone_number[0]);
						$start = substr($employee->phone_number, 0, 1 );
						if($start == '0'){
							$employee->phone_number = '62'.substr($employee->phone_number, 1,strlen($employee->phone_number));
						}else if($start == '+'){
							$employee->phone_number = substr($employee->phone_number, 1,strlen($employee->phone_number));
						}
						$message['phone_number'] = $employee->phone_number;
						$message['name'] = $employee->full_name;
						$send = $this->send_media($message);
						if($send){
							$send_success = $send_success+1;
							if($employee->followup_status == '' 
								&& $employee->followup_note == ''){

								$approval = array('id' => '', 'employee_id' => $employee_id , 'site_id' => 0, 'company_id' => 0, 'position_id' => 0, 'status_approval' => 'Hubungi', 'note' => 'Dihubungi Melalui Brodcast whatsapp');
								$result = $this->approval_model->save($approval);
								$this->candidate_model->save(array('id' => $employee_id, 'followup_status' => 'Hubungi', 'followup_note' => 'Dihubungi Melalui Brodcast whatsapp'));
								}
						}else{
							$send_failed = $send_failed+1;
						}
					}else{
						$send_failed = $send_failed+1;
					}
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> '.$send_success.' Pesan berhasil dikirim, '.$send_failed.' Pesan Gagal Dikirim','success'));
				redirect(base_url('candidate/list/'.$type.'/'.$branch));
		}else{
			redirect(base_url());
		}
	}

	public function followup($type = 'all', $branch = 1){

		$note	= $this->input->post('note_followup');
		$status	= $this->input->post('status_followup');
		foreach ($this->input->post('id') as $candidate_id)
		{
			$this->load->model(array('candidate_model', 'approval_model'));

			$approval = array('id' => '', 'employee_id' => $candidate_id , 'site_id' => 0, 'company_id' => 0, 'position_id' => 0, 'status_approval' => $status, 'note' => $note);
			$result = $this->approval_model->save($approval);
			$this->candidate_model->save(array('id' => $candidate_id, 'followup_status' => $status, 'followup_note' => $note));
			if (!$result) {
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Update followup gagal.','danger'));
				redirect(base_url('candidate/list/'.$type.'/'.$branch));
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Update followup.','success'));
		redirect(base_url('candidate/list/'.$type.'/'.$branch));
	}

	private function send_media($data = array()){
		$url 		= $data['base_url'].'/v1/messages';
		$request 	= array(
			'to' => $data['phone_number'],
			'type'	=> 'template',
			'template' => array(
				'name'	=> $this->config->item('global_template_name'),
				'namespace' => $this->config->item('global_template_namespace'),
				'language'	=> array(
					'policy' => 'deterministic',
					'code'	=> 'id'
				),
				'components' => array(
					array(
						'type'		=> 'header',
						'parameters'=> array(
							array(
								'type' => 'image',
								'image'	=> array(
									'link' => $data['image']
								)
							)
						)
					),
					array(
						'type' => 'body',
						'parameters'=> array(
							array(
								'type' => 'text',
								'text'	=> $data['name']
							),
							array(
								'type' => 'text',
								'text'	=> $data['message']
							)
						)
					)
				)
			),
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $data['token']));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
		$response = curl_exec($ch);
		$result = json_decode($response, true);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if($httpcode == 200){
			return true;
		}else{
			return false;
		}
	}

	public function pdf($type=FALSE)
	{
		$this->load->model(array('candidate_model'));
		if($type == 'day'){
			$params['last_date'] = date("Y-m-d");
		} else{
			$params['last_date'] = date('Y-m-d', strtotime('-1 month', date("Y-m-d")));
		}

		$params['columns'] 			= "A.full_name, A.id_card, A.phone_number, FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25) AS age , A.gender, A.education_level, A.experience, C.name AS preference_job1, D.name AS branch_name";
		$params['status_approval'] 	= 1;
		$list_candidate 				= $this->candidate_model->gets_register($params);

		$html = '<table width="100%"><tr><td><strong>Total Pelamar : '.count($list_candidate).'</strong></td></tr></table>';
		$html .= '<div syle="width=100%"></hr></div>';
		$html .= '<table width="100%">';
		$html .= '<tr>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">Nama Lengkap</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">NIK KTP</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">Posisi dilamar</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">Pengalaman</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">Usia</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">Jenis Kelamin</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">Pendidikan</td>
		<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">Area</td></tr>'; 
		foreach ($list_candidate as $item) {
			$html .= '<tr>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">'.$item->full_name.'</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">'.$item->id_card.'</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">'.$item->preference_job1.'</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">'.$item->experience.'</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">'.$item->age.'</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">'.$item->gender.'</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">'.$item->education_level.'</td>
			<td style="border: 1px solid black;border-collapse: collapse; padding-right:5px;">'.$item->branch_name.'</td></tr>';
		}
		$html .='</table>';
		$html .= '<div syle="width=100%"></hr></div><br pagebreak="true"/>';
		$data['content'] 	= $html; 
		$data['title'] 		= 'daftar_pelamar';
		$data['basepath'] 	= FCPATH.'files/candidate/';
        $this->generate_PDF($data);
       	redirect(base_url('files/candidate/'.strtolower(str_replace(" ", "_",$data['title'])).'.pdf'));
	}

	private function generate_PDF($data = array())
	{	
		$this->load->library("Pdf");
		ob_start();
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// $pdf->SetProtection(array('print', 'copy'), $data['email']->pdf_password, null, 0, null);
		// $pdf->SetHeaderData($data['image'], '181', '', null);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shelter');
		$pdf->SetTitle($data['title']);
		$pdf->SetSubject($data['title']);
		$pdf->SetKeywords('Shelter');

		// $pdf->setPrintHeader(true);
		// $pdf->setPrintFooter(false);

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$pdf->SetFont('times', '', 10);
		$pdf->AddPage();
		$pdf->writeHTML($data['content'], true, false, true, false, '');

		$base_path  = $data['basepath'];
		if(!is_dir($base_path)){
			mkdir($base_path, 0777, true);
		}
		return $pdf->Output($base_path.strtolower(str_replace(" ", "_",$data['title'])).'.pdf', 'F');
	}
}
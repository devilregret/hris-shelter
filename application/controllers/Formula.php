<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Formula extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function list(){
		$data['_TITLE_'] 		= 'Formula Gaji Karyawan';
		$data['_PAGE_'] 		= 'formula/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'formula';
		$this->view($data);
	}

	public function list_ajax(){

		$this->load->model(array('site_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "B.code AS company_code, A.id, A.name AS site_name, A.address AS site_address";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['company_code']	= $_POST['columns'][1]['search']['value'];
		$params['site_name']	= $_POST['columns'][2]['search']['value'];
		$params['site_address']	= $_POST['columns'][3]['search']['value'];
		
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		$list 	= $this->site_model->gets_ro_site($params);
		$total 	= $this->site_model->gets_ro_site($params, TRUE);
		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['company_code']	= $item->company_code;
			$result['site_name']	= $item->site_name;
			$result['site_address']	= $item->site_address;
			$result['action'] 		=
				'
				<a class="btn-sm btn-success btn-block" href="'.base_url("formula/form/".$item->id).'">Ubah</a>
				<a class="btn-sm btn-primary btn-block" href="'.base_url("formula/site/".$item->id).'">Detail</a>';
			array_push($data, $result);
			$i++;
		}
				
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);
		echo json_encode($response);
	}

	public function form($site_id = FALSE)
	{
		$this->load->model(array('formula_site_model', 'site_model'));
		$formula_path = FCPATH.'files/formula/';

		if($this->input->post()){
			$site_id 			= $this->input->post('site_id');
			
			if($_FILES['formula']['name'] != ""){

				$formula_path = FCPATH.'files/formula/';
				$config['upload_path'] 	= $formula_path;
				$config['overwrite']  	= TRUE;
				$config['allowed_types']= 'xlsx|xls';
				$config['file_name'] 	= $site_id;

				$this->load->library('upload', $config, 'formula');
				if (!$this->formula->do_upload('formula')) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> '.$this->image->display_errors().'.','danger'));
					redirect(base_url('formula/list'));
					die();
				}
			}

			$this->formula_site_model->delete(array('site_id' => $site_id));
			$list_no		= $this->input->post('no');
			$list_kolom		= $this->input->post('kolom');
			$list_nama		= $this->input->post('nama');
			$list_tipe		= $this->input->post('tipe');
			$list_kategori	= $this->input->post('kategori');
			$list_nilai		= $this->input->post('nilai');
			foreach ($list_no AS $key => $value ) {
				if($list_nama[$key] == ''){
					continue;
				}
				$this->formula_site_model->save(
						array(
							'site_id' 	=> $site_id,
							'no' 		=> $list_no[$key],
							'kolom' 	=> strtoupper($list_kolom[$key]),
							'nama' 		=> strtoupper($list_nama[$key]),
							'tipe' 		=> $list_tipe[$key],
							'kategori' 	=> $list_kategori[$key],
							'nilai' 	=> str_replace(".", "", $list_nilai[$key])
						));

			}
			$this->session->set_flashdata('message', message_box('<strong>Berhasil!</strong> Formula Berhasil disimpan.','success'));
			redirect(base_url('formula/form/'.$site_id));
		}

		$data['site']			= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.id, A.name, A.address'));
		if (!$data['site']){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Site bisnis tidak ditemukan.','danger'));
			redirect(base_url('formula/list'));
		}

		$data['formula'] 		= $this->formula_site_model->gets(array(
				'site_id' => $site_id,
				'orderby' => 'A.no', 
				'order' => 'ASC'
			));
		$data['url_formula']	= '';

		if(file_exists($formula_path.$site_id.'.xlsx')){
			$data['url_formula']	= base_url('files/formula/'.$site_id.'.xlsx');
		}
		$data['_TITLE_'] 		= 'Form Formula Gaji';
		$data['_PAGE_'] 		= 'formula/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'formula';
		return $this->view($data);
	}

	public function site($site_id = FALSE){
		
		$this->load->model(array('site_model'));
		
		$data['_TITLE_'] 		= 'Formula Gaji Karyawan';
		$data['_PAGE_'] 		= 'formula/site';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'formula';
		if(!$site_id){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('formula/list'));
		}
		if($this->session->userdata('role') == 17){

		}else if(!in_array($this->session->userdata('role'), $this->config->item('administrator')) && $site_id <= 2){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('formula/list'));
		}
		$data['site']			= $this->site_model->get(array('id' => $site_id, 'columns' => 'A.id, A.name, A.address'));
		$this->view($data);
	}

	public function site_ajax($site_id = FALSE){

		$this->load->model(array('formula_employee_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id AS employee_id, A.full_name, A.employee_number, A.id_card, B.name AS position";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']		=  $_POST['start'];
		
		
		$params['id_card']			= $_POST['columns'][1]['search']['value'];
		$params['employee_number']	= $_POST['columns'][2]['search']['value'];
		$params['full_name']		= $_POST['columns'][3]['search']['value'];
		$params['position']			= $_POST['columns'][4]['search']['value'];
		
		$params['site_id']		=  $site_id;

		$list 	= $this->formula_employee_model->gets_employee($params);
		$total 	= $this->formula_employee_model->gets_employee($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{			
			$result['no'] 				= $i;
			$result['id_card']			= $item->id_card;
			$result['employee_number']	= $item->employee_number;
			$result['full_name']		= $item->full_name;
			$result['position']			= $item->position;
			$basic_salary 				= $this->formula_employee_model->get(array('employee_id' => $item->employee_id, 'no' => '17', 'columns' => 'A.nilai'));
			
			$result['basic_salary']		= '';
			if($basic_salary){
				$result['basic_salary']		= format_rupiah($basic_salary->nilai);	
			}
			$result['action'] 			= '<a class="btn-sm btn-success btn-block" href="'.base_url("formula/form_employee/".$site_id."/".$item->employee_id).'">Ubah</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function form_employee($site_id = FALSE, $employee_id = FALSE)
	{
		$this->load->model(array('formula_employee_model', 'employee_model'));

		if($this->input->post()){
			$employee_id 			= $this->input->post('employee_id');

			$this->formula_employee_model->delete(array('employee_id' => $employee_id));
			$list_no		= $this->input->post('no');
			$list_kolom		= $this->input->post('kolom');
			$list_nama		= $this->input->post('nama');
			$list_tipe		= $this->input->post('tipe');
			$list_kategori	= $this->input->post('kategori');
			$list_nilai		= $this->input->post('nilai');
			foreach ($list_no AS $key => $value ) {
				if($list_nama[$key] == ''){
					continue;
				}
				$this->formula_employee_model->save(
						array(
							'employee_id' 	=> $employee_id,
							'no' 		=> $list_no[$key],
							'kolom' 	=> strtoupper($list_kolom[$key]),
							'nama' 		=> strtoupper($list_nama[$key]),
							'tipe' 		=> $list_tipe[$key],
							'kategori' 	=> $list_kategori[$key],
							'nilai' 	=> str_replace(".", "", $list_nilai[$key])
						));

			}
			$this->session->set_flashdata('message', message_box('<strong>Berhasil!</strong> Formula Berhasil disimpan.','success'));
			redirect(base_url('formula/form_employee/'.$site_id.'/'.$employee_id));
		}

		$data['employee']			= $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.id, A.full_name, A.id_card, A.address_domisili'));
		if (!$data['employee']){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Site bisnis tidak ditemukan.','danger'));
			redirect(base_url('formula/site/'.$site_id));
		}

		$data['formula'] 		= $this->formula_employee_model->gets(array(
				'employee_id' => $employee_id,
				'orderby' => 'A.no', 
				'order' => 'ASC'
			));

		$data['site_id'] 		= $site_id;
		$data['_TITLE_'] 		= 'Form Formula Gaji';
		$data['_PAGE_'] 		= 'formula/form_employee';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'formula';
		return $this->view($data);
	}
}
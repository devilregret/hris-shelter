<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Attendance extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('payroll'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function site(){
		if($this->input->post('site_id')){
			$this->session->set_userdata('site',$this->input->post('site_id'));
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Site sudah dipilih.','success'));
		redirect(base_url('attendance/list'));
	}

	public function list($view = FALSE){
		
		$this->load->model(array('cutoff_model', 'site_model', 'employee_model'));
		$data['_TITLE_'] 		= 'Jadwal Kerja';
		$data['_PAGE_'] 		= 'attendance/list';
		$data['_MENU_PARENT_'] 	= 'payroll_employee';
		$data['_MENU_'] 		= 'attendance';
		
		if(!$view){
			$view = date('m');
		}

		$data['site_id']		= $this->session->userdata('site');
		$data['view']			= $view;
		$data['site']			= $this->site_model->get_site(array('id' => $data['site_id'], 'columns' => 'A.id, A.name, A.address, A.attendance_machine'));
		$data['employee']		= $this->employee_model->gets(array('status' => 1, 'status_approval' => 3, 'site_id' => $data['site_id']), TRUE);
		$data['cutoff']			= $this->cutoff_model->get(array('site_id' => $data['site_id'], 'columns' => 'A.start_date, A.end_date, B.name AS formula'));
		
		$branch_id				= '';
		if($this->session->userdata('branch') > 1){
			$branch_id 	= $this->session->userdata('branch');
		}

		if(in_array($this->session->userdata('role'), $this->config->item('personalia'))){
			$data['list_site']		= $this->site_model->gets_ro_site(array('columns' => 'A.id, A.name, B.code AS company_code', 'indirect' => true, 'branch_id' => $branch_id)); 
		}else{
			$data['list_site']		= $this->site_model->gets_ro_site(array('columns' => 'A.id, A.name, B.code AS company_code', 'branch_id' => $branch_id));
		}
		$this->view($data); 
	}

	public function list_ajax($view = FALSE){
		
		$this->load->model(array('attendance_model', 'cutoff_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.employee_id, B.full_name, B.employee_number, B.id_card, DATE_FORMAT(A.date, '%d/%m/%Y') AS date, A.date AS raw_date, A.schedule_start, A.schedule_end, A.attendance_start, A.attendance_end, A.type, A.description, C.name AS position";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['employee_number']	= $_POST['columns'][1]['search']['value'];
		$params['id_card']			= $_POST['columns'][2]['search']['value'];
		$params['full_name']		= $_POST['columns'][3]['search']['value'];
		$params['position']			= $_POST['columns'][4]['search']['value'];
		$params['type']				= $_POST['columns'][5]['search']['value'];
		$params['description']		= $_POST['columns'][6]['search']['value'];
		$params['date']				= $_POST['columns'][7]['search']['value'];
		
		$params['site_id']		= $this->session->userdata('site');
		$cutoff					= $this->cutoff_model->get(array('site_id' => $params['site_id'], 'columns' => 'A.start_date, A.end_date'));
		if(!$view){
			$view = date('m');
		}
		if($cutoff){
			$params['date_start'] 		= date('Y-'.$view.'-'.$cutoff->start_date);
			$params['date_end'] 		= date('Y-'.$view.'-'.$cutoff->end_date);
			if($params['date_start'] > $params['date_end']){
				$params['date_start'] 	= date('Y-m-d',strtotime($params['date_start']. "-1 months"));
			}
		}

		$list 	= $this->attendance_model->gets($params);
		$total 	= $this->attendance_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{

			$result['id']				= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->employee_id.','.$item->raw_date.'">';
			$result['no'] 				= $i;
			$result['employee_number']	= $item->employee_number;
			$result['id_card']			= $item->id_card;
			$result['full_name']		= $item->full_name;
			$result['position']			= $item->position;
			$result['type']				= $item->type;
			$result['description']		= $item->description;
			$result['date']				= $item->date;
			$result['day']				= get_day(date('l', strtotime($item->raw_date)));
			$result['schedule_start']	= $item->schedule_start;
			$result['schedule_end']		= $item->schedule_end;
			$result['attendance_start']	= $item->attendance_start;
			$result['attendance_end']	= $item->attendance_end;
			$result['action'] 			=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("attendance/preview/".$item->employee_id).'">Lihat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function generate(){
		if($this->input->post()){
			$this->load->model(array('workday_model', 'cutoff_model', 'employeeshift_model', 'attendance_model'));
			$periode 	= $this->input->post('periode');
			$site_id	= $this->session->userdata('site');
			$cutoff		= $this->cutoff_model->get(array('site_id' => $site_id, 'columns' => 'A.start_date, A.end_date'));
			if($cutoff){
				$office_hours_shift 			= array('Shift 1' => array(), 'Shift 2' => array(), 'Shift 3' => array());
				foreach($office_hours_shift as $key => $value)
				{
					$office_hours 	= array();
					$workday 		= $this->workday_model->gets(array('site_id' => $site_id, 'shift' => $key, 'columns' => 'A.day, A.hours_start, A.hours_end'));
					foreach($workday as $item)
					{
						$office_hours[$item->day] = array('start' => $item->hours_start, 'end' => $item->hours_end);
					}
					$office_hours_shift[$key] 	= $office_hours; 
				}


				$schedule_shift = array();
				$start 			= date('Y-'.$periode.'-'.$cutoff->start_date);
				$end 			= date('Y-'.$periode.'-'.$cutoff->end_date);
				if($start > $end){
					$start 	= date('Y-m-d', strtotime($start."-1 months"));
				}

				$schedule_shift1 = array();
				$schedule_shift2 = array();
				$schedule_shift3 = array();
				while(true) {
					$day  	= get_day(date('l', strtotime($start)));
					if(!empty($office_hours_shift['Shift 1'])){
						$office_hours = $office_hours_shift['Shift 1'];
						if(isset($office_hours[$day])){
							$schedule_shift1[$start] = $office_hours[$day];
						}else{
							$schedule_shift1[$start] = array('start' => '00:00', 'end' => '00:00');		
						}
					}
					if(!empty($office_hours_shift['Shift 2'])){
						$office_hours = $office_hours_shift['Shift 2'];
						if(isset($office_hours[$day])){
							$schedule_shift2[$start] = $office_hours[$day];
						}else{
							$schedule_shift2[$start] = array('start' => '00:00', 'end' => '00:00');		
						}
					}
					if(!empty($office_hours_shift['Shift 3'])){
						$office_hours = $office_hours_shift['Shift 3'];
						if(isset($office_hours[$day])){
							$schedule_shift3[$start] = $office_hours[$day];
						}else{
							$schedule_shift3[$start] = array('start' => '00:00', 'end' => '00:00');		
						}
					}
					if($start >= $end){
						break;
					}
					$start 	= date('Y-m-d', strtotime($start. "+1 days"));
				}

				$schedule_shift = array('Shift 1' => $schedule_shift1, 'Shift 2' => $schedule_shift2, 'Shift 3' => $schedule_shift3);

				$list_employee = $this->employeeshift_model->gets(array('site_id' => $site_id, 'columns' => 'A.id, B.shift, A.full_name'));
				foreach($list_employee as $item)
				{
					if($item->shift == ''){
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Shift '.$item->full_name.' belum ada, silahkan diatur <a href="'.base_url('employeeshift/site/'.$site_id).'">disini</>.','danger'));
						redirect(base_url('attendance/list/'.$periode));
						die();
					}
					$schedule = $schedule_shift[$item->shift];
					if(!$schedule){
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> '.$item->shift.' belum ada, silahkan diatur <a href="'.base_url('workday/site/'.$site_id).'">disini</>.','danger'));
						redirect(base_url('attendance/list/'.$periode));
						die();
					}
					$data['employee_id'] = $item->id;
					foreach($schedule AS $key => $value){
						$data['date'] 				= $key;
						$data['schedule_start'] 	= $value['start'];
						$data['schedule_end'] 		= $value['end'];
						$data['attendance_start'] 	= '00:00';
						$data['attendance_end'] 	= '00:00';
						$data['is_active'] 			= 1;
						$attendance 				= $this->attendance_model->get(array('employee_id' => $data['employee_id'], 'date' => $data['date']));
						if($attendance){
							$data 	= (array) $attendance;
							$data['schedule_start'] 	= $value['start'];
							$data['schedule_end'] 		= $value['end'];
						}
						$this->attendance_model->save($data);
					}
				}
				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Jadwal Periode '.get_month($periode).' '.date('Y').' berhasil di proses.','success'));
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Cut off belum diatur, silahkan diatur <a href="'.base_url('cutoff/list').'">disini</>.','danger'));
			}
			redirect(base_url('attendance/list/'.$periode));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal diproses.','danger'));
			redirect(base_url('attendance/list'));
		}
	}

	public function syncronize(){
		$periode = "";
		if($this->input->post()){
			$this->load->model(array('site_model', 'cutoff_model', 'employee_model', 'attendance_model'));
			$periode 			= $this->input->post('periode');
			$attendance_machine = $this->input->post('attendance_machine');
			$site_id			= $this->session->userdata('site');
			$this->site_model->save(array('id' => $site_id, 'attendance_machine' => $attendance_machine));
			$cutoff		= $this->cutoff_model->get(array('site_id' => $site_id, 'columns' => 'A.start_date, A.end_date'));
			if($cutoff){
				$start 			= date('Y-'.$periode.'-'.$cutoff->start_date);
				$end 			= date('Y-'.$periode.'-'.$cutoff->end_date);
				if($start > $end){
					$start 	= date('Y-m-d', strtotime($start."-1 months"));
				}

				$params['api_token'] 	= 'aVwAQh5GSsaO8EyPYSnTkfxOzNCSMbhs1Sk3IQpyC94PB4IjgFmH1pgYxsa9';
				$params['id_mesin'] 	= $attendance_machine;
				$params['tgl_mulai'] 	= $start;
				$params['tgl_akhir'] 	= $end;

				$curl = curl_init();
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
				
				curl_setopt_array($curl, array(
				  CURLOPT_URL => 'https://presensi.shelterapp.co.id/api/getRekapPresensi',
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => '',
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => 'POST',
				  CURLOPT_POSTFIELDS => json_encode($params),
				  CURLOPT_HTTPHEADER => array(
				    'Content-Type: application/json'
				  ),
				));
				$response = curl_exec($curl);
				curl_close($curl);
				$response = json_decode($response);
				if(isset($response->data)){
					foreach($response->data as $item)
					{
						$employee = $this->employee_model->get(array('pin_finger' => $item->id_mesin, 'columns' => 'A.id'));
						if($employee){
							$date							= substr($item->absen_in, 0,10);
							$attendance 					= (array) $this->attendance_model->get(array('employee_id' => $employee->id, 'date' => $date));
							$attendance['employee_id']		= $employee->id;
							$attendance['date']				= substr($item->absen_in, 0,10);
							$attendance['attendance_start']	= substr($item->absen_in, 11,8);
							$attendance['attendance_end']	= substr($item->absen_out, 11,8);
							$attendance['is_active']		= 1;
							$save_id = $this->attendance_model->save($attendance);
						}
					}
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disinkronisasi.','success'));
				}else{
					if(isset($response->message)){
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> '.$response->message.'.','danger'));
					}else{
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Sinkronisasi absensi gagal dilakukan.','danger'));
					}
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Cut off belum diatur, silahkan diatur <a href="'.base_url('cutoff/list').'">disini</>.','danger'));
			}
		}
		redirect(base_url('attendance/list/'.$periode));
	}

	public function process($view = FALSE){
		
		if($this->input->post()){
			$warning = '';

			$this->load->model(array('vacation_model', 'attendance_model'));

			$data['type']				= $this->input->post('type');
			$data['schedule_start']		= $this->input->post('hours_start');
			$data['schedule_end']		= $this->input->post('hours_end');
			$data['attendance_start']	= $this->input->post('hours_come');
			$data['attendance_end']		= $this->input->post('hours_return');
			$data['description']		= $this->input->post('description');
			$data['is_active']			= 1;
			// $data['attendance_start']	= '00:00';
			// $data['attendance_end']		= '00:00';
			foreach($this->input->post('id') AS $item){
				$value = explode (",", $item); 
				$data['employee_id']= $value[0];
				$data['date'] 		= $value[1];

				if($data['type'] == 'Izin Cuti'){
					$vacation = $this->vacation_model->get(array('employee_id' => $data['employee_id'], 'columns' => 'SUM(B.vacation) AS vacation, A.full_name'));
					if($vacation){
						if($vacation->vacation < 1){
							$warning .= $vacation->full_name;
							continue;
						}
						$this->vacation_model->save(array('employee_id' => $data['employee_id'], 'vacation' => $vacation->vacation-1));
					}else{
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Cuti belum diatur, silahkan diatur <a href="'.base_url('vacation/site').'">disini</>.','danger'));
					}

					if($warning == ''){
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
					}else{
						$this->session->set_flashdata('message', message_box('<strong>Peringatan!</strong> '.$warning.', sudah tidak memiliki jatah cuti.','warning'));
					}
				}

				$attendance = (array) $this->attendance_model->get(array('employee_id' => $data['employee_id'], 'date' => $data['date']));
				if($data['schedule_start'] != ""){
					$attendance['schedule_start'] = $data['schedule_start'];
				}
				if($data['schedule_end'] != ""){
					$attendance['schedule_end'] = $data['schedule_end'];
				}
				if($data['attendance_start'] != ""){
					$attendance['attendance_start'] = $data['attendance_start'];
				}	
				if($data['attendance_end'] != ""){
					$attendance['attendance_end'] = $data['attendance_end'];
				}
				if($data['description'] != ""){
					$attendance['description'] = $data['description'];
				}
				if($data['type'] != ""){
					$attendance['type'] = $data['type'];
				}
				$this->attendance_model->save($attendance);
			}
		}
		redirect(base_url('attendance/list/'.$view));
	}

	public function preview($employee_id = FALSE)
	{
		$this->load->model(array('vacation_model'));
		$data['_TITLE_'] 		= 'Preview Cuti Karyawan';
		$data['_PAGE_']	 		= 'attendance/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'workday';
		
		if (!$employee_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('attendance/list'));
		}
		$data['preview'] = $this->vacation_model->get(array('employee_id' => $employee_id, 'columns' => 'B.vacation, A.full_name, A.id_card, A.employee_number'));
		$this->load->view('attendance/preview', $data);
	}

	public function export($view = FALSE)
	{
		$this->load->model(array('attendance_model', 'cutoff_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$params 				= $this->input->get();
		$params['columns'] 		= "A.employee_id, B.full_name, B.employee_number, B.id_card, DATE_FORMAT(A.date, '%d/%m/%Y') AS date, A.date AS raw_date, A.schedule_start, A.schedule_end, A.attendance_start, A.attendance_end, A.type, A.description, C.name AS position";
		$params['orderby'] 		= 'B.full_name, A.date';
		$params['order']		= 'ASC';		
		$params['site_id']		= $this->session->userdata('site');
		
		$cutoff					= $this->cutoff_model->get(array('site_id' => $params['site_id'], 'columns' => 'A.start_date, A.end_date'));
		if(!$view){
			$view = date('m');
		}
		if($cutoff){
			$params['date_start'] 		= date('Y-'.$view.'-'.$cutoff->start_date);
			$params['date_end'] 		= date('Y-'.$view.'-'.$cutoff->end_date);
			if($params['date_start'] > $params['date_end']){
				$params['date_start'] 	= date('Y-m-d',strtotime($params['date_start']. "-1 months"));
			}
		}

		$title = 'jadwal_kerja';
		$files = glob(FCPATH."files/".$title."/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
		->setLastModifiedBy($this->session->userdata('name'))
		->setTitle($title)
		->setSubject($title)
		->setDescription($title)
		->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);
		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "NO");
		$excel->getActiveSheet()->setCellValue("B".$i, "NIP");
		$excel->getActiveSheet()->setCellValue("C".$i, "NIK");
		$excel->getActiveSheet()->setCellValue("D".$i, "Nama Karyawan");
		$excel->getActiveSheet()->setCellValue("E".$i, "Jabatan");
		$excel->getActiveSheet()->setCellValue("F".$i, "Jadwal Masuk");
		$excel->getActiveSheet()->setCellValue("G".$i, "Jadwal Pulang");
		$excel->getActiveSheet()->setCellValue("H".$i, "Tanggal");
		$excel->getActiveSheet()->setCellValue("I".$i, "Karyawan Masuk");
		$excel->getActiveSheet()->setCellValue("J".$i, "Karyawan Pulang");
		$excel->getActiveSheet()->setCellValue("K".$i, "Izin");
		$excel->getActiveSheet()->setCellValue("L".$i, "Keterangan");

		$list 	= $this->attendance_model->gets($params);
		$i++;
		$no = 1;
		foreach($list as $item)
		{
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $no);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->employee_number);
			$excel->getActiveSheet()->setCellValueExplicit("C".$i, $item->id_card);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValue("E".$i, $item->position);
			$excel->getActiveSheet()->setCellValueExplicit("F".$i, $item->schedule_start);
			$excel->getActiveSheet()->setCellValueExplicit("G".$i, $item->schedule_end);
			
			$date = new DateTime($item->raw_date);
			$excel->getActiveSheet()->setCellValue("H".$i, PHPExcel_Shared_Date::PHPToExcel($date));
			$excel->getActiveSheet()->getStyle("H".$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);

			$excel->getActiveSheet()->setCellValueExplicit("I".$i, $item->attendance_start);
			$excel->getActiveSheet()->setCellValueExplicit("J".$i, $item->attendance_end);
			$excel->getActiveSheet()->setCellValueExplicit("K".$i, $item->type);
			$excel->getActiveSheet()->setCellValueExplicit("L".$i, $item->description);

			$no++;
			$i++;
		}
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}
	
	public function import($view = FALSE){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('employee_model', 'attendance_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {
				$data['schedule_start']		= '00:00';
				$data['schedule_end']		= '00:00';
				$data['is_active']			= 1;
				$data['attendance_start']	= '00:00';
				$data['attendance_end']		= '00:00';
				$data['employee_id']		= '';
				$data['date'] 				= '';
				
				$id_card				= replace_null($sheet->getCell('C'.$row)->getValue());
				if($id_card == ''){
					continue;
				}

				$employee = $this->employee_model->get(array('id_card' => $id_card, 'columns' => 'A.id'));
				if(!$employee){
					continue;
				}
				$data['employee_id']	= $employee->id;
				$excel_date 			= $sheet->getCell('H'.$row)->getValue();
				$unix_date 				= ($excel_date - 25569) * 86400;
				$excel_date 			= 25569 + ($unix_date / 86400);
				$unix_date 				= ($excel_date - 25569) * 86400;
				$data['date']			= gmdate("Y-m-d", $unix_date);

				$schedule_start 		= replace_null($sheet->getCell('F'.$row)->getValue());
				if($schedule_start != ''){
					$data['schedule_start']	= $schedule_start;
				}
				$schedule_end 			= replace_null($sheet->getCell('G'.$row)->getValue());
				if($schedule_end != ''){
					$data['schedule_end']	= $schedule_end;
				}
				$attendance_start 		= replace_null($sheet->getCell('I'.$row)->getValue());
				// print_r($attendance_start);
				if($attendance_start != ''){
					$data['attendance_start']	= $attendance_start;
				}
				$attendance_end 		= replace_null($sheet->getCell('J'.$row)->getValue());
				if($attendance_end != ''){
					$data['attendance_end']	= $attendance_end;
				}

				$data['type'] 			= replace_null($sheet->getCell('K'.$row)->getValue());
				$data['description'] 	= replace_null($sheet->getCell('L'.$row)->getValue());
				$save_id = $this->attendance_model->save($data);
			}

			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
		}
		redirect(base_url('attendance/list/'.$view));
	}
}
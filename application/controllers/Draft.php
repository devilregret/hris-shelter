<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Draft extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function sni()
	{
		$this->load->view('0draft/sn_pernyataan');
	}

	public function sn()
	{
		$this->load->view('0draft/sn_perjanjian_bersama');
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rocutoff extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!in_array($this->session->userdata('role'), array(2,3,8))){
			redirect(base_url());
		}
		if($this->session->userdata('site') == ''){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan pilih site bisnis.','danger'));
			redirect(base_url());	
		}
	}

	public function list(){
		
		$this->load->model(array('site_model'));

		$data['_TITLE_'] 		= 'Cut Off';
		$data['_PAGE_'] 		= 'rocutoff/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'rocutoff';
		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address'));
		$this->view($data);
	}

	public function list_ajax(){

		$this->load->model(array('rocutoff_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.start_date, A.end_date, A.description";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['start_date']	= $_POST['columns'][1]['search']['value'];
		$params['end_date']		= $_POST['columns'][2]['search']['value'];
		$params['description']	= $_POST['columns'][3]['search']['value'];
		$params['site_id']		= $this->session->userdata('site');
		$list 	= $this->rocutoff_model->gets($params);
		$total 	= $this->rocutoff_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{			
			$result['no'] 			= $i;
			$result['start_date']	= $item->start_date;
			$result['end_date']		= $item->end_date;
			$result['description']	= $item->description;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action"  style="cursor:pointer;" data-href="'. base_url("rocutoff/preview/".$item->id).'"><i class="fas fa-eye text-white"></i></a>
				<a class="btn-sm btn-success" href="'.base_url("rocutoff/form/".$item->id).'"><i class="fas fa-edit"></i></a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger" style="cursor:pointer;" data-href="'.base_url("rocutoff/delete/".$item->id).'"><i class="far fa-trash-alt text-white"></i></a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function form($cutoff_id = FALSE)
	{
		$this->load->model(array('rocutoff_model'));

		$data['id'] 			= '';
		$data['start_date']		= '';
		$data['end_date']		= '';
		$data['description']	= '';

		if($this->input->post()){
			$data['id'] 			= $this->input->post('id');
			$data['start_date'] 	= $this->input->post('start_date');
			$data['end_date']		= $this->input->post('end_date');
			$data['description']	= $this->input->post('description');
			
			$this->form_validation->set_rules('start_date', '', 'required');
			$this->form_validation->set_rules('end_date', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$insert = array('id' => $data['id'], 'start_date' => $data['start_date'], 'end_date' => $data['end_date'], 'description' => $data['description'], 'site_id' => $this->session->userdata('site'));
				$save_id	 	= $this->rocutoff_model->save($insert);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('rocutoff/list'));
		}

		if ($cutoff_id)
		{
			$data = (array) $this->rocutoff_model->get(array('id' => $cutoff_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('rocutoff/list'));
			}
		}

		$data['_TITLE_'] 		= 'Cut Off';
		$data['_PAGE_'] 		= 'rocutoff/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'rocutoff';
		return $this->view($data);
	}

	public function preview($cutoff_id=FALSE)
	{
		$this->load->model(array('rocutoff_model'));
		$data['_TITLE_'] 		= 'Preview Cut Off';
		$data['_PAGE_']	 		= 'workday/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'workday';
		
		if (!$cutoff_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('rocutoff/list'));
		}

		$data['preview'] = $this->rocutoff_model->preview(array('id' => $cutoff_id, 'columns' => 'A.*'));
		$this->load->view('rocutoff/preview', $data);
	}

	public function delete($cutoff_id = false)
	{
		$this->load->model('rocutoff_model');
		if ($cutoff_id)
		{
			$data =  $this->rocutoff_model->get(array('id' => $cutoff_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $cutoff_id, 'is_active' => 0);
				$result = $this->rocutoff_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('rocutoff/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('rocutoff/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('rocutoff/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('rocutoff/list'));
		}
	}
}
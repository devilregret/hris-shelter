<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
	}

	public function approval_history($employee_id = ""){
		$this->load->model(array('site_model', 'candidate_model'));
		$data['_TITLE_'] 		= 'Preview History Pengajuan';
		$data['_PAGE_'] 		= 'candidate/approval_history';
		$data['_MENU_PARENT_'] 	= 'recruitment';
		$data['_MENU_'] 		= 'candidate';
		$data['employee_id']	= $employee_id;
		$data['employee'] 		= $this->candidate_model->get(array('id' => $employee_id, 'columns' => 'A.id, A.full_name, A.id_card'));
		$this->load->view('approval/approval_history', $data);

	}

	public function approval_history_ajax($employee_id = ""){

		$this->load->model(array('approval_model'));

		$params['columns'] 		= "A.created_at AS date_approval, A.status_approval, A.note, C.name AS company_name, D.name AS site_name, E.full_name AS submit_by";
		$params['status']		= 0;
		$params['employee_id']	= $employee_id;

		$i 		= 1;
		$data 	= array();
		$list 	= $this->approval_model->gets($params);

		foreach($list as $item)
		{
			$result['no'] 					= $i;
			$result['date_approval']		= $item->date_approval;
			$result['status_approval']		= $item->status_approval;
			$result['note']					= $item->note;
			$result['company_name']			= $item->company_name;
			$result['site_name']			= $item->site_name;
			$result['submit_by']			= $item->submit_by;
			
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> count($list),
			"iTotalDisplayRecords" 	=> count($list),
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}
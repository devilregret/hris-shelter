<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_template extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('sales'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,10))){
			redirect(base_url());
		}
	}

	public function preview($template_id = FALSE)
	{
		$this->load->model('sales_template_model');
		$data['_TITLE_'] 		= 'Preview Template Sales';
		$data['_PAGE_']	 		= 'sales_template/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'sales_template';

		$data['id'] = $template_id;

		
		if (!$template_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('sales_template/list'));
		}

		$data['preview'] = $this->sales_template_model->preview(array('id' => $template_id));
		$this->load->view('sales_template/preview', $data);
	}

	public function form($template_id = FALSE)
	{
		$this->load->model(array('sales_template_model'));

		$data['id'] 			= '';
		$data['title']			= '';
		$data['content']		= '';

		if($this->input->post()){
			$data['id'] 			= $this->input->post('id');
			$data['title'] 			= $this->input->post('title');
			$data['content']		= $this->input->post('content');
			
			$this->form_validation->set_rules('title', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$save_id	 	= $this->sales_template_model->save($data);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('sales_template/list'));
		}

		if ($template_id)
		{
			$data = (array) $this->sales_template_model->get(array('id' => $template_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('sales_template/list'));
			}
		}
		$data['_TITLE_'] 		= 'Template Sales';
		$data['_PAGE_'] 		= 'sales_template/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'sales_template';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Template Sales';
		$data['_PAGE_'] 		= 'sales_template/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'sales_template';
		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('sales_template_model'));

		$column_index = $_POST['order'][0]['column']; 
		$params['columns'] 		= 'A.id, A.title';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['title']	= $_POST['columns'][1]['search']['value'];

		$list 	= $this->sales_template_model->gets($params);
		$total 	= $this->sales_template_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 		= $i;
			$result['title'] 	= $item->title;
			$result['action'] 	=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("sales_template/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("sales_template/form/".$item->id).'">Ubah</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}

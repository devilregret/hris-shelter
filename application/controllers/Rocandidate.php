<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rocandidate extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!in_array($this->session->userdata('role'), array(2,3,8))){
			redirect(base_url());
		}
		if($this->session->userdata('site') == ''){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan pilih site bisnis.','danger'));
			redirect(base_url());	
		}
	}

	public function list($type = FALSE){
		$this->load->model(array('site_model'));
		$data['_TITLE_'] 		= 'Kandidat';
		$data['_PAGE_'] 		= 'rocandidate/list';
		$data['_MENU_PARENT_'] 	= 'roemployee';
		$data['_MENU_'] 		= 'rocandidate';

		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address'));
	
		$this->view($data);
	}

	public function list_ajax(){
		$this->load->model(array('candidate_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.registration_number, A.full_name, A.id_card, A.address_domisili, FLOOR(DATEDIFF(CURRENT_DATE, A.date_birth)/365.25) AS age, DATE_FORMAT(A.date_birth, '%d/%m/%Y') AS date_birth, G.name AS city_birth, J.name AS company_name, K.name AS position, M.name AS branch_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['site_id']		= $this->session->userdata('site');
		$params['status_approval'] 	= 2;

		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['id_card']			= $_POST['columns'][3]['search']['value'];
		$params['age']				= $_POST['columns'][4]['search']['value'];
		$params['city_birth']		= $_POST['columns'][5]['search']['value'];
		$params['date_birth']		= $_POST['columns'][6]['search']['value'];
		$params['address_domisili']	= $_POST['columns'][7]['search']['value'];
		$params['position']			= $_POST['columns'][8]['search']['value'];
		$params['company_name']		= $_POST['columns'][9]['search']['value'];
		$params['branch_name']		= $_POST['columns'][10]['search']['value'];

		$list 	= $this->candidate_model->gets($params);
		$total 	= $this->candidate_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['full_name']			= $item->full_name;
			$result['id_card']				= $item->id_card;
			$result['age']					= $item->age;
			$result['city_birth']			= $item->city_birth;
			$result['date_birth']			= $item->date_birth;
			$result['address_domisili']		= $item->address_domisili;
			$result['position']				= $item->position;
			$result['company_name']			= $item->company_name;
			$result['branch_name']			= $item->branch_name;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action"  style="cursor:pointer;" data-href="'. base_url("rocandidate/preview/".$item->id).'"><i class="fas fa-eye text-white"></i></a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function approval(){

		$this->load->model(array('candidate_model', 'approval_model'));
		$action = $this->input->post('action');

		foreach ($this->input->post('id') as $candidate_id)
		{
			$data = (array) $this->candidate_model->get(array('id' => $candidate_id));
			if (empty($data)){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('rocandidate/list'));
			}else{
				$status_approval = 0;
				$insert = array('id' => $candidate_id, 'status_approval' => $status_approval, 'site_id' => $data['site_id']);
				$result = $this->candidate_model->save($insert);

				$approval = array('id' => '', 'employee_id' => $data['id'] , 'site_id' => $data['site_id'], 'company_id' => $data['company_id'], 'position_id' => $data['position_id'], 'status_approval' => $status_approval);
				$this->approval_model->save($approval);

				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pembatalan gagal.','danger'));
					redirect(base_url('rocandidate/list'));
				}
			}
		}
		
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Pembatalan Sukses.','success'));
		redirect(base_url('rocandidate/list'));
	}

	public function preview($candidate_id=FALSE)
	{
		$this->load->model(array('candidate_model','employee_education_model', 'employee_emergency_model', 'employee_history_model', 'employee_language_model', 'employee_reference_model', 'employee_skill_model'));
		$data['_TITLE_'] 		= 'Preview Pelamar';
		$data['_PAGE_']	 		= 'candidate/preview';
		$data['_MENU_PARENT_'] 	= 'recruitment';
		$data['_MENU_'] 		= 'candidate';

		$data['id'] = $candidate_id;

		if (!$candidate_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('candidate/list'));
		}

		$data['list_education'] = $this->employee_education_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.institute, A.start, A.end'));
		$data['list_emergency'] = $this->employee_emergency_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.relation, A.phone, A.address'));
		$data['list_history'] 	= $this->employee_history_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.company_name, A.position, A.period,  A.salary, A.company_address, A.resign_note'));
		$data['list_language'] 	= $this->employee_language_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.language, A.verbal, A.write'));
		$data['list_reference'] = $this->employee_reference_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.company_name, A.position, A.phone'));
		$data['list_skill'] 	= $this->employee_skill_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));

		$data['preview'] = $this->candidate_model->preview(array('id' => $candidate_id));
		$data['preview']->date_birth = format_date_ina($data['preview']->date_birth);
		$data['preview']->city_birth = str_replace(array('Kota', 'Kabupaten'), '', $data['preview']->city_birth);
		$this->load->view('rocandidate/preview', $data);
	}

	public function import(){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('employee_model', 'city_model', 'branch_model', 'position_model', 'company_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {
				$data['id'] 					= '';
				$data['registration_number']	= '';
				$data['status']					= 0;
				$data['status_approval']		= 2;
				$data['site_id']				= $this->session->userdata('site');
				$data['full_name'] 				= replace_null($sheet->getCell('B'.$row)->getValue());
				$data['id_card'] 				= replace_null($sheet->getCell('C'.$row)->getValue());
				$city_birth 					= $this->city_model->get(array('name' => replace_null($sheet->getCell('D'.$row)->getValue()), 'columns' => 'A.id'));
				if($city_birth){
					$data['city_birth_id'] 	= $data['city_card_id'] = $data['city_domisili_id'] = $city_birth->id;
				}

				$excel_date = $sheet->getCell('E'.$row)->getValue();
				$unix_date = ($excel_date - 25569) * 86400;
				$excel_date = 25569 + ($unix_date / 86400);
				$unix_date = ($excel_date - 25569) * 86400;
				$data['date_birth']		= gmdate("Y-m-d", $unix_date);
				$data['address_card']	= $data['address_domisili'] = replace_null($sheet->getCell('F'.$row)->getValue());
				
				$position 	= $this->position_model->get(array('name' => replace_null($sheet->getCell('G'.$row)->getValue()), 'columns' => 'A.id'));
				if($position){
					$data['position_id'] 			= $position->id;
				}

				$company 	= $this->company_model->get(array('code' => replace_null(strtoupper($sheet->getCell('H'.$row)->getValue())), 'columns' => 'A.id'));
				if($company){
					$data['company_id'] 			= $company->id;
				}

				$branch 	= $this->branch_model->get(array('name' => replace_null($sheet->getCell('I'.$row)->getValue()), 'columns' => 'A.id'));
				if($branch){
					$data['branch_id'] 			= $branch->id;
				}

				$candidate = $this->employee_model->get(array('id_card' => $data['id_card'], 'columns' => 'A.id, A.registration_number'));
				if($candidate){
					if($candidate->status <= 2){
						$data['id']					= $candidate->id;
						$data['registration_number']= $candidate->registration_number;
					}
				}

				$save_id = $this->employee_model->save($data);
				if ($save_id) {
					if($data['registration_number'] == ''){
						$registration_number 		= 10000+$save_id;
						$this->employee_model->save(array('id' => $save_id, 'registration_number' => $registration_number));
					}
				}
			}

			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
					redirect(base_url('rocandidate/list'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
					redirect(base_url('rocandidate/list'));
		}
	}
}
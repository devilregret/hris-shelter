<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roattendance extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
		if($this->session->userdata('site') == ''){
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Silahkan pilih site bisnis.','danger'));
			redirect(base_url());	
		}
	}

	public function list($date = 0){
		
		$this->load->model(array('site_model', 'rocutoff_model', 'workday_model'));
		
		$data['_TITLE_'] 		= 'Kehadiran Karyawan';
		$data['_PAGE_'] 		= 'roattendance/list';
		$data['_MENU_PARENT_'] 	= 'roemployee';
		$data['_MENU_'] 		= 'roattendance_regular';
		$data['site'] 			= $this->site_model->get_site(array('id' => $this->session->userdata('site'), 'columns' => 'A.id, A.name, A.address, A.attendance_machine'));
		$cutoff 	= $this->rocutoff_model->get(array('columns' => 'A.start_date, A.end_date', 'site_id' => $this->session->userdata('site')));
		if($cutoff){
			$data['start_date'] = $cutoff->start_date.'/'.date("m/Y", strtotime("-".($date+1)." month"));
			$data['end_date'] 	= $cutoff->end_date.'/'.date('m/Y', strtotime("-".$date." month"));
		}else{
			$this->session->set_flashdata('message', message_box('Silahkan setting tanggal cutoff terlebih dahulu.','warning'));
			redirect(base_url('cutoff/form/'.$this->session->userdata('site')));
		}
		$data['sdate'] = $date;
		
		$this->view($data);
	}

	public function list_ajax($date = 0){

		$this->load->model(array('roattendance_model', 'rocutoff_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id AS employee_id, A.pin_finger, A.id_card, A.full_name, A.employee_number";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['pin_finger']		= $_POST['columns'][1]['search']['value'];
		$params['employee_number']	= $_POST['columns'][2]['search']['value'];
		$params['full_name']		= $_POST['columns'][3]['search']['value'];
		
		$params['site_id']		= $this->session->userdata('site');

		$cutoff 	= $this->rocutoff_model->get(array('columns' => 'A.start_date, A.end_date', 'site_id' => $params['site_id']));
		if($cutoff){
			$data['start_date'] = $cutoff->start_date.'/'.date("m/Y", strtotime("-".($date+1)." month"));
			$data['end_date'] 	= $cutoff->end_date.'/'.date('m/Y', strtotime("-".$date." month"));
		}

		$list 	= $this->roattendance_model->gets($params);
		$total 	= $this->roattendance_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{			
			$result['no'] 				= $i;
			$result['pin_finger']		= $item->pin_finger;
			$result['id_card']			= $item->id_card;
			$result['full_name']		= $item->full_name;
			$result['employee_number']	= $item->employee_number;
			$result['present']			= 0;
			$result['not_present']		= 0;
			$result['overtime']			= 0;
			$result['action'] 			=
				'<a class="btn-sm btn-success btn-block" href="'.base_url("roattendance/employee/".$item->employee_id).'">Detail</i></a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function employee($employee_id = FALSE){
		
		$this->load->model(array('employee_model'));
		
		$data['employee'] 		= $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.id, A.full_name, A.employee_number, A.address_card, A.site_id'));

		$data['_TITLE_'] 		= 'Karyawan ';
		$data['_PAGE_'] 		= 'roattendance/employee';
		$data['_MENU_PARENT_'] 	= 'roemployee';
		$data['_MENU_'] 		= 'roattendance';
		$this->view($data);
	}

	public function employee_ajax($employee_id = FALSE){

		$this->load->model(array('roattendance_model'));
		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.employee_id, A.date, A.hours_start, A.hours_end";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['date']			= $_POST['columns'][1]['search']['value'];
		$params['hours_start']	= $_POST['columns'][2]['search']['value'];
		$params['hours_end']	= $_POST['columns'][3]['search']['value'];
		
		$params['employee_id']		= $employee_id;

		$list 	= $this->roattendance_model->employee_gets($params);
		$total 	= $this->roattendance_model->employee_gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();
		foreach($list as $item)
		{			
			$result['no'] 			= $i;
			$result['date']			= $item->date;
			$result['hours_start']	= $item->hours_start;
			$result['hours_end']	= $item->hours_end;
			$result['action'] 			=
				'<a class="btn-sm btn-success btn-block" href="'.base_url("roattendance/form/".$item->employee_id."/".$item->date).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("roattendance/delete/".$item->employee_id."/".$item->date).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function import($date = 0){
		if(!$_FILES['file']['name'] == ""){
			$this->load->library("Excel");
			
			$this->load->model(array('roemployeeshift_model', 'roattendance_model'));
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {

				$pin_finger = 	replace_null($sheet->getCell('A'.$row)->getValue());
				$date 	= 	replace_null($sheet->getCell('D'.$row)->getValue());
				$format_date = substr($date, 6, 4).'-'.substr($date, 3, 2).'-'.substr($date, 0, 2);
				$timestamp = strtotime($format_date);
				$day = date('l', $timestamp);
				
				$employee  	= $this->roattendance_model->get_schedule(array('pin_finger' => $pin_finger, 'site_id' => $this->session->userdata('site'), 'day' => get_day($day), 'columns' => 'A.id, C.hours_start, C.hours_end'));
				// print_prev($employee);

				// die();
				if(!$employee){
					continue;
				}
				print_prev($employee);

				die();
				$shift;

				$date 					= replace_null($sheet->getCell('B'.$row)->getValue());
				$insert['date'] 		= PHPExcel_Shared_Date::ExcelToPHPObject($date)->format('Y-m-d');
				$hours_start 			= replace_null($sheet->getCell('C'.$row)->getValue());
				$insert['hours_start'] 	= PHPExcel_Shared_Date::ExcelToPHPObject($hours_start)->format('H:i');
				$hours_end 				= replace_null($sheet->getCell('D'.$row)->getValue());
				$insert['hours_end'] 	= PHPExcel_Shared_Date::ExcelToPHPObject($hours_end)->format('H:i');
				$insert['employee_id'] 	= $employee->id;

				$attendance 	= $this->roattendance_model->get(array('employee_id' => $employee->id, 'date' => $date, 'columns' => 'A.employee_id'));
				if($attendance){
					$this->roattendance_model->update($insert);
				}else{
					$this->roattendance_model->insert($insert);

				}
			}

			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
		}

		redirect(base_url('roattendance/list/'.$date));
	}

	public function form($employee_id = FALSE, $date = FALSE)
	{
		$this->load->model(array('roattendance_model', 'employee_model'));
		$data['employee_id'] 	= $employee_id;
		$data['date']			= $date;
		$data['hours_start']	= '';
		$data['hours_end']		= '';

		if($this->input->post()){
			$insert['employee_id']	= $data['employee_id'] 	= $this->input->post('employee_id');
			$insert['date']			= $data['date'] 		= $this->input->post('date');
			$insert['hours_start']	= $data['hours_start'] 	= $this->input->post('hours_start');
			$insert['hours_end']	= $data['hours_end'] 	= $this->input->post('hours_end');

			$this->form_validation->set_rules('date', '', 'required');
			$this->form_validation->set_rules('hours_start', '', 'required');
			$this->form_validation->set_rules('hours_end', '', 'required');
				
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{

				$attendance 	= $this->roattendance_model->get(array('employee_id' => $data['employee_id'], 'date' => $data['date'], 'columns' => 'A.employee_id'));
				if($attendance){
					$this->roattendance_model->update($insert);
				}else{
					$this->roattendance_model->insert($insert);
				}

				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
			}
			redirect(base_url('roattendance/employee/'.$data['employee_id']));
		}

		if ($employee_id)
		{
			$employee = $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.id, A.full_name, A.employee_number, A.site_id'));
			$data['employee'] = $employee;
			if (!$employee)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('roattendance/employee/'.$data['employee_id']));
			}
		}

		$attendance 	= $this->roattendance_model->get(array('employee_id' => $data['employee_id'], 'date' => $data['date'], 'columns' => 'A.employee_id, A.hours_start, A.hours_end'));
		if($attendance){
			$data['hours_start']	= $attendance->hours_start;
			$data['hours_end']		= $attendance->hours_end;
		}

		$data['_TITLE_'] 		= 'Karyawan';
		$data['_PAGE_'] 		= 'roattendance/form';
		$data['_MENU_PARENT_'] 	= 'roemployee';
		$data['_MENU_'] 		= 'roattendance';
		return $this->view($data);
	}

	function delete($employee_id = FALSE, $date = FALSE)
	{
		$this->load->model('roattendance_model');
		if ($employee_id && $date)
		{
			$data =  $this->roattendance_model->get(array('employee_id' =>$employee_id, 'date' => $date, 'columns' => 'A.employee_id'));

			if ($data)
			{

				$insert = array('employee_id' => $employee_id, 'date' => $date, 'is_active' => 0);
				$result =$this->roattendance_model->update($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('roattendance/employee/'.$employee_id));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('roattendance/employee/'.$employee_id));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('roattendance/employee/'.$employee_id));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('roattendance/employee/'.$employee_id));
		}
	}

	public function syncronize($date = 0){

		$this->load->model(array('employee_casual_attendance_model', 'employee_casual_model', 'site_model'));
		if($this->input->post()){
			$params['api_token'] 	= 'aVwAQh5GSsaO8EyPYSnTkfxOzNCSMbhs1Sk3IQpyC94PB4IjgFmH1pgYxsa9';
			$params['id_mesin'] 	= $this->input->post('attendance_machine');
			$params['tgl_mulai'] 	= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_start'))));
			$params['tgl_akhir'] 	= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_finish'))));

			$this->site_model->save(array('id' => $this->session->userdata('site'), 'attendance_machine' => $params['id_mesin']));

			$curl = curl_init();
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			
			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://presensi.shelterapp.co.id/api/getRekapPresensi',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS => json_encode($params),
			  CURLOPT_HTTPHEADER => array(
			    'Content-Type: application/json'
			  ),
			));
			$response = curl_exec($curl);
			curl_close($curl);
			$response = json_decode($response);
			if(isset($response->data)){
				foreach($response->data as $item)
				{
					$employee = $this->employee_casual_model->get(array('employee_number' => $item->id_pegawai, 'columns' => 'A.id'));
					if($employee){
						$data['id'] 			= '';
						$data['employee_id']	= $employee->id;
						$data['date']			= substr($item->absen_in, 0,10);
						$data['check_in']		= substr($item->absen_in, 11,8);
						$data['check_out']		= substr($item->absen_out, 11,8);
						$data['date_in']		= $item->absen_in;
						$data['date_out']		= $item->absen_out;
						$attendance = $this->employee_casual_attendance_model->get($data);
						if($attendance){
							$data['id'] 		= $attendance->id;					
						}
						$save_id = $this->employee_casual_attendance_model->save($data);
					}
				}

				$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disinkronisasi.','success'));
				redirect(base_url('roattendance/list/'.$date));
			}else{
				if(isset($response->message)){
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> '.$response->message.'.','danger'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Sinkronisasi absensi gagal dilakukan.','danger'));
				}
				redirect(base_url('roattendance/list/'.$date));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Sinkronisasi absensi gagal dilakukan.','danger'));
			redirect(base_url('roattendance/list/'.$date));
		}
	}
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Test extends CI_Controller {

	public function form(){
		if($this->input->post()){
			if(!$_FILES['file']['name'] == ""){
				$this->load->library("Excel");
				
				$excelreader 	= new PHPExcel_Reader_Excel2007();
				$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
				$sheet  		= $loadexcel->getActiveSheet(0);
				$rend			= $sheet->getHighestRow();
				for ($row = 2; $row <= $rend; $row++) {
					print_prev(replace_null($sheet->getCell('A'.$row)->getValue()));
				}
			}
		}

		$data['_TITLE_'] 		= 'Preview Gaji Karyawan';
		$data['_MENU_PARENT_'] 	= 'employee_contract';
		$data['_MENU_'] 		= 'employee';
		$this->load->view('test/import', $data);
	}

	public function scroll(){
		$data['_TITLE_'] 		= 'Preview Gaji Karyawan';
		$data['_MENU_PARENT_'] 	= 'employee_contract';
		$data['_MENU_'] 		= 'employee';
		$this->load->view('test/scroll', $data);
	}
}
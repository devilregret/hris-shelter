<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipment extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('sales'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,10))){
			redirect(base_url());
		}
	}

	public function list(){
		$data['_TITLE_'] 		= 'HPP Barang';
		$data['_PAGE_'] 		= 'equipment/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'equipment';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('selling_price_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.name, A.type_item, A.price, A.amortization, A.amortization_price';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['type']			= 'Barang';

		$params['name']					= $_POST['columns'][1]['search']['value'];
		$params['type_item']			= $_POST['columns'][2]['search']['value'];
		$params['price']				= $_POST['columns'][3]['search']['value'];
		$params['amortization']			= $_POST['columns'][4]['search']['value'];
		$params['amortization_price']	= $_POST['columns'][5]['search']['value'];
		
		$list 	= $this->selling_price_model->gets($params);
		$total 	= $this->selling_price_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 					= $i;
			$result['name'] 				= $item->name;
			$result['type_item']			= $item->type_item;
			$result['price']				= nominal_rupiah($item->price);
			$result['amortization']			= $item->amortization;
			$result['amortization_price']	= nominal_rupiah((int)$item->amortization_price);
			$result['action'] 			=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("equipment/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("equipment/form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("equipment/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function form($equipment_id = FALSE)
	{
		$this->load->model(array('selling_price_model'));

		$data['id'] 				= '';
		$data['name']				= '';
		$data['type_item'] 			= '';
		$data['type']				= 'Barang';
		$data['price']				= 0;
		$data['amortization']		= 0;
		$data['amortization_price']	= 0;
		if($this->input->post()){
			$data['id'] 				= $this->input->post('id');
			$data['name']				= $this->input->post('name');
			$data['type_item']			= $this->input->post('type_item');
			$data['amortization']		= $this->input->post('amortization');
			$data['price']				= str_replace(".", "", $this->input->post('price'));
			$data['amortization_price']	= str_replace(".", "", $this->input->post('amortization_price'));
			
			$this->form_validation->set_rules('name', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$save_id	 	= $this->selling_price_model->save($data);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('equipment/list'));
		}

		if ($equipment_id)
		{
			$data = (array) $this->selling_price_model->get(array('id' => $equipment_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('equipment/list'));
			}
		}

		$data['_TITLE_'] 		= 'HPP Barang';
		$data['_PAGE_'] 		= 'equipment/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'equipment';
		return $this->view($data);
	}

	public function preview($equipment_id=FALSE)
	{
		$this->load->model('selling_price_model');
		$data['_TITLE_'] 		= 'Preview HPP Barang';
		$data['_PAGE_']	 		= 'equipment/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'equipment';

		$data['id'] = $equipment_id;

		
		if (!$equipment_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('equipment/list'));
		}

		$data['preview'] = $this->selling_price_model->preview(array('id' => $equipment_id));
		$this->load->view('equipment/preview', $data);
	}

	public function delete($equipment_id = false)
	{
		$this->load->model('selling_price_model');
		if ($equipment_id)
		{
			$data =  $this->selling_price_model->get(array('id' => $equipment_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $equipment_id, 'is_active' => 0);
				$result = $this->selling_price_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('equipment/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal dihapus.','danger'));
					redirect(base_url('equipment/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('equipment/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('equipment/list'));
		}
	}

	public function export()
	{

		$this->load->model(array('selling_price_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = 'equipment';
		$files = glob(FCPATH."files/".$title."/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "No");
		$excel->getActiveSheet()->setCellValue("B".$i, "Nama Barang");
		$excel->getActiveSheet()->setCellValue("C".$i, "Tipe Barang");
		$excel->getActiveSheet()->setCellValue("D".$i, "Harga Jual");
		$excel->getActiveSheet()->setCellValue("E".$i, "Masa Amortisasi (bulan)");
		$excel->getActiveSheet()->setCellValue("F".$i, "Harga Jual Setelah Amortisasi");

		$params 			= $this->input->get();
		$params['type']		= 'Barang';
		$params['columns'] 	= 'A.id, A.name, A.type_item, A.price, A.amortization, A.amortization_price';
		$list_wages 		= $this->selling_price_model->gets($params);
		
		$i 	=2;
		$no = 1;
		foreach ($list_wages as $item) {
			$excel->getActiveSheet()->setCellValue("A".$i, $no);
			$excel->getActiveSheet()->setCellValue("B".$i, $item->name);
			$excel->getActiveSheet()->setCellValue("C".$i, $item->type_item);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->price);
			$excel->getActiveSheet()->setCellValue("E".$i, $item->amortization);
			$excel->getActiveSheet()->setCellValue("F".$i, $item->amortization_price);
			$i++;
			$no++;
		}
		$excel->getActiveSheet()->setTitle('Harga Jual');
				$excel->setActiveSheetIndex(0);
		
		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;
	}

	public function import(){

		if(!$_FILES['file']['name'] == ""){

			$this->load->model(array('selling_price_model'));
			
			$this->load->library("Excel");
			
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {
				$data['id'] 				= '';
				$data['type'] 				= 'Barang';
				$data['name']				= replace_null($sheet->getCell('B'.$row)->getValue());
				$data['type_item']			= replace_null($sheet->getCell('C'.$row)->getValue());
				$data['price'] 				= replace_null($sheet->getCell('D'.$row)->getValue());
				$data['amortization'] 		= replace_null($sheet->getCell('E'.$row)->getValue());
				$data['amortization_price'] = replace_null($sheet->getCell('F'.$row)->getCalculatedValue());
				$equipment 			= $this->selling_price_model->get(array('name' => $data['name'], 'columns' => 'A.id'));
				if($equipment){
					$data['id'] = $equipment->id;
				}
				$save_id = $this->selling_price_model->save($data);
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil diimport.','success'));
			redirect(base_url('equipment/list'));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pilih file yang akan diupload.','danger'));
			redirect(base_url('equipment/list'));
		}
	}
}
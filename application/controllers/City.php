<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function delete($city_id = false)
	{
		$this->load->model('city_model');
		if ($city_id)
		{
			$data =  $this->city_model->get(array('id' => $city_id, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $city_id, 'is_active' => 0);
				$result = $this->city_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('city/list'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal dihapus.','danger'));
					redirect(base_url('city/list'));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('city/list'));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('city/list'));
		}
	}

	public function preview($city_id=FALSE)
	{
		$this->load->model('city_model');
		$data['_TITLE_'] 		= 'Preview Kota';
		$data['_PAGE_']	 		= 'city/preview';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'city';

		$data['id'] = $city_id;

		
		if (!$city_id)
		{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('city/list'));
		}

		$data['preview'] = $this->city_model->preview(array('id' => $city_id));
		$this->load->view('city/preview', $data);
	}

	public function form($city_id = FALSE)
	{
		$this->load->model(array('province_model','city_model'));

		$data['id'] 		= '';
		$data['name']		= '';
		$data['address'] 	= '';
		$data['province_id']= '';
		if($this->input->post()){
			$data['id'] 		= $this->input->post('id');
			$data['name'] 		= $this->input->post('name');
			$data['province_id']= $this->input->post('province_id');
			
			$this->form_validation->set_rules('name', '', 'required');
			$this->form_validation->set_rules('province_id', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$insert = array('id' => $data['id'], 'name' => $data['name'], 'province_id' => $data['province_id']);
				$save_id	 	= $this->city_model->save($insert);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('city/list'));
		}

		if ($city_id)
		{
			$data = (array) $this->city_model->get(array('id' => $city_id));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('city/list'));
			}
		}

		$data['list_province'] 	= $this->province_model->gets(array('columns' => 'A.id, A.name'));
		$data['_TITLE_'] 		= 'Kota';
		$data['_PAGE_'] 		= 'city/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'city';
		return $this->view($data);
	}

	public function list(){
		$data['_TITLE_'] 		= 'Kota';
		$data['_PAGE_'] 		= 'city/list';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'city';

		$this->view($data);

	}

	public function list_ajax(){
		$this->load->model(array('city_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.name AS name, B.name AS province_name';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];

		$params['name']			= $_POST['columns'][1]['search']['value'];
		$params['province_name']= $_POST['columns'][2]['search']['value'];
		
		$list 	= $this->city_model->gets($params);
		$total 	= $this->city_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['no'] 			= $i;
			$result['name'] 		= $item->name;
			$result['province_name']= $item->province_name;
			$result['action'] 		=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("city/preview/".$item->id).'">Lihat</a>
				<a class="btn-sm btn-success btn-block" href="'.base_url("city/form/".$item->id).'">Ubah</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("city/delete/".$item->id).'">Hapus</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Account extends Frontend_Controller {

	public function form_attendance($date = FALSE)
	{
		$this->load->model(array('employee_attendance_model'));

		$data['date']			= '';
		$data['hours_start'] 	= '';
		$data['hours_end']		= '';
		$data['employee_id'] 	= $this->session->userdata('employee_id');
		
		if($this->input->post()){
			$data['date'] 			= $this->input->post('date');
			$data['hours_start']	= $this->input->post('hours_start');
			$data['hours_end']		= $this->input->post('hours_end');
			
			$this->form_validation->set_rules('date', '', 'required');
			$this->form_validation->set_rules('hours_start', '', 'required');
			$this->form_validation->set_rules('hours_end', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$exist = $this->employee_attendance_model->get(array('employee_id' => $data['employee_id'], 'date' => $data['date'], 'columns' => 'A.date'));
				if($exist){
					$save_id	 	= $this->employee_attendance_model->update($data);
					if ($save_id) {
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
					}else{
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
					}
					// $this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Tanggal yang di pilih sudah terisi.','danger'));
				}else{
					$save_id	 	= $this->employee_attendance_model->insert($data);
					if ($save_id) {
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
					}else{
						$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
					}
				}
			}
			redirect(base_url('account/attendance'));
		}

		if ($date)
		{
			$data = (array) $this->employee_attendance_model->get(array('employee_id' => $data['employee_id'], 'date' => $date, 'columns' => 'A.date, A.hours_start, A.hours_end'));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('account/attendance'));
			}
		}

		$data['_TITLE_'] 		= 'Kehadiran';
		$data['_PAGE_'] 		= 'account/form_attendance';
		$data['_MENU_PARENT_'] 	= 'account';
		$data['_MENU_'] 		= 'attendance';
		return $this->view($data);
	}
	
	public function register()
	{
		$this->load->model(array('city_model', 'province_model', 'preference_model', 'candidate_model','employee_education_model', 'employee_emergency_model', 'employee_history_model', 'employee_language_model', 'employee_reference_model', 'employee_skill_model', 'auth_model', 'email_model'));
		
		$data['id']								= $this->session->userdata('employee_id');
		$data['registration_number']			= '';
		$data['full_name']						= '';
		$data['id_card']						= '';
		$data['id_card_period']					= 'SEUMUR HIDUP';
		$data['city_birth_id']					= '';
		$data['date_birth']						= '';
		$data['city_card_id']					= '';
		$data['address_card']					= '';
		$data['city_domisili_id'] 				= '';
		$data['address_domisili'] 				= '';
		$data['status_residance'] 				= '';
		$data['phone_number'] 					= '';
		$data['email'] 							= '';
		$data['socmed_fb'] 						= '';
		$data['socmed_ig'] 						= '';
		$data['socmed_tw'] 						= '';
		$data['religion'] 						= '';
		$data['marital_status']					= '';
		$data['gender'] 						= '';
		$data['heigh'] 							= '';
		$data['weigh'] 							= '';
		$data['clothing_size'] 					= '';
		$data['shoe_size'] 						= '';
		$data['education_level']	 			= '';
		$data['education_majors'] 				= '';
		$data['illness_minor'] 					= '';
		$data['illness_serious'] 				= '';
		$data['illness_treated'] 				= '';
		$data['crime_notes'] 					= '';
		$data['bank_account'] 					= '';
		$data['bank_account_name'] 				= '';
		$data['bank_account_create'] 			= '';
		$data['tax_number'] 					= '';
		$data['health_insurance'] 				= '';
		$data['health_insurance_note'] 			= '';
		$data['health_insurance_company'] 		= '';
		$data['preference_job1'] 				= '';
		$data['preference_job2'] 				= '';
		$data['preference_job3'] 				= '';
		$data['preference_placement']			= '';
		$data['recommendation'] 				= '';
		$data['blood_group'] 					= '';
		$data['covid_vaccine'] 					= '';
		$data['family_mother'] 					= '';

		$data['document_photo']					= '';
		$data['document_id_card']				= '';
		$data['document_marriage_certificate']	= '';
		$data['document_tax_number']			= '';
		$data['document_family_card']			= '';
		$data['document_doctor_note']			= '';
		$data['document_police_certificate']	= '';
		$data['document_certificate_education']	= '';
		$data['document_cv']					= '';

		$data['period_doctor_note']				= '';

		$data['status']							= 0;
		$data['status_approval']				= 0;
		$data['status_completeness']			= 0;

		if($this->input->post()){
			$this->form_validation->set_rules('full_name', 'Nama Lengkap', 'required');
			$this->form_validation->set_rules('id_card', 'Nomor KTP', 'required|numeric|min_length[16]|max_length[18]');

			$data['id_card']			= $params['id_card']			= $this->input->post('id_card');
			$result = (array) $this->candidate_model->get(array('id_card' => $data['id_card']));
			if(!empty($result)){
				$data = $result;
			}
			
			if ($this->form_validation->run() == FALSE){
				$data['message']= message_box('<strong>Gagal!</strong> Silahkan lengkapi data anda sesuai dengan ketentuan.','danger');
			}else{
				$data['full_name']			= $params['full_name']			= $this->input->post('full_name');
				$data['id_card']			= $params['id_card']			= $this->input->post('id_card');
				$data['city_birth_id']		= $params['city_birth_id'] 		= $this->input->post('city_birth_id');
				$data['date_birth']			= $params['date_birth'] 		= date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('date_birth'))));
				$data['city_card_id']		= $params['city_card_id'] 		= $this->input->post('city_card_id');
				$data['address_card']		= $params['address_card'] 		= $this->input->post('address_card');
				$data['city_domisili_id']	= $params['city_domisili_id'] 	= $this->input->post('city_domisili_id');
				$data['address_domisili']	= $params['address_domisili'] 	= $this->input->post('address_domisili');
				if($data['city_domisili_id'] == ''){
					$data['city_domisili_id']	= $params['city_domisili_id'] = $data['city_card_id'];
				}
				if($data['address_domisili'] == ''){
					$data['address_domisili']	= $params['address_domisili'] = $data['address_card'];
				}

				$data['status_residance']	= $params['status_residance'] 	= strtolower($this->input->post('status_residance'));
				$data['phone_number']		= $params['phone_number'] 		= $this->input->post('phone_number');
				$data['email']				= $params['email'] 				= $this->input->post('email');
				$data['socmed_fb']			= $params['socmed_fb'] 			= $this->input->post('socmed_fb');
				$data['socmed_ig']			= $params['socmed_ig'] 			= $this->input->post('socmed_ig');
				$data['socmed_tw']			= $params['socmed_tw'] 			= $this->input->post('socmed_tw');
				$data['religion']			= $params['religion'] 			= $this->input->post('religion');
				$data['marital_status']		= $params['marital_status'] 	= $this->input->post('marital_status');
				$data['gender']				= $params['gender'] 			= $this->input->post('gender');
				$data['heigh']				= $params['heigh'] 				= $this->input->post('heigh');
				$data['weigh']				= $params['weigh'] 				= $this->input->post('weigh');
				$data['clothing_size']		= $params['clothing_size'] 		= $this->input->post('clothing_size');
				$data['shoe_size']			= $params['shoe_size'] 			= $this->input->post('shoe_size');
				$data['education_level']	= $params['education_level'] 	= $this->input->post('education_level');
				$data['education_majors']	= $params['education_majors'] 	= strtoupper($this->input->post('education_majors'));

				$data['illness_minor']		= $params['illness_minor'] 		= $this->input->post('illness_minor');
				$data['illness_serious']	= $params['illness_serious'] 	= $this->input->post('illness_serious');
				$data['illness_treated']	= $params['illness_treated'] 	= $this->input->post('illness_treated');

				$data['crime_notes']				= $params['crime_notes'] 				= $this->input->post('crime_notes');
				$data['bank_account_create']		= $params['bank_account_create']		= $this->input->post('bank_account_create');
				$data['bank_account']				= $params['bank_account'] 				= $this->input->post('bank_account');
				$data['bank_account_name']			= $params['bank_account_name'] 			= $this->input->post('bank_account_name');
				$data['tax_number']					= $params['tax_number'] 				= $this->input->post('tax_number');
				$data['health_insurance']			= $params['health_insurance'] 			= $this->input->post('health_insurance');
				$data['health_insurance_note']		= $params['health_insurance_note'] 		= $this->input->post('health_insurance_note');
				$data['health_insurance_company']	= $params['health_insurance_company'] 	= $this->input->post('health_insurance_company');
				$data['recommendation']				= $params['recommendation'] 			= $this->input->post('recommendation');
				$data['blood_group']				= $params['blood_group'] 				= $this->input->post('blood_group');
				$data['covid_vaccine']				= $params['covid_vaccine'] 				= $this->input->post('covid_vaccine');
				$data['family_mother']				= $params['family_mother'] 				= $this->input->post('family_mother');
				
				$data['preference_placement']		= $params['preference_placement'] 		= $this->input->post('preference_placement');
				if($data['preference_placement'] == ''){
					$data['preference_placement']	= $params['preference_placement'] 		= $data['city_domisili_id'];
				}
				

				$data['period_doctor_note']			= $params['period_doctor_note'] 	= $this->input->post('period_doctor_note');

				$preference_job 					= $this->input->post('preference_job');
				if(isset($preference_job[0])){
					$data['preference_job1'] = $params['preference_job1'] = $preference_job[0];
				}
				if(isset($preference_job[1])){
					$data['preference_job2'] = $params['preference_job2'] = $preference_job[1];
				}
				if(isset($preference_job[2])){
					$data['preference_job3'] = $params['preference_job3'] = $preference_job[2];
				}

				$params['id_card_period']			= $data['id_card_period'];
				$params['status']					= $data['status'];
				$params['status_approval']			= $data['status_approval'];
				$params['status_completeness']		= 0;

				$url_document = 'files/document/'.$data['id_card'];
				$document_path = FCPATH.$url_document;
				if(!is_dir($document_path)){
					mkdir($document_path, 0755, TRUE);
				}

				$config['upload_path'] 	= $document_path;
				$config['overwrite']  	= TRUE;
				$config['allowed_types']= 'jpg|jpeg|png';
				
				if($_FILES['document_photo']['name'] != ""){
					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'foto';
					$this->load->library('upload', $config, 'document_photo');

					if (!$this->document_photo->do_upload('document_photo')) {
						$data['message']= message_box('<strong>Gagal!</strong> '.$this->document_photo->display_errors().'.','danger');
					} else {
						$document 	= $this->document_photo->data();
						$params['document_photo'] = $data['document_photo'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_id_card']['name'] != ""){

					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'ktp';
					$this->load->library('upload', $config, 'document_id_card');

					if (!$this->document_id_card->do_upload('document_id_card')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_id_card->display_errors().'.','danger');
					} else {
						$document = $this->document_id_card->data();
						$params['document_id_card'] =	$data['document_id_card'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_marriage_certificate']['name'] != ""){

					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'buku-nikah';
					$this->load->library('upload', $config, 'document_marriage_certificate');

					if (!$this->document_marriage_certificate->do_upload('document_marriage_certificate')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_marriage_certificate->display_errors().'.','danger');
					} else {
						$document = $this->document_marriage_certificate->data();
						$params['document_marriage_certificate'] =	$data['document_marriage_certificate'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_tax_number']['name'] != ""){

					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'npwp';
					$this->load->library('upload', $config, 'document_tax_number');

					if (!$this->document_tax_number->do_upload('document_tax_number')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_tax_number->display_errors().'.','danger');
					} else {
						$document = $this->document_tax_number->data();
						$params['document_tax_number'] =	$data['document_tax_number'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_family_card']['name'] != ""){

					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'kartu-keluarga';
					$this->load->library('upload', $config, 'document_family_card');

					if (!$this->document_family_card->do_upload('document_family_card')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_family_card->display_errors().'.','danger');
					} else {
						$document = $this->document_family_card->data();
						$params['document_family_card'] =	$data['document_family_card'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_doctor_note']['name'] != ""){

					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'surat-sehat';
					$this->load->library('upload', $config, 'document_doctor_note');

					if (!$this->document_doctor_note->do_upload('document_doctor_note')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_doctor_note->display_errors().'.','danger');
					} else {
						$document = $this->document_doctor_note->data();
						$params['document_doctor_note'] =	$data['document_doctor_note'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_police_certificate']['name'] != ""){

					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'skck';
					$this->load->library('upload', $config, 'document_police_certificate');

					if (!$this->document_police_certificate->do_upload('document_police_certificate')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_police_certificate->display_errors().'.','danger');
					} else {
						$document = $this->document_police_certificate->data();
						$params['document_police_certificate'] =	$data['document_police_certificate'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_certificate_education']['name'] != ""){

					$config['allowed_types']= 'jpg|jpeg|png';
					$config['file_name'] 	= 'skck';
					$this->load->library('upload', $config, 'document_certificate_education');

					if (!$this->document_certificate_education->do_upload('document_certificate_education')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_certificate_education->display_errors().'.','danger');
					} else {
						$document = $this->document_certificate_education->data();
						$params['document_certificate_education'] =	$data['document_certificate_education'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($_FILES['document_cv']['name'] != ""){

					$config['allowed_types']= 'pdf';
					$config['file_name'] 	= 'curriculum_vitae';
					$this->load->library('upload', $config, 'document_cv');

					if (!$this->document_cv->do_upload('document_cv')) {
						$data['message'] = message_box('<strong>Gagal!</strong> '.$this->document_cv->display_errors().'.','danger');
					} else {
						$document = $this->document_cv->data();
						$params['document_cv'] =	$data['document_cv'] = base_url($url_document.'/'.$document['file_name']);
					}
				}

				if($data['id'] == ''){
						$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data tidak ditemukan.','danger'));
					redirect(base_url('account/register'));
				}
				
				if($data['id'] == ''){
					$params['id'] 		= '';
					$params['site_id'] 	= 1;
					$city 				= $this->city_model->get(array('id' => $data['city_card_id'], 'columns' => 'A.province_id'));
					$province 			= $this->province_model->get(array('id' => $city->province_id, 'columns' => 'A.branch_id'));
					$params['branch_id']= $province->branch_id;
				}else{
					$params['id'] 		= $data['id'];
				}

				if($this->session->userdata('is_employee')){
					unset($params['full_name']);
					unset($params['id_card']);
					unset($params['bank_account']);
				}
				$save_id = $this->candidate_model->save($params);
				if ($save_id) {
					$this->session->set_userdata('name',$params['full_name']);
					$this->auth_model->save(array('id' => $this->session->userdata('user_id'), 'username' => $params['id_card'], 'full_name' => $params['full_name'], 'email' => $params['email']));
					if($data['registration_number'] == ''){
						$registration_number 		= 10000+$save_id;
						$this->candidate_model->save(array('id' => $save_id, 'registration_number' => $registration_number));
					}

					$this->session->set_userdata('employee_id',$save_id);
					$this->auth_model->save(array('id' => $this->session->userdata('user_id'), 'employee_id' => $save_id));

					$data['id'] = $save_id;

					$emergency_contact_id		= $this->input->post('emergency_contact_id');
					$emergency_contact_name		= $this->input->post('emergency_contact_name');
					$emergency_contact_relation	= $this->input->post('emergency_contact_relation');
					$emergency_contact_phone	= $this->input->post('emergency_contact_phone');
					$emergency_contact_address	= $this->input->post('emergency_contact_address');

					foreach ($emergency_contact_id as $key => $value ) {
						if($emergency_contact_name[$key] == ''){
							continue;
						}
						$params_emergency['employee_id']= $data['id'];
						$params_emergency['id']			= $emergency_contact_id[$key];
						$params_emergency['name']		= $emergency_contact_name[$key];
						$params_emergency['relation']	= $emergency_contact_relation[$key];
						$params_emergency['phone']		= $emergency_contact_phone[$key];
						$params_emergency['address']	= $emergency_contact_address[$key];
						$this->employee_emergency_model->save($params_emergency);
					}

					$education_id				= $this->input->post('education_id');
					$education					= $this->input->post('education');
					$education_start			= $this->input->post('education_start');
					$education_end				= $this->input->post('education_end');

					foreach ($education_id as $key => $value ) {
						if($education[$key] == ''){
							continue;
						}
						$params_education['employee_id']= $data['id'];
						$params_education['id']			= $education_id[$key];
						$params_education['institute']	= $education[$key];
						$params_education['start']		= $education_start[$key];
						$params_education['end']		= $education_end[$key];
						$this->employee_education_model->save($params_education);
					}

					$languange_id				= $this->input->post('languange_id');
					$languange					= $this->input->post('languange');
					$languange_verbal			= $this->input->post('languange_verbal');
					$languange_write			= $this->input->post('languange_write');

					foreach ($languange_id as $key => $value ) {
						if($languange[$key] == ''){
							continue;
						}
						$params_languange['employee_id']= $data['id'];
						$params_languange['id']			= $languange_id[$key];
						$params_languange['language']	= $languange[$key];
						$params_languange['verbal']		= $languange_verbal[$key];
						$params_languange['write']		= $languange_write[$key];
						$this->employee_language_model->save($params_languange);
					}

					$skill_id				= $this->input->post('skill_id');
					$skill					= $this->input->post('skill');
					$certificate_number		= $this->input->post('certificate_number');
					$skill_certificate		= $this->input->post('skill_certificate');

					$files = $_FILES;
					foreach ($skill_id as $key => $value ) {
						if($skill[$key] == ''){
							continue;
						}
						$params_skill['employee_id']		= $data['id'];
						$params_skill['id']					= $skill_id[$key];
						$params_skill['certificate_name']	= $skill[$key];
						$params_skill['certificate_number']	= $certificate_number[$key];

						$temp_skill_id	=	$this->employee_skill_model->save($params_skill);
						
						if($files['skill_certificate']['name'][$key] == ''){
							continue;
						}
						
						$_FILES['skill_certificate']['name']	= $files['skill_certificate']['name'][$key];
				        $_FILES['skill_certificate']['type']	= $files['skill_certificate']['type'][$key];
				        $_FILES['skill_certificate']['tmp_name']= $files['skill_certificate']['tmp_name'][$key];
				        $_FILES['skill_certificate']['error']	= $files['skill_certificate']['error'][$key];
				        $_FILES['skill_certificate']['size']	= $files['skill_certificate']['size'][$key];  

						$this->load->library('upload');
						$config['upload_path'] 	= $document_path;
						$config['overwrite']  	= TRUE;
						$config['allowed_types']= 'jpg|jpeg|png';
						$config['file_name'] 	= 'certificate-'.$temp_skill_id;
						$this->upload->initialize($config);

						if($this->upload->do_upload('skill_certificate')){
							$document = $this->upload->data();	
							$params_skill['certificate_document'] = base_url($url_document.'/'.$document['file_name']);
							$params_skill['id']	= $temp_skill_id;
							$this->employee_skill_model->save($params_skill);
						}
					}

					$history_id					= $this->input->post('history_id');
					$history_company			= $this->input->post('history_company');
					$history_company_position	= $this->input->post('history_company_position');
					$history_company_period		= $this->input->post('history_company_period');
					$history_company_salary		= $this->input->post('history_company_salary');
					$history_company_address	= $this->input->post('history_company_address');
					$history_company_resign_note= $this->input->post('history_company_resign_note');

					foreach ($history_id as $key => $value ) {
						if($history_company[$key] == ''){
							continue;
						}
						$params_history['employee_id']	= $data['id'];
						$params_history['id']			= $history_id[$key];
						$params_history['company_name']	= $history_company[$key];
						$params_history['position']		= $history_company_position[$key];
						$params_history['period']		= $history_company_period[$key];
						$params_history['salary']		= $history_company_salary[$key];
						$params_history['company_address']	= $history_company_address[$key];
						$params_history['resign_note']	= $history_company_resign_note[$key];
						$this->employee_history_model->save($params_history);
					}

					$reference_id		= $this->input->post('reference_id');
					$reference_name		= $this->input->post('reference_name');
					$reference_company	= $this->input->post('reference_company');
					$reference_position	= $this->input->post('reference_position');
					$reference_phone	= $this->input->post('reference_phone');

					foreach ($reference_id as $key => $value ) {
						if($reference_name[$key] == ''){
							continue;
						}
						$params_reference['employee_id']	= $data['id'];
						$params_reference['id']				= $reference_id[$key];
						$params_reference['name']			= $reference_name[$key];
						$params_reference['company_name']	= $reference_company[$key];
						$params_reference['position']		= $reference_position[$key];
						$params_reference['phone']			= $reference_phone[$key];
						$this->employee_reference_model->save($params_reference);
					}

					$data_candidate =  $this->candidate_model->get(array('id' => $data['id'], 'columns' => 'A.document_family_card, A.document_photo, A.document_id_card, A.document_certificate_education'));
					if($data_candidate->document_family_card != '' && $data_candidate->document_photo != ''&& $data_candidate->document_id_card != '' && $data_candidate->document_certificate_education != ''){
						$this->candidate_model->save(array('id' => $data['id'], 'status_completeness' => 1));
					}
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
					redirect(base_url('account/register'));
				}else{
					$data['message'] = message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger');
				}
			}
		}else{
			$result = (array) $this->candidate_model->get(array('id' => $data['id']));
			if(!empty($result)){
				$data = $result;
			}else{
				$data['email'] = $this->session->userdata('email');
			}
		}

		$data['date_birth'] = date("d/m/Y", strtotime($data['date_birth']));
		$data['list_education'] = $this->employee_education_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.institute, A.start, A.end'));
		$data['list_emergency'] = $this->employee_emergency_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.relation, A.phone, A.address'));
		$data['list_history'] 	= $this->employee_history_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.company_name, A.position, A.period,  A.salary, A.company_address, A.resign_note'));
		$data['list_language'] 	= $this->employee_language_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.language, A.verbal, A.write'));
		$data['list_reference'] = $this->employee_reference_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.name, A.company_name, A.position, A.phone'));
		$data['list_skill'] 	= $this->employee_skill_model->gets(array('employee_id' => $data['id'], 'columns' => 'A.id, A.certificate_name, A.certificate_number, A.certificate_document'));

		$data['_TITLE_']		= 'Form Pendaftaran';
		$data['_PAGE_']			= 'account/register';
		$data['_MENU_PARENT_']	= 'account';
		$data['_MENU_']			= 'register';
		$data['list_city'] 		= $this->city_model->gets(array());
		$data['list_job'] 		= $this->preference_model->gets(array());
		$this->view($data);
	}

	public function form()
	{
		$this->load->model(array('account_model', 'auth_model'));
		$data['password'] 	= '';
		$data['repassword'] = '';
		if($this->input->post('password')){
			$data['password'] 	= $this->input->post('password');
			$data['repassword'] = $this->input->post('repassword');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('repassword', 'Ulangi Password', 'required|matches[password]');
			if ($this->form_validation->run() == FALSE)
			{
				$data['message']= message_box('<strong>Gagal!</strong> Silahkan lengkapi data anda sesuai dengan ketentuan.','danger');
			}else{
				$params['id']		= $this->session->userdata('user_id');
				$params['password']	= generate_password($data['password']);

				$result	 			= $this->account_model->save($params);
				if ($result) {
					$data['message']= message_box('<strong>Sukses!</strong> perubahan password berhasil disimpan.','success');
				}else{
					$data['message']= message_box('<strong>Gagal!</strong> perubahan password gagal disimpan.','danger');
				}
			}
		}

		$data['_TITLE_'] 		= 'Pengaturan Akun';
		$data['_PAGE_'] 		= 'account/form';
		$data['_MENU_PARENT_'] 	= 'setting';
		$data['_MENU_'] 		= 'account';

		$this->view($data);
	}

	public function emergency_delete(){
		$this->load->model(array('employee_emergency_model'));
		$response['code'] 		= 500;
		$response['description']= "Data gagal dihapus";

		$result	 = $this->employee_emergency_model->delete(array('id' => $this->input->post('id')));
		if ($result) {
			$response['code'] 		= 200;
			$response['description']= "data berhasil dihapus.";
		}

		echo json_encode($response);
	}

	public function education_delete(){
		$this->load->model(array('employee_education_model'));
		$response['code'] 		= 500;
		$response['description']= "data gagal dihapus";

		$result	 = $this->employee_education_model->delete(array('id' => $this->input->post('id')));
		if ($result) {
			$response['code'] 		= 200;
			$response['description']= "data berhasil dihapus.";
		}

		echo json_encode($response);
	}

	public function languange_delete(){
		$this->load->model(array('employee_language_model'));
		$response['code'] 		= 500;
		$response['description']= "data gagal dihapus";

		$result	 		= $this->employee_language_model->delete(array('id' => $this->input->post('id')));
		if ($result) {
			$response['code'] 		= 200;
			$response['description']= "data berhasil dihapus.";
		}

		echo json_encode($response);
	}

	public function skill_delete(){
		
		$this->load->model(array('employee_skill_model'));
		$response['code'] 		= 500;
		$response['description']= "data gagal dihapus";
		$skill 	=	$this->employee_skill_model->get(array('id' => $this->input->post('id'), 'columns' => 'A.certificate_document'));
		$result	= $this->employee_skill_model->delete(array('id' => $this->input->post('id')));

		if ($result) {
			if($skill->certificate_document != ''){
				unlink(FCPATH.str_replace(base_url(), '', $skill->certificate_document));
			}
			$response['code'] 		= 200;
			$response['description']= "data berhasil dihapus.";
		}

		echo json_encode($response);
	}

	public function history_delete(){
		$this->load->model(array('employee_history_model'));
		$response['code'] 		= 500;
		$response['description']= "data gagal dihapus";

		$result	 		= $this->employee_history_model->delete(array('id' => $this->input->post('id')));
		if ($result) {
			$response['code'] 		= 200;
			$response['description']= "data berhasil dihapus.";
		}

		echo json_encode($response);
	}

	public function reference_delete(){
		$this->load->model(array('employee_reference_model'));
		$response['code'] 		= 500;
		$response['description']= "data gagal dihapus";

		$result	 = $this->employee_reference_model->delete(array('id' => $this->input->post('id')));
		if ($result) {
			$response['code'] 		= 200;
			$response['description']= "data berhasil dihapus.";
		}

		echo json_encode($response);
	}

	public function payroll(){
		$this->load->model(array('payroll_model', 'employee_model'));
		$data['_TITLE_'] 		= 'Slip Gaji';
		$data['_PAGE_'] 		= 'account/payroll';
		$data['_MENU_PARENT_'] 	= 'payroll';
		$data['_MENU_'] 		= 'payroll';
		$data['employee'] 		= $this->employee_model->get(array('id' => $this->session->userdata('employee_id'), 'columns' => 'A.status_completeness'));
		$params['status']			= 1;
		$params['status_approval'] 	= 3;
		$params['position_type']    = 'all';
		$params['employee_id'] 		= $this->session->userdata('employee_id');
		$params['columns'] 			= "A.id, B.full_name AS employee_name, B.id_card, DATE_FORMAT(A.periode_start, '%d/%m/%Y') AS periode_start, DATE_FORMAT(A.periode_end, '%d/%m/%Y') AS periode_end, A.salary, A.basic_salary, A.overtime_calculation, A.bpjs_ks, (A.bpjs_jht + A.bpjs_jp) AS bpjs_tk, A.tax_calculation, B.site_id";
		$params['payment']			= 'Selesai';
		$params['orderby'] 			= 'A.periode_end';
		$params['order'] 			= 'DESC';

		$data['payroll'] 		= $this->payroll_model->gets($params);
		if($data['payroll']){
		    if($data['payroll'][0]->site_id == 2){
			    $this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> maaf menu belum bisa diakses.','danger'));
			    redirect(base_url());
		    }
		}
		$this->view($data);
	}

	public function preview_payroll($payroll_id=FALSE)
	{
		$this->load->model(array('payroll_model', 'employee_model', 'position_model'));
		$data['payroll']		= $this->payroll_model->get(array('id' => $payroll_id, 'columns' => 'A.preview'));
		
		$data['_TITLE_'] 		= 'Preview Gaji Karyawan';
		$data['_PAGE_']	 		= 'ropayroll/preview';
		$data['_MENU_PARENT_'] 	= 'ropayroll';
		$data['_MENU_'] 		= 'ropayroll';
		$this->load->view('account/preview_payroll', $data);
	}
	public function pdf_payroll($payroll_id=FALSE)
	{
		$this->load->model(array('payroll_model', 'employee_model', 'position_model'));
		$payroll		= $this->payroll_model->get(array('id' => $payroll_id, 'columns' => 'A.id, A.employee_id, A.pdf, A.company_code'));
		if (!$payroll_id){
			$data["message"] = message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger');
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
			redirect(base_url('ropayroll/list'));
		}

		$data['content'] 	= $payroll->pdf; 
		$data['title'] 		= $payroll->employee_id.'_payroll';
		$data['basepath'] 	= FCPATH.'files/';
		$data['image']		= strtolower($payroll->company_code).'.jpg';
        $this->generate_PDF($data);
       	redirect(base_url('files/'.strtolower(str_replace(" ", "_",$data['title'])).'.pdf'));
	}
	private function generate_PDF($data = array())
	{	
		$this->load->library("Pdf");
		ob_start();
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// $pdf->SetProtection(array('print', 'copy'), $data['email']->pdf_password, null, 0, null);
		$pdf->SetHeaderData($data['image'], '181', '', null);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shelter');
		$pdf->SetTitle($data['title']);
		$pdf->SetSubject($data['title']);
		$pdf->SetKeywords('Shelter');

		// $pdf->setPrintHeader(true);
		// $pdf->setPrintFooter(false);

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$pdf->SetFont('times', '', 10);
		$pdf->AddPage();
		$pdf->writeHTML($data['content'], true, false, true, false, '');

		$base_path  = $data['basepath'];
		if(!is_dir($base_path)){
			mkdir($base_path, 0777, true);
		}
		return $pdf->Output($base_path.strtolower(str_replace(" ", "_",$data['title'])).'.pdf', 'F');
	}

	public function activity(){
		$data['_TITLE_'] 		= 'Catatan Pekerjaan';
		$data['_PAGE_'] 		= 'account/activity';
		$data['_MENU_PARENT_'] 	= 'account';
		$data['_MENU_'] 		= 'activity';

		$this->view($data);
	}

	public function activity_ajax(){
		$this->load->model(array('activity_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.employee_id, B.full_name AS employee_name, C.name AS branch_name, D.code AS company_code, E.name AS position_name, DATE_FORMAT(A.date, "%d/%m/%Y") AS date, A.time_start, A.time_finish, A.activity, A.result';
		// $params['columns'] 		= 'A.employee_id, DATE_FORMAT(A.date, "%d/%m/%Y") AS date, A.time_start, A.time_finish, A.activity, A.result';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			= $_POST['start'];
		$params['employee_id']	= $this->session->userdata('employee_id');
		
		$params['employee_name']= $_POST['columns'][0]['search']['value'];
		$params['position_name']= $_POST['columns'][1]['search']['value'];
		$params['branch_name']	= $_POST['columns'][2]['search']['value'];
		$params['company_code']	= $_POST['columns'][3]['search']['value'];
		$params['date']			= $_POST['columns'][4]['search']['value'];
		$params['time_start']	= $_POST['columns'][5]['search']['value'];
		$params['time_finish']	= $_POST['columns'][6]['search']['value'];
		$params['activity']		= $_POST['columns'][7]['search']['value'];
		$params['result']		= $_POST['columns'][8]['search']['value'];
		
		$list 	= $this->activity_model->gets_employee($params);
		$total 	= $this->activity_model->gets_employee($params, TRUE);
		
		$data 	= array();
		foreach($list as $item)
		{
			$result['employee_name']= $item->employee_name;
			$result['position_name']= $item->position_name;
			$result['branch_name'] 	= $item->branch_name;
			$result['company_code'] = $item->company_code;
			$result['date'] 		= $item->date;
			$result['time_start'] 	= $item->time_start;
			$result['time_finish']	= $item->time_finish;
			$result['activity']		= $item->activity;
			$result['result']		= $item->result;
			array_push($data, $result);
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function activity_form()
	{
		$this->load->model(array('activity_model'));

		$data['employee_id']		= $this->session->userdata('employee_id');
		$data['date']				= '';
		$data['time_start'] 		= '';
		$data['time_finish'] 		= '';
		$data['activity'] 			= '';
		$data['result'] 			= '';

		if($this->input->post()){
			$data['date'] 				= $this->input->post('date');
			$data['time_start'] 		= $this->input->post('time_start');
			$data['time_finish'] 		= $this->input->post('time_finish');
			$data['activity'] 			= $this->input->post('activity');
			$data['result'] 			= $this->input->post('result');
			
			$this->form_validation->set_rules('date', '', 'required');
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$save_id	 	= $this->activity_model->save($data);
				
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('account/activity'));
		}

		$data['_TITLE_'] 		= 'Catatan Aktivitas';
		$data['_PAGE_'] 		= 'account/activity_form';
		$data['_MENU_PARENT_'] 	= 'account';
		$data['_MENU_'] 		= 'activity';
		return $this->view($data);
	}

	public function target(){
		$this->load->model(array('evaluation_target_model'));
		$data['_TITLE_'] 		= 'Sasaran Mutu';
		$data['_PAGE_'] 		= 'account/target';
		$data['_MENU_PARENT_'] 	= 'account';
		$data['_MENU_'] 		= 'personal_target';
		$data['name']			= '';
		$data['description']	= '';
		
		$target 				= $this->evaluation_target_model->get(array('id' => 1, 'columns' => 'A.name, A.description'));
		if($target){
			$data['name']		= $target->name;
			$data['description']= $target->description;
		}
		$this->view($data);
	}

	public function jobdesc(){
		$this->load->model(array('job_description_model'));
		$data['_TITLE_'] 		= 'Sasaran Mutu';
		$data['_PAGE_'] 		= 'account/jobdesc';
		$data['_MENU_PARENT_'] 	= 'account';
		$data['_MENU_'] 		= 'personal_jobdesc';
		$data['description']	= '';
		$employee_id 			= '';
		if($this->session->userdata('employee_id') != ''){
			$employee_id 	= $this->session->userdata('employee_id');
		}
		$jobdesc 	= $this->job_description_model->gets(array('employee_id' => $employee_id, 'columns' => 'C.description'));
		if($jobdesc){
			$data['description']	= $jobdesc[0]->description;
		}
		$this->view($data);
	}
}
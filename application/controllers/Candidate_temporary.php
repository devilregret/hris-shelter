<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Candidate_temporary extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('recruitment'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function courier($type = 'all', $branch = FALSE){
		$this->load->model(array('branch_model'));
		$data['_TITLE_'] 		= 'Daftar Pelamar Kurir';
		$data['_PAGE_'] 		= 'candidate_temporary/courier';
		$data['_MENU_PARENT_'] 	= 'recruitment';
		$data['_MENU_'] 		= 'courier';

		$data['type'] 			= $type;
		if($branch){
			$data['branch'] 	= $branch;
		}else{
			$data['branch'] 	= $this->session->userdata('branch');
		}
		
		$data['list_branch'] 	= $this->branch_model->gets(array('columns' => 'A.id, A.name'));
		$this->view($data);
	}

	public function courier_ajax($type = 'all', $branch = 1){

		$this->load->model(array('candidate_temporary_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.id_card, A.full_name, A.phone_number, A.address, A.driver_license, A.vaccine_covid, B.name AS city_name, A.followup_status, A.followup_note, DATE_FORMAT(A.followup_date, '%d/%m/%Y') AS followup_date, DATE_FORMAT(A.created_at, '%d/%m/%Y') AS created_at ";
		$params['orderby'] 			= $_POST['columns'][$column_index]['data'];
		$params['order']			= $_POST['order'][0]['dir'];
		$params['limit']			= $_POST['length'];
		$params['page']				=  $_POST['start'];
		$params['type']				=  'kurir';

		$params['created_at']		= $_POST['columns'][1]['search']['value'];
		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['id_card']			= $_POST['columns'][3]['search']['value'];
		$params['driver_license']	= $_POST['columns'][4]['search']['value'];
		$params['phone_number']		= $_POST['columns'][5]['search']['value'];
		$params['address']			= $_POST['columns'][6]['search']['value'];
		$params['city_name']		= $_POST['columns'][7]['search']['value'];

		if($type == 'lihat'){
			$params['followup_status'] 	= 'lihat';
		}else if($type == 'hubungi'){
			$params['followup_status'] 	= 'hubungi';
		}else if($type == 'proses'){
			$params['followup_status'] 	= 'proses';
		}else if($type == 'tolak'){
			$params['followup_status'] 	= 'tolak';
		}else if($type == 'blokir'){
			$params['followup_status'] 	= 'blokir';
		}

		if($branch > 1){
			$params['branch_id']	= $branch;
		}
		
		$list 	= $this->candidate_temporary_model->gets($params);
		$total 	= $this->candidate_temporary_model->gets($params, TRUE);
		
		$data 	= array();

		foreach($list as $item)
		{	

			$result['id'] 				= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';

			$result['created_at']			= $item->created_at;
			$result['full_name']			= $item->full_name;
			$result['id_card']				= $item->id_card;
			$result['driver_license']		= $item->driver_license;
			$list_number 					= $item->phone_number;
			$phone_number					= "";
			if($list_number != ""){
				$phone_number = explode ("/", $list_number);
				$phone_number = preg_replace("/[^0-9]/", "", $phone_number[0]);
				$start = substr($phone_number, 0, 1 );
				if($start == '0'){
					$phone_number = '62'.substr($phone_number, 1,strlen($phone_number));
				}else if($start == '+'){
					$phone_number = substr($phone_number, 1,strlen($phone_number));
				}
				$phone_number = '<a href="http://wa.me/'.$phone_number.'" target="_blank">'.$item->phone_number.'</a>';
			}
			$result['phone_number']			= $phone_number;
			$result['address']				= $item->address; 
			$result['city_name']			= $item->city_name; 
			$result['followup_status']		= $item->followup_status;
			$result['followup_note']		= $item->followup_note;
			if($item->followup_status != ''){
				$result['followup_status'] = $result['followup_status'].'<br>'.$item->followup_date;
			}

			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("candidate_temporary/preview/".$item->id).'">Lihat</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("candidate_temporary/delete/courier/".$item->id).'">Hapus</a>';
			array_push($data, $result);
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function casual($type = 'all', $branch = FALSE){
		$this->load->model(array('branch_model'));
		$data['_TITLE_'] 		= 'Daftar Pelamar casual';
		$data['_PAGE_'] 		= 'candidate_temporary/casual';
		$data['_MENU_PARENT_'] 	= 'recruitment';
		$data['_MENU_'] 		= 'casual';

		$data['type'] 			= $type;
		if($branch){
			$data['branch'] 	= $branch;
		}else{
			$data['branch'] 	= $this->session->userdata('branch');
		}
		
		$data['list_branch'] 	= $this->branch_model->gets(array('columns' => 'A.id, A.name'));
		$this->view($data);
	}

	public function casual_ajax($type = 'all', $branch = 1){

		$this->load->model(array('candidate_temporary_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= "A.id, A.id_card, A.full_name, A.phone_number, A.address, A.driver_license, A.vaccine_covid, B.name AS city_name, A.followup_status, A.followup_note, DATE_FORMAT(A.followup_date, '%d/%m/%Y') AS followup_date, DATE_FORMAT(A.created_at, '%d/%m/%Y') AS created_at ";
		$params['orderby'] 			= $_POST['columns'][$column_index]['data'];
		$params['order']			= $_POST['order'][0]['dir'];
		$params['limit']			= $_POST['length'];
		$params['page']				=  $_POST['start'];
		$params['type']				=  'Casual';

		$params['created_at']		= $_POST['columns'][1]['search']['value'];
		$params['full_name']		= $_POST['columns'][2]['search']['value'];
		$params['id_card']			= $_POST['columns'][3]['search']['value'];
		$params['vaccine_covid']	= $_POST['columns'][4]['search']['value'];
		$params['phone_number']		= $_POST['columns'][5]['search']['value'];
		$params['address']			= $_POST['columns'][6]['search']['value'];
		$params['city_name']		= $_POST['columns'][7]['search']['value'];

		if($type == 'lihat'){
			$params['followup_status'] 	= 'lihat';
		}else if($type == 'hubungi'){
			$params['followup_status'] 	= 'hubungi';
		}else if($type == 'proses'){
			$params['followup_status'] 	= 'proses';
		}else if($type == 'tolak'){
			$params['followup_status'] 	= 'tolak';
		}else if($type == 'blokir'){
			$params['followup_status'] 	= 'blokir';
		}

		if($branch > 1){
			$params['branch_id']	= $branch;
		}
		
		$list 	= $this->candidate_temporary_model->gets($params);
		$total 	= $this->candidate_temporary_model->gets($params, TRUE);
		
		$data 	= array();

		foreach($list as $item)
		{	

			$result['id'] 				= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';

			$result['created_at']			= $item->created_at;
			$result['full_name']			= $item->full_name;
			$result['id_card']				= $item->id_card;
			$result['vaccine_covid']		= $item->vaccine_covid;
			$list_number 					= $item->phone_number;
			$phone_number					= "";
			if($list_number != ""){
				$phone_number = explode ("/", $list_number);
				$phone_number = preg_replace("/[^0-9]/", "", $phone_number[0]);
				$start = substr($phone_number, 0, 1 );
				if($start == '0'){
					$phone_number = '62'.substr($phone_number, 1,strlen($phone_number));
				}else if($start == '+'){
					$phone_number = substr($phone_number, 1,strlen($phone_number));
				}
				$phone_number = '<a href="http://wa.me/'.$phone_number.'" target="_blank">'.$item->phone_number.'</a>';
			}
			$result['phone_number']			= $phone_number;
			$result['address']				= $item->address; 
			$result['city_name']			= $item->city_name; 
			$result['followup_status']		= $item->followup_status;
			$result['followup_note']		= $item->followup_note;
			if($item->followup_status != ''){
				$result['followup_status'] = $result['followup_status'].'<br>'.$item->followup_date;
			}

			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block" style="cursor:pointer;" data-href="'. base_url("candidate_temporary/preview/".$item->id).'">Lihat</a>
				<a onclick="confirm_del(this)" class="btn-sm btn-danger btn-block" style="cursor:pointer;" data-href="'.base_url("candidate_temporary/delete/casual/".$item->id).'">Hapus</a>';
			array_push($data, $result);
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function delete($menu = 'courier', $candidate_id = false)
	{
		$this->load->model('candidate_temporary_model');
		if ($candidate_id)
		{
			$data =  $this->candidate_temporary_model->get(array('id' => $candidate_id, 'status' => 0, 'columns' => 'A.id'));

			if ($data)
			{
				$insert = array('id' => $candidate_id, 'is_active' => 0);
				$result = $this->candidate_temporary_model->save($insert);
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> data berhasil dihapus.','success'));
					redirect(base_url('candidate_temporary/'.$menu));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> sata gagal dihapus.','danger'));
					redirect(base_url('candidate_temporary/'.$menu));
				}
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('candidate_temporary/'.$menu));
			}
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
			redirect(base_url('candidate_temporary/'.$menu));
		}
	}

	public function preview($candidate_id=FALSE)
	{
		$this->load->model(array('candidate_temporary_model'));
		$data['_TITLE_'] 		= 'Preview Pelamar';
		$data['_PAGE_']	 		= 'candidate_temporary/preview';
		$data['_MENU_PARENT_'] 	= 'recruitment';
		$data['_MENU_'] 		= 'candidate';

		$data['id'] = $candidate_id;

		$data['preview'] = $this->candidate_temporary_model->preview(array('id' => $candidate_id, 'columns' => "A.id, A.id_card, A.full_name, A.phone_number, A.address, A.driver_license, A.vaccine_covid, B.name AS city_name, A.followup_status, A.followup_note, DATE_FORMAT(A.followup_date, '%d/%m/%Y') AS followup_date, DATE_FORMAT(A.created_at, '%d/%m/%Y') AS created_at "));
		if($data['preview']->followup_status == '' 
			&& $data['preview']->followup_note == ''){
			$this->candidate_temporary_model->save(array('id' => $candidate_id, 'followup_status' => 'Lihat', 'followup_note' => 'Detail Pelamar Sudah Dilihat', 'followup_date' => date('Y-m-d H:i:s')));
		}
		$this->load->view('candidate_temporary/preview', $data);
	}

	public function send($menu = '', $type = 'all', $branch = 1){
		if($this->input->post()){
			$this->load->model(array('wappin_model'));
			$config_id = 1;
			$wappin 		= $this->wappin_model->get(array('id' => $config_id, 'columns' => 'A.access_token, A.base_url'));
			if($wappin){
				$message['token'] 	= "Authorization: Bearer ".$wappin->access_token;
				$message['base_url']= $wappin->base_url;
			}else{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Melakukan Koneksi API.','danger'));
				redirect(base_url('candidate_temporary/'.$menu.'/'.$type.'/'.$branch));
				die();
			}
				
			if($_FILES['message-image']['name'] != ""){
				$image_path = FCPATH.'files/whatsapp/';
				$config['upload_path'] 	= $image_path;
				$config['overwrite']  	= TRUE;
				$config['allowed_types']= 'jpg|jpeg|png';
				$config['file_name'] 	= $this->session->userdata('user_id');

				$this->load->library('upload', $config, 'image');
				if (!$this->image->do_upload('message-image')) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> '.$this->image->display_errors().'.','danger'));
					redirect(base_url('candidate_temporary/'.$menu.'/'.$type.'/'.$branch));
					die();
				}

				$message['image']		= base_url('files/whatsapp/'.$this->image->data()['file_name']);
				$message['message']		= $this->input->post('message-personal');
			}
			$send_success = 0;
			$send_failed = 0;

			$this->load->model(array('candidate_temporary_model'));
			$id 		= $this->input->post('id');
			foreach ($id as $employee_id) {
				$employee 	= $this->candidate_temporary_model->get(array('id' => $employee_id, 'columns' => 'A.phone_number, A.full_name, A.followup_status, A.followup_note'));
				if($employee){
					if($employee->phone_number != ''){
						$phone_number = explode ("/", $employee->phone_number);
						$employee->phone_number = preg_replace("/[^0-9]/", "", $phone_number[0]);
						$start = substr($employee->phone_number, 0, 1 );
						if($start == '0'){
							$employee->phone_number = '62'.substr($employee->phone_number, 1,strlen($employee->phone_number));
						}else if($start == '+'){
							$employee->phone_number = substr($employee->phone_number, 1,strlen($employee->phone_number));
						}
						$message['phone_number'] = $employee->phone_number;
						$message['name'] = $employee->full_name;
						$send = $this->send_media($message);
						if($send){
							$send_success = $send_success+1;
							if($employee->followup_status == '' 
								&& $employee->followup_note == ''){
								$this->candidate_temporary_model->save(array('id' => $employee_id, 'followup_status' => 'Hubungi', 'followup_note' => 'Dihubungi Melalui Brodcast whatsapp', 'followup_date' => date('Y-m-d H:i:s')));
								}
						}else{
							$send_failed = $send_failed+1;
						}
					}else{
						$send_failed = $send_failed+1;
					}
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> '.$send_success.' Pesan berhasil dikirim, '.$send_failed.' Pesan Gagal Dikirim','success'));
				redirect(base_url('candidate_temporary/'.$menu.'/'.$type.'/'.$branch));
		}else{
			redirect(base_url());
		}
	}

	public function followup($menu = '', $type = 'all', $branch = 1){

		$note	= $this->input->post('note_followup');
		$status	= $this->input->post('status_followup');
		foreach ($this->input->post('id') as $candidate_id)
		{
			$this->load->model(array('candidate_temporary_model'));

			$this->candidate_temporary_model->save(array('id' => $candidate_id, 'followup_status' => $status, 'followup_note' => $note, 'followup_date' => date('Y-m-d H:i:s')));
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Update followup.','success'));
		redirect(base_url('candidate_temporary/'.$menu.'/'.$type.'/'.$branch));
	}

	private function send_media($data = array()){
		$url 		= $data['base_url'].'/v1/messages';
		$request 	= array(
			'to' => $data['phone_number'],
			'type'	=> 'template',
			'template' => array(
				'name'	=> $this->config->item('global_template_name'),
				'namespace' => $this->config->item('global_template_namespace'),
				'language'	=> array(
					'policy' => 'deterministic',
					'code'	=> 'id'
				),
				'components' => array(
					array(
						'type'		=> 'header',
						'parameters'=> array(
							array(
								'type' => 'image',
								'image'	=> array(
									'link' => $data['image']
								)
							)
						)
					),
					array(
						'type' => 'body',
						'parameters'=> array(
							array(
								'type' => 'text',
								'text'	=> $data['name']
							),
							array(
								'type' => 'text',
								'text'	=> $data['message']
							)
						)
					)
				)
			),
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $data['token']));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
		$response = curl_exec($ch);
		$result = json_decode($response, true);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if($httpcode == 200){
			return true;
		}else{
			return false;
		}
	}
}
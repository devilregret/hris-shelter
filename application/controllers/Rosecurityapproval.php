<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rosecurityapproval extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		$role 	= array_merge($role, $this->config->item('relation_officer_security'));
		if(!in_array($this->session->userdata('role'), $role)){
			redirect(base_url());
		}
	}

	public function resign_submit(){
		foreach ($this->input->post('id') as $employee_id)
		{
			$this->load->model(array('employee_model', 'approval_model'));
			$data = (array) $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.site_id, A.company_id, A.position_id'));
			if (empty($data)){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('rosecurityapproval/resign'));
			}else{
				$insert = array('id' => $employee_id, 'status_approval' => 3);
				$result = $this->employee_model->save($insert);
				$approval = array('id' => '', 'employee_id' => $employee_id , 'site_id' => $data['site_id'], 'company_id' => $data['company_id'], 'position_id' => $data['position_id'], 'note' => 'Resign Dibatalkan','status_approval' => 'Resign Dibatalkan');
				$this->approval_model->save($approval);
				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pembatalan gagal.','danger'));
					redirect(base_url('rosecurityapproval/resign'));
				}
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Pembatalan Sukses.','success'));
		redirect(base_url('rosecurityapproval/resign'));
	}

	public function resign(){
		$this->load->model(array('site_model'));
		$data['_TITLE_'] 		= 'Pengajuan Resign';
		$data['_PAGE_'] 		= 'Rosecurityapproval/resign';
		$data['_MENU_PARENT_'] 	= 'ro_security';
		$data['_MENU_'] 		= 'rosecurityresign';
		$this->view($data);
	}

	public function resign_ajax(){
		$this->load->model(array('roresign_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.employee_number, A.id_card, A.full_name, A.document_resign, A.resign_note, A.resign_burden, A.resign_type, DATE_FORMAT(A.resign_submit, "%d/%m/%Y") AS resign_submit';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']		= 1;
		$params['position_type']= 'Security';
		
		$params['status_approval'] 		= 4;
		$params['employee_number']		= $_POST['columns'][2]['search']['value'];
		$params['id_card']				= $_POST['columns'][3]['search']['value'];
		$params['full_name']			= $_POST['columns'][4]['search']['value'];
		$params['resign_submit']		= $_POST['columns'][5]['search']['value'];
		$params['resign_type']			= $_POST['columns'][6]['search']['value'];
		$params['resign_note']			= $_POST['columns'][7]['search']['value'];
		$params['resign_burden']		= $_POST['columns'][8]['search']['value'];

		$list 	= $this->roresign_model->gets($params);
		$total 	= $this->roresign_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$document_resign				= '';
			if(isset($item->document_resign)){
				$document_resign = '<a href="'.$item->document_resign.'" target="_blank">Dokumen</a>';
			}

			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['employee_number']		= $item->employee_number;
			$result['id_card']				= $item->id_card;
			$result['full_name']			= $item->full_name;
			$result['resign_submit']		= $item->resign_submit;
			$result['resign_type']			= $item->resign_type;
			$result['resign_note']			= $item->resign_note;
			$result['resign_burden']		= $item->resign_burden;
			$result['document_resign']		= $document_resign;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("rosecurity/employee_preview/".$item->id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("approval/approval_history/".$item->id).'">Riwayat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function nonjob_submit(){
		foreach ($this->input->post('id') as $employee_id)
		{
			$this->load->model(array('employee_model', 'approval_model'));
			$data = (array) $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.site_id, A.company_id, A.position_id'));
			if (empty($data)){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('rosecurityapproval/nonjob'));
			}else{
				$insert = array('id' => $employee_id, 'status_approval' => 3);
				$result = $this->employee_model->save($insert);
				$approval = array('id' => '', 'employee_id' => $employee_id , 'site_id' => $data['site_id'], 'company_id' => $data['company_id'], 'position_id' => $data['position_id'], 'note' => 'Nonjob Dibatalkan','status_approval' => 'Nonjob Dibatalkan');
				$this->approval_model->save($approval);

				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pembatalan gagal.','danger'));
					redirect(base_url('rosecurityapproval/nonjob'));
				}
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Pembatalan Sukses.','success'));
		redirect(base_url('rosecurityapproval/nonjob'));
	}

	public function nonjob(){
		$this->load->model(array('site_model'));
		$data['_TITLE_'] 		= 'Pengajuan Nonjob';
		$data['_PAGE_'] 		= 'Rosecurityapproval/nonjob';
		$data['_MENU_PARENT_'] 	= 'ro_security';
		$data['_MENU_'] 		= 'rosecuritynonjob';
		$this->view($data);
	}

	public function nonjob_ajax(){
		$this->load->model(array('roresign_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.employee_number, A.id_card, A.full_name, A.document_resign, A.resign_note, A.resign_burden, A.resign_type, DATE_FORMAT(A.resign_submit, "%d/%m/%Y") AS resign_submit';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']		= 1;
		$params['position_type']= 'Security';
		
		$params['status_approval'] 		= 5;
		$params['employee_number']		= $_POST['columns'][2]['search']['value'];
		$params['id_card']				= $_POST['columns'][3]['search']['value'];
		$params['full_name']			= $_POST['columns'][4]['search']['value'];
		$params['resign_submit']		= $_POST['columns'][5]['search']['value'];
		$params['resign_type']			= $_POST['columns'][6]['search']['value'];
		$params['resign_note']			= $_POST['columns'][7]['search']['value'];
		$params['resign_burden']		= $_POST['columns'][8]['search']['value'];

		$list 	= $this->roresign_model->gets($params);
		$total 	= $this->roresign_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{

			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['employee_number']		= $item->employee_number;
			$result['id_card']				= $item->id_card;
			$result['full_name']			= $item->full_name;
			$result['resign_submit']		= $item->resign_submit;
			$result['resign_type']			= $item->resign_type;
			$result['resign_note']			= $item->resign_note;
			$result['resign_burden']		= $item->resign_burden;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("rosecurity/employee_preview/".$item->id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("approval/approval_history/".$item->id).'">Riwayat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}


	public function mutation_submit(){
		foreach ($this->input->post('id') as $employee_id)
		{
			$this->load->model(array('employee_model', 'approval_model'));
			$data = (array) $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.site_id, A.company_id, A.position_id'));
			if (empty($data)){
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
				redirect(base_url('rosecurityapproval/mutation'));
			}else{
				$insert = array('id' => $employee_id, 'status_approval' => 3, 'new_site_id' => 0);
				$result = $this->employee_model->save($insert);
				$approval = array('id' => '', 'employee_id' => $employee_id , 'site_id' => $data['site_id'], 'company_id' => $data['company_id'], 'position_id' => $data['position_id'], 'note' => 'Mutasi Dibatalkan','status_approval' => 'Mutasi Dibatalkan');
				$this->approval_model->save($approval);

				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Pembatalan gagal.','danger'));
					redirect(base_url('rosecurityapproval/mutation'));
				}
			}
		}
		$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Pembatalan Sukses.','success'));
		redirect(base_url('rosecurityapproval/mutation'));
	}

	public function mutation(){
		$this->load->model(array('site_model'));
		$data['_TITLE_'] 		= 'Pengajuan Nonjob';
		$data['_PAGE_'] 		= 'Rosecurityapproval/mutation';
		$data['_MENU_PARENT_'] 	= 'ro_security';
		$data['_MENU_'] 		= 'rosecuritymutation';
		$this->view($data);
	}

	public function mutation_ajax(){
		$this->load->model(array('roresign_model'));

		$column_index = $_POST['order'][0]['column']; 

		$params['columns'] 		= 'A.id, A.employee_number, A.id_card, A.full_name, A.document_resign, A.resign_note, A.resign_burden, A.resign_type, DATE_FORMAT(A.resign_submit, "%d/%m/%Y") AS resign_submit';
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		$params['status']		= 1;
		$params['position_type']= 'Security';
		
		$params['status_approval'] 		= 6;
		$params['employee_number']		= $_POST['columns'][2]['search']['value'];
		$params['id_card']				= $_POST['columns'][3]['search']['value'];
		$params['full_name']			= $_POST['columns'][4]['search']['value'];
		$params['resign_submit']		= $_POST['columns'][5]['search']['value'];
		$params['resign_type']			= $_POST['columns'][6]['search']['value'];
		$params['resign_note']			= $_POST['columns'][7]['search']['value'];
		$params['resign_burden']		= $_POST['columns'][8]['search']['value'];

		$list 	= $this->roresign_model->gets($params);
		$total 	= $this->roresign_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{

			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['employee_number']		= $item->employee_number;
			$result['id_card']				= $item->id_card;
			$result['full_name']			= $item->full_name;
			$result['resign_submit']		= $item->resign_submit;
			$result['resign_type']			= $item->resign_type;
			$result['resign_note']			= $item->resign_note;
			$result['resign_burden']		= $item->resign_burden;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("rosecurity/employee_preview/".$item->id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("approval/approval_history/".$item->id).'">Riwayat</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}
}

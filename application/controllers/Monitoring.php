<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Monitoring extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$role 	= array();
		$role 	= array_merge($role, $this->config->item('administrator'));
		$role 	= array_merge($role, $this->config->item('personalia'));
		if(!in_array($this->session->userdata('role'), $role)){
		// if(!in_array($this->session->userdata('role'), array(2,52,5,6))){
			redirect(base_url());
		}
	}
	
	public function import(){

		$this->load->model(array('employee_model', 'employee_contract_model'));
		if($_FILES['file']['name'] != ""){
			$this->load->library("Excel");
			$excelreader 	= new PHPExcel_Reader_Excel2007();
			$loadexcel		= $excelreader->load($_FILES['file']['tmp_name']);
			$sheet  		= $loadexcel->getActiveSheet(0);
			
			$rend			= $sheet->getHighestRow();
			
			for ($row = 2; $row <= $rend; $row++) {
				$id_card 		= replace_null($sheet->getCell('B'.$row)->getValue());
				$type 			= replace_null($sheet->getCell('C'.$row)->getValue());
				$employee 		= $this->employee_model->get(array('id_card' => $id_card, 'columns' => 'A.id'));
				if($id_card == ""){
					continue;
				}
				if($employee){

					$start_date 	= $sheet->getCell('D'.$row)->getValue();
					if(!is_numeric($start_date)){
						continue;
					}
					$unix_date 		= ($start_date - 25569) * 86400;
					$start_date 	= 25569 + ($unix_date / 86400);
					$unix_date 		= ($start_date - 25569) * 86400;
					$contract_start	= gmdate("Y-m-d", $unix_date);

					$end_date 		= $sheet->getCell('F'.$row)->getValue();
					if(!is_numeric($end_date)){
						continue;
					}
					$unix_date 		= ($end_date - 25569) * 86400;
					$end_date 		= 25569 + ($unix_date / 86400);
					$unix_date 		= ($end_date - 25569) * 86400;
					$contract_end	= gmdate("Y-m-d", $unix_date);

					$end 		= strtotime($contract_end);
					$start 		= strtotime($contract_start);
					$datediff 	= $end - $start;

					$diff 		= round($datediff / (60 * 60 * 24));
					
					if($diff < 0){
						continue;
					}

					$contract_type = 'PKWT';
					if (strtoupper($type) == 'PKHL') {
						$contract_type = 'PKHL';
					}

					$data_contract['employee_id'] 	= $employee->id;
					$data_contract['contract'] 		= '';
					$data_contract['contract_type'] = $contract_type;
					$data_contract['contract_start']= $contract_start;
					$data_contract['contract_end'] 	= $contract_end;
					$data_contract['contract_duration'] = $diff;
					$data_contract['contract_value'] = 0;

					$contract_exist  = $this->employee_contract_model->get(array('employee_id' => $employee->id, 'columns' => 'A.id'));
					if($contract_exist){
						$data_contract['id'] = $contract_exist->id;
					}
					$this->employee_contract_model->save($data_contract);
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
			redirect(base_url('monitoring/list/direct'));
		}
	}

	public function adendum()
	{
		if($this->input->post()){
			$this->load->model(array('employee_model', 'contract_model', 'employee_contract_model', 'employee_salary_model', 'city_model'));
		
			$ids 		= $this->input->post('id');
			$template_id= $this->input->post('template_id');
			$duration	= str_replace(" ", "", $this->input->post('duration'));
			$template 	= $this->contract_model->get(array('id' => $template_id, 'columns' => 'A.type, A.content'));
			$contract_type 		= '';
			$content 	= '';
			if($template){
				$contract_type 	= $template->type;
				$content 	= $template->content;
			}
			
			$insert['contract_type'] 	= $contract_type;
			$insert['contract_start'] 	= substr($duration, 6, 4).'-'.substr($duration, 3, 2).'-'.substr($duration, 0, 2);
			$insert['contract_end'] 	= substr($duration, 17, 4).'-'.substr($duration, 14, 2).'-'.substr($duration, 11, 2);
				
			$content = str_replace("{KONTRAK_MULAI}", $insert['contract_start'], $content);
			$content = str_replace("{KONTRAK_SELESAI}", $insert['contract_end'], $content);
			foreach ($ids as $employee_id) {
				$employee 	= $this->employee_model->get_detail(array('id' => $employee_id, 'columns' => 'A.full_name, A.employee_number, A.id_card, A.gender, A.date_birth, A.marital_status, A.city_card_id, A.site_id, A.address_card, B.code AS site_code, B.name AS site_name, B.address AS site_address, C.code AS company_code, C.name AS company_name, C.name AS company_address, D.name AS position, E.name AS city_birth'));
				$type 		= 'indirect';
				if($employee->site_id != '2'){
					$type 	= 'direct';
				}
				$month 		= substr($params['duration'], 3, 2);
				$nomor_pkwt = $employee->employee_number.'/'.$template->type.'-'.$type.'/HRD-'.$employee->company_code.'/'.$employee->site_code.'/'.get_roman_month($month).'/'.substr($params['duration'], 6, 4);
			
				$tmp = $content;
				$tmp = str_replace("{NOMOR_KONTRAK}", $nomor_pkwt, $tmp);
				$tmp = str_replace("{NOMOR_KARYAWAN}", $employee->employee_number, $tmp);
				$tmp = str_replace("{NAMA_LENGKAP}",$employee->full_name,$tmp);
				$tmp = str_replace("{NIK}", $employee->id_card, $tmp);

				$birth_date	= substr($employee->date_birth, 8, 2)." ".get_month(substr($employee->date_birth, 5, 2))." ".substr($employee->date_birth, 0, 4);
				$birth_place = str_replace("KOTA", "", strtoupper($employee->city_birth));
				$birth_place = str_replace("KABUPATEN", "", strtoupper($employee->city_birth));
				$tmp = str_replace("{TANGGAL_LAHIR}", $birth_date, $tmp);
				$tmp = str_replace("{TEMPAT_LAHIR}",$birth_place,$tmp);
				$tmp = str_replace("{JENIS_KELAMIN}",$employee->gender,$tmp);
				$tmp = str_replace("{STATUS_PERNIKAHAN}",$employee->marital_status,$tmp);

				$address_card	= $employee->address_card;
				$tmp = str_replace("{ALAMAT_KTP}",$address_card,$tmp);
				$tmp = str_replace("{JABATAN}",$employee->position,$tmp);
				$tmp = str_replace("{NAMA_SITE}",$employee->site_name,$tmp);
				$tmp = str_replace("{ALAMAT_SITE}",$employee->site_address,$tmp);
				$tmp = str_replace("{NAMA_BISNIS_UNIT}",$employee->company_name,$tmp);
				$tmp = str_replace("{ALAMAT_BISNIS_UNIT}",$employee->company_address,$tmp);

				$manager = $this->employee_model->get(array('position_id' => 628, 'site_id' => 2, 'columns' => 'A.full_name'));
				if($manager){
					$tmp = str_replace("{MANAGER_HRD}",$manager->full_name,$tmp);
				}
				$salary = $this->employee_salary_model->get(array('id' => $employee_id, 'columns' => 'B.basic_salary'));
				if($salary->basic_salary != ''){
					$tmp = str_replace("{GAJI POKOK}",format_rupiah($basic_salary),$tmp);
				}
				$insert['employee_id'] = $employee_id;
				$insert['contract'] 	= $tmp;
				$result =  $this->employee_contract_model->save($insert);
				if (!$result) {
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal disimpan.','danger'));
					redirect(base_url('monitoring/list/'.$type));
				}
			}
			$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil disimpan.','success'));
			redirect(base_url('monitoring/list/'.$type));
		}
	}

	public function work($employee_id = FALSE){
		$this->load->model(array('work_model', 'monitoring_model'));
		$employee = $this->monitoring_model->get(array('id' => $employee_id, 'columns' => 'A.company_id, A.full_name, A.employee_number, B.name AS position, C.name AS site_name, D.code AS company_code'));
		$template = $this->work_model->get(array('company_id' => $employee->company_id, 'columns' => 'A.content'));
		$period = $this->monitoring_model->get_work_period(array('id' => $employee_id));


		$data['content'] 	= $template->content;
		$data['content']	= str_replace("{NAMA_LENGKAP}", $employee->full_name, $data['content']);
		$data['content']	= str_replace("{NOMOR_KARYAWAN}", $employee->employee_number, $data['content']);
		$data['content']	= str_replace("{JABATAN}", $employee->position, $data['content']);
		$data['content']	= str_replace("{NAMA_SITE}", $employee->site_name, $data['content']);
		$data['content']	= str_replace("{TANGGAL_SEKARANG}", format_date_ina(date('Y-m-d')), $data['content']);
		$data['content']	= str_replace("{MULAI_KERJA}", format_date_ina($period->contract_start), $data['content']);
		$data['content']	= str_replace("{AKHIR_KERJA}", format_date_ina($period->contract_end), $data['content']);

		$letter_number = $employee->employee_number.'/KKS/'.$employee->company_code.'/'.get_roman_month(date('m')).'/'.date('Y');
		$data['content']	= str_replace("{NOMOR_SURAT}", $letter_number, $data['content']);

		$data['_TITLE_'] 		= 'Surat Keterangan Kerja';
		$this->load->view('monitoring/work', $data);
	}

	public function export($type = FALSE)
	{
		$this->load->model(array('monitoring_model'));
		$this->load->library("Excel");
		$this->load->library('PHPExcel/iofactory');

		$title = "Monitoring Kontrak Karyawan";
		$params = $this->input->get();
		$params['columns'] 		= "A.id, B.employee_number, B.full_name, B.site_id, C.name AS company_name, D.name AS site_name, E.name AS position, DATE_FORMAT(A.contract_end, '%W, %M %d,%Y') AS contract_end, CONCAT_WS(', ', F.full_name, G.full_name, H.full_name) AS ro";

		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		$params['status']			= 1;
		$params['status_approval'] 	= 3;
		$params['status_nonjob'] 	= 0;
		
		$title = 'monitoring_kontrak_'.$type;
		$files = glob(FCPATH."files/".$title."/*");

		foreach($files as $file){
			gc_collect_cycles();
			if(is_file($file)) {
				unlink($file);
			}
		}

		if($type == 'direct'){
			$params['not_site_id']		= 2;
		}else{
			$params['site_id']		= 2;
		}
		
		$excel = new PHPExcel();
		$excel->getProperties()->setCreator($this->session->userdata('name'))
             ->setLastModifiedBy($this->session->userdata('name'))
             ->setTitle($title)
             ->setSubject($title)
             ->setDescription($title)
             ->setKeywords($title);
		
		$excel->setActiveSheetIndex(0);

		$i=1;
		$excel->getActiveSheet()->setCellValue("A".$i, "No");
		$excel->getActiveSheet()->setCellValue("B".$i, "ID Karyawan");
		$excel->getActiveSheet()->setCellValue("C".$i, "Nama Lengkap");
		$excel->getActiveSheet()->setCellValue("D".$i, "Unit Kontrak");
		$excel->getActiveSheet()->setCellValue("E".$i, "Site Bisnis");
		$excel->getActiveSheet()->setCellValue("F".$i, "Jabatan");
		$excel->getActiveSheet()->setCellValue("G".$i, "Akhir Kontrak");
		$excel->getActiveSheet()->setCellValue("H".$i, "RO");
		
		$list_monitoring 	= $this->monitoring_model->gets($params);
		$no = 1;		
		$i=2;
		foreach ($list_monitoring as $item) {
			$excel->getActiveSheet()->setCellValueExplicit("A".$i, $no);
			$excel->getActiveSheet()->setCellValueExplicit("B".$i, $item->employee_number);
			$excel->getActiveSheet()->setCellValue("C".$i, $item->full_name);
			$excel->getActiveSheet()->setCellValue("D".$i, $item->company_name);
			$excel->getActiveSheet()->setCellValue("E".$i, $item->site_name);
			$excel->getActiveSheet()->setCellValue("F".$i, $item->position);
			$excel->getActiveSheet()->setCellValue("G".$i, $item->contract_end);
			$excel->getActiveSheet()->setCellValue("H".$i, $item->ro);

			$i++;
			$no++;
		}

		$objWriter = IOFactory::createWriter($excel, 'Excel2007');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$title.'"');
		$filename = strtolower(str_replace(" ", "_", $title))."_tgl_".date('Y-m-d')."_jam_".date("h.i.s");
		$objWriter->save(FCPATH."files/".$title."/".$filename.".xlsx");
		redirect(base_url("files/".$title."/".$filename.".xlsx"));
		exit;		
	}

	public function list($type = FALSE){
		$this->load->model(array('contract_model'));
		$data['_TITLE_'] 		= 'Monitoring Kontrak';
		$data['_PAGE_'] 		= 'monitoring/list';
		$data['_MENU_PARENT_'] 	= $type;
		$data['_MENU_'] 		= 'monitoring_'.$type;
		$data['type'] 			= $type;
		$data['list_template'] = $this->contract_model->gets(array('type' => 'ADENDUM'));
		$this->view($data);
	}

	public function list_ajax($type = FALSE){
		$this->load->model(array('monitoring_model'));

		$column_index = $_POST['order'][0]['column']; 
		$params['columns'] 		= "A.id, A.id_card, A.employee_number, A.full_name, A.site_id, A.warning_letter, C.name AS company_name, B.name AS site_name, E.name AS position, DATE_FORMAT(D.contract_end, '%d/%m/%Y') AS contract_end, D.contract_end AS raw_end, CONCAT_WS(', ', F.full_name, G.full_name, H.full_name) AS ro, I.name AS business_name";
		$params['orderby'] 		= $_POST['columns'][$column_index]['data'];
		$params['order']		= $_POST['order'][0]['dir'];
		$params['limit']		= $_POST['length'];
		$params['page']			=  $_POST['start'];
		
		$params['branch_id']	= '';
		if($this->session->userdata('branch') > 1){
			$params['branch_id']= $this->session->userdata('branch');
		}
		$params['status']			= 1;
		$params['status_approval'] 	= 3;
		$params['status_nonjob'] 	= 0;

		if($type == 'direct'){
			$params['not_site_id']	= 2;
		}else{
			$params['site_id']		= 2;
		}

		$params['id_card']			= $_POST['columns'][1]['search']['value'];
		$params['employee_number']	= $_POST['columns'][2]['search']['value'];
		$params['full_name']		= $_POST['columns'][3]['search']['value'];
		$params['company_name']		= $_POST['columns'][4]['search']['value'];
		$params['site_name']		= $_POST['columns'][5]['search']['value'];
		$params['business_name']	= $_POST['columns'][6]['search']['value'];
		$params['position']			= $_POST['columns'][7]['search']['value'];
		$params['warning_letter']	= $_POST['columns'][8]['search']['value'];
		$params['contract_end']		= $_POST['columns'][9]['search']['value'];
		$params['ro']				= $_POST['columns'][10]['search']['value'];

		$list 	= $this->monitoring_model->gets($params);
		$total 	= $this->monitoring_model->gets($params, TRUE);
		
		$i 		= $_POST['start']+1;
		$data 	= array();

		foreach($list as $item)
		{
			$result['id']					= '<input type="checkbox"  class="form-check-input check-id" style="position:revert; margin:0 auto;" name="id[]" value="'.$item->id.'">';
			$result['no'] 					= $i;
			$result['id_card']				= $item->id_card;
			$result['employee_number']		= $item->employee_number;
			$result['full_name']			= $item->full_name;
			$result['company_name']			= $item->company_name;
			$result['site_name']			= $item->site_name;
			$result['business_name']		= $item->business_name;
			$result['position']				= $item->position;
			$result['warning_letter']		= $item->warning_letter;
			$contract_end					= "";
			if($item->raw_end){
				$now 		= date("Y-m-d");
				$dt2 = new DateTime("+1 month");
				$next_month = $dt2->format("Y-m-d");
				if($item->raw_end < $now){
					$contract_end = '<span class="text-danger">'.$item->contract_end.'</span>';
				}else if($item->raw_end < $next_month){
					$contract_end = '<span class="text-warning">'.$item->contract_end.'</span>';
				}else{
					$contract_end = '<span class="text-success">'.$item->contract_end.'</span>';	
				}
			}else{
				$result['contract_end']		= $item->contract_end;
			}
			$result['contract_end']			= $item->contract_end;
			$result['ro']					= $item->ro;
			$result['action'] 				=
				'<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("employee_contract/preview/".$item->id).'">Lihat</a>
				<a onclick="preview(this)" class="btn-sm btn-info btn-action btn-block"  style="cursor:pointer;" data-href="'. base_url("monitoring/work/".$item->id).'">Pekerjaan</a><p></p>
				<a class="btn-sm btn-success btn-block" href="'.base_url("monitoring/contract/".$type."/".$item->id).'">Kontrak</a>
				<a class="btn-sm btn-warning btn-block" href="'.base_url("monitoring/warning/".$type."/".$item->id).'">SPK</a>';
			array_push($data, $result);
			$i++;
		}
		
		$response = array(
			"iTotalRecords" 		=> $total,
			"iTotalDisplayRecords" 	=> $total,
			"aaData" 				=> $data
		);

		echo json_encode($response);
	}

	public function contract($type = FALSE, $employee_id = FALSE)
	{
		if (!$employee_id)
		{
			$data["message"] = message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger');
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
			redirect(base_url('employee_contract/list/'.$type));
		}

		$this->load->model(array('employee_model', 'employee_contract_model'));
		$data['employee'] = $this->employee_model->get(array('id'=>$employee_id, 'columns' => 'A.id, A.full_name, A.id_card, A.address_card, A.site_id'));
		$data['list_contract'] = $this->employee_contract_model->gets(array('employee_id'=>$employee_id, 'columns' => 'A.id, A.contract_type, DATE_FORMAT(A.contract_start, "%d/%m/%Y") AS contract_start, DATE_FORMAT(A.contract_end, "%d/%m/%Y") AS contract_end'));

		$data['_TITLE_'] 		= 'Kontrak Karyawan '.$data['employee']->full_name;
		$data['_PAGE_'] 		= 'employee_contract/contract';
		$data['_MENU_PARENT_'] 	= $type;
		$data['_MENU_'] 		= 'monitoring_'.$type;
		$data['type'] 			= $type;
		
		$this->view($data);
	}

	public function warning($type = FALSE, $employee_id = FALSE)
	{
		if (!$employee_id)
		{
			$data["message"] = message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger');
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
			redirect(base_url('employee_contract/list/'.$type));
		}

		$this->load->model(array('employee_model', 'employee_warning_model'));
		$data['employee'] = $this->employee_model->get(array('id'=>$employee_id, 'columns' => 'A.id, A.full_name, A.id_card, A.address_card, A.site_id'));
		$data['list_contract'] = $this->employee_warning_model->gets(array('employee_id'=>$employee_id, 'columns' => 'A.id, A.warning_type, A.warning_note, A.penalty, DATE_FORMAT(A.warning_date, "%d/%m/%Y") AS warning_date'));

		$data['_TITLE_'] 		= 'Peringatan Karyawan '.$data['employee']->full_name;
		$data['_PAGE_'] 		= 'monitoring/warning';
		$data['_MENU_PARENT_'] 	= $type;
		$data['_MENU_'] 		= 'monitoring_'.$type;
		$data['type'] 			= $type;
		
		$this->view($data);
	}
	
	public function form_warning($type = FALSE, $employee_id = FALSE, $warning_id = FALSE)
	{
		$this->load->model(array('city_model', 'employee_model', 'warning_model', 'employee_warning_model', 'candidate_model'));

		$data['id'] 				= '';
		$data['employee_id']		= $employee_id;
		$data['warning_template_id']= '';
		$data['warning_note'] 		= '';
		$data['penalty'] 			= '';
		$data['warning_type'] 		= '';
		$data['warning_date'] 		= '';

		if($this->input->post()){
			$insert['id']					= $data['id'] 					= $this->input->post('id');
			$insert['employee_id']			= $data['employee_id'] 			= $this->input->post('employee_id');
			$insert['warning_template_id']	= $data['warning_template_id'] 	= $this->input->post('warning_template_id');
			$insert['warning_note']			= $data['warning_note'] 		= $this->input->post('warning_note');
			$insert['penalty']				= $data['penalty'] 				= $this->input->post('penalty');
			$insert['warning_type']			= $data['warning_type'] 		= $this->input->post('warning_type');
			$insert['warning_date']			= $data['warning_date'] 		= $this->input->post('warning_date');
			
			$this->form_validation->set_rules('employee_id', '', 'required');
			$this->form_validation->set_rules('warning_date', '', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> silahkan lengkapi data anda sesuai dengan ketentuan.','danger'));
			}else{
				$template = $this->warning_model->get(array('id' => $insert['warning_template_id']));
				$content = $template->content;

				$warning_date	= substr($insert['warning_date'], 8, 2)." ".get_month(substr($insert['warning_date'], 5, 2))." ".substr($insert['warning_date'], 0, 4);
				$content = str_replace("{TANGGAL_PERINGATAN}",$warning_date, $content);
				$content = str_replace("{CATATAN_PERINGATAN}",$insert['warning_note'], $content);
				$content = str_replace("{SANKSI_PERINGATAN}",$insert['penalty'], $content);

				$employee 	= $this->employee_model->preview(array('id' => $employee_id));
				if($employee){
					$content = str_replace("{NAMA_LENGKAP}",$employee->full_name, $content);
					$content = str_replace("{NOMOR_KARYAWAN}",$employee->employee_number, $content);
					$content = str_replace("{NIK}",$employee->id_card, $content);

					$city = $this->city_model->preview(array('columns' => 'A.name, B.name AS province_name', 'id' => $employee->city_card_id));
					$address_card	= $employee->address_card;
					if($city){
						$address_card = $address_card.", ".$city->name.", ".$city->province_name;
					}
					$content = str_replace("{ALAMAT_KTP}",$address_card,$content);

					$birth_date	= substr($employee->date_birth, 8, 2)." ".get_month(substr($employee->date_birth, 5, 2))." ".substr($employee->date_birth, 0, 4);
					$content = str_replace("{TANGGAL_LAHIR}",$birth_date, $content);


					$birth_place = str_replace("KOTA", "", strtoupper($employee->city_birth));
					$birth_place = str_replace("KABUPATEN", "", strtoupper($employee->city_birth));
					$content = str_replace("{TEMPAT_LAHIR}",$birth_place, $content);
					$content = str_replace("{NAMA_SITE}",$employee->site_name, $content);
					$content = str_replace("{ALAMAT_SITE}",$employee->site_address, $content);
					$content = str_replace("{NAMA_BISNIS_UNIT}",$employee->company_name, $content);
					$content = str_replace("{ALAMAT_BISNIS_UNIT}",$employee->company_address, $content);
					$content = str_replace("{JABATAN}",$employee->position, $content);
				}else{
					$content = "";
				}
				$insert['warning'] = $content;

				$this->candidate_model->save(array('id' => $data['employee_id'], 'warning_letter' => $data['warning_type']));
				$save_id	 	= $this->employee_warning_model->save($insert);
				if ($save_id) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong>  data berhasil disimpan.','success'));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data gagal disimpan.','danger'));
				}
			}
			redirect(base_url('monitoring/warning/'.$type.'/'.$employee_id));
		}

		if ($warning_id)
		{
			$data = (array) $this->employee_warning_model->get(array('id' => $warning_id, 'A.id, A.employee_id, a.warning_template_id, A.warning_note, A.penalty, A.warning_date, A.warning_type'));
			if (empty($data))
			{
				$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> data tidak ditemukan.','danger'));
				redirect(base_url('monitoring/list/'.$type));
			}
		}

		$data['employee'] 		= $this->employee_model->get(array('id'=>$employee_id, 'columns' => 'A.id, A.full_name, A.id_card, A.address_card, A.site_id'));
		$data['list_warning'] 	= $this->warning_model->gets(array('columns' => 'A.id, A.title'));
		$data['_TITLE_'] 		= 'Surat Peringatan';
		$data['_PAGE_'] 		= 'monitoring/form_warning';

		$data['_MENU_PARENT_'] 	= $type;
		$data['_MENU_'] 		= 'monitoring_'.$type;
		$data['type'] 			= $type;

		return $this->view($data);
	}

	public function preview_warning($employee_id = FALSE)
	{
		$this->load->model(array('employee_model', 'city_model', 'warning_model','employee_warning_model'));
		
		$data['_TITLE_'] 		= 'Preview Surat Peringatan';
		$data['_PAGE_']	 		= 'monitoring/preview_warning';
		$data['_MENU_PARENT_'] 	= '';
		$data['_MENU_'] 		= '';

		$params = $this->input->get();
		$template = $this->warning_model->get(array('id' => $params['template']));
		$content = $template->content;

		$warning_date	= substr($params['warning_date'], 8, 2)." ".get_month(substr($params['warning_date'], 5, 2))." ".substr($params['warning_date'], 0, 4);
		$content = str_replace("{TANGGAL_PERINGATAN}",$warning_date, $content);
		$content = str_replace("{CATATAN_PERINGATAN}",$params['warning_note'], $content);
		$content = str_replace("{SANKSI_PERINGATAN}",$params['penalty'], $content);

		$employee 	= $this->employee_model->preview(array('id' => $employee_id));
		if($employee){
			$content = str_replace("{NAMA_LENGKAP}",$employee->full_name, $content);
			$content = str_replace("{NOMOR_KARYAWAN}",$employee->employee_number, $content);
			$content = str_replace("{NIK}",$employee->id_card, $content);

			$city = $this->city_model->preview(array('columns' => 'A.name, B.name AS province_name', 'id' => $employee->city_card_id));
			$address_card	= $employee->address_card;
			if($city){
				$address_card = $address_card.", ".$city->name.", ".$city->province_name;
			}
			$content = str_replace("{ALAMAT_KTP}",$address_card,$content);

			$birth_date	= substr($employee->date_birth, 8, 2)." ".get_month(substr($employee->date_birth, 5, 2))." ".substr($employee->date_birth, 0, 4);
			$content = str_replace("{TANGGAL_LAHIR}",$birth_date, $content);


			$birth_place = str_replace("KOTA", "", strtoupper($employee->city_birth));
			$birth_place = str_replace("KABUPATEN", "", strtoupper($employee->city_birth));
			$content = str_replace("{TEMPAT_LAHIR}",$birth_place, $content);
			$content = str_replace("{NAMA_SITE}",$employee->site_name, $content);
			$content = str_replace("{ALAMAT_SITE}",$employee->site_address, $content);
			$content = str_replace("{NAMA_BISNIS_UNIT}",$employee->company_name, $content);
			$content = str_replace("{ALAMAT_BISNIS_UNIT}",$employee->company_address, $content);
			$content = str_replace("{JABATAN}",$employee->position, $content);
		}else{
			$content = "";
		}
		$data['content'] = $content;   
		$this->load->view('monitoring/warning_preview', $data);
	}

	public function nonjob($type = FALSE)
	{
		if($this->input->post()){
			$this->load->model(array('employee_model'));
			$ids 		= $this->input->post('id');
			foreach ($ids as $employee_id) {
				$employee 	= $this->employee_model->get(array('id' => $employee_id, 'columns' => 'A.full_name'));
				if(!$employee){
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data Karyawan tidak ditemukan.','danger'));
					redirect(base_url('monitoring/list/'.$type));
				}
				$save_id = $this->employee_model->save(array('id' => $employee_id, 'status_nonjob' => 1));
			}
			$this->session->set_flashdata('message', message_box('<strong>Berhasil!</strong> Data Karyawan berhasil diproses.','success'));
			redirect(base_url('monitoring/list/'.$type));
		}else{
			$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data Karyawan tidak ditemukan.','danger'));
			redirect(base_url('monitoring/list/'.$type));
		}
	}

	public function delete_warning($type = FALSE, $employee_id = FALSE, $warning_id = false)
	{
		$this->load->model(array('employee_warning_model', 'candidate_model'));
		if ($warning_id){
            $data = (array) $this->employee_warning_model->get(array('id' => $warning_id));
			if (empty($data)){
                $this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
                redirect(base_url('monitoring/warning/'.$type.'/'.$employee_id));
            }else{
				$insert = array('id' => $warning_id, 'is_active' => 0);
				$result = $this->employee_warning_model->save($insert);
				$this->candidate_model->save(array('id' => $employee_id, 'warning_letter' => ''));
				if ($result) {
					$this->session->set_flashdata('message', message_box('<strong>Sukses!</strong> Data berhasil dihapus.','success'));
					redirect(base_url('monitoring/warning/'.$type.'/'.$employee_id));
				}else{
					$this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data gagal dihapus.','danger'));
					redirect(base_url('monitoring/warning/'.$type.'/'.$employee_id));
				}
			}
        }else{
			 $this->session->set_flashdata('message', message_box('<strong>Gagal!</strong> Data tidak ditemukan.','danger'));
            redirect(base_url('monitoring/list/'.$type));
		}
    }
}